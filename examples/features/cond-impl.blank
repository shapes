/** This file is part of Shapes.
 **
 ** Shapes is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** any later version.
 **
 ** Shapes is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 **
 ** Copyright 2009, 2010, 2013, 2014, 2015 Henrik Tidefelt
 **/

##lookin ..Shapes
##lookin ..Shapes..Control

/** This is what the standard implementation of <cond> looks like (or at least
 ** it did look like this at one point).  It uses forced immediate evaluation.
 **/
|** cond: \ <>cases →
|**   (escape_continuation return
|**     {
|**       ![(list []<>cases).foldr
|**          \ e p → [if e.car (escape_continue return e.cdr) p]
|**          void]
|**       ![error `No matching cond clause.´]
|**     })

IO..•stdout
  << `cond: ´
  << [cond [Data..cons 1=0 `Doesn't happen, but not evaluated anyway.´]
           [Data..cons 1=0 [error 'bad `(while testing cond)´ `This should never be evaluated (false case)!´]]
           [Data..cons 1=1 `This is the correct answer.´]
           [Data..cons 1=1 [error 'bad `(while testing cond)´ `This should never be evaluated (after true case)!´]]
           [Data..cons true `This is the default, in case no other case is true.´]]
  << "{n}


purecond: \ <>cases →
 {
   tmp: [(Data..list []<>cases).foldr
            \ e p → [if [typeof p] = Data..Type..§Void
                        [if e.car e p]
                        p]
            void]
   [if [typeof tmp] = Data..Type..§Void
       [error 'misc VARNAME `No matching cond clause.´]
       tmp.cdr]
  }

IO..•stdout
  << `purecond: ´
  << [purecond [Data..cons 1=0 `Doesn't happen, but not evaluated anyway.´]
               [Data..cons 1=0 [error 'bad `(while testing purecond)´ `This should never be evaluated (false case)!´]]
               [Data..cons 1=1 `This is the correct answer.´]
               [Data..cons 1=1 [error 'bad `(while testing purecond)´ `This should never be evaluated (after true case)!´]]
               [Data..cons true `This is the default, in case no other case is true.´]]
  << "{n}
