/** This file is part of Shapes.
 **
 ** Shapes is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** any later version.
 **
 ** Shapes is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 **
 ** Copyright 2008, 2014 Henrik Tidefelt
 **/

##lookin ..Shapes
##lookin ..Shapes..Geometry

|** We begin with two useful general-purpose abstractions.

rotateAbout: \ z a → [shift z]()[rotate a]()[shift ~z]

accumulateGraphics2D: \ lst fun → [lst.foldl \ p e → ( p & [fun e] )
                                              Graphics..null]


|** It is now straight-forward to implement the Koch curve (at some finite depth).

koch: \ pth depth →
  [if depth = '0
      pth
      {
        len: [Numeric..Math..abs pth]
        sl1: [pth len / 3]
        sl2: [pth 2 * len / 3]
        seg1: pth.begin--sl1
        seg2: sl1--sl2
        seg3: sl2--pth.end
        depth: ../depth - '1
        [koch seg1 depth]
          --[koch [rotateAbout sl1.p 60°][]seg2 depth]
          --[koch [rotateAbout sl2.p ~60°][]seg2 depth]
          --[koch seg3 depth]
      }]


|** Here's our first simple application to a straight line:

IO..•page << Traits..@width:0.3bp | [Graphics..stroke [koch (0cm,0cm)--(15cm,10cm) '6]]


|** As a more advanced application, we start from a circle, and draw also the curves at intermediate depths, in successively darker shades of gray:

IO..•page << {
           c: [shift (5cm,~10cm)][][Geometry..circle 5cm]
           end: '5
           [accumulateGraphics2D
             [Data..range '0 end]
             \ i → ( Traits..@stroking:[Layout..mediate i*(1/end) 0.9*Traits..BW..WHITE Traits..BW..BLACK]
                      |
                      [Graphics..stroke [koch c i]] ) ]
         }
