/** This file is part of Shapes.
 **
 ** Shapes is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** any later version.
 **
 ** Shapes is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 **
 ** Copyright 2008, 2010, 2014, 2015 Henrik Tidefelt
 **/

##lookin ..Shapes
##lookin ..Shapes..Geometry3D

/** Define transform to work with. **/
tf: [shift (3cm,4cm,5cm)]*[rotate angle:25° dir:(1,1,2)]
IO..•stdout << `Linear? ´ << tf.linear? << "{n}
IO..•stdout << `Translation? ´ << tf.translation? << "{n}
IO..•stdout << `Special? ´ << tf.special? << "{n}
IO..•stdout << `Euclidean? ´ << tf.Euclidean? << "{n}

IO..•stdout << `Transforms in new coordinates:´ << "{n}

/** Compute decompositions, both with automatic and manual selection of rank. **/
sda: [Schur_decomp tf]
IO..•stdout << sda.U << "{n}
IO..•stdout << [sda.U.chop L:0.00001 p:0.000001mm] << ` (chopped)´ << "{n}

sdac: [Schur_decomp tf canonical:true]
IO..•stdout << sdac.U << "{n}

sd0: [Schur_decomp tf rank:'0]
IO..•stdout << sd0.U << "{n}

sd2: [Schur_decomp tf rank:'2]
IO..•stdout << sd2.U << "{n}

/** Verify decompositions; if two transforms are equal, then combining one with
 ** the inverse of the other shall be the identity.
 **/
check: \ tf decomp →
{
  /** The decomposition is verified by reconstructing the original transform, and comparing
   ** with tf by composing with the inverse of tf, and comparing that result with the identity.
   ** The identity transform is characterized by being both linear and a translation, but to apply
   ** these predicates we must chop the elements of the transform first.
   **/
  tmp: [( [inverse tf] * decomp.Q*decomp.U*[inverse decomp.Q] ).chop L:0.00001 p:0.00001mm]
  `Verifying ´ + [Debug..sourceof decomp] + `: ´ + [if tmp.linear? and tmp.translation? `OK´ `Incorrect´]
}

IO..•stdout << `Verification:´ << "{n}
IO..•stdout << [check tf [Debug..locate sda]] << "{n}
IO..•stdout << [check tf [Debug..locate sdac]] << "{n}
IO..•stdout << [check tf [Debug..locate sd0]] << "{n}
IO..•stdout << [check tf [Debug..locate sd2]] << "{n}

IO..•stdout << `(Not canonical:) Change of coordinates is special? ´ << sda.Q.special? << "{n}
IO..•stdout << `(Canonical:) Change of coordinates is special? ´ << sdac.Q.special? << "{n}
