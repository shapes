/** This file is part of Shapes.
 **
 ** Shapes is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** any later version.
 **
 ** Shapes is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 **
 ** Copyright 2014 Henrik Tidefelt
 **/

##needs ..Shapes..Data / seq-support

##lookin ..Shapes
##lookin ..Shapes..Data
##lookin ..Shapes..Geometry
##lookin ..Shapes..Layout
##lookin ..Shapes..Graphics

/** Graph layout function for nodes on a circle.
 ** (Ignoring any previous content of node values.)
 **/
circleGraphLayout: \ g radius:3cm →
{
  delta: 360° / g.node_count
  [g.with_node_values [fmap (\ node → (> coords:radius*[dir node.index*delta] <)) g.nodes]]
}

/** Basic rendering of a graph with nodes layout.
 **/
renderMultiGraph: \ g →
{
  /** Rendering parameters **/
  nodeRadius: 0.75 * Text..@size
  multiplicityRadius: 0.7 * Text..@size

  /** A circle at each node. **/
  nodeCircles: [g.nodes.foldl \ s n → (s & [[shift n.value.coords] stroke[][circle nodeRadius]]) null]

  /** Node keys. **/
  nodeKeys: [g.nodes.foldl \ s n → (s & [[shift n.value.coords] (Text..newText << (String..newString << n.key) ) >> center_x >> [shift (0,~0.5*nodeRadius)]]) null]

  /** Draw each multiedge as a straigt line between the nodes. **/
  edgePath: \ me →
  {
    source: me.source.value.coords
    target: me.target.value.coords
    tmp: source--target
    (source + nodeRadius * tmp.begin.T)--(target + nodeRadius * tmp.end.rT)
  }

  /** Arrow head for directed multiedges. **/
  edgeArrow: [ShapesArrow width:4bp ...]

  /** A label showing the multiplicity of a multiedge, positioned inside an imaginary circle of radius multiplicityRadius. **/
  multiplicityLabel: \ count →
    (Text..newText << (String..newString << [String..sprintf `(%d)´ count]) ) >> center_x >> [shift (0,~0.5 * multiplicityRadius)]

  /** Stroke an edge path and add a multiplicity label to the side of the stroke. **/
  drawMultiEdge: \ me →
  {
    p: [edgePath me]
    sl: [p [Numeric..Math..abs p]/2]
    (Traits..@width: 3 * Traits..@width | [stroke p head:[if me.directed? edgeArrow NO_ARROW]])
    &
    [[shift sl.p + sl.N * multiplicityRadius] [multiplicityLabel me.count]]
  }

  /** Draw all edges. **/
  edgeStrokes: [g.multiedges.foldl \ s me → (s & [drawMultiEdge me]) null]

  /** Combine all results. **/
  nodeCircles & nodeKeys & edgeStrokes
}

/** Example graph.
 **/
g: [graph undirected:true parallel:true
      nodes: [list 'a 'b 'c]
      edges: [list
               (> 'a 'b label:'even <) (> 'a 'b label:'odd <)
               (> 'a 'c label:'even <) (> 'a 'c label:'odd <)
               (> 'b 'c label:'even <) (> 'b 'c label:'odd <)
               (> 'c 'b label:'even <) (> 'c 'b label:'odd <)
             ]
   ]

/** Apply layout and rendering functions.
 **/
Text..@size:9bp & Traits..@width:0.3bp | [renderMultiGraph [circleGraphLayout g]]
