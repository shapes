/** This file is part of Shapes.
 **
 ** Shapes is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** any later version.
 **
 ** Shapes is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 **
 ** Copyright 2014 Henrik Tidefelt
 **/

##needs ..Shapes..Data / seq-support

##lookin ..Shapes
##lookin ..Shapes..Data

/** Helper function for converting a sequence to a string, with elements separated by spaces.
 **/
seq_sep_string: \ seq → [seq_string [separate ` ´ seq]]

/** Set up a graph to work with. **/
g: [graph undirected:true
     nodes: [list 'a 'b 'c 'd]
     edges: [list (> 'a 'b <) (> 'a 'c <) (> 'b 'c <) (> 'd 'b <) (> 'd 'a <)]
   ]

/** Define a walk directly in terms of edges of the graph.  Usually, the edges are
 ** the result of some kind of search of traversal, but this time we just pick
 ** the edges by hand.
 **/
w: [walk [list
           [g.the_u_edge 'a 'b]
           [g.the_u_edge 'b 'd]
           [g.the_u_edge 'd 'a]
           [g.the_u_edge 'a 'd]
           [g.the_u_edge 'd 'b]
           [g.the_u_edge 'b 'c]
         ]
    ]

/** Examine the walk. **/
IO..•stdout << `Length of walk: ´ << w.edge_count << "{n}
IO..•stdout << `Simple?:        ´ << w.simple? << "{n}
IO..•stdout << `Node cover?:    ´ << w.node_cover? << "{n}
IO..•stdout << `Edge cover?:    ´ << w.edge_cover? << "{n}
IO..•stdout << `Open or closed: ´ << [if w.open? `open´ `closed´] << "{n}
IO..•stdout << `Edges in the walk: ´ << [seq_sep_string w.edges] << "{n}

/** Visit all nodes along the walk. **/
IO..•stdout << `Node keys along the walk: ´ << w.start.key
ignore [] [w.edges.foldsl
  \ n e •dst →
  {
    next: [n.trace e]
    •dst << ` ´ << next.key
    next
  }
  w.start
  IO..•stdout
]
IO..•stdout << "{n}
