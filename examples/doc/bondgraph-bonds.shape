/** This file is part of Shapes.
 **
 ** Shapes is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** any later version.
 **
 ** Shapes is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 **
 ** Copyright 2008, 2010, 2014 Henrik Tidefelt
 **/

##needs ..Applications..Blockdraw

##lookin ..Shapes
##lookin ..Shapes..Geometry
##lookin ..Applications..Blockdraw

##unit dx = 8cm
##unit dy = 3.5cm
dc: (2.5cm,0)

centermark: Traits..@stroking:[Traits..gray 0.7] | [Graphics..stroke (~1cm,0cm)--(1cm,0cm) & (0cm,~1cm)--(0cm,1cm) ]

helper: \ pos dc fun lbl →
[[shift pos]
{
  j1: sjunction
  j2: [shift dc] [] pjunction
  p: [bgconnect j1 j2]
  centermark & [[shift dc] centermark]
  & j1 & j2
  & [fun p]
  & [[shift 0.5*dc + (0,~1.2cm)] [Layout..shiftoff_wlm lbl Layout..to_bot]]
}]

IO..•page << [helper (1dx,3dy) dc [Graphics..stroke head:Graphics..ShapesArrow ...] [Graphics..TeX `\texttt{head:Graphics..ShapesArrow}´]]

IO..•page << [helper (1dx,2dy) dc [Graphics..stroke head:[bondgraphArrow doHook:true ...] ...] [Graphics..TeX `\texttt{head:[bondgraphArrow doHook:true ...]}´]]
IO..•page << [helper (2dx,2dy) dc [Graphics..stroke head:[bondgraphArrow doCausal:true ...] ...] [Graphics..TeX `\texttt{head:[bondgraphArrow doCausal:true ...]}´]]

IO..•page << [helper (1dx,1dy) dc ubond [Graphics..TeX `\texttt{ubond}´]]
IO..•page << [helper (2dx,1dy) dc hbond [Graphics..TeX `\texttt{hbond}´]]
IO..•page << [helper (3dx,1dy) dc tbond [Graphics..TeX `\texttt{tbond}´]]
