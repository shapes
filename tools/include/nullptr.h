#pragma once

template<class T>
class NullPtr
{
 public:
  NullPtr()
    { }
  operator T * () const { return 0; }
};
