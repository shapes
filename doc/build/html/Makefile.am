# This file is part of Shapes.
#
# Shapes is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# Shapes is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright 2008, 2010, 2013 Gustaf Hendeby

docdir = ../..
partsdir = ${docdir}/parts
extensionsdir = ${partsdir}/extensions
namespacedir = ${partsdir}/namespace
guidesdir = ${partsdir}/guides
formatsdir = ${partsdir}/formats
# Note: stylesdir must not be relative, or cyclic dependencies will result!
stylesdir = ${top_srcdir}/doc/styles
toolsdir = ${docdir}/tools
imagesdir = ${docdir}/images

POSTPROC_HTML = ${top_builddir}/doc/tools/postproc/postproc-html

SAXON = @SAXON@

SHAPES = $(top_builddir)/source/shapes
SHAPESFLAGS = --warn=error --resources=no \
              --fontmetricspath ${top_srcdir}/resources/fontmetrics \
              --needpath ${top_srcdir}/resources/extensions \
              --tp=no \
              --tmpdir "/tmp/shapes.${USER}" --tmp*=yes


include ../Makefile.goalscheck

# In combination with ../Makefile.goalscheck, this how we include a file only if the make goal does not match *clean*:
include $(if $(findstring clean,$(MAKECMDGOALS)),,${top_builddir}/source/SHAPES-VERSION-FILE)

DATE_PATTERN = '[0-9]\{4\}-[0-9]\{2\}-[0-9]\{2\}'
VERSION_PATTERN = '[0-9]\{1,\}\(\.[0-9]\{1,\}\)*\(-[a-z0-9]\{1,\}\)*'
SSI = ${top_builddir}/doc/tools/ssi/ssipp
# Setting BUILDDIR=^ means that paths will be relative to the current working
# directory.
SSIFLAGS = -dEXAMPLES=example/ -dBASE= -dBUILDDIR=^/ $(MORE_SSIFLAGS)
SSIFLAGS_SUBTOPIC = -dEXAMPLES=example/ -dBASE=../ -dBUILDDIR=^/ \
                     $(MORE_SSIFLAGS) -I${srcdir}
MORE_SSIFLAGS = -dMODIFICATION_DATE=$(shell echo ${VERSION_DATE} | \
             ${SED} -e 's!.*\('${DATE_PATTERN}'\).*!\1!' | \
             grep -e ${DATE_PATTERN} 2> /dev/null || echo --- ) \
               -dSHAPES_VERSION=$(shell echo ${VERSION_NUMBER} | \
             ${SED} -e 's![^0-9]*\('${VERSION_PATTERN}'\).*!\1!' | \
             grep -e ${VERSION_PATTERN} 2> /dev/null || echo ?.?.? ) \
               -dHTML_DOC_DIR=$(htmldir) -I${srcdir}

SHEXT_LIST = doc/blockdraw-tutorial-tut-prefs.shext doc/winding-rule-paths.shext
SHEXT_FILES = $(addprefix example/, $(SHEXT_LIST))

EXTRA_html_FILES = \
  algo-tol.html namespace-index.html edit.html install.html \
  man.html state-types.html structure.html syntax.html types.html \
  tutorial.html interactive.html index.html \
  $(addprefix namespace/, \
    Shapes.html \
    Shapes-Control.html \
    Shapes-Data.html \
    Shapes-Data-Type.html \
    Shapes-Debug.html \
    Shapes-Geometry.html \
    Shapes-Geometry3D.html \
    Shapes-Graphics.html \
    Shapes-Graphics-PDF.html \
    Shapes-Graphics-Tag.html \
    Shapes-Graphics3D.html \
    Shapes-IO.html \
    Shapes-Layout.html \
    Shapes-Numeric.html \
    Shapes-Numeric-Constant.html \
    Shapes-Numeric-Math.html \
    Shapes-Numeric-Random.html \
    Shapes-String.html \
    Shapes-Text.html \
    Shapes-Text-Font.html \
    Shapes-Traits.html \
    Shapes-Traits-BW.html \
    Shapes-Traits-Blend.html \
    Shapes-Traits-Cap.html \
    Shapes-Traits-Device.html \
    Shapes-Traits-Join.html \
    Shapes-Traits-Light.html \
    Shapes-Traits-RGB.html \
    Applications.html \
    Applications-Blockdraw.html \
  ) \
  $(addprefix extensions/, \
    Applications-Blockdraw-blockdraw.html \
    Applications-Blockdraw-bondgraph.html \
    Shapes-Control-cond.html \
    Shapes-Data-seq-support.html \
    Shapes-Geometry-circle.html \
    Shapes-Geometry-linkpaths.html \
    Shapes-Geometry-pathmapping.html \
    Shapes-Geometry3D-circle.html \
    Shapes-Graphics-arrowheads.html \
    Shapes-Graphics-metapostarrow.html \
    Shapes-Graphics-shapesarrow.html \
    Shapes-Graphics3D-arrowheads.html \
    Shapes-Graphics3D-metapostarrow.html \
    Shapes-Layout-basic-layout.html \
    Shapes-Layout-centering-X.html \
    Shapes-Layout-shiftoff.html \
    Shapes-Numeric-Constant-constants.html \
  ) \
  $(addprefix guides/, \
    paths.html 3D.html text.html graphs.html hierarchical.html index.html)

nobase_dist_html_DATA = shapes.css toc-miss.html
EXTRA_nobase_dist_html = ${html_FILES}

nobase_dist_noinst_DATA = \
  ${extensionsdir}/formats/html.xsl \
  ${namespacedir}/formats/html.xsl \
  ${guidesdir}/formats/html.xsl \
  $(addprefix ${formatsdir}/, \
     examplecode-html.xsl html.xsl language-elements-html.xsl \
     bindings-html.xsl man.xsl plain-book-html.xsl) \
  ${partsdir}/namespace-toc/formats/toc.xsl \
  $(foreach part,\
    $(addprefix ${partsdir}/, \
      algo-tol namespace-index edit man state-types \
        structure syntax tutorial interactive types index),\
    $(part)/formats/html.xsl $(wildcard $(part)/formats/*-html.xsl)) \
  $(patsubst ${srcdir}/%,%,\
    $(foreach part,\
      $(addprefix ${srcdir}/${partsdir}/, \
        algo-tol namespace-index edit man state-types \
          structure syntax tutorial interactive types index),\
      $(part)/formats/html.xsl $(wildcard $(part)/formats/*-html.xsl))) \
  $(SHEXT_FILES) \
  ${partsdir}/namespace-toc/toc-miss.html

EXTRA_DATA = ${EXTRA_nobase_dist_html}

html_FILES = ${EXTRA_html_FILES}
nobase_dist_html_DATA += ${EXTRA_nobase_dist_html}

EXTRA_DIST = ${nobase_dist_html_DATA}

# Temporary solution for putting the SF logo on each page:
ADD-SF: $(html_FILES)
	-for i in $(html_FILES); do \
    echo "Putting SF logo in " $$i; \
    ex -sC $$i < insert-SF-logo.ex; \
  done

guides/%.xml : ${guidesdir}/%.sxml ${DEPDIR}/guides/%.ssidep
	test -d $(@D) || ${mkdir_p} $(@D)
	if ${SSI} --in $< ${SSIFLAGS_SUBTOPIC} > guides/$*.Txml; \
  then \
    mv guides/$*.Txml $@; \
  else \
    ${RM} guides/$*.Txml; \
    exit 1; \
  fi

namespace/%.xml : ${namespacedir}/%.sxml ${DEPDIR}/namespace/%.ssidep
	test -d $(@D) || ${mkdir_p} $(@D)
	if ${SSI} --in $< ${SSIFLAGS_SUBTOPIC} > namespace/$*.Txml; \
  then \
    mv namespace/$*.Txml $@; \
  else \
    ${RM} namespace/$*.Txml; \
    exit 1; \
  fi

extensions/%.xml : ${extensionsdir}/%.sxml ${DEPDIR}/extensions/%.ssidep
	test -d $(@D) || ${mkdir_p} $(@D)
	if ${SSI} --in $< ${SSIFLAGS_SUBTOPIC} > extensions/$*.Txml; \
  then \
    mv extensions/$*.Txml $@; \
  else \
    ${RM} extensions/$*.Txml; \
    exit 1; \
  fi

${DEPDIR}/guides/%.ssidep : ${guidesdir}/%.sxml toc.xml Makefile
	test -d $(@D) || ${mkdir_p} ${@D}
	if $(SSI) --in $< $(SSIFLAGS_SUBTOPIC) --head guides/$*.xml \
    --deps > ${DEPDIR}/guides/$*.Tssidep; \
  then \
    ${SED} -e 's!^.*:!nobase_dist_noinst_DATA += $< !' \
      ${DEPDIR}/guides/$*.Tssidep > ${DEPDIR}/guides/$*.dist ; \
    ${SED} -e 's!^!$@ !' ${DEPDIR}/guides/$*.Tssidep > $@ ; \
    mv ${DEPDIR}/guides/$*.Tssidep $@; \
  else \
    ${RM} ${DEPDIR}/guides/$*.Tssidep; \
    exit 1; \
  fi

${DEPDIR}/namespace/%.ssidep : ${namespacedir}/%.sxml toc.xml Makefile
	test -d $(@D) || ${mkdir_p} ${@D}
	if $(SSI) --in $< $(SSIFLAGS_SUBTOPIC) --head namespace/$*.xml \
    --deps > ${DEPDIR}/namespace/$*.Tssidep; \
  then \
    ${SED} -e 's!^.*:!nobase_dist_noinst_DATA += $< !' \
      ${DEPDIR}/namespace/$*.Tssidep > ${DEPDIR}/namespace/$*.dist ; \
    ${SED} -e 's!^!$@ !' ${DEPDIR}/namespace/$*.Tssidep > $@ ; \
    mv ${DEPDIR}/namespace/$*.Tssidep $@; \
  else \
    ${RM} ${DEPDIR}/namespace/$*.Tssidep; \
    exit 1; \
  fi

${DEPDIR}/extensions/%.ssidep : ${extensionsdir}/%.sxml toc.xml Makefile
	test -d $(@D) || ${mkdir_p} ${@D}
	if $(SSI) --in $< $(SSIFLAGS_SUBTOPIC) --head extensions/$*.xml \
    --deps > ${DEPDIR}/extensions/$*.Tssidep; \
  then \
    ${SED} -e 's!^.*:!nobase_dist_noinst_DATA += $< !' \
      ${DEPDIR}/extensions/$*.Tssidep > ${DEPDIR}/extensions/$*.dist ; \
    ${SED} -e 's!^!$@ !' ${DEPDIR}/extensions/$*.Tssidep > $@ ; \
    mv ${DEPDIR}/extensions/$*.Tssidep $@; \
  else \
    ${RM} ${DEPDIR}/extensions/$*.Tssidep; \
    exit 1; \
  fi

# WARNING!
# Dependencies such as extensions/../dynamic.html will cause
# dynamic.html to be built as if it was the extension ../dynamic.html,
# which will produce the wrong result.  Therefore, we make an
# attempt at removing collapsable parts from the dependencies.
if HAVE_SAXON

extensions/%.html : extensions/%.xml ${extensionsdir}/formats/html.xsl \
                    ${formatsdir}/html.xsl
	test -d ${@D} || ${mkdir_p} ${@D} ; \
    ( $(SAXON) $< $$( test -f ${extensionsdir}/formats/html.xsl || \
      echo '${srcdir}'/ )${extensionsdir}/formats/html.xsl || exit 1 ) | \
    ${POSTPROC_HTML} > $@
	if $(SAXON) $@ $$( test -f ${toolsdir}/xhtml-deps/filter.xsl || \
    echo '${srcdir}'/)${toolsdir}/xhtml-deps/filter.xsl > \
      ${DEPDIR}/extensions/$*.Thtmldep ; \
  then \
    echo "nobase_dist_html_DATA += $$( ${SED} -e 's!\([^ ]\{1,\}\)!extensions/\1!g' < ${DEPDIR}/extensions/$*.Thtmldep | ${SED} -e 's![^/ ]\{1,\}/\.\./!!g' | tr ' ' '\n' | sort -u | tr '\n' ' ' )" > ${DEPDIR}/extensions/$*.htmldep ; \
    ${RM} ${DEPDIR}/extensions/$*.Thtmldep; \
  else \
    ${RM} ${DEPDIR}/extensions/$*.Thtmldep; \
    exit 1; \
  fi

namespace/%.html : namespace/%.xml ${namespacedir}/formats/html.xsl \
                    ${formatsdir}/html.xsl
	test -d ${@D} || ${mkdir_p} ${@D} ; \
    ( $(SAXON) $< $$( test -f ${namespacedir}/formats/html.xsl || \
      echo '${srcdir}'/ )${namespacedir}/formats/html.xsl || exit 1 ) | \
    ${POSTPROC_HTML} > $@
	if $(SAXON) $@ $$( test -f ${toolsdir}/xhtml-deps/filter.xsl || \
    echo '${srcdir}'/)${toolsdir}/xhtml-deps/filter.xsl > \
      ${DEPDIR}/namespace/$*.Thtmldep ; \
  then \
    echo "nobase_dist_html_DATA += $$( ${SED} -e 's!\([^ ]\{1,\}\)!namespace/\1!g' < ${DEPDIR}/namespace/$*.Thtmldep | ${SED} -e 's![^/ ]\{1,\}/\.\./!!g' | tr ' ' '\n' | sort -u | tr '\n' ' ' )" > ${DEPDIR}/namespace/$*.htmldep ; \
    ${RM} ${DEPDIR}/namespace/$*.Thtmldep; \
  else \
    ${RM} ${DEPDIR}/namespace/$*.Thtmldep; \
    exit 1; \
  fi

guides/%.html : guides/%.xml ${guidesdir}/formats/html.xsl \
                    ${formatsdir}/html.xsl
	test -d ${@D} || ${mkdir_p} ${@D} ; \
    ( $(SAXON) $< $$( test -f ${guidesdir}/formats/html.xsl || \
      echo '${srcdir}'/ )${guidesdir}/formats/html.xsl || exit 1 ) | \
    ${POSTPROC_HTML} > $@
	if $(SAXON) $@ $$( test -f ${toolsdir}/xhtml-deps/filter.xsl || \
    echo '${srcdir}'/)${toolsdir}/xhtml-deps/filter.xsl > \
      ${DEPDIR}/guides/$*.Thtmldep ; \
  then \
    echo "nobase_dist_html_DATA += $$( ${SED} -e 's!\([^ ]\{1,\}\)!guides/\1!g' < ${DEPDIR}/guides/$*.Thtmldep | ${SED} -e 's![^/ ]\{1,\}/\.\./!!g' | tr ' ' '\n' | sort -u | tr '\n' ' ' )" > ${DEPDIR}/guides/$*.htmldep ; \
    ${RM} ${DEPDIR}/guides/$*.Thtmldep; \
  else \
    ${RM} ${DEPDIR}/guides/$*.Thtmldep; \
    exit 1; \
  fi

endif # HAVE_SAXON

%.css : ${stylesdir}/html/%.css
	test -d $(@D) || ${mkdir_p} $(@D)
	${INSTALL_DATA} $< $@

toc-miss.html : ${partsdir}/namespace-toc/toc-miss.html
	test -d $(@D) || ${mkdir_p} $(@D)
	${INSTALL_DATA} $< $@

img/% : ${imagesdir}/%
	test -d $(@D) || ${mkdir_p} $(@D)
	${INSTALL_DATA} $< $@

img/%-www.jpg : img/%.png
	convert $(CONVERT_FLAGS) -scale 30% $< $@

example/%.shape : ${top_srcdir}/examples/%.shape
	test -d $(@D) || ${mkdir_p} $(@D)
	${INSTALL_DATA} $< $@

example/%.blank : ${top_srcdir}/examples/%.blank
	test -d $(@D) || ${mkdir_p} $(@D)
	${INSTALL_DATA} $< $@

example/%.shext : ${top_srcdir}/examples/%.shext
	test -d $(@D) || ${mkdir_p} $(@D)
	${INSTALL_DATA} $< $@

%.htmldep : %.ssidep
	@touch $@

.PRECIOUS : $(addprefix ${DEPDIR}/, $(html_FILES:.html=.ssidep) \
                                    $(html_FILES:.html=.htmldep) \
                                    $(html_FILES:.html=.dist) \
                                    namespace-toc.ssidep namespace-toc.dist) \
            $(html_FILES:.html=.xml \
            namespace-toc.xml)

clean-local :
	-find . \( -name "*~" -o -name "*.html" -o -name "*.xml" -o -name "*.css" \) -exec ${RM} {} \;
	-test -d ${DEPDIR} && find ${DEPDIR} -type f -exec ${RM} {} \;

distclean-local :
	-find . -type f -exec ${RM} {} \;

# In combination with ../Makefile.goalscheck, this how we include a file only if the make goal does not match *clean*:
include $(if $(findstring clean,$(MAKECMDGOALS)),,$(addprefix ${DEPDIR}/, $(html_FILES:.html=.ssidep)))

-include $(addprefix ${DEPDIR}/, $(html_FILES:.html=.htmldep))
-include $(addprefix ${DEPDIR}/, $(html_FILES:.html=.dist))
-include $(addprefix ${DEPDIR}/, namespace-toc.dist)

# When including ../Makefile.common, we must first set up htmldepvar.
htmldepvar = nobase_dist_html_DATA
include ../Makefile.common

html-local : ${nobase_dist_html_DATA} ${nobase_dist_noinst_DATA} FORCE
dist-hook : all-local
all-local : html-local
	${MAKE} html-local

FORCE :
