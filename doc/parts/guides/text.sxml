<!-- This file is part of Shapes.                                           -->
<!--                                                                        -->
<!-- Shapes is free software: you can redistribute it and/or modify         -->
<!-- it under the terms of the GNU General Public License as published by   -->
<!-- the Free Software Foundation, either version 3 of the License, or      -->
<!-- any later version.                                                     -->
<!--                                                                        -->
<!-- Shapes is distributed in the hope that it will be useful,              -->
<!-- but WITHOUT ANY WARRANTY; without even the implied warranty of         -->
<!-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          -->
<!-- GNU General Public License for more details.                           -->
<!--                                                                        -->
<!-- You should have received a copy of the GNU General Public License      -->
<!-- along with Shapes.  If not, see <http://www.gnu.org/licenses/>.        -->
<!--                                                                        -->
<!-- Copyright 2014 Henrik Tidefelt                                         -->

<book>
  <title>Text</title>
  <description>
    <p>Producing typeset text.</p>
  </description>
  <meta-selflink><guide-href name="text" /></meta-selflink>
  <base href=<!--#expand-next-string-->"$(BASE)" />
  <examples-home href=<!--#expand-next-string-->"$(EXAMPLES)" />
  <shapes-version number=<!--#expand-next-string-->"$(SHAPES_VERSION)" />
  <up-link><a part="guides">User's guide index</a></up-link>
  <external>
    <!--#include virtual="^/toc.xml" -->
  </external>
  <top>
    <p><str-Shapes /> provides an interface to the text capabilities of <str-PDF />.  Compared to using <str-LaTeX /> for typesetting, this will compile faster and yield smaller output files, while <str-LaTeX /> is your friend if you need to typeset maths or simply want typesetting consistency with other material produced using <str-LaTeX />.</p>

    <p>This guide is not yet written.  In the meantime, please refer to <named-statetype name="Text" />, and the silly example below.</p>

<example-with-output title="Font metrics and kerning" internal-id="guides:text:fontmeterdemo">
<image pdf="features/fontmeterdemo_3.pdf" jpg="features/fontmeterdemo_y_big.jpg" />
<source file="features/fontmeterdemo.shape">
<![CDATA[<!--#include depth="0" virtual="$(BUILDDIR)$(EXAMPLES)features/fontmeterdemo.shape" -->]]>
</source>
<caption>
  <p>Using a <named-statetype name="Text" /> state for producing typeset text, and then show that the computed bounding box is correct.</p>
</caption>
</example-with-output>
  </top>

</book>
