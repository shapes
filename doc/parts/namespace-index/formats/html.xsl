<?xml version="1.0" encoding="UTF-8"?>

<!-- This file is part of Shapes.                                           -->
<!--                                                                        -->
<!-- Shapes is free software: you can redistribute it and/or modify         -->
<!-- it under the terms of the GNU General Public License as published by   -->
<!-- the Free Software Foundation, either version 3 of the License, or      -->
<!-- any later version.                                                     -->
<!--                                                                        -->
<!-- Shapes is distributed in the hope that it will be useful,              -->
<!-- but WITHOUT ANY WARRANTY; without even the implied warranty of         -->
<!-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          -->
<!-- GNU General Public License for more details.                           -->
<!--                                                                        -->
<!-- You should have received a copy of the GNU General Public License      -->
<!-- along with Shapes.  If not, see <http://www.gnu.org/licenses/>.        -->
<!--                                                                        -->
<!-- Copyright 2015 Henrik Tidefelt                                         -->

<xsl:stylesheet
  version="2.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns="http://www.w3.org/1999/xhtml"
>
<xsl:output method="xml" indent="no"
 doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
 doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
/>

<xsl:include href="../../formats/html.xsl" />
<xsl:include href="../../formats/bindings-html.xsl" />
<xsl:include href="../../formats/language-elements-html.xsl" />

<xsl:template match="/book">
  <html>
    <head>
      <title><xsl:apply-templates select="title" /></title>
      <xsl:element name="link">
        <xsl:attribute name="rel">stylesheet</xsl:attribute>
        <xsl:attribute name="href"><xsl:value-of select="/book/base/@href" />shapes.css</xsl:attribute>
      </xsl:element>
    </head>
    <body>
      <xsl:call-template name="head-navigation" />
      <h2><xsl:apply-templates select="title" /></h2>
      <hr class="thick"/>
      <xsl:apply-templates select="top" />

      <hr class="thin"/>
      <div class="p" style="text-align:center;"><b>Alphabetical list of namespaces</b></div>
      <div class="p" style="text-align:center;">
        <xsl:for-each select="/book/external/toc/namespaces//namespace">
          <xsl:sort select="@name" />
          <xsl:variable name="ns"><xsl:value-of select="@name" /></xsl:variable>
          <xsl:if test="position() != 1"><xsl:text> </xsl:text></xsl:if>
          <span class="horizontal-item">
            <xsl:element name="a">
              <xsl:attribute name="class">discrete</xsl:attribute>
              <xsl:attribute name="href">
                #ns:
                <xsl:call-template name="namespace-to-a-id">
                  <xsl:with-param name="name"><xsl:value-of select="$ns" /></xsl:with-param>
                </xsl:call-template>
              </xsl:attribute>
              <xsl:call-template name="format-namespace-name">
                <xsl:with-param name="name"><xsl:value-of select="$ns" /></xsl:with-param>
              </xsl:call-template>
            </xsl:element>
          </span>
        </xsl:for-each>
      </div>
      <hr class="thin"/>

      <xsl:for-each select="/book/external/toc/namespaces//namespace">
        <xsl:sort select="@name" />
        <xsl:variable name="ns"><xsl:value-of select="@name" /></xsl:variable>
        <h4>
          <xsl:element name="a">
            <xsl:attribute name="name">
              ns:
              <xsl:call-template name="namespace-to-a-id">
                <xsl:with-param name="name"><xsl:value-of select="$ns" /></xsl:with-param>
              </xsl:call-template>
            </xsl:attribute>
            <xsl:call-template name="name-to-linked-namespace">
              <xsl:with-param name="name"><xsl:value-of select="$ns" /></xsl:with-param>
            </xsl:call-template>
          </xsl:element>
        </h4>
        <xsl:element name="table">
          <xsl:attribute name="class">loose</xsl:attribute>
          <xsl:element name="tr">
            <xsl:element name="th">Name</xsl:element>
            <xsl:element name="th">Summary</xsl:element>
          </xsl:element>
          <xsl:for-each select="/book/external/toc/bindings/binding[@namespace = $ns]">
            <xsl:sort select="@name" />
            <xsl:variable name="ext"><xsl:apply-templates mode="extension-of-binding" select="ancestor::book[1]"/></xsl:variable>
            <xsl:variable name="kind"><xsl:apply-templates mode="kind-of-binding" select="."/></xsl:variable>
            <xsl:element name="tr">
              <xsl:element name="td">
                <xsl:element name="span">
                  <xsl:attribute name="class">weak</xsl:attribute>
                  <xsl:call-template name="format-namespace-name"><xsl:with-param name="name"><xsl:value-of select="$ns" /></xsl:with-param></xsl:call-template><xsl:text>^</xsl:text>
                </xsl:element>
                <xsl:apply-templates mode="placed-binding" select="." />
                <xsl:element name="span">
                  <xsl:attribute name="class">weak</xsl:attribute>
                  <xsl:text>$</xsl:text>
                </xsl:element>
              </xsl:element>
              <xsl:element name="td">
                <xsl:apply-templates select="summary"/>
              </xsl:element>
            </xsl:element> <!-- tr -->
          </xsl:for-each>
        </xsl:element> <!-- table -->
      </xsl:for-each>
      <xsl:call-template name="SF-placeholder" />
    </body>
  </html>
</xsl:template>

</xsl:stylesheet>
