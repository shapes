<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="formats/html.xsl"?>

<!-- This file is part of Shapes.                                           -->
<!--                                                                        -->
<!-- Shapes is free software: you can redistribute it and/or modify         -->
<!-- it under the terms of the GNU General Public License as published by   -->
<!-- the Free Software Foundation, either version 3 of the License, or      -->
<!-- any later version.                                                     -->
<!--                                                                        -->
<!-- Shapes is distributed in the hope that it will be useful,              -->
<!-- but WITHOUT ANY WARRANTY; without even the implied warranty of         -->
<!-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          -->
<!-- GNU General Public License for more details.                           -->
<!--                                                                        -->
<!-- You should have received a copy of the GNU General Public License      -->
<!-- along with Shapes.  If not, see <http://www.gnu.org/licenses/>.        -->
<!--                                                                        -->
<!-- Copyright 2008, 2014, 2015 Henrik Tidefelt                             -->


<book>
  <namespace>..Shapes..Graphics</namespace>
  <extension>metapostarrow</extension>
  <description>
    <p>Arrowheads available through the prelude.</p>
  </description>
  <prelude />
  <needs></needs>

  <title><self /></title>
  <meta-selflink><self-href /></meta-selflink>
  <up-link><parent-namespace /></up-link>
  <base href=<!--#expand-next-string-->"$(BASE)" />
  <examples-home href=<!--#expand-next-string-->"$(EXAMPLES)" />
  <shapes-version number=<!--#expand-next-string-->"$(SHAPES_VERSION)" />
  <external>
    <!--#include virtual="^/toc.xml" -->
  </external>

  <top>
    <alphabetical-index/>
    <p>Please refer to <extension path="..Shapes..Graphics/arrowheads" /> for an introduction to arrowheads.</p>
  </top>

  <body>

    <system-binding name="MetaPostArrow">
      <function>
        <case>
          <arguments>
            <arg identifier="p">
              <type><named-type name="Path" /></type>
            </arg>
            <arg identifier="ahLength">
              <type><named-type name="Length" /></type>
              <default><value-the-void /></default>
            </arg>
            <arg identifier="ahAngle">
              <type><named-type name="Float" /></type>
              <default><eq>30°</eq></default>
            </arg>
            <arg identifier="fillAsStroking">
              <type><named-type name="Boolean" /></type>
              <default><eq><const-true /></eq></default>
            </arg>
          </arguments>
          <result>
            <type>
              <structure-type>
                <field name="picture"><type><named-type name="Drawable" /></type></field>
                <field name="cut"><type><named-type name="Length" /></type></field>
              </structure-type>
            </type>
          </result>
          <dynamic-references>Whole graphics state</dynamic-references>
          <description>
            <p>As the name of this function indicates, this arrowhead design is borrowed from <str-MetaPost />, although there are differences.  The main idea with the design is that the arrowhead should bend with the path, please refer to the canonical illustration on the <str-MetaPost /> <a href="http://ect.bell-labs.com/who/hobby/MetaPost.html">home page</a>. The argument <arg name="ahLength" /> suggests how long the arrowhead should be (this will only be overridden in exceptional cases when the path is too short).  If it is not provided, its value will be computed based on the width of the stroke and <arg name="ahAngle" /> — exactly how will remain subject to change for some time.  <arg name="ahAngle" /> is the angle at the tip of the arrowhead.  When <arg name="fillAsStroking" /> is true, the fill color of the arrowhead will be <dynamic namespace="..Shapes..Traits" name="stroking" /> instead of <dynamic namespace="..Shapes..Traits" name="nonstroking" />.</p>
          </description>
        </case>
      </function>
    </system-binding>

  </body>

</book>
