<?xml version="1.0" encoding="UTF-8"?>

<!-- This file is part of Shapes.                                           -->
<!--                                                                        -->
<!-- Shapes is free software: you can redistribute it and/or modify         -->
<!-- it under the terms of the GNU General Public License as published by   -->
<!-- the Free Software Foundation, either version 3 of the License, or      -->
<!-- any later version.                                                     -->
<!--                                                                        -->
<!-- Shapes is distributed in the hope that it will be useful,              -->
<!-- but WITHOUT ANY WARRANTY; without even the implied warranty of         -->
<!-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          -->
<!-- GNU General Public License for more details.                           -->
<!--                                                                        -->
<!-- You should have received a copy of the GNU General Public License      -->
<!-- along with Shapes.  If not, see <http://www.gnu.org/licenses/>.        -->
<!--                                                                        -->
<!-- Copyright 2008 Henrik Tidefelt                                         -->

<xsl:stylesheet
  version="2.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns="http://www.w3.org/1999/xhtml"
>
<xsl:output method="xml" indent="no"
 doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
 doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
/>

<xsl:include href="../../formats/html.xsl" />
<xsl:include href="../../formats/bindings-html.xsl" />
<xsl:include href="../../formats/examplecode-html.xsl" />
<xsl:include href="../../formats/plain-book-html.xsl" />
<xsl:include href="../../formats/language-elements-html.xsl" />

<xsl:template match="alphabetical-index">
  <xsl:variable name="ns"><xsl:value-of select="ancestor::book/namespace" /></xsl:variable>
  <xsl:variable name="ext"><xsl:value-of select="ancestor::book/extension" /></xsl:variable>
  <div class="p" style="text-align:center;"><b>Alphabetical index</b></div>
  <div class="p" style="text-align:center;">
    <xsl:for-each select="/book/external/toc/bindings/binding[@namespace = $ns and @extension = $ext]">
      <xsl:sort select="@name" />
      <xsl:if test="position() != 1"><xsl:text> </xsl:text></xsl:if>
      <span class="horizontal-item">
        <xsl:apply-templates mode="placed-binding" select="." />
      </span>
    </xsl:for-each>
  </div>
  <hr class="thin"/>
  <xsl:apply-templates select="/book/namespace" />
  <xsl:if test="/book/needs/a[@extension]">
    <div class="p">
      <b>Dependencies:</b>
      <xsl:for-each select="/book/needs/a[@extension]">
        <xsl:text>  </xsl:text>
        <xsl:apply-templates select="."/>
      </xsl:for-each>
    </div>
  </xsl:if>
  <hr class="thin"/>
</xsl:template>

<xsl:template match="index-of-books">
	<ul>
		<xsl:apply-templates select="/book/external/book" />
	</ul>
</xsl:template>

<xsl:template match="external/book">
	<li>
		<xsl:element name="a">
			<xsl:attribute name="href"><xsl:apply-templates select="meta-selflink" /></xsl:attribute>
			<b><xsl:apply-templates select="title" /></b>
		</xsl:element>
		<xsl:if test="prelude">
			<xsl:text> (in standard prelude)</xsl:text>
		</xsl:if>:
		<xsl:apply-templates select="description" />
	</li>
</xsl:template>

<xsl:template match="/book/namespace">
	<div class="p">
		<b>Namespace:</b><xsl:text>  </xsl:text>
		<xsl:call-template name="name-to-linked-namespace">
			<xsl:with-param name="name"><xsl:value-of select="." /></xsl:with-param>
		</xsl:call-template>
	</div>
</xsl:template>

</xsl:stylesheet>
