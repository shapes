<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="formats/html.xsl"?>

<!-- This file is part of Shapes.                                           -->
<!--                                                                        -->
<!-- Shapes is free software: you can redistribute it and/or modify         -->
<!-- it under the terms of the GNU General Public License as published by   -->
<!-- the Free Software Foundation, either version 3 of the License, or      -->
<!-- any later version.                                                     -->
<!--                                                                        -->
<!-- Shapes is distributed in the hope that it will be useful,              -->
<!-- but WITHOUT ANY WARRANTY; without even the implied warranty of         -->
<!-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          -->
<!-- GNU General Public License for more details.                           -->
<!--                                                                        -->
<!-- You should have received a copy of the GNU General Public License      -->
<!-- along with Shapes.  If not, see <http://www.gnu.org/licenses/>.        -->
<!--                                                                        -->
<!-- Copyright 2015 Henrik Tidefelt                                         -->


<book>
  <namespace>..Shapes..Geometry3D</namespace>
  <description>
    <p>Geometric operations in <str-3D />.</p>
  </description>

  <title><self /></title>
  <up-link><parent-namespace /></up-link>
  <base href=<!--#expand-next-string-->"$(BASE)" />
  <examples-home href=<!--#expand-next-string-->"$(EXAMPLES)" />
  <shapes-version number=<!--#expand-next-string-->"$(SHAPES_VERSION)" />
  <external>
    <!--#include virtual="^/toc.xml" -->
  </external>

  <top>
    <alphabetical-index/>
    <p>The geometric objects in <str-Shapes /> are coordinates and paths.  This namespace is for operations within this class of objects.  Once the geometric objects are painted (typically stroked or filled), they become graphical objects and are no longer under the topic of this namespace.  Compare <namespace name="..Shapes..Graphics3D" />.    This namespace is also used for the border between the <str-2D /> and <str-3D /> geometry worlds, so that <namespace name="..Shapes..Geometry" /> doesn't get disturbed by the things that only concern those that think in <str-3D />.</p>
    <p>Separation of <str-2D /> and <str-3D /> concerns in its current form was initiated with the introduction of namespaces in <str-Shapes />, and this separation is still work in progress.  This means that even when working purely in <str-3D />, it is still necessary to use contents of the <namespace name="..Shapes..Geometry" /> namespace from time to time.  The <binding namespace="..Shapes..Geometry" name="normalized" /> function is a typical such example; it still follows the old design where a single function is used for both <str-2D /> and <str-3D />.</p>
  </top>

  <section id="namespace/shapes/geometry3d/3d2d">
    <title>From <str-3D /> to <str-2D /> and back</title>
    <body>

<system-binding name="view">
  <function>
    <case constructor-of="Geometric">
      <arguments>
        <arg>
          <type><named-type name="Geometric3D" /></type>
        </arg>
      </arguments>
      <dynamic-references><dynamic name="eyez" /></dynamic-references>
      <description>
        <p>Apply pin-hole camera projection.  The geometry of the projection is documented under <dynamic name="eyez" />.</p>
        <p>Exactly how <str-3D /> objects, if they can at all, behave when being projected, depend on the exact object type.  <named-type name="Drawable3D" /> objects will result in <named-type name="Drawable" /> objects, the operation is generally very complicated if occlusions are to be taken into account.  (What makes it difficult is that <str-Shapes /> deals with scalable graphics, and cannot resort to a pixel <eq>z</eq>-buffer.)  There are three <str-3D /> graphics container types with different behaviors.  First, <named-type name="Group3D" /> takes no consideration of occlusions, and just puts the projections of its part on top of each other in the order in which they appear in the data structure.  The two remaining types tries to respect occlusions, and the reader is referred to the documentation of these types to find out more about their algorithms, see <named-type name="ZBuffer" /> and <named-type name="ZSorter" />.</p>
      </description>
      <see-also><value name="immerse" /></see-also>
    </case>
  </function>
</system-binding>

<dynamic-variable name="eyez">
  <type><named-type name="Length" /></type>
  <constraint><self /> &gt; <physical><scalar>0</scalar><unit>bp</unit></physical> <b>or</b> <self /> = <eq>∞</eq></constraint>
  <default><physical><scalar>50</scalar><unit>cm</unit></physical></default>
  <description>
    <p>When a <str-3D /> world is viewed through a pin-hole camera to obtain <str-2D /> image, the pin-hole camera (the eye) is located at <eq>( 0, 0, z<sub><rm>eye</rm></sub> )</eq>, and the image plane is <eq>z = 0</eq>.  The special float value <eq>∞</eq> can be used to place the eye at infinity, which will make the projection independent of <eq>z</eq> coordinates.</p>
  </description>
</dynamic-variable>

<system-binding name="immerse">
  <function>
    <case constructor-of="Geometric3D">
      <arguments>
        <arg>
          <type><named-type name="Geometric" /></type>
        </arg>
      </arguments>
      <dynamic-references></dynamic-references>
      <description>
        <p>Move object from <str-2D /> to <str-3D /> by equipping it with a <eq>z = 0</eq> coordinate.  The operation is reversed by <value name="view" />.</p>
      </description>
    </case>
  </function>
  <see-also><value name="view" /> <value name="facing" /></see-also>
</system-binding>

    </body>
  </section> <!-- end of namespace/shapes/geometry3d/3d2d -->


  <section id="namespace/shapes/geometry3d/syntax">
    <title>Syntax equivalents</title>
    <body>

<system-binding name="coords">
  <function>
    <case constructor-of="FloatTriple">
      <arguments>
        <arg>
          <type><named-type name="Float" /></type>
        </arg>
        <arg>
          <type><named-type name="Float" /></type>
        </arg>
        <arg>
          <type><named-type name="Float" /></type>
        </arg>
      </arguments>
      <dynamic-references></dynamic-references>
      <description>
        <p>Implements <syntax name="float-triple" />.</p>
      </description>
    </case>
    <case constructor-of="Coords3D">
      <arguments>
        <arg>
          <type><named-type name="LengthLike" /></type>
        </arg>
        <arg>
          <type><named-type name="LengthLike" /></type>
        </arg>
        <arg>
          <type><named-type name="LengthLike" /></type>
        </arg>
      </arguments>
      <dynamic-references></dynamic-references>
      <description>
        <p>Implements <syntax name="coords-3D" />.</p>
        <p>Note that all arguments must be <named-type name="LengthLike" />, with no exception.</p>
      </description>
    </case>
  </function>
</system-binding>

    </body>
  </section> <!-- end of namespace/shapes/geometry3d/syntax -->


  <section id="namespace/shapes/geometry3d/pointfun">
    <title>Functions of points</title>
    <body>

<system-binding name="orthogonal">
  <summary>Orthogonal vector</summary>
  <function>
    <case>
      <arguments>
        <arg>
          <type><named-type name="FloatTriple" /></type>
        </arg>
      </arguments>
      <result>
        <type><named-type name="FloatTriple" /></type>
      </result>
      <dynamic-references></dynamic-references>
      <description>
        <p>Produces vector which is orthogonal to the given one.  The funciton preserves norm, but the argument must not have zero length.</p>
        <p>The direction of the result is computed as a cross product with either the <eq>x</eq> or the <eq>y</eq> basis vector, depending on which one is less parallel to the argument.</p>
      </description>
    </case>
  </function>
</system-binding>

    </body>
  </section> <!-- end of namespace/shapes/geometry3d/pointfun -->


  <section id="namespace/shapes/geometry3d/pathfun">
    <title>Functions of paths</title>
    <body>

<system-binding name="emptypath">
  <simple-value>
    <type><named-type name="Path3D" /></type>
    <description>
      <p>The analogue of <value namespace="..Shapes..Geometry" name="emptypath" />, but in <str-3D /> instead of <str-2D />.</p>
    </description>
  </simple-value>
</system-binding>

    </body>
  </section> <!-- end of namespace/shapes/geometry3d/pathfun -->


  <section id="namespace/shapes/geometry3d/transforms">
    <title>Affine transforms</title>
    <body>

<system-binding name="affinetransform">
  <function>
    <case constructor-of="Transform3D">
      <arguments>
        <arg>
          <type><named-type name="FloatTriple" /></type>
        </arg>
        <arg>
          <type><named-type name="FloatTriple" /></type>
        </arg>
        <arg>
          <type><named-type name="FloatTriple" /></type>
        </arg>
        <arg>
          <type><named-type name="Coords3D" /></type>
        </arg>
      </arguments>
      <dynamic-references></dynamic-references>
      <description><p>Construct transform from multiplier for x, y, and z coordinates, followed by a shift.</p></description>
    </case>
  </function>
</system-binding>

<system-binding name="shift">
  <function>
    <case constructor-of="Transform3D">
      <arguments>
        <arg>
          <type><named-type name="Coords3D" /></type>
        </arg>
      </arguments>
      <dynamic-references></dynamic-references>
      <description><p>Construct transform.</p></description>
    </case>
  </function>
</system-binding>

<system-binding name="rotate">
  <function>
    <case constructor-of="Transform3D">
      <arguments>
        <arg identifier="dir">
          <type><named-type name="FloatTriple" /></type>
        </arg>
        <arg identifier="angle">
          <type><named-type name="Float" /></type>
        </arg>
      </arguments>
      <dynamic-references></dynamic-references>
      <description><p>Construct rotation transform about given direction.</p></description>
    </case>
  </function>
</system-binding>

<system-binding name="scale">
  <function>
    <case constructor-of="Transform3D">
      <arguments>
        <arg identifier="r">
          <type><named-type name="Float" /></type>
          <default>1</default>
        </arg>
        <arg identifier="x">
          <type><named-type name="Float" /></type>
          <default>1</default>
        </arg>
        <arg identifier="y">
          <type><named-type name="Float" /></type>
          <default>1</default>
        </arg>
        <arg identifier="z">
          <type><named-type name="Float" /></type>
          <default>1</default>
        </arg>
      </arguments>
      <dynamic-references></dynamic-references>
      <description><p>Construct transform that scales x by <arg name="r" /><char-cdot /><arg name="x" />, and similarly with y and z.</p></description>
    </case>
  </function>
</system-binding>

<system-binding name="inverse">
  <function>
    <case constructor-of="Transform3D">
      <arguments>
        <arg>
          <type><named-type name="Transform3D" /></type>
        </arg>
      </arguments>
      <dynamic-references></dynamic-references>
      <description><p>Constructs the inverse of a transform.  This is only possible if the linear part of the transform is non-singular.</p></description>
    </case>
  </function>
</system-binding>

<system-binding name="Schur_decomp">
  <function>
    <case>
      <arguments>
        <arg identifier="tf">
          <type><named-type name="Transform3D" /></type>
        </arg>
        <arg identifier="rank">
          <type><named-type name="Integer" /></type>
        </arg>
        <arg identifier="canonical">
          <type><named-type name="Boolean" /></type>
          <default><const-false /></default>
        </arg>
      </arguments>
      <result>
        <type>
          <structure-type>
            <field name="Q"><type><named-type name="Transform3D" /></type></field>
            <field name="U"><type><named-type name="Transform3D" /></type></field>
          </structure-type>
        </type>
      </result>
      <description>
        <p>Computes a rigid body coordinate transform that brings the transform <arg name="tf" /> into a simple form.  The linear part of the coordinate transform is given by the real Schur decomposition, ensuring that a real eigenvalue is associated with the third basis vector.  (Hence, if there is a 2 by 2 block in the Schur form, it will be in the upper left part.)  The translational part of the coordinate transform is computed to make the translational part in the new coordinates small.  The <arg name="rank"/> parameter determines how many equation to use when the translational part is computed.  If <arg name="rank" /> is not provided, it is determined automatically from singular values.  A <arg name="rank" /> of zero means that the translational part of the coordinate transform shall not be used at all.  If <arg name="canonical" /> is set, the coordinate transform is made special (that is, it preserves orientation), and if there is an extra degree of freedom in the coodinate transform due to complex eigenvalues in the linear part of <arg name="tf" />, it is used to minimize the angle of rotation in the coordinate transform.</p>
        <p>Note that the rank shall <em>not</em> be selected full (that is, <eq>3</eq>) if <arg name="tf" /> is a rigid body transformation; use <eq>2</eq> if the rotation is not close to zero, and <eq>0</eq> otherwise (or use the automatic selection).  Compare Chasles' theorem, which says that the new transform will in general have a non-trivial translational part.</p>
        <p>This function may be used to find a rotation's eigenvector (that is, the direction of the rotation), as the eigenvector will be in the third column of the linear part of the change of coordinates.
<pre>
tf: [rotate angle:25° dir:(1,2,3)]
•stdout &lt;&lt; [Schur_decomp tf].Q.Lz
</pre>
        </p>
<example-with-output title="Schur decomposition" internal-id="example:schur_decomp">
<source file="doc/schur.blank">
<![CDATA[<!--#include depth="0" virtual="$(BUILDDIR)$(EXAMPLES)doc/schur.blank" -->]]>
</source>
<stdout>
<![CDATA[<!--#include depth="0" virtual="$(BUILDDIR)$(EXAMPLES)doc/schur.stdout" -->]]>
</stdout>
<caption>
  <p>The generalization of real Schur decomposition to also include a translation.  It can be seen that rank <eq>2</eq> is selected for a rigid body transform with non-trivial rotation.  It is also seen that, whithout setting the <arg name="canonical" /> flag, the coordinate transform may not preserve orientation.</p>
</caption>
        </example-with-output>
      </description>
    </case>
  </function>
</system-binding>

    </body>
  </section> <!-- end of namespace/shapes/geometry3d/transforms -->

</book>
