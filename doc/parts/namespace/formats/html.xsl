<?xml version="1.0" encoding="UTF-8"?>

<!-- This file is part of Shapes.                                           -->
<!--                                                                        -->
<!-- Shapes is free software: you can redistribute it and/or modify         -->
<!-- it under the terms of the GNU General Public License as published by   -->
<!-- the Free Software Foundation, either version 3 of the License, or      -->
<!-- any later version.                                                     -->
<!--                                                                        -->
<!-- Shapes is distributed in the hope that it will be useful,              -->
<!-- but WITHOUT ANY WARRANTY; without even the implied warranty of         -->
<!-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          -->
<!-- GNU General Public License for more details.                           -->
<!--                                                                        -->
<!-- You should have received a copy of the GNU General Public License      -->
<!-- along with Shapes.  If not, see <http://www.gnu.org/licenses/>.        -->
<!--                                                                        -->
<!-- Copyright 2015 Henrik Tidefelt                                         -->

<xsl:stylesheet
  version="2.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns="http://www.w3.org/1999/xhtml"
>
<xsl:output method="xml" indent="no"
 doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
 doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
/>

<xsl:include href="../../formats/html.xsl" />
<xsl:include href="../../formats/bindings-html.xsl" />
<xsl:include href="../../formats/examplecode-html.xsl" />
<xsl:include href="../../formats/plain-book-html.xsl" />
<xsl:include href="../../formats/language-elements-html.xsl" />

<xsl:template match="alphabetical-index">
  <xsl:variable name="ns"><xsl:value-of select="ancestor::book/namespace" /></xsl:variable>
  <xsl:if test="/book/external/toc/namespaces//namespace[@name = $ns and namespace]">
    <div class="p" style="text-align:center;"><b>Nested namespaces</b></div>
    <div class="p" style="text-align:center;">
      <xsl:for-each select="/book/external/toc/namespaces//namespace[@name = $ns]/namespace">
        <xsl:sort select="@relative" />
        <xsl:if test="position() != 1"><xsl:text> </xsl:text></xsl:if>
        <span class="horizontal-item">
          <xsl:element name="a">
            <xsl:attribute name="class">discrete</xsl:attribute>
            <xsl:attribute name="href">
              <xsl:call-template name="namespace-to-href">
                <xsl:with-param name="name"><xsl:value-of select="@name" /></xsl:with-param>
              </xsl:call-template>
            </xsl:attribute>
            <xsl:call-template name="format-namespace-name">
              <xsl:with-param name="name"><xsl:value-of select="@relative" /></xsl:with-param>
            </xsl:call-template>
          </xsl:element>
        </span>
      </xsl:for-each>
    </div>
  </xsl:if>
  <xsl:if test="/book/external/toc/bindings/binding[@namespace = $ns and not(@extension)]">
    <xsl:if test="/book/external/toc/namespaces//namespace[@name = $ns and namespace]"> <!-- Make sure to have test that agrees with presence testing for earlier divisions. -->
      <hr class="thin"/>
    </xsl:if>
    <div class="p" style="text-align:center;"><b>Core bindings</b></div>
    <div class="p" style="text-align:center;">
      <xsl:for-each select="/book/external/toc/bindings/binding[@namespace = $ns and not(@extension)]">
        <xsl:sort select="@name" />
        <xsl:if test="position() != 1"><xsl:text> </xsl:text></xsl:if>
        <span class="horizontal-item">
          <xsl:apply-templates mode="placed-binding" select="." />
        </span>
      </xsl:for-each>
    </div>
  </xsl:if>
  <xsl:if test="/book/needs/a[@extension]">
    <xsl:if test="/book/external/toc/namespaces//namespace[@name = $ns and namespace] | /book/external/toc/bindings/binding[@namespace = $ns and not(@extension)]"> <!-- Make sure to have test that agrees with presence testing for earlier divisions. -->
      <hr class="thin"/>
    </xsl:if>
    <div class="p">
      <b>Dependencies:</b>
      <xsl:for-each select="/book/needs/a[@extension]">
        <xsl:text>  </xsl:text>
        <xsl:apply-templates select="."/>
      </xsl:for-each>
    </div>
  </xsl:if>
  <xsl:if test="/book/external/toc/namespaces//namespace[@name = $ns and extension]">
    <xsl:if test="/book/external/toc/namespaces//namespace[@name = $ns and namespace] | /book/external/toc/bindings/binding[@namespace = $ns and not(@extension)] | /book/needs/a[@extension]"> <!-- Make sure to have test that agrees with presence testing for earlier divisions. -->
      <hr class="thin"/>
    </xsl:if>
    <div class="p" style="text-align:center;"><b>Summary of extensions</b></div>
    <xsl:for-each select="/book/external/toc/namespaces//namespace[@name = $ns]/extension">
      <xsl:sort select="@relative" />
      <xsl:variable name="ext"><xsl:value-of select="@relative" /></xsl:variable>
      <div class="p" style="text-align:center;margin-top:0.8em;margin-bottom:0.0em;">
        <xsl:call-template name="name-to-linked-extension">
          <xsl:with-param name="namespace"><xsl:value-of select="$ns" /></xsl:with-param>
          <xsl:with-param name="name"><xsl:value-of select="@relative" /></xsl:with-param>
        </xsl:call-template>
      </div>
      <div class="p" style="text-align:center;margin-top:0.1em;">
        <xsl:for-each select="/book/external/toc/bindings/binding[@namespace = $ns and @extension = $ext]">
          <xsl:sort select="@name" />
          <xsl:if test="position() != 1"><xsl:text> </xsl:text></xsl:if>
          <span class="horizontal-item">
            <xsl:apply-templates mode="placed-binding" select="." />
          </span>
        </xsl:for-each>
      </div>
    </xsl:for-each>
  </xsl:if>
  <hr class="thin"/>
</xsl:template>

<xsl:template match="index-of-books">
  <ul>
    <xsl:apply-templates select="/book/external/book" />
  </ul>
</xsl:template>

<xsl:template match="external/book">
  <li>
    <xsl:element name="a">
      <xsl:attribute name="href"><xsl:apply-templates select="meta-selflink" /></xsl:attribute>
      <b><xsl:apply-templates select="title" /></b>
    </xsl:element>
    <xsl:if test="prelude">
      <xsl:text> (in standard prelude)</xsl:text>
    </xsl:if>:
    <xsl:apply-templates select="description" />
  </li>
</xsl:template>

</xsl:stylesheet>
