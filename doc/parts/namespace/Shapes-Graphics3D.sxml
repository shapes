<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="formats/html.xsl"?>

<!-- This file is part of Shapes.                                           -->
<!--                                                                        -->
<!-- Shapes is free software: you can redistribute it and/or modify         -->
<!-- it under the terms of the GNU General Public License as published by   -->
<!-- the Free Software Foundation, either version 3 of the License, or      -->
<!-- any later version.                                                     -->
<!--                                                                        -->
<!-- Shapes is distributed in the hope that it will be useful,              -->
<!-- but WITHOUT ANY WARRANTY; without even the implied warranty of         -->
<!-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          -->
<!-- GNU General Public License for more details.                           -->
<!--                                                                        -->
<!-- You should have received a copy of the GNU General Public License      -->
<!-- along with Shapes.  If not, see <http://www.gnu.org/licenses/>.        -->
<!--                                                                        -->
<!-- Copyright 2015 Henrik Tidefelt                                         -->


<book>
  <namespace>..Shapes..Graphics3D</namespace>
  <description>
    <p>Handling the things in a <str-3D /> scene.</p>
  </description>

  <title><self /></title>
  <up-link><parent-namespace /></up-link>
  <base href=<!--#expand-next-string-->"$(BASE)" />
  <examples-home href=<!--#expand-next-string-->"$(EXAMPLES)" />
  <shapes-version number=<!--#expand-next-string-->"$(SHAPES_VERSION)" />
  <external>
    <!--#include virtual="^/toc.xml" -->
  </external>

  <top>
    <alphabetical-index/>
    <p>A <str-3D /> graphical object in <str-Shapes /> is basically something that ca be projected to <str-2D />, and then results in a <str-2D /> graphical object.  This namespace is also used for the border between the <str-2D /> and <str-3D /> graphical worlds, so that <namespace name="..Shapes..Graphics" /> doesn't get disturbed by the things that only concern those that work with <str-3D /> scenes.</p>
    <p>Separation of <str-2D /> and <str-3D /> concerns in its current form was initiated with the introduction of namespaces in <str-Shapes />, and this separation is still work in progress.  This means that even when working purely in <str-3D />, it is still necessary to use contents of the <namespace name="..Shapes..Graphics" /> namespace from time to time.  The <binding namespace="..Shapes..Graphics" name="stroke" /> function is a typical such example; it still follows the old design where a single function is used for both <str-2D /> and <str-3D />.</p>
  </top>

  <section id="namespace/shapes/graphics3d/elementary">
    <title>Elementary <str-3D /></title>
    <body>

<system-binding name="null">
  <simple-value>
    <type><named-type name="Drawable3D" /></type>
    <description>
      <p>The analogue of <value namespace="..Shapes..Graphics" name="null" />, but in <str-3D /> instead of <str-2D />.</p>
    </description>
  </simple-value>
</system-binding>

<system-binding name="newGroup">
  <hot>
    <constructor-of><named-statetype name="Group3D" /></constructor-of>
    <description></description>
  </hot>
</system-binding>

    </body>
  </section> <!-- end of namespace/shapes/graphics3d/elementary -->

  <section id="namespace/shapes/graphics3d/facets">
    <title>Facets</title>
    <body>

<system-binding name="facet">
  <function>
    <case constructor-of="Drawable3D">
      <arguments>
        <arg identifier="path">
          <type><named-type name="Path" /></type>
        </arg>
        <arg identifier="n1">
          <type><template-type name="N" /></type>
        </arg>
        <arg identifier="n2">
          <type><template-type name="N" /></type>
        </arg>
        <arg identifier="n3">
          <type><template-type name="N" /></type>
        </arg>
        <arg identifier="tiebreaker">
          <type><named-type name="Length" /></type>
          <default>0</default>
        </arg>
        <arg identifier="double">
          <type><named-type name="Boolean" /></type>
        </arg>
      </arguments>
      <dynamic-references><dynamic name="nonstroking" /> <dynamic name="reflections" /> <dynamic name="autointensity" /> <dynamic name="autoscattering" /> <dynamic name="viewresolution" /> <dynamic name="shadeorder" /></dynamic-references>
      <type-templates>
        <template name="N">
          <description>
            <p>Type of surface normal.  Should either be <named-type name="SurfaceNormalGray" /> or <named-type name="SurfaceNormalRGB" />.</p>
          </description>
        </template>
      </type-templates>
      <description>
        <p>The only required argument is <arg name="path" />, which must be a flat polygon.  The three arguments of type <template-type name="N" /> will be described shortly.  The parameter <arg name="tiebreaker" /> has the same meaning as for <value namespace="..Shapes..Graphics" name="fill" />, and <arg name="double" /> tells whether the surface should be visible when viewed from the back.  Surfaces that are not visisble when viewed from the back allow more efficient rendering when they cover a solid body, so that one knows that the inside of the body surface should never be visible.  The default value for <arg name="double" /> depends on the presence of surface normals; if there are none, it defaults to true, otherwise it defaults to false unless the surface normals disagree on which is the visible side.</p>
        <p>Up to three surface normals may be provided (provide <arg name="n1" /> before <arg name="n2" /> and so forth).  A surface normal has a location on the surface and specifies both a normal direction to be used in light computations (although the surface is physically flat, it can be treated as if curved in the light model), and a color.  See <value name="facetnormal" /> for more details on how to construct surface normals.  If no facet normal is provided, one is computed by using the physical normal direction of <arg name="path" />, with color determined by <dynamic namespace="..Shapes..Traits" name="nonstroking" />.  If just one facet normal is provided, it defines a constant normal direction and color over the whole facet.  If more than one is provided, normal direction and color are interpolated over the facet.</p>
        <p>Please refer to the documentation on the many dynamic variables accessed by this function, to learn how these affect the light computations.</p>
      </description>
    </case>
  </function>
</system-binding>

    </body>
  </section> <!-- end of namespace/shapes/graphics3d/facets -->

  <section id="namespace/shapes/graphics3d/scenes">
    <title>Scenes and light</title>
    <body>

<system-binding name="newLights">
  <hot>
    <constructor-of><named-statetype name="Lights" /></constructor-of>
    <description></description>
  </hot>
</system-binding>

<system-binding name="newZSorter">
  <hot>
    <constructor-of><named-statetype name="Drawable3D" /></constructor-of>
    <description></description>
  </hot>
</system-binding>

<system-binding name="newZBuf">
  <hot>
    <constructor-of><named-statetype name="Drawable3D" /></constructor-of>
    <description></description>
  </hot>
</system-binding>

    </body>
  </section> <!-- end of namespace/shapes/graphics3d/scenes -->

  <section id="namespace/shapes/graphics3d/quasi">
    <title>Quasi <str-3D /></title>
    <body>

<system-binding name="facing">
  <function>
    <case constructor-of="Drawable3D">
      <arguments>
        <arg identifier="obj">
          <type><named-type name="Drawable" /></type>
        </arg>
        <arg identifier="scale">
          <type><named-type name="Boolean" /></type>
          <default>false</default>
        </arg>
        <arg identifier="distort">
          <type><named-type name="Boolean" /></type>
          <default>false</default>
        </arg>
      </arguments>
      <description>
        <p>Make a special kind of <str-3D /> object at <eq>z = 0</eq>, which will always face the viewer even after <str-3D /> transforms.  It is useful for labels and other <str-2D /> annotations that need to be positioned in terms of objects in a <str-3D /> world.</p>
        <p>When the object is viewed after being transformed in <str-3D />, it will just be transformed by a <str-2D /> transform, so the object never has to go truly <str-3D />.  The object will be shifted according to how its origin moved by the <str-3D /> transform.  The arguments <arg name="scale" /> and <arg name="distort" /> determine how the linear part of the <str-2D /> transform is obtained from the <str-3D /> transform.  If <arg name="scale" /> is set, the object will be scaled according to the <eq>z</eq> coordinate of its transformed origin.  If <arg name="distort" /> is set, the linear <eq>(x,y)</eq> part of the <str-3D /> transform will be used.</p>
      </description>
    </case>
    <case constructor-of="Drawable3D">
      <arguments>
        <arg identifier="obj">
          <type>
            <function-type>
              <arguments>
                <arg><type><named-type name="Transform3D" /></type></arg>
              </arguments>
              <result><named-type name="Drawable" /></result>
            </function-type>
          </type>
        </arg>
      </arguments>
      <dynamic-references><dynstate name="all" /></dynamic-references>
      <description>
        <p>Gives the user full control of how to display the object when being viewed through a <str-3D /> transform.  The dynamic state is captured, and will be in scope later when the embedded function is invoked.</p>

        <note>
          <p>The evaluation order is quite non-standard for the objects created here.  Viewing an object is generally an atomic operation from a continuation passing point of view.  Hence, when the function is applied to the transform to obtain the <str-2D /> result, this will take place in an evaluation loop of its own.  This is very likely to render generated error messages rather useless when it comes to localizing the problem.</p>
          <p>Since the object created here only has a meaning when being viewed, it cannot be searched (see <value namespace="..Shapes..Graphics..Tag" name="tag" /> and <value namespace="..Shapes..Graphics..Tag" name="find" />) for tags as long as it remains in <str-3D />.</p>
        </note>

        <p>If you ever considered solving the intricate problem of generating nice arrowheads for paths in <str-3D /> by simply treating the path as if it was in <str-2D />, think again, as the example below shows!</p>

        <example-with-output title="Example" internal-id="example:facing-function">
          <image pdf="features/facefun_3.pdf" jpg="features/facefun_y_big.jpg" />
<source file="features/facefun.shape">
<![CDATA[<!--#include depth="0" virtual="$(BUILDDIR)$(EXAMPLES)features/facefun.shape" -->]]>
</source>
        </example-with-output>
      </description>
    </case>
  </function>
  <see-also><value name="immerse" /></see-also>
</system-binding>

    </body>
  </section> <!-- end of namespace/shapes/graphics3d/quasi -->

</book>
