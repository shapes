<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="formats/html.xsl"?>

<!-- This file is part of Shapes.                                           -->
<!--                                                                        -->
<!-- Shapes is free software: you can redistribute it and/or modify         -->
<!-- it under the terms of the GNU General Public License as published by   -->
<!-- the Free Software Foundation, either version 3 of the License, or      -->
<!-- any later version.                                                     -->
<!--                                                                        -->
<!-- Shapes is distributed in the hope that it will be useful,              -->
<!-- but WITHOUT ANY WARRANTY; without even the implied warranty of         -->
<!-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          -->
<!-- GNU General Public License for more details.                           -->
<!--                                                                        -->
<!-- You should have received a copy of the GNU General Public License      -->
<!-- along with Shapes.  If not, see <http://www.gnu.org/licenses/>.        -->
<!--                                                                        -->
<!-- Copyright 2015 Henrik Tidefelt                                         -->


<book>
  <namespace>..Shapes..Debug</namespace>
  <description>
    <p>Debugging utilities and source code reflection.</p>
  </description>

  <title><self /></title>
  <up-link><parent-namespace /></up-link>
  <base href=<!--#expand-next-string-->"$(BASE)" />
  <examples-home href=<!--#expand-next-string-->"$(EXAMPLES)" />
  <shapes-version number=<!--#expand-next-string-->"$(SHAPES_VERSION)" />
  <external>
    <!--#include virtual="^/toc.xml" -->
  </external>

  <top>
    <alphabetical-index/>
    <p>Didn't get the result you expected?  Maybe it's time to set a break point or add some debug prints.  This is where you find the tools.</p>
  </top>

  <section id="namespace/shapes/debug/reflection">
    <title>Source code reflection</title>

    <body>

<system-binding name="locate">
  <function>
    <case>
      <arguments>
        <arg>
          <type><named-type name="Value" /></type>
        </arg>
      </arguments>
      <result>
        <type><named-type name="Value" /></type>
      </result>
      <dynamic-references></dynamic-references>
      <description>
        <p><em>Mutates</em> the value in order to associate it with the abstract syntax tree node of the argument passed to this function.</p>
      </description>
      <see-also>
        <value name="sourceof" />
      </see-also>
    </case>
  </function>
</system-binding>

<system-binding name="sourceof">
  <function>
    <case constructor-of="String">
      <arguments>
        <arg>
          <type><named-type name="Value" /></type>
        </arg>
      </arguments>
      <dynamic-references></dynamic-references>
      <description>
        <p>Returns the source code string determined by the abstract syntax tree node associated with the argument.  The value needs to be properly prepared using <value name="locate" />.</p>
      </description>
    </case>
  </function>
</system-binding>

    </body>
  </section> <!-- end of namespace/shapes/debug/reflection -->


  <section id="namespace/shapes/debug/profiling">
    <title>Profiling</title>

    <body>

<system-binding name="newTimer">
  <hot>
    <constructor-of><named-statetype name="Float" /></constructor-of>
    <description>
      <p>When the timer is frozen, it returns the time in seconds since its creation.</p>
    </description>
  </hot>
</system-binding>

    </body>
  </section> <!-- end of namespace/shapes/debug/profiling -->


  <section id="namespace/shapes/debug/interactive">
    <title>Interactive mode</title>
    <top>
      <p>The escape continuations in <str-Shapes /> are not treated as dynamic variables, but they are nevertheless dynamically bound, which is why they are listed among the global dynamic variables.  In the non-interactive mode of the compiler, there is only one globally available escape continuation, but there are some more available in the interactive mode.</p>
      <p>See <a part="syntax" id="syntax/continuations" /> for more information on escape continuations in general.</p>
    </top>

    <body>

<escape-continuation name="bye">
  <purpose><p>Quit compiler.</p></purpose>
  <type>
    <union-type>
      <named-type name="Integer" />
      <named-type name="Symbol" />
    </union-type>
  </type>
  <default><p>Described in <a part="interactive" id="interactive/continuations" />.  <em>Only available in interactive mode.</em></p></default>
</escape-continuation>

<escape-continuation name="top_repl">
  <purpose><p>The read-evaluate-print loop of the interactive mode.</p></purpose>
  <type>
    <named-type name="Value" />
  </type>
  <default><p>Described in <a part="interactive" id="interactive/continuations" />.  <em>Only available in interactive mode.</em></p></default>
</escape-continuation>

<escape-continuation name="resume">
  <purpose><p>Resume program after break during interactive debugging.</p></purpose>
  <type><em>depends on context</em></type>
  <default><p>Described in <a part="interactive" id="interactive/debug" />.  <em>Only available in debug context in interactive mode.</em></p></default>
</escape-continuation>

<system-state name="db">
  <type><named-statetype name="Debugger" /></type>
  <description>
    <p><em>There is no global <db-state /> state.</em>  This piece of documentation is just here to guide readers to the documentation in <a part="interactive" id="interactive-debug" />.</p>
  </description>
  <see-also>
    <db-state />
  </see-also>
</system-state>

    </body>
  </section> <!-- end of namespace/shapes/debug/interactive -->


</book>
