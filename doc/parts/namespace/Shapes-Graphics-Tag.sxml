<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="formats/html.xsl"?>

<!-- This file is part of Shapes.                                           -->
<!--                                                                        -->
<!-- Shapes is free software: you can redistribute it and/or modify         -->
<!-- it under the terms of the GNU General Public License as published by   -->
<!-- the Free Software Foundation, either version 3 of the License, or      -->
<!-- any later version.                                                     -->
<!--                                                                        -->
<!-- Shapes is distributed in the hope that it will be useful,              -->
<!-- but WITHOUT ANY WARRANTY; without even the implied warranty of         -->
<!-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          -->
<!-- GNU General Public License for more details.                           -->
<!--                                                                        -->
<!-- You should have received a copy of the GNU General Public License      -->
<!-- along with Shapes.  If not, see <http://www.gnu.org/licenses/>.        -->
<!--                                                                        -->
<!-- Copyright 2015 Henrik Tidefelt                                         -->


<book>
  <namespace>..Shapes..Graphics..Tag</namespace>
  <description>
    <p>Tagged values inside graphics.</p>
  </description>

  <title><self /></title>
  <up-link><parent-namespace /></up-link>
  <base href=<!--#expand-next-string-->"$(BASE)" />
  <examples-home href=<!--#expand-next-string-->"$(EXAMPLES)" />
  <shapes-version number=<!--#expand-next-string-->"$(SHAPES_VERSION)" />
  <external>
    <!--#include virtual="^/toc.xml" -->
  </external>

  <top>
    <alphabetical-index/>
    <p>Graphics in both <str-2D /> and <str-3D /> may hold tagged values.  Unlike just having an ordinary map from keys to values, tagged values have the advantage that they can transform with the graphics in which they are contained.</p>
  </top>

  <body>

<system-binding name="tag">
  <function>
    <case>
      <arguments>
        <arg identifier="key">
          <type><named-type name="Symbol" /></type>
        </arg>
        <arg identifier="obj">
        </arg>
        <arg identifier="transform">
          <type><named-type name="Boolean" /></type>
          <default>true</default>
        </arg>
        <arg identifier="draw">
          <type><named-type name="Boolean" /></type>
          <default>true</default>
        </arg>
      </arguments>
      <result>
        <type><named-type name="Value" /></type>
      </result>
      <dynamic-references></dynamic-references>
      <description>
        <p>Creates a tagged object by attaching key <arg name="key" /> to <arg name="obj" />.</p>
        <p>The result is either a <named-type name="Drawable" /> or a <named-type name="Drawable3D" />, depending on the type of <arg name="obj" /> and the values of <arg name="transform" /> and <arg name="draw" />.  Non-geometric values result in <named-type name="Drawable" />.</p>
        <p><arg name="transform" /> means the tagged object shall try to preserve the geometric nature of <arg name="obj" />, while <arg name="draw" /> means that the drawable nature should be preserved.  Note that a drawable object is also geometric.</p>
        <p>Tagged objects are useful for many purposes.  At the lowest level, they may be retrieved later by their key, and they can be referred to for removal from a group state.</p>
      </description>
      <see-also>
        <value name="find" /> <value name="findall" /> <named-statetype name="Group" />
      </see-also>
    </case>
  </function>
</system-binding>

<system-binding name="find">
  <function>
    <case>
      <arguments>
        <arg identifier="container">
          <type><named-type name="Group" /></type>
        </arg>
        <arg identifier="key">
          <type><named-type name="Symbol" /></type>
        </arg>
      </arguments>
      <result>
        <type><named-type name="Value" /></type>
      </result>
      <dynamic-references></dynamic-references>
      <description>
        <p>Retrieves one value tagged with <arg name="key" />.  It is an error if the key is not present in <arg name="container" />.</p>
      </description>
      <see-also>
        <value name="tag" /> <value name="findall" />
      </see-also>
    </case>
    <case>
      <arguments>
        <arg identifier="container">
          <type><named-type name="Group3D" /></type>
        </arg>
        <arg identifier="key">
          <type><named-type name="Symbol" /></type>
        </arg>
      </arguments>
      <result>
        <type><named-type name="Value" /></type>
      </result>
      <dynamic-references></dynamic-references>
      <description>
        <p>See the <str-2D /> case.</p>
      </description>
    </case>
  </function>
</system-binding>

<system-binding name="findall">
  <function>
    <case>
      <arguments>
        <arg identifier="container">
          <type><named-type name="Group" /></type>
        </arg>
        <arg identifier="key">
          <type><named-type name="Symbol" /></type>
        </arg>
      </arguments>
      <result>
        <type><named-type name="Array" /></type>
      </result>
      <dynamic-references></dynamic-references>
      <description>
        <p>Retrieves all values tagged with <arg name="key" />.</p>
      </description>
      <see-also>
        <value name="tag" /> <value name="find" />
      </see-also>
    </case>
    <case>
      <arguments>
        <arg identifier="container">
          <type><named-type name="Group3D" /></type>
        </arg>
        <arg identifier="key">
          <type><named-type name="Symbol" /></type>
        </arg>
      </arguments>
      <result>
        <type><named-type name="Array" /></type>
      </result>
      <dynamic-references></dynamic-references>
      <description>
        <p>See the <str-2D /> case.</p>
      </description>
    </case>
  </function>
</system-binding>

  </body>

</book>
