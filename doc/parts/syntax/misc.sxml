<!-- This file is part of Shapes.                                           -->
<!--                                                                        -->
<!-- Shapes is free software: you can redistribute it and/or modify         -->
<!-- it under the terms of the GNU General Public License as published by   -->
<!-- the Free Software Foundation, either version 3 of the License, or      -->
<!-- any later version.                                                     -->
<!--                                                                        -->
<!-- Shapes is distributed in the hope that it will be useful,              -->
<!-- but WITHOUT ANY WARRANTY; without even the implied warranty of         -->
<!-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          -->
<!-- GNU General Public License for more details.                           -->
<!--                                                                        -->
<!-- You should have received a copy of the GNU General Public License      -->
<!-- along with Shapes.  If not, see <http://www.gnu.org/licenses/>.        -->
<!--                                                                        -->
<!-- Copyright 2008, 2009, 2010, 2013, 2015 Henrik Tidefelt                 -->

<section id="syntax/misc-expr">
<title>Other expressions and expressions in general</title>
<top>
<p>There are many kinds of expressions.  An expression can be either <em>non-pure</em> or <em>pure</em> depending on whether it interacts with states or not.  An expression may also be <em>immediate</em>, either because it is of a certain kind which is always immediate, or because the user has flagged it to be immediate for some reason.  Expressions that are non-pure or immediate cannot or must not be delayed, and are evaluated in the second phase of code bracket evaluation.  While non-purity is a property that a child can (and generally does) give to its parent in the abstract syntax tree, the immediate flag is never transferred between child and parent.</p>

<p>The following breaks down <syntax name="expr" />:</p>
<syntax-table>
<tr> <td><syntax name="expr" class="new" /></td> <td><bnf>→</bnf></td> <td>( <syntax name="expr" /> )</td></tr>
<tr> <td></td> <td><bnf>|</bnf></td> <td><syntax name="constant" /> <bnf>|</bnf> <syntax name="reference" /></td></tr>
<tr> <td></td> <td><bnf>|</bnf></td> <td><syntax name="tuple" /> <bnf>|</bnf> <syntax name="structure" /></td></tr>
<tr> <td></td> <td><bnf>|</bnf></td> <td><syntax name="function" /> <bnf>|</bnf> <syntax name="application" /></td></tr>
<tr> <td></td> <td><bnf>|</bnf></td> <td><syntax name="reach-out" /> <bnf>|</bnf> <syntax name="immediate-expr" /></td></tr>
<tr> <td></td> <td><bnf>|</bnf></td> <td><syntax name="dynamic-binding" /> <bnf>|</bnf> <syntax name="with-dynamic" /></td></tr>
<tr> <td></td> <td><bnf>|</bnf></td> <td><syntax name="with-escape-continuation" /> <bnf>|</bnf> <syntax name="invoke-escape-continuation" /></td></tr>
<tr> <td></td> <td><bnf>|</bnf></td> <td><syntax name="last-expr" /></td></tr>
<tr> <td></td> <td><bnf>|</bnf></td> <td><syntax name="unary" /> <bnf>|</bnf> <syntax name="binary-arithmetic" /> <bnf>|</bnf> <syntax name="binary-relational" /></td></tr>
<tr> <td></td> <td><bnf>|</bnf></td> <td><syntax name="code-bracket" /> <bnf>|</bnf> <syntax name="unnamed-state-expr" /></td></tr>

<tr> <td><syntax name="reference" class="new" /></td> <td><bnf>→</bnf></td> <td> <syntax name="identifier" /> <bnf>|</bnf> <syntax name="dyn-var" /> <bnf>|</bnf> <syntax name="peek-state" /></td></tr>
<tr> <td></td> <td><bnf>|</bnf></td> <td><syntax name="field-ref" /> <bnf>|</bnf> <syntax name="lookup-symbol" /> <bnf>|</bnf> <syntax name="typename" /></td></tr>

<tr> <td><syntax name="application" class="new" /></td> <td><bnf>→</bnf></td> <td> <syntax name="basic-call" /> <bnf>|</bnf> <syntax name="mutator-call" /> <bnf>|</bnf> <syntax name="unary-call" /> <bnf>|</bnf> <syntax name="split-call" /></td></tr>
<tr> <td></td> <td><bnf>|</bnf></td> <td> <syntax name="basic-cute" /> <bnf>|</bnf> <syntax name="unary-cute" /> <bnf>|</bnf> <syntax name="split-cute" /></td></tr>
<tr> <td></td> <td><bnf>|</bnf></td> <td> <lexerregexp>"("</lexerregexp> <syntax name="mutator-caller" /> <lexerregexp>")"</lexerregexp></td></tr>

<tr> <td><syntax name="constant" class="new" /></td> <td><bnf>→</bnf></td> <td><syntax name="float" /> <bnf>|</bnf> <syntax name="length" /> <bnf>|</bnf> <syntax name="string" /> <bnf>|</bnf> <syntax name="boolean" /> <bnf>|</bnf> <syntax name="integer" /> <bnf>|</bnf> <syntax name="character" />  <bnf>|</bnf> <syntax name="symbol" /></td></tr>
<tr> <td></td> <td><bnf>|</bnf></td> <td><syntax name="source-reflection" /></td></tr>

<tr> <td><syntax name="tuple" class="new" /></td> <td><bnf>→</bnf></td> <td><syntax name="float-pair" /> <bnf>|</bnf> <syntax name="float-triple" /> <bnf>|</bnf> <syntax name="coords-2D" /> <bnf>|</bnf> <syntax name="coords-3D" /></td></tr>
<tr> <td></td> <td><bnf>|</bnf></td> <td><syntax name="polar-handle-2D" /> <bnf>|</bnf> <syntax name="corner-point-2D" /></td></tr>

<tr> <td><syntax name="unary" class="new" /></td> <td><bnf>→</bnf></td> <td><syntax name="not-expr" /> <bnf>|</bnf> <syntax name="cycle" /> <bnf>|</bnf> <syntax name="abs-call" /></td></tr>
<tr> <td></td> <td><bnf>|</bnf></td> <td><syntax name="negation" /> <bnf>|</bnf> <syntax name="relative" /></td></tr>

<tr> <td><syntax name="binary-arithmetic" class="new" /></td> <td><bnf>→</bnf></td> <td><syntax name="plus" /> <bnf>|</bnf> <syntax name="minus" /> <bnf>|</bnf> <syntax name="star" /> <bnf>|</bnf> <syntax name="slash" /></td></tr>
<tr> <td></td> <td><bnf>|</bnf></td> <td><syntax name="plus-plus" /> <bnf>|</bnf> <syntax name="minus-minus" /></td></tr>
<tr> <td></td> <td><bnf>|</bnf></td> <td><syntax name="angle" /> <bnf>|</bnf> <syntax name="projection" /></td></tr>
<tr> <td></td> <td><bnf>|</bnf></td> <td><syntax name="ampersand" /> <bnf>|</bnf> <syntax name="compose" /> <bnf>|</bnf> <syntax name="fcons" /></td></tr>

<tr> <td><syntax name="binary-relational" class="new" /></td> <td><bnf>→</bnf></td> <td><syntax name="less" /> <bnf>|</bnf> <syntax name="less-equal" /> <bnf>|</bnf> <syntax name="greater" /> <bnf>|</bnf> <syntax name="greater-equal" /></td></tr>
<tr> <td></td> <td><bnf>|</bnf></td> <td><syntax name="equal" /> <bnf>|</bnf> <syntax name="not-equal" /></td></tr>
<tr> <td></td> <td><bnf>|</bnf></td> <td><syntax name="and" /> <bnf>|</bnf> <syntax name="or" /> <bnf>|</bnf> <syntax name="xor" /></td></tr>
</syntax-table>
</top>

<section id="syntax/misc-expr/precedence">
<title>Operator precedence and associativity</title>
<body>
<p>This section just gives the operator precedence and associativity.  It is just a polished version of the corresponding segment of the Bison parser input.</p>
<p>In the table below, the operators have higher precedence (binds stronger) towards the bottom, and the precedence is equal in each row.  Each regexp corresponds to one operator, so the rexexps with alternatives typically give an ASCII fallback for a pretty non-ASCII character.  The table may contain unused operators.</p>
<loose-table>
	<head>
		<tr> <td>Associativity</td> <td>Operators</td></tr>
	</head>
	<body>
		<tr> <td>non</td> <td><lexerregexp>:</lexerregexp></td> </tr>

		<tr> <td>left</td> <td><lexerregexp>≪|"&lt;&lt;"</lexerregexp></td> </tr>
		<tr> <td>non</td> <td><lexerregexp>!!</lexerregexp></td> </tr>

		<tr> <td>non</td> <td><lexerregexp>→|"-&gt;"</lexerregexp></td> </tr>
		<tr> <td>right</td> <td><lexerregexp>"|"</lexerregexp></td> </tr>
		<tr> <td>left</td> <td><lexerregexp>"[]"</lexerregexp>   <lexerregexp>"[!]"</lexerregexp>   <lexerregexp>"[...]"</lexerregexp>   <lexerregexp>"[!...]"</lexerregexp>   <lexerregexp>≫|"&gt;&gt;"</lexerregexp></td> </tr>

		<tr> <td>right</td> <td><lexerregexp>;</lexerregexp></td> </tr>
		<tr> <td>left</td> <td><lexerregexp>"&amp;|"</lexerregexp></td> </tr>
		<tr> <td>left</td> <td><lexerregexp>&amp;</lexerregexp></td> </tr>
		<tr> <td>non</td> <td><lexerregexp>:</lexerregexp> <em>(dynamic variable binding)</em></td> </tr>

		<tr> <td>left</td> <td><lexerregexp>⋁|"or"</lexerregexp></td> </tr>
		<tr> <td>non</td> <td><lexerregexp>⊻|"xor"</lexerregexp></td> </tr>
		<tr> <td>left</td> <td><lexerregexp>⋀|"and"</lexerregexp></td> </tr>
		<tr> <td>left</td> <td><lexerregexp>¬|"not"</lexerregexp></td> </tr>
		<tr> <td>non</td> <td><lexerregexp>=</lexerregexp>   <lexerregexp>≠|"/="</lexerregexp>   <lexerregexp>≤|"&lt;="</lexerregexp>   <lexerregexp>≥|"&gt;="</lexerregexp></td> </tr>

		<tr> <td>left</td> <td><lexerregexp>"++"</lexerregexp>   <lexerregexp>--</lexerregexp></td> </tr>
		<tr> <td>left</td> <td><lexerregexp>&lt;</lexerregexp>   <lexerregexp>&gt;</lexerregexp></td> </tr>

		<tr> <td>left</td> <td><lexerregexp>"+"</lexerregexp>   <lexerregexp>-</lexerregexp></td> </tr>
		<tr> <td>non</td> <td><lexerregexp>∠|"/_"</lexerregexp></td> </tr>
		<tr> <td>left</td> <td><lexerregexp>"*"</lexerregexp>   <lexerregexp>/</lexerregexp>   <lexerregexp>∥|"*/"</lexerregexp></td> </tr>
		<tr> <td>left</td> <td><lexerregexp>~</lexerregexp></td> </tr>

		<tr> <td>left</td> <td><lexerregexp>⊙|"()"</lexerregexp></td> </tr>

		<tr> <td>left</td> <td><lexerregexp>"."</lexerregexp></td> </tr>
		<tr> <td>left</td> <td><lexerregexp>#</lexerregexp></td> </tr>
		<tr> <td>left</td> <td><lexerregexp>&lt;&gt;</lexerregexp></td> </tr>
		<tr> <td>left</td> <td><lexerregexp>@@</lexerregexp>   <lexerregexp>"../"</lexerregexp></td> </tr>

		<tr> <td>left</td> <td><lexerregexp>,</lexerregexp></td> </tr>
	</body>
</loose-table>
<p>The higher precedence of <operator name=":" /> is additionally used in for the rule
	<syntax-table>
		<tr> <td><syntax name="unary-cute" /></td> <td><bnf>→</bnf></td> <td><bnf>&lt;</bnf><named-type name="Function" /><bnf>&gt;</bnf> <lexerregexp>"[...]"</lexerregexp> <syntax name="named-expr" /></td></tr>
	</syntax-table>
	where it appears as part of <syntax name="named-expr" />.</p>

<p>The precedence and associativity of <operator name="&lt;" /> and <operator name="&gt;" /> is non-standard, but is necessary to allow for convenient path construction.</p>
</body>
</section>

<section id="syntax/misc-expr/path-constr">
<title>Path construction</title>
<body>
<p>In the end, all paths are piecewise cubic splines.  Parameterizing the splines as Bezier splines, we refer to <em>interpolation points</em> instead of <em>spline coefficients</em>.  In general, the spline has two <em>endpoints</em>, and two <em>intermediate</em> interpolation points.  The two end points may be referred to as the <em>first endpoint</em> and <em>second endpoint</em>, respectively.  The intermediate interpolation points may be referred to as the <em>first intermediate</em> and <em>second intermediate</em> interpolation point, respectively.  The intermediate interpolation points may be omitted as a shorthand for placing them at the neighboring endpoint.</p>

<p>The Bezier spline will pass through the enpoints, and be contained in the convex hull of all four interpolation points.  Adjacent splines share one of the endpoints, so that the path becomes continuous.  This makes it convenient to think of the path, not as a sequence of Bezier splines, but as a sequence of points on the path, where each point on the path may have a forward and a backward control point (or <em>handle</em>).  Please refer to the WWW for more details on the splines, for instance <a href="http://en.wikipedia.org/wiki/B%C3%A9zier_curve">Wikipedia</a>.  Here, we shall focus on how the handles may be laid out.</p>

<p>
	The syntax that constructs path points with handles is:
	<syntax-table>
		<tr> <td><syntax name="path-point-2D" /></td> <td><bnf>::</bnf></td> <td><named-type name="PathPoint2D" /></td></tr>
		<tr> <td><syntax name="path-point-2D" class="new" /></td> <td><bnf>→</bnf></td> <td><bnf>&lt;</bnf><named-type name="Coords2D" /><bnf>&gt;</bnf></td></tr>
		<tr> <td></td> <td><bnf>|</bnf></td> <td><bnf>&lt;</bnf><named-type name="Handle2D" /><bnf>&gt;</bnf> <lexerregexp>&lt;</lexerregexp> <bnf>&lt;</bnf><named-type name="PathPoint2D" /><bnf>&gt;</bnf></td></tr>
		<tr> <td></td> <td><bnf>|</bnf></td> <td><bnf>&lt;</bnf><named-type name="PathPoint2D" /><bnf>&gt;</bnf> <lexerregexp>&gt;</lexerregexp> <bnf>&lt;</bnf><named-type name="Handle2D" /><bnf>&gt;</bnf></td></tr>
		<tr> <td><named-type name="Handle2D" /></td> <td><bnf>=</bnf></td> <td><named-type name="Coords2D" /> <bnf>|</bnf> <named-type name="PolarHandle2D" /></td></tr>
	</syntax-table>
	It is illegal to put a handle on a pathpoint which already has a handle in the same direction.
</p>

<p>
	Often, paths are constructed according to the pattern
<pre>
<syntax name="path-point-2D" /> <bnf>(</bnf> <lexerregexp>--</lexerregexp> <syntax name="path-point-2D" /> <bnf>)*</bnf> <bnf>(</bnf> <lexerregexp>--</lexerregexp> <lexerregexp>cycle</lexerregexp> <bnf>)?</bnf>
</pre>
	but this is just a special case of the general
</p>

<syntax-table>
<tr> <td><syntax name="simple-path-2D" /></td> <td><bnf>::</bnf></td> <td><named-type name="SimplePath2D" /></td></tr>
<tr> <td><syntax name="simple-path-2D" class="new" /></td> <td><bnf>→</bnf></td> <td><bnf>&lt;</bnf><named-type name="PathPoint2D" /><bnf>&gt;</bnf></td></tr>
<tr> <td></td> <td><bnf>|</bnf></td> <td><bnf>&lt;</bnf><named-type name="SimplePath2D" /><bnf>&gt;</bnf> <lexerregexp>--</lexerregexp> <bnf>&lt;</bnf><named-type name="SimplePath2D" /><bnf>&gt;</bnf></td></tr>
<tr> <td></td> <td><bnf>|</bnf></td> <td><syntax name="cycle" /></td></tr>
<tr> <td><syntax name="cycle" class="new" /></td> <td><bnf>→</bnf></td> <td><bnf>&lt;</bnf><named-type name="SimplePath2D" /><bnf>&gt;</bnf> <lexerregexp>--</lexerregexp> <lexerregexp>cycle</lexerregexp></td></tr>
</syntax-table>
<p>It is illegal to close a path that is already closed, or to extend a closed path.</p>

<p>What remains to be described regarding simple paths is how the free parts of polar handles are determined.  When all handles have been determined, an <em>elementary</em> path has been obtained, but this concept is insignificant from the user's perspective.  Anyway, most of the computations on paths require an elementary path, and will trigger the following compuation if needed:</p>
<ol>
<li><p>Find all angles to handles that are determined explicitly.</p></li>
<li><p>Propagate all known angles to any free angles on the other side of a path point (taking corner angles into account).</p></li>
<li><p>Compute remaining angles based on the path point's position relative its neighboring path points.</p><p class="todo">Fill in details!</p></li>
<li><p>Compute all distances to handles that are given explicitly.  Note that this requires all angles to be known.</p></li>
<li><p>Propagate known distances to any free distances on the other side of a path point.</p></li>
<li><p>Use <dynamic namespace="..Shapes..Geometry" name="defaultunit" /> to find one value per remaining free distance.  At path points where only one such value is computed, use it as it is.  At path points where two such values are computed, use the smalles of these values on both sides.</p></li>
</ol>
<p>
	Note that the rules above means that there is a subtle difference between the following two path points:
<pre>
<![CDATA[p1: (1%C^)<(0cm,0cm)>(1%C^)
p2: @defaultunit:1%C | (^)<(0cm,0cm)>(^)]]>
</pre>
</p>

<note>
	<p>The difference is that <user-value name="p1" />  will generally get handles of unequal length, while <user-value name="p2" /> gets handles of equal length.</p>
</note>

<example-with-output title="Example" internal-id="example:pathconstruction">
<image pdf="features/pathconstruction.pdf" jpg="features/pathconstruction_y_small.jpg" />
<source file="features/pathconstruction.shape">
<![CDATA[<!--#include depth="0" virtual="$(BUILDDIR)$(EXAMPLES)features/pathconstruction.shape" -->]]>
</source>
</example-with-output>

<p>When it comes to filling paths with color, it becomes necessary to handle collections of simple paths.  Sometimes, composite paths may also serve merely as a container for simple paths.  The ampersand operator is used to construct composite paths:</p>
<syntax-table>
<tr> <td><syntax name="composite-path-2D" /></td> <td><bnf>::</bnf></td> <td><named-type name="Path2D" /></td></tr>
<tr> <td><syntax name="composite-path-2D" class="new" /></td> <td><bnf>→</bnf></td> <td><bnf>&lt;</bnf><named-type name="SimplePath2D" /><bnf>&gt;</bnf></td></tr>
<tr> <td></td> <td><bnf>|</bnf></td> <td><bnf>&lt;</bnf><named-type name="CompositePath2D" /><bnf>&gt;</bnf> <lexerregexp>&amp;</lexerregexp> <bnf>&lt;</bnf><named-type name="CompositePath2D" /><bnf>&gt;</bnf></td></tr>
</syntax-table>

<p>Paths in <str-3D /> are constructed in the same way as in <str-2D />, although their path points cannot have polar handles.</p>

</body>
</section>

<section id="syntax/misc-expr/laziness">
<title>Laziness control</title>
<body>
<p>While <str-Shapes /> is designed to combine functional programming and lazy evaluation with the conveniences of expression states and escape continuations, the interplay between lazy evaluation and the latter conveniences is not always transparent.  Clearly, <em>order of evaluation</em> is important for expressions with free states.  The evaluation of <syntax name="code-bracket" /> is defined so that expressions with free states are evaluated in order of appearance.  On the contrary, expressions with free states being passed as arguments in a function application will generally be delayed, and the user needs a means to prevent this.</p>
<p>A more delicate issue with delayed evaluation are the expressions which affect the program flow.  Some functions, such as <value namespace="..Shapes" name="error" /> or any function which unconditionally calls <value namespace="..Shapes" name="error" />, will cause the program to stop with an error message, and it is generally not the user's intention that the evaluation of calls to such functions be delayed.  The <str-Shapes /> language makes no distinction between such functions and other functions, so the compiler cannot conclude automatically that evaluation is not to be delayed.  Similarly, evaluation of <syntax name="invoke-escape-continuation" /> or calling function which unconditionally invokes a continuation, will cause a jump in the program flow and are rarely meant to be delayed.  Again, the compiler has no clue, and in both cases the user needs a means to demand immediate evaluation.</p>
<p>Further, prohibiting delayed evaluation may be important for efficiency reasons, and finally immediate evaluation is required to be made explicit under some circumstances.</p>
<p>Delayed evaluation is prohibited by flagging an expression as <em>immediate</em>:</p>
<syntax-table>
<tr> <td><syntax name="immediate-expr" class="new" /></td> <td><bnf>→</bnf></td> <td><lexerregexp>!</lexerregexp> <syntax name="expr" /></td></tr>
</syntax-table>

<example-with-output title="Example" internal-id="example:cond-impl">
<source file="features/cond-impl.blank">
<![CDATA[<!--#include depth="0" virtual="$(BUILDDIR)$(EXAMPLES)features/cond-impl.blank" -->]]>
</source>
<stdout>
<![CDATA[<!--#include depth="0" virtual="$(BUILDDIR)$(EXAMPLES)features/cond-impl.stdout" -->]]>
</stdout>
<caption>
	<p>Two implementations of the <inline>cond</inline> construct of Scheme as a function in <str-Shapes />.</p>
	<p>Since <value namespace="..Shapes..Data" name="cons" /> delays its arguments, it can be used to group a predicate with its consequence syntactically, still allowing us to evaluate the predicate by itself.  All predicate-consequence pairs (which shall be <named-type name="ConsPair" />) are received in a sink, which is turned into a list so that a fold can be used to scan the arguments from left to right.</p>
	<p>In the first implementation, called just <inline>cond</inline>, an escape continuation is used to break the fold when a pair with true predicate is found, and if the fold completes, the next expression signals an error.  Note that the fold expression has no free states, so its evaluation must be made immediate explicitly.  Not making immediate evaluation explicit is an error since it is generally hard to detect that the expression is meaningful to evaluate.  Forcing immediate evaluation of the following call to <value namespace="..Shapes" name="error" /> is not necessary since it will be forced anyway by the continuation taking the result of the code bracket.</p>
	<p>The second implementation shows that the same functionality can be achieved without the use of escape continuations, but will be less efficient when there are many cases following the first true case.</p>
</caption>
</example-with-output>

</body>
</section>

<section id="syntax/misc-expr/fcons">
<title>List construction</title>
<body>
<p>Singly linked lists are frequently used in <str-Shapes />, as they are the fundamental container of the functional paradigm.  They can be constructed using functions such as <value namespace="..Shapes..Data" name="list" />, <value namespace="..Shapes..Data" name="cons" />, and <value namespace="..Shapes..Data" name="fcons" />.  The latter, being the more efficient way of prepending an element to a list, has an infix notation.</p>
<syntax-table>
<tr> <td><syntax name="fcons" class="new" /></td> <td><bnf>::</bnf></td> <td><named-type name="Seq" /></td></tr>
<tr> <td><syntax name="fcons" class="new" /></td> <td><bnf>→</bnf></td> <td><syntax name="expr" /> <lexerregexp>;</lexerregexp> <bnf>&lt;</bnf><named-type name="Seq" /><bnf>&gt;</bnf></td></tr>
</syntax-table>
<p>
	Here are some of several ways to construct singly linked lists:
<pre><![CDATA[
lst1: [list 1 2 3]                   |** Efficient construction, efficient representation.
lst2: 1 ; 2 ; 3 ; nil                |** Step by step construction, efficient representation.
lst3: [cons 1 [cons 2 [cons 3 lin]]] |** Lazy construction, inefficient representation.
]]></pre>
</p>
</body>
</section>

<section id="syntax/misc-expr/unary">
<title>Unary operators</title>
<body>
<p>The expansions of <syntax name="unary" /> which have not been described elsewhere are given here.  Note that the unary plus and minus signs are completely unrelated operations.  The special syntax for calling the <value namespace="..Shapes..Numeric..Math" name="abs" /> function is also included here.</p>
<syntax-table>
<tr> <td><syntax name="negation" class="new" /></td> <td><bnf>→</bnf></td> <td><lexerregexp>"("</lexerregexp> <lexerregexp>-</lexerregexp> <syntax name="expr" /> <lexerregexp>")"</lexerregexp></td></tr>
<tr> <td></td> <td><bnf>|</bnf></td> <td><lexerregexp>~</lexerregexp> <syntax name="expr" /></td></tr>
<tr> <td><syntax name="relative" class="new" /></td> <td><bnf>→</bnf></td> <td><lexerregexp>"("</lexerregexp> <lexerregexp>"+"</lexerregexp> <syntax name="expr" /> <lexerregexp>")"</lexerregexp></td></tr>
<tr> <td><syntax name="not-expr" class="new" /></td> <td><bnf>→</bnf></td> <td><lexerregexp>not</lexerregexp> <bnf>&lt;</bnf><named-type name="Boolean" /><bnf>&gt;</bnf></td></tr>
<tr> <td><syntax name="abs-call" class="new" /></td> <td><bnf>→</bnf></td> <td><lexerregexp>"(|"</lexerregexp> <syntax name="expr" /> <lexerregexp>"|)"</lexerregexp></td></tr>
</syntax-table>
</body>
</section>

<section id="syntax/misc-expr/binary">
<title>Binary arithmetic operators</title>
<body>
	<p><em>This section remains to be written.  Please refer to the operator listings for any particular type.</em></p>
</body>
</section>

<section id="syntax/misc-expr/relational">
<title>Relational operators</title>
<body>
	<p><em>This section remains to be written.  Please refer to the operator listings for any particular type.</em></p>
</body>
</section>

</section><!-- end of syntax/misc-expr -->
