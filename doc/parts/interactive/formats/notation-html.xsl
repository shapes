<?xml version="1.0" encoding="UTF-8"?>

<!-- This file is part of Shapes.                                           -->
<!--                                                                        -->
<!-- Shapes is free software: you can redistribute it and/or modify         -->
<!-- it under the terms of the GNU General Public License as published by   -->
<!-- the Free Software Foundation, either version 3 of the License, or      -->
<!-- any later version.                                                     -->
<!--                                                                        -->
<!-- Shapes is distributed in the hope that it will be useful,              -->
<!-- but WITHOUT ANY WARRANTY; without even the implied warranty of         -->
<!-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          -->
<!-- GNU General Public License for more details.                           -->
<!--                                                                        -->
<!-- You should have received a copy of the GNU General Public License      -->
<!-- along with Shapes.  If not, see <http://www.gnu.org/licenses/>.        -->
<!--                                                                        -->
<!-- Copyright 2009 Henrik Tidefelt                                         -->

<xsl:stylesheet
  version="2.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns="http://www.w3.org/1999/xhtml"
>

<xsl:template match="db-state">
	<xsl:element name="a">
		<xsl:attribute name="class">discrete</xsl:attribute>
		<xsl:attribute name="href">
			<xsl:call-template name="part-to-href">
				<xsl:with-param name="name">interactive</xsl:with-param>
			</xsl:call-template>
			<xsl:text>#</xsl:text>
			<xsl:call-template name="id-to-anchor-name"><xsl:with-param name="id">interactive/debug</xsl:with-param></xsl:call-template>
		</xsl:attribute>
		<xsl:call-template name="format-state-name">
			<xsl:with-param name="name">db</xsl:with-param>
		</xsl:call-template>
	</xsl:element>
</xsl:template>

</xsl:stylesheet>

