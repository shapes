<?xml version="1.0" encoding="UTF-8"?>

<!-- This file is part of Shapes.                                           -->
<!--                                                                        -->
<!-- Shapes is free software: you can redistribute it and/or modify         -->
<!-- it under the terms of the GNU General Public License as published by   -->
<!-- the Free Software Foundation, either version 3 of the License, or      -->
<!-- any later version.                                                     -->
<!--                                                                        -->
<!-- Shapes is distributed in the hope that it will be useful,              -->
<!-- but WITHOUT ANY WARRANTY; without even the implied warranty of         -->
<!-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          -->
<!-- GNU General Public License for more details.                           -->
<!--                                                                        -->
<!-- You should have received a copy of the GNU General Public License      -->
<!-- along with Shapes.  If not, see <http://www.gnu.org/licenses/>.        -->
<!--                                                                        -->
<!-- Copyright 2008, 2009, 2014 Henrik Tidefelt                             -->

<xsl:stylesheet
  version="2.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns="http://www.w3.org/1999/xhtml"
>
<xsl:output method="xml" indent="no"
 doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
 doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
/>

<xsl:include href="../../formats/html.xsl" />
<xsl:include href="../../formats/bindings-html.xsl" />

<xsl:template match="/book">
  <html>
    <head>
      <title><xsl:apply-templates select="title" /></title>
			<xsl:element name="link">
				<xsl:attribute name="rel">stylesheet</xsl:attribute>
				<xsl:attribute name="href"><xsl:value-of select="/book/base/@href" />shapes.css</xsl:attribute>
			</xsl:element>
    </head>
    <body>
			<xsl:call-template name="head-navigation" />
			<h2><xsl:apply-templates select="title" /></h2>
			<hr class="thick"/>
			<xsl:apply-templates select="top" />
			<div class="p">
				<b>Algorithms:</b>
				<xsl:for-each select="section">
					  
					<xsl:element name="a">
						<xsl:attribute name="href">#<xsl:call-template name="id-to-anchor-name"><xsl:with-param name="id"><xsl:value-of select="@id" /></xsl:with-param></xsl:call-template></xsl:attribute>
						<xsl:apply-templates select="title" />
					</xsl:element>
				</xsl:for-each>
			</div>

			<hr class="thin"/>
			<div class="p" style="text-align:center;"><b>Tolerance parameters</b></div>
			<xsl:for-each select="/book/tolerance-parameter[@name]">
				<xsl:sort select="@name" />
				<h5>
					<xsl:element name="a">
						<xsl:attribute name="name">tol-<xsl:value-of select="@name" /></xsl:attribute>
						<span class="tolparam"><xsl:value-of select="@name" /></span>
					</xsl:element>
					<xsl:text> : </xsl:text><xsl:apply-templates select="default" /><xsl:text> :: </xsl:text><xsl:apply-templates select="type" />
				</h5>
				<xsl:apply-templates select="description" />
			</xsl:for-each>
			<hr class="thin"/>

			<xsl:for-each select="section">
				<h3>
					<xsl:element name="a">
						<xsl:attribute name="name"><xsl:call-template name="id-to-anchor-name"><xsl:with-param name="id"><xsl:value-of select="@id" /></xsl:with-param></xsl:call-template></xsl:attribute>
						<xsl:apply-templates select="title" />
					</xsl:element>
				</h3>
				<xsl:apply-templates select="top" />
				<xsl:apply-templates select="body" />
				<xsl:for-each select="section">
					<h4>
						<xsl:element name="a">
							<xsl:attribute name="name"><xsl:call-template name="id-to-anchor-name"><xsl:with-param name="id"><xsl:value-of select="@id" /></xsl:with-param></xsl:call-template></xsl:attribute>
							<xsl:apply-templates select="title" />
						</xsl:element>
					</h4>
					<xsl:apply-templates select="top" />
					<xsl:apply-templates select="body" />
					<xsl:for-each select="section">
						<h5>
							<xsl:element name="a">
								<xsl:attribute name="name"><xsl:call-template name="id-to-anchor-name"><xsl:with-param name="id"><xsl:value-of select="@id" /></xsl:with-param></xsl:call-template></xsl:attribute>
								<xsl:apply-templates select="title" />
							</xsl:element>
						</h5>
						<xsl:apply-templates select="top" />
						<xsl:apply-templates select="body" />
					</xsl:for-each>
				</xsl:for-each>
			</xsl:for-each>
			<xsl:call-template name="SF-placeholder" />
		</body>
  </html>
</xsl:template>

</xsl:stylesheet>
