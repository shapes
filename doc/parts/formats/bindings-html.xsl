<?xml version="1.0" encoding="UTF-8"?>

<!-- This file is part of Shapes.                                           -->
<!--                                                                        -->
<!-- Shapes is free software: you can redistribute it and/or modify         -->
<!-- it under the terms of the GNU General Public License as published by   -->
<!-- the Free Software Foundation, either version 3 of the License, or      -->
<!-- any later version.                                                     -->
<!--                                                                        -->
<!-- Shapes is distributed in the hope that it will be useful,              -->
<!-- but WITHOUT ANY WARRANTY; without even the implied warranty of         -->
<!-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          -->
<!-- GNU General Public License for more details.                           -->
<!--                                                                        -->
<!-- You should have received a copy of the GNU General Public License      -->
<!-- along with Shapes.  If not, see <http://www.gnu.org/licenses/>.        -->
<!--                                                                        -->
<!-- Copyright 2015 Henrik Tidefelt                                         -->

<xsl:stylesheet
  version="2.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns="http://www.w3.org/1999/xhtml"
>

<xsl:template name="name-to-a-id">
  <xsl:param name="name" />
  <xsl:value-of select="replace(replace($name, '-', '--'), '\?', '-_')" />
</xsl:template>

<xsl:template name="namespace-to-a-id">
  <xsl:param name="name" />
  <xsl:value-of select="replace(replace($name, '^\.\.', ''), '\.\.', '-')" />
</xsl:template>
<xsl:template name="namespace-to-href">
  <xsl:param name="name" />
  <xsl:value-of select="/book/base/@href" /><xsl:text>namespace/</xsl:text><xsl:value-of select="replace(replace($name, '^\.\.', ''), '\.\.', '-')" /><xsl:text>.html</xsl:text>
</xsl:template>

<xsl:template name="format-value-name">
  <xsl:param name="namespace" />
  <xsl:param name="name" />
  <span class="varname">
    <xsl:if test="$namespace != ''">
      <xsl:value-of select="$namespace" /><xsl:text>..</xsl:text>
    </xsl:if>
    <xsl:value-of select="$name" />
  </span>
</xsl:template>
<xsl:template name="format-dynamic-name">
  <xsl:param name="namespace" />
  <xsl:param name="name" />
  <span class="varname">
    <xsl:if test="$namespace != ''">
      <xsl:value-of select="$namespace" /><xsl:text>..</xsl:text>
    </xsl:if>
    <xsl:text>@</xsl:text>
    <xsl:value-of select="$name" />
  </span>
</xsl:template>
<xsl:template name="format-state-name">
  <xsl:param name="namespace" />
  <xsl:param name="name" />
  <span class="varname">
    <xsl:if test="$namespace != ''">
      <xsl:value-of select="$namespace" /><xsl:text>..</xsl:text>
    </xsl:if>
    <xsl:text>•</xsl:text>
    <xsl:value-of select="$name" />
  </span>
</xsl:template>
<xsl:template name="format-escape-name">
  <xsl:param name="namespace" />
  <xsl:param name="name" />
  <span class="varname">
    <xsl:if test="$namespace != ''">
      <xsl:value-of select="$namespace" /><xsl:text>..</xsl:text>
    </xsl:if>
    <b>C—</b>
    <xsl:value-of select="$name" />
  </span>
</xsl:template>
<xsl:template name="format-type-name">
  <xsl:param name="namespace" />
  <xsl:param name="name" />
  <span class="typename">
    <xsl:if test="$namespace != ''">
      <xsl:value-of select="$namespace" /><xsl:text>..</xsl:text>
    </xsl:if>
    <xsl:text>§</xsl:text>
    <xsl:value-of select="$name" />
  </span>
</xsl:template>
<xsl:template name="format-statetype-name">
  <xsl:param name="namespace" />
  <xsl:param name="name" />
  <span class="typename">
    <xsl:if test="$namespace != ''">
      <xsl:value-of select="$namespace" /><xsl:text>..</xsl:text>
    </xsl:if>
    <xsl:text>§•</xsl:text>
    <xsl:value-of select="$name" />
  </span>
</xsl:template>

<xsl:template name="format-kind-and-name">
  <xsl:param name="kind" />
  <xsl:param name="namespace" />
  <xsl:param name="name" />
  <xsl:choose>
    <xsl:when test="$kind='value'">
      <xsl:call-template name="format-value-name">
        <xsl:with-param name="namespace"><xsl:value-of select="$namespace" /></xsl:with-param>
        <xsl:with-param name="name"><xsl:value-of select="$name" /></xsl:with-param>
      </xsl:call-template>
    </xsl:when>
    <xsl:when test="$kind='state'">
      <xsl:call-template name="format-state-name">
        <xsl:with-param name="namespace"><xsl:value-of select="$namespace" /></xsl:with-param>
        <xsl:with-param name="name"><xsl:value-of select="$name" /></xsl:with-param>
      </xsl:call-template>
    </xsl:when>
    <xsl:when test="$kind='dynamic'">
      <xsl:call-template name="format-dynamic-name">
        <xsl:with-param name="namespace"><xsl:value-of select="$namespace" /></xsl:with-param>
        <xsl:with-param name="name"><xsl:value-of select="$name" /></xsl:with-param>
      </xsl:call-template>
    </xsl:when>
    <xsl:when test="$kind='escape'">
      <xsl:call-template name="format-escape-name">
        <xsl:with-param name="namespace"><xsl:value-of select="$namespace" /></xsl:with-param>
        <xsl:with-param name="name"><xsl:value-of select="$name" /></xsl:with-param>
      </xsl:call-template>
    </xsl:when>
    <xsl:when test="$kind='type'">
      <xsl:call-template name="format-type-name">
        <xsl:with-param name="namespace"><xsl:value-of select="$namespace" /></xsl:with-param>
        <xsl:with-param name="name"><xsl:value-of select="$name" /></xsl:with-param>
      </xsl:call-template>
    </xsl:when>
    <xsl:when test="$kind='statetype'">
      <xsl:call-template name="format-statetype-name">
        <xsl:with-param name="namespace"><xsl:value-of select="$namespace" /></xsl:with-param>
        <xsl:with-param name="name"><xsl:value-of select="$name" /></xsl:with-param>
      </xsl:call-template>
    </xsl:when>
  </xsl:choose>
</xsl:template>

<xsl:template name="ns-kind-name-to-linked-binding">
  <xsl:param name="current-namespace" />
  <xsl:param name="placed" />
  <xsl:param name="namespace" />
  <xsl:param name="kind" />
  <xsl:param name="name" />
  <xsl:variable name="ns">
    <xsl:choose>
      <xsl:when test="starts-with($namespace, '..')">
        <xsl:value-of select="$namespace" />
      </xsl:when>
      <xsl:when test="$namespace = ''">
        <xsl:value-of select="$current-namespace" />
      </xsl:when>
      <xsl:when test="$current-namespace = '..'">
        <xsl:value-of select="concat('..', $namespace)" />
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="concat($current-namespace, '..', $namespace)" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  <xsl:choose>
    <xsl:when test="/book/external/toc/bindings/binding[@namespace = $ns and @kind = $kind and @name = $name]">
      <xsl:element name="a">
        <xsl:attribute name="class">discrete</xsl:attribute>
        <xsl:attribute name="href">
          <xsl:for-each select="/book/external/toc/bindings/binding[@namespace = $ns and @kind = $kind and @name = $name][1]">
            <xsl:choose>
              <xsl:when test="@extension">
                <xsl:call-template name="extension-to-href">
                  <xsl:with-param name="namespace"><xsl:value-of select="$ns" /></xsl:with-param>
                  <xsl:with-param name="name"><xsl:value-of select="@extension" /></xsl:with-param>
                </xsl:call-template>
              </xsl:when>
              <xsl:otherwise>
                <xsl:call-template name="namespace-to-href">
                  <xsl:with-param name="name"><xsl:value-of select="$ns" /></xsl:with-param>
                </xsl:call-template>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
          <xsl:text>#</xsl:text><xsl:value-of select="$kind" /><xsl:text>:</xsl:text>
          <xsl:call-template name="name-to-a-id">
            <xsl:with-param name="name"><xsl:value-of select="$name" /></xsl:with-param>
          </xsl:call-template>
        </xsl:attribute>
        <xsl:choose>
          <xsl:when test="$placed != ''">
            <xsl:call-template name="format-kind-and-name">
              <xsl:with-param name="kind"><xsl:value-of select="$kind" /></xsl:with-param>
              <xsl:with-param name="name"><xsl:value-of select="$name" /></xsl:with-param>
            </xsl:call-template>
          </xsl:when>
          <xsl:when test="$ns = $current-namespace">
            <xsl:call-template name="format-kind-and-name">
              <xsl:with-param name="kind"><xsl:value-of select="$kind" /></xsl:with-param>
              <xsl:with-param name="name"><xsl:value-of select="$name" /></xsl:with-param>
            </xsl:call-template>
          </xsl:when>
          <xsl:when test="$current-namespace != ''">
            <xsl:variable name="prefix">^<xsl:value-of select="$current-namespace" />\.\.</xsl:variable>
            <xsl:call-template name="format-kind-and-name">
              <xsl:with-param name="kind"><xsl:value-of select="$kind" /></xsl:with-param>
              <xsl:with-param name="name"><xsl:value-of select="$name" /></xsl:with-param>
              <xsl:with-param name="namespace"><xsl:value-of select="replace($ns, $prefix, '')" /></xsl:with-param>
            </xsl:call-template>
          </xsl:when>
          <xsl:otherwise>
            <xsl:call-template name="format-kind-and-name">
              <xsl:with-param name="kind"><xsl:value-of select="$kind" /></xsl:with-param>
              <xsl:with-param name="name"><xsl:value-of select="$name" /></xsl:with-param>
              <xsl:with-param name="namespace"><xsl:value-of select="$ns" /></xsl:with-param>
            </xsl:call-template>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:element>
    </xsl:when>
    <xsl:otherwise>
      <xsl:element name="a">
        <xsl:attribute name="class">discrete</xsl:attribute>
        <xsl:attribute name="href"><xsl:value-of select="/book/base/@href" />toc-miss.html?namespace=<xsl:value-of select="$ns" />&amp;kind=<xsl:value-of select="$kind" />&amp;name=<xsl:value-of select="$name" /></xsl:attribute>
        <span class="broken-link">
          <xsl:call-template name="format-kind-and-name">
            <xsl:with-param name="kind"><xsl:value-of select="$kind" /></xsl:with-param>
            <xsl:with-param name="name"><xsl:value-of select="$name" /></xsl:with-param>
          </xsl:call-template>
        </span>
      </xsl:element>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<!-- Placed bindings may be generated without access to current-environment. -->
<xsl:template mode="placed-binding" match="toc/bindings/binding">
  <xsl:call-template name="ns-kind-name-to-linked-binding">
    <xsl:with-param name="placed">true</xsl:with-param>
    <xsl:with-param name="namespace"><xsl:value-of select="@namespace" /></xsl:with-param>
    <xsl:with-param name="kind"><xsl:value-of select="@kind" /></xsl:with-param>
    <xsl:with-param name="name"><xsl:value-of select="@name" /></xsl:with-param>
  </xsl:call-template>
</xsl:template>

<xsl:template name="name-to-linked-value">
  <xsl:param name="current-namespace" />
  <xsl:param name="placed" />
  <xsl:param name="namespace" />
  <xsl:param name="name" />
  <xsl:call-template name="ns-kind-name-to-linked-binding">
    <xsl:with-param name="kind">value</xsl:with-param>
    <xsl:with-param name="current-namespace"><xsl:value-of select="$current-namespace" /></xsl:with-param>
    <xsl:with-param name="placed"><xsl:value-of select="$placed" /></xsl:with-param>
    <xsl:with-param name="namespace"><xsl:value-of select="$namespace" /></xsl:with-param>
    <xsl:with-param name="name"><xsl:value-of select="$name" /></xsl:with-param>
  </xsl:call-template>
</xsl:template>
<xsl:template name="name-to-linked-dynamic">
  <xsl:param name="current-namespace" />
  <xsl:param name="placed" />
  <xsl:param name="namespace" />
  <xsl:param name="name" />
  <xsl:call-template name="ns-kind-name-to-linked-binding">
    <xsl:with-param name="kind">dynamic</xsl:with-param>
    <xsl:with-param name="current-namespace"><xsl:value-of select="$current-namespace" /></xsl:with-param>
    <xsl:with-param name="placed"><xsl:value-of select="$placed" /></xsl:with-param>
    <xsl:with-param name="namespace"><xsl:value-of select="$namespace" /></xsl:with-param>
    <xsl:with-param name="name"><xsl:value-of select="$name" /></xsl:with-param>
  </xsl:call-template>
</xsl:template>
<xsl:template name="name-to-linked-state">
  <xsl:param name="current-namespace" />
  <xsl:param name="placed" />
  <xsl:param name="namespace" />
  <xsl:param name="name" />
  <xsl:call-template name="ns-kind-name-to-linked-binding">
    <xsl:with-param name="kind">state</xsl:with-param>
    <xsl:with-param name="current-namespace"><xsl:value-of select="$current-namespace" /></xsl:with-param>
    <xsl:with-param name="placed"><xsl:value-of select="$placed" /></xsl:with-param>
    <xsl:with-param name="namespace"><xsl:value-of select="$namespace" /></xsl:with-param>
    <xsl:with-param name="name"><xsl:value-of select="$name" /></xsl:with-param>
  </xsl:call-template>
</xsl:template>
<xsl:template name="name-to-linked-escape">
  <xsl:param name="current-namespace" />
  <xsl:param name="placed" />
  <xsl:param name="namespace" />
  <xsl:param name="name" />
  <xsl:call-template name="ns-kind-name-to-linked-binding">
    <xsl:with-param name="kind">escape</xsl:with-param>
    <xsl:with-param name="current-namespace"><xsl:value-of select="$current-namespace" /></xsl:with-param>
    <xsl:with-param name="placed"><xsl:value-of select="$placed" /></xsl:with-param>
    <xsl:with-param name="namespace"><xsl:value-of select="$namespace" /></xsl:with-param>
    <xsl:with-param name="name"><xsl:value-of select="$name" /></xsl:with-param>
  </xsl:call-template>
</xsl:template>

<xsl:template match="/book[namespace]//value[@name]">
  <xsl:call-template name="name-to-linked-value">
    <xsl:with-param name="current-namespace"><xsl:value-of select="ancestor::book[namespace][1]/namespace" /></xsl:with-param>
    <xsl:with-param name="namespace"><xsl:value-of select="@namespace" /></xsl:with-param>
    <xsl:with-param name="name"><xsl:value-of select="@name" /></xsl:with-param>
  </xsl:call-template>
</xsl:template>
<xsl:template match="/book[not(namespace)]//value[@name]">
  <xsl:call-template name="name-to-linked-value">
    <xsl:with-param name="namespace"><xsl:value-of select="@namespace" /></xsl:with-param>
    <xsl:with-param name="name"><xsl:value-of select="@name" /></xsl:with-param>
  </xsl:call-template>
</xsl:template>
<xsl:template match="placed-value[@name]">
  <xsl:call-template name="name-to-linked-value">
    <xsl:with-param name="placed">true</xsl:with-param>
    <xsl:with-param name="namespace"><xsl:value-of select="@namespace" /></xsl:with-param>
    <xsl:with-param name="name"><xsl:value-of select="@name" /></xsl:with-param>
  </xsl:call-template>
</xsl:template>
<xsl:template match="user-value[@name]">
  <xsl:call-template name="format-value-name">
    <xsl:with-param name="namespace"><xsl:value-of select="@namespace" /></xsl:with-param>
    <xsl:with-param name="name"><xsl:value-of select="@name" /></xsl:with-param>
  </xsl:call-template>
</xsl:template>

<xsl:template match="/book[namespace]//dynamic[@name]">
  <xsl:call-template name="name-to-linked-dynamic">
    <xsl:with-param name="current-namespace"><xsl:value-of select="ancestor::book[namespace][1]/namespace" /></xsl:with-param>
    <xsl:with-param name="namespace"><xsl:value-of select="@namespace" /></xsl:with-param>
    <xsl:with-param name="name"><xsl:value-of select="@name" /></xsl:with-param>
  </xsl:call-template>
</xsl:template>
<xsl:template match="/book[not(namespace)]//dynamic[@name]">
  <xsl:call-template name="name-to-linked-dynamic">
    <xsl:with-param name="namespace"><xsl:value-of select="@namespace" /></xsl:with-param>
    <xsl:with-param name="name"><xsl:value-of select="@name" /></xsl:with-param>
  </xsl:call-template>
</xsl:template>
<xsl:template match="placed-dynamic[@name]">
  <xsl:call-template name="name-to-linked-dynamic">
    <xsl:with-param name="placed">true</xsl:with-param>
    <xsl:with-param name="namespace"><xsl:value-of select="@namespace" /></xsl:with-param>
    <xsl:with-param name="name"><xsl:value-of select="@name" /></xsl:with-param>
  </xsl:call-template>
</xsl:template>
<xsl:template match="user-dynamic[@name]">
  <xsl:call-template name="format-dynamic-name">
    <xsl:with-param name="namespace"><xsl:value-of select="@namespace" /></xsl:with-param>
    <xsl:with-param name="name"><xsl:value-of select="@name" /></xsl:with-param>
  </xsl:call-template>
</xsl:template>

<xsl:template match="/book[namespace]//state[@name]">
  <xsl:call-template name="name-to-linked-state">
    <xsl:with-param name="current-namespace"><xsl:value-of select="ancestor::book[namespace][1]/namespace" /></xsl:with-param>
    <xsl:with-param name="namespace"><xsl:value-of select="@namespace" /></xsl:with-param>
    <xsl:with-param name="name"><xsl:value-of select="@name" /></xsl:with-param>
  </xsl:call-template>
</xsl:template>
<xsl:template match="/book[not(namespace)]//state[@name]">
  <xsl:call-template name="name-to-linked-state">
    <xsl:with-param name="namespace"><xsl:value-of select="@namespace" /></xsl:with-param>
    <xsl:with-param name="name"><xsl:value-of select="@name" /></xsl:with-param>
  </xsl:call-template>
</xsl:template>
<xsl:template match="placed-state[@name]">
  <xsl:call-template name="name-to-linked-state">
    <xsl:with-param name="placed">true</xsl:with-param>
    <xsl:with-param name="namespace"><xsl:value-of select="@namespace" /></xsl:with-param>
    <xsl:with-param name="name"><xsl:value-of select="@name" /></xsl:with-param>
  </xsl:call-template>
</xsl:template>
<xsl:template match="user-state[@name]">
  <xsl:call-template name="format-state-name">
    <xsl:with-param name="namespace"><xsl:value-of select="@namespace" /></xsl:with-param>
    <xsl:with-param name="name"><xsl:value-of select="@name" /></xsl:with-param>
  </xsl:call-template>
</xsl:template>

<xsl:template match="/book[namespace]//escape[@name]">
  <xsl:call-template name="name-to-linked-escape">
    <xsl:with-param name="current-namespace"><xsl:value-of select="ancestor::book[namespace][1]/namespace" /></xsl:with-param>
    <xsl:with-param name="namespace"><xsl:value-of select="@namespace" /></xsl:with-param>
    <xsl:with-param name="name"><xsl:value-of select="@name" /></xsl:with-param>
  </xsl:call-template>
</xsl:template>
<xsl:template match="/book[not(namespace)]//escape[@name]">
  <xsl:call-template name="name-to-linked-escape">
    <xsl:with-param name="namespace"><xsl:value-of select="@namespace" /></xsl:with-param>
    <xsl:with-param name="name"><xsl:value-of select="@name" /></xsl:with-param>
  </xsl:call-template>
</xsl:template>
<xsl:template match="placed-escape[@name]">
  <xsl:call-template name="name-to-linked-escape">
    <xsl:with-param name="placed">true</xsl:with-param>
    <xsl:with-param name="namespace"><xsl:value-of select="@namespace" /></xsl:with-param>
    <xsl:with-param name="name"><xsl:value-of select="@name" /></xsl:with-param>
  </xsl:call-template>
</xsl:template>
<xsl:template match="user-escape[@name]">
  <xsl:call-template name="format-escape-name">
    <xsl:with-param name="namespace"><xsl:value-of select="@namespace" /></xsl:with-param>
    <xsl:with-param name="name"><xsl:value-of select="@name" /></xsl:with-param>
  </xsl:call-template>
</xsl:template>

<!-- Special templates for commonly used values -->
<xsl:template match="value-the-void">
  <xsl:call-template name="name-to-linked-value">
    <xsl:with-param name="namespace">..Shapes</xsl:with-param>
    <xsl:with-param name="name">void</xsl:with-param>
    <xsl:with-param name="placed">true</xsl:with-param> <!-- Suppress namespace for brevity. -->
  </xsl:call-template>
</xsl:template>
<xsl:template match="value-the-false">
  <xsl:call-template name="format-value-name">
    <xsl:with-param name="name">false</xsl:with-param>
  </xsl:call-template>
</xsl:template>
<xsl:template match="value-the-true">
  <xsl:call-template name="format-value-name">
    <xsl:with-param name="name">true</xsl:with-param>
  </xsl:call-template>
</xsl:template>

</xsl:stylesheet>
