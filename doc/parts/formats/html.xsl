<?xml version="1.0" encoding="UTF-8"?>

<!-- This file is part of Shapes.                                           -->
<!--                                                                        -->
<!-- Shapes is free software: you can redistribute it and/or modify         -->
<!-- it under the terms of the GNU General Public License as published by   -->
<!-- the Free Software Foundation, either version 3 of the License, or      -->
<!-- any later version.                                                     -->
<!--                                                                        -->
<!-- Shapes is distributed in the hope that it will be useful,              -->
<!-- but WITHOUT ANY WARRANTY; without even the implied warranty of         -->
<!-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          -->
<!-- GNU General Public License for more details.                           -->
<!--                                                                        -->
<!-- You should have received a copy of the GNU General Public License      -->
<!-- along with Shapes.  If not, see <http://www.gnu.org/licenses/>.        -->
<!--                                                                        -->
<!-- Copyright 2008, 2009, 2010, 2013, 2014 Henrik Tidefelt                 -->

<xsl:stylesheet
  version="2.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns="http://www.w3.org/1999/xhtml"
>

<xsl:template match="str-PDF"><span class="medium-caps">pdf</span></xsl:template>
<xsl:template match="str-HTML"><span class="medium-caps">html</span></xsl:template>
<xsl:template match="str-XML"><span class="medium-caps">xml</span></xsl:template>
<xsl:template match="str-SVG"><span class="medium-caps">svg</span></xsl:template>
<xsl:template match="str-PNG"><span class="medium-caps">png</span></xsl:template>
<xsl:template match="str-JPEG"><span class="medium-caps">jpeg</span></xsl:template>
<xsl:template match="str-DAG"><span class="medium-caps">dag</span></xsl:template>
<xsl:template match="str-PostScript">PostScript</xsl:template>
<xsl:template match="str-Shapes">Shapes</xsl:template>
<xsl:template match="str-shapes-mode"><em>shapes-mode</em></xsl:template>
<xsl:template match="str-TeX"><span class="tex">T<sub>e</sub>X</span></xsl:template>
<xsl:template match="str-LaTeX"><span class="latex">L<sup>a</sup>T<sub>e</sub>X</span></xsl:template>
<xsl:template match="str-pdfLaTeX"><span class="latex">pdfL<sup>a</sup>T<sub>e</sub>X</span></xsl:template>
<xsl:template match="str-MetaPost">MetaPost</xsl:template>
<xsl:template match="str-C-plus-plus"><span class="medium-caps">c</span>++</xsl:template>
<xsl:template match="str-UTF-8"><span class="medium-caps">utf</span>-8</xsl:template>
<xsl:template match="str-2D">2<span class="medium-caps"><sup>d</sup></span></xsl:template>
<xsl:template match="str-3D">3<span class="medium-caps"><sup>d</sup></span></xsl:template>

<xsl:template name="str-Google" match="str-Google"><span class="Google"><span class="blue">G</span><span class="red">o</span><span class="yellow">o</span><span class="blue">g</span><span class="green">l</span><span class="red">e</span><span class="black">™</span></span></xsl:template>

<xsl:template match="abbr-etc">&amp;c</xsl:template>

<xsl:template match="char-cdot">*</xsl:template>
<xsl:template match="char-bullet">•</xsl:template>
<xsl:template match="char-str-open">`</xsl:template>
<xsl:template match="char-str-close">´</xsl:template>

<xsl:template match="const-false"><span class="rm">false</span></xsl:template>
<xsl:template match="const-true"><span class="rm">true</span></xsl:template>

<xsl:template match="x-hat"><b>x</b></xsl:template>
<xsl:template match="y-hat"><b>y</b></xsl:template>
<xsl:template match="z-hat"><b>z</b></xsl:template>

<xsl:template match="physical"><span class="nowrap"><xsl:apply-templates select="scalar" /><span class="xx-small"> </span><xsl:apply-templates select="unit" /></span></xsl:template>
<xsl:template match="sci-fmt[@mantissa,@exp]"><span class="nowrap"><xsl:value-of select="@mantissa" /><span class="small-caps">e</span><xsl:value-of select="@exp" /></span></xsl:template>
<xsl:template match="quote">“<xsl:apply-templates />”</xsl:template>

<xsl:template match="p[not(@class)]">
  <div class="p"><xsl:apply-templates/></div>
</xsl:template>
<xsl:template match="p[@class='center']">
  <div class="p" style="text-align:center;"><xsl:apply-templates/></div>
</xsl:template>
<xsl:template match="p[@class='todo']">
  <div class="p todo"><xsl:apply-templates/></div>
</xsl:template>

<xsl:template match="ol">
  <ol><xsl:apply-templates/></ol>
</xsl:template>
<xsl:template match="ol/li">
  <li><xsl:apply-templates/></li>
</xsl:template>

<xsl:template match="ul">
  <ul><xsl:apply-templates/></ul>
</xsl:template>
<xsl:template match="ul/li">
  <li><xsl:apply-templates/></li>
</xsl:template>

<xsl:template match="pre">
<pre>
<xsl:apply-templates/>
</pre>
</xsl:template>
<xsl:template match="pre[@class]">
<xsl:element name="pre">
<xsl:attribute name="class"><xsl:value-of select="@class" /></xsl:attribute>
<xsl:apply-templates/>
</xsl:element>
</xsl:template>

<xsl:template match="note">
  <table class="note">
    <tr><td><xsl:apply-templates/></td></tr>
  </table>
</xsl:template>

<xsl:template match="see-also">
  <div class="p">
    <b>See also:</b>
    <xsl:for-each select="*">
      <xsl:text> </xsl:text>
      <span class="horizontal-item">
        <xsl:apply-templates select="."/>
      </span>
    </xsl:for-each>
  </div>
</xsl:template>

<xsl:template name="under-development-banner">
	<div class="upper-right-banner">
		<div class="banner-content">Under development</div>
	</div>
</xsl:template>

<xsl:template name="version-header">
	<div class="version-header"><xsl:text>For version </xsl:text>
		<xsl:choose>
			<xsl:when test="/book/shapes-version"><xsl:value-of select="/book/shapes-version/@number" /></xsl:when>
			<xsl:when test="/man/manhead/shapes-version"><xsl:value-of select="/man/manhead/shapes-version/@number" /></xsl:when>
		</xsl:choose>
		<xsl:text>.</xsl:text>
	</div>
</xsl:template>

<xsl:template name="head-navigation" >
	<xsl:call-template name="version-header" />
	<div class="p">
		<xsl:element name="a"><xsl:attribute name="href"><xsl:value-of select="/book/base/@href" />index.html</xsl:attribute><xsl:text>To top.</xsl:text></xsl:element>
		<xsl:if test="/book/up-link">
			<xsl:text>  Up: </xsl:text><xsl:apply-templates select="/book/up-link" />
		</xsl:if>
	</div>
</xsl:template>

<xsl:template name="part-to-href">
	<xsl:param name="name" />
	<xsl:value-of select="/book/base/@href" /><xsl:choose>
		<xsl:when test="$name='structure'">structure.html</xsl:when>
		<xsl:when test="$name='syntax'">syntax.html</xsl:when>
		<xsl:when test="$name='types'">types.html</xsl:when>
		<xsl:when test="$name='state-types'">state-types.html</xsl:when>
		<xsl:when test="$name='algo-tol'">algo-tol.html</xsl:when>
		<xsl:when test="$name='man'">man.html</xsl:when>
		<xsl:when test="$name='tutorial'">tutorial.html</xsl:when>
		<xsl:when test="$name='guides'">guides/index.html</xsl:when>
		<xsl:when test="$name='extensions'">extensions/index.html</xsl:when>
		<xsl:when test="$name='install'">install.html</xsl:when>
		<xsl:when test="$name='interactive'">interactive.html</xsl:when>
		<xsl:when test="$name='edit'">edit.html</xsl:when>
		<xsl:when test="$name='namespace-index'">namespace-index.html</xsl:when>
	</xsl:choose>
</xsl:template>
<xsl:template name="extension-to-href">
  <xsl:param name="namespace" />
  <xsl:param name="name" />
  <xsl:choose>
    <xsl:when test="$namespace">
      <xsl:value-of select="/book/base/@href" /><xsl:text>extensions/</xsl:text><xsl:value-of select="replace(replace($namespace, '^\.\.', ''), '\.\.', '-')" />-<xsl:value-of select="$name" /><xsl:text>.html</xsl:text>
    </xsl:when>
    <xsl:otherwise>
      <xsl:value-of select="/book/base/@href" /><xsl:text>extensions/</xsl:text><xsl:value-of select="$name" /><xsl:text>.html</xsl:text>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>
<xsl:template name="guide-to-href">
	<xsl:param name="name" />
	<xsl:value-of select="/book/base/@href" /><xsl:text>guides/</xsl:text><xsl:value-of select="$name" /><xsl:text>.html</xsl:text>
</xsl:template>
<xsl:template name="id-to-anchor-name">
	<xsl:param name="id" />
	<xsl:value-of select="replace(replace(concat('secid:', $id), '-', '--'), '/', '-.')" />
</xsl:template>
<xsl:template name="env-var-to-anchor-name">
	<xsl:param name="id" />
	<xsl:value-of select="replace(replace(concat('env:', $id), '-', '--'), '/', '-.')" />
</xsl:template>

<xsl:template match="env-var[@name]">
	<xsl:element name="a">
		<xsl:attribute name="class">discrete</xsl:attribute>
		<xsl:attribute name="href">
			<xsl:call-template name="part-to-href">
				<xsl:with-param name="name">man</xsl:with-param>
			</xsl:call-template>#<xsl:call-template name="env-var-to-anchor-name">
				<xsl:with-param name="id"><xsl:value-of select="@name" /></xsl:with-param>
			</xsl:call-template>
		</xsl:attribute>
		<xsl:value-of select="@name" />
	</xsl:element>
</xsl:template>

<xsl:template match="part-href[@name]">
  <xsl:call-template name="part-to-href">
    <xsl:with-param name="name"><xsl:value-of select="@name" /></xsl:with-param>
  </xsl:call-template>
</xsl:template>
<xsl:template match="extension-href[@name]">
  <xsl:call-template name="extension-to-href">
    <xsl:with-param name="name"><xsl:value-of select="@name" /></xsl:with-param>
  </xsl:call-template>
</xsl:template>
<xsl:template name="name-to-linked-extension">
  <xsl:param name="current-namespace" />
  <xsl:param name="namespace" />
  <xsl:param name="name" />
  <xsl:variable name="ns">
    <xsl:choose>
      <xsl:when test="starts-with($namespace, '..')">
        <xsl:value-of select="$namespace" />
      </xsl:when>
      <xsl:when test="$namespace = ''">
        <xsl:value-of select="$current-namespace" />
      </xsl:when>
      <xsl:when test="$current-namespace = '..'">
        <xsl:value-of select="concat('..', $namespace)" />
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="concat($current-namespace, '..', $namespace)" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  <xsl:element name="a">
    <xsl:attribute name="class">discrete</xsl:attribute>
    <xsl:attribute name="href">
      <xsl:call-template name="extension-to-href">
        <xsl:with-param name="namespace"><xsl:value-of select="$ns" /></xsl:with-param>
        <xsl:with-param name="name"><xsl:value-of select="$name" /></xsl:with-param>
      </xsl:call-template>
    </xsl:attribute>
    <xsl:choose>
      <xsl:when test="$ns = $current-namespace">
        <span class="filename"><xsl:value-of select="$name" /></span>
      </xsl:when>
      <xsl:when test="$current-namespace != ''">
        <xsl:variable name="prefix">^<xsl:value-of select="$current-namespace" />\.\.</xsl:variable>
        <xsl:call-template name="format-namespace-name">
          <xsl:with-param name="name"><xsl:value-of select="replace($ns, $prefix, '')" /></xsl:with-param>
        </xsl:call-template>
        <xsl:text> / </xsl:text>
        <span class="filename"><xsl:value-of select="$name" /></span>
      </xsl:when>
      <xsl:otherwise>
        <xsl:call-template name="format-namespace-name">
          <xsl:with-param name="name"><xsl:value-of select="$ns" /></xsl:with-param>
        </xsl:call-template>
        <xsl:text> / </xsl:text>
        <span class="filename"><xsl:value-of select="$name" /></span>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:element>
</xsl:template>
<xsl:template match="extension[@path]">
  <xsl:variable name="namespace"><xsl:value-of select="replace(@path, '/.*$', '')" /></xsl:variable>
  <xsl:variable name="name"><xsl:value-of select="replace(@path, '^.*/', '')" /></xsl:variable>
  <xsl:call-template name="name-to-linked-extension">
    <xsl:with-param name="namespace"><xsl:value-of select="$namespace" /></xsl:with-param>
    <xsl:with-param name="name"><xsl:value-of select="$name" /></xsl:with-param>
  </xsl:call-template>
</xsl:template>
<xsl:template match="/book[namespace]//extension[@name]">
  <xsl:call-template name="name-to-linked-extension">
    <xsl:with-param name="current-namespace"><xsl:value-of select="ancestor::book[namespace][1]/namespace" /></xsl:with-param>
    <xsl:with-param name="namespace"><xsl:value-of select="@namespace" /></xsl:with-param>
    <xsl:with-param name="name"><xsl:value-of select="@name" /></xsl:with-param>
  </xsl:call-template>
</xsl:template>
<xsl:template match="/book[not(namespace)]//extension[@name]">
  <xsl:call-template name="name-to-linked-extension">
    <xsl:with-param name="namespace"><xsl:value-of select="@namespace" /></xsl:with-param>
    <xsl:with-param name="name"><xsl:value-of select="@name" /></xsl:with-param>
  </xsl:call-template>
</xsl:template>

<xsl:template match="guide-href[@name]">
  <xsl:call-template name="guide-to-href">
    <xsl:with-param name="name"><xsl:value-of select="@name" /></xsl:with-param>
  </xsl:call-template>
</xsl:template>

<xsl:template match="a[(@part or @id) and not(@extension)]">
	<xsl:element name="a">
		<xsl:attribute name="href">
			<xsl:if test="@part">
				<xsl:call-template name="part-to-href">
					<xsl:with-param name="name"><xsl:value-of select="@part" /></xsl:with-param>
				</xsl:call-template>
			</xsl:if>
			<xsl:if test="@id">
				<xsl:text>#</xsl:text>
				<xsl:call-template name="id-to-anchor-name"><xsl:with-param name="id"><xsl:value-of select="@id" /></xsl:with-param></xsl:call-template>
			</xsl:if>
		</xsl:attribute>
		<xsl:choose>
			<xsl:when test="node()">
				<xsl:apply-templates />
			</xsl:when>
			<xsl:when test="@part">
				<span class="filename">
					<xsl:call-template name="part-to-href">
						<xsl:with-param name="name"><xsl:value-of select="@part" /></xsl:with-param>
					</xsl:call-template>
				</span>
			</xsl:when>
			<xsl:otherwise>
				<xsl:variable name="dstid">
					<xsl:value-of select="@id" />
				</xsl:variable>
				<xsl:apply-templates select="//section[@id=$dstid]/title" />
				<xsl:apply-templates select="//example-with-output[@id=$dstid]/@title" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:element>
</xsl:template>
<xsl:template match="a[@id]/title">
  <xsl:variable name="dstid">
    <xsl:value-of select="../@id" />
  </xsl:variable>
  <xsl:apply-templates select="//section[@id=$dstid]/title" />
</xsl:template>
<xsl:template match="a[@extension]">
	<xsl:element name="a">
		<xsl:attribute name="href">
			<xsl:call-template name="extension-to-href">
				<xsl:with-param name="name"><xsl:value-of select="@extension" /></xsl:with-param>
			</xsl:call-template>
			<xsl:if test="@id">
				<xsl:text>#</xsl:text>
				<xsl:call-template name="id-to-anchor-name"><xsl:with-param name="id"><xsl:value-of select="@id" /></xsl:with-param></xsl:call-template>
			</xsl:if>
		</xsl:attribute>
		<span class="filename"><xsl:value-of select="@extension" /></span>
	</xsl:element>
</xsl:template>
<xsl:template match="a[@guide]">
	<xsl:element name="a">
		<xsl:attribute name="href">
			<xsl:call-template name="guide-to-href">
				<xsl:with-param name="name"><xsl:value-of select="@guide" /></xsl:with-param>
			</xsl:call-template>
			<xsl:if test="@id">
				<xsl:text>#</xsl:text>
				<xsl:call-template name="id-to-anchor-name"><xsl:with-param name="id"><xsl:value-of select="@id" /></xsl:with-param></xsl:call-template>
			</xsl:if>
		</xsl:attribute>
		<xsl:apply-templates />
	</xsl:element>
</xsl:template>

<xsl:template match='a[@method="google-lucky" and @query]'>
	<xsl:element name="a">
		<xsl:attribute name="href">http://www.google.com/search?btnI=I%27m%20Feeling%20Lucky&amp;q=<xsl:value-of select="encode-for-uri(@query)" /></xsl:attribute>
		<xsl:choose>
			<xsl:when test="node()">
				<xsl:apply-templates />
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>[</xsl:text><xsl:call-template name="str-Google" /><xsl:text>_lucky `</xsl:text><xsl:value-of select="@query" /><xsl:text>´]</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:element>
</xsl:template>
<xsl:template match='a[@method="google" and @query]'>
	<xsl:element name="a">
		<xsl:attribute name="href">http://www.google.com/search?q=<xsl:value-of select="encode-for-uri(@query)" /></xsl:attribute>
		<xsl:choose>
			<xsl:when test="node()">
				<xsl:apply-templates />
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>[</xsl:text><xsl:call-template name="str-Google" /><xsl:text> `</xsl:text><xsl:value-of select="@query" /><xsl:text>´]</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:element>
</xsl:template>
<xsl:template match="a[@query]/query">
  <xsl:value-of select="../@query" />
</xsl:template>

<xsl:template name="name-to-linked-type">
  <xsl:param name="name" />
  <xsl:element name="a">
    <xsl:attribute name="class">discrete</xsl:attribute>
    <xsl:attribute name="href"><xsl:call-template name="part-to-href"><xsl:with-param name="name">types</xsl:with-param></xsl:call-template>#type:<xsl:value-of select="$name" /></xsl:attribute>
    <xsl:call-template name="format-type-name">
      <xsl:with-param name="name"><xsl:value-of select="$name" /></xsl:with-param>
    </xsl:call-template>
  </xsl:element>
</xsl:template>
<xsl:template name="name-to-linked-statetype">
  <xsl:param name="name" />
  <xsl:element name="a">
    <xsl:attribute name="class">discrete</xsl:attribute>
    <xsl:attribute name="href"><xsl:call-template name="part-to-href"><xsl:with-param name="name">state-types</xsl:with-param></xsl:call-template>#statetype:<xsl:value-of select="$name" /></xsl:attribute>
    <xsl:call-template name="format-statetype-name">
      <xsl:with-param name="name"><xsl:value-of select="$name" /></xsl:with-param>
    </xsl:call-template>
  </xsl:element>
</xsl:template>

<xsl:template match="named-type[@name and not(@link)]">
  <xsl:call-template name="name-to-linked-type">
    <xsl:with-param name="name"><xsl:value-of select="@name" /></xsl:with-param>
  </xsl:call-template>
</xsl:template>
<xsl:template match="named-type[@name and @link='no']">
  <xsl:call-template name="format-type-name">
    <xsl:with-param name="name"><xsl:value-of select="@name" /></xsl:with-param>
  </xsl:call-template>
</xsl:template>

<xsl:template match="named-statetype[@name and not(@link)]">
  <xsl:call-template name="name-to-linked-statetype">
    <xsl:with-param name="name"><xsl:value-of select="@name" /></xsl:with-param>
  </xsl:call-template>
</xsl:template>
<xsl:template match="named-statetype[@name and @link='no']">
  <xsl:call-template name="format-statetype-name">
    <xsl:with-param name="name"><xsl:value-of select="@name" /></xsl:with-param>
  </xsl:call-template>
</xsl:template>

<xsl:template name="format-template-type-name">
  <xsl:param name="name" />
  <span class="typename template">§<xsl:value-of select="$name" /></span>
</xsl:template>
<xsl:template name="format-template-statetype-name">
  <xsl:param name="name" />
  <span class="typename template">§•<xsl:value-of select="$name" /></span>
</xsl:template>

<xsl:template match="parameter-type[@name]">
  <xsl:call-template name="format-template-type-name">
    <xsl:with-param name="name"><xsl:value-of select="@name" /></xsl:with-param>
  </xsl:call-template>
</xsl:template>
<xsl:template match="template-type[@name]">
  <xsl:call-template name="format-template-type-name">
    <xsl:with-param name="name"><xsl:value-of select="@name" /></xsl:with-param>
  </xsl:call-template>
</xsl:template>
<xsl:template match="template-statetype[@name]">
  <xsl:call-template name="format-template-statetype-name">
    <xsl:with-param name="name"><xsl:value-of select="@name" /></xsl:with-param>
  </xsl:call-template>
</xsl:template>

<xsl:template name="format-mutator-name">
	<xsl:param name="name" />
	<span class="varname"><xsl:value-of select="$name" /></span>
</xsl:template>
<xsl:template name="name-to-linked-mutator">
	<xsl:param name="extension" />
	<xsl:param name="extension-href" />
	<xsl:param name="type" />
	<xsl:param name="name" />
	<xsl:element name="a">
		<xsl:attribute name="class">discrete</xsl:attribute>
		<xsl:attribute name="href">
			<xsl:choose>
				<xsl:when test="$extension">
					<xsl:call-template name="extension-to-href"><xsl:with-param name="name"><xsl:value-of select="$extension" /></xsl:with-param></xsl:call-template>#mutator:<xsl:value-of select="$type" />:<xsl:value-of select="$name" />
				</xsl:when>
				<xsl:when test="$extension-href">
					<xsl:value-of select="$extension-href" />#mutator:<xsl:value-of select="$type" />:<xsl:value-of select="$name" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="part-to-href"><xsl:with-param name="name">statetypes</xsl:with-param></xsl:call-template>#mutator:<xsl:value-of select="$type" />:<xsl:value-of select="$name" />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:attribute>
		<xsl:call-template name="format-statetype-name">
			<xsl:with-param name="name"><xsl:value-of select="$type" /></xsl:with-param>
		</xsl:call-template>
		<span class="inline">/</span>
		<xsl:call-template name="format-mutator-name">
			<xsl:with-param name="name"><xsl:value-of select="$name" /></xsl:with-param>
		</xsl:call-template>
	</xsl:element>
</xsl:template>
<xsl:template match="mutator[@type and @name and not(@extension)]">
	<xsl:call-template name="name-to-linked-mutator">
		<xsl:with-param name="type"><xsl:value-of select="@type" /></xsl:with-param>
		<xsl:with-param name="name"><xsl:value-of select="@name" /></xsl:with-param>
	</xsl:call-template>
</xsl:template>
<xsl:template match="mutator[@type and @name and @extension]">
	<xsl:call-template name="name-to-linked-mutator">
		<xsl:with-param name="extension"><xsl:value-of select="@extension" /></xsl:with-param>
		<xsl:with-param name="type"><xsl:value-of select="@type" /></xsl:with-param>
		<xsl:with-param name="name"><xsl:value-of select="@name" /></xsl:with-param>
	</xsl:call-template>
</xsl:template>

<xsl:template name="name-to-tol-param">
	<xsl:param name="name" />
	<span class="tolparam"><xsl:value-of select="@name" /></span>
</xsl:template>
<xsl:template match="tol-param[@name and @link='no']">
	<xsl:call-template name="name-to-operator">
		<xsl:with-param name="name"><xsl:value-of select="@name" /></xsl:with-param>
	</xsl:call-template>
</xsl:template>
<xsl:template match="tol-param[@name and not(@link)]">
	<xsl:element name="a">
		<xsl:attribute name="class">discrete</xsl:attribute>
		<xsl:attribute name="href"><xsl:call-template name="part-to-href"><xsl:with-param name="name">algo-tol</xsl:with-param></xsl:call-template>#tol-<xsl:value-of select="@name" /></xsl:attribute>
		<xsl:call-template name="name-to-operator">
			<xsl:with-param name="name"><xsl:value-of select="@name" /></xsl:with-param>
		</xsl:call-template>
	</xsl:element>
</xsl:template>

<xsl:template name="name-to-operator">
	<xsl:param name="name" />
	<span class="inline"><xsl:value-of select="$name" /></span>
</xsl:template>
<xsl:template match="operator[@name]">
	<xsl:call-template name="name-to-operator">
		<xsl:with-param name="name"><xsl:value-of select="@name" /></xsl:with-param>
	</xsl:call-template>
</xsl:template>
<xsl:template name="name-to-operator-unary-prefix">
	<xsl:param name="name" />
	<span class="inline"><xsl:value-of select="$name" /></span>
</xsl:template>
<xsl:template match="operator-unary-prefix[@name]">
	<xsl:call-template name="name-to-operator-unary-prefix">
		<xsl:with-param name="name"><xsl:value-of select="@name" /></xsl:with-param>
	</xsl:call-template>
</xsl:template>

<xsl:template name="format-namespace-name">
  <xsl:param name="name" />
  <span class="inline"><xsl:value-of select="$name" /></span>
</xsl:template>
<xsl:template name="name-to-linked-namespace">
  <xsl:param name="name" />
  <xsl:element name="a">
    <xsl:attribute name="class">discrete</xsl:attribute>
    <xsl:attribute name="href">
      <xsl:call-template name="namespace-to-href"><xsl:with-param name="name"><xsl:value-of select="$name" /></xsl:with-param></xsl:call-template>
    </xsl:attribute>
    <xsl:call-template name="format-namespace-name">
      <xsl:with-param name="name"><xsl:value-of select="$name" /></xsl:with-param>
    </xsl:call-template>
  </xsl:element>
</xsl:template>
<xsl:template match="user-namespace[@name]">
  <xsl:call-template name="format-namespace-name">
    <xsl:with-param name="name"><xsl:value-of select="@name" /></xsl:with-param>
  </xsl:call-template>
</xsl:template>
<xsl:template match="namespace[@name]">
  <xsl:call-template name="name-to-linked-namespace">
    <xsl:with-param name="name"><xsl:value-of select="@name" /></xsl:with-param>
  </xsl:call-template>
</xsl:template>
<xsl:template match="namespace[@current and @relative]">
  <xsl:element name="a">
    <xsl:attribute name="class">discrete</xsl:attribute>
    <xsl:attribute name="href">
      <xsl:call-template name="namespace-to-href"><xsl:with-param name="name"><xsl:value-of select="@current" />..<xsl:value-of select="@relative" /></xsl:with-param></xsl:call-template>
    </xsl:attribute>
    <xsl:call-template name="format-namespace-name">
      <xsl:with-param name="name"><xsl:value-of select="@relative" /></xsl:with-param>
    </xsl:call-template>
  </xsl:element>
</xsl:template>
<xsl:template match="/book[namespace]//namespace[@relative and not(@current)]">
  <xsl:element name="a">
    <xsl:attribute name="class">discrete</xsl:attribute>
    <xsl:attribute name="href">
      <xsl:call-template name="namespace-to-href"><xsl:with-param name="name"><xsl:value-of select="/book/namespace" />..<xsl:value-of select="@relative" /></xsl:with-param></xsl:call-template>
    </xsl:attribute>
    <xsl:call-template name="format-namespace-name">
      <xsl:with-param name="name"><xsl:value-of select="@relative" /></xsl:with-param>
    </xsl:call-template>
  </xsl:element>
</xsl:template>

<xsl:template match="em"><em><xsl:apply-templates/></em></xsl:template>
<xsl:template match="b"><b><xsl:apply-templates/></b></xsl:template>
<xsl:template match="weak"><span class="weak"><xsl:apply-templates/></span></xsl:template>
<xsl:template match="inline"><span class="inline"><xsl:apply-templates/></span></xsl:template>
<xsl:template match="bnf"><span class="bnf"><xsl:apply-templates/></span></xsl:template>
<xsl:template match="union-type">
  <span class="typename"><xsl:text>( </xsl:text></span>
  <xsl:for-each select="./*">
    <xsl:if test="position() > 1">
      <span class="typestx"><xsl:text>∪</xsl:text></span><xsl:text> </xsl:text>
    </xsl:if>
    <xsl:apply-templates select="." />
    <xsl:text> </xsl:text>
  </xsl:for-each>
  <span class="typename">)</span>
</xsl:template>
<xsl:template match="singleton-type">
  <span class="typestx">§(</span>
  <xsl:apply-templates />
  <span class="typestx">)</span>
</xsl:template>
<xsl:template match="intersection-type">
  <span class="typestx"><xsl:text>(</xsl:text></span><xsl:text> </xsl:text>
  <xsl:for-each select="./*">
    <xsl:if test="position() > 1">
      <span class="typestx"><xsl:text>∩</xsl:text></span><xsl:text> </xsl:text>
    </xsl:if>
    <xsl:apply-templates select="." />
    <xsl:text> </xsl:text>
  </xsl:for-each>
  <span class="typestx">)</span>
</xsl:template>
<xsl:template name="name-to-field">
  <xsl:param name="name" />
  <fieldname><xsl:value-of select="$name" /></fieldname>
</xsl:template>
<xsl:template match="structure-type">
  <span class="typestx"><xsl:text>(&gt;</xsl:text></span><xsl:text> </xsl:text>
  <xsl:for-each select="field">
    <xsl:if test="@name">
      <xsl:call-template name="name-to-field">
        <xsl:with-param name="name"><xsl:value-of select="@name" /></xsl:with-param>
      </xsl:call-template>
    </xsl:if>
    <xsl:if test="not(@name)">
      <xsl:value-of select="position( )" />
    </xsl:if>
    <xsl:choose>
    <xsl:when test="type">
      <xsl:text>::</xsl:text><xsl:apply-templates select="type" />
    </xsl:when>
    <xsl:when test="value">
      <xsl:text>:</xsl:text><xsl:apply-templates select="value" />
    </xsl:when>
    <xsl:otherwise>
      <xsl:text>::</xsl:text><typename>Any-Type</typename>
    </xsl:otherwise>
    </xsl:choose>
    <xsl:text> </xsl:text>
  </xsl:for-each>
  <span class="typestx">&lt;)</span>
</xsl:template>
<xsl:template match="function-type">
  <span class="typestx"><xsl:text>(</xsl:text></span><xsl:text> </xsl:text>
  <xsl:apply-templates select="arguments" />
  <xsl:text> </xsl:text><span class="typestx"><xsl:text>→</xsl:text></span><xsl:text> </xsl:text>
  <xsl:apply-templates select="result" />
  <xsl:text> </xsl:text><span class="typestx"><xsl:text>)</xsl:text></span>
</xsl:template>
<xsl:template match="typename"><span class="typename"><xsl:apply-templates/></span></xsl:template>
<xsl:template match="typename[@class='replacable']">
  <span class="typename replacable"><xsl:apply-templates/></span>
</xsl:template>
<xsl:template match="replacable-text[@name]">
  <span class="varname replacable"><xsl:value-of select="@name" /></span>
</xsl:template>
<xsl:template match="parameterized-type">
	<span class="typestx"><xsl:text>[</xsl:text></span>
	<xsl:call-template name="name-to-linked-type">
		<xsl:with-param name="name"><xsl:value-of select="@name" /></xsl:with-param>
	</xsl:call-template>
	<xsl:for-each select="./*">
		<xsl:text> </xsl:text>
		<xsl:apply-templates select="." />
	</xsl:for-each>
	<span class="typestx"><xsl:text>]</xsl:text></span>
</xsl:template>

<xsl:template name="name-to-argument">
	<xsl:param name="name" />
	<xsl:param name="class" />
	<xsl:element name="span">
		<xsl:attribute name="class">varname <xsl:value-of select="$class" /></xsl:attribute>
		<xsl:value-of select="$name" />
	</xsl:element>
</xsl:template>
<xsl:template match="p/arg[@name] | td/arg[@name] | singleton-type/arg[@name]">
	<xsl:call-template name="name-to-argument">
		<xsl:with-param name="name"><xsl:value-of select="@name" /></xsl:with-param>
		<xsl:with-param name="class"><xsl:value-of select="@class" /></xsl:with-param>
	</xsl:call-template>
</xsl:template>

<xsl:template name="name-to-state-argument">
	<xsl:param name="name" />
	<xsl:param name="class" />
	<xsl:element name="span">
		<xsl:attribute name="class">varname <xsl:value-of select="$class" /></xsl:attribute>
		<xsl:call-template name="format-state-name">
			<xsl:with-param name="name"><xsl:value-of select="$name" /></xsl:with-param>
		</xsl:call-template>
	</xsl:element>
</xsl:template>
<xsl:template match="state-arg[@name]">
	<xsl:call-template name="name-to-state-argument">
		<xsl:with-param name="name"><xsl:value-of select="@name" /></xsl:with-param>
		<xsl:with-param name="class"><xsl:value-of select="@class" /></xsl:with-param>
	</xsl:call-template>
</xsl:template>

<xsl:template match="field[@name and not(@class)]">
  <span class="varname"><xsl:value-of select="@name" /></span>
</xsl:template>
<xsl:template match="field[@name and @class='replacable']">
  <span class="varname replacable"><xsl:value-of select="@name" /></span>
</xsl:template>

<xsl:template match="lexerregexp">
  <span class="lexerregexp"><xsl:apply-templates/></span>
</xsl:template>
<xsl:template match="syntax[@name and @class='new']">
  <xsl:element name="a">
    <xsl:attribute name="name">stx-.<xsl:value-of select="@name" /></xsl:attribute>
    <span class="syntaxname-new"><xsl:value-of select="@name"/></span>
  </xsl:element>
</xsl:template>
<xsl:template match="syntax[@name and not(@class)]">
  <xsl:element name="a">
    <xsl:attribute name="class">discrete</xsl:attribute>
    <xsl:attribute name="href"><xsl:call-template name="part-to-href"><xsl:with-param name="name">syntax</xsl:with-param></xsl:call-template>#stx-.<xsl:value-of select="@name" /></xsl:attribute>
    <span class="syntaxname"><xsl:value-of select="@name"/></span>
  </xsl:element>
</xsl:template>
<xsl:template match="a[@href]">
  <xsl:element name="a">
    <xsl:attribute name="href"><xsl:value-of select="@href" /></xsl:attribute>
    <xsl:apply-templates/>
  </xsl:element>
</xsl:template>

<xsl:template match="unit">
  <xsl:element name="span">
    <xsl:attribute name="class">
      <xsl:text>unit</xsl:text>
      <xsl:if test="@class"><xsl:text> </xsl:text><xsl:value-of select="@class" /></xsl:if>
    </xsl:attribute>
    <xsl:apply-templates/>
  </xsl:element>
</xsl:template>

<xsl:template match="filename"><span class="filename"><xsl:value-of select="." /></span></xsl:template>
<xsl:template match="progname"><span class="terminal"><xsl:value-of select="." /></span></xsl:template>
<xsl:template match="command-line">
<pre class="terminal">
<xsl:apply-templates/>
</pre>
</xsl:template>

<xsl:template match="tight-table">
  <table class="tight"><xsl:apply-templates select="head/tr | body/tr"/></table>
</xsl:template>
<xsl:template match="loose-table">
  <table class="loose"><xsl:apply-templates select="head/tr | body/tr"/></table>
</xsl:template>

<xsl:template match="tr">
  <tr><xsl:apply-templates/></tr>
</xsl:template>
<xsl:template match="tr[@align]">
  <xsl:element name="tr">
    <xsl:attribute name="align"><xsl:value-of select="./@align" /></xsl:attribute>
    <xsl:apply-templates/>
  </xsl:element>
</xsl:template>
<xsl:template match="head/tr/td">
  <th><xsl:apply-templates/></th>
</xsl:template>
<xsl:template match="td">
  <td><xsl:apply-templates/></td>
</xsl:template>
<xsl:template match="th">
  <th><xsl:apply-templates/></th>
</xsl:template>
<xsl:template match="th[@colspan]">
  <xsl:element name="th">
    <xsl:attribute name="colspan"><xsl:value-of select="./@colspan" /></xsl:attribute>
    <xsl:apply-templates/>
  </xsl:element>
</xsl:template>

<xsl:template match="img">
	<xsl:choose>
		<xsl:when test="@target">
			<xsl:element name="a">
				<xsl:attribute name="href"><xsl:value-of select="iri-to-uri(replace(@src,'%','%25'))" /></xsl:attribute>
				<xsl:attribute name="target"><xsl:value-of select="@target" /></xsl:attribute>
				<xsl:element name="img">
					<xsl:if test="@class">
						<xsl:attribute name="class"><xsl:value-of select="@class" /></xsl:attribute>
					</xsl:if>
					<xsl:attribute name="src"><xsl:value-of select="iri-to-uri(replace(@src,'%','%25'))" /></xsl:attribute>
					<xsl:attribute name="alt"><xsl:value-of select="@alt" /></xsl:attribute>
					<xsl:if test="@width"><xsl:attribute name="width"><xsl:value-of select="@width" /></xsl:attribute></xsl:if>
					<xsl:if test="@height"><xsl:attribute name="height"><xsl:value-of select="@height" /></xsl:attribute></xsl:if>
				</xsl:element>
			</xsl:element>
		</xsl:when>
		<xsl:otherwise>
			<xsl:element name="img">
					<xsl:if test="@class">
						<xsl:attribute name="class"><xsl:value-of select="@class" /></xsl:attribute>
					</xsl:if>
				<xsl:attribute name="src"><xsl:value-of select="iri-to-uri(replace(@src,'%','%25'))" /></xsl:attribute>
				<xsl:attribute name="alt"><xsl:value-of select="@alt" /></xsl:attribute>
				<xsl:if test="@width"><xsl:attribute name="width"><xsl:value-of select="@width" /></xsl:attribute></xsl:if>
				<xsl:if test="@height"><xsl:attribute name="height"><xsl:value-of select="@height" /></xsl:attribute></xsl:if>
			</xsl:element>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>
<xsl:template match="figure[image]">
	<div class="p">
		<table class="figure">
			<tr align="center"><td>
				<xsl:if test="@id">
					<xsl:element name="a">
						<xsl:attribute name="name"><xsl:call-template name="id-to-anchor-name"><xsl:with-param name="id"><xsl:value-of select="@id" /></xsl:with-param></xsl:call-template></xsl:attribute>
					</xsl:element>
				</xsl:if>
				<xsl:apply-templates select="image/*" />
			</td></tr>
			<tr><td class="here-caption">
				<xsl:apply-templates select="caption/*" />
			</td></tr>
		</table>
	</div>
</xsl:template>


<xsl:template match="arguments">
	<xsl:for-each select="arg | state">
		<xsl:if test="position() > 1">
			<xsl:text> </xsl:text>
		</xsl:if>
		<xsl:apply-templates select="." />
	</xsl:for-each>
	<xsl:if test="sink">
		<xsl:if test="arg | state">
			<xsl:text> </xsl:text>
		</xsl:if>
		<xsl:apply-templates select="sink"/>
	</xsl:if>
</xsl:template>

<xsl:template match="arguments/arg">
	<xsl:if test="@identifier">
		<xsl:call-template name="name-to-argument"><xsl:with-param name="name"><xsl:value-of select="@identifier" /></xsl:with-param></xsl:call-template>
	</xsl:if>
	<xsl:apply-templates select="default"/>
	<xsl:apply-templates select="type"/>
</xsl:template>
<xsl:template match="arguments/arg/default">:<xsl:apply-templates /></xsl:template>
<xsl:template match="arguments/arg/type"><xsl:text>::</xsl:text><xsl:apply-templates select="*" /></xsl:template>

<xsl:template match="arguments/state[@identifier]">
	<xsl:call-template name="name-to-state-argument"><xsl:with-param name="name"><xsl:value-of select="@identifier" /></xsl:with-param></xsl:call-template>
	<xsl:apply-templates select="type"/>
</xsl:template>
<xsl:template match="arguments/state[not(@identifier)]">
	<xsl:apply-templates select="type"/>
</xsl:template>
<xsl:template match="arguments/state/type"><xsl:text>::</xsl:text><xsl:apply-templates /></xsl:template>

<xsl:template match="arguments/sink[@identifier]">
	<xsl:text>&lt;&gt;</xsl:text><xsl:call-template name="name-to-argument"><xsl:with-param name="name"><xsl:value-of select="@identifier" /></xsl:with-param></xsl:call-template>
	<xsl:apply-templates select="type"/>
</xsl:template>
<xsl:template match="arguments/sink[not(@identifier)]">
	<xsl:text>&lt;&gt;</xsl:text>
	<xsl:apply-templates select="type"/>
</xsl:template>
<xsl:template match="arguments/sink/type"><xsl:text>::{</xsl:text><xsl:apply-templates /><xsl:text>}</xsl:text></xsl:template>

<xsl:template match="type-templates">
	<table class="type-templates">
		<xsl:apply-templates />
	</table>
</xsl:template>
<xsl:template match="type-templates/template[@name]">
	<tr>
		<td align="right"><xsl:call-template name="format-template-type-name"><xsl:with-param name="name"><xsl:value-of select="@name" /></xsl:with-param></xsl:call-template>  </td>
		<td>
			<xsl:apply-templates select="description"/>
		</td>
	</tr>
</xsl:template>
<xsl:template match="type-templates/template-state[@name]">
	<tr>
		<td align="right"><xsl:call-template name="format-template-statetype-name"><xsl:with-param name="name"><xsl:value-of select="@name" /></xsl:with-param></xsl:call-template>  </td>
		<td>
			<xsl:apply-templates select="description"/>
		</td>
	</tr>
</xsl:template>

<xsl:template match="book[namespace and not(extension)]/title/self">
  <xsl:value-of select="../../namespace" />
</xsl:template>
<xsl:template match="/book[namespace and not(extension)]/top/p/self">
  <xsl:call-template name="format-namespace-name">
    <xsl:with-param name="name"><xsl:value-of select="/book/namespace" /></xsl:with-param>
  </xsl:call-template>
</xsl:template>

<xsl:template match="book[namespace and extension]/title/self">
  <xsl:value-of select="../../extension" />
</xsl:template>
<xsl:template match="/book[namespace and extension]/top/p/self">
  <span class="filename"><xsl:value-of select="ancestor::book/extension" /></span>
</xsl:template>

<xsl:template match="book[namespace and not(extension)]/meta-selflink/self-href">
  <xsl:call-template name="namespace-to-href">
    <xsl:with-param name="name"><xsl:value-of select="../../namespace" /></xsl:with-param>
  </xsl:call-template>
</xsl:template>
<xsl:template match="book[namespace and extension]/meta-selflink/self-href">
  <xsl:call-template name="extension-to-href">
    <xsl:with-param name="namespace"><xsl:value-of select="../../namespace" /></xsl:with-param>
    <xsl:with-param name="name"><xsl:value-of select="../../extension" /></xsl:with-param>
  </xsl:call-template>
</xsl:template>

<xsl:template match="book[namespace and not(extension)]/up-link/parent-namespace">
  <xsl:variable name="ns"><xsl:value-of select="../../namespace" /></xsl:variable>
  <xsl:call-template name="name-to-linked-namespace">
    <xsl:with-param name="name"><xsl:value-of select="replace($ns, '\.\.[^.]*$', '')" /></xsl:with-param>
  </xsl:call-template>
</xsl:template>
<xsl:template match="book[namespace and extension]/up-link/parent-namespace">
  <xsl:call-template name="name-to-linked-namespace">
    <xsl:with-param name="name"><xsl:value-of select="../../namespace" /></xsl:with-param>
  </xsl:call-template>
</xsl:template>

<xsl:template match="type-parameters">
  <table class="type-templates">
    <xsl:apply-templates />
  </table>
</xsl:template>
<xsl:template match="type-parameters/parameter[@name]">
  <tr>
    <td align="right"><xsl:call-template name="format-template-type-name"><xsl:with-param name="name"><xsl:value-of select="@name" /></xsl:with-param></xsl:call-template>  </td>
    <td>
      <xsl:apply-templates select="description"/>
    </td>
  </tr>
</xsl:template>
<xsl:template match="type-parameters/parameter-state[@name]">
  <tr>
    <td align="right"><xsl:call-template name="format-template-statetype-name"><xsl:with-param name="name"><xsl:value-of select="@name" /></xsl:with-param></xsl:call-template>  </td>
    <td>
      <xsl:apply-templates select="description"/>
    </td>
  </tr>
</xsl:template>

<xsl:template name="SF-placeholder" >
	<xsl:comment>PUT-SF-LOGO-HERE</xsl:comment>
</xsl:template>

<xsl:template match="eq"><span class="eq"><xsl:apply-templates/></span></xsl:template>
<xsl:template match="eq//op"><span class="rm"><xsl:apply-templates/></span></xsl:template>
<xsl:template match="eq//rm"><span class="rm"><xsl:apply-templates/></span></xsl:template>
<xsl:template match="eq//text"><span class="text"><xsl:apply-templates/></span></xsl:template>
<xsl:template match="eq//sub"><sub><xsl:apply-templates/></sub></xsl:template>
<xsl:template match="eq//sup"><sup><xsl:apply-templates/></sup></xsl:template>

</xsl:stylesheet>
