<?xml version="1.0" encoding="UTF-8"?>

<!-- This file is part of Shapes.                                           -->
<!--                                                                        -->
<!-- Shapes is free software: you can redistribute it and/or modify         -->
<!-- it under the terms of the GNU General Public License as published by   -->
<!-- the Free Software Foundation, either version 3 of the License, or      -->
<!-- any later version.                                                     -->
<!--                                                                        -->
<!-- Shapes is distributed in the hope that it will be useful,              -->
<!-- but WITHOUT ANY WARRANTY; without even the implied warranty of         -->
<!-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          -->
<!-- GNU General Public License for more details.                           -->
<!--                                                                        -->
<!-- You should have received a copy of the GNU General Public License      -->
<!-- along with Shapes.  If not, see <http://www.gnu.org/licenses/>.        -->
<!--                                                                        -->
<!-- Copyright 2009, 2013 Henrik Tidefelt                                   -->

<xsl:stylesheet version="2.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="str-PDF">PDF</xsl:template>
<xsl:template match="str-HTML">HTML</xsl:template>
<xsl:template match="str-XML">XML</xsl:template>
<xsl:template match="str-SVG">SVG</xsl:template>
<xsl:template match="str-JPEG">JPEG</xsl:template>
<xsl:template match="str-DAG">DAG</xsl:template>
<xsl:template match="str-PostScript">PostScript</xsl:template>
<xsl:template match="str-Shapes">Shapes</xsl:template>
<xsl:template match="str-TeX">TeX</xsl:template>
<xsl:template match="str-LaTeX">LaTeX</xsl:template>
<xsl:template match="str-pdfLaTeX">pdfLaTeX</xsl:template>
<xsl:template match="str-MetaPost">MetaPost</xsl:template>
<xsl:template match="str-C-plus-plus">C++</xsl:template>
<xsl:template match="str-UTF-8">UTF-8</xsl:template>
<xsl:template match="str-2D">2D</xsl:template>
<xsl:template match="str-3D">3D</xsl:template>

<xsl:template name="str-Google" match="str-Google">Google</xsl:template>

<xsl:template match="abbr-etc">etc</xsl:template>

<xsl:template match="char-cdot">*</xsl:template>
<xsl:template match="char-bullet">#</xsl:template>
<xsl:template match="char-str-open">("</xsl:template>
<xsl:template match="char-str-close">")</xsl:template>

<xsl:template match="physical"><xsl:apply-templates select="scalar" /><xsl:apply-templates select="unit" /></xsl:template>
<xsl:template match="sci-fmt[@mantissa,@exp]"><xsl:value-of select="@mantissa" />e<xsl:value-of select="@exp" /></xsl:template>
<xsl:template match="quote">"<xsl:apply-templates />"</xsl:template>

<xsl:template match="const-false">false</xsl:template>
<xsl:template match="const-true">true</xsl:template>

<xsl:template match="x-hat">x</xsl:template>
<xsl:template match="y-hat">y</xsl:template>
<xsl:template match="z-hat">z</xsl:template>

<xsl:template match="filename"><xsl:value-of select="." /></xsl:template>

<xsl:template match="p">
  <xsl:apply-templates/><xsl:text>
&amp;paragraph-break;</xsl:text>
</xsl:template>

<xsl:template match="ol">
  <xsl:apply-templates select="li"/>
</xsl:template>
<xsl:template match="ol/li">
  <xsl:text>&amp;line-break;o  &amp;push;</xsl:text>&amp;pop;<xsl:apply-templates/>
</xsl:template>

<xsl:template match="ul">
  <xsl:apply-templates select="li"/>
</xsl:template>
<xsl:template match="ul/li">
<xsl:text>&amp;line-break;</xsl:text><xsl:value-of select="string(count( preceding-sibling::* )+1)" /><xsl:text>  &amp;push;</xsl:text><xsl:apply-templates/><xsl:text>&amp;pop;</xsl:text>
</xsl:template>

<xsl:template match="pre">
<xsl:text>&amp;line-break;</xsl:text>
<xsl:apply-templates/>
<xsl:text>&amp;line-break;</xsl:text>
</xsl:template>

<xsl:template match="note">
<xsl:text>&amp;line-break;---( Note )---&amp;line-break;</xsl:text>
<xsl:apply-templates/>
<xsl:text>&amp;line-break;--------------&amp;line-break;</xsl:text>
</xsl:template>

<xsl:template match="see-also">
<xsl:text>&amp;line-break;---( See also )---&amp;line-break;</xsl:text>
<xsl:for-each select="*">
	<xsl:text>  </xsl:text><xsl:apply-templates select="."/>
</xsl:for-each>
<xsl:text>&amp;line-break;--------------&amp;line-break;</xsl:text>
</xsl:template>

<xsl:template match="env-var[@name]">
	<xsl:value-of select="@name" />
</xsl:template>

<xsl:template match="a[(@part or @id) and not(@extension)]">
	<xsl:choose>
		<xsl:when test="node()">
			<xsl:apply-templates />
		</xsl:when>
		<xsl:when test="@part">
			<xsl:value-of select="@part" />
		</xsl:when>
		<xsl:otherwise>
			<xsl:variable name="dstid">
				<xsl:value-of select="@id" />
			</xsl:variable>
			<xsl:apply-templates select="//section[@id=$dstid]/title" />
			<xsl:apply-templates select="//example-with-output[@id=$dstid]/@title" />
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template match='a[@method="google-lucky" and @query]'>
	<xsl:choose>
		<xsl:when test="node()">
			<xsl:apply-templates />
		</xsl:when>
		<xsl:otherwise>
			<xsl:text>[</xsl:text><xsl:call-template name="str-Google" /><xsl:text>_lucky `</xsl:text><xsl:value-of select="@query" /><xsl:text>´]</xsl:text>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>
<xsl:template match='a[@method="google" and @query]'>
	<xsl:choose>
		<xsl:when test="node()">
			<xsl:apply-templates />
		</xsl:when>
		<xsl:otherwise>
			<xsl:text>[</xsl:text><xsl:call-template name="str-Google" /><xsl:text> `</xsl:text><xsl:value-of select="@query" /><xsl:text>´]</xsl:text>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>
<xsl:template match="a[@query]/query">
  <xsl:value-of select="../@query" />
</xsl:template>

<xsl:template match="command-line">
<xsl:text>&amp;line-break;&amp;ignore-next-newline;
  &amp;push;&amp;ignore-next-newline;</xsl:text><xsl:apply-templates/><xsl:text>&amp;ignore-prev-newline;&amp;pop;</xsl:text>
</xsl:template>

<xsl:template match="inline"><xsl:apply-templates/></xsl:template>
<xsl:template match="em"><xsl:text>_</xsl:text><xsl:apply-templates/><xsl:text>_</xsl:text></xsl:template>
<xsl:template match="b"><xsl:text>*</xsl:text><xsl:apply-templates/><xsl:text>*</xsl:text></xsl:template>
<xsl:template match="bnf"><xsl:apply-templates/></xsl:template>

<xsl:template match="eq"><xsl:apply-templates/></xsl:template>
<xsl:template match="eq//op"><xsl:apply-templates/></xsl:template>
<xsl:template match="eq//rm"><xsl:apply-templates/></xsl:template>
<xsl:template match="eq//text"><xsl:apply-templates/></xsl:template>
<xsl:template match="eq//sub"><xsl:text>_{</xsl:text><xsl:apply-templates/><xsl:text>}</xsl:text></xsl:template>
<xsl:template match="eq//sup"><xsl:text>^{</xsl:text><xsl:apply-templates/><xsl:text>}</xsl:text></xsl:template>

</xsl:stylesheet>
