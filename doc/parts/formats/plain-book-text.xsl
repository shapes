<?xml version="1.0" encoding="UTF-8"?>

<!-- This file is part of Shapes.                                           -->
<!--                                                                        -->
<!-- Shapes is free software: you can redistribute it and/or modify         -->
<!-- it under the terms of the GNU General Public License as published by   -->
<!-- the Free Software Foundation, either version 3 of the License, or      -->
<!-- any later version.                                                     -->
<!--                                                                        -->
<!-- Shapes is distributed in the hope that it will be useful,              -->
<!-- but WITHOUT ANY WARRANTY; without even the implied warranty of         -->
<!-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          -->
<!-- GNU General Public License for more details.                           -->
<!--                                                                        -->
<!-- You should have received a copy of the GNU General Public License      -->
<!-- along with Shapes.  If not, see <http://www.gnu.org/licenses/>.        -->
<!--                                                                        -->
<!-- Copyright 2009 Henrik Tidefelt                                         -->

<xsl:stylesheet version="2.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/book">
	<xsl:if test="prelude">
		<xsl:text>[standard prelude]&amp;line-break;</xsl:text>
	</xsl:if>
	<xsl:text>=== </xsl:text><xsl:apply-templates select="title" /><xsl:text> ===
&amp;paragraph-break;</xsl:text>
  <xsl:apply-templates select="top" />
	<xsl:text>&amp;line-break;*Sections:*</xsl:text>
	<xsl:for-each select="section">
		<xsl:text> [</xsl:text>
		<xsl:apply-templates select="title" />
		<xsl:text>]</xsl:text>
	</xsl:for-each>
	<xsl:text>
&amp;paragraph-break;</xsl:text>
  <xsl:for-each select="section">
		<xsl:text>&amp;line-break;&amp;empty-line;---*** </xsl:text><xsl:apply-templates select="title" /><xsl:text> ***---&amp;line-break;</xsl:text>
    <xsl:apply-templates select="top" />
    <xsl:apply-templates select="body" />
    <xsl:for-each select="section">
		<xsl:text>&amp;line-break;&amp;line-break;**</xsl:text><xsl:apply-templates select="title" /><xsl:text>**&amp;line-break;</xsl:text>
			<xsl:apply-templates select="top" />
			<xsl:apply-templates select="body" />
			<xsl:for-each select="section">
				<xsl:text>&amp;line-break;&amp;line-break;*</xsl:text><xsl:apply-templates select="title" /><xsl:text>*&amp;line-break;</xsl:text>
				<xsl:apply-templates select="top" />
				<xsl:apply-templates select="body" />
			</xsl:for-each>
    </xsl:for-each>
  </xsl:for-each>
</xsl:template>

</xsl:stylesheet>
