<?xml version="1.0" encoding="UTF-8"?>

<!-- This file is part of Shapes.                                           -->
<!--                                                                        -->
<!-- Shapes is free software: you can redistribute it and/or modify         -->
<!-- it under the terms of the GNU General Public License as published by   -->
<!-- the Free Software Foundation, either version 3 of the License, or      -->
<!-- any later version.                                                     -->
<!--                                                                        -->
<!-- Shapes is distributed in the hope that it will be useful,              -->
<!-- but WITHOUT ANY WARRANTY; without even the implied warranty of         -->
<!-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          -->
<!-- GNU General Public License for more details.                           -->
<!--                                                                        -->
<!-- You should have received a copy of the GNU General Public License      -->
<!-- along with Shapes.  If not, see <http://www.gnu.org/licenses/>.        -->
<!--                                                                        -->
<!-- Copyright 2008, 2009, 2014, 2015 Henrik Tidefelt                       -->

<xsl:stylesheet
  version="2.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns="http://www.w3.org/1999/xhtml"
>

<xsl:template match="system-binding[@name]">
	<div class="p">
		<xsl:element name="table">
			<xsl:attribute name="class">
				<xsl:choose>
					<xsl:when test="function">function</xsl:when>
					<xsl:when test="simple-value">simple</xsl:when>
					<xsl:when test="hot">hotvalue</xsl:when>
					<xsl:otherwise>loose</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<tr>
				<th class="big" colspan="2">
					<xsl:element name="a">
						<xsl:attribute name="name">value:<xsl:call-template name="name-to-a-id"><xsl:with-param name="name"><xsl:value-of select="@name" /></xsl:with-param></xsl:call-template></xsl:attribute>
						<xsl:call-template name="format-value-name"><xsl:with-param name="name"><xsl:value-of select="@name" /></xsl:with-param></xsl:call-template>
					</xsl:element>
				</th>
			</tr>
			<xsl:apply-templates select="function | simple-value | hot"/>
		</xsl:element>
	</div>
</xsl:template>

<xsl:template match="system-binding[@name]/function | type-method[@identifier]/function | mutator/function">
  <xsl:if test="type-templates">
    <td colspan="2"><xsl:apply-templates select="type-templates"/></td>
  </xsl:if>
  <xsl:if test="top">
    <td colspan="2"><xsl:apply-templates select="top"/></td>
  </xsl:if>
  <xsl:apply-templates select="case"/>
  <xsl:if test="body">
    <td colspan="2"><xsl:apply-templates select="body"/></td>
  </xsl:if>
</xsl:template>

<xsl:template match="function/case">
	<tr>
		<th class="heading" colspan="2">
			<xsl:apply-templates select="arguments"/>
			<xsl:text> </xsl:text><span class="typestx"><xsl:text>→</xsl:text></span><xsl:text> </xsl:text>
			<xsl:choose>
				<xsl:when test="result">
					<xsl:apply-templates select="result/type" />
				</xsl:when>
				<xsl:when test="@constructor-of">
					<xsl:call-template name="name-to-linked-type"><xsl:with-param name="name"><xsl:value-of select="@constructor-of" /></xsl:with-param></xsl:call-template>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="name-to-linked-type"><xsl:with-param name="name">Void</xsl:with-param></xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</th>
	</tr>
	<xsl:apply-templates select="type-templates/*"/>
 	<xsl:apply-templates select="dynamic-references"/>
	<xsl:apply-templates select="side-effect"/>
	<tr>
		<td colspan="2">
			<xsl:apply-templates select="description" />
		</td>
	</tr>
 	<xsl:apply-templates select="see-also"/>
</xsl:template>

<xsl:template match="dynamic-references[not(dynvar | dynstate)]">
	<tr>
		<th class="horiz">Dynamic references:</th>
		<td><em>none</em></td>
	</tr>
</xsl:template>
<xsl:template match="dynamic-references[dynvar | dynstate]">
	<tr>
		<th class="horiz">Dynamic references:</th>
		<td><xsl:apply-templates /></td>
	</tr>
</xsl:template>
<xsl:template match="dynamic-references/dynstate[@name='all']">
	<xsl:text>&lt;The entire dynamic state&gt;</xsl:text>
</xsl:template>
<xsl:template match="dynamic-references/dynstate[@name='graphics']">
	<xsl:text>&lt;The graphics dynamic state&gt;</xsl:text>
</xsl:template>
<xsl:template match="dynamic-references/dynstate[@name='text']">
	<xsl:text>&lt;The text dynamic state&gt;</xsl:text>
</xsl:template>

<xsl:template match="side-effect">
	<tr>
		<th class="horiz">Side effect:</th>
		<td><xsl:apply-templates /></td>
	</tr>
</xsl:template>

<xsl:template match="system-binding[@name]/hot">
	<tr>
		<th class="note" colspan="2">This is a hot value.</th>
	</tr>
	<tr>
		<th class="horiz">Spawns:</th>
		<td><xsl:apply-templates select="constructor-of" /></td>
	</tr>
</xsl:template>

<xsl:template match="system-binding[@name]/simple-value">
	<tr>
		<th class="horiz">Type:</th>
		<td><xsl:apply-templates select="type"/></td>
	</tr>
	<tr>
		<td colspan="2">
			<xsl:apply-templates select="description"/>
		</td>
	</tr>
	<!-- I think the "Related by type list" lost its significance when constants was organized in namespaces, typically according to their type. -->
<!--<xsl:if test="type/named-type"> -->
<!--	<xsl:variable name="self"><xsl:value-of select="../@name" /></xsl:variable> -->
<!--	<xsl:variable name="t"><xsl:value-of select="type/named-type/@name" /></xsl:variable> -->
<!--	<tr> -->
<!--		<th class="horiz">Related by type:</th> -->
<!--		<td> -->
<!--			<xsl:for-each select="//system-binding[@name!=$self]/simple-value/type/named-type[@name=$t]"> -->
<!--				<xsl:if test="position() != 1"><xsl:text> </xsl:text></xsl:if> -->
<!--				<span class="horizontal-item"> -->
<!--					<xsl:call-template name="name-to-linked-value"> -->
<!--						<xsl:with-param name="name"><xsl:value-of select="../../../@name" /></xsl:with-param> -->
<!--					</xsl:call-template> -->
<!--				</span> -->
<!--			</xsl:for-each> -->
<!--		</td> -->
<!--	</tr> -->
<!--</xsl:if> -->
	<xsl:apply-templates select="see-also"/>
</xsl:template>

<xsl:template match="system-binding[@name]/function/case/description/p/self">
	<xsl:call-template name="format-value-name"><xsl:with-param name="name"><xsl:value-of select="../../../../../@name" /></xsl:with-param></xsl:call-template>
</xsl:template>


<xsl:template match="system-binding//see-also | dynamic-variable//see-also | system-state//see-also | core-state-type/see-also | mutator//see-also | escape-continuation//see-also">
 	<tr>
		<th class="horiz">See also:</th>
		<td>
			<xsl:for-each select="./*">
				<xsl:if test="position() != 1"><xsl:text> </xsl:text></xsl:if>
				<span class="horizontal-item">
					<xsl:apply-templates select="."/>
				</span>
			</xsl:for-each>
		</td>
	</tr>
</xsl:template>


<xsl:template match="body/dynamic-variable[@name] | section/dynamic-variable[@name]">
  <xsl:variable name="self">
    <xsl:value-of select="@name" />
  </xsl:variable>
	<div class="p">
		<table class="dynvars">
			<tr>
				<th class="big" colspan="2">
					<xsl:element name="a">
						<xsl:attribute name="name">dyn:<xsl:value-of select="@name" /></xsl:attribute>
						<xsl:call-template name="format-dynamic-name"><xsl:with-param name="name"><xsl:value-of select="@name" /></xsl:with-param></xsl:call-template>
					</xsl:element>
				</th>
			</tr>
			<tr>
				<th class="horiz">Used by:</th>
				<td>
					<xsl:choose>
						<xsl:when test="/book/external">
							<xsl:for-each select="/book/external/section/system-binding[@name]">
								<xsl:if test="function/case/dynamic-references/dynvar[@name=$self]">
									<xsl:call-template name="name-to-linked-value"><xsl:with-param name="name"><xsl:value-of select="@name" /></xsl:with-param></xsl:call-template>
									<xsl:text> </xsl:text>
								</xsl:if>
							</xsl:for-each>
						</xsl:when>
						<xsl:otherwise>
							<i>Not implemented!</i>
						</xsl:otherwise>
					</xsl:choose>
				</td>
			</tr>
			<xsl:apply-templates select="type" />
			<xsl:apply-templates select="default" />
			<xsl:apply-templates select="constraint" />
			<tr>
				<td colspan="2">
					<xsl:apply-templates select="description" />
				</td>
			</tr>
			<xsl:apply-templates select="see-also"/>
		</table>
	</div>
</xsl:template>

<xsl:template match="dynamic-variable[@name]/type">
	<tr>
		<th class="horiz">Type:</th>
		<td><xsl:apply-templates /></td>
	</tr>
</xsl:template>

<xsl:template match="dynamic-variable[@name]/default | escape-continuation[@name]/default">
	<tr>
		<th class="horiz">Default binding:</th>
		<td><xsl:apply-templates /></td>
	</tr>
</xsl:template>

<xsl:template match="dynamic-variable[@name]/constraint">
	<tr>
		<th class="horiz">Constraint:</th>
		<td><xsl:apply-templates /></td>
	</tr>
</xsl:template>

<xsl:template match="dynamic-variable[@name]/description | escape-continuation[@name]/description">
	<xsl:apply-templates />
</xsl:template>

<xsl:template match="dynamic-variable[@name]/constraint/self">
	<xsl:call-template name="format-dynamic-name"><xsl:with-param name="name"><xsl:value-of select="../../@name" /></xsl:with-param></xsl:call-template>
</xsl:template>

<xsl:template match="dynamic-variable-table">
  <table class="dynvars">
		<tr>
			<th>Binding</th>
			<th>Type</th>
			<th>Default</th>
			<th>Description</th>
		</tr>
		<xsl:apply-templates select="dynamic-variable"/>
	</table>
</xsl:template>
<xsl:template match="dynamic-variable-table/dynamic-variable">
	<tr>
		<td>
			<xsl:element name="a">
				<xsl:attribute name="name">dyn:<xsl:value-of select="@name" /></xsl:attribute>
				<xsl:call-template name="format-dynamic-name"><xsl:with-param name="name"><xsl:value-of select="@name" /></xsl:with-param></xsl:call-template>
			</xsl:element>
		</td>
		<td><xsl:apply-templates select="type/*"/></td>
		<td><xsl:apply-templates select="default/*"/></td>
		<td><xsl:apply-templates select="description"/></td>
	</tr>
</xsl:template>

<xsl:template match="body/escape-continuation[@name] | section/escape-continuation[@name]">
  <xsl:variable name="self">
    <xsl:value-of select="@name" />
  </xsl:variable>
	<div class="p">
		<table class="dynvars">
			<tr>
				<th class="big" colspan="2">
					<xsl:element name="a">
						<xsl:attribute name="name">esc:<xsl:value-of select="@name" /></xsl:attribute>
						<xsl:call-template name="format-escape-name"><xsl:with-param name="name"><xsl:value-of select="@name" /></xsl:with-param></xsl:call-template>
					</xsl:element>
				</th>
			</tr>
			<xsl:apply-templates select="purpose" />
			<xsl:apply-templates select="type" />
			<xsl:apply-templates select="default" />
			<tr>
				<td colspan="2">
					<xsl:apply-templates select="description" />
				</td>
			</tr>
			<xsl:apply-templates select="see-also"/>
		</table>
	</div>
</xsl:template>

<xsl:template match="escape-continuation[@name]/type">
	<tr>
		<th class="horiz">Expects type:</th>
		<td><xsl:apply-templates /></td>
	</tr>
</xsl:template>


<xsl:template match="escape-continuation[@name]/purpose">
	<tr>
		<th class="horiz">Purpose:</th>
		<td><xsl:apply-templates /></td>
	</tr>
</xsl:template>


<xsl:template match="system-state[@name]">
	<div class="p">
		<xsl:element name="table">
			<xsl:attribute name="class">state</xsl:attribute>
			<tr>
				<th class="big" colspan="2">
					<xsl:element name="a">
						<xsl:attribute name="name">state:<xsl:value-of select="@name" /></xsl:attribute>
						<xsl:call-template name="format-state-name"><xsl:with-param name="name"><xsl:value-of select="@name" /></xsl:with-param></xsl:call-template>
					</xsl:element>
				</th>
			</tr>
			<tr>
				<th class="horiz">Type:</th>
				<td><xsl:apply-templates select="type"/></td>
			</tr>
			<tr>
				<td colspan="2">
					<xsl:apply-templates select="description/*" />
				</td>
			</tr>
			<xsl:apply-templates select="see-also"/>
		</xsl:element>
	</div>
</xsl:template>

<xsl:template match="system-state[@name]/description/p/self">
	<xsl:call-template name="format-state-name"><xsl:with-param name="name"><xsl:value-of select="../../../@name" /></xsl:with-param></xsl:call-template>
</xsl:template>


<xsl:template match="core-state-type[@name]">
	<div class="p">
		<xsl:element name="table">
			<xsl:attribute name="class">state-type</xsl:attribute>
			<tr>
				<th class="big" colspan="2">
					<xsl:element name="a">
						<xsl:attribute name="name">statetype:<xsl:value-of select="@name" /></xsl:attribute>
						<xsl:call-template name="format-statetype-name"><xsl:with-param name="name"><xsl:value-of select="@name" /></xsl:with-param></xsl:call-template>
					</xsl:element>
				</th>
			</tr>
			<xsl:apply-templates select="abstraction" />
			<xsl:apply-templates select="construction" />
			<xsl:if test="description">
				<tr>
					<td colspan="2">
						<xsl:apply-templates select="description/*" />
					</td>
				</tr>
			</xsl:if>
			<xsl:apply-templates select="see-also"/>
			<tr>
				<th class="big" colspan="2">Mutators</th>
			</tr>
			<tr>
				<td colspan="2"><xsl:apply-templates select="type-templates" /></td>
			</tr>
			<tr>
				<td colspan="2">
					<table class="mutator">
						<tr>
							<th class="big" colspan="2"><span class="inline">• &lt;&lt;</span></th>
						</tr>
						<xsl:apply-templates select="mutator[@op='tack-on']" />
						<xsl:if test="not(mutator[@op='tack-on'])">
							<tr>
								<td colspan="2">
									<div class="p">There are no tack-on mutators.</div>
								</td>
							</tr>
						</xsl:if>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<table class="mutator">
						<tr>
							<th class="big" colspan="2"><span class="inline">(•)</span></th>
						</tr>
						<xsl:apply-templates select="mutator[@op='peek']" />
						<xsl:if test="not(mutator[@op='peek'])">
							<tr>
								<td colspan="2">
									<div class="p">The state cannot be peeked.</div>
								</td>
							</tr>
						</xsl:if>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<table class="mutator">
						<tr>
							<th class="big" colspan="2"><inline>freeze •</inline></th>
						</tr>
						<xsl:apply-templates select="mutator[@op='freeze']" />
						<xsl:if test="not(mutator[@op='freeze'])">
							<tr>
								<td colspan="2">
									<div class="p">The state cannot be frozen.</div>
								</td>
							</tr>
						</xsl:if>
					</table>
				</td>
			</tr>
			<xsl:for-each select="mutator[@identifier]">
				<tr>
					<td colspan="2">
						<table class="mutator">
							<tr>
								<th class="big" colspan="2">
									<xsl:element name="a">
										<xsl:attribute name="name">mutator:<xsl:value-of select="./../@name" />:<xsl:value-of select="@identifier" /></xsl:attribute>
										<xsl:call-template name="format-mutator-name"><xsl:with-param name="name"><xsl:value-of select="@identifier" /></xsl:with-param></xsl:call-template>
									</xsl:element>
									<xsl:if test="alias[@identifier]">
										  <em>alias:</em> <xsl:call-template name="format-mutator-name"><xsl:with-param name="name"><xsl:value-of select="alias/@identifier" /></xsl:with-param></xsl:call-template>
									</xsl:if>
								</th>
							</tr>
							<xsl:apply-templates />
						</table>
					</td>
				</tr>
			</xsl:for-each>
		</xsl:element>
	</div>
</xsl:template>

<xsl:template match="core-state-type[@name]/abstraction">
	<tr>
		<th class="horiz">Abstraction:</th>
		<td><xsl:apply-templates /></td>
	</tr>
</xsl:template>

<xsl:template match="core-state-type[@name]/abstraction/p/self">
	<xsl:call-template name="format-statetype-name">
		<xsl:with-param name="name"><xsl:value-of select="../../../@name" /></xsl:with-param>
	</xsl:call-template>
</xsl:template>
<xsl:template match="core-state-type[@name]/description/p/self">
	<xsl:call-template name="format-statetype-name">
		<xsl:with-param name="name"><xsl:value-of select="../../../@name" /></xsl:with-param>
	</xsl:call-template>
</xsl:template>

<xsl:template match="core-state-type[@name]/construction">
	<tr>
		<th class="horiz">Construction:</th>
		<td>
			<xsl:apply-templates select="*" />
		</td>
	</tr>
</xsl:template>

<xsl:template match="structure-fields-table">
	<table class="loose">
		<tr><th class="title" colspan="3"><xsl:apply-templates select="title" /></th></tr>
		<tr> <th>Field</th> <th>Type</th> <th>Description</th> </tr>
		<tr><td colspan="3"><hr /></td></tr>
		<xsl:apply-templates select="type-field"/>
	</table>
</xsl:template>

<xsl:template match="structure-fields-table/type-field">
  <tr>
    <td><span class="varname"><xsl:value-of select="@name" /></span></td>
    <td><xsl:apply-templates select="type"/></td>
    <td><xsl:apply-templates select="description"/></td>
  </tr>
</xsl:template>

</xsl:stylesheet>
