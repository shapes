<?xml version="1.0" encoding="UTF-8"?>

<!-- This file is part of Shapes.                                           -->
<!--                                                                        -->
<!-- Shapes is free software: you can redistribute it and/or modify         -->
<!-- it under the terms of the GNU General Public License as published by   -->
<!-- the Free Software Foundation, either version 3 of the License, or      -->
<!-- any later version.                                                     -->
<!--                                                                        -->
<!-- Shapes is distributed in the hope that it will be useful,              -->
<!-- but WITHOUT ANY WARRANTY; without even the implied warranty of         -->
<!-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          -->
<!-- GNU General Public License for more details.                           -->
<!--                                                                        -->
<!-- You should have received a copy of the GNU General Public License      -->
<!-- along with Shapes.  If not, see <http://www.gnu.org/licenses/>.        -->
<!--                                                                        -->
<!-- Copyright 2015 Henrik Tidefelt                                         -->

<xsl:stylesheet
  version="2.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
>
<xsl:output method="xml" indent="yes"
 doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
 doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
/>

<xsl:include href="../../formats/html.xsl" />
<xsl:include href="../../formats/bindings-html.xsl" />

<xsl:template match="/external">
  <toc>

    <namespaces>
      <xsl:apply-templates mode="tree-ns" select="/external/book[namespace and not(up-link)]" />
    </namespaces>

    <bindings>
      <xsl:apply-templates mode="binding" select="/external/book[namespace]//system-binding[@name]" />
      <xsl:apply-templates mode="binding" select="/external/book[namespace]//dynamic-variable[@name]" />
      <xsl:apply-templates mode="binding" select="/external/book[namespace]//escape-continuation[@name]" />
      <xsl:apply-templates mode="binding" select="/external/book[namespace]//system-state[@name]" />
      <xsl:apply-templates mode="binding" select="/external/book[namespace]//coretype[@name]" />
      <xsl:apply-templates mode="binding" select="/external/book[namespace]//core-state-type[@name]" />
    </bindings>

  </toc>
</xsl:template>

<xsl:template mode="tree-ns" match="/external/book[namespace and not(extension)]">
  <xsl:variable name="ns"><xsl:value-of select="namespace" /></xsl:variable>
  <xsl:variable name="indent"><xsl:value-of select="replace(replace($ns, '[^\.]', ''), '\.', ' ')" /></xsl:variable>
  <xsl:element name="namespace">
    <xsl:attribute name="name">
      <xsl:value-of select="$ns" />
    </xsl:attribute>
    <xsl:attribute name="relative">
      <xsl:value-of select="replace($ns, '^.*\.\.', '')" />
    </xsl:attribute>
    <xsl:apply-templates mode="tree-ns" select="/external/book[not(extension) and replace(namespace/text(), '\.\.[^.]+$', '') = $ns]" />
    <xsl:apply-templates mode="tree-ext" select="/external/book[extension and namespace/text() = $ns]" />
  </xsl:element>
</xsl:template>

<xsl:template mode="tree-ext" match="/external/book[namespace and extension]">
  <xsl:variable name="ns"><xsl:value-of select="namespace" /></xsl:variable>
  <xsl:variable name="indent"><xsl:value-of select="replace(replace($ns, '[^\.]', ''), '\.', ' ')" /></xsl:variable>
  <xsl:element name="extension">
    <xsl:attribute name="name">
      <xsl:value-of select="$ns" />/<xsl:value-of select="extension" />
    </xsl:attribute>
    <xsl:attribute name="relative">
      <xsl:value-of select="extension" />
    </xsl:attribute>
  </xsl:element>
</xsl:template>

<xsl:template mode="binding" match="/external/book[namespace and not(extension)]//system-binding[@name]">
  <xsl:variable name="ns"><xsl:value-of select="ancestor::book[1]/namespace" /></xsl:variable>
  <xsl:element name="binding">
    <xsl:attribute name="namespace"><xsl:value-of select="$ns" /></xsl:attribute>
    <xsl:attribute name="kind">value</xsl:attribute>
    <xsl:attribute name="name"><xsl:value-of select="@name" /></xsl:attribute>
    <xsl:element name="summary"><xsl:copy-of select="summary/node()" /></xsl:element>
    <xsl:if test="function/case/@constructor-of">
      <xsl:for-each select="function/case[@constructor-of]">
        <xsl:element name="constructor-of">
          <xsl:element name="named-type">
            <xsl:attribute name="name"><xsl:value-of select="@constructor-of" /></xsl:attribute>
          </xsl:element>
        </xsl:element>
      </xsl:for-each>
    </xsl:if>
    <xsl:if test="hot/constructor-of">
      <xsl:element name="hot"><xsl:copy-of select="hot/constructor-of" /></xsl:element>
    </xsl:if>
  </xsl:element>
</xsl:template>

<xsl:template mode="binding" match="/external/book[namespace and not(extension)]//dynamic-variable[@name]">
  <xsl:variable name="ns"><xsl:value-of select="ancestor::book[1]/namespace" /></xsl:variable>
  <xsl:element name="binding">
    <xsl:attribute name="namespace"><xsl:value-of select="$ns" /></xsl:attribute>
    <xsl:attribute name="kind">dynamic</xsl:attribute>
    <xsl:attribute name="name"><xsl:value-of select="@name" /></xsl:attribute>
    <xsl:element name="summary"><xsl:copy-of select="summary/node()" /></xsl:element>
  </xsl:element>
</xsl:template>

<xsl:template mode="binding" match="/external/book[namespace and not(extension)]//escape-continuation[@name]">
  <xsl:variable name="ns"><xsl:value-of select="ancestor::book[1]/namespace" /></xsl:variable>
  <xsl:element name="binding">
    <xsl:attribute name="namespace"><xsl:value-of select="$ns" /></xsl:attribute>
    <xsl:attribute name="kind">escape</xsl:attribute>
    <xsl:attribute name="name"><xsl:value-of select="@name" /></xsl:attribute>
    <xsl:element name="summary"><xsl:copy-of select="summary/node()" /></xsl:element>
  </xsl:element>
</xsl:template>

<xsl:template mode="binding" match="/external/book[namespace and not(extension)]//system-state[@name]">
  <xsl:variable name="ns"><xsl:value-of select="ancestor::book[1]/namespace" /></xsl:variable>
  <xsl:element name="binding">
    <xsl:attribute name="namespace"><xsl:value-of select="$ns" /></xsl:attribute>
    <xsl:attribute name="kind">state</xsl:attribute>
    <xsl:attribute name="name"><xsl:value-of select="@name" /></xsl:attribute>
    <xsl:element name="summary"><xsl:copy-of select="summary/node()" /></xsl:element>
  </xsl:element>
</xsl:template>

<xsl:template mode="binding" match="/external/book[namespace and not(extension)]//coretype[@name]">
  <xsl:variable name="ns"><xsl:value-of select="ancestor::book[1]/namespace" /></xsl:variable>
  <xsl:element name="binding">
    <xsl:attribute name="namespace"><xsl:value-of select="$ns" /></xsl:attribute>
    <xsl:attribute name="kind">type</xsl:attribute>
    <xsl:attribute name="name"><xsl:value-of select="@name" /></xsl:attribute>
    <xsl:element name="summary"><xsl:copy-of select="summary/node()" /></xsl:element>
  </xsl:element>
</xsl:template>

<xsl:template mode="binding" match="/external/book[namespace and not(extension)]//core-state-type[@name]">
  <xsl:variable name="ns"><xsl:value-of select="ancestor::book[1]/namespace" /></xsl:variable>
  <xsl:element name="binding">
    <xsl:attribute name="namespace"><xsl:value-of select="$ns" /></xsl:attribute>
    <xsl:attribute name="kind">statetype</xsl:attribute>
    <xsl:attribute name="name"><xsl:value-of select="@name" /></xsl:attribute>
    <xsl:element name="summary"><xsl:copy-of select="summary/node()" /></xsl:element>
  </xsl:element>
</xsl:template>

<xsl:template mode="binding" match="/external/book[namespace and extension]//system-binding[@name]">
  <xsl:variable name="ns"><xsl:value-of select="ancestor::book[1]/namespace" /></xsl:variable>
  <xsl:variable name="ext"><xsl:value-of select="ancestor::book[1]/extension" /></xsl:variable>
  <xsl:element name="binding">
    <xsl:attribute name="namespace"><xsl:value-of select="$ns" /></xsl:attribute>
    <xsl:attribute name="extension"><xsl:value-of select="$ext" /></xsl:attribute>
    <xsl:attribute name="kind">value</xsl:attribute>
    <xsl:attribute name="name"><xsl:value-of select="@name" /></xsl:attribute>
    <xsl:element name="summary"><xsl:copy-of select="summary/node()" /></xsl:element>
    <xsl:if test="function/case/@constructor-of">
      <xsl:for-each select="function/case[@constructor-of]">
        <xsl:element name="constructor-of">
          <xsl:element name="named-type">
            <xsl:attribute name="name"><xsl:value-of select="@constructor-of" /></xsl:attribute>
          </xsl:element>
        </xsl:element>
      </xsl:for-each>
    </xsl:if>
    <xsl:if test="hot/constructor-of">
      <xsl:element name="hot"><xsl:copy-of select="hot/constructor-of" /></xsl:element>
    </xsl:if>
  </xsl:element>
</xsl:template>

<xsl:template mode="binding" match="/external/book[namespace and extension]//dynamic-variable[@name]">
  <xsl:variable name="ns"><xsl:value-of select="ancestor::book[1]/namespace" /></xsl:variable>
  <xsl:variable name="ext"><xsl:value-of select="ancestor::book[1]/extension" /></xsl:variable>
  <xsl:element name="binding">
    <xsl:attribute name="namespace"><xsl:value-of select="$ns" /></xsl:attribute>
    <xsl:attribute name="extension"><xsl:value-of select="$ext" /></xsl:attribute>
    <xsl:attribute name="kind">dynamic</xsl:attribute>
    <xsl:attribute name="name"><xsl:value-of select="@name" /></xsl:attribute>
    <xsl:element name="summary"><xsl:copy-of select="summary/node()" /></xsl:element>
  </xsl:element>
</xsl:template>

<xsl:template mode="binding" match="/external/book[namespace and extension]//escape-continuation[@name]">
  <xsl:variable name="ns"><xsl:value-of select="ancestor::book[1]/namespace" /></xsl:variable>
  <xsl:variable name="ext"><xsl:value-of select="ancestor::book[1]/extension" /></xsl:variable>
  <xsl:element name="binding">
    <xsl:attribute name="namespace"><xsl:value-of select="$ns" /></xsl:attribute>
    <xsl:attribute name="extension"><xsl:value-of select="$ext" /></xsl:attribute>
    <xsl:attribute name="kind">escape</xsl:attribute>
    <xsl:attribute name="name"><xsl:value-of select="@name" /></xsl:attribute>
    <xsl:element name="summary"><xsl:copy-of select="summary/node()" /></xsl:element>
  </xsl:element>
</xsl:template>

<xsl:template mode="binding" match="/external/book[namespace and extension]//system-state[@name]">
  <xsl:variable name="ns"><xsl:value-of select="ancestor::book[1]/namespace" /></xsl:variable>
  <xsl:variable name="ext"><xsl:value-of select="ancestor::book[1]/extension" /></xsl:variable>
  <xsl:element name="binding">
    <xsl:attribute name="namespace"><xsl:value-of select="$ns" /></xsl:attribute>
    <xsl:attribute name="extension"><xsl:value-of select="$ext" /></xsl:attribute>
    <xsl:attribute name="kind">state</xsl:attribute>
    <xsl:attribute name="name"><xsl:value-of select="@name" /></xsl:attribute>
    <xsl:element name="summary"><xsl:copy-of select="summary/node()" /></xsl:element>
  </xsl:element>
</xsl:template>


</xsl:stylesheet>
