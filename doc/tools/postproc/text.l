/* ÅÄÖåäö -- UTF-8 hint
 */
/* This file is part of Shapes.
 *
 * Shapes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Shapes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2009 Henrik Tidefelt
 */

%{

  void showPop( const char * str );

%}

LineBreak ([ \t\n]|" "|"&pop;")*("&line-break;"([ \t\n]|" "|"&pop;")*)+
ParagraphBreak ([ \t\n]|" "|"&pop;")*{LineBreak}?("\n&paragraph-break;"([ \t\n]|" "|"&pop;")*{LineBreak}?)+

%%

{ParagraphBreak} {
  printf( "\n\n" );
  showPop( yytext );
}
{LineBreak} {
  printf( "\n" );
  showPop( yytext );
}

"&empty-line;" { printf( "\n" ); }
"&ignore-next-newline;\n" { }
"&ignore-next-newline;" { }
"\n&ignore-prev-newline;" { }
"&ignore-prev-newline;" { }

"—" { printf( "--" ); }
"…" { printf( "..." ); }
"⋅" { printf( "*" ); }
" " { printf( " " ); }

%%

int
main( )
{
	yylex( );
	return 0;
}

void
showPop( const char * str )
{
  const char * i;
	for( i = strstr( str, "&pop;" ); i != 0; i = strstr( i + 1, "&pop;" ) )
		{
			printf( "&pop;" );
		}
}
