/* ÅÄÖåäö -- UTF-8 hint
 */
/* This file is part of Shapes.
 *
 * Shapes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Shapes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2009 Henrik Tidefelt
 */
%{
  int column = 0;
#define STACK_SIZE 20
  int leftMarginStack[ STACK_SIZE ];
	int * leftMarginStackEnd = leftMarginStack + STACK_SIZE;
  int * leftMarginTop = leftMarginStack;
  const int WIDTH = 80;
%}

%%

"&push;" {
	++leftMarginTop;
	if( leftMarginTop >= leftMarginStackEnd )
		{
			fprintf( stderr, "Push: stack owerflow.\n" );
			exit( 1 );
		}
	*leftMarginTop = column;
}

"&pop;" {
	--leftMarginTop;
	if( leftMarginTop < leftMarginStack )
		{
			fprintf( stderr, "Pop: stack underflow.\n" );
			exit( 1 );
		}
}

[^& \n]+|"&" {
  if( column + yyleng > WIDTH )
		{
			printf( "\n" );
			column = 0;
		}
	if( column == 0 )
		{
			int tmp = 0;
			for( ; tmp < *leftMarginTop; ++tmp )
				{
					printf( " " );
				}
			column = *leftMarginTop;
		}
	ECHO;
	column += yyleng;
}

[ ] { ECHO; column += 1; }

[\n] { ECHO; column = 0; }

%%

int
main( )
{
	*leftMarginTop = 0;
	yylex( );
	if( leftMarginTop != leftMarginStack )
		{
			fprintf( stderr, "EOF reached with unbalanced pushes.\n" );
			exit( 1 );
		}
	return 0;
}
