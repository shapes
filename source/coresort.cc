/* This file is part of Shapes.
 *
 * Shapes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Shapes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2014 Henrik Tidefelt
 */

#include <cmath>

#include "shapescore.h"
#include "globals.h"
#include "shapesexceptions.h"
#include "astfun.h"
#include "continuations.h"

#include <vector>

using namespace Shapes;


namespace Shapes
{
  namespace Kernel
  {

    class Core_leiographicSort_cont_values : public Kernel::Continuation
    {
      RefCountPtr< const Lang::SingleList > keys_;
      const Ast::SourceLocation & keysLoc_;
      Kernel::ContRef cont_;
    public:
      Core_leiographicSort_cont_values( RefCountPtr< const Lang::SingleList > keys, const Ast::SourceLocation & keysLoc, const Kernel::ContRef & cont, const Ast::SourceLocation & valuesLoc )
      : Kernel::Continuation( valuesLoc ), keys_( keys ), keysLoc_( keysLoc ), cont_( cont )
      { }
      virtual ~Core_leiographicSort_cont_values( ) { }
      virtual void takeValue( const RefCountPtr< const Lang::Value > & keysUntyped, Kernel::EvalState * evalState, bool dummy ) const;
      virtual Kernel::ContRef up( ) const
      {
        return cont_;
      }
      virtual RefCountPtr< const char > description( ) const
      {
        return strrefdup( "force list of values associated with keys to be sorted" );
      }
      virtual void gcMark( Kernel::GCMarkedSet & marked )
      {
        const_cast< Lang::SingleList * >( keys_.getPtr( ) )->gcMark( marked );
        cont_->gcMark( marked );
      }
    };

    class Core_leiographicSort_cont_keys : public Kernel::Continuation
    {
      RefCountPtr< const Lang::Value > values_;
      const Ast::SourceLocation & valuesLoc_;
      Kernel::ContRef cont_;
    public:
      Core_leiographicSort_cont_keys( RefCountPtr< const Lang::Value > values, const Ast::SourceLocation & valuesLoc, const Kernel::ContRef & cont, const Ast::SourceLocation & keysLoc )
      : Kernel::Continuation( keysLoc ), values_( values ), valuesLoc_( valuesLoc ), cont_( cont )
      { }
      virtual ~Core_leiographicSort_cont_keys( ) { }
      virtual void takeValue( const RefCountPtr< const Lang::Value > & keysUntyped, Kernel::EvalState * evalState, bool dummy ) const;
      virtual Kernel::ContRef up( ) const
      {
        return cont_;
      }
      virtual RefCountPtr< const char > description( ) const
      {
        return strrefdup( "force list of keys to be sorted" );
      }
      virtual void gcMark( Kernel::GCMarkedSet & marked )
      {
        const_cast< Lang::Value * >( values_.getPtr( ) )->gcMark( marked );
        cont_->gcMark( marked );
      }
    };

    class Core_mergeSort_cont_sort: public Kernel::Continuation
    {
      bool reversed_;
      RefCountPtr< const Lang::Function > precedes_;
      const Ast::SourceLocation & precedesLoc_;
      Kernel::PassedDyn dyn_;
      Kernel::ContRef cont_;
    public:
      Core_mergeSort_cont_sort( bool reversed, RefCountPtr< const Lang::Function > precedes, const Ast::SourceLocation & precedesLoc, const Kernel::PassedDyn & dyn, const Kernel::ContRef & cont, const Ast::SourceLocation & callLoc )
      : Kernel::Continuation( callLoc ), reversed_( reversed ), precedes_( precedes ), precedesLoc_( precedesLoc ), dyn_( dyn ), cont_( cont )
      { }
      virtual ~Core_mergeSort_cont_sort( ) { }
      virtual void takeValue( const RefCountPtr< const Lang::Value > & valuesUntyped, Kernel::EvalState * evalState, bool dummy ) const;
      virtual Kernel::ContRef up( ) const
      {
        return cont_;
      }
      virtual RefCountPtr< const char > description( ) const
      {
        return strrefdup( "mergesort split in two, and sort first part" );
      }
      virtual void gcMark( Kernel::GCMarkedSet & marked )
      {
        const_cast< Lang::Function * >( precedes_.getPtr( ) )->gcMark( marked );
        dyn_->gcMark( marked );
        cont_->gcMark( marked );
      }
    };

    class Core_mergeSort_cont_sortSecond: public Kernel::Continuation
    {
      bool reversed_;
      RefCountPtr< const Lang::Function > precedes_;
      const Ast::SourceLocation & precedesLoc_;
      RefCountPtr< const Lang::SingleListPair > secondUnsorted_;
      Kernel::PassedDyn dyn_;
      Kernel::ContRef cont_;
    public:
      Core_mergeSort_cont_sortSecond( bool reversed, RefCountPtr< const Lang::Function > precedes, const Ast::SourceLocation & precedesLoc, const RefCountPtr< const Lang::SingleListPair > & secondUnsorted, const Kernel::PassedDyn & dyn, const Kernel::ContRef & cont, const Ast::SourceLocation & callLoc )
      : Kernel::Continuation( callLoc ), reversed_( reversed ), precedes_( precedes ), precedesLoc_( precedesLoc ), secondUnsorted_( secondUnsorted ), dyn_( dyn ), cont_( cont )
      { }
      virtual ~Core_mergeSort_cont_sortSecond( ) { }
      virtual void takeValue( const RefCountPtr< const Lang::Value > & valUntyped, Kernel::EvalState * evalState, bool dummy ) const;
      virtual Kernel::ContRef up( ) const
      {
        return cont_;
      }
      virtual RefCountPtr< const char > description( ) const
      {
        return strrefdup( "mergesort sort second part" );
      }
      virtual void gcMark( Kernel::GCMarkedSet & marked )
      {
        const_cast< Lang::Function * >( precedes_.getPtr( ) )->gcMark( marked );
        const_cast< Lang::SingleListPair * >( secondUnsorted_.getPtr( ) )->gcMark( marked );
        dyn_->gcMark( marked );
        cont_->gcMark( marked );
      }
    };

    class Core_mergeSort_cont_merge: public Kernel::Continuation
    {
      bool reversed_;
      RefCountPtr< const Lang::Function > precedes_;
      const Ast::SourceLocation & precedesLoc_;
      RefCountPtr< const Lang::SingleListPair > firstSorted_;
      Kernel::PassedDyn dyn_;
      Kernel::ContRef cont_;
    public:
      Core_mergeSort_cont_merge( bool reversed, RefCountPtr< const Lang::Function > precedes, const Ast::SourceLocation & precedesLoc, const RefCountPtr< const Lang::SingleListPair > & firstSorted, const Kernel::PassedDyn & dyn, const Kernel::ContRef & cont, const Ast::SourceLocation & callLoc )
      : Kernel::Continuation( callLoc ), reversed_( reversed ), precedes_( precedes ), precedesLoc_( precedesLoc ), firstSorted_( firstSorted ), dyn_( dyn ), cont_( cont )
      { }
      virtual ~Core_mergeSort_cont_merge( ) { }
      virtual void takeValue( const RefCountPtr< const Lang::Value > & valUntyped, Kernel::EvalState * evalState, bool dummy ) const;
      virtual Kernel::ContRef up( ) const
      {
        return cont_;
      }
      virtual RefCountPtr< const char > description( ) const
      {
        return strrefdup( "mergesort merge" );
      }
      virtual void gcMark( Kernel::GCMarkedSet & marked )
      {
        const_cast< Lang::Function * >( precedes_.getPtr( ) )->gcMark( marked );
        const_cast< Lang::SingleListPair * >( firstSorted_.getPtr( ) )->gcMark( marked );
        dyn_->gcMark( marked );
        cont_->gcMark( marked );
      }
    };

    class Core_mergeSort_cont_mergeShortcut: public Kernel::Continuation
    {
      bool reversed_;
      RefCountPtr< const Lang::Function > precedes_;
      const Ast::SourceLocation & precedesLoc_;
      RefCountPtr< const Lang::SingleListPair > firstSorted_;
      RefCountPtr< const Lang::SingleListPair > secondSorted_;
      Kernel::PassedDyn dyn_;
      Kernel::ContRef cont_;
      const Ast::SourceLocation & callLoc_;
    public:
      Core_mergeSort_cont_mergeShortcut( bool reversed, RefCountPtr< const Lang::Function > precedes, const Ast::SourceLocation & precedesLoc, const RefCountPtr< const Lang::SingleListPair > & firstSorted, const RefCountPtr< const Lang::SingleListPair > & secondSorted, const Kernel::PassedDyn & dyn, const Kernel::ContRef & cont, const Ast::SourceLocation & callbackLoc, const Ast::SourceLocation & callLoc )
      : Kernel::Continuation( callbackLoc ), reversed_( reversed ), precedes_( precedes ), precedesLoc_( precedesLoc ), firstSorted_( firstSorted ), secondSorted_( secondSorted ), dyn_( dyn ), cont_( cont ), callLoc_( callLoc )
      { }
      virtual ~Core_mergeSort_cont_mergeShortcut( ) { }
      virtual void takeValue( const RefCountPtr< const Lang::Value > & secondSortedUntyped, Kernel::EvalState * evalState, bool dummy ) const;
      virtual Kernel::ContRef up( ) const
      {
        return cont_;
      }
      virtual RefCountPtr< const char > description( ) const
      {
        return strrefdup( "mergesort merge possibly shortcut" );
      }
      virtual void gcMark( Kernel::GCMarkedSet & marked )
      {
        const_cast< Lang::Function * >( precedes_.getPtr( ) )->gcMark( marked );
        const_cast< Lang::SingleListPair * >( firstSorted_.getPtr( ) )->gcMark( marked );
        const_cast< Lang::SingleListPair * >( secondSorted_.getPtr( ) )->gcMark( marked );
        dyn_->gcMark( marked );
        cont_->gcMark( marked );
      }
    };

    class Core_mergeSort_cont_mergeStep: public Kernel::Continuation
    {
      bool reversed_;
      bool firstAtStart_;
      RefCountPtr< const Lang::Function > precedes_;
      const Ast::SourceLocation & precedesLoc_;
      RefCountPtr< const Lang::SingleListPair > first_;
      RefCountPtr< const Lang::SingleListPair > second_;
      RefCountPtr< const Lang::SingleList > reverseResult_;
      Kernel::VariableHandle lastInResult_;
      Kernel::PassedDyn dyn_;
      Kernel::ContRef cont_;
      const Ast::SourceLocation & callLoc_;
    public:
      Core_mergeSort_cont_mergeStep( bool reversed, RefCountPtr< const Lang::Function > precedes, const Ast::SourceLocation & precedesLoc, const RefCountPtr< const Lang::SingleListPair > & first, const RefCountPtr< const Lang::SingleListPair > & second, bool firstAtStart, const RefCountPtr< const Lang::SingleList > & reverseResult, const Kernel::VariableHandle & lastInResult, const Kernel::PassedDyn & dyn, const Kernel::ContRef & cont, const Ast::SourceLocation & callbackLoc, const Ast::SourceLocation & callLoc )
      : Kernel::Continuation( callbackLoc ), reversed_( reversed ), firstAtStart_( firstAtStart ), precedes_( precedes ), precedesLoc_( precedesLoc ), first_( first ), second_( second ), reverseResult_( reverseResult ), lastInResult_( lastInResult ), dyn_( dyn ), cont_( cont ), callLoc_( callLoc )
      { }
      virtual ~Core_mergeSort_cont_mergeStep( ) { }
      virtual void takeValue( const RefCountPtr< const Lang::Value > & precedesResUntyped, Kernel::EvalState * evalState, bool dummy ) const;
      virtual Kernel::ContRef up( ) const
      {
        return cont_;
      }
      virtual RefCountPtr< const char > description( ) const
      {
        return strrefdup( "mergesort merge step" );
      }
      virtual void gcMark( Kernel::GCMarkedSet & marked )
      {
        const_cast< Lang::Function * >( precedes_.getPtr( ) )->gcMark( marked );
        const_cast< Lang::SingleListPair * >( first_.getPtr( ) )->gcMark( marked );
        const_cast< Lang::SingleListPair * >( second_.getPtr( ) )->gcMark( marked );
        const_cast< Lang::SingleList * >( reverseResult_.getPtr( ) )->gcMark( marked );
        dyn_->gcMark( marked );
        cont_->gcMark( marked );
      }
      static void callback( bool reversed, RefCountPtr< const Lang::Function > precedes, const Ast::SourceLocation & precedesLoc, const RefCountPtr< const Lang::SingleListPair > & first, const RefCountPtr< const Lang::SingleListPair > & second, bool firstAtStart, RefCountPtr< const Lang::SingleList > reverseResult, const Kernel::VariableHandle & lastInResult, const Kernel::PassedDyn & dyn, const Kernel::ContRef & cont, const Ast::SourceLocation & callLoc, Kernel::EvalState * evalState );
    };

    class Core_mergeSort_cont_finish: public Kernel::Continuation
    {
      Kernel::ContRef cont_;
    public:
      Core_mergeSort_cont_finish( const Kernel::ContRef & cont, const Ast::SourceLocation & callLoc )
      : Kernel::Continuation( callLoc ), cont_( cont )
      { }
      virtual ~Core_mergeSort_cont_finish( ) { }
      virtual void takeValue( const RefCountPtr< const Lang::Value > & valuesSortedUntyped, Kernel::EvalState * evalState, bool dummy ) const;
      virtual Kernel::ContRef up( ) const
      {
        return cont_;
      }
      virtual RefCountPtr< const char > description( ) const
      {
        return strrefdup( "mergesort finish" );
      }
      virtual void gcMark( Kernel::GCMarkedSet & marked )
      {
        cont_->gcMark( marked );
      }
    };

  }

  namespace Lang
  {

    class Core_lexiographicSort : public Lang::CoreFunction
    {
    public:
      Core_lexiographicSort( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name )
        : CoreFunction( ns, name, new Kernel::EvaluatedFormals( Ast::FileID::build_internal( name ), true ) )
      {
        formals_->appendEvaluatedCoreFormal( "keys", Kernel::THE_SLOT_VARIABLE );
        formals_->appendEvaluatedCoreFormal( "values", Kernel::THE_VOID_VARIABLE );
      }

      virtual void
      call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
      {
        args.applyDefaults( callLoc );

        size_t argsi = 0;
        size_t keysi = argsi;
        RefCountPtr< const Lang::Value > keys = args.getValue( keysi );

        ++argsi;
        size_t valuesi = argsi;
        RefCountPtr< const Lang::Value > values = args.getValue( valuesi );

        Kernel::ContRef cont = Kernel::ContRef
          ( new Kernel::ForcingListContinuation
            ( Kernel::ContRef( new Kernel::Core_leiographicSort_cont_keys
                                ( values, args.getLoc( valuesi ), evalState->cont_, args.getLoc( keysi ) ) ),
              args.getLoc( keysi ),
              false /* don't force structures */,
              true /* consify */) );
        evalState->cont_ = cont;
        cont->takeValue( keys, evalState );
      }

      template< class T >
      static RefCountPtr< const Lang::SingleList > sortTyped( const RefCountPtr< const Lang::SingleList > & keys, const Ast::SourceLocation & keysLoc, const RefCountPtr< const Lang::SingleList > & values, const Ast::SourceLocation & valuesLoc );
      static RefCountPtr< const Lang::SingleList > sort( const RefCountPtr< const Lang::SingleList > & keys, const Ast::SourceLocation & keysLoc, const RefCountPtr< const Lang::SingleList > & values, const Ast::SourceLocation & valuesLoc );
    };

    class Core_mergeSort : public Lang::CoreFunction
    {
    public:
      Core_mergeSort( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name )
        : CoreFunction( ns, name, new Kernel::EvaluatedFormals( Ast::FileID::build_internal( name ), true ) )
      {
        formals_->appendEvaluatedCoreFormal( "values", Kernel::THE_SLOT_VARIABLE );
        formals_->appendEvaluatedCoreFormal( "precedes?", Kernel::THE_SLOT_VARIABLE );
      }

      virtual void
      call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
      {
        args.applyDefaults( callLoc );

        size_t argsi = 0;
        size_t valuesi = argsi;
        RefCountPtr< const Lang::Value > values = args.getValue( valuesi );

        ++argsi;
        size_t precedesi = argsi;
        typedef const Lang::Function PredType;
        RefCountPtr< PredType > precedes = Helpers::down_cast_CoreArgument< PredType >( id_, args, argsi, callLoc );

        Kernel::ContRef finishCont = Kernel::ContRef( new Kernel::Core_mergeSort_cont_finish( evalState->cont_, callLoc ) );

        Kernel::ContRef cont = Kernel::ContRef
          ( new Kernel::ForcingListContinuation
            ( Kernel::ContRef( new Kernel::Core_mergeSort_cont_sort( true, precedes, args.getLoc( precedesi ), evalState->dyn_, finishCont, callLoc ) ),
              args.getLoc( valuesi ),
              false /* don't sort structures */,
              true /* consify */ ) );
        evalState->cont_ = cont;
        cont->takeValue( values, evalState );
      }
    };

  }
}


void
Kernel::Core_leiographicSort_cont_values::takeValue( const RefCountPtr< const Lang::Value > & keysUntyped, Kernel::EvalState * evalState, bool dummy ) const
{
  typedef const Lang::SingleList ArgType;
  RefCountPtr< ArgType > values = Helpers::down_cast< ArgType >( keysUntyped, "< Internal error situation in Core_leiographicSort_cont_values >" );

  RefCountPtr< const Lang::SingleList > res = Lang::Core_lexiographicSort::sort( keys_, keysLoc_, values, traceLoc_ );
  evalState->cont_ = cont_;
  cont_->takeValue( res, evalState );
}

void
Kernel::Core_leiographicSort_cont_keys::takeValue( const RefCountPtr< const Lang::Value > & keysUntyped, Kernel::EvalState * evalState, bool dummy ) const
{
  typedef const Lang::SingleList ArgType;
  RefCountPtr< ArgType > keys = Helpers::down_cast< ArgType >( keysUntyped, "< Internal error situation in Core_leiographicSort_cont_keys >" );

  if( values_ == Lang::THE_VOID ){

    RefCountPtr< const Lang::SingleList > res = Lang::Core_lexiographicSort::sort( keys, traceLoc_, keys, traceLoc_ );
    evalState->cont_ = cont_;
    cont_->takeValue( res, evalState );
    return;

  }else{

    Kernel::ContRef cont = Kernel::ContRef
      ( new Kernel::ForcingListContinuation
        ( Kernel::ContRef( new Kernel::Core_leiographicSort_cont_values( keys, traceLoc_, cont_, valuesLoc_ ) ),
          valuesLoc_,
          false /* don't force structures */,
          true /* consify */ ) );
    evalState->cont_ = cont;
    cont->takeValue( values_, evalState );

  }
}

namespace Shapes
{
  namespace Lang
  {

    template< class T >
    struct ElementarySortItem
    {
      std::vector< T > key_;
      RefCountPtr< const Lang::Value > val_;

      ElementarySortItem( const RefCountPtr< const Lang::Value > & val, size_t keySize )
      : val_( val )
      {
        key_.reserve( keySize );
      }
    };

    template< class T >
    class ElementarySortItemLess
    {
    public:
      bool operator () ( const ElementarySortItem< T > * x, const ElementarySortItem< T > * y ) const
      {
        typedef typeof x->key_ VecType;
        typename VecType::const_iterator i = x->key_.begin( );
        typename VecType::const_iterator xEnd = x->key_.end( );
        typename VecType::const_iterator j = y->key_.begin( );
        typename VecType::const_iterator yEnd = y->key_.end( );
        for( ; i != xEnd && j != yEnd; ++i, ++j ){
          if( *i < *j )
            return true;
          if( *i > *j )
            return false;
        }
        return j != yEnd;
      }
    };

    template< class T >
    RefCountPtr< const Lang::SingleList >
    Lang::Core_lexiographicSort::sortTyped( const RefCountPtr< const Lang::SingleList > & keys, const Ast::SourceLocation & keysLoc, const RefCountPtr< const Lang::SingleList > & values, const Ast::SourceLocation & valuesLoc )
    {
      typedef T KeySymbolType;
      typedef ElementarySortItem< typename KeySymbolType::ValueType > SortItem;

      /* Initialize items to be sorted.
       */
      PtrOwner_back_Access< std::list< ElementarySortItem< typename KeySymbolType::ValueType > * > > itemsMem;

      RefCountPtr< const Lang::SingleListPair > k = keys.down_cast< const Lang::SingleListPair >( );
      RefCountPtr< const Lang::SingleListPair > v = values.down_cast< const Lang::SingleListPair >( );
      while( k != NullPtr< const Lang::SingleListPair >( ) ){
        if( v == NullPtr< const Lang::SingleListPair >( ) ){
          throw Exceptions::MiscellaneousRequirement( valuesLoc, "Values cannot be associated with keys since the number of elements differ." );
        }
        RefCountPtr< const Lang::VectorFunction > kvec = k->car_->getVal< const Lang::VectorFunction >( "Element in list of keys to be sorted." );
        RefCountPtr< const Lang::VectorFunction::vector_type > kmem = kvec->mem( );
        SortItem * item = new SortItem( v->car_->getUntyped( ), kmem->size( ) );
        itemsMem.push_back( item );
        typedef typeof *kmem VecType;
        for( typename VecType::const_iterator i = kmem->begin( ); i != kmem->end( ); ++i ){
          const KeySymbolType * sym = dynamic_cast< const KeySymbolType * >( i->getPtr( ) );
          if( sym == NULL ){
            throw Exceptions::TypeMismatch( keysLoc, "(element of key to be sorted, expected all to be of same type)", (*i)->getTypeName( ), KeySymbolType::staticTypeName( ) );
          }
          item->key_.push_back( sym->val_ );
        }
        k = k->cdr_.down_cast< const Lang::SingleListPair >( );
        v = v->cdr_.down_cast< const Lang::SingleListPair >( );
      }
      if( v != NullPtr< const Lang::SingleListPair >( ) ){
        throw Exceptions::MiscellaneousRequirement( valuesLoc, "Values cannot be associated with keys since the number of elements differ." );
      }

      /* Place items in vector and sort.
       */
      std::vector< SortItem * > items;
      items.reserve( itemsMem.size( ) );
      {
        typedef typeof itemsMem ListType;
        for( typename ListType::const_iterator i = itemsMem.begin( ); i != itemsMem.end( ); ++i ){
          items.push_back( *i );
        }
      }
      std::stable_sort( items.begin( ), items.end( ), ElementarySortItemLess< typename KeySymbolType::ValueType >( ) );

      /* Construct result.
       */
      RefCountPtr< const Lang::SingleList > result = Lang::THE_CONS_NULL;
      {
        typedef typeof items VecType;
        for( typename VecType::const_reverse_iterator i = items.rbegin( ); i != items.rend( ); ++i ){
          result = Helpers::SingleList_cons( (*i)->val_, result );
        }
      }

      return result;
    }

  }
}


RefCountPtr< const Lang::SingleList >
Lang::Core_lexiographicSort::sort( const RefCountPtr< const Lang::SingleList > & keys, const Ast::SourceLocation & keysLoc, const RefCountPtr< const Lang::SingleList > & values, const Ast::SourceLocation & valuesLoc )
{
  /* Currently, all keys must be of type Lang::VectorFunction, and we just need to find one
   * non-empty key in order to find the vector element type.
   */
  RefCountPtr< const Lang::Value > firstSymbol = RefCountPtr< const Lang::Value >( NullPtr< const Lang::Value >( ) );

  {
    size_t count = 0;
    RefCountPtr< const Lang::SingleListPair > k = keys.down_cast< const Lang::SingleListPair >( );
    while( k != NullPtr< const Lang::SingleListPair >( ) ){
      ++count;
      RefCountPtr< const Lang::VectorFunction > kvec = k->car_->getVal< const Lang::VectorFunction >( "Element in list of keys to be sorted." );
      RefCountPtr< const Lang::VectorFunction::vector_type > kmem = kvec->mem( );
      if( kmem->empty( ) ){
        k = k->cdr_.down_cast< const Lang::SingleListPair >( );
        continue;
      }
      firstSymbol = kmem->front( );
      break;
    }

    if( k == NullPtr< const Lang::SingleListPair >( ) ){
      /* All keys were empty, hence already in order.  We just need to check that the number of values match the number of keys. */
      RefCountPtr< const Lang::SingleListPair > v = values.down_cast< const Lang::SingleListPair >( );
      while( v != NullPtr< const Lang::SingleListPair >( ) ){
        if( count == 0){
          throw Exceptions::MiscellaneousRequirement( valuesLoc, "Values cannot be associated with keys since the number of elements differ." );
        }
        --count;
        v = v->cdr_.down_cast< const Lang::SingleListPair >( );
      }
      if( count != 0 ){
        throw Exceptions::MiscellaneousRequirement( valuesLoc, "Values cannot be associated with keys since the number of elements differ." );
      }
      return values;
    }
  }

  /* Dispatch based on the type of vector elements. */
  {
    typedef Lang::Integer KeySymbolType;
    if( dynamic_cast< const KeySymbolType * >( firstSymbol.getPtr( ) ) != NULL )
      return sortTyped< KeySymbolType >( keys, keysLoc, values, valuesLoc );
  }
  {
    typedef Lang::Float KeySymbolType;
    if( dynamic_cast< const KeySymbolType * >( firstSymbol.getPtr( ) ) != NULL )
      return sortTyped< KeySymbolType >( keys, keysLoc, values, valuesLoc );
  }

  throw Exceptions::TypeMismatch( keysLoc, "(element of key to be sorted)", firstSymbol->getTypeName( ), Helpers::typeSetString( Lang::Integer::staticTypeName( ), Lang::Float::staticTypeName( ) ) );
}


void
Kernel::Core_mergeSort_cont_sort::takeValue( const RefCountPtr< const Lang::Value > & valuesUntyped, Kernel::EvalState * evalState, bool dummy ) const
{
  typedef const Lang::SingleList ArgType;
  RefCountPtr< ArgType > valuesMaybeNil = Helpers::down_cast< ArgType >( valuesUntyped, "< Internal error situation in Core_mergeSort_cont_sort >" );

  RefCountPtr< const Lang::SingleListPair > values = valuesMaybeNil.down_cast< const Lang::SingleListPair >( );
  if( values == NullPtr< const Lang::SingleListPair >( ) ){
    /* Special case of empty input, never happens during recursion.
     */
    evalState->cont_ = cont_;
    /* The Kernel::THE_VOID_VARIABLE is just a dummy below.  It will just be discared by the receiving continuation. */
    cont_->takeValue( RefCountPtr< const Lang::SingleList >( new Lang::SingleListPair( Kernel::THE_VOID_VARIABLE, Lang::THE_CONS_NULL ) ),
                      evalState );
    return;
  }

  /* Split in half, with each part reversed. */
  RefCountPtr< const Lang::SingleListPair > p = values.down_cast< const Lang::SingleListPair >( );
  RefCountPtr< const Lang::SingleListPair > q = p;
  std::stack< Kernel::VariableHandle > leftReversed;
  while( q != NullPtr< const Lang::SingleListPair >( ) ){
    leftReversed.push( p->car_ );
    p = p->cdr_.down_cast< const Lang::SingleListPair >( );
    q = q->cdr_.down_cast< const Lang::SingleListPair >( );
    if( q == NullPtr< const Lang::SingleListPair >( ) )
      break;
    q = q->cdr_.down_cast< const Lang::SingleListPair >( );
  }
  if( p == NullPtr< const Lang::SingleListPair >( ) ){
    /* There was just a single element.
     * First and only element is at the same time the last element of the sorted sequence.
     */
    evalState->cont_ = cont_;
    cont_->takeValue( RefCountPtr< const Lang::SingleList >( new Lang::SingleListPair( values->car_, values ) ),
                      evalState );
    return;
  }

  /* There's at least one element in each part.
   */
  RefCountPtr< const Lang::SingleListPair > left =
    RefCountPtr< const Lang::SingleListPair >( new Lang::SingleListPair( leftReversed.top( ), Lang::THE_CONS_NULL ) );
  leftReversed.pop( );
  while( ! leftReversed.empty( ) ){
    left = RefCountPtr< const Lang::SingleListPair >( new Lang::SingleListPair( leftReversed.top( ), left ) );
    leftReversed.pop( );
  }
  RefCountPtr< const Lang::SingleListPair > right = p.down_cast< const Lang::SingleListPair >( );

  /* Now is the time to decide in which order to sort the left and right parts.  The goal is to be able to
   * shortcut the merge if values are sorted from the beginning.
   * The part sorted first will have only one end value available, while the part sorted second, just before
   * the merge begins, will have both first and last values available.  The order will depend on whether the merge
   * proceeds with values in increasing order or decreasing order.
   *
   * Decreasing order (reversed_):
   * Consider the input { 1 2 3 4 5 6 }.  It will be split and after sorting we will have the left part sorted as ( 3 ; 2 ; 1 )
   * and the right part sorted as ( 6 ; 5 ; 4 ).  The values that need to be compared are 3 and 4 (4 < 3 would mean that the merge
   * cannot be shortcut).  Since 4 is at the and of the sorted right part, this must be the second part to be sorted.
   * First: left, ( 3 ; 2 ; 1 ).  Second: right, ( 6 ; 5 ; 4 ).
   *
   * Increasing order (not reversed_):
   * Consider the input { 1 2 3 4 5 6 }.  It will be split and after sorting we will have the left part sorted as ( 1 ; 2 ; 3 )
   * and the right part sorted as ( 4 ; 5 ; 6 ).  The values that need to be compared are 3 and 4 (4 < 3 would mean that the merge
   * cannot be shortcut).  Since 3 is at the and of the sorted left part, this must be the second part to be sorted.
   * First: right, ( 4 ; 5 ; 6 ).  Second: left, ( 1 ; 2 ; 3 ).
   */
  if( reversed_ ){
    RefCountPtr< const Lang::SingleListPair > tmp = right;
    right = left;
    left = tmp;
  }

  /* Initiate sorting of first part.
   */
  Kernel::ContRef cont = Kernel::ContRef
    ( new Kernel::Core_mergeSort_cont_sort
      ( ! reversed_, precedes_, precedesLoc_, dyn_,
        Kernel::ContRef( new Kernel::Core_mergeSort_cont_sortSecond( reversed_, precedes_, precedesLoc_, left, dyn_, cont_, traceLoc_ ) ),
        traceLoc_ ) );
  evalState->cont_ = cont;
  cont->takeValue( right, evalState );
}

void
Kernel::Core_mergeSort_cont_sortSecond::takeValue( const RefCountPtr< const Lang::Value > & valUntyped, Kernel::EvalState * evalState, bool dummy ) const
{
  /* The value received by this continuation shall be a SingleListPair where
   *   cdr_ is the sorted first part, reversed
   *   car_ is the last element in the cdr_ sequence
   */
  typedef const Lang::SingleListPair ArgType;
  RefCountPtr< ArgType > val = Helpers::down_cast< ArgType >( valUntyped, "< Internal error situation in Core_mergeSort_cont_sortSecond::takeValue >" );

  RefCountPtr< ArgType > firstSorted = Helpers::down_cast< ArgType >( val->cdr_, "< Internal error situation in Core_mergeSort_cont_sortSecond::takeValue >" );

  /* Initiate sorting of second part.
   */
  Kernel::ContRef cont = Kernel::ContRef
    ( new Kernel::Core_mergeSort_cont_sort
      ( ! reversed_, precedes_, precedesLoc_, dyn_,
        Kernel::ContRef( new Kernel::Core_mergeSort_cont_merge
                          ( reversed_, precedes_, precedesLoc_, firstSorted, dyn_, cont_, traceLoc_ ) ),
        traceLoc_ ) );
  evalState->cont_ = cont;
  cont->takeValue( secondUnsorted_, evalState );
}

void
Kernel::Core_mergeSort_cont_merge::takeValue( const RefCountPtr< const Lang::Value > & valUntyped, Kernel::EvalState * evalState, bool dummy ) const
{
  /* The value received by this continuation shall be a SingleListPair where
   *   cdr_ is the sorted second part, reversed
   *   car_ is the last element in the cdr_ sequence
   */
  typedef const Lang::SingleListPair ArgType;
  RefCountPtr< ArgType > val = Helpers::down_cast< ArgType >( valUntyped, "< Internal error situation in Core_mergeSort_cont_merge::takeValue >" );

  RefCountPtr< ArgType > secondSorted = Helpers::down_cast< ArgType >( val->cdr_, "< Internal error situation in Core_mergeSort_cont_merge::takeValue >" );
  Kernel::VariableHandle lastInSecond = val->car_;

  static Ast::SourceLocation callbackLoc( Ast::FileID::build_internal( "< mergesort callback >" ) );

  evalState->cont_ = Kernel::ContRef( new Kernel::Core_mergeSort_cont_mergeShortcut( reversed_, precedes_, precedesLoc_, firstSorted_, secondSorted, dyn_, cont_, callbackLoc, traceLoc_ ) );
  evalState->dyn_ = dyn_;

  if( dynamic_cast< const Lang::UserFunction * >( precedes_.getPtr( ) ) == NULL ){
    /* Compare the use of RelaxContinuation in Core_mergeSort_cont_mergeStep::callback.
     */
    evalState->cont_ = Kernel::ContRef( new Kernel::RelaxContinuation( evalState->cont_ ) );
  }

  if( reversed_ )
    precedes_->call( precedes_, evalState, lastInSecond, firstSorted_->car_, callbackLoc );
  else
    precedes_->call( precedes_, evalState, firstSorted_->car_, lastInSecond, callbackLoc );
}

void
Kernel::Core_mergeSort_cont_mergeShortcut::takeValue( const RefCountPtr< const Lang::Value > & precUntyped, Kernel::EvalState * evalState, bool dummy ) const
{
  bool prec = Helpers::down_cast_ContinuationArgument< const Lang::Boolean >( precUntyped, this )->val_;

  if( prec ){

    /* First element in _second_ list precedes last element in _first_ list, cannot shortcut.
     * However, if both lists have only one element, we know how they shall be ordered.
     */
    if( dynamic_cast< const Lang::SingleListNull * >( firstSorted_->cdr_.getPtr( ) ) != NULL
        && dynamic_cast< const Lang::SingleListNull * >( secondSorted_->cdr_.getPtr( ) ) != NULL ){
      RefCountPtr< const Lang::SingleList > result = Lang::THE_CONS_NULL;
      result = RefCountPtr< const Lang::SingleList >( new Lang::SingleListPair( firstSorted_->car_, result ) );
      result = RefCountPtr< const Lang::SingleList >( new Lang::SingleListPair( secondSorted_->car_, result ) );
      evalState->cont_ = cont_;
      cont_->takeValue( RefCountPtr< const Lang::SingleList >( new Lang::SingleListPair( firstSorted_->car_, result ) ),
                        evalState );
      return;
    }else{
      Core_mergeSort_cont_mergeStep::callback( reversed_, precedes_, precedesLoc_, firstSorted_, secondSorted_, true,
                                               Lang::THE_CONS_NULL, Kernel::THE_SLOT_VARIABLE, dyn_, cont_, callLoc_,
                                               evalState );
      return;
    }

  }else{

    /* Otherwise, shortcut by just appending sequences in reverse and return. */
    RefCountPtr< const Lang::SingleListPair > p = secondSorted_;
    RefCountPtr< const Lang::SingleList > result = Lang::THE_CONS_NULL;
    Kernel::VariableHandle lastInResult = p->car_;
    while( p != NullPtr< const Lang::SingleListPair >( ) ){
      result = RefCountPtr< const Lang::SingleList >( new Lang::SingleListPair( p->car_, result ) );
      p = p->cdr_.down_cast< const Lang::SingleListPair >( );
    }
    p = firstSorted_;
    while( p != NullPtr< const Lang::SingleListPair >( ) ){
      result = RefCountPtr< const Lang::SingleList >( new Lang::SingleListPair( p->car_, result ) );
      p = p->cdr_.down_cast< const Lang::SingleListPair >( );
    }
    evalState->cont_ = cont_;
    cont_->takeValue( RefCountPtr< const Lang::SingleList >( new Lang::SingleListPair( lastInResult, result ) ),
                      evalState );
    return;

  }
}

void
Kernel::Core_mergeSort_cont_mergeStep::callback( bool reversed, RefCountPtr< const Lang::Function > precedes, const Ast::SourceLocation & precedesLoc, const RefCountPtr< const Lang::SingleListPair > & first, const RefCountPtr< const Lang::SingleListPair > & second, bool firstAtStart, RefCountPtr< const Lang::SingleList > reverseResult, const Kernel::VariableHandle & lastInResult, const Kernel::PassedDyn & dyn, const Kernel::ContRef & cont, const Ast::SourceLocation & callLoc, Kernel::EvalState * evalState )
{
  static Ast::SourceLocation callbackLoc( Ast::FileID::build_internal( "< mergesort callback >" ) );

  if( firstAtStart && dynamic_cast< const Lang::SingleListNull * >( second->cdr_.getPtr( ) ) != NULL ){
    /* We would not get here if it would have been possible to shortcut the merge.
     * This means that the first element in the first list is next in turn.
     */
    reverseResult = RefCountPtr< const Lang::SingleListPair >( new Lang::SingleListPair( first->car_, reverseResult ) );
    RefCountPtr< const Lang::SingleListPair > rest = first->cdr_.down_cast< const Lang::SingleListPair >( );

    if( rest == NullPtr< const Lang::SingleListPair >( ) ){
      /* Only one value in second remains. */

      reverseResult = RefCountPtr< const Lang::SingleList >( new Lang::SingleListPair( second->car_, reverseResult ) );
      evalState->cont_ = cont;
      cont->takeValue( RefCountPtr< const Lang::SingleList >( new Lang::SingleListPair( lastInResult, reverseResult ) ),
                       evalState );
      return;

    }else{

      /* Continue merge. */
      Core_mergeSort_cont_mergeStep::callback( reversed, precedes, precedesLoc, rest, second, false, reverseResult, lastInResult, dyn, cont, callLoc,
                                               evalState );
      return;

    }

  }

  evalState->cont_ = Kernel::ContRef( new Kernel::Core_mergeSort_cont_mergeStep( reversed, precedes, precedesLoc, first, second, firstAtStart, reverseResult, lastInResult, dyn, cont, callbackLoc, callLoc ) );
  evalState->dyn_ = dyn;

  if( dynamic_cast< const Lang::UserFunction * >( precedes.getPtr( ) ) == NULL ){
    /* We are not sure that calling precedes will allow the evaluation call chain to unwind.
     * Relax after precedes has produced a result.
     * (We could do more elaborate testing here to determine more exactly when we need to relax
     * the evaluation, what we have now is just a rough approximation that is expected to catch
     * the most important cases.  We just have to be sure that we don't fail to relax the evaluation
     * when needed -- better safe than sorry.)
     */
    evalState->cont_ = Kernel::ContRef( new Kernel::RelaxContinuation( evalState->cont_ ) );
  }

  /* Recall that the merge is constructing a reversed result.  That is, when in reversed mode, the resulting list is
   * actually increasing.  This means that we shall take the element from the right (!) part in case of a tie.
   * When not inreversed mode, it is the opposite since we are constructing a reversed result.
   *
   * Decreasing order (reversed):
   * First: left, ( 3 ; 2 ; 1 ).  Second: right, ( 6 ; 5 ; 4 ).
   * Thus, pick element from first only if 3 > 6, that is 6 < 3.
   *
   * Increasing order (not reversed):
   * First: right, ( 4 ; 5 ; 6 ).  Second: left, ( 1 ; 2 ; 3 ).
   * This, pick element from first only if 4 < 1.
   *
   * In both cases, the comparison is the same.
   */
  if( reversed )
    precedes->call( precedes, evalState, second->car_, first->car_, callbackLoc );
  else
    precedes->call( precedes, evalState, first->car_, second->car_, callbackLoc );
}


void
Kernel::Core_mergeSort_cont_mergeStep::takeValue( const RefCountPtr< const Lang::Value > & precUntyped, Kernel::EvalState * evalState, bool dummy ) const
{
  bool prec = Helpers::down_cast_ContinuationArgument< const Lang::Boolean >( precUntyped, this )->val_;

  if( prec ){

    /* Pick value from first.
     */
    RefCountPtr< const Lang::SingleListPair > reverseResult = RefCountPtr< const Lang::SingleListPair >( new Lang::SingleListPair( first_->car_, reverseResult_ ) );
    RefCountPtr< const Lang::SingleListPair > rest = first_->cdr_.down_cast< const Lang::SingleListPair >( );

    Kernel::VariableHandle lastInResult = (lastInResult_ == Kernel::THE_SLOT_VARIABLE) ? first_->car_ : lastInResult_;

    if( rest == NullPtr< const Lang::SingleListPair >( ) ){
      /* Only values in second remain. */

      RefCountPtr< const Lang::SingleListPair > p = second_;
      while( p != NullPtr< const Lang::SingleListPair >( ) ){
        reverseResult = RefCountPtr< const Lang::SingleList >( new Lang::SingleListPair( p->car_, reverseResult ) );
        p = p->cdr_.down_cast< const Lang::SingleListPair >( );
      }
      evalState->cont_ = cont_;
      cont_->takeValue( RefCountPtr< const Lang::SingleList >( new Lang::SingleListPair( lastInResult, reverseResult ) ),
                        evalState );
      return;

    }else{

      /* Continue merge. */
      Core_mergeSort_cont_mergeStep::callback( reversed_, precedes_, precedesLoc_, rest, second_, firstAtStart_, reverseResult, lastInResult, dyn_, cont_, callLoc_,
                                               evalState );
      return;

    }

  }else{

    /* Pick value from second.
     */
    RefCountPtr< const Lang::SingleListPair > reverseResult = RefCountPtr< const Lang::SingleListPair >( new Lang::SingleListPair( second_->car_, reverseResult_ ) );
    RefCountPtr< const Lang::SingleListPair > rest = second_->cdr_.down_cast< const Lang::SingleListPair >( );

    Kernel::VariableHandle lastInResult = (lastInResult_ == Kernel::THE_SLOT_VARIABLE) ? second_->car_ : lastInResult_;

    if( rest == NullPtr< const Lang::SingleListPair >( ) ){
      /* Only values in first remain. */

      RefCountPtr< const Lang::SingleListPair > p = first_;
      while( p != NullPtr< const Lang::SingleListPair >( ) ){
        reverseResult = RefCountPtr< const Lang::SingleList >( new Lang::SingleListPair( p->car_, reverseResult ) );
        p = p->cdr_.down_cast< const Lang::SingleListPair >( );
      }
      evalState->cont_ = cont_;
      cont_->takeValue( RefCountPtr< const Lang::SingleList >( new Lang::SingleListPair( lastInResult, reverseResult ) ),
                        evalState );
      return;

    }else{

      /* Continue merge. */
      Core_mergeSort_cont_mergeStep::callback( reversed_, precedes_, precedesLoc_, first_, rest, false, reverseResult, lastInResult, dyn_, cont_, callLoc_,
                                               evalState );
      return;

    }

  }
}

void
Kernel::Core_mergeSort_cont_finish::takeValue( const RefCountPtr< const Lang::Value > & valUntyped, Kernel::EvalState * evalState, bool dummy ) const
{
  /* The value received by this continuation shall be a SingleListPair where
   *   cdr_ is the sorted sequence, reversed
   *   car_ is the last element in the cdr_ sequence
   */
  typedef const Lang::SingleListPair ArgType;
  RefCountPtr< ArgType > val = Helpers::down_cast< ArgType >( valUntyped, "< Internal error situation in Core_mergeSort_cont_finish::takeValue >" );

  evalState->cont_ = cont_;
  cont_->takeValue( val->cdr_, evalState );
}


void
Kernel::registerCore_sort( Kernel::Environment * env )
{
  env->initDefineCoreFunction( new Lang::Core_lexiographicSort( Lang::THE_NAMESPACE_Shapes_Data, "lexiographicSort" ) );
  env->initDefineCoreFunction( new Lang::Core_mergeSort( Lang::THE_NAMESPACE_Shapes_Data, "sort" ) );
}

