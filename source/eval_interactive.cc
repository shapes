/* This file is part of Shapes.
 *
 * Shapes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Shapes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2008, 2009, 2014 Henrik Tidefelt
 */

#include "main.h"
#include "astvar.h"
#include "breakpoints.h"
#include "shapescore.h"

#include <typeinfo>

using namespace Shapes;
using namespace SimplePDF;

namespace Shapes
{
	namespace Ast
	{
		class VoidExpression : public Expression
		{
		public:
			VoidExpression( const Ast::SourceLocation & loc );
			virtual ~VoidExpression( );
			virtual void analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst );
			virtual void eval( Kernel::EvalState * evalState ) const;
		};
	}

	namespace Kernel
	{
		bool theInteractionDone = false;

		class InteractionContinuation : public Kernel::Continuation
		{
			Ast::AnalysisEnvironment * parentAnalysisEnv_;
			mutable Ast::CodeBracket *	bracket_;
			mutable Kernel::Environment * env_;
			Kernel::ContRef resume_;
			Kernel::PassedDyn dyn_;
			Ast::Expression * breakExpr_;
			int result_inputNo_;
			std::istream & is_;
			std::ostream & os_;
			std::string outputName_;
			SplitMode splitMode_;
		public:
			InteractionContinuation( Ast::AnalysisEnvironment * parentAnalysisEnv, Ast::CodeBracket *	bracket, Kernel::Environment * env, Kernel::ContRef resume, const Kernel::PassedDyn & dyn, Ast::Expression * breakExpr, int result_inputNo, std::istream & is, std::ostream & os, const std::string & outputName, SplitMode splitMode, const Ast::SourceLocation & traceLoc );
			InteractionContinuation( Kernel::EvalState & evalState, int result_inputNo, std::istream & is, std::ostream & os, const std::string & outputName, SplitMode splitMode ); // set up new dynamic and lexical environment for debugging an error at evalState
			~InteractionContinuation( );
			RefCountPtr< Kernel::InteractionContinuation > cloneWithNewContinuation( Kernel::ContRef resume, Kernel::WarmDebugger * state );
			void setDyn( const Kernel::PassedDyn & dyn );
			bool reusable( Kernel::EvalState & evalState ) const;
			virtual void takeValue( const RefCountPtr< const Lang::Value > & val, Kernel::EvalState * evalState, bool dummy ) const;
			virtual Kernel::ContRef up( ) const;
			virtual RefCountPtr< const char > description( ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );

			Kernel::ContRef resume( ) const { return resume_; } /* For debug backtraces and to skip continuations. */
		private:
			void showValue( const RefCountPtr< const Lang::Value > & resultUntyped ) const;
		};

		class ExitInteractionContinuation : public Kernel::Continuation
		{
			bool * done_;
			int * exitCode_;
		public:
			ExitInteractionContinuation( bool * done, int * exitCode, const Ast::SourceLocation & traceLoc );
			~ExitInteractionContinuation( );
			virtual void takeValue( const RefCountPtr< const Lang::Value > & val, Kernel::EvalState * evalState, bool dummy ) const;
			virtual Kernel::ContRef up( ) const;
			virtual RefCountPtr< const char > description( ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
		};

		class DebugStepInfo
		{
		public:
			typedef enum { EXPR, STEP } StepMode;

			bool active_;
			StepMode mode_;
			Ast::SourceLocation refLoc_; /* Note: Not a const reference.  Will be reassigned.  Make sure not to initialize const SourceLocation & references with this value! */
			size_t count_;
			DebugStepInfo( )
				: active_( false ), mode_( EXPR ), refLoc_( Ast::THE_UNKNOWN_LOCATION, bool( ) ), count_( 1 )
			{ }
			bool stop( const Ast::SourceLocation & loc )
			{
				switch( mode_ )
					{
					case EXPR:
						/* Stop at everything.  No need to update refLoc_ . */
						break;
					case STEP:
						{
							if( refLoc_.contains( loc ) )
								{
									return false;
								}
							refLoc_ = loc;
						}
						break;
					}
				--count_;
				if( count_ == 0 )
					{
						active_ = false;
						return true;
					}
				return false;
			}
		};

		const int NO_INPUT_INPUT_NO = std::numeric_limits< int >::max( );
		bool interactive_debug = true;
		typedef typeof Ast::theInteractiveInput InputType;
		std::vector< InputType > interactive_history;
		std::set< Ast::Expression * > theBreakpoints; /* This set shall contain all expressions that have a Kernel::BreakpointInfo, incloding also those thare are not enabled. */
		std::set< Ast::Expression * > theTemporaryBreakpoints; /* This set shall be a subset of theBreakpoints, and contain the breakpoints that should be reset when evaluation is interrupted. */
		DebugStepInfo theDebugStepInfo;
		Ast::VoidExpression * theVoidExpression = new Ast::VoidExpression( Ast::THE_UNKNOWN_LOCATION ); /* Order of initializaiton of globals?! */
	}

	namespace Lang
	{
		const char * CONTINUATION_TOP_REPL_ID = "top_repl";
	}
}

void
interactiveEvaluation( std::istream & in, std::ostream & out, const std::string & outputName, SplitMode splitMode,
											 bool evalTrace, bool evalBackTrace, bool cleanupMemory,
											 RefCountPtr< std::ifstream > & labelDBFile, const std::string & labelDBName )
{
	if( ! Interaction::blankMode ){
		/* Clean up outout from earlier sessions. */
		switch( splitMode )
			{
			case SPLIT_NO:
				break;
			case SPLIT_FLAT:
				{
					rmSplitFiles( outputName, "-" );
				}
				break;
			case SPLIT_DIR:
				{
					mkSplitDir( outputName );
					rmSplitFiles( outputName, "/" );
				}
				break;
			}
	}

	RefCountPtr< const Kernel::GraphicsState > graphicsState( new Kernel::GraphicsState( true ) );
	Kernel::PassedDyn baseDyn( new Kernel::DynamicEnvironment( graphicsState ) );

	try
		{
			/* Evaluate preamble. */
			shapesparse( );
			initCore( labelDBFile, labelDBName );

			bool done = false;
			Kernel::ValueRef resultUntyped = NullPtr< const Lang::Value >( );
			Kernel::EvalState evalState( Ast::theProgram,
																	 Kernel::theGlobalEnvironment,
																	 baseDyn,
																	 Kernel::ContRef( new Kernel::StoreValueContinuation( & resultUntyped,
																																												Kernel::ContRef( new Kernel::ExitContinuation( & done, Ast::theProgram->loc( ) ) ),
																																												Ast::theProgram->loc( ) ) ) );
			while( ! done )
				{
					Ast::Expression * expr = evalState.expr_;
					expr->eval( & evalState );
				}
		}
	catch( const Exceptions::StaticInconsistency & ball )
		{
			std::cout.flush( );
			std::cerr << ball.loc( ) << ": " ;
			ball.display( std::cerr );
			std::cerr << "There were errors in the preamble.  Aborting interactive evaluation." ;
			abortProcedure( ball.exitCode( ) );
		}
	catch( const Exceptions::Exception & ball )
		{
			std::cout.flush( );
			ball.display( std::cerr );
			std::cerr << "There were errors in the preamble.  Aborting interactive evaluation." ;
			abortProcedure( ball.exitCode( ) );
		}

	Ast::theShapesScanner.setInteractive( true ); /* Do this after reading the preamble! */

	/* Find the prelude environment... */
	Kernel::Environment * preludeEnv = 0;
	for( std::list< Kernel::Environment * >::iterator e = Kernel::theEnvironmentList.begin( ); e != Kernel::theEnvironmentList.end( ); ++e )
		{
			try
				{
					if( (*e)->getParent( ) == Kernel::theGlobalEnvironment )
						{
							preludeEnv = *e;
							break;
						}
				}
			catch( const Exceptions::MiscellaneousRequirement & ball )
				{
					/* That was the global environment, which doesn't have a parent.  Never mind. */
				}
		}
	if( preludeEnv == 0 )
		{
			std::cerr << "Internal error: Failed to locate prelude environment." << std::endl ;
			exit( Interaction::EXIT_INTERNAL_ERROR );
		}
	Ast::AnalysisEnvironment * preludeAnalysisEnvironment = preludeEnv->newAnalysisEnvironment( Ast::theGlobalAnalysisEnvironment );

	static Ast::SourceLocation loc( Ast::FileID::build_internal( "{ interactive }" ) );
	Ast::CodeBracket * initialBracket( new Ast::CodeBracket( loc, new std::list< Ast::Node * >( ) ) );
	{
		Ast::StateIDSet freeStates;
		initialBracket->analyze( Ast::theProgram, preludeAnalysisEnvironment, & freeStates );
	}
	Kernel::Environment * bracketEnv = initialBracket->newEnvironment( preludeEnv, "Interaction" );
	bracketEnv->initDefine( Lang::THE_NAMESPACE_Shapes_Debug, "db", new Kernel::WarmDebugger( bracketEnv, baseDyn, Kernel::ContRef( NullPtr< Kernel::Continuation >( ) ), 0 ) );

	int exitCode = Interaction::EXIT_OK;

	RefCountPtr< Kernel::InteractionContinuation > interactionCont
		( new Kernel::InteractionContinuation( preludeAnalysisEnvironment, initialBracket, bracketEnv, Kernel::ContRef( NullPtr< Kernel::Continuation >( ) ), baseDyn, 0, Kernel::NO_INPUT_INPUT_NO,
																					 in, out, outputName, splitMode,
																					 initialBracket->loc( ) ) );
	Kernel::PassedDyn interactionDyn( new Kernel::DynamicEnvironment( Kernel::PassedDyn( new Kernel::DynamicEnvironment( baseDyn, "bye", Kernel::ContRef( new Kernel::ExitInteractionContinuation( & Kernel::theInteractionDone, & exitCode, initialBracket->loc( ) ) ) ) ),
																																		Lang::CONTINUATION_TOP_REPL_ID, interactionCont ) );
	interactionCont->setDyn( interactionDyn );

	Kernel::EvalState evalState( Kernel::theVoidExpression, /* This will invoke the continuation first thing. */
															 Kernel::theGlobalEnvironment, /* Dummy value */
															 baseDyn, /* Dummy value */
															 interactionCont );

	while( ! Kernel::theInteractionDone )
		{
			Ast::Expression * expr = evalState.expr_;
			if( Kernel::theDebugStepInfo.active_ && Kernel::theDebugStepInfo.stop( expr->loc( ) ) )
				{
					out << "Reached " << expr->loc( ) << "." << std::endl ;
					evalState.cont_ = Kernel::ContRef( new Kernel::InteractionContinuation( evalState, Kernel::NO_INPUT_INPUT_NO, in, out, outputName, splitMode ) );
					evalState.expr_ = Kernel::theVoidExpression;
					Kernel::theDebugStepInfo.active_ = false;
					continue;
				}
			if( expr->breakpoint_ != 0 && expr->breakpoint_->enabled_ )
				{
					if(	expr->breakpoint_->ignoreCount_ > 0 )
						{
							/* Don't break this time, but re-enable breaking in the future. */
							--(expr->breakpoint_->ignoreCount_);
						}
					else
						{
							out << "Stopping at breakpoint " << expr->loc( ) << "." << std::endl ;
							typedef typeof Kernel::theTemporaryBreakpoints SetType;
							for( SetType::iterator i = Kernel::theTemporaryBreakpoints.begin( ); i != Kernel::theTemporaryBreakpoints.end( ); ++i )
								{
									if( (*i)->breakpoint_->createdForTemporaryUse_ )
										{
											delete (*i)->breakpoint_;
											(*i)->breakpoint_ = 0;
											Kernel::theBreakpoints.erase( *i );
										}
									else
										{
											(*i)->breakpoint_->enabled_ = (*i)->breakpoint_->persistentEnabled_;
										}
								}
							Kernel::theTemporaryBreakpoints.clear( );
							evalState.cont_ = Kernel::ContRef( new Kernel::InteractionContinuation( evalState, Kernel::NO_INPUT_INPUT_NO, in, out, outputName, splitMode ) );
							evalState.expr_ = Kernel::theVoidExpression;
							Kernel::theDebugStepInfo.active_ = false;
							continue;
						}
				}

			try
				{
					try
						{
							expr->eval( & evalState );
						}
					catch( const Exceptions::CatchableError & ball )
						{
							Kernel::ContRef cont = evalState.dyn_->getEscapeContinuation( Lang::CONTINUATION_ID_ERROR, ball.getLoc( ) );
							cont->takeValue( ball.clone( evalState.cont_ ),
															 & evalState );
						}
					continue;
				}
			catch( const Exceptions::StaticInconsistency & ball )
				{
					out.flush( );
					out << ball.loc( ) << ": " ;
					ball.display( out );
				}
			catch( const Exceptions::Exception & ball )
				{
					out.flush( );
					if( Interaction::debugBacktrace )
						{
							evalState.cont_->backTrace( out );
						}

					try
						{
							const Exceptions::RuntimeError & rerr = dynamic_cast< const Exceptions::RuntimeError & >( ball );
							if( rerr.getLoc( ).isUnknown( ) )
								{
									throw std::bad_cast( ); /* It's not a bad cast, but the cast value is just as little useful as a bad cast. */
								}
							out << rerr.getLoc( ) << Exceptions::Exception::locsep ;
						}
					catch( std::bad_cast & bad_cast_ball )
						{
							out << evalState.cont_->traceLoc( ) << Exceptions::Exception::locsep ;
						}
					ball.display( out );
				}
			if( Kernel::interactive_debug )
				{
					Kernel::InteractionContinuation * iCont = dynamic_cast< Kernel::InteractionContinuation * >( evalState.cont_.getPtr( ) );
					if( iCont != 0 && iCont->reusable( evalState ) )
						{
							/* We don't create a new interactive continuation if the active continuation is already an interactive continuation.
							 * For instance, failing to set a breakpoint is not something that's interesting to debug in itself.
							 */
							out << "Reusing interactive continuation." << std::endl ;
						}
					else if( evalState.expr_ != 0 )
						{
							evalState.cont_ = Kernel::ContRef( new Kernel::InteractionContinuation( evalState, Kernel::NO_INPUT_INPUT_NO, in, out, outputName, splitMode ) );
						}
					else
						{
							/* We interpret this situation as a failure to explicitly apply a continuation.
							 * See ContinueDynamicECFunction::call .
							 */
							evalState.cont_ = Kernel::ContRef( new Kernel::InteractionContinuation
																								 ( preludeAnalysisEnvironment, initialBracket, bracketEnv, evalState.cont_,
																									 Kernel::PassedDyn( new Kernel::DynamicEnvironment( baseDyn, "resume", evalState.cont_ ) ),
																									 0,
																									 Kernel::NO_INPUT_INPUT_NO,
																									 in, out, outputName, splitMode,
																									 initialBracket->loc( ) ) );
						}
				}
			else
				{
					evalState.cont_ = interactionCont;
				}
			Kernel::theDebugStepInfo.active_ = false;
			evalState.expr_ = Kernel::theVoidExpression;
		}

	/* Someone ended the input stream or invoked the exit continuation.
	 * For now, we circumvent the normal termination procedures in <main> by exiting here,
	 * which is similar to stopping the program by pressing ctrl-c, which is probably what
	 * most people will do anyway.
	 */
	exit( exitCode );
}


Interaction::InteractionFormats::InteractionFormats( )
	: prompt_( "[%!#!!3R] " ),
		show_( "%!#!!3R ⇒  " ),
		file_( "#File: " ),
		bye_( "<<EOF>>\n" )
{ }

void
Interaction::InteractionFormats::setPrompt( const char * format )
{
	prompt_ = format;
}

void
Interaction::InteractionFormats::setShow( const char * format )
{
	show_ = format;
}

void
Interaction::InteractionFormats::setFile( const char * format )
{
	file_ = format;
}

void
Interaction::InteractionFormats::setBye( const char * format )
{
	bye_ = format;
}

void
Interaction::InteractionFormats::formatPrompt( std::ostream & os, size_t inputNo ) const
{
	writePrompt( os, prompt_, inputNo );
}

void
Interaction::InteractionFormats::formatShow( std::ostream & os, size_t inputNo ) const
{
	writePrompt( os, show_, inputNo );
}

void
Interaction::InteractionFormats::formatFile( std::ostream & os, size_t inputNo ) const
{
	writePrompt( os, file_, inputNo );
}

void
Interaction::InteractionFormats::formatBye( std::ostream & os, size_t inputNo ) const
{
	writePrompt( os, bye_, inputNo );
}

void
Interaction::InteractionFormats::writePrompt( std::ostream & os, const char * format, size_t inputNo ) const
{
	for( const char * src = format; *src != '\0'; ++src )
		{
			if( *src == '%' )
				{
					++src;
					const char sep = *src;
					++src;
					std::ostringstream noStr;
					while( *src != sep )
						{
							if( *src == '\0' )
								{
									std::cerr << "Bad interactive prompt format string (expected second '" << sep << "'): " << format << std::endl ;
									exit( Interaction::EXIT_INVOCATION_ERROR );
								}
							if( *src == '\\' )
								{
									++src;
									writeEscape( noStr, *src );
								}
							else
								{
									noStr << *src ;
								}
							++src;
						}
					++src;
					noStr << inputNo ;
					while( *src != sep )
						{
							if( *src == '\0' )
								{
									std::cerr << "Bad interactive prompt format string (expected third '" << sep << "'): " << format << std::endl ;
									exit( Interaction::EXIT_INVOCATION_ERROR );
								}
							if( *src == '\\' )
								{
									++src;
									writeEscape( noStr, *src );
								}
							else
								{
									noStr << *src ;
								}
							++src;
						}
					++src;
					char * endp;
					int width = strtol( src, & endp, 10 );
					if( endp == src )
						{
							width = -1;
						}
					else if( width < 0 || width > 10 )
						{
							std::cerr << "Bad interactive prompt format string (the width " << width << " is out of the range [0,10]): " << format << std::endl ;
							exit( Interaction::EXIT_INVOCATION_ERROR );

						}
					src = endp;
					switch( *src )
						{
						case 'L':
							os.setf(std::ios::left);
							break;
						case 'R':
							os.setf(std::ios::right);
							break;
						default:
							std::cerr << "Bad interactive prompt format string (expected 'L' or 'R' after '%'): " << format << std::endl ;
							exit( Interaction::EXIT_INVOCATION_ERROR );
						}
					if( width >= 0 )
						{
							os << std::setw(width) ;
						}
					os << noStr.str( ) ;
				}
			else if( *src == '\\' )
				{
					++src;
					writeEscape( os, *src );
				}
			else
				{
					os << *src ;
				}
		}
}

void
Interaction::InteractionFormats::writeEscape( std::ostream & os, char c ) const
{
	switch( c )
		{
		case '\\':
			os << '\\';
			break;
		case '%':
			os << '%';
			break;
		case 'a':
			os << '\a';
			break;
		case 'n':
			os << '\n';
			break;
		case 't':
			os << '\t';
			break;
		default:
			os << '\\' << c ;
		}
}

Ast::VoidExpression::VoidExpression( const Ast::SourceLocation & loc )
	: Ast::Expression( loc )
{ }

Ast::VoidExpression::~VoidExpression( )
{ }

void
Ast::VoidExpression::analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst )
{
	/* Nothing to do! */
}

void
Ast::VoidExpression::eval( Kernel::EvalState * evalState ) const
{
	Kernel::ContRef cont = evalState->cont_;
	cont->takeValue( Lang::THE_VOID,
									 evalState );
}

Kernel::InteractionContinuation::InteractionContinuation( Ast::AnalysisEnvironment * parentAnalysisEnv, Ast::CodeBracket * bracket, Kernel::Environment * env, Kernel::ContRef resume, const Kernel::PassedDyn & dyn, Ast::Expression * breakExpr, int result_inputNo, std::istream & is, std::ostream & os, const std::string & outputName, SplitMode splitMode, const Ast::SourceLocation & traceLoc )
	: Kernel::Continuation( traceLoc ), parentAnalysisEnv_( parentAnalysisEnv ), bracket_( bracket ), env_( env ), resume_( resume ), dyn_( dyn ), breakExpr_( breakExpr ), result_inputNo_( result_inputNo ), is_( is ), os_( os ), outputName_( outputName ), splitMode_( splitMode )
{ }

Ast::SourceLocation debug_loc( Ast::FileID::build_internal( "{ debug }" ) );

Kernel::InteractionContinuation::InteractionContinuation( Kernel::EvalState & evalState, int result_inputNo, std::istream & is, std::ostream & os, const std::string & outputName, SplitMode splitMode )
	: Kernel::Continuation( evalState.expr_->loc( ) ),
		parentAnalysisEnv_( evalState.expr_->getEnv( ) ),
		bracket_( new Ast::CodeBracket( debug_loc, new std::list< Ast::Node * >( ) ) ),
		env_( bracket_->newEnvironment( evalState.env_, "Debug" ) ),
		resume_( evalState.cont_ ),
		dyn_( new Kernel::DynamicEnvironment( evalState.dyn_, "resume", resume_ ) ),
		breakExpr_( evalState.expr_ ),
		result_inputNo_( result_inputNo ),
		is_( is ), os_( os ), outputName_( outputName ), splitMode_( splitMode )
{
	{
		Ast::StateIDSet freeStates;
		bracket_->analyze( evalState.expr_, parentAnalysisEnv_, & freeStates );
	}
	env_->initDefine( Lang::THE_NAMESPACE_Shapes_Debug, "db", new Kernel::WarmDebugger( env_, dyn_, resume_, breakExpr_ ) );
	env_->initDefineHandle( Ast::PlacedIdentifier( Lang::THE_NAMESPACE_Shapes_Debug, "expr" ), Kernel::VariableHandle( new Kernel::Variable( new Kernel::Thunk( evalState.env_, evalState.dyn_, evalState.expr_ ) ) ) );
}

Kernel::InteractionContinuation::~InteractionContinuation( )
{ }

RefCountPtr< Kernel::InteractionContinuation >
Kernel::InteractionContinuation::cloneWithNewContinuation( Kernel::ContRef resume, Kernel::WarmDebugger * state )
{
	state->env_ = env_;
	state->dyn_ = Kernel::PassedDyn( new Kernel::DynamicEnvironment( dyn_, "resume", resume ) );
	state->breakExpr_ = breakExpr_;
	state->resume_ = resume;
	return RefCountPtr< Kernel::InteractionContinuation >
		( new Kernel::InteractionContinuation( parentAnalysisEnv_, bracket_, state->env_, resume,
																					 state->dyn_,
																					 state->breakExpr_,
																					 NO_INPUT_INPUT_NO,
																					 is_, os_, outputName_, splitMode_,
																					 traceLoc_ ) );
}

void
Kernel::InteractionContinuation::setDyn( const Kernel::PassedDyn & dyn )
{
	dyn_ = dyn;
}

bool
Kernel::InteractionContinuation::reusable( Kernel::EvalState & evalState ) const
{
	return env_ == evalState.env_ && dyn_ == evalState.dyn_;
}

void
Kernel::InteractionContinuation::takeValue( const RefCountPtr< const Lang::Value > & val, Kernel::EvalState * evalState, bool dummy ) const
{
	Kernel::theDebugStepInfo.active_ = false;
	showValue( val );

	size_t debugDepth = 0;
	{
		Kernel::ContRef tmp = evalState->cont_;
		while( tmp != NullPtr< Kernel::Continuation >( ) )
			{
				RefCountPtr< Kernel::InteractionContinuation > inter = tmp.down_cast< Kernel::InteractionContinuation >( );
				if( inter != NullPtr< Kernel::InteractionContinuation >( ) )
					{
						tmp = inter->resume_;
						if( tmp != NullPtr< Kernel::Continuation >( ) )
							{
								++debugDepth;
							}
					}
				else
					{
						tmp = tmp->up( );
					}
			}
	}

	while( true )
		{
			/* As long as we fail to return, we ask for new input... */
			if( debugDepth > 0 )
				{
					os_ << "[" ;
					for( size_t i = 0; i < debugDepth; ++i )
						{
							if( i > 0 )
								{
									os_ << "," ;
								}
							os_ << "debug" ;
						}
					os_ << "]" ;
				}
			const Ast::FileID * fileID = Ast::FileID::build_fresh_inMemory( );
			int interactive_inputNo = fileID->inMemoryIndex( );
			std::ostringstream inputBuf;
			while( true )
				{
					interactionFormats.formatPrompt( os_, interactive_inputNo );
					std::string line;
					std::getline( is_, line );
					if( is_.eof( ) )
						{
							interactionFormats.formatBye( os_, interactive_inputNo );
							Kernel::theInteractionDone = true;
							return;
						}
					if( line.length( ) > 1 && line[ line.length( ) - 1] == ' ' )
						{
							inputBuf << line.substr( 0, line.length( ) - 1 ) << '\n' ;
						}
					else
						{
							inputBuf << line ;
							break;
						}
				}
			std::string line = inputBuf.str( );
			if( line.length( ) > 1 && line[0] == ':' )
				{
					line = "#db.[" + line.substr(1) + "]";
				}

			const_cast< Ast::FileID * >( fileID )->setMem( strdup( line.c_str( ) ) );
			std::istringstream inLine( line + "\n" );
			Ast::theShapesScanner.queueStream( & inLine, fileID );
			Ast::theShapesScanner.start( );
			try
				{
					shapesparse( );
					interactive_history.push_back( Ast::theInteractiveInput );
					Ast::CodeBracket * extension = new Ast::CodeBracket( bracket_->loc( ), Ast::theInteractiveInput, bracket_ );
					{
						Ast::StateIDSet freeStates;
						extension->analyze( Ast::theProgram, parentAnalysisEnv_, & freeStates );
					}
					if( ! Ast::theAnalysisErrorsList.empty( ) )
						{
							std::cout.flush( );
							typedef typeof Ast::theAnalysisErrorsList ListType;
							for( ListType::const_iterator i = Ast::theAnalysisErrorsList.begin( ); i != Ast::theAnalysisErrorsList.end( ); ++i )
								{
									{
										typedef const Exceptions::StaticInconsistency ErrorType;
										ErrorType * err = dynamic_cast< ErrorType * >( *i );
										if( err != 0 )
											{
												std::cerr << err->loc( ) << ": " ;
												err->display( std::cerr );
												continue;
											}
									}
									std::cerr << "(Bad exception type)" << ": " ;
									(*i)->display( std::cerr );
								}
							Ast::theAnalysisErrorsList.clear( );
						}
					else
						{
							try
								{
									// evalState->env_ = env_; /* Not needed since extension->eval handles this. */
									evalState->dyn_ = dyn_;
									/* For the continuation, we make a copy of ourselves. */
									evalState->cont_ = Kernel::ContRef( new Kernel::InteractionContinuation( parentAnalysisEnv_, bracket_, env_, resume_, dyn_, breakExpr_,
																																													 interactive_inputNo,
																																													 is_, os_, outputName_, splitMode_,
																																													 traceLoc_ ) );
									extension->eval( evalState, env_ ); /* Note that this is a special kind of eval that extends the given environment. */
								}
							catch( const Exceptions::CatchableError & ball )
								{
									Kernel::ContRef cont = evalState->dyn_->getEscapeContinuation( Lang::CONTINUATION_ID_ERROR, ball.getLoc( ) );
									cont->takeValue( ball.clone( evalState->cont_ ),
																	 evalState );
								}
							return;
						}
				}
			catch( const Exceptions::StaticInconsistency & ball )
				{
					os_.flush( );
					os_ << ball.loc( ) << ": " ;
					ball.display( os_ );
				}
			catch( const Exceptions::Exception & ball )
				{
					os_.flush( );
					ball.display( os_ );
				}
		}
}

Kernel::ContRef
Kernel::InteractionContinuation::up( ) const
{
	return Kernel::ContRef( NullPtr< Kernel::Continuation >( ) );
}

RefCountPtr< const char >
Kernel::InteractionContinuation::description( ) const
{
	if( resume_ != NullPtr< Kernel::Continuation >( ) )
		{
			return strrefdup( "interaction with resume" );
		}
	else
		{
			return strrefdup( "interaction" );
		}
}

void
Kernel::InteractionContinuation::gcMark( Kernel::GCMarkedSet & marked )
{ }

void
Kernel::InteractionContinuation::showValue( const RefCountPtr< const Lang::Value > & resultUntyped ) const
{
	try
		{
			typedef const Lang::Void ResType;
			RefCountPtr< ResType > result = Helpers::try_cast_CoreArgument< ResType >( resultUntyped );
			/* Just be quiet. */
			return;
		}
	catch( const NonLocalExit::NotThisType & ball )
		{
			/* Wrong type; never mind!.. but see below!
			 */
		}
	try
		{
			if( Interaction::blankMode ){
				/* Skip special handling of Lang::Drawable2D when running the compiler in blank mode. */
				throw NonLocalExit::NotThisType( );
			}
			typedef const Lang::Drawable2D ResType;
			RefCountPtr< ResType > result = Helpers::try_cast_CoreArgument< ResType >( resultUntyped );
			Kernel::WarmCatalog catalog;
			catalog.tackOnPage( dyn_, result, Ast::THE_UNKNOWN_LOCATION );
			Kernel::WarmCatalog::ShipoutList documents;
			catalog.shipout( splitMode_ != SPLIT_NO, & documents );
			std::ofstream oFile;
			std::string filename;
			switch( splitMode_ )
				{
				case SPLIT_NO:
					{
						noSplitOpen( & oFile, outputName_ );
						filename = outputName_;
					}
					break;
				case SPLIT_FLAT:
					{
						filename = splitOpen( & oFile, outputName_, "-", result_inputNo_ );
					}
					break;
				case SPLIT_DIR:
					{
						filename = splitOpen( & oFile, outputName_, "/", result_inputNo_ );
					}
					break;
				}
			documents.front( ).writeFile( oFile, Kernel::the_PDF_version );
			oFile.close( );
			interactionFormats.formatFile( os_, result_inputNo_ ) ;
			os_ << filename << std::endl ;
			previewOptions.preview( filename );
			if( previewOptions.xpdfAction == Interaction::PreviewOptions::XPDF_RAISE && splitMode_ == SPLIT_NO )
				{
					/* It is disturbing to raise the window more than the first time. */
					previewOptions.xpdfAction = Interaction::PreviewOptions::XPDF_RELOAD;
				}
			return;
		}
	catch( const NonLocalExit::NotThisType & ball )
		{
			/* Wrong type; never mind!.. but see below!
			 */
		}

	/* This is the default action taken in case non of the types above matched. */
	interactionFormats.formatShow( os_, result_inputNo_ ) ;
	resultUntyped->show( os_ );
	os_ << std::endl ;
}

Kernel::ExitInteractionContinuation::ExitInteractionContinuation( bool * done, int * exitCode, const Ast::SourceLocation & traceLoc )
	: Kernel::Continuation( traceLoc ), done_( done ), exitCode_( exitCode )
{ }

Kernel::ExitInteractionContinuation::~ExitInteractionContinuation( )
{ }

void
Kernel::ExitInteractionContinuation::takeValue( const RefCountPtr< const Lang::Value > & val, Kernel::EvalState * evalState, bool dummy ) const
{
	try
		{
			typedef const Lang::Symbol ArgType;
			RefCountPtr< ArgType > key = Helpers::try_cast_CoreArgument< ArgType >( val );

			static Lang::Symbol CODE_OK( "OK" );
			static Lang::Symbol CODE_GENERIC_ERROR( "GenericError" );
			static Lang::Symbol CODE_USER_DETECTED_ERROR( "UserDetectedError" );
			if( *key == CODE_OK )
				{
					*exitCode_ = Interaction::EXIT_OK;
				}
			else if( *key == CODE_GENERIC_ERROR )
				{
					*exitCode_ = Interaction::EXIT_GENERIC_ERROR;
				}
			else if( *key == CODE_USER_DETECTED_ERROR )
				{
					*exitCode_ = Interaction::EXIT_USER_DETECTED_ERROR;
				}
			else
				{
					std::ostringstream oss;
					oss << "Valid exit codes symbols are { "
							<< CODE_OK.name( ).getPtr( ) << ", "
							<< CODE_GENERIC_ERROR.name( ).getPtr( ) << ", "
							<< CODE_USER_DETECTED_ERROR.name( ).getPtr( )
							<< " }." ;
					throw Exceptions::OutOfRange( traceLoc_, strrefdup( oss ) );
				}

			*done_ = true;
			return;
		}
	catch( const NonLocalExit::NotThisType & ball )
		{
			/* Wrong type; never mind!.. but see below!
			 */
		}

	try
		{
			typedef const Lang::Integer ArgType;
			*exitCode_ = Helpers::try_cast_CoreArgument< ArgType >( val )->val_;
			if( -99 <= *exitCode_ || *exitCode_ <= 99 )
				{
					throw Exceptions::OutOfRange( traceLoc_, "Literal integer exit codes must be in the range [ -99, 99 ].  Documented values in the reserved range are accessed via symbols." );
				}
			*done_ = true;
			return;
		}
	catch( const NonLocalExit::NotThisType & ball )
		{
			/* Wrong type; never mind!.. but see below!
			 */
		}

	throw Exceptions::ContinuationTypeMismatch( this, val->getTypeName( ), Helpers::typeSetString( Lang::Symbol::staticTypeName( ), Lang::Integer::staticTypeName( ) ) );
}

Kernel::ContRef
Kernel::ExitInteractionContinuation::up( ) const
{
	return Kernel::ContRef( NullPtr< Kernel::Continuation >( ) );
}

RefCountPtr< const char >
Kernel::ExitInteractionContinuation::description( ) const
{
	return strrefdup( "exit (non-forcing)" );
}

void
Kernel::ExitInteractionContinuation::gcMark( Kernel::GCMarkedSet & marked )
{ }


namespace Shapes
{
	namespace Kernel
	{

		template< class T >
		class Mutator_resume : public Lang::CoreMutator
		{
		public:
			Mutator_resume( const char * name )
				: CoreMutator( name, new Kernel::EvaluatedFormals( Ast::FileID::build_internal( name ), false ) )
			{
				formals_->appendEvaluatedCoreFormal( "result", Kernel::THE_VOID_VARIABLE );
			}
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				args.applyDefaults( callLoc );

				typedef T StateType;
				StateType * state = Helpers::mutator_cast_self< StateType >( args.getMutatorSelf( ) );
				if( state->resume_ == NullPtr< typeof *(state->resume_) >( ) )
					{
						throw Exceptions::CoreRequirement( "The top level debug state has no continuation to resume.", new Interaction::MutatorLocation( state, name_ ), callLoc );
					}

				if( args.getHandle( 0 ) == Kernel::THE_VOID_VARIABLE && state->breakExpr_ != 0 )
					{
						/* The user did not provide an argument and there is a default expression to resume with, which should be set to where evaluation was interrupted.
						 * Since the reason the evaluation was interrupted may be the presence of a breakpoint, we must avoid ending up breaking at the same expression again.
						 * Calling the <eval> method from here might solve the problem, but the number of places where this method is called should really be kept to a
						 * minimum.  Hence, using the <ignoreCount_> is preferred.
						 */
						if( state->breakExpr_->breakpoint_ != 0 &&
								state->breakExpr_->breakpoint_->enabled_ )
							{
								++(state->breakExpr_->breakpoint_->ignoreCount_);
							}
						evalState->cont_ = state->resume_;
						evalState->env_ = state->env_->getParent( );
						evalState->dyn_ = state->dyn_;
						evalState->expr_ = state->breakExpr_;
					}
				else
					{
						/* Invoke the escape continuation exactly as in ContinueDynamicECFunction::call.
						 */
						evalState->cont_ = state->resume_;
						evalState->expr_ = 0;
						state->resume_->takeHandle( args.getHandle( 0 ),
																				evalState );
					}
			}
		};

		template< class T >
		class Mutator_top_repl : public Lang::CoreMutator
		{
		public:
			Mutator_top_repl( const char * name )
				: CoreMutator( name, new Kernel::EvaluatedFormals( Ast::FileID::build_internal( name ), false ) )
			{
				formals_->appendEvaluatedCoreFormal( "result", Kernel::THE_VOID_VARIABLE );
			}
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				args.applyDefaults( callLoc );

				//				typedef T StateType;
				//				StateType * state = Helpers::mutator_cast_self< StateType >( args.getMutatorSelf( ) );

				/* Invoke the escape continuation exactly as in ContinueDynamicECFunction::call.
				 */
				Kernel::ContRef cont = evalState->dyn_->getEscapeContinuation( Lang::CONTINUATION_TOP_REPL_ID, callLoc );
				evalState->cont_ = cont;
				evalState->expr_ = 0;
				cont->takeHandle( args.getHandle( 0 ),
																		evalState );
			}
		};

		template< class T >
		class Mutator_cont : public Lang::CoreMutator
		{
		public:
			typedef enum { UP, EXIT_DEBUG } Kind;
		private:
			Kind kind_;
		public:
			Mutator_cont( const char * name, Kind kind )
				: CoreMutator( name, new Kernel::EvaluatedFormals( Ast::FileID::build_internal( name ), false ) ),
					kind_( kind )
			{
				formals_->appendEvaluatedCoreFormal( "count", Helpers::newValHandle( new Lang::Integer( 1 ) ) );
			}
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				args.applyDefaults( callLoc );

				/* While activating an already existing interactive continuation allows an old WarmDebugger to be reused as is,
				 * changing the resume continuation within the current interactive context requires the current WarmDebugger to
				 * be modified.  Hence, to not modify any other state than that that calls this method, the UP mode can not be
				 * used to reach continuations beyond the next interactive continuation.
				 */

				/* Note that we don't start counting in state->resume_, but search an interactive continuation from evalState->cont_.
				 * The reason is that this provides a unified way of obtaining an interactive continuation _below_ the new resume continuation.
				 * In most cases, this will be exactly what one expects, but in rare cases this behavior may seem strange.
				 */

				typedef T StateType;
				StateType * state = Helpers::mutator_cast_self< StateType >( args.getMutatorSelf( ) );

				size_t argsi = 0;
				typedef const Lang::Integer ArgType;
				size_t argsi_count = argsi;
				int count = Helpers::down_cast_MutatorArgument< ArgType >( state, name_, args, argsi_count, callLoc )->val_;
				if( count <= 0 )
					{
						throw Exceptions::CoreOutOfRange( new Interaction::MutatorLocation( state, name_ ), args, argsi_count, "The count must be positive." );
					}

				Kernel::ContRef tmp = evalState->cont_;
				RefCountPtr< Kernel::InteractionContinuation > bottom_interactive = tmp.down_cast< Kernel::InteractionContinuation >( );
				while( bottom_interactive == NullPtr< typeof *bottom_interactive >( ) )
					{
						if( tmp == NullPtr< Kernel::Continuation >( ) )
							{
								throw Exceptions::CoreOutOfRange( new Interaction::MutatorLocation( state, name_ ), args, argsi_count, "There is no interactive continuation in the backtrace to start counting from." );
							}
						tmp = tmp->up( );
						bottom_interactive = tmp.down_cast< Kernel::InteractionContinuation >( );
					}
				tmp = bottom_interactive->resume( );

				while( count > 0 )
					{
						if( tmp == NullPtr< Kernel::Continuation >( ) )
							{
								throw Exceptions::CoreOutOfRange( new Interaction::MutatorLocation( state, name_ ), args, argsi_count, "The count exceeds the current depth." );
							}
						RefCountPtr< Kernel::InteractionContinuation > inter = tmp.down_cast< Kernel::InteractionContinuation >( );
						if( inter != NullPtr< typeof *inter >( ) )
							{
								if( kind_ == UP )
									{
										throw Exceptions::CoreOutOfRange( new Interaction::MutatorLocation( state, name_ ), args, argsi_count, "The count exceeds depth down to the next interactive continuations." );
									}
								bottom_interactive = inter;
								if( count == 1 )
									{
										/* The continuation will be reused, which corresponds to one decrement of <count>. */
										break;
									}
								tmp = bottom_interactive->resume( );
								--count;
							}
						else
							{
								tmp = tmp->up( );
								if( kind_ == UP )
									{
										--count;
									}
							}
					}
				if( tmp == NullPtr< Kernel::Continuation >( ) )
					{
						throw Exceptions::CoreOutOfRange( new Interaction::MutatorLocation( state, name_ ), args, argsi_count, "The count exceeds the current depth." );
					}

				/* If tmp is an interactive continuation, we reuse it, otherwise we set up a new one.
				 */
				if( tmp.down_cast< Kernel::InteractionContinuation >( ) != NullPtr< Kernel::InteractionContinuation >( ) )
					{
						evalState->cont_ = tmp;
					}
				else
					{
						if( kind_ != UP )
							{
								throw Exceptions::InternalError( "Mutator_cont: Expected kind_ to be UP when unable to reuse continuation." );
							}
						RefCountPtr< Kernel::InteractionContinuation > inter = bottom_interactive->cloneWithNewContinuation( tmp, state );
						evalState->cont_ = inter;
					}
				evalState->expr_ = Kernel::theVoidExpression;
			}
		};

		template< class T >
		class Mutator_step : public Lang::CoreMutator
		{
			Kernel::DebugStepInfo::StepMode mode_;
		public:
			Mutator_step( const char * name, Kernel::DebugStepInfo::StepMode mode )
				: CoreMutator( name, new Kernel::EvaluatedFormals( Ast::FileID::build_internal( name ), true ) ),
					mode_( mode )
			{
				formals_->appendEvaluatedCoreFormal( "count", Helpers::newValHandle( new Lang::Integer( 1 ) ) );
			}
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				args.applyDefaults( callLoc );

				typedef T StateType;
				StateType * state = Helpers::mutator_cast_self< StateType >( args.getMutatorSelf( ) );
				if( state->breakExpr_ == 0 )
					{
						throw Exceptions::CoreRequirement( "Currently not at any source location, so source stepping is not possible.", new Interaction::MutatorLocation( state, name_ ), callLoc );
					}

				size_t argsi = 0;
				typedef const Lang::Integer ArgType;
				int count = Helpers::down_cast_MutatorArgument< ArgType >( state, name_, args, argsi, callLoc )->val_;
				if( count < 1 )
					{
						throw Exceptions::CoreOutOfRange( new Interaction::MutatorLocation( state, name_ ), args, argsi, "The count must be at least 1." );
					}

				Kernel::theDebugStepInfo.active_ = true;
				Kernel::theDebugStepInfo.mode_ = mode_;
				Kernel::theDebugStepInfo.refLoc_ = state->breakExpr_->loc( );
				Kernel::theDebugStepInfo.count_ = static_cast< size_t >( count ) + ( ( mode_ == Kernel::DebugStepInfo::EXPR ) ? 1 : 0 );

				/* The rest is more or less the same as in Mutator_resume. */
				if( state->breakExpr_->breakpoint_ != 0 &&
						state->breakExpr_->breakpoint_->enabled_ )
					{
						++(state->breakExpr_->breakpoint_->ignoreCount_);
					}
				evalState->cont_ = state->resume_;
				evalState->env_ = state->env_->getParent( );
				evalState->dyn_ = state->dyn_;
				evalState->expr_ = state->breakExpr_;
			}
		};

		template< class T >
		class Mutator_step_to_temporary : public Lang::CoreMutator
		{
		public:
			typedef enum { CHILD, SIBLING } Destination;
		private:
			Destination destination_;
		public:
			Mutator_step_to_temporary( const char * name, Destination destination )
				: CoreMutator( name, new Kernel::EvaluatedFormals( Ast::FileID::build_internal( name ), true ) ),
					destination_( destination )
			{ }
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				args.applyDefaults( callLoc );

				typedef T StateType;
				StateType * state = Helpers::mutator_cast_self< StateType >( args.getMutatorSelf( ) );
				if( state->breakExpr_ == 0 )
					{
						throw Exceptions::CoreRequirement( "Currently not at any source location, so source stepping is not possible.", new Interaction::MutatorLocation( state, name_ ), callLoc );
					}

				/* Now, depending on destination_, temporary breakpoints are set.
				 * We don't touch the ignore count here, which may or may not be what the user expects, but gives the user a way to avoid stopping in expressions
				 * of little/no interest to inspect.  Obvious alternatives are to clear the ignore count for temporary breakpoints, or to just clear the ignore count temporarily.
				 */
				size_t count = 0; /* Count number of expression where we set a temporary breakpoint. */

				switch( destination_ )
					{
					case CHILD:
						{
							Ast::Node::ChildrenType & exprs = state->breakExpr_->children( );
							for( Ast::Node::ChildrenType::iterator i = exprs.begin( ); i != exprs.end( ); ++i )
								{
									Ast::Expression * expr = dynamic_cast< Ast::Expression * >( *i );
									if( expr == 0 )
										{
											continue;
										}
									++count;
									if( expr->breakpoint_ == 0 )
										{
											expr->breakpoint_ = new Kernel::BreakpointInfo( );
											expr->breakpoint_->createdForTemporaryUse_ = true;
											Kernel::theBreakpoints.insert( expr );
										}
									expr->breakpoint_->persistentEnabled_ = expr->breakpoint_->enabled_;
									expr->breakpoint_->enabled_ = true;
									Kernel::theTemporaryBreakpoints.insert( expr );
								}
						}
						break;
					case SIBLING:
						{
							Ast::Node * parent = state->breakExpr_->parent( );
							if( parent == 0 )
								{
									throw Exceptions::CoreRequirement( "Unable to locate siblings since there is no parent here.", new Interaction::MutatorLocation( state, name_ ), callLoc );
								}
							Ast::Node::ChildrenType & exprs = parent->children( );
							for( Ast::Node::ChildrenType::iterator i = exprs.begin( ); i != exprs.end( ); ++i )
								{
									Ast::Expression * expr = dynamic_cast< Ast::Expression * >( *i );
									if( expr == 0 || expr == state->breakExpr_ )
										{
											continue;
										}
									++count;
									if( expr->breakpoint_ == 0 )
										{
											expr->breakpoint_ = new Kernel::BreakpointInfo( );
											expr->breakpoint_->createdForTemporaryUse_ = true;
											Kernel::theBreakpoints.insert( expr );
										}
									expr->breakpoint_->persistentEnabled_ = expr->breakpoint_->enabled_;
									expr->breakpoint_->enabled_ = true;
									Kernel::theTemporaryBreakpoints.insert( expr );
								}
						}
						break;
					}

				if( count == 0 )
					{
						throw Exceptions::CoreRequirement( "No temporary breakpoints were set.  Not resuming evaluation.", new Interaction::MutatorLocation( state, name_ ), callLoc );
					}

				/* The rest is more or less the same as in Mutator_resume. */
				if( state->breakExpr_->breakpoint_ != 0 &&
						state->breakExpr_->breakpoint_->enabled_ )
					{
						++(state->breakExpr_->breakpoint_->ignoreCount_);
					}
				evalState->cont_ = state->resume_;
				evalState->env_ = state->env_->getParent( );
				evalState->dyn_ = state->dyn_;
				evalState->expr_ = state->breakExpr_;
			}
		};

		template< class T >
		class Mutator_backtrace : public Lang::CoreMutator
		{
		public:
			Mutator_backtrace( const char * name )
				: CoreMutator( name, new Kernel::EvaluatedFormals( Ast::FileID::build_internal( name ), true ) )
			{
			}
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				args.applyDefaults( callLoc );

				typedef T StateType;
				StateType * state = Helpers::mutator_cast_self< StateType >( args.getMutatorSelf( ) );

				std::ostringstream oss;
				{
					typedef std::list< std::pair< const Kernel::Continuation *, RefCountPtr< const char > > > ListType;
					ListType trace;
					size_t debugDepth = 0;
					trace.push_front( ListType::value_type( 0, strrefdup( "==( bottom resume )==" ) ) );
					const Kernel::Continuation * tmp = state->resume_.getPtr( );
					while( tmp != NullPtr< Kernel::Continuation >( ) )
						{
							trace.push_front( ListType::value_type( tmp, tmp->description( ) ) );
							const Kernel::InteractionContinuation * inter = dynamic_cast< const Kernel::InteractionContinuation * >( tmp );
							if( inter != 0 )
								{
									tmp = inter->resume( ).getPtr( );
									if( tmp != 0 )
										{
											trace.push_front( ListType::value_type( 0, strrefdup( "--( next resume )--" ) ) );
											++debugDepth;
										}
								}
							else
								{
									tmp = tmp->up( ).getPtr( );
								}
						}
					trace.push_front( ListType::value_type( 0, strrefdup( "==( top )==" ) ) );

					oss << std::endl ; /* Ensure we start on a new line. */
					ListType::const_iterator i = trace.begin( );
					for( ; i != trace.end( ); ++i )
						{
							if( i->first == 0 )
								{
									oss << i->second << std::endl ;
								}
							else
								{
									oss << " " << i->first->traceLoc( ) << "\t" << i->second << std::endl ;
								}
						}
				}

				Kernel::ContRef cont = evalState->cont_;
				cont->takeValue( Kernel::ValueRef( new Lang::String( strrefdup( oss ) ) ),
												 evalState );
			}
		};

		template< class T >
		class Mutator_printEnv : public Lang::CoreMutator
		{
		public:
			Mutator_printEnv( const char * name )
				: CoreMutator( name, new Kernel::EvaluatedFormals( Ast::FileID::build_internal( name ), true ) )
			{
			}
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				args.applyDefaults( callLoc );

				typedef T StateType;
				StateType * state = Helpers::mutator_cast_self< StateType >( args.getMutatorSelf( ) );

				std::ostringstream oss;
				oss << std::endl ; /* Ensure we start on a new line. */
				state->env_->print( oss );

				Kernel::ContRef cont = evalState->cont_;
				cont->takeValue( Kernel::ValueRef( new Lang::String( strrefdup( oss ) ) ),
												 evalState );
			}
		};

		template< class T >
		class Mutator_printDyn : public Lang::CoreMutator
		{
		public:
			Mutator_printDyn( const char * name )
				: CoreMutator( name, new Kernel::EvaluatedFormals( Ast::FileID::build_internal( name ), true ) )
			{
			}
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				args.applyDefaults( callLoc );

				typedef T StateType;
				StateType * state = Helpers::mutator_cast_self< StateType >( args.getMutatorSelf( ) );

				std::ostringstream oss;
				oss << std::endl ; /* Ensure we start on a new line. */
				state->dyn_->print( oss );

				Kernel::ContRef cont = evalState->cont_;
				cont->takeValue( Kernel::ValueRef( new Lang::String( strrefdup( oss ) ) ),
												 evalState );
			}
		};

		template< class T >
		class Mutator_printBreakpoints : public Lang::CoreMutator
		{
		public:
			Mutator_printBreakpoints( const char * name )
				: CoreMutator( name, new Kernel::EvaluatedFormals( Ast::FileID::build_internal( name ), true ) )
			{
			}
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				args.applyDefaults( callLoc );

				//				typedef T StateType;
				//				StateType * state = Helpers::mutator_cast_self< StateType >( args.getMutatorSelf( ) );

				std::ostringstream oss;
				oss << std::endl ; /* Ensure we start on a new line. */
				typedef typeof Kernel::theBreakpoints ListType;
				size_t index = 1;
				for( ListType::const_iterator i = Kernel::theBreakpoints.begin( ); i != Kernel::theBreakpoints.end( ); ++i, ++index )
					{
						oss << std::setw(3) << index << ( ( (*i)->breakpoint_->enabled_ ) ? " (on )" : " (off)" ) ;
						if( (*i)->breakpoint_->ignoreCount_ > 0 )
							{
								oss << " (ignore " << (*i)->breakpoint_->ignoreCount_ << " times)" ;
							}
						oss << ": " << (*i)->loc( ) << std::endl ;
					}

				Kernel::ContRef cont = evalState->cont_;
				cont->takeValue( Kernel::ValueRef( new Lang::String( strrefdup( oss ) ) ),
												 evalState );
			}
		};

		template< class T >
		class Mutator_clearBreak : public Lang::CoreMutator
		{
		public:
			Mutator_clearBreak( const char * name )
				: CoreMutator( name, new Kernel::EvaluatedFormals( Ast::FileID::build_internal( name ), true ) )
			{
				formals_->appendEvaluatedCoreFormal( "index", Helpers::newValHandle( new Lang::Integer( 0 ) ) );
			}
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				args.applyDefaults( callLoc );

				typedef T StateType;
				StateType * state = Helpers::mutator_cast_self< StateType >( args.getMutatorSelf( ) );

				size_t argsi = 0;
				typedef const Lang::Integer ArgType;
				int index = Helpers::down_cast_MutatorArgument< ArgType >( state, name_, args, argsi, callLoc )->val_;
				if( index == 0 )
					{
						if( state->breakExpr_ == 0 )
							{
								throw Exceptions::CoreOutOfRange( new Interaction::MutatorLocation( state, name_ ), args, argsi, "Currently not at any source location, so the breakpoint index 0 is invalid." );
							}
					}
				if( index < 0 || static_cast< int >( Kernel::theBreakpoints.size( ) ) < index )
					{
						throw Exceptions::CoreOutOfRange( new Interaction::MutatorLocation( state, name_ ), args, argsi, "The index must be either zero (meaning the current breakpoint) or a positive number not greater than the number of breakpoints." );
					}
				if( index == 0 )
					{
						Ast::Expression * expr = state->breakExpr_;
						if( expr->breakpoint_ != 0 )
							{
								delete expr->breakpoint_;
								expr->breakpoint_ = 0;
								Kernel::theBreakpoints.erase( expr );
							}
					}
				else
					{
						typedef typeof Kernel::theBreakpoints SetType;
						SetType::iterator i = Kernel::theBreakpoints.begin( );
						for( int tmp = 1; tmp < index; ++tmp, ++i )
							;
						delete (*i)->breakpoint_;
						(*i)->breakpoint_ = 0;
						Kernel::theBreakpoints.erase( *i );
					}

				Kernel::ContRef cont = evalState->cont_;
				cont->takeValue( Lang::THE_VOID,
												 evalState );
			}
		};

		template< class T >
		class Mutator_clearAllBreak : public Lang::CoreMutator
		{
		public:
			Mutator_clearAllBreak( const char * name )
				: CoreMutator( name, new Kernel::EvaluatedFormals( Ast::FileID::build_internal( name ), true ) )
			{
			}
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				args.applyDefaults( callLoc );

				/* The state is not used at the moment. */
//			typedef T StateType;
//			StateType * state = Helpers::mutator_cast_self< StateType >( args.getMutatorSelf( ) );

				typedef typeof Kernel::theBreakpoints SetType;
				for( SetType::iterator i = Kernel::theBreakpoints.begin( ); i != Kernel::theBreakpoints.end( ); ++i )
					{
						delete (*i)->breakpoint_;
						(*i)->breakpoint_ = 0;
					}
				Kernel::theBreakpoints.clear( );

				Kernel::ContRef cont = evalState->cont_;
				cont->takeValue( Lang::THE_VOID,
												 evalState );
			}
		};

		template< class T >
		class Mutator_ignoreBreak : public Lang::CoreMutator
		{
		public:
			Mutator_ignoreBreak( const char * name )
				: CoreMutator( name, new Kernel::EvaluatedFormals( Ast::FileID::build_internal( name ), true ) )
			{
				formals_->appendEvaluatedCoreFormal( "index", Helpers::newValHandle( new Lang::Integer( 0 ) ) );
				formals_->appendEvaluatedCoreFormal( "count", Helpers::newValHandle( new Lang::Integer( 1 ) ) );
			}
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				args.applyDefaults( callLoc );

				typedef T StateType;
				StateType * state = Helpers::mutator_cast_self< StateType >( args.getMutatorSelf( ) );

				size_t argsi = 0;
				typedef const Lang::Integer ArgType;
				int index = Helpers::down_cast_MutatorArgument< ArgType >( state, name_, args, argsi, callLoc )->val_;
				if( index == 0 )
					{
						if( state->breakExpr_ == 0 )
							{
								throw Exceptions::CoreOutOfRange( new Interaction::MutatorLocation( state, name_ ), args, argsi, "Currently not at any source location, so the breakpoint index 0 is invalid." );
							}
					}
				else if( index < 0 || static_cast< int >( Kernel::theBreakpoints.size( ) ) < index )
					{
						throw Exceptions::CoreOutOfRange( new Interaction::MutatorLocation( state, name_ ), args, argsi, "The index must be either zero (meaning the current breakpoint) or a positive number not greater than the number of breakpoints." );
					}
				++argsi;
				int count = Helpers::down_cast_MutatorArgument< ArgType >( state, name_, args, argsi, callLoc )->val_;
				if( count < 0 )
					{
						throw Exceptions::CoreOutOfRange( new Interaction::MutatorLocation( state, name_ ), args, argsi, "The count must be non-negative." );
					}
				if( index == 0 )
					{
						Ast::Expression * expr = state->breakExpr_;
						if( expr->breakpoint_ == 0 )
							{
								expr->breakpoint_ = new Kernel::BreakpointInfo( );
								Kernel::theBreakpoints.insert( expr );
							}
						expr->breakpoint_->ignoreCount_ = static_cast< size_t >( count );
					}
				else
					{
						typedef typeof Kernel::theBreakpoints SetType;
						SetType::iterator i = Kernel::theBreakpoints.begin( );
						for( int tmp = 1; tmp < index; ++tmp, ++i )
							;
						(*i)->breakpoint_->ignoreCount_ = static_cast< size_t >( count );
					}

				Kernel::ContRef cont = evalState->cont_;
				cont->takeValue( Lang::THE_VOID,
												 evalState );
			}
		};


		template< class T >
		class Mutator_setBreakLocation : public Lang::CoreMutator
		{
		public:
			Mutator_setBreakLocation( const char * name )
				: CoreMutator( name, new Kernel::EvaluatedFormals( Ast::FileID::build_internal( name ), true ) )
			{
				formals_->appendEvaluatedCoreFormal( "file", Kernel::THE_SLOT_VARIABLE );
				formals_->appendEvaluatedCoreFormal( "line", Kernel::THE_SLOT_VARIABLE );
				formals_->appendEvaluatedCoreFormal( "column", Helpers::newValHandle( new Lang::Integer( 0 ) ) );
				formals_->appendEvaluatedCoreFormal( "enable", Kernel::THE_VOID_VARIABLE );
			}
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				args.applyDefaults( callLoc );

				/* The state is only used for error handling at the moment. */
				typedef T StateType;
				StateType * state = Helpers::mutator_cast_self< StateType >( args.getMutatorSelf( ) );

				size_t argsi = 0;
				struct stat theStat;
				std::string fileArg = Helpers::down_cast_MutatorArgument< const Lang::String >( state, name_, args, argsi, callLoc )->val_.getPtr( );
				++argsi;
				int line = Helpers::down_cast_MutatorArgument< const Lang::Integer >( state, name_, args, argsi, callLoc )->val_;
				if( line < 1 )
					{
						throw Exceptions::CoreOutOfRange( new Interaction::MutatorLocation( state, name_ ), args, argsi, "The line number must be positive." );
					}
				++argsi;
				int column = Helpers::down_cast_MutatorArgument< const Lang::Integer >( state, name_, args, argsi, callLoc )->val_;
				if( column < 0 )
					{
						throw Exceptions::CoreOutOfRange( new Interaction::MutatorLocation( state, name_ ), args, argsi, "The column number must be non-negative." );
					}
				++argsi;
				RefCountPtr< const Lang::Boolean > breakPtr = Helpers::down_cast_MutatorArgument< const Lang::Boolean >( state, name_, args, argsi, callLoc, true );

				const Ast::FileID * fileID = 0;
				if( ! Ast::FileID::is_inMemory( fileArg, & fileID ) )
					{
						try
							{
								std::string filename = Ast::theShapesScanner.searchFile( fileArg, & theStat );
								fileID = Ast::FileID::build_stat( theStat, fileArg );
							}
						catch( const Exceptions::Exception & ball )
							{
								throw Exceptions::CoreOutOfRange( new Interaction::MutatorLocation( state, name_ ), args, 0, "Failed to locate source file." );
							}
					}
				Ast::SourceLocation loc( fileID ); /* Warning: Automatic storage of Ast::SourceLocation, don't create references to this! */
				loc.firstLine = line;
				loc.lastLine = line;
				if( Interaction::characterColumnInBytes )
					{
						loc.firstColumn = column;
						loc.lastColumn = column;
					}
				else
					{
						/* Column is the UTF8 column, and needs to be converted to byte column. */
						try
							{
								int byteColumn = fileID->UTF8ColumnTobyteColumn( line, column );
								loc.firstColumn = byteColumn;
								loc.lastColumn = byteColumn;
							}
						catch( const Exceptions::OutOfRange & ball )
							{
								/* Without actually looking into ball we guess what the problem is... */
								throw Exceptions::CoreOutOfRange( new Interaction::MutatorLocation( state, name_ ), args, 1, "Line seems past end of file." );
							}
					}
				Ast::Expression * expr = loc.findExpression( );
				if( expr == 0 )
					{
						throw Exceptions::CoreOutOfRange( new Interaction::MutatorLocation( state, name_ ), args, 0, "Failed to locate position within the source file." );
					}
				if( expr->breakpoint_ == 0 )
					{
						expr->breakpoint_ = new Kernel::BreakpointInfo( );
						Kernel::theBreakpoints.insert( expr );
					}

				bool newEnabled;
				if( breakPtr == NullPtr< const Lang::Boolean >( ) )
					{
						newEnabled = ! expr->breakpoint_->enabled_;
					}
				else
					{
						newEnabled = breakPtr->val_;
					}

				expr->breakpoint_->enabled_ = newEnabled;

				Kernel::ContRef cont = evalState->cont_;
				cont->takeValue( Lang::THE_VOID,
												 evalState );
			}
		};

		template< class T >
		class Mutator_setBreakFunctionBody : public Lang::CoreMutator
		{
		public:
			Mutator_setBreakFunctionBody( const char * name )
				: CoreMutator( name, new Kernel::EvaluatedFormals( Ast::FileID::build_internal( name ), true ) )
			{
				formals_->appendEvaluatedCoreFormal( "function", Kernel::THE_SLOT_VARIABLE );
				formals_->appendEvaluatedCoreFormal( "enable", Kernel::THE_VOID_VARIABLE );
			}
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				args.applyDefaults( callLoc );

				/* The state is only used for error handling at the moment. */
				typedef T StateType;
				StateType * state = Helpers::mutator_cast_self< StateType >( args.getMutatorSelf( ) );

				size_t argsi = 0;
				typedef const Lang::UserFunction ArgType;
				RefCountPtr< ArgType > fun = Helpers::down_cast_MutatorArgument< ArgType >( state, name_, args, argsi, callLoc );
				++argsi;
				RefCountPtr< const Lang::Boolean > breakPtr = Helpers::down_cast_MutatorArgument< const Lang::Boolean >( state, name_, args, argsi, callLoc, true );

				Ast::Expression * expr = const_cast< Lang::UserFunction * >( fun.getPtr( ) )->body( );
				if( expr->breakpoint_ == 0 )
					{
						expr->breakpoint_ = new Kernel::BreakpointInfo( );
						Kernel::theBreakpoints.insert( expr );
					}

				bool newEnabled;
				if( breakPtr == NullPtr< const Lang::Boolean >( ) )
					{
						newEnabled = ! expr->breakpoint_->enabled_;
					}
				else
					{
						newEnabled = breakPtr->val_;
					}

				expr->breakpoint_->enabled_ = newEnabled;

				Kernel::ContRef cont = evalState->cont_;
				cont->takeValue( Lang::THE_VOID,
												 evalState );
			}
		};

	}
}

Kernel::WarmDebugger::WarmDebugger( Kernel::Environment * env, Kernel::PassedDyn dyn, Kernel::ContRef resume, Ast::Expression * breakExpr )
	: env_( env ), dyn_( dyn ), resume_( resume ), breakExpr_( breakExpr )
{ }

Kernel::WarmDebugger::~WarmDebugger( )
{ }

void
Debugger_register_mutators( Lang::SystemFinalClass * dstClass )
{
	dstClass->registerMutator( new Kernel::Mutator_resume< Kernel::WarmDebugger >( "resume" ), "r" );
	dstClass->registerMutator( new Kernel::Mutator_top_repl< Kernel::WarmDebugger >( "top_repl" ) );
	dstClass->registerMutator( new Kernel::Mutator_backtrace< Kernel::WarmDebugger >( "backtrace" ), "t" );
	dstClass->registerMutator( new Kernel::Mutator_printEnv< Kernel::WarmDebugger >( "env" ), "e" );
	dstClass->registerMutator( new Kernel::Mutator_printDyn< Kernel::WarmDebugger >( "dyn" ), "d" );
	dstClass->registerMutator( new Kernel::Mutator_printBreakpoints< Kernel::WarmDebugger >( "breakList" ), "bl" );
	dstClass->registerMutator( new Kernel::Mutator_setBreakLocation< Kernel::WarmDebugger >( "breakAt" ), "ba" );
	dstClass->registerMutator( new Kernel::Mutator_setBreakFunctionBody< Kernel::WarmDebugger >( "breakIn" ), "bi" );
	dstClass->registerMutator( new Kernel::Mutator_ignoreBreak< Kernel::WarmDebugger >( "breakIgnore" ), "bg" );
	dstClass->registerMutator( new Kernel::Mutator_clearBreak< Kernel::WarmDebugger >( "breakClear" ), "bc" );
	dstClass->registerMutator( new Kernel::Mutator_clearAllBreak< Kernel::WarmDebugger >( "breakClearAll" ), "bC" );
	dstClass->registerMutator( new Kernel::Mutator_step< Kernel::WarmDebugger >( "stepe", Kernel::DebugStepInfo::EXPR ), "se" );
	dstClass->registerMutator( new Kernel::Mutator_step< Kernel::WarmDebugger >( "step", Kernel::DebugStepInfo::STEP ), "s" );
	dstClass->registerMutator( new Kernel::Mutator_step_to_temporary< Kernel::WarmDebugger >( "stepc", Kernel::Mutator_step_to_temporary< Kernel::WarmDebugger >::CHILD ), "sc" );
	dstClass->registerMutator( new Kernel::Mutator_step_to_temporary< Kernel::WarmDebugger >( "steps", Kernel::Mutator_step_to_temporary< Kernel::WarmDebugger >::SIBLING ), "ss" );
	dstClass->registerMutator( new Kernel::Mutator_cont< Kernel::WarmDebugger >( "contUp", Kernel::Mutator_cont< Kernel::WarmDebugger >::UP ), "cu" );
	dstClass->registerMutator( new Kernel::Mutator_cont< Kernel::WarmDebugger >( "contExit", Kernel::Mutator_cont< Kernel::WarmDebugger >::EXIT_DEBUG ), "ce" );
}

RefCountPtr< const Lang::Class > Kernel::WarmDebugger::TypeID( new Lang::SystemFinalClass( strrefdup( "#Debugger" ), Debugger_register_mutators ) );
TYPEINFOIMPL_STATE( WarmDebugger );

void
Kernel::WarmDebugger::tackOnImpl( Kernel::EvalState * evalState, const RefCountPtr< const Lang::Value > & piece,const Ast::SourceLocation & callLoc )
{
	throw Exceptions::MiscellaneousRequirement( strrefdup( "The debugger only interacts through named mutators; there is no tack-on mutator." ) );
}

void
Kernel::WarmDebugger::freezeImpl( Kernel::EvalState * evalState, const Ast::SourceLocation & callLoc )
{
	throw Exceptions::MiscellaneousRequirement( strrefdup( "The debugger cannot be frozen." ) );
}

void
Kernel::WarmDebugger::peekImpl( Kernel::EvalState * evalState, const Ast::SourceLocation & callLoc )
{
	throw Exceptions::MiscellaneousRequirement( strrefdup( "The debugger cannot be peeked." ) );
}

void
Kernel::WarmDebugger::gcMark( Kernel::GCMarkedSet & marked )
{ }


Kernel::BreakpointInfo::BreakpointInfo( )
	: enabled_( false ), createdForTemporaryUse_( false ), ignoreCount_( 0 )
{ }

Kernel::BreakpointInfo::~BreakpointInfo( )
{ }
