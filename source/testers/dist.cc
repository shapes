#include <cmath>

#include "autoonoff.h"
#include "dtforth.h"
#include "dtforthtypes.h"

#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>
#include <cmath>
#include <stdint.h>
#include <gsl/gsl_blas.h>

#include "../quadratic_programs.h"
#include "../mathematicarepresentation.h"

bool strprefixcmp( char * str, const char * prefix, char ** endp );

int
main( int argc, char ** argv )
{
	typedef enum { TASK_DEFAULT, TASK_SOLVE, TASK_MATHEMATICA_FORM, TASK_QP_TESTER_FORM, TASK_SHAPES_FIGURE } Task;
	Task task = TASK_DEFAULT;

	double gapTolRel = -1;
	double gapTolAbs = -1;
	double lmTol = 0;
	double sigmaTol = 0;
	bool quiet = false;
	double dataScaling = 1;

  size_t dim;
	size_t n1;
	size_t n2;
	double * g1 = 0;
	double * g2 = 0;

	argc -= 1;
	argv += 1;
	size_t nRepeat = 1;
	while( argc > 0 )
		{
			char * optionSuffix;
			if( strprefixcmp( *argv, "--task=", & optionSuffix ) )
				{
					if( strcmp( optionSuffix, "solve" ) == 0 )
						{
							task = TASK_SOLVE;
						}
					else if( strcmp( optionSuffix, "mathematica-form" ) == 0 )
						{
							task = TASK_MATHEMATICA_FORM;
						}
					else if( strcmp( optionSuffix, "qp-tester-form" ) == 0 )
						{
							task = TASK_QP_TESTER_FORM;
						}
					else if( strcmp( optionSuffix, "figure" ) == 0 )
						{
							task = TASK_SHAPES_FIGURE;
						}
					else
						{
							std::cerr << "Illegal command parameter in: " << *argv << std::endl ;
							exit( 1 );
						}
					--argc;
					++argv;
				}
			else if( strprefixcmp( *argv, "--repeat=", & optionSuffix ) )
				{
					char * end;
					int tmp = strtol( optionSuffix, & end, 10 );
					if( *end != '\0' )
						{
							std::cerr << "Malformed integer in " << *argv << std::endl ;
							exit( 1 );
						}
					if( tmp < 1 )
						{
							std::cerr << "Value in " << *argv << " must be at least 1." << std::endl ;
						}
					nRepeat = static_cast< size_t >( tmp );
					--argc;
					++argv;
				}
			else if( strprefixcmp( *argv, "--gap-abs=", & optionSuffix ) )
				{
					char * end;
					double tmp = strtod( optionSuffix, & end );
					if( *end != '\0' )
						{
							std::cerr << "Malformed float in " << *argv << std::endl ;
							exit( 1 );
						}
					if( tmp <= 0 )
						{
							std::cerr << "Value in " << *argv << " must be positive." << std::endl ;
						}
					gapTolAbs = tmp;
					--argc;
					++argv;
				}
			else if( strprefixcmp( *argv, "--gap-rel=", & optionSuffix ) )
				{
					char * end;
					double tmp = strtod( optionSuffix, & end );
					if( *end != '\0' )
						{
							std::cerr << "Malformed float in " << *argv << std::endl ;
							exit( 1 );
						}
					if( tmp <= 0 )
						{
							std::cerr << "Value in " << *argv << " must be positive." << std::endl ;
						}
					gapTolRel = tmp;
					--argc;
					++argv;
				}
			else if( strprefixcmp( *argv, "--lm-tol=", & optionSuffix ) )
				{
					char * end;
					double tmp = strtod( optionSuffix, & end );
					if( *end != '\0' )
						{
							std::cerr << "Malformed float in " << *argv << std::endl ;
							exit( 1 );
						}
					if( tmp <= 0 )
						{
							std::cerr << "Value in " << *argv << " must be positive." << std::endl ;
						}
					lmTol = tmp;
					--argc;
					++argv;
				}
			else if( strprefixcmp( *argv, "--sigma-tol=", & optionSuffix ) )
				{
					char * end;
					double tmp = strtod( optionSuffix, & end );
					if( *end != '\0' )
						{
							std::cerr << "Malformed float in " << *argv << std::endl ;
							exit( 1 );
						}
					if( tmp <= 0 )
						{
							std::cerr << "Value in " << *argv << " must be positive." << std::endl ;
						}
					sigmaTol = tmp;
					--argc;
					++argv;
				}
			else if( strprefixcmp( *argv, "--scale=", & optionSuffix ) )
				{
					char * end;
					double tmp = strtod( optionSuffix, & end );
					if( *end != '\0' )
						{
							std::cerr << "Malformed float in " << *argv << std::endl ;
							exit( 1 );
						}
					if( tmp <= 0 )
						{
							std::cerr << "Value in " << *argv << " must be positive." << std::endl ;
						}
					dataScaling = tmp;
					--argc;
					++argv;
				}
			else if( strcmp( *argv, "--quiet" ) == 0 )
				{
					quiet = true;
					--argc;
					++argv;
				}
			else if( strcmp( *argv, "--read" ) == 0 )
				{
					if( argc < 2 )
						{
							std::cerr << "Missing parameter after " << *argv << std::endl ;
							exit( 1 );
						}
					const char * filename = argv[ 1 ];
					std::ifstream iFile( filename, std::ios::in | std::ios::binary );
					uint32_t tmp;
					iFile.read( reinterpret_cast< char * >( & tmp ), sizeof( tmp ) );
					dim = tmp;
					iFile.read( reinterpret_cast< char * >( & tmp ), sizeof( tmp ) );
					n1 = tmp;
					iFile.read( reinterpret_cast< char * >( & tmp ), sizeof( tmp ) );
					n2 = tmp;
					g1 = new double[ dim * n1 ];
					g2 = new double[ dim * n2 ];
					{
						double * begin = g1;
						double * end = begin + dim * n1;
						for( double * dst = begin; dst != end; ++dst )
							{
								iFile.read( reinterpret_cast< char * >( dst ), sizeof( *dst ) );
							}
					}
					{
						double * begin = g2;
						double * end = begin + dim * n2;
						for( double * dst = begin; dst != end; ++dst )
							{
								iFile.read( reinterpret_cast< char * >( dst ), sizeof( *dst ) );
							}
					}
					argc -= 2;
					argv += 2;
				}
			else
				{
					std::cerr << "Illegal command line option: " << *argv << std::endl ;
					exit( 1 );
				}
		}

	if( task == TASK_DEFAULT )
		{
			task = TASK_SOLVE;
		}

	if( g1 == 0 )
		{
			/* No has been read while scanning command line arguments.  Read textual input from stdin.
			 */
			{
				// The file begins with a comment, that ends after the first line beginning with an equal sign.

				char dummy;
				std::cin.get( dummy );
				for( ; dummy == '\n'; std::cin.get( dummy ) )
					if( std::cin.eof( ) )
						{
							std::cerr << "Reached end of file while searching for '===' that marks the start of data." << std::endl ;
							exit( 1 );
						}
				while( dummy != '=' )
					{
						for( std::cin.get( dummy ); dummy != '\n'; std::cin.get( dummy ) )
							if( std::cin.eof( ) )
								{
									std::cerr << "Reached end of file while searching for '===' that marks the start of data." << std::endl ;
									exit( 1 );
								}
						for( ; dummy == '\n'; std::cin.get( dummy ) )
							if( std::cin.eof( ) )
								{
									std::cerr << "Reached end of file while searching for '===' that marks the start of data." << std::endl ;
									exit( 1 );
								}
					}

				// Skip the final comment row.
				for( std::cin.get( dummy ); dummy != '\n'; std::cin.get( dummy ) )
					if( std::cin.eof( ) )
						{
							std::cerr << "Reached end of file while searching for '===' that marks the start of data." << std::endl ;
							exit( 1 );
						}
			}

			std::cin >> dim >> n1 >> n2 ;
			g1 = new double[ dim * n1 ];
			g2 = new double[ dim * n2 ];

			DTForth myForth( 10000 );

			try
				{
					std::string line;
					std::getline( std::cin, line );
					if( line.empty( ) )
						{
							// After reading nEq, the trailing newline was not eaten up.
							std::getline( std::cin, line );
						}
					myForth << line ;
					{
						double * dst = g1 + dim * n1 - 1;
						for( size_t i = 0; i < dim * n1; ++i, --dst )
							{
								std::ostringstream tmp;
								tmp << "First polytope, generator " << i ;
								*dst = dataScaling * myForth.pop< ForthFloat >( tmp.str( ).c_str( ) );
							}
						if( myForth.stack.depth( ) > 0 )
							{
								std::cerr << "Too many elements in first generator set." << std::endl ;
								exit( 1 );
							}
					}

					std::getline( std::cin, line );
					myForth << line ;
					{
						double * dst = g2 + dim * n2 - 1;
						for( size_t i = 0; i < dim * n2; ++i, --dst )
							{
								std::ostringstream tmp;
								tmp << "Second polytope, generator " << i ;
								*dst = dataScaling * myForth.pop< ForthFloat >( tmp.str( ).c_str( ) );
							}
						if( myForth.stack.depth( ) > 0 )
							{
								std::cerr << "Too many elements in second generator set." << std::endl ;
								exit( 1 );
							}
					}

				}
			catch( ForthErr& msg )
				{
					std::cerr << "Oups!" << std::endl
										<< "\t" << msg.msg << std::endl ;
					exit( 1 );
				}
			catch( char * msg )
				{
					std::cerr << "Caught char * : " << msg << std::endl ;
					exit( 1 );
				}
			catch( std::string & msg )
				{
					std::cerr << "Caught string: " << msg << std::endl ;
					exit( 1 );
				}
			catch( std::exception & msg )
				{
					std::cerr << "Exception: " << msg.what() << std::endl ;
					exit( 1 );
				}
			catch(...)
				{
					std::cerr << "Unknown exception caugt in main." << std::endl ;
					exit( 1 );
				}
		}

	switch( task )
		{
		case TASK_SOLVE:
			{

				Shapes::Computation::QPSolverStatus status;
				double costLow;
				double costHigh;
				for( size_t i = 0; i < nRepeat; ++i )
					{
						Shapes::Computation::polytope_distance_generator_form( dim,
																																	 n1, g1,
																																	 n2, g2,
																																	 & costLow, &costHigh,
																																	 -1, -1, /* No early stops. */
																																	 gapTolAbs, gapTolRel, lmTol, 0, sigmaTol, /* Zero means default tolerance. */
																																	 0, 0, /* Don't care about optimal point. */
																																	 & status );
					}
				if( ! quiet )
					{
						switch( status.code )
							{
							case Shapes::Computation::QP_OK:
								{
									std::cout << "Solver finished OK, in " << status.iterations << " iterations." << std::endl
														<< "  Termination: " << status.sub_str( ) << std::endl ;
								}
								break;
							case Shapes::Computation::QP_WARNING:
								{
									std::cout << "Solver finished with a warning after " << status.iterations << " iterations." << std::endl
														<< "  Reason: " << status.sub_str( ) << std::endl ;
								}
								break;
							default:
								{
									std::cerr << "Solver returned with exit code \"" << status.code_str( ) << "\"" << std::endl
														<< "  Reason: " << status.sub_str( ) << std::endl ;
									exit( 1 );
								}
								break;
							}

						std::cout << "Distance: " << sqrt( costLow ) << " -- " << sqrt( costHigh ) << std::endl ;
					}
			}
			break;
		case TASK_MATHEMATICA_FORM:
			{
				gsl_matrix g1_gsl;
				g1_gsl.data = g1;
				g1_gsl.size1 = n1;
				g1_gsl.size2 = g1_gsl.tda = dim;
				gsl_matrix g2_gsl;
				g2_gsl.data = g2;
				g2_gsl.size1 = n2;
				g2_gsl.size2 = g2_gsl.tda = dim;
				std::cout << "With[{v1=Table[Unique[\"v1\"],{i,1," << n1 << "}],v2=Table[Unique[\"v2\"],{i,1," << n2 << "}],"
									<< "g1=" << Shapes::Helpers::mathematicaFormat( g1_gsl )
									<< ",g2=" << Shapes::Helpers::mathematicaFormat( g2_gsl )
									<< "}," << std::endl
									<< "  Sqrt[First[NMinimize[Join[{Norm[v1.g1-v2.g2]^2,Plus@@v1==1,Plus@@v2==1},Thread[Join[v1,v2]>=0]],Join[v1,v2]]]]]" << std::endl ;
			}
			break;
		case TASK_QP_TESTER_FORM:
			{
				gsl_matrix g1_gsl;
				g1_gsl.data = g1;
				g1_gsl.size1 = n1;
				g1_gsl.size2 = g1_gsl.tda = dim;
				gsl_matrix g2_gsl;
				g2_gsl.data = g2;
				g2_gsl.size1 = n2;
				g2_gsl.size2 = g2_gsl.tda = dim;
				gsl_matrix_scale( & g2_gsl, -1 );
				gsl_matrix * H_gsl = gsl_matrix_alloc( n1 + n2, n1 + n2 );
				std::cout << std::showpoint ;
				std::cout << "Problem generated by polytope distance tester program." << std::endl
									<< "=============" << std::endl ;
				std::cout << n1 + n2 << " " << 2 << " " << 2 << std::endl ;
				/* Lower box constraints at 0. */
				for( size_t i = 0; i < n1 + n2; ++i )
					{
						std::cout << std::setw( 9 ) << 0.0 << " " ;
					}
				/* No upper box constraints. */
				std::cout << std::endl
									<< "#" << std::endl ;
				/* Quadratic cost. */
				for( size_t row = 0; row < n1 + n2; ++row )
					{
						gsl_vector_view x1;
						if( row < n1 )
							{
								x1 = gsl_matrix_row( & g1_gsl, row );
							}
						else
							{
								x1 = gsl_matrix_row( & g2_gsl, row - n1 );
							}
						for( size_t col = 0; col < n1 + n2; ++col )
							{
								gsl_vector_view x2;
								if( col < n1 )
									{
										x2 = gsl_matrix_row( & g1_gsl, col );
									}
								else
									{
										x2 = gsl_matrix_row( & g2_gsl, col - n1 );
									}
								std::cout << std::setw( 9 ) ;
								if( col < row )
									{
										std::cout << "#   " ;
									}
								else
									{
										double tmp;
										gsl_blas_ddot( & x1.vector, & x2.vector, & tmp );
										std::cout << tmp ;
									}
								std::cout << " " ;
							}
						std::cout << std::endl ;
					}
				/* Linear cost. */
				for( size_t i = 0; i < n1 + n2; ++i )
					{
						std::cout << std::setw( 9 ) << 0.0 << " " ;
					}
				std::cout << std::endl ;
				/* Linear equality constraint for first polytope. */
				for( size_t i = 0; i < n1 + n2; ++i )
					{
						std::cout << std::setw( 9 ) << ( (i<n1) ? 1.0 : 0.0 ) << " " ;
					}
				std::cout << std::setw( 9 ) << 1.0 << std::endl ;
				/* Linear equality constraint for second polytope. */
				for( size_t i = 0; i < n1 + n2; ++i )
					{
						std::cout << std::setw( 9 ) << ( (i<n1) ? 0.0 : 1.0 ) << " " ;
					}
				std::cout << std::setw( 9 ) << 1.0 << std::endl ;
				gsl_matrix_free( H_gsl );
			}
			break;
		case TASK_SHAPES_FIGURE:
			{
				switch( dim )
					{
					case 2:
						{
							{
								std::cout << std::fixed << "poly1: ";
								const double * begin = g1;
								const double * end = begin + dim * n1;
								for( const double * gSrc = begin; gSrc != end; gSrc += dim )
									{
										if( gSrc != begin )
											{
												std::cout << "--" ;
											}
										std::cout << "(" ;
										for( const double * src = gSrc; src != gSrc + dim; ++src )
											{
												if( src != gSrc )
													{
														std::cout << "," ;
													}
												std::cout << "(" << *src << "bp)" ;
											}
										std::cout << ")" ;
									}
								std::cout << std::endl ;
							}
							{
								std::cout << "poly2: ";
								const double * begin = g2;
								const double * end = begin + dim * n2;
								for( const double * gSrc = begin; gSrc != end; gSrc += dim )
									{
										if( gSrc != begin )
											{
												std::cout << "--" ;
											}
										std::cout << "(" ;
										for( const double * src = gSrc; src != gSrc + dim; ++src )
											{
												if( src != gSrc )
													{
														std::cout << "," ;
													}
												std::cout << "(" << *src << "bp)" ;
											}
										std::cout << ")" ;
									}
								std::cout << std::endl ;
							}

							std::cout << "#page << @stroking:RGB_RED&@width:2bp | [[range '0 [duration poly1]].foldl \\ p e → p & [spot [poly1 1*e].p] null]" << std::endl
												<< "      << @stroking:RGB_GREEN&@width:2bp | [[range '0 [duration poly2]].foldl \\ p e → p & [spot [poly2 1*e].p] null]" << std::endl
												<< "      << @dash:[Traits..dashpattern 2bp 2bp]&@width:0.3bp | [stroke [controlling_hull poly1] & [controlling_hull poly2]]" << std::endl ;
						}
						break;
					case 3:
						{
							std::cout << std::fixed << "poly1: @nonstrokingalpha:[alphaopacity 0.5] & @nonstroking:[rgb 0.7 0.3 0.3] | ( Graphics3D..newGroup" ;
							{
								const double * begin = g1;
								const double * end = begin + dim * n1;
								for( const double * exclude = begin; exclude != end; exclude += dim )
									{
										std::cout << " << [fill " ;
										for( const double * gSrc = begin; gSrc != end; gSrc += dim )
											{
												if( gSrc == exclude )
													{
														continue;
													}
												std::cout << "(" ;
												for( const double * src = gSrc; src != gSrc + dim; ++src )
													{
														if( src != gSrc )
															{
																std::cout << "," ;
															}
														std::cout << "(" << *src << "bp)" ;
													}
												std::cout << ")--" ;
											}
										std::cout << "cycle]" ;
									}
							}
							std::cout << ")" << std::endl ;
							std::cout << "poly2: @nonstrokingalpha:[alphaopacity 0.5] & @nonstroking:[rgb 0.3 0.7 0.3] | ( Graphics3D..newGroup" ;
							{
								const double * begin = g2;
								const double * end = begin + dim * n1;
								for( const double * exclude = begin; exclude != end; exclude += dim )
									{
										std::cout << " << [fill " ;
										for( const double * gSrc = begin; gSrc != end; gSrc += dim )
											{
												if( gSrc == exclude )
													{
														continue;
													}
												std::cout << "(" ;
												for( const double * src = gSrc; src != gSrc + dim; ++src )
													{
														if( src != gSrc )
															{
																std::cout << "," ;
															}
														std::cout << "(" << *src << "bp)" ;
													}
												std::cout << ")--" ;
											}
										std::cout << "cycle]" ;
									}
							}
							std::cout << ")" << std::endl ;
							std::cout << "#page << [view poly1 & poly2]" << std::endl ;
						}
						break;
					default:
						std::cerr << "Dimension not supported by SHAPES_FIG mode: " << dim << std::endl ;
						exit( 1 );
					}
			}
			break;
		default:
			std::cout << "Unknown task." << std::endl ;
		}

  return 0;
}


bool
strprefixcmp( char * str, const char * prefix, char ** endp )
{
	int len = strlen( prefix );
	bool res = ( strncmp( str, prefix, len ) == 0 );
	*endp = str + len;
	return res;
}
