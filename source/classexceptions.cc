/* This file is part of Shapes.
 *
 * Shapes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Shapes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2008, 2009, 2013 Henrik Tidefelt
 */

#include "classexceptions.h"
#include "astclass.h"

using namespace Shapes;
using namespace std;


Exceptions::IllegalFinalReference::IllegalFinalReference( RefCountPtr< const char > valueType, const char * fieldID )
	: Exceptions::RuntimeError( Ast::THE_UNKNOWN_LOCATION ), valueType_( valueType ), fieldID_( fieldID )
{ }

Exceptions::IllegalFinalReference::~IllegalFinalReference( )
{ }

void
Exceptions::IllegalFinalReference::display( std::ostream & os ) const
{
	os << "Attempt to access field of non-final class " << valueType_ << " without home class.  Use <HomeClass>#" << fieldID_ << " instead." << std::endl ;
}


Exceptions::NonExistentMember::NonExistentMember( RefCountPtr< const char > valueType, const char * fieldID )
	: Exceptions::RuntimeError( Ast::THE_UNKNOWN_LOCATION ), valueType_( valueType ), fieldID_( fieldID )
{ }

Exceptions::NonExistentMember::~NonExistentMember( )
{ }

void
Exceptions::NonExistentMember::display( std::ostream & os ) const
{
	os << "Class " << valueType_ << " has no (non-private) field called " << fieldID_ << "." << std::endl ;
}


Exceptions::NonExistentMutator::NonExistentMutator( RefCountPtr< const char > valueType, const char * mutatorID )
	: Exceptions::RuntimeError( Ast::THE_UNKNOWN_LOCATION ), valueType_( valueType ), mutatorID_( mutatorID )
{ }

Exceptions::NonExistentMutator::~NonExistentMutator( )
{ }

void
Exceptions::NonExistentMutator::display( std::ostream & os ) const
{
	os << "State type " << valueType_ << " has no mutator called " << mutatorID_ << "." << std::endl ;
}


Exceptions::ProtectedMemberPublicScope::ProtectedMemberPublicScope( RefCountPtr< const char > valueType, const char * fieldID )
	: Exceptions::RuntimeError( Ast::THE_UNKNOWN_LOCATION ), valueType_( valueType ), fieldID_( fieldID )
{ }

Exceptions::ProtectedMemberPublicScope::~ProtectedMemberPublicScope( )
{ }

void
Exceptions::ProtectedMemberPublicScope::display( std::ostream & os ) const
{
	os << "Trying to access protected member " << fieldID_ << " of class " << valueType_ << " via public instance." << std::endl ;
}


Exceptions::MemberNotAssignable::MemberNotAssignable( RefCountPtr< const char > valueType, const char * fieldID, RefCountPtr< const char > scope )
	: Exceptions::RuntimeError( Ast::THE_UNKNOWN_LOCATION ), valueType_( valueType ), fieldID_( fieldID ), scope_( scope )
{ }

Exceptions::MemberNotAssignable::~MemberNotAssignable( )
{ }

void
Exceptions::MemberNotAssignable::display( std::ostream & os ) const
{
	os << "In " << scope_ << " scope of " << valueType_ << " :  The member " << fieldID_ << " is not assignable." << std::endl ;
}


Exceptions::InstantiatingAbstractClass::InstantiatingAbstractClass( RefCountPtr< const Lang::Class > cls )
	: Exceptions::RuntimeError( Ast::THE_UNKNOWN_LOCATION ), cls_( cls )
{ }

Exceptions::InstantiatingAbstractClass::~InstantiatingAbstractClass( )
{ }

void
Exceptions::InstantiatingAbstractClass::display( ostream & os ) const
{
	os << "Trying to instantiate abstract class: " << cls_->getPrettyName( ) << std::endl ;
}


Exceptions::FailedToDeclareClassAbstract::FailedToDeclareClassAbstract( RefCountPtr< const Lang::Class > cls, const Shapes::Ast::ClassFunction * classExpr )
	: Exceptions::RuntimeError( Ast::THE_UNKNOWN_LOCATION ), cls_( cls ), classExpr_( classExpr )
{ }

Exceptions::FailedToDeclareClassAbstract::~FailedToDeclareClassAbstract( )
{ }

void
Exceptions::FailedToDeclareClassAbstract::display( ostream & os ) const
{
	os << classExpr_->loc( ) << " " << "Class " << cls_->getPrettyName( ) << " has abstract methods but is not declared abstract: " ;
	cls_->showAbstractSet( os );
	os << std::endl ;
}


Exceptions::RepeatedImmediateParent::RepeatedImmediateParent( RefCountPtr< const Lang::Class > cls, RefCountPtr< const Lang::Class > parent )
	: Exceptions::RuntimeError( Ast::THE_UNKNOWN_LOCATION ), cls_( cls ), parent_( parent )
{ }

Exceptions::RepeatedImmediateParent::~RepeatedImmediateParent( )
{ }

void
Exceptions::RepeatedImmediateParent::display( ostream & os ) const
{
	os << "When creating the class " << cls_->getPrettyName( ) << ", the following class is a repeated immediate parent: " << parent_->getPrettyName( ) << std::endl ;
}


Exceptions::OverridingNonParent::OverridingNonParent( RefCountPtr< const Lang::Class > cls, RefCountPtr< const Lang::Class > parent )
	: Exceptions::RuntimeError( Ast::THE_UNKNOWN_LOCATION ), cls_( cls ), parent_( parent )
{ }

Exceptions::OverridingNonParent::~OverridingNonParent( )
{ }

void
Exceptions::OverridingNonParent::display( ostream & os ) const
{
	os << "The class " << cls_->getPrettyName( ) << " cannot override methods from the class " << parent_->getPrettyName( ) << ", since the latter is not a parent of the former." << std::endl ;
}


Exceptions::InheritingFinal::InheritingFinal( RefCountPtr< const Lang::Class > cls, RefCountPtr< const Lang::Class > parent )
	: Exceptions::RuntimeError( Ast::THE_UNKNOWN_LOCATION ), cls_( cls ), parent_( parent )
{ }

Exceptions::InheritingFinal::~InheritingFinal( )
{ }

void
Exceptions::InheritingFinal::display( ostream & os ) const
{
	os << "When creating the class " << cls_->getPrettyName( ) << ", trying to inherit from final class: " << parent_->getPrettyName( ) << std::endl ;
}


Exceptions::OverridingUndeclaredMethod::OverridingUndeclaredMethod( RefCountPtr< const Lang::Class > cls, RefCountPtr< const Lang::Class > parent, const char * id )
	: Exceptions::RuntimeError( Ast::THE_UNKNOWN_LOCATION ), cls_( cls ), parent_( parent ), id_( id )
{ }

Exceptions::OverridingUndeclaredMethod::~OverridingUndeclaredMethod( )
{ }

void
Exceptions::OverridingUndeclaredMethod::display( ostream & os ) const
{
	os << "In override declaration in class " << cls_->getPrettyName( ) << ": there is no method called " << id_ << " in class " << parent_->getPrettyName( ) << " to override." << std::endl ;
}


Exceptions::OverridingFinalMethod::OverridingFinalMethod( RefCountPtr< const Lang::Class > cls, RefCountPtr< const Lang::Class > parent, const char * id )
	: Exceptions::RuntimeError( Ast::THE_UNKNOWN_LOCATION ), cls_( cls ), parent_( parent ), id_( id )
{ }

Exceptions::OverridingFinalMethod::~OverridingFinalMethod( )
{ }

void
Exceptions::OverridingFinalMethod::display( ostream & os ) const
{
	os << "In override declaration in class " << cls_->getPrettyName( ) << ": The method " << id_ << " in class " << parent_->getPrettyName( ) << " is final." << std::endl ;
}


Exceptions::IllegalRepeatedBase::IllegalRepeatedBase( RefCountPtr< const Lang::Class > cls, RefCountPtr< const Lang::Class > parent )
	: Exceptions::RuntimeError( Ast::THE_UNKNOWN_LOCATION ), cls_( cls ), parent_( parent )
{ }

Exceptions::IllegalRepeatedBase::~IllegalRepeatedBase( )
{ }

void
Exceptions::IllegalRepeatedBase::display( ostream & os ) const
{
	os << "When creating the class " << cls_->getPrettyName( ) << ", the following repeated base class may not be repeated: " << parent_->getPrettyName( ) << std::endl ;
}


Exceptions::AmbiguousInheritedMethod::AmbiguousInheritedMethod( RefCountPtr< const Lang::Class > cls, RefCountPtr< const char > id, std::set< RefCountPtr< const Lang::Class > > parents )
	: Exceptions::RuntimeError( Ast::THE_UNKNOWN_LOCATION ), cls_( cls ), id_( id ), parents_( parents )
{ }

Exceptions::AmbiguousInheritedMethod::~AmbiguousInheritedMethod( )
{ }

void
Exceptions::AmbiguousInheritedMethod::display( ostream & os ) const
{
	os << "In class " << cls_->getPrettyName( ) << ": the method name " << id_ << " is ambiguous, as it is inherited from more than one class:" ;
	for( std::set< RefCountPtr< const Lang::Class > >::const_iterator i = parents_.begin( ); i != parents_.end( ); ++i )
		{
			os << " " << (*i)->getPrettyName( ) ;
		}
	os << std::endl ;
}


Exceptions::MisplacedSuperReference::MisplacedSuperReference( const Ast::SourceLocation & loc )
	: Exceptions::RuntimeError( loc )
{ }

Exceptions::MisplacedSuperReference::~MisplacedSuperReference( )
{ }

void
Exceptions::MisplacedSuperReference::display( std::ostream & os ) const
{
	os << "Misplaced reference to parent instance." << std::endl ;
}


Exceptions::SuperReferenceClassNotParent::SuperReferenceClassNotParent( RefCountPtr< const Lang::Class > cls, RefCountPtr< const Lang::Class > parent )
	: Exceptions::RuntimeError( Ast::THE_UNKNOWN_LOCATION ), cls_( cls ), parent_( parent )
{ }

Exceptions::SuperReferenceClassNotParent::~SuperReferenceClassNotParent( )
{ }

void
Exceptions::SuperReferenceClassNotParent::display( std::ostream & os ) const
{
	os << "When referencing super instance, " << parent_->getPrettyName( ) << " is not a parent of " << cls_->getPrettyName( ) << "." << std::endl ;
}


Exceptions::NoSuchMethod::NoSuchMethod( RefCountPtr< const Lang::Class > cls, const Kernel::MethodId & method )
	: Exceptions::RuntimeError( Ast::THE_UNKNOWN_LOCATION ), cls_( cls ), method_( method )
{ }

Exceptions::NoSuchMethod::~NoSuchMethod( )
{ }

void
Exceptions::NoSuchMethod::display( std::ostream & os ) const
{
	os << "Class " << cls_->getPrettyName( ) << " has no method called " << method_.prettyName( ) << std::endl ;
}


Exceptions::NoSuchLocalMethod::NoSuchLocalMethod( RefCountPtr< const Lang::Class > cls, const Kernel::MethodId & method )
	: Exceptions::RuntimeError( Ast::THE_UNKNOWN_LOCATION ), cls_( cls ), method_( method )
{ }

Exceptions::NoSuchLocalMethod::~NoSuchLocalMethod( )
{ }

void
Exceptions::NoSuchLocalMethod::display( std::ostream & os ) const
{
	os << "Class " << cls_->getPrettyName( ) << " does not itself define the method " << method_.prettyName( ) << std::endl ;
}
