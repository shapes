/* This file is part of Shapes.
 *
 * Shapes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Shapes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2008, 2010, 2013, 2014, 2015 Henrik Tidefelt
 */

#include "shapesscanner.h"
#include "yyltype.h"

#include "shapestypes.h"
#include "ast.h"
#include "astflow.h"
#include "astclass.h"
#include "glyphlist.h"
#include "globals.h"
#include "shapesexceptions.h"

using namespace Shapes;
#include "shapesparser.hh"

#include <iostream>

using namespace std;

bool
operator < ( const NeededFile & n1, const NeededFile & n2 )
{
	int cmp = Ast::compare( *(n1.first), *(n2.first) );
	if( cmp != 0 )
		return cmp < 0;
	return *(n1.second) < *(n2.second);
}


ShapesScanner::ShapesScanner( )
	: yyFlexLexer( 0, & std::cerr ), moreState_( false ), lastleng_( 0 ),
		bracketDepth_( 0 ),
	  searchContext_( new Ast::SearchContext( Ast::THE_EMPTY_NAMESPACE_PATH, Ast::SearchContext::makePrivateName( 0 ) ) ), uniqueNamespaceNumber_( 1 ),
		showFiles_( false ), randSeedSet_( false ), interactive_( false ),
		namespace_( Ast::NamespaceReference::ABSOLUTE, new Ast::NamespacePath( ) )
{
	shapeslloc = Ast::THE_UNKNOWN_LOCATION;

	angleUnits_[ "rad" ] = 1;
	angleUnits_[ "deg" ] = M_PI / 180;
	angleUnits_[ "°" ] = M_PI / 180;
	angleUnits_[ "grad" ] = M_PI / 200;

	lengthUnits_[ "bp" ] = 1;
	lengthUnits_[ "mm" ] = 0.1 * 72 / 2.54;
	lengthUnits_[ "cm" ] = 72 / 2.54;
	lengthUnits_[ "m" ] = 100 * 72 / 2.54;
	lengthUnits_[ "in" ] = 72;

	namespaceLimits_.push( 0 );
	loadStackSizeStack_.push( 0 );
	stateStackSizeStack_.push( 0 );
}

ShapesScanner::~ShapesScanner( )
{
	while( ! locStack_.empty( ) )
		{
			delete locStack_.top( );
			locStack_.pop( );
		}
}

void
ShapesScanner::setInteractive( bool interactive )
{
	interactive_ = interactive;
}

// The following method is placed in shapesyylex to access YY_BUF_SIZE
// ShapesScanner::start( )

void
ShapesScanner::setSourceDir( const std::string & sourceDir )
{
	sourceDir_ = sourceDir;
}

void
ShapesScanner::push_backNeedPath( const std::string & path )
{
  if( path.empty( ) ){
    Interaction::warn_or_push( new Exceptions::InvocationError( "Ignoring empty need entry on need path.", true ), & Ast::theAnalysisErrorsList );
  }if( path == "/" ){
    /* If the path ends in a '/', it is also a leading '/'. */
    needSearchPath_.push_back( path );
    loader_.registerNeedDirectory( path );
  }else if( path[ path.size( ) - 1 ] == '/' ){
    needSearchPath_.push_back( path );
    loader_.registerNeedDirectory( path.substr( 0, path.size( ) - 1 ) );
  }else{
    needSearchPath_.push_back( path + "/" );
    loader_.registerNeedDirectory( path );
  }
}

void
ShapesScanner::push_frontNeedPath( const std::string & path )
{
	if( path.empty( ) ||
			path[ path.size( ) - 1 ] == '/' )
		{
			needSearchPath_.push_front( path );
		}
	else
		{
			needSearchPath_.push_front( path + "/" );
		}
}

void
ShapesScanner::pop_frontNeedPath( )
{
	needSearchPath_.pop_front( );
}

void
ShapesScanner::setShowFiles( bool showFiles )
{
	showFiles_ = showFiles;
}


void
ShapesScanner::more( )
{
	moreState_ = true; /* This one is for ourselves to use in doBeforeEachAction. */
	yy_more_flag = 1; /* This one is for flex, and will be reset before we reach doBeforeEachAction. */
	lastleng_ = yyleng;
}

void
ShapesScanner::doBeforeEachAction( )
{
	if( moreState_ )
		{
			shapeslloc.lastColumn += yyleng - lastleng_;
		}
	else
		{
			shapeslloc.firstLine = shapeslloc.lastLine;
			shapeslloc.firstColumn = shapeslloc.lastColumn;
			shapeslloc.lastColumn = shapeslloc.firstColumn + yyleng;
		}
	moreState_ = false;
}

double
ShapesScanner::lookupLengthUnitFactor( const char * name ) const
{
	typedef typeof lengthUnits_ MapType;
	MapType::const_iterator i = lengthUnits_.find( name );
	if( i == lengthUnits_.end( ) )
		{
			return -1;
		}
	return 1 / i->second;
}

Concrete::Length
ShapesScanner::strtoLength( const char * str ) const
{
	char * endp;
	double scalar = strtod( str, &endp );
	typedef typeof lengthUnits_ MapType;
	MapType::const_iterator i = lengthUnits_.find( endp );
	if( i == lengthUnits_.end( ) )
		{
			throw "Malformed length.";
		}
	return scalar * i->second;
}

RefCountPtr< const char >
ShapesScanner::newPrivateName( )
{
	RefCountPtr< const char > result = Ast::SearchContext::makePrivateName( uniqueNamespaceNumber_ );
	++uniqueNamespaceNumber_;
	return result;
}

RefCountPtr< const char >
ShapesScanner::newEncapsulationName( )
{
	RefCountPtr< const char > result = Ast::SearchContext::makeEncapsulationName( uniqueNamespaceNumber_ );
	++uniqueNamespaceNumber_;
	return result;
}

void
ShapesScanner::setNamespace( char * str )
{
	char * src = str;

	Ast::NamespaceReference::Base base = Ast::NamespaceReference::RELATIVE;
	switch( *src ){
	case '^':
		base = Ast::NamespaceReference::LOCAL;
		src += 2;
		break;
	case '.':
		base = Ast::NamespaceReference::ABSOLUTE;
		src += 2;
		break;
	default:
		break;
	}

	Ast::NamespacePath * path = new Ast::NamespacePath( );
	char * last;
	for( char * tok = strtok_r( src, ".", & last ); tok != NULL; tok = strtok_r( NULL, ".", & last ) ){
		path->push_back( strdup( tok ) );
	}

	namespace_ = Ast::NamespaceReference( base, path );
}

/* This internal helper function is only meant to work with well-formed identifiers.
 */
Ast::Identifier *
ShapesScanner::prependNamespace( const char * idstr ) const
{
	return new Ast::Identifier( searchContext_, namespace_, strdup( idstr ) );
}

Shapes::Ast::Identifier *
ShapesScanner::simpleIdentifier( const char * idstr ) const
{
	return new Ast::Identifier( searchContext_, Ast::NamespaceReference::RELATIVE, Ast::THE_EMPTY_NAMESPACE_PATH, strdup( idstr ) );
}

Shapes::Ast::PlacedIdentifier *
ShapesScanner::placedIdentifier( const char * idstr ) const
{
	return new Ast::PlacedIdentifier( searchContext_->lexicalPath( ), strdup( idstr ) );
}

RefCountPtr< const Ast::NamespacePath >
ShapesScanner::resolvedNamespace( ) const
{
	if( namespace_.base( ) == Ast::NamespaceReference::ABSOLUTE )
		return namespace_.pathRef( );

	Ast::NamespacePath * resPtr( new Ast::NamespacePath( *(searchContext_->lexicalPath( )) ) );
	if( namespace_.base( ) == Ast::NamespaceReference::LOCAL ){
		while ( ! resPtr->empty( ) && ! Ast::SearchContext::isEncapsulationName( resPtr->back( ) ) )
			resPtr->pop_back( );
	}

	for (Ast::NamespacePath::const_iterator i = namespace_.path( ).begin( ); i != namespace_.path( ).end( ); ++i ){
		resPtr->push_back( *i );
	}
	return RefCountPtr< const Ast::NamespacePath >( resPtr );
}

std::string
ShapesScanner::searchFile( const std::string & filename, bool runtime ) const
{
	struct stat theStatDummy;
	std::string res = searchFile( filename, & theStatDummy, runtime );
	if( showFiles_ )
		{
			for( size_t i = 0; i < locStack_.size( ); ++i )
				{
					std::cerr << "  " ;
				}
			std::cerr << "  " << res << std::endl ;
		}
	return res;
}

std::string
ShapesScanner::searchFile( const std::string & filename, struct stat * dstStat, bool runtime ) const
{
	std::string res;

	if( filename.empty( ) )
		{
			if( runtime )
				{
					throw strrefdup( "Empty file name." ); /* This string should be embedded in a catchable exception by the caller. */
				}
			else
				{
					throw Exceptions::InternalError( strrefdup( "ShapesScanner::searchFile called with empty argument." ) );
				}
		}

	if( filename[ 0 ] == '/' )
		{
			res = filename;
			if( stat( res.c_str( ), dstStat ) == 0 )
				{
					return res;
				}
			if( runtime )
				{
					std::ostringstream msg;
					msg << "The absolute filename \"" << filename << "\" does not name a file." ;
					throw strrefdup( msg ); /* This string should be embedded in a catchable exception by the caller. */
				}
			else
				{
					throw Exceptions::FileReadOpenError( shapeslloc, strrefdup( filename ), 0, 0 );
				}
		}

	if( needSearchPath_.empty( ) )
		{
			if( runtime )
				{
					std::ostringstream msg;
					msg << "The relative filename \"" << filename << "\" cannot be searched since the search path is empty." ;
					throw strrefdup( msg ); /* This string should be embedded in a catchable exception by the caller. */
				}
			else
				{
					throw Exceptions::ScannerError( shapeslloc, strrefdup( "Relative file inclusion impossible since search path is empty." ) );
				}
		}

	typedef typeof needSearchPath_ ListType;
	for( ListType::const_iterator i = needSearchPath_.begin( ); i != needSearchPath_.end( ); ++i )
		{
			res = needpathWithSuffix( *i, filename );
			if( stat( res.c_str( ), dstStat ) == 0 )
				{
					return res;
				}
		}
	if( runtime )
		{
			std::ostringstream msg;
			msg << "The relative filename \"" << filename << "\" did not name a file in the search path." ;
			throw strrefdup( msg ); /* This string should be embedded in a catchable exception by the caller. */
		}
	else
		{
			throw Exceptions::FileReadOpenError( shapeslloc, strrefdup( filename ), & sourceDir_, & needSearchPath_ );
		}
}

std::string
ShapesScanner::needpathWithSuffix( const std::string & needpath, const std::string & suffix ) const
{
	if( needpath[0] == '/' )
		{
			return needpath + suffix;
		}
	else if( needpath == "./" )
		{
			return sourceDir_ + suffix;
		}
	else
		{
			return sourceDir_ + needpath + suffix;
		}
}

void
ShapesScanner::rinseString( )
{
	/* Both types of strings are terminated by a two byte sequence, ") or ´. */
	size_t bytecount = yyleng - ( ( yyleng >= 3 && yytext[ yyleng - 3 ] == '\n' ) ? 3 : 2 );
	char * res = new char[ bytecount + 1 ];
	memcpy( res, yytext, bytecount );
	res[ bytecount ] = '\0';
	shapeslval.Lang_String = new Lang::String( RefCountPtr< const char >( res ), bytecount );
}

void
ShapesScanner::concatenateDataString( )
{
	/* Remember: strcpy, strdup and friends may fail here, since the string may contain zeros. */

	char * res = new char[ dataStringTotalLength_ + 1 ];
	char * dst = res;
	while( ! dataStringChunks_.empty( ) )
		{
			char * ptr = dataStringChunks_.front( ).first;
			memcpy( dst, ptr, dataStringChunks_.front( ).second );
			dst += dataStringChunks_.front( ).second;
			delete ptr;
			dataStringChunks_.pop_front( );
		}
	*dst = '\0';
	shapeslval.Lang_String = new Lang::String( RefCountPtr< const char >( res ), dataStringTotalLength_ );
}

uint32_t
ShapesScanner::unicodeFromGlyphname( const char * name )
{
	FontMetrics::GlyphList::UnicodeType res;
	const FontMetrics::GlyphList & glyphList = Helpers::requireGlyphList( );
	if( ! glyphList.name_to_UCS4( name, & res ) )
		{
			std::ostringstream msg;
			msg << "Unrecognized glyph name: " << name ;
			Ast::theAnalysisErrorsList.push_back( new Exceptions::ScannerError( shapeslloc, strrefdup( msg ) ) );
		}
	return res;
}
