/* This file is part of Shapes.
 *
 * Shapes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Shapes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2013, 2014 Henrik Tidefelt
 */

#pragma once

#include <list>
#include <set>
#include <vector>

#include "ptrowner.h"
#include "refcount.h"
#include "shapesvalue.h"
#include "environment.h"
#include "charptrless.h"
#include "elementarytypes.h"
#include "containertypes.h"

namespace Shapes
{
	namespace Kernel
	{

		class Edge
		{
			/* Rather than storing whether an edge is directed or not as a flag here, directed and undirected edges are always kept apart.
			 * Lang::Edge, on the other hand, will contain this information explicitly.
			 */
			Kernel::Node * source_;
			Kernel::Node * target_;
			size_t index_;
		public:
			Edge( Kernel::Node * source, Kernel::Node * target, size_t index, bool directed );

			size_t index( ) const { return index_; }

			const Kernel::Node * source( ) const { return source_; }
			const Kernel::Node * target( ) const { return target_; }
			Kernel::Node * source( ) { return source_; }
			Kernel::Node * target( ) { return target_; }
		};

		class EdgePtrLessSource
		{
		public:
			bool operator () ( const Edge * x, const Edge * y ) const;
		};

		class EdgePtrLessTarget
		{
		public:
			bool operator () ( const Edge * x, const Edge * y ) const;
		};

		class EdgePredicate
		{
		protected:
			bool always_false_;
			bool always_true_;
		public:
			EdgePredicate( );
			virtual ~EdgePredicate( );
			virtual bool operator () ( const Edge & e ) const = 0;
			bool always_false( ) const { return always_false_; }
			bool always_true( ) const { return always_true_; }
		};

		template< class T >
		class EdgeLabelPredicate : public EdgePredicate
		{
			RefCountPtr< std::vector< Kernel::ValueRef > > edgeLabels_;
			T label_;
		public:
			EdgeLabelPredicate( const RefCountPtr< std::vector< Kernel::ValueRef > > & edgeLabels, const T & label );
			virtual ~EdgeLabelPredicate( );
			virtual bool operator () ( const Edge & e ) const;
		};

		class Node
		{
		public:
			typedef RefCountPtr< const Lang::SingleList > ListRef;
			/* The containers types EdgeSetSource and EdgeSetTarget can be either std::set
			 * or std::multiset, depending on whether the comparison will treat parallel edges
			 * equal or not (parallel edges can always be distinguished using the edge index).
			 * Treating parallel edges as different (using std::set) allows the edges to be
			 * stored in a canonical order.  Treating them as equal (using std::multiset) makes
			 * it easy to access the parallel edges as an equal range.  However, with std::set
			 * it is still easy to access the range of parallel edges, since it is easy to construct
			 * the elements to use with lower_bound and upper_bound that will do the job.
			 */
			typedef std::set< const Kernel::Edge *, EdgePtrLessSource > EdgeSetSource;
			typedef std::set< const Kernel::Edge *, EdgePtrLessTarget > EdgeSetTarget;

		private:
			EdgeSetSource uedgesLow_; /* Adjacent node has lower index. */
			EdgeSetTarget uedgesHigh_; /* Adjacent node has higher index. */
			EdgeSetSource uloops_;
			EdgeSetSource dedgesIn_;
			EdgeSetTarget dedgesOut_;
			EdgeSetSource dloops_;
			size_t index_;
			RefCountPtr< const Lang::Value > key_;
		public:
			Node( size_t index, const RefCountPtr< const Lang::Value > & key )
				: index_( index ), key_( key )
			{ }

			size_t index( ) const { return index_; }
			const RefCountPtr< const Lang::Value > & key( ) const { return key_; }

			void show( std::ostream & os ) const;

			void add_uedgeLow( Kernel::Edge * edge ) { uedgesLow_.insert( edge ); }
			void add_uedgeHigh( Kernel::Edge * edge ) { uedgesHigh_.insert( edge ); }
			void add_uloop( Kernel::Edge * edge ) { uloops_.insert( edge ); }
			void add_dedgeIn( Kernel::Edge * edge ) { dedgesIn_.insert( edge ); }
			void add_dedgeOut( Kernel::Edge * edge ) { dedgesOut_.insert( edge ); }
			void add_dloop( Kernel::Edge * edge ) { dloops_.insert( edge ); }

			size_t u_degree( ) const { return uedgesLow_.size( ) + uedgesHigh_.size( ) + uloops_.size( ); }
			size_t d_degree_in( ) const { return dedgesIn_.size( ) + dloops_.size( ); }
			size_t d_degree_out( ) const { return dedgesOut_.size( ) + dloops_.size( ); }
			size_t d_degree( ) const { return dedgesIn_.size( ) + dedgesOut_.size( ) + 2 * dloops_.size( ); }
			size_t degree( ) const { return uedgesLow_.size( ) + uedgesHigh_.size( ) + uloops_.size( ) + dedgesIn_.size( ) + dedgesOut_.size( ) + 2 * dloops_.size( ); }
			size_t u_loop_count( ) const { return uloops_.size( ); }
			size_t d_loop_count( ) const { return dloops_.size( ); }
			size_t loop_count( ) const { return uloops_.size( ) + dloops_.size( ); }

			ListRef prepend_uedges( const ListRef & rest, const RefCountPtr< const Lang::Graph > & graph, const Kernel::EdgePredicate * filter = 0 ) const;
			ListRef prepend_dedgesIn( const ListRef & rest, const RefCountPtr< const Lang::Graph > & graph, const Kernel::EdgePredicate * filter = 0 ) const;
			ListRef prepend_dedgesOut( const ListRef & rest, const RefCountPtr< const Lang::Graph > & graph, const Kernel::EdgePredicate * filter = 0 ) const;
			ListRef prepend_uloops( const ListRef & rest, const RefCountPtr< const Lang::Graph > & graph, const Kernel::EdgePredicate * filter = 0 ) const;
			ListRef prepend_dloops( const ListRef & rest, const RefCountPtr< const Lang::Graph > & graph, const Kernel::EdgePredicate * filter = 0 ) const;

			ListRef prepend_uedges_to( Kernel::Node * neighbor, const ListRef & rest, const RefCountPtr< const Lang::Graph > & graph, const Kernel::EdgePredicate * filter = 0 ) const;
			ListRef prepend_dedges_from( Kernel::Node * source, const ListRef & rest, const RefCountPtr< const Lang::Graph > & graph, const Kernel::EdgePredicate * filter = 0 ) const;
			ListRef prepend_dedges_to( Kernel::Node * target, const ListRef & rest, const RefCountPtr< const Lang::Graph > & graph, const Kernel::EdgePredicate * filter = 0 ) const;

			ListRef prepend_uedgeNeighbors( const ListRef & rest, const RefCountPtr< const Lang::Graph > & graph ) const;
			ListRef prepend_dedgeNeighborsIn( const ListRef & rest, const RefCountPtr< const Lang::Graph > & graph ) const;
			ListRef prepend_dedgeNeighborsOut( const ListRef & rest, const RefCountPtr< const Lang::Graph > & graph ) const;
			ListRef prepend_uloopNeighbors( const ListRef & rest, const RefCountPtr< const Lang::Graph > & graph ) const;
			ListRef prepend_dloopNeighbors( const ListRef & rest, const RefCountPtr< const Lang::Graph > & graph ) const;

			ListRef prepend_unique_uedgeNeighbors( const ListRef & rest, const RefCountPtr< const Lang::Graph > & graph, std::set< size_t > * added ) const;
			ListRef prepend_unique_dedgeNeighborsIn( const ListRef & rest, const RefCountPtr< const Lang::Graph > & graph, std::set< size_t > * added ) const;
			ListRef prepend_unique_dedgeNeighborsOut( const ListRef & rest, const RefCountPtr< const Lang::Graph > & graph, std::set< size_t > * added ) const;
			ListRef prepend_unique_uloopNeighbors( const ListRef & rest, const RefCountPtr< const Lang::Graph > & graph, std::set< size_t > * added ) const;
			ListRef prepend_unique_dloopNeighbors( const ListRef & rest, const RefCountPtr< const Lang::Graph > & graph, std::set< size_t > * added ) const;

			/* Note on efficiency:  With the current representation of multiedges, there is a certain lack of symmetry
			 * in the methods below.  Both prepend_d_multiedgesOut and prepend_u_multiedgesHigh have much worse time
			 * complexicy than the other methods, as they need to find the edges as stored in adjacent nodes.
			 */
			ListRef prepend_u_multiedges( const ListRef & rest, const RefCountPtr< const Lang::Graph > & graph ) const;
			ListRef prepend_d_multiedgesIn( const ListRef & rest, const RefCountPtr< const Lang::Graph > & graph ) const;
			ListRef prepend_d_multiedgeInFrom( const Kernel::Node * source, const ListRef & rest, const RefCountPtr< const Lang::Graph > & graph ) const;
			ListRef prepend_d_multiedgesOut( const ListRef & rest, const RefCountPtr< const Lang::Graph > & graph ) const;
			ListRef prepend_u_multiloops( const ListRef & rest, const RefCountPtr< const Lang::Graph > & graph ) const;
			ListRef prepend_d_multiloops( const ListRef & rest, const RefCountPtr< const Lang::Graph > & graph ) const;
			ListRef prepend_u_multiedgesLow( const ListRef & rest, const RefCountPtr< const Lang::Graph > & graph ) const;
			ListRef prepend_u_multiedgeLowFrom( const Kernel::Node * source, const ListRef & rest, const RefCountPtr< const Lang::Graph > & graph ) const;
			ListRef prepend_u_multiedgesHigh( const ListRef & rest, const RefCountPtr< const Lang::Graph > & graph ) const;

		private:
			template< class T >
			static ListRef prepend_edges( const T & edges, bool directed, const ListRef & rest, const RefCountPtr< const Lang::Graph > & graph, const Kernel::EdgePredicate * filter );
			template< class T >
			static ListRef prepend_edges_neighbor( Kernel::Node * neighbor, const T & edges, bool directed, const ListRef & rest, const RefCountPtr< const Lang::Graph > & graph, const Kernel::EdgePredicate * filter );
		};

		inline
		bool
		EdgePtrLessSource::operator () ( const Edge * x, const Edge * y ) const
		{
			if( x->source( )->index( ) < y->source( )->index( ) )
				return true;
			if( x->source( )->index( ) > y->source( )->index( ) )
				return false;
			return x->index( ) < y->index( );
		}

		inline
		bool
		EdgePtrLessTarget::operator () ( const Edge * x, const Edge * y ) const
		{
			if( x->target( )->index( ) < y->target( )->index( ) )
				return true;
			if( x->target( )->index( ) > y->target( )->index( ) )
				return false;
			return x->index( ) < y->index( );
		}

		class GraphDomain
		{
			bool undirected_;
			bool directed_;
			bool loops_;
			bool parallel_;
		public:
			GraphDomain( )
				: undirected_( false ), directed_( false ), loops_( false ), parallel_( false )
			{ }

			void allowUndirected( ) { undirected_ = true; }
			void allowDirected( ) { directed_ = true; }
			void allowLoops( ) { loops_ = true; }
			void allowParallel( ) { parallel_ = true; }

			bool undirected( ) const { return undirected_; }
			bool directed( ) const { return directed_; }
			bool loops( ) const { return loops_; }
			bool parallel( ) const { return parallel_; }
		};

		class NodeData
		{
		public:
			Kernel::ValueRef key_;
			Kernel::ValueRef value_;
			Kernel::ValueRef partition_;
			NodeData( const Kernel::ValueRef & key, const Kernel::ValueRef & value, const Kernel::ValueRef & partition )
				: key_( key ), value_( value ), partition_( partition )
			{ }
		};

		class EdgeData
		{
		public:
			bool directed_;
			Kernel::ValueRef sourceKey_;
			Kernel::ValueRef targetKey_;
			Kernel::ValueRef value_;
			Kernel::ValueRef label_;
			EdgeData( bool directed, const Kernel::ValueRef & sourceKey, const Kernel::ValueRef & targetKey, const Kernel::ValueRef & value, const Kernel::ValueRef & label )
				: directed_( directed ), sourceKey_( sourceKey ), targetKey_( targetKey ), value_( value ), label_( label )
			{ }
		};

		class EdgeDataIndexed
		{
		public:
			/* The directed property is not stored, since directed and undirected edges should not be mixed in the same container. */
			size_t source_;
			size_t target_;
			Kernel::ValueRef value_;
			Kernel::ValueRef label_;
			EdgeDataIndexed( size_t source, size_t target, const Kernel::ValueRef & value, const Kernel::ValueRef & label )
				: source_( source ), target_( target ), value_( value ), label_( label )
			{ }
		};

		class LoopDataIndexed
		{
		public:
			/* The directed property is not stored, since directed and undirected edges should not be mixed in the same container. */
			size_t node_;
			Kernel::ValueRef value_;
			Kernel::ValueRef label_;
			LoopDataIndexed( size_t node, const Kernel::ValueRef & value, const Kernel::ValueRef & label )
				: node_( node ), value_( value ), label_( label )
			{ }
		};

		/* The Kernel::Graph only stores the graph structure.
		 * In particular, node and edge values are only stored in the Lang::Graph.
		 */
		class Graph
		{
		public:
			typedef std::vector< Kernel::Node * > NodeVector;
			typedef std::vector< Kernel::Edge * > EdgeVector;
			typedef RefCountPtr< const Lang::SingleList > ListRef;
			typedef std::vector< Kernel::ValueRef > ValueVector;
			enum SubgraphKind{ INDUCED, SPANNING };
			enum ItemType{ NODE, EDGE };

		private:
			typedef std::map< Lang::Symbol::KeyType, const Kernel::Node * > SymbolKeyMap;
			typedef std::map< Lang::Integer::ValueType, const Kernel::Node * > IntegerKeyMap;
			typedef std::map< Lang::Symbol::KeyType, NodeVector * > SymbolKeyPartitionMap;
			typedef std::map< Lang::Integer::ValueType, NodeVector * > IntegerKeyPartitionMap;

			Kernel::GraphDomain domain_;
			ListRef partitionKeys_; /* Partition keys in original order.  Null if graph is not partitioned. */
			NodeVector nodes_;
			EdgeVector uedges_;
			EdgeVector uloops_;
			EdgeVector dedges_;
			EdgeVector dloops_;
			bool keyIsRange_;
			Lang::Integer::ValueType keyOffset_;

			SymbolKeyMap symbolKeyMap_;
			IntegerKeyMap integerKeyMap_;

			RefCountPtr< ValueVector > edgeLabels_; /* Null if all edge labels are void. */

			RefCountPtr< ValueVector > nodePartitions_; /* The partition of each node.  Null if graph is not partitioned. */
			PtrOwner_back_Access< std::list< NodeVector * > > partitions_; /* Partition storage.  Empty if graph is not partitioned. */
			SymbolKeyPartitionMap symbolKeyPartitionMap_;
			IntegerKeyPartitionMap integerKeyPartitionMap_;

		public:
			Graph( const Kernel::GraphDomain & domain, const ListRef & partitionKeys, const std::list< Kernel::NodeData > & nodeData, bool hasEdgeLabels, const std::list< Kernel::EdgeDataIndexed > & uedgeData, const std::list< Kernel::EdgeDataIndexed > & dedgeData, const std::list< Kernel::LoopDataIndexed > & uloopData, const std::list< Kernel::LoopDataIndexed > & dloopData, RefCountPtr< const ValueVector > * nodeValues, RefCountPtr< const ValueVector > * edgeValues, const Ast::SourceLocation & callLoc ); /* Generally invoked by Helpers::graphFromNodeEdgeData. */
			Graph( const Kernel::Graph & orig, const std::set< size_t > & subgraphIndices, SubgraphKind kind, const RefCountPtr< const ValueVector > & origNodeValues, const RefCountPtr< const ValueVector > & origEdgeValues, RefCountPtr< const ValueVector > * nodeValues, RefCountPtr< const ValueVector > * edgeValues );
			Graph( const Kernel::Graph & orig, Lang::Integer::ValueType offset ); /* Rekey with index. */
			virtual ~Graph( );

			const NodeVector & nodes( ) const { return nodes_; }
			const EdgeVector & uedges( ) const { return uedges_; }
			const EdgeVector & dedges( ) const { return dedges_; }
			const EdgeVector & uloops( ) const { return uloops_; }
			const EdgeVector & dloops( ) const { return dloops_; }

			void show( std::ostream & os ) const;

			const Kernel::GraphDomain & domain( ) const { return domain_; }
			size_t nodeCount( ) const { return nodes_.size( ); }
			size_t edgeCount( ) const;
			bool keyIsRange( ) const { return keyIsRange_; }
			Lang::Integer::ValueType keyOffset( ) const { return keyOffset_; }
			bool partitioned( ) const { return partitionKeys_ != NullPtr< const Lang::SingleList >( ); }
			size_t partitionCount( ) const { return partitions_.size( ); }
			ListRef partitionKeys( ) const { return partitionKeys_; }

			bool isKey( const RefCountPtr< const Lang::Value > & key ) const;
			RefCountPtr< const Lang::Node > indexNode( const RefCountPtr< const Lang::Graph > & owner, size_t index ) const;
			RefCountPtr< const Lang::Edge > indexEdge( const RefCountPtr< const Lang::Graph > & owner, size_t index ) const;
			RefCountPtr< const Lang::Node > findNode( const RefCountPtr< const Lang::Graph > & owner, const RefCountPtr< const Lang::Value > & key ) const;
			size_t findIndex( const RefCountPtr< const Lang::Value > & key ) const;
			ListRef findEdges( const RefCountPtr< const Lang::Graph > & owner, Kernel::Node * source, Kernel::Node * target, bool directed, bool undirected, bool loops, const Kernel::ValueRef & label ) const;
			ListRef findMultiEdges( const RefCountPtr< const Lang::Graph > & owner, const Kernel::Node * source, const Kernel::Node * target, bool directed, bool undirected, bool loops ) const;
			const NodeVector * getPartition( const Kernel::ValueRef & key ) const;

			Kernel::VariableHandle edgeLabel( const Kernel::Edge * edge ) const;
			Kernel::VariableHandle nodePartition( const Kernel::Node * node ) const;

			Kernel::State * newState( const RefCountPtr< const ValueVector > & nodeValues, const RefCountPtr< const ValueVector > & edgeValues ) const;

		private:
			void initializeKeyRangeOffset( );
			void initializeKeyMaps( );
			void initializeAndCheckPartitions( const Ast::SourceLocation & callLoc );
			void addEdgesToNodes( );
			void initSubgraphInduced( const Kernel::Graph & orig, const std::set< size_t > & subgraphNodeIndices, const RefCountPtr< const ValueVector > & origNodeValues, const RefCountPtr< const ValueVector > & origEdgeValues, RefCountPtr< const ValueVector > * nodeValues, RefCountPtr< const ValueVector > * edgeValues );
			void initSubgraphSpanning( const Kernel::Graph & orig, const std::set< size_t > & subgraphEdgeIndices, const RefCountPtr< const ValueVector > & origNodeValues, const RefCountPtr< const ValueVector > & origEdgeValues, RefCountPtr< const ValueVector > * nodeValues, RefCountPtr< const ValueVector > * edgeValues );
		};

	}

	namespace Lang
	{

		class Node : public Lang::NoOperatorOverloadValue
		{
			RefCountPtr< const Lang::Graph > graph_;
			const Kernel::Node * node_;
		public:
			Node( const RefCountPtr< const Lang::Graph > & graph, const Kernel::Node * node );
			virtual ~Node( );

			RefCountPtr< const Lang::Graph > graph( ) const { return graph_; }
			const Kernel::Node * node( ) const { return node_; }

			virtual void show( std::ostream & os ) const;
			virtual Kernel::VariableHandle getField( const char * fieldID, const RefCountPtr< const Lang::Value > & selfRef ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
			TYPEINFODECL;
		};

		class Edge : public Lang::NoOperatorOverloadValue
		{
			RefCountPtr< const Lang::Graph > graph_;
			const Kernel::Edge * edge_;
			bool directed_; /* The Kernel::Edge does not store whether the edge is directed or not. */
		public:
			Edge( const RefCountPtr< const Lang::Graph > & graph, const Kernel::Edge * edge, bool directed );
			virtual ~Edge( );

			RefCountPtr< const Lang::Graph > graph( ) const { return graph_; }
			const Kernel::Edge * edge( ) const { return edge_; }
			bool directed( ) const { return directed_; }

			const Kernel::Node * trace( const Kernel::Node * source, bool nullOnError = false ) const;
			const Kernel::Node * trace( const Kernel::Node * source, const Ast::SourceLocation & callLoc ) const;
			const Kernel::Node * backtrace( const Kernel::Node * target, bool nullOnError = false ) const;
			const Kernel::Node * backtrace( const Kernel::Node * target, const Ast::SourceLocation & callLoc ) const;

			virtual void show( std::ostream & os ) const;
			virtual Kernel::VariableHandle getField( const char * fieldID, const RefCountPtr< const Lang::Value > & selfRef ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
			TYPEINFODECL;
		};

		class MultiEdge : public Lang::NoOperatorOverloadValue
		{
		public:
			typedef Kernel::Node::EdgeSetSource SetType;
		private:
			RefCountPtr< const Lang::Graph > graph_;
			bool directed_;
			const Kernel::Node * source_;
			const Kernel::Node * target_;
			size_t count_;
			/* The parallel edges are stored as a sub-range of a node's edge set.
			 * While a Kernel::Node uses two different types of sets to store its incident
			 * edges, every edge is stored somewhere in a set of type Kernel::Node::EdgeSetSource.
			 * This means that we will pay a time penalty for for constructing all multiedges
			 * incident to a node, as all multiedges which are not sorted by source in the
			 * current node will require a search in an adjacent node.
			 */
			SetType::const_iterator begin_;
			SetType::const_iterator end_;
		public:
			MultiEdge( const RefCountPtr< const Lang::Graph > & graph, bool directed, const Kernel::Node * source, const Kernel::Node * target, size_t count, const SetType::const_iterator & begin, const SetType::const_iterator & end );
			virtual ~MultiEdge( );

			RefCountPtr< const Lang::Graph > graph( ) const { return graph_; }
			bool directed( ) const { return directed_; }
			const Kernel::Node * source( ) const { return source_; }
			const Kernel::Node * target( ) const { return target_; }
			size_t count( ) const { return count_; }
			SetType::const_iterator begin( ) const { return begin_; }
			SetType::const_iterator end( ) const { return end_; }

			const Kernel::Node * trace( const Kernel::Node * source, bool nullOnError = false ) const;
			const Kernel::Node * trace( const Kernel::Node * source, const Ast::SourceLocation & callLoc ) const;
			const Kernel::Node * backtrace( const Kernel::Node * target, bool nullOnError = false ) const;
			const Kernel::Node * backtrace( const Kernel::Node * target, const Ast::SourceLocation & callLoc ) const;

			RefCountPtr< const Lang::SingleList > edges( ) const;

			virtual void show( std::ostream & os ) const;
			virtual Kernel::VariableHandle getField( const char * fieldID, const RefCountPtr< const Lang::Value > & selfRef ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
			TYPEINFODECL;
		};

		class Graph : public Lang::Hot
		{
		public:
			typedef Kernel::Graph::ValueVector ValueVector;
			typedef RefCountPtr< const Lang::SingleList > ListRef;

			RefCountPtr< const Kernel::Graph > graph_;
			RefCountPtr< const ValueVector > nodeValues_; /* Null in case all values are void. */
			RefCountPtr< const ValueVector > edgeValues_; /* Null in case all values are void. */

		public:
			Graph( const RefCountPtr< const Kernel::Graph > & graph );
			Graph( const RefCountPtr< const Kernel::Graph > & graph, const RefCountPtr< const ValueVector > & nodeValues, const RefCountPtr< const ValueVector > & edgeValues );
			Graph( const Lang::Graph & orig, const std::set< size_t > & subgraphIndices, Kernel::Graph::SubgraphKind kind );
			Graph( const Lang::Graph & orig, Lang::Integer::ValueType offset ); /* Rekey with index. */
			virtual ~Graph( );
			void setNodeValues( const RefCountPtr< const ValueVector > nodeValues );
			void setNodeValues( const std::list< Kernel::NodeData > & nodeData );
			void setEdgeValues( const RefCountPtr< const ValueVector > edgeValues ); /* Only invoke if at least one value is different from void. */
			void setEdgeValues( const std::list< Kernel::EdgeDataIndexed > & uedgeData, const std::list< Kernel::EdgeDataIndexed > & dedgeData, const std::list< Kernel::LoopDataIndexed > & uloopData, const std::list< Kernel::LoopDataIndexed > & dloopData ); /* Only invoke if at least one value is different from void. */

			virtual void show( std::ostream & os ) const;
			virtual Kernel::VariableHandle getField( const char * fieldID, const RefCountPtr< const Lang::Value > & selfRef ) const;
			virtual Kernel::State * newState( ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );

			RefCountPtr< const Kernel::Graph > graph( ) const { return graph_; }
			RefCountPtr< const ValueVector > nodeValues( ) const { return nodeValues_; }
			RefCountPtr< const ValueVector > edgeValues( ) const { return edgeValues_; }

			Kernel::VariableHandle nodeValue( const Kernel::Node * node ) const;
			Kernel::VariableHandle nodePartition( const Kernel::Node * node ) const;
			Kernel::VariableHandle edgeValue( const Kernel::Edge * edge ) const;
			Kernel::VariableHandle edgeLabel( const Kernel::Edge * edge ) const;
			/* The access to Kernel::Node pointers should only be used by Lang::Graph methods. */
			Kernel::Node * kernelNodeByValue( Kernel::Arguments & args, size_t argsi, bool voidIsNull, const RefCountPtr< const Interaction::CoreLocation > & coreLoc, const Ast::SourceLocation & callLoc ) const;

			RefCountPtr< const Lang::SingleList > getPartition( const Kernel::ValueRef & key, const RefCountPtr< const Lang::Graph > & selfRef ) const;
			size_t getPartitionSize( const Kernel::ValueRef & key ) const;

			static ListRef prepend_nodes( const Kernel::Graph::NodeVector & nodes, const ListRef & rest, const RefCountPtr< const Lang::Graph > & selfRef );
			static ListRef prepend_edges( const Kernel::Graph::EdgeVector & edges, bool directed, const ListRef & rest, const RefCountPtr< const Lang::Graph > & selfRef, const Kernel::EdgePredicate * filter = 0 );

			TYPEINFODECL;
		};

		class Walk : public Lang::NoOperatorOverloadValue
		{
			RefCountPtr< const Lang::Graph > graph_;
			RefCountPtr< const Lang::Node > first_;
			RefCountPtr< const Lang::Node > last_;
			RefCountPtr< const Lang::SingleList > edges_;
			bool closed_;
			bool simple_;
			bool trail_;
			bool nodeCover_;
			bool edgeCover_;
			size_t edgeCount_;
		public:
			/* The constructor allows the <first> and <last> arguments to be null pointers, in which case the nodes will be determined
			 * based on the other arguments.
			 */
			Walk( const RefCountPtr< const Lang::Graph > & graph, const RefCountPtr< const Lang::Node > & first, const RefCountPtr< const Lang::Node > & last, const RefCountPtr< const Lang::SingleList > & edges, const Ast::SourceLocation & edgesLoc, const Ast::SourceLocation & callLoc );
			virtual Kernel::VariableHandle getField( const char * fieldID, const RefCountPtr< const Lang::Value > & selfRef ) const;
			virtual ~Walk( );
			TYPEINFODECL;
			virtual void show( std::ostream & os ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
		};

	}

	namespace Helpers
	{

		/* Helper for Graph construction.
		 * Takes care of input data checking, so that all errors detected in Graph::Graph can be considered internal errors.
		 */
		RefCountPtr< const Lang::Graph > graphFromNodeEdgeData( const Kernel::GraphDomain & domain, const RefCountPtr< const Lang::SingleList > & partitions, const std::list< Kernel::NodeData > & nodeData, const std::list< Kernel::EdgeData > & edgeData, const Ast::SourceLocation & partitionsLoc, const Ast::SourceLocation & callLoc );

	}

	namespace Kernel
	{

		class WarmGraph : public Kernel::State
		{
			Kernel::GraphDomain domain_;
			RefCountPtr< const Lang::SingleList > partitionKeys_; /* Partition keys in original order.  Null if graph is not partitioned. */
			RefCountPtr< std::list< Kernel::NodeData > > nodeDataMem_;
			RefCountPtr< std::list< Kernel::EdgeData > > edgeDataMem_;
			std::list< Kernel::NodeData > & nodeData_;
			std::list< Kernel::EdgeData > & edgeData_;
		public:
			WarmGraph( const Kernel::GraphDomain & domain, const RefCountPtr< const Lang::SingleList > & partitionKeys, RefCountPtr< std::list< Kernel::NodeData > > & nodeDataMem, RefCountPtr< std::list< Kernel::EdgeData > > & edgeDataMem );
			virtual ~WarmGraph( );
			virtual void tackOnImpl( Kernel::EvalState * evalState, const RefCountPtr< const Lang::Value > & piece, const Ast::SourceLocation & callLoc );
			virtual void peekImpl( Kernel::EvalState * evalState, const Ast::SourceLocation & callLoc );
			virtual void freezeImpl( Kernel::EvalState * evalState, const Ast::SourceLocation & callLoc );
			virtual void gcMark( Kernel::GCMarkedSet & marked );
			const Kernel::GraphDomain & domain( ) const { return domain_; }
			RefCountPtr< const Lang::SingleList > partitionKeys( ) const { return partitionKeys_; }
			void addNode( const Kernel::NodeData & node );
			void addEdge( const Kernel::EdgeData & edge );
			TYPEINFODECL;
		};

	}
}
