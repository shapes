/* This file is part of Shapes.
 *
 * Shapes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Shapes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2008, 2014 Henrik Tidefelt
 */

#include <cmath>

#include "shapescore.h"
#include "globals.h"
#include "shapesexceptions.h"
#include "consts.h"
#include "simplepdfi.h"
#include "astfun.h"
#include "continuations.h"
#include "multipage.h"
#include "debuglog.h"

#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include <stdio.h>
#include <sys/time.h>

using namespace Shapes;


namespace Shapes
{
	namespace Lang
	{

		class NullFunction : public Lang::CoreFunction
		{
		public:
			NullFunction( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name ) : CoreFunction( ns, name ) { }
			virtual void call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				Kernel::ContRef cont = evalState->cont_;
				cont->takeValue( Lang::THE_VOID,
												 evalState );
			}
		};

		class Core_identity : public Lang::CoreFunction
		{
		public:
			Core_identity( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name ) : CoreFunction( ns, name ) { }
			virtual void call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				const size_t ARITY = 1;
				CHECK_ARITY( args, ARITY, id_ );

				Kernel::ContRef cont = evalState->cont_;
				cont->takeHandle( args.getHandle( 0 ),
													evalState );
			}
		};

		class Core_typeof : public Lang::CoreFunction
		{
		public:
			Core_typeof( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name ) : CoreFunction( ns, name ) { }
			virtual void call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				const size_t ARITY = 1;
				CHECK_ARITY( args, ARITY, id_ );

				Kernel::ContRef cont = evalState->cont_;
				cont->takeValue( args.getValue( 0 )->getClass( ),
												 evalState );
			}
		};

		class Core_error : public Lang::CoreFunction
		{
		public:
			Core_error( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name )
				: CoreFunction( ns, name, new Kernel::EvaluatedFormals( Ast::FileID::build_internal( name ), true ) )
			{
				formals_->appendEvaluatedCoreFormal( "kind", Kernel::THE_SLOT_VARIABLE );
				formals_->appendEvaluatedCoreFormal( "source", Kernel::THE_SLOT_VARIABLE );
				formals_->appendEvaluatedCoreFormal( "message", Kernel::THE_SLOT_VARIABLE );
				formals_->appendEvaluatedCoreFormal( "details", Kernel::THE_VOID_VARIABLE );
				formals_->appendEvaluatedCoreFormal( "cause", Kernel::THE_VOID_VARIABLE );
			}
			virtual void call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				args.applyDefaults( callLoc );

				static Lang::Symbol OUT_OF_RANGE( "out_of_range" );
				static Lang::Symbol TYPE_MISMATCH( "type_mismatch" );
				static Lang::Symbol MISC( "misc" );
				static Lang::Symbol EXTERNAL( "external" );
				static Lang::Symbol PDF_VERSION( "PDF_version" );
				static Lang::Symbol DTMIN( "dtmin" );
				static Lang::Symbol NUMERIC( "numeric" );

				size_t argsi = 0;
				typedef const Lang::Symbol KindType;
				RefCountPtr< KindType > kind = Helpers::down_cast_CoreArgument< KindType >( id_, args, argsi, callLoc );

				++argsi;
				typedef const Lang::String SourceType;
				RefCountPtr< SourceType > source = Helpers::down_cast_CoreArgument< SourceType >( id_, args, argsi, callLoc );

				++argsi;
				typedef const Lang::String MessageType;
				RefCountPtr< MessageType > message = Helpers::down_cast_CoreArgument< MessageType >( id_, args, argsi, callLoc );

				++argsi;
				size_t details_argsi = argsi;

				++argsi;
				size_t cause_argsi = argsi;
				{
					Kernel::VariableHandle cause = args.getHandle( argsi );
					if( *kind != TYPE_MISMATCH &&
							cause != Kernel::THE_VOID_VARIABLE )
						{
							throw Exceptions::CoreRequirement( "The argument <cause> must not be provided for the given value of <kind>.", id_, callLoc );
						}
				}

				if( *kind == OUT_OF_RANGE )
					{
						typedef const Lang::Integer DetailsType;
						RefCountPtr< DetailsType > details = Helpers::down_cast_CoreArgument< DetailsType >( id_, args, details_argsi, callLoc );
						throw Exceptions::UserOutOfRange( kind, source, details, message->val_ );
					}
				else if( *kind == TYPE_MISMATCH )
					{
						typedef const Lang::Integer DetailsType;
						RefCountPtr< DetailsType > details = Helpers::down_cast_CoreArgument< DetailsType >( id_, args, details_argsi, callLoc );

						throw Exceptions::UserTypeMismatch( kind, source, details, message->val_, args.getValue( cause_argsi )->getTypeName( ) );
					}
				else if( *kind == MISC ||
								 *kind == EXTERNAL ||
								 *kind == PDF_VERSION ||
								 *kind == NUMERIC )
					{
						Kernel::VariableHandle details = args.getHandle( details_argsi );
						if( details != Kernel::THE_VOID_VARIABLE )
							{
								throw Exceptions::CoreRequirement( "The argument <details> must not be provided for the given value of <kind>.", id_, callLoc );
							}

						throw Exceptions::UserError( kind, source, args.getValue( details_argsi ), message->val_ );
					}
				else if( *kind == DTMIN )
					{
						typedef const Lang::Float DetailsType;
						RefCountPtr< DetailsType > details = Helpers::down_cast_CoreArgument< DetailsType >( id_, args, details_argsi, callLoc );

						throw Exceptions::UserError( kind, source, details, message->val_ );
					}
				else
					{
						throw Exceptions::UserError( kind, source, args.getValue( details_argsi ), message->val_ );
					}

			}
		};

		class Core_show : public Lang::CoreFunction
		{
		public:
			Core_show( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name ) : CoreFunction( ns, name ) { }
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				std::ostringstream oss;
				for( size_t i = 0; i != args.size( ); ++i )
					{
						args.getValue( i )->show( oss );
					}
				Kernel::ContRef cont = evalState->cont_;
				cont->takeValue( RefCountPtr< const Lang::Value >( new Lang::String( strrefdup( oss ) ) ),
												 evalState );
			}
		};

		class Core_typename : public Lang::CoreFunction
		{
		public:
			Core_typename( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name ) : CoreFunction( ns, name ) { }
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				const size_t ARITY = 1;
				CHECK_ARITY( args, ARITY, id_ );

				Kernel::ContRef cont = evalState->cont_;
				cont->takeValue( RefCountPtr< const Lang::Value >( new Lang::String( args.getValue( 0 )->getTypeName( ) ) ),
												 evalState );
			}
		};

		class Core_debuglog_before : public Lang::CoreFunction
		{
		public:
			Core_debuglog_before( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name )
				: CoreFunction( ns, name, new Kernel::EvaluatedFormals( Ast::FileID::build_internal( name ) ) )
			{
				formals_->appendEvaluatedCoreFormal( "msg", Kernel::THE_SLOT_VARIABLE, true );
				formals_->appendEvaluatedCoreFormal( "result", Kernel::THE_SLOT_VARIABLE, false );
			}
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				args.applyDefaults( callLoc );

				args.getValue( 0 )->show( Kernel::theDebugLog.os( ) );
				Kernel::theDebugLog.os( ) << std::flush ;

				Kernel::ContRef cont = evalState->cont_;
				cont->takeHandle( args.getHandle( 1 ),
													evalState );
			}
		};

		class Core_debuglog_after : public Lang::CoreFunction
		{
		public:
			Core_debuglog_after( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name )
				: CoreFunction( ns, name, new Kernel::EvaluatedFormals( Ast::FileID::build_internal( name ) ) )
			{
				formals_->appendEvaluatedCoreFormal( "msg", Kernel::THE_SLOT_VARIABLE, true );
				formals_->appendEvaluatedCoreFormal( "result", Kernel::THE_SLOT_VARIABLE, true );
			}
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				args.applyDefaults( callLoc );

				args.getValue( 0 )->show( Kernel::theDebugLog.os( ) );
				Kernel::theDebugLog.os( ) << std::flush ;

				Kernel::ContRef cont = evalState->cont_;
				cont->takeValue( args.getValue( 1 ),
													evalState );
			}
		};

		class Core_if : public Lang::CoreFunction
		{
		public:
			Core_if( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name )
				: CoreFunction( ns, name, new Kernel::EvaluatedFormals( Ast::FileID::build_internal( name ) ) )
			{
				formals_->appendEvaluatedCoreFormal( "predicate", Kernel::THE_SLOT_VARIABLE, true );
				formals_->appendEvaluatedCoreFormal( "consequence", Kernel::THE_SLOT_VARIABLE, false );
				formals_->appendEvaluatedCoreFormal( "alternative", Kernel::VariableHandle( new Kernel::Variable( Lang::THE_VOID ) ), false );
			}

			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				args.applyDefaults( callLoc );

				evalState->cont_ = Kernel::ContRef( new Kernel::IfContinuation( args.getHandle( 1 ), args.getHandle( 2 ), evalState->cont_, callLoc ) );

				Kernel::ContRef cont = evalState->cont_;
				cont->takeHandle( args.getHandle( 0 ), evalState );
			}
		};

		class Core_memoryinfo : public Lang::CoreFunction
		{
		public:
			Core_memoryinfo( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name ) : CoreFunction( ns, name ) { }
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				const size_t ARITY = 0;
				CHECK_ARITY( args, ARITY, id_ );
				std::cerr << "Environments: alive: " << Kernel::Environment::liveCount << " of total: " << Kernel::Environment::createdCount
									<< " (" << 100 * static_cast< double >( Kernel::Environment::liveCount ) / static_cast< double >( Kernel::Environment::createdCount ) << "%)" << std::endl ;
				Kernel::ContRef cont = evalState->cont_;
				cont->takeHandle( Kernel::THE_VOID_VARIABLE,
													evalState );
			}
		};

		class Core_rectangle : public Lang::CoreFunction
		{
		public:
			Core_rectangle( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name ) : CoreFunction( ns, name ) { }
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				const size_t ARITY = 2;
				CHECK_ARITY( args, ARITY, id_ );

				typedef const Lang::Coords2D ArgType;

				RefCountPtr< ArgType > arg1 = Helpers::down_cast_CoreArgument< ArgType >( id_, args, 0, callLoc );
				RefCountPtr< ArgType > arg2 = Helpers::down_cast_CoreArgument< ArgType >( id_, args, 1, callLoc );

				Lang::ElementaryPath2D * res = new Lang::ElementaryPath2D;

				res->push_back( new Concrete::PathPoint2D( arg1->x_.get( ), arg1->y_.get( ) ) );
				res->push_back( new Concrete::PathPoint2D( arg2->x_.get( ), arg1->y_.get( ) ) );
				res->push_back( new Concrete::PathPoint2D( arg2->x_.get( ), arg2->y_.get( ) ) );
				res->push_back( new Concrete::PathPoint2D( arg1->x_.get( ), arg2->y_.get( ) ) );
				res->close( );

				Kernel::ContRef cont = evalState->cont_;
				cont->takeValue( Kernel::ValueRef( res ),
												 evalState );
			}
		};

		class Core_hot : public Lang::CoreFunction
		{
		public:
			Core_hot( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name )
				: CoreFunction( ns, name, new Kernel::EvaluatedFormals( Ast::FileID::build_internal( name ), true ) )
			{
				formals_->appendEvaluatedCoreFormal( "init", Kernel::THE_SLOT_VARIABLE );
				formals_->appendEvaluatedCoreFormal( "tackon", Kernel::THE_SLOT_VARIABLE );
				formals_->appendEvaluatedCoreFormal( "freeze", Kernel::VariableHandle( new Kernel::Variable( Lang::THE_IDENTITY ) ) );
			}

			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				args.applyDefaults( callLoc );

				Kernel::ContRef cont = evalState->cont_;
				cont->takeValue( Kernel::ValueRef( new Lang::HotTriple
																					 ( args.getValue( 0 ),
																						 Helpers::down_cast_CoreArgument< const Lang::Function >( id_, args, 1, callLoc ),
																						 Helpers::down_cast_CoreArgument< const Lang::Function >( id_, args, 2, callLoc ) ) ),
												 evalState );
			}
		};

		class Core_ampersand_dynamic : public Lang::CoreFunction
		{
		public:
			Core_ampersand_dynamic( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name )
				: CoreFunction( ns, name, new Kernel::EvaluatedFormals( Ast::FileID::build_internal( name ), true ) )
			{
			}

			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				//				args.applyDefaults( callLoc );
				/*
				 * Here, we should check that there are no named arguments, and that there are no states being passed...
				 */

				RefCountPtr< const Lang::DynamicBindings > res = RefCountPtr< const Lang::DynamicBindings >( new Lang::DynamicBindingsNull( ) );

				for( size_t i = 0; i != args.size( ); ++i )
					{
						res = RefCountPtr< const Lang::DynamicBindings >
							( new Lang::DynamicBindingsPair( Helpers::down_cast_CoreArgument< const Lang::DynamicBindings >( id_, args, args.size( ) - 1 - i, callLoc, true ),
																							 res ) );
					}

				Kernel::ContRef cont = evalState->cont_;
				cont->takeValue( res,
												 evalState );
			}
		};

		class Core_locate : public Lang::CoreFunction
		{
		public:
			Core_locate( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name ) : CoreFunction( ns, name ) { }
			virtual void call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				const size_t ARITY = 1;
				CHECK_ARITY( args, ARITY, id_ );

				RefCountPtr< const Lang::Value > res = args.getValue( 0 );
				res->set_node( args.getNode( 0 ) );

				Kernel::ContRef cont = evalState->cont_;
				cont->takeValue( res,
												 evalState );
			}
		};

		class Core_sourceof : public Lang::CoreFunction
		{
		public:
			Core_sourceof( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name ) : CoreFunction( ns, name ) { }
			virtual void call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				const size_t ARITY = 1;
				CHECK_ARITY( args, ARITY, id_ );

				const Ast::Node * node = args.getValue( 0 )->node( );
				if( node == 0 )
					{
						throw Exceptions::CoreOutOfRange( id_, args, 0, "The value has not been located." );
					}

				std::ostringstream oss;
				try
					{
						node->loc( ).copy( & oss );
					}
				catch( const std::string & ball )
					{
						std::ostringstream msg;
						msg << "Source of located value could not be copied.  Reason: " << ball ;
						throw Exceptions::CoreOutOfRange( id_, args, 0, strrefdup( msg ) );
					}

				Kernel::ContRef cont = evalState->cont_;
				cont->takeValue( RefCountPtr< const Lang::Value >( new Lang::String( strrefdup( oss.str( ) ) ) ),
												 evalState );
			}
		};

	}
}


RefCountPtr< const Lang::CoreFunction > Lang::THE_IDENTITY( new Lang::Core_identity( Lang::THE_NAMESPACE_Shapes, "identity" ) );

void
Kernel::registerCore_misc( Kernel::Environment * env )
{
	env->initDefineCoreFunction( new Lang::Core_typeof( Lang::THE_NAMESPACE_Shapes, "typeof" ) );
	env->initDefineCoreFunction( new Lang::Core_error( Lang::THE_NAMESPACE_Shapes, "error" ) );
	env->initDefineCoreFunction( new Lang::Core_show( Lang::THE_NAMESPACE_Shapes_String, "show" ) );
	env->initDefineCoreFunction( new Lang::Core_typename( Lang::THE_NAMESPACE_Shapes_Debug, "typename" ) );
	env->initDefineCoreFunction( new Lang::Core_debuglog_before( Lang::THE_NAMESPACE_Shapes_Debug, "log_before" ) );
	env->initDefineCoreFunction( new Lang::Core_debuglog_after( Lang::THE_NAMESPACE_Shapes_Debug, "log_after" ) );
	env->initDefineCoreFunction( new Lang::Core_if( Lang::THE_NAMESPACE_Shapes, "if" ) );
	env->initDefineCoreFunction( new Lang::NullFunction( Lang::THE_NAMESPACE_Shapes, "ignore" ) );
	env->initDefineCoreFunction( new Lang::Core_rectangle( Lang::THE_NAMESPACE_Shapes_Geometry, "rectangle" ) );
	env->initDefineCoreFunction( new Lang::Core_memoryinfo( Lang::THE_NAMESPACE_Shapes_Debug, "memoryinfo" ) );
	env->initDefineCoreFunction( new Lang::Core_hot( Lang::THE_NAMESPACE_Shapes, "hot" ) );
	env->initDefineCoreFunction( new Lang::Core_ampersand_dynamic( Lang::THE_NAMESPACE_Shapes, "bindings" ) );

	env->initDefineCoreFunction( new Lang::Core_locate( Lang::THE_NAMESPACE_Shapes_Debug, "locate" ) );
	env->initDefineCoreFunction( new Lang::Core_sourceof( Lang::THE_NAMESPACE_Shapes_Debug, "sourceof" ) );
}

