/* This file is part of Shapes.
 *
 * Shapes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Shapes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2008 Henrik Tidefelt
 */

#pragma once

#include "Shapes_Ast_decls.h"
#include "Shapes_Kernel_decls.h"
#include "Shapes_Lang_decls.h"
#include "Shapes_Lang_decls.h"

#include "ptrowner.h"
#include "refcount.h"
#include "environment.h"
#include "pdfstructure.h"
#include "elementarylength.h"
#include "concretecolors.h"

#include <list>
#include <iostream>
#include <stack>
#include <set>


namespace Shapes
{
	namespace Lang
	{

		class DynamicBindings : public Lang::Value
		{
		public:
			typedef std::map< Kernel::DynamicEnvironmentKeyType, std::pair< Kernel::VariableHandle, const Ast::DynamicBindingExpression * > > MapType;
			DynamicBindings( );
			virtual ~DynamicBindings( );
			TYPEINFODECL;
			virtual void bind( MapType & bindings, Kernel::SystemDynamicVariables ** sysBindings ) const = 0;
			DISPATCHDECL;
		};

		class DynamicBindingsNull : public DynamicBindings
		{
		public:
			DynamicBindingsNull( );
			virtual ~DynamicBindingsNull( );
			virtual void bind( MapType & bindings, Kernel::SystemDynamicVariables ** sysBindings ) const;
			virtual void show( std::ostream & os ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
		};

		class DynamicBindingsPair : public DynamicBindings
		{
			bool override_;
			RefCountPtr< const Lang::DynamicBindings > car_;
			RefCountPtr< const Lang::DynamicBindings > cdr_;
		public:
			DynamicBindingsPair( const RefCountPtr< const Lang::DynamicBindings > & car, const RefCountPtr< const Lang::DynamicBindings > & cdr, bool override = false );
			virtual ~DynamicBindingsPair( );
			virtual void bind( MapType & bindings, Kernel::SystemDynamicVariables ** sysBindings ) const;
			virtual void show( std::ostream & os ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
		};

		class UserDynamicBinding : public DynamicBindings
		{
			Kernel::DynamicEnvironmentKeyType key_;
			const Ast::PlacedIdentifier * id_;
			const Ast::DynamicBindingExpression * bindingExpr_;
			Kernel::VariableHandle var_;
		public:
			UserDynamicBinding( const Kernel::DynamicEnvironmentKeyType & key, const Ast::PlacedIdentifier * id, const Ast::DynamicBindingExpression * bindingExpr, const Kernel::VariableHandle & var );
			virtual ~UserDynamicBinding( );
			virtual void bind( MapType & bindings, Kernel::SystemDynamicVariables ** sysBindings ) const;
			virtual void show( std::ostream & os ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
		};

		class WidthBinding : public DynamicBindings
		{
			const Ast::DynamicBindingExpression * bindingExpr_;
			Concrete::Length val_;
			const Ast::PlacedIdentifier * id_;
		public:
			WidthBinding( const Ast::PlacedIdentifier * id, const Ast::DynamicBindingExpression * bindingExpr, Concrete::Length val );
			virtual ~WidthBinding( );
			virtual void bind( MapType & bindings, Kernel::SystemDynamicVariables ** sysBindings ) const;
			virtual void show( std::ostream & os ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
		};

		class MiterLimitBinding : public DynamicBindings
		{
			const Ast::DynamicBindingExpression * bindingExpr_;
			double val_;
			const Ast::PlacedIdentifier * id_;
		public:
			MiterLimitBinding( const Ast::PlacedIdentifier * id, const Ast::DynamicBindingExpression * bindingExpr, double val );
			virtual ~MiterLimitBinding( );
			virtual void bind( MapType & bindings, Kernel::SystemDynamicVariables ** sysBindings ) const;
			virtual void show( std::ostream & os ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
		};

		class Color : public Lang::Value
		{
		public:
			Color( );
			virtual ~Color( );
			static RefCountPtr< const Lang::Class > TypeID;
			static RefCountPtr< const char > staticTypeName( );
			virtual void setStroking( std::ostream & os ) const = 0;
			virtual void setNonStroking( std::ostream & os ) const = 0;
			virtual RefCountPtr< SimplePDF::PDF_Vector > componentVector( ) const = 0;
			virtual void gcMark( Kernel::GCMarkedSet & marked ){ };
			DISPATCHDECL;
		};

		class Gray : public Color
		{
			Concrete::Gray components_;
		public:
			Gray( const Concrete::Gray & components );
			virtual ~Gray( );
			TYPEINFODECL;
			virtual void show( std::ostream & os ) const;
			virtual void setStroking( std::ostream & os ) const;
			virtual void setNonStroking( std::ostream & os ) const;
			virtual RefCountPtr< SimplePDF::PDF_Vector > componentVector( ) const;
			const Concrete::Gray & components( ) const { return components_; };
			DISPATCHDECL;
		};

		class RGB : public Color
		{
			Concrete::RGB components_;
		public:
			RGB( const Concrete::RGB & components );
			virtual ~RGB( );
			TYPEINFODECL;
			virtual void show( std::ostream & os ) const;
			virtual void setStroking( std::ostream & os ) const;
			virtual void setNonStroking( std::ostream & os ) const;
			virtual RefCountPtr< SimplePDF::PDF_Vector > componentVector( ) const;
			const Concrete::RGB & components( ) const { return components_; };
			DISPATCHDECL;
		};

		class CMYK : public Color
		{
			Concrete::CMYK components_;
		public:
			CMYK( const Concrete::CMYK & components );
			virtual ~CMYK( );
			TYPEINFODECL;
			virtual void show( std::ostream & os ) const;
			virtual void setStroking( std::ostream & os ) const;
			virtual void setNonStroking( std::ostream & os ) const;
			virtual RefCountPtr< SimplePDF::PDF_Vector > componentVector( ) const;
			const Concrete::CMYK & components( ) const { return components_; };
			DISPATCHDECL;
		};

		class StrokingBinding : public DynamicBindings
		{
			const Ast::DynamicBindingExpression * bindingExpr_;
			RefCountPtr< const Lang::Color > color_;
			const Ast::PlacedIdentifier * id_;
		public:
			StrokingBinding( const Ast::PlacedIdentifier * id, const Ast::DynamicBindingExpression * bindingExpr, RefCountPtr< const Lang::Color > color );
			virtual ~StrokingBinding( );
			virtual void bind( MapType & bindings, Kernel::SystemDynamicVariables ** sysBindings ) const;
			virtual void show( std::ostream & os ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
		};

		class NonStrokingBinding : public DynamicBindings
		{
			const Ast::DynamicBindingExpression * bindingExpr_;
			RefCountPtr< const Lang::Color > color_;
			const Ast::PlacedIdentifier * id_;
		public:
			NonStrokingBinding( const Ast::PlacedIdentifier * id, const Ast::DynamicBindingExpression * bindingExpr, RefCountPtr< const Lang::Color > color );
			virtual ~NonStrokingBinding( );
			virtual void bind( MapType & bindings, Kernel::SystemDynamicVariables ** sysBindings ) const;
			virtual void show( std::ostream & os ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
		};

		class Alpha : public Lang::NoOperatorOverloadValue
		{
		public:
			bool isShape_;
			double a_;
			Alpha( bool isShape, double a );
			virtual ~Alpha( );
			static void applyGraphicsState( std::ostream & os, SimplePDF::PDF_Resources * resources, const Lang::Alpha & self, bool isStroking );
			TYPEINFODECL;
			virtual void gcMark( Kernel::GCMarkedSet & marked ){ };

		private:
			static std::map< double, RefCountPtr< SimplePDF::PDF_Object > > strokingShapeResourcemap;
			static std::map< double, RefCountPtr< SimplePDF::PDF_Object > > strokingOpacityResourcemap;
			static std::map< double, RefCountPtr< SimplePDF::PDF_Object > > nonStrokingShapeResourcemap;
			static std::map< double, RefCountPtr< SimplePDF::PDF_Object > > nonStrokingOpacityResourcemap;
		};

		class AlphaBinding : public DynamicBindings
		{
			const Ast::DynamicBindingExpression * bindingExpr_;
			RefCountPtr< const Lang::Alpha > alpha_;
			bool isStroking_;
			const Ast::PlacedIdentifier * id_;
		public:
			AlphaBinding( const Ast::PlacedIdentifier * id, const Ast::DynamicBindingExpression * bindingExpr, const RefCountPtr< const Lang::Alpha > & alpha, bool isStroking );
			virtual ~AlphaBinding( );
			virtual void bind( MapType & bindings, Kernel::SystemDynamicVariables ** sysBindings ) const;
			virtual void show( std::ostream & os ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
		};

		class SoftMask : public Lang::NoOperatorOverloadValue
		{
		public:
			typedef enum { ALPHA, LUMINOSITY } SubType;
			RefCountPtr< SimplePDF::PDF_Object > graphicsStateResource_;

			SoftMask( );	// this yields the None soft mask
			SoftMask( SubType subType, const RefCountPtr< const Lang::TransparencyGroup > & tpGroup, const RefCountPtr< const Lang::Color > & background, const RefCountPtr< const Lang::PDF_Function > & transfer );
			virtual ~SoftMask( );
			TYPEINFODECL;
			virtual void gcMark( Kernel::GCMarkedSet & marked ){ };
		};

		class Dash : public Lang::Value
		{
		public:
			class Iterator
			{
				RefCountPtr< std::list< Concrete::Length > > mem_;
				std::list< Concrete::Length >::const_iterator begin_;
				std::list< Concrete::Length >::const_iterator end_;
				double scale_;
				std::list< Concrete::Length >::const_iterator i_;
				bool on_;
				Concrete::Length length_;
			public:
				Iterator( RefCountPtr< std::list< Concrete::Length > > pattern, double scale, std::list< Concrete::Length >::const_iterator i, bool on, Concrete::Length length );
				Iterator & operator ++ ( );
				bool isOn( ) const;
				Concrete::Length getLength( ) const;
			};
		private:
			RefCountPtr< std::list< Concrete::Length > > pattern_;
			Concrete::Length phase_;
			double scale_;
			Concrete::Length myLength_;
		public:
			Dash( );
			Dash( RefCountPtr< std::list< Concrete::Length > > pattern, Concrete::Length phase, double scale );
			Dash( RefCountPtr< std::list< Concrete::Length > > pattern, Concrete::Length phase, double scale, Concrete::Length length ); // length must be the sum of pattern
			~Dash( );
			TYPEINFODECL;
			void setDash( std::ostream & os ) const;
			RefCountPtr< SimplePDF::PDF_Vector > getDashArray( ) const;
			RefCountPtr< const Lang::Dash > scaled( double factor ) const;
			RefCountPtr< const Lang::Dash > shifted( Concrete::Length dist ) const;
			Concrete::Length length( ) const;
			bool isSolid( ) const;
			Iterator begin( ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked ){ };
			DISPATCHDECL;
		};

		class DashBinding : public DynamicBindings
		{
			const Ast::DynamicBindingExpression * bindingExpr_;
			RefCountPtr< const Lang::Dash > dash_;
			const Ast::PlacedIdentifier * id_;
		public:
			DashBinding( const Ast::PlacedIdentifier * id, const Ast::DynamicBindingExpression * bindingExpr, RefCountPtr< const Lang::Dash > dash );
			virtual ~DashBinding( );
			virtual void bind( MapType & bindings, Kernel::SystemDynamicVariables ** sysBindings ) const;
			virtual void show( std::ostream & os ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
		};

		class CapStyle : public Lang::NoOperatorOverloadValue
		{
		public:
			typedef enum { CAP_BUTT = 0, CAP_ROUND, CAP_SQUARE, CAP_SAME, CAP_UNDEFINED } ValueType;
			ValueType cap_;
			CapStyle( const ValueType & cap );
			TYPEINFODECL;
			virtual void gcMark( Kernel::GCMarkedSet & marked ){ };
		};

		class CapStyleBinding : public DynamicBindings
		{
			const Ast::DynamicBindingExpression * bindingExpr_;
			Lang::CapStyle::ValueType cap_;
			const Ast::PlacedIdentifier * id_;
		public:
			CapStyleBinding( const Ast::PlacedIdentifier * id, const Ast::DynamicBindingExpression * bindingExpr, const Lang::CapStyle::ValueType & cap );
			virtual ~CapStyleBinding( );
			virtual void bind( MapType & bindings, Kernel::SystemDynamicVariables ** sysBindings ) const;
			virtual void show( std::ostream & os ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
		};

		class JoinStyle : public Lang::NoOperatorOverloadValue
		{
		public:
			typedef enum { JOIN_MITER = 0, JOIN_ROUND, JOIN_BEVEL, JOIN_SAME, JOIN_UNDEFINED } ValueType;
			ValueType join_;
			JoinStyle( const ValueType & join );
			TYPEINFODECL;
			virtual void gcMark( Kernel::GCMarkedSet & marked ){ };
		};

		class JoinStyleBinding : public DynamicBindings
		{
			const Ast::DynamicBindingExpression * bindingExpr_;
			Lang::JoinStyle::ValueType join_;
			const Ast::PlacedIdentifier * id_;
		public:
			JoinStyleBinding( const Ast::PlacedIdentifier * id, const Ast::DynamicBindingExpression * bindingExpr, const Lang::JoinStyle::ValueType & join );
			virtual ~JoinStyleBinding( );
			virtual void bind( MapType & bindings, Kernel::SystemDynamicVariables ** sysBindings ) const;
			virtual void show( std::ostream & os ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
		};

		class BlendMode : public Lang::NoOperatorOverloadValue
		{
		public:
			typedef enum {
				NORMAL = 0, MULTIPLY, SCREEN, OVERLAY, DARKEN, LIGHTEN, COLOR_DODGE, COLOR_BURN, HARD_LIGHT, SOFT_LIGHT, DIFFERENCE, EXCLUSION,
				HUE, SATURATION, COLOR, LUMINOSITY, BLEND_SAME, BLEND_UNDEFINED } ValueType;
			ValueType mode_;
			BlendMode( const ValueType & mode );
			TYPEINFODECL;
			virtual void gcMark( Kernel::GCMarkedSet & marked ){ };
			static void applyGraphicsState( std::ostream & os, SimplePDF::PDF_Resources * resources, const ValueType & mode );
		private:
			static std::map< ValueType, RefCountPtr< SimplePDF::PDF_Object > > resourceMap;
		};

		class BlendModeBinding : public DynamicBindings
		{
			const Ast::DynamicBindingExpression * bindingExpr_;
			Lang::BlendMode::ValueType blend_;
			const Ast::PlacedIdentifier * id_;
		public:
			BlendModeBinding( const Ast::PlacedIdentifier * id, const Ast::DynamicBindingExpression * bindingExpr, const Lang::BlendMode::ValueType & blend );
			virtual ~BlendModeBinding( );
			virtual void bind( MapType & bindings, Kernel::SystemDynamicVariables ** sysBindings ) const;
			virtual void show( std::ostream & os ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
		};

		// Currently, color space is not part of the graphics state; it's only used for blending in transparency groups.
		// However, since it can be part of the graphcs state, the definition is placed here anyway.
		// The related dynamic binding classes are found in dynamicenvironment.h
		class ColorSpace : public Lang::NoOperatorOverloadValue
		{
		public:
			ColorSpace( );
			virtual ~ColorSpace( );
			TYPEINFODECL;
			virtual RefCountPtr< SimplePDF::PDF_Name > name( ) const = 0;
			virtual bool isInherent( ) const { return false; }
			virtual bool isBlendable( ) const { return false; }
			virtual bool containsColor( const Lang::Color * col ) const = 0;
			virtual size_t numberOfComponents( ) const = 0;
			virtual void show( std::ostream & os ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked ){ };
		};

		class InheritedColorSpace : public Lang::ColorSpace
		{
		public:
			InheritedColorSpace( );
			~InheritedColorSpace( );
			virtual bool isInherent( ) const { return true; }
			virtual bool isBlendable( ) const { return true; }
			virtual bool containsColor( const Lang::Color * col ) const;
			virtual RefCountPtr< SimplePDF::PDF_Name > name( ) const;
			virtual size_t numberOfComponents( ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked ){ };
		};

		template< class C >
		class DeviceColorSpace : public Lang::ColorSpace
		{
			RefCountPtr< const SimplePDF::PDF_Name > space_;
			size_t numberOfComponents_;
		public:
			DeviceColorSpace( const char * spaceName, size_t numOfComponents );
			~DeviceColorSpace( );
			virtual bool isBlendable( ) const { return true; }
			virtual bool containsColor( const Lang::Color * col ) const;
			virtual RefCountPtr< SimplePDF::PDF_Name > name( ) const;
			virtual size_t numberOfComponents( ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked ){ };
		};

		template< class C >
		DeviceColorSpace< C >::DeviceColorSpace( const char * spaceName, size_t numOfComponents )
			: space_( RefCountPtr< SimplePDF::PDF_Name >( new SimplePDF::PDF_Name( spaceName ) ) ),
				numberOfComponents_( numOfComponents )
		{ }

		template< class C >
			DeviceColorSpace< C >::~DeviceColorSpace< C >( )
			{ }

		template< class C >
			RefCountPtr< SimplePDF::PDF_Name >
			DeviceColorSpace< C >::name( ) const
			{
				return space_.unconst_cast< SimplePDF::PDF_Name >( );
			}

		template< class C >
		size_t
		DeviceColorSpace< C >::numberOfComponents( ) const
		{
			return numberOfComponents_;
		}

		template< class C >
		bool
		DeviceColorSpace< C >::containsColor( const Lang::Color * col ) const
		{
			return dynamic_cast< const C * >( col ) != 0;
		}

	}

	namespace Kernel
	{

		class UserDynamicVariableProperties : public Kernel::DynamicVariableProperties
		{
			Kernel::DynamicEnvironmentKeyType key_;
			RefCountPtr< const Lang::Function > filter_;
			Kernel::VariableHandle defaultVal_;
		public:
			UserDynamicVariableProperties( const Ast::PlacedIdentifier * id, const Kernel::DynamicEnvironmentKeyType & key, const RefCountPtr< const Lang::Function > & filter, const Kernel::VariableHandle & defaultVal );
			virtual ~UserDynamicVariableProperties( );

			virtual Kernel::VariableHandle fetch( const Kernel::PassedDyn & dyn ) const;
			virtual void makeBinding( Kernel::VariableHandle val, const Ast::DynamicBindingExpression * bindingExpr, Kernel::EvalState * evalState ) const;
		};

		class UserDynamicStateProperties : public Kernel::DynamicStateProperties
		{
//		Kernel::DynamicEnvironmentKeyType key_; /* Commented out to quiet compiler warning about unused member. */
			Kernel::PassedEnv defaultStateEnv_;
			Kernel::PassedDyn defaultStateDyn_;
			Ast::StateReference * defaultState_;
		public:
			UserDynamicStateProperties( const Ast::PlacedIdentifier * id, const Kernel::PassedEnv & defaultStateEnv, Kernel::PassedDyn defaultStateDyn, Ast::StateReference * defaultState );
//		UserDynamicStateProperties( const Ast::PlacedIdentifier * id, const Kernel::DynamicEnvironmentKeyType & key, const Kernel::PassedEnv & defaultStateEnv, Kernel::PassedDyn defaultStateDyn, Ast::StateReference * defaultState );
			virtual ~UserDynamicStateProperties( );

			virtual Kernel::StateHandle fetch( const Kernel::PassedDyn & dyn ) const;
			virtual void makeBinding( Kernel::StateHandle val, const Ast::DynamicStateBindingExpression * bindingExpr, Kernel::EvalState * evalState ) const;
		};

		class WidthDynamicVariableProperties : public Kernel::DynamicVariableProperties
		{
		public:
			WidthDynamicVariableProperties( const Ast::PlacedIdentifier * id );
			virtual ~WidthDynamicVariableProperties( );
			virtual Kernel::VariableHandle fetch( const Kernel::PassedDyn & dyn ) const;
			virtual void makeBinding( Kernel::VariableHandle val, const Ast::DynamicBindingExpression * bindingExpr, Kernel::EvalState * evalState ) const;
		};
		class MiterLimitDynamicVariableProperties : public Kernel::DynamicVariableProperties
		{
		public:
			MiterLimitDynamicVariableProperties( const Ast::PlacedIdentifier * id );
			virtual ~MiterLimitDynamicVariableProperties( );
			virtual Kernel::VariableHandle fetch( const Kernel::PassedDyn & dyn ) const;
			virtual void makeBinding( Kernel::VariableHandle val, const Ast::DynamicBindingExpression * bindingExpr, Kernel::EvalState * evalState ) const;
		};

		class StrokingDynamicVariableProperties : public Kernel::DynamicVariableProperties
		{
		public:
			StrokingDynamicVariableProperties( const Ast::PlacedIdentifier * id );
			virtual ~StrokingDynamicVariableProperties( );
			virtual Kernel::VariableHandle fetch( const Kernel::PassedDyn & dyn ) const;
			virtual void makeBinding( Kernel::VariableHandle val, const Ast::DynamicBindingExpression * bindingExpr, Kernel::EvalState * evalState ) const;
		};
		class NonStrokingDynamicVariableProperties : public Kernel::DynamicVariableProperties
		{
		public:
			NonStrokingDynamicVariableProperties( const Ast::PlacedIdentifier * id );
			virtual ~NonStrokingDynamicVariableProperties( );
			virtual Kernel::VariableHandle fetch( const Kernel::PassedDyn & dyn ) const;
			virtual void makeBinding( Kernel::VariableHandle val, const Ast::DynamicBindingExpression * bindingExpr, Kernel::EvalState * evalState ) const;
		};
		class AlphaDynamicVariableProperties : public Kernel::DynamicVariableProperties
		{
			bool isStroking_;
		public:
			AlphaDynamicVariableProperties( const Ast::PlacedIdentifier * id, bool isStroking );
			virtual ~AlphaDynamicVariableProperties( );
			virtual Kernel::VariableHandle fetch( const Kernel::PassedDyn & dyn ) const;
			virtual void makeBinding( Kernel::VariableHandle val, const Ast::DynamicBindingExpression * bindingExpr, Kernel::EvalState * evalState ) const;
		};

		class DashDynamicVariableProperties : public Kernel::DynamicVariableProperties
		{
		public:
			DashDynamicVariableProperties( const Ast::PlacedIdentifier * id );
			virtual ~DashDynamicVariableProperties( );
			virtual Kernel::VariableHandle fetch( const Kernel::PassedDyn & dyn ) const;
			virtual void makeBinding( Kernel::VariableHandle val, const Ast::DynamicBindingExpression * bindingExpr, Kernel::EvalState * evalState ) const;
		};
		class CapStyleDynamicVariableProperties : public Kernel::DynamicVariableProperties
		{
		public:
			CapStyleDynamicVariableProperties( const Ast::PlacedIdentifier * id );
			virtual ~CapStyleDynamicVariableProperties( );
			virtual Kernel::VariableHandle fetch( const Kernel::PassedDyn & dyn ) const;
			virtual void makeBinding( Kernel::VariableHandle val, const Ast::DynamicBindingExpression * bindingExpr, Kernel::EvalState * evalState ) const;
		};
		class JoinStyleDynamicVariableProperties : public Kernel::DynamicVariableProperties
		{
		public:
			JoinStyleDynamicVariableProperties( const Ast::PlacedIdentifier * id );
			virtual ~JoinStyleDynamicVariableProperties( );
			virtual Kernel::VariableHandle fetch( const Kernel::PassedDyn & dyn ) const;
			virtual void makeBinding( Kernel::VariableHandle val, const Ast::DynamicBindingExpression * bindingExpr, Kernel::EvalState * evalState ) const;
		};
		class BlendModeDynamicVariableProperties : public Kernel::DynamicVariableProperties
		{
		public:
			BlendModeDynamicVariableProperties( const Ast::PlacedIdentifier * id );
			virtual ~BlendModeDynamicVariableProperties( );
			virtual Kernel::VariableHandle fetch( const Kernel::PassedDyn & dyn ) const;
			virtual void makeBinding( Kernel::VariableHandle val, const Ast::DynamicBindingExpression * bindingExpr, Kernel::EvalState * evalState ) const;
		};

		class GraphicsState
		{
		public:
			RefCountPtr< const Lang::Color > strokingColor_;			// Use Gray( -1 ) for no-op, and NullPtr for undefined
			RefCountPtr< const Lang::Color > nonStrokingColor_;	 // Use Gray( -1 ) for no-op, and NullPtr for undefined
			Concrete::Length width_;												// Use negative value for no-op, and NAN for undefined
			Lang::CapStyle::ValueType cap_;
			Lang::JoinStyle::ValueType join_;
			double miterLimit_;																		// Use negative value for no-op, and NAN for undefined
			RefCountPtr< const Lang::Dash > dash_;								// (Dash with negative scale is no-op.)	Use NullPtr for undefined
			Lang::BlendMode::ValueType blend_;
			bool alphaIsShape_;																			// See below.
			RefCountPtr< const Lang::Alpha > strokingAlpha_;			// Use negative value for no-op, NAN for undefined
			RefCountPtr< const Lang::Alpha > nonStrokingAlpha_;	 // Use negative value for no-op, NAN for undefined
		public:
			GraphicsState( );
			explicit GraphicsState( const Kernel::GraphicsState & orig );	 // explicit, since reference counting shall be used in most cases
			GraphicsState( const Kernel::GraphicsState & newValues, const Kernel::GraphicsState & oldValues );
			GraphicsState( bool setDefaults );
			~GraphicsState( );

			bool synchStrokingColor( std::ostream & os, const GraphicsState * ref, SimplePDF::PDF_Resources * resources, bool force = false );
			bool synchNonStrokingColor( std::ostream & os, const GraphicsState * ref, SimplePDF::PDF_Resources * resources, bool force = false );
			bool synchStrokingAlpha( std::ostream & os, const GraphicsState * ref, SimplePDF::PDF_Resources * resources, bool force = false );
			bool synchNonStrokingAlpha( std::ostream & os, const GraphicsState * ref, SimplePDF::PDF_Resources * resources, bool force = false );
			bool synchWidth( std::ostream & os, const GraphicsState * ref, SimplePDF::PDF_Resources * resources, bool force = false );
			bool synchCap( std::ostream & os, const GraphicsState * ref, SimplePDF::PDF_Resources * resources, bool force = false );
			bool synchJoin( std::ostream & os, const GraphicsState * ref, SimplePDF::PDF_Resources * resources, bool force = false );
			bool synchMiterLimit( std::ostream & os, const GraphicsState * ref, SimplePDF::PDF_Resources * resources, bool force = false );
			bool synchDash( std::ostream & os, const GraphicsState * ref, SimplePDF::PDF_Resources * resources, bool force = false );
			bool synchBlend( std::ostream & os, const GraphicsState * ref, SimplePDF::PDF_Resources * resources, bool force = false );

			bool synchForStroke( std::ostream & os, const GraphicsState * ref, SimplePDF::PDF_Resources * resources, bool force = false );
			bool synchForNonStroke( std::ostream & os, const GraphicsState * ref, SimplePDF::PDF_Resources * resources, bool force = false );

			bool synchStrokingColorWithNonStrokingColor( std::ostream & os, SimplePDF::PDF_Resources * resources, Concrete::Length width = Concrete::Length( std::numeric_limits< double >::signaling_NaN( ) ) );

			void print( std::ostream & os, const std::string & indentation ) const;
		};

		class Auto_qQ
		{
			/* Since it is not easy to determine from the PDF documentation that the text state is also
			 * affected by the q-Q pair, the text state was originally not saved here.
			 * However, this generated bad results on all tested PDF viewers when a transform
			 * was applied to a Clipped2D object, and it was concluded that with respect to q-Q, there
			 * is no difference between text and graphics states.
			 */
			Kernel::GraphicsState * graphicsState_;
			Kernel::GraphicsState * graphicsEnterState_;
			Kernel::TextState * textState_;
			Kernel::TextState * textEnterState_;
			std::ostream & os_;
			bool activated_;
		public:
			Auto_qQ( Kernel::GraphicsState * graphicsState, Kernel::TextState * textState, std::ostream & os, bool active = true );
			void activate( );
			~Auto_qQ( );
		};

	}
}
