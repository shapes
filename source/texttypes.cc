/* This file is part of Shapes.
 *
 * Shapes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Shapes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2008, 2010 Henrik Tidefelt
 */

#include "Shapes_Helpers_decls.h"

#include "texttypes.h"
#include "dynamicenvironment.h"
#include "lighttypes.h"
#include "ast.h"
#include "astvar.h"
#include "isnan.h"
#include "globals.h"
#include "autoonoff.h"
#include "afmscanner.h"
#include "charconverters.h"
#include "constructorrepresentation.h"
#include "pagecontentstates.h"
#include "methodbase.h"
#include "shapescore.h"
#include "warn.h"
#include "config.h"

#include <fstream>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdint.h>
#include <cstddef>
#include <iomanip>

#ifdef HAVE_FT2
#include FT_OUTLINE_H
#endif

using namespace Shapes;

std::map< RefCountPtr< const char >, RefCountPtr< SimplePDF::PDF_Object >, charRefPtrLess > Lang::Font::theFontResourceMap_;
std::map< RefCountPtr< const char >, RefCountPtr< const FontMetrics::FontMetric >, charRefPtrLess > Lang::Font::theFontMetricsMap_;
std::list< std::string > Lang::Font::theFontMetricsSearchPath_;

namespace Shapes
{
	namespace Helpers
	{
		Kernel::StructureFactory FontMethod_glyph_resultFactory( "odd?", "paths", "advance" );
	}
}

SimplePDF::PDF_ToUnicode::PDF_ToUnicode( RefCountPtr< CharSet > charSet, const RefCountPtr< const Shapes::Lang::Font > & font )
	: charSet_( charSet ),
		font_( font )
{
	dic[ "Filter" ] = SimplePDF::newName( "FlateDecode" );
}

SimplePDF::PDF_ToUnicode::~PDF_ToUnicode( )
{ }

void
SimplePDF::PDF_ToUnicode::writeTo( std::ostream & os, SimplePDF::PDF_xref * xref, const RefCountPtr< const PDF_Object > & self ) const
{
	std::ostringstream & wdata = const_cast< std::ostringstream & >( data );
	wdata << std::setfill( '0' );
	wdata << "/CIDInit /ProcSet findresource begin" << std::endl
				<< "12 dict begin" << std::endl
				<< "begincmap" << std::endl
				<< "/CIDSystemInfo << /Registry (Adobe) /Ordering (UCS) /Supplement 0 >> def" << std::endl
				<< "/CMapName /Adobe-Identity-UCS def" << std::endl
				<< "/CMapType 2 def" << std::endl
				<< "1 begincodespacerange" << std::endl
				<< "<0000> <FFFF>" << std::endl
				<< "endcodespacerange" << std::endl ;
	const size_t bufSize = 10;
	unsigned char buf[ bufSize ];
	size_t remaining = charSet_->size( );
	size_t room = std::min( static_cast< size_t >( 100 ), remaining );
	remaining -= room;
	wdata << room << " beginbfchar" << std::endl ;
	for( CharSet::const_iterator i = charSet_->begin( ); i != charSet_->end( ); ++i, --room )
		{
			if( room == 0 )
				{
					wdata << "endbfchar" << std::endl ;
					room = std::min( static_cast< size_t >( 100 ), remaining );
					remaining -= room;
					wdata << room << " beginbfchar" << std::endl ;
				}
			size_t out_avail = 2;
			unsigned char * out_buf = buf;
			font_->encode( *i, reinterpret_cast< char ** >( & out_buf ), & out_avail );
			if( out_avail != 0 )
				{
					throw Exceptions::InternalError( "Expected the encoded character to occupy exactly two bytes." );
				}
			wdata << "<" << std::hex << std::setw( 2 ) << static_cast< int >( buf[0] ) << std::hex << std::setw( 2 ) << static_cast< int >( buf[1] ) << "> " ;
			out_avail = bufSize;
			out_buf = buf;
			i->encode_UTF16BE( reinterpret_cast< char ** >( & out_buf ), & out_avail );
			wdata << "<" ;
			for( const unsigned char * c = buf; c != out_buf; ++c )
				{
					wdata << std::hex << std::setw( 2 ) << static_cast< int >( *c ) ;
				}
			wdata << ">" << std::endl ;
		}
	wdata << "endbfchar" << std::endl
				<< "endcmap" << std::endl
				<< "CMapName currentdict /CMap defineresource pop" << std::endl
				<< "end" << std::endl
				<< "end" ; /* No need to put a newline at the end. */
	PDF_Stream_out::writeTo( os, xref, self );
}

SimplePDF::PDF_Widths::PDF_Widths( RefCountPtr< CharSet > charSet, const RefCountPtr< const Shapes::Lang::Font > & font )
	: charSet_( charSet ),
		font_( font )
{ }

SimplePDF::PDF_Widths::~PDF_Widths( )
{ }

void
SimplePDF::PDF_Widths::writeTo( std::ostream & os, SimplePDF::PDF_xref * xref, const RefCountPtr< const PDF_Object > & self ) const
{
	const FontMetrics::WritingDirectionMetrics * metrics = font_->metrics( )->horizontalMetrics_.getPtr( );
	const size_t bufSize = 2;
	unsigned char buf[ bufSize ];
	VecType & wvec = const_cast< VecType & >( vec );
	FontMetrics::CharacterMetrics m( 0 );
	for( CharSet::const_iterator i = charSet_->begin( ); i != charSet_->end( ); )
		{
			CharSet::const_iterator j = i;
			CharSet::const_iterator last = i;
			++i;
			for( ; i != charSet_->end( ) && i->get_UCS4( ) == last->get_UCS4( ) + 1; last = i, ++i )
				;
			size_t out_avail = bufSize;
			unsigned char * out_buf = buf;
			font_->encode( j->get_UCS4( ), reinterpret_cast< char ** >( & out_buf ), & out_avail );
			if( out_avail != 0 )
				{
					throw Exceptions::InternalError( "Expected the encoded character to occupy exactly two bytes." );
				}
			wvec.push_back( SimplePDF::newInt( static_cast< SimplePDF::PDF_Int::ValueType >( buf[0] ) * 0x100 +
																				 static_cast< SimplePDF::PDF_Int::ValueType >( buf[1] ) ) );
			SimplePDF::PDF_Vector * sub_vec = new SimplePDF::PDF_Vector;
			wvec.push_back( RefCountPtr< SimplePDF::PDF_Object >( sub_vec ) );
			for( ; j != i; ++j )
				{
					sub_vec->vec.push_back( SimplePDF::newFloat( metrics->charByCode( j->get_UCS4( ), & m )->horizontalCharWidthX_* 1000 ) );
				}
		}
	PDF_Vector::writeTo( os, xref, self );
}

namespace Shapes
{

	namespace Lang
	{

		class FontMethod_glyph : public Lang::MethodBase< Lang::Font >
		{
		public:
			FontMethod_glyph( RefCountPtr< const Lang::Font > _self, const Ast::FileID * fullMethodID );
			virtual ~FontMethod_glyph( );
			virtual void call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const;
			static const char * staticFieldID( ) { return "glyph"; }
		};

	}
}

void
Font_register_methods( Lang::SystemFinalClass * dstClass )
{
	dstClass->registerMethod( new Kernel::MethodFactory< Lang::Font, Lang::FontMethod_glyph >( ) );
}

void
Lang::Font::push_backFontMetricsPath( const std::string & path )
{
	if( path.empty( ) ||
			path[ path.size( ) - 1 ] == '/' )
		{
			theFontMetricsSearchPath_.push_back( path );
		}
	else
		{
			theFontMetricsSearchPath_.push_back( path + "/" );
		}
}

std::string
Lang::Font::searchGlyphList( )
{
	std::string res;

	if( theFontMetricsSearchPath_.empty( ) )
		{
			throw Exceptions::ExternalError( strrefdup( "The font metrics path was not set up (needed for the glyph list).	Consider defining the environment variable SHAPESFONTMETRICS." ) );
		}

	typedef typeof theFontMetricsSearchPath_ ListType;
	for( ListType::const_iterator i = theFontMetricsSearchPath_.begin( ); i != theFontMetricsSearchPath_.end( ); ++i )
		{
			res = *i + "glyphlist.txt";
			struct stat theStatDummy;
			if( stat( res.c_str( ), & theStatDummy ) == 0 )
				{
					return res;
				}
		}
	throw Exceptions::MiscellaneousRequirement( "A font operation required the glyph list, but it was not found.	It should be named \"glyphlist.txt\" and reside in a directory on the font metrics path.	Please refer to your environment variable SHAPESFONTMETRICS." );
}

std::string
Lang::Font::searchFontMetrics( RefCountPtr< const char > fontName )
{
	std::string res;

	if( strlen( fontName.getPtr( ) ) == 0 )
		{
			throw Exceptions::InternalError( strrefdup( "Lang::Font::searchFontMetrics called with empty argument." ) );
		}

	if( *fontName.getPtr( ) == '/' )
		{
			throw Exceptions::InternalError( "The font name cannot begin with \"/\", as if is was an absolute path to something." );
		}

	if( theFontMetricsSearchPath_.empty( ) )
		{
			throw Exceptions::ExternalError( strrefdup( "The font metrics path was not set up.	Consider defining the environment variable SHAPESFONTMETRICS." ) );
		}

	typedef typeof theFontMetricsSearchPath_ ListType;
	for( ListType::const_iterator i = theFontMetricsSearchPath_.begin( ); i != theFontMetricsSearchPath_.end( ); ++i )
		{
			res = *i + fontName.getPtr( ) + ".afm";
			struct stat theStatDummy;
			if( stat( res.c_str( ), & theStatDummy ) == 0 )
				{
					return res;
				}
		}
	throw Exceptions::MissingFontMetrics( fontName, & theFontMetricsSearchPath_ );
}

std::string
Lang::Font::searchCharacterEncoding( const char * encodingName )
{
	std::string res;

	if( theFontMetricsSearchPath_.empty( ) )
		{
			throw Exceptions::ExternalError( strrefdup( "The font metrics path was not set up.	Consider defining the environment variable SHAPESFONTMETRICS." ) );
		}

	typedef typeof theFontMetricsSearchPath_ ListType;
	for( ListType::const_iterator i = theFontMetricsSearchPath_.begin( ); i != theFontMetricsSearchPath_.end( ); ++i )
		{
			res = *i + encodingName + ".enc";
			struct stat theStatDummy;
			if( stat( res.c_str( ), & theStatDummy ) == 0 )
				{
					return res;
				}
		}
	std::ostringstream msg;
	msg << "A font operation required a character encoding file for " << encodingName << ", but it was not found.	It should be named \"" << encodingName << ".enc\" and reside in a directory on the font metrics path.	Please refer to your environment variable SHAPESFONTMETRICS." ;
	throw Exceptions::MiscellaneousRequirement( strrefdup( msg ) );
}


Lang::Font::Font( const RefCountPtr< const char > fontName, bool outline, RefCountPtr< SimplePDF::PDF_Object > & resource, RefCountPtr< const FontMetrics::FontMetric > metrics )
	: fontName_( fontName ), outline_( outline ), resource_( resource ), metrics_( metrics )
{
	// If this font has been instantiated before, it is reused.
	{
		typedef typeof theFontResourceMap_ MapType;
		MapType::const_iterator i = theFontResourceMap_.find( fontName_ );
		if( i != theFontResourceMap_.end( ) )
			{
				resource_ = i->second;
			}
		else
			{
				theFontResourceMap_.insert( MapType::value_type( fontName_, resource_ ) );
			}
	}
	{
		typedef typeof theFontMetricsMap_ MapType;
		MapType::const_iterator i = theFontMetricsMap_.find( fontName_ );
		if( i != theFontMetricsMap_.end( ) )
			{
				metrics_ = i->second;
			}
		else if( metrics_ != NullPtr< const FontMetrics::FontMetric >( ) )
			{
				theFontMetricsMap_.insert( MapType::value_type( fontName_, metrics_ ) );
			}
	}
}

// Note that we must not initialize name_ with builtinFontMap_[ fontConst ] here, since builtinFontMap_ may not be
// initialized when this constructor is invoked from globals.cc.
Lang::Font::Font( const RefCountPtr< const char > fontName )
	: fontName_( fontName ), outline_( false ), resource_( NullPtr< SimplePDF::PDF_Object >( ) ), metrics_( NullPtr< const FontMetrics::FontMetric >( ) )
{ }

Lang::Font::~Font( )
{ }

Kernel::VariableHandle
Lang::Font::getField( const char * fieldID, const RefCountPtr< const Lang::Value > & selfRef ) const
{
// 	if( strcmp( fieldID, "CIDFont?" ) == 0 )
// 		{
// 			return Helpers::newValHandle( new Lang::Boolean( metrics_->isCIDFont_ ) );
// 		}
	if( strcmp( fieldID, "kerning?" ) == 0 )
		{
			return Helpers::newValHandle( new Lang::Boolean( metrics_->hasKerning_ ) );
		}
	if( strcmp( fieldID, "family" ) == 0 )
		{
			return Kernel::VariableHandle( new Kernel::Variable( this->family_name( ) ) );
		}
	if( strcmp( fieldID, "style" ) == 0 )
		{
			return Kernel::VariableHandle( new Kernel::Variable( this->style_name( ) ) );
		}
	if( strcmp( fieldID, "PostScript_name" ) == 0 )
		{
			return Kernel::VariableHandle( new Kernel::Variable( this->PostScript_name( ) ) );
		}

	return TypeID->getMethod( selfRef, fieldID ); /* This will throw if there is no such method. */
}

RefCountPtr< const Lang::String >
Lang::Font::family_name( ) const
{
	return RefCountPtr< const Lang::String >( new Lang::String( fontName_ ) );
}

RefCountPtr< const Lang::String >
Lang::Font::style_name( ) const
{
	return RefCountPtr< const Lang::String >( new Lang::String( "", true ) );
}

RefCountPtr< const Lang::Symbol >
Lang::Font::PostScript_name( ) const
{
	return RefCountPtr< const Lang::Symbol >( new Lang::Symbol( fontName_.getPtr( ) ) );
}

RefCountPtr< const Lang::Value >
Lang::Font::getGlyph( Kernel::UnicodeCodePoint c, Concrete::Length size ) const
{
	throw Exceptions::MiscellaneousRequirement( "It is not possible to extract individual glyphs from this font." );
}


const RefCountPtr< SimplePDF::PDF_Object > &
Lang::Font::resource( ) const
{
	if( resource_ != NullPtr< SimplePDF::PDF_Object >( ) )
		{
			return resource_;
		}

	typedef typeof theFontResourceMap_ MapType;

	{
		MapType::const_iterator i = theFontResourceMap_.find( fontName_ );
		if( i != theFontResourceMap_.end( ) )
			{
				resource_ = i->second;
				return resource_;
			}
	}

	{
		RefCountPtr< SimplePDF::PDF_Dictionary > dic;
		resource_ = SimplePDF::indirect( dic, & Kernel::theIndirectObjectCount );
		(*dic)[ "Type" ] = SimplePDF::newName( "Font" );
		(*dic)[ "Subtype" ] = SimplePDF::newName( "Type1" );
		(*dic)[ "Encoding" ] = SimplePDF::newName( "MacRomanEncoding" );
		(*dic)[ "BaseFont" ] = SimplePDF::newName( fontName_.getPtr( ) );	// Here, it is crucial that this is really a built-in font!
	}
	return resource_;
}

RefCountPtr< const char >
Lang::Font::fontName( ) const
{
	return fontName_;
}

bool
Lang::Font::outline( ) const
{
	return outline_;
}

RefCountPtr< const FontMetrics::FontMetric >
Lang::Font::metrics( ) const
{
	if( metrics_ != NullPtr< const FontMetrics::FontMetric >( ) )
		{
			return metrics_;
		}

	// Otherwise, we hope that we can find an Adobe Font Metrics file.
	std::string filename = searchFontMetrics( fontName_ );
	std::ifstream afmFile( filename.c_str( ) );
	if( ! afmFile.is_open( ) )
		{
			std::ostringstream oss;
			oss << "File has been located but couldn't be opened: " << filename ;
			throw Exceptions::ExternalError( strrefdup( oss ) );
		}

	// I fiddle a little with the consts here...
	FontMetrics::AFM * newMetrics = new FontMetrics::AFM;
	metrics_ = RefCountPtr< const FontMetrics::FontMetric >( newMetrics );

	AfmScanner scanner( newMetrics, & afmFile );
	scanner.setTellQue( Interaction::fontMetricMessages ); // We want to know about things that are not recognized and this ignored.
	if( Interaction::fontMetricDebug )
		{
			scanner.set_debug( 1 );
		}
	try
		{
			int status = scanner.yylex( );
			if( status != 0 )
				{
					std::ostringstream oss;
					oss << "Font metrics parser returned with non-zero status: " << status ;
					throw Exceptions::InternalError( strrefdup( oss ) );
				}
		}
	catch( const char * ball )
		{
			std::ostringstream oss;
			oss << "Font metrics parser failed with message: " << ball ;
			throw Exceptions::InternalError( strrefdup( oss ) );
		}
	catch( const RefCountPtr< const char > ball )
		{
			std::ostringstream oss;
			oss << "Font metrics parser failed with message: " << ball ;
			throw Exceptions::InternalError( strrefdup( oss ) );
		}

	return metrics_;
}

void
Lang::Font::show( std::ostream & os ) const
{
	os << "< font: " << fontName_ << " >" ;
}

void
Lang::Font::gcMark( Kernel::GCMarkedSet & marked )
{ }

RefCountPtr< const Lang::Class > Lang::Font::TypeID( new Lang::SystemFinalClass( strrefdup( "Font" ), Font_register_methods ) );
TYPEINFOIMPL( Font );


Lang::PDFStandardFont::PDFStandardFont( const RefCountPtr< const char > builtInFontName )
	: Lang::Font( builtInFontName )
{ }

Lang::PDFStandardFont::~PDFStandardFont( )
{ }

void
Lang::PDFStandardFont::encode( const char ** text_UTF8, size_t * in_avail, char ** dst_PDF, size_t * out_avail ) const
{
	encode_MacRoman( text_UTF8, in_avail, dst_PDF, out_avail );
}

void
Lang::PDFStandardFont::encode( Kernel::UnicodeCodePoint c, char ** dst_PDF, size_t * out_avail ) const
{
	encode_MacRoman( c, dst_PDF, out_avail );
}

void
Lang::PDFStandardFont::encode_MacRoman( const char ** text_UTF8, size_t * in_avail, char ** dst_PDF, size_t * out_avail )
{
	iconv_t converter = Helpers::requireUTF8ToMacRomanConverter( );
	// The ICONV_CAST macro is defined in config.h.
	size_t count = iconv( converter,
												ICONV_CAST( text_UTF8 ), in_avail,
												dst_PDF, out_avail );
	if( count == (size_t)(-1) )
		{
			if( errno == EILSEQ )
				{
					throw Exceptions::MiscellaneousRequirement( "It is suspected that one of the UFT-8 characters used in showed text cannot be represented in the MacRoman encoding (used for the 14 standard fonts of PDF; consider using a normal font instead)." );
				}
			else if( errno == EINVAL )
				{
					throw Exceptions::MiscellaneousRequirement( "It is suspected that showed text ended with an incomplete multibyte character." );
				}
			else if( errno == E2BIG )
				{
					throw Exceptions::InternalError( "The buffer allocated for UTF-8 to PDF show-text string conversion was too small." );
				}
			else
				{
					std::ostringstream msg;
					msg << "iconv failed with an unrecognized error code: " << errno ;
					throw Exceptions::InternalError( strrefdup( msg ) );
				}
		}
}

void
Lang::PDFStandardFont::encode_MacRoman( Kernel::UnicodeCodePoint c, char ** dst_PDF, size_t * out_avail )
{
	if( *out_avail < 1 )
		{
			throw Exceptions::InternalError( "The buffer allocated conversion of a single UCS-4 to MacRoman was too small." );
		}
	**dst_PDF = c.get_MacRoman( );
	++*dst_PDF;
	--*out_avail;
}


Lang::Type3Font::Type3Font( const RefCountPtr< const char > fontName, RefCountPtr< SimplePDF::PDF_Object > & resource, RefCountPtr< const FontMetrics::FontMetric > metrics )
	: Lang::Font( fontName, false, resource, metrics )
{ }

Lang::Type3Font::~Type3Font( )
{ }

void
Lang::Type3Font::encode( const char ** text_UTF8, size_t * in_avail, char ** dst_PDF, size_t * out_avail ) const
{
	Lang::PDFStandardFont::encode_MacRoman( text_UTF8, in_avail, dst_PDF, out_avail );
}

void
Lang::Type3Font::encode( Kernel::UnicodeCodePoint c, char ** dst_PDF, size_t * out_avail ) const
{
	Lang::PDFStandardFont::encode_MacRoman( c, dst_PDF, out_avail );
}



#ifdef HAVE_FT2

Lang::FreeTypeFont::FreeTypeFont( FT_Face face, const RefCountPtr< const char > fontName, bool outline, RefCountPtr< SimplePDF::PDF_Object > & resource, RefCountPtr< const FontMetrics::FontMetric > metrics, RefCountPtr< SimplePDF::PDF_ToUnicode::CharSet > & usedChars )
	: Lang::Font( fontName, outline, resource, metrics ),
		face_( face ),
		usedChars_( usedChars )
{ }

Lang::FreeTypeFont::~FreeTypeFont( )
{ }

void
Lang::FreeTypeFont::encode( const char ** text_UTF8, size_t * in_avail, char ** dst_PDF, size_t * out_avail ) const
{
	iconv_t converter = Helpers::requireUTF8ToUCS4Converter( );

	size_t bufSize = 4 * *in_avail;
	char * buf = new char[ bufSize ];
	char * outbuf = buf;
	DeleteOnExit< char > bufDeleter( buf );

	// The ICONV_CAST macro is defined in config.h.
	size_t count = iconv( converter,
												ICONV_CAST( text_UTF8 ), in_avail,
												& outbuf, & bufSize );
	if( count == (size_t)(-1) )
		{
			if( errno == EILSEQ )
				{
					throw Exceptions::MiscellaneousRequirement( "Failed to convert from UTF-8 to UCS-4 code points." );
				}
			else if( errno == EINVAL )
				{
					throw Exceptions::MiscellaneousRequirement( "It is suspected that showed text ended with an incomplete multibyte character." );
				}
			else if( errno == E2BIG )
				{
					throw Exceptions::InternalError( "The temporary UCS-4 buffer allocated for UTF-8 to PDF show-text string conversion was too small." );
				}
			else
				{
					std::ostringstream msg;
					msg << "iconv failed with an unrecognized error code: " << errno ;
					throw Exceptions::InternalError( strrefdup( msg ) );
				}
		}
	/* The number of characters equals ( outbuf - buf ) / 4, so the memory needed to store these equals
	 *   2 * ( outbuf - buf ) / 4 = ( outbuf - buf ) / 2
	 */
	if( static_cast< ptrdiff_t >( *out_avail ) < ( outbuf - buf ) / 2 )
		{
			throw Exceptions::InternalError( "The buffer allocated for UTF-8 to PDF show-text string conversion was too small." );
		}
	*out_avail -= ( outbuf - buf ) / 2;
	for( const char * src = buf; src != outbuf; src += 4)
		{
			Kernel::UnicodeCodePoint code;
			code.decode_UCS4( src );
			usedChars_->insert( usedChars_->begin( ), code );
			FT_UInt index = FT_Get_Char_Index( face_, code.get_UCS4( ) );
			if( index == 0 )
				{
					std::ostringstream msg;
					msg << "Missing glyph U+" << std::hex << code.get_UCS4( ) << "." ;
					WARN_OR_THROW( Exceptions::FontProblem( this->PostScript_name( ), strrefdup( msg ), true ) );
				}
			if( index > 0xFFFF )
				{
					throw Exceptions::InternalError( "PDF text encoding: Glyph index is out of the two byte code range." );
				}
			/* Remember that the endianness is unknown!  Hence, don't */
			//			*reinterpret_cast< uint16_t * >( *dst_PDF ) = index;
			//			*dst_PDF += 2;
			/* ... but do */
			**dst_PDF = index / 0x100;
			++*dst_PDF;
			**dst_PDF = index % 0x100;
			++*dst_PDF;
		}
}

void
Lang::FreeTypeFont::encode( Kernel::UnicodeCodePoint c, char ** dst_PDF, size_t * out_avail ) const
{
	if( *out_avail < 2 )
		{
			throw Exceptions::InternalError( "The buffer allocated conversion of one UCS-4 code point to PDF show-text encoding was too small." );
		}
	FT_UInt index = FT_Get_Char_Index( face_, c.get_UCS4( ) );
	if( index == 0 )
		{
			std::ostringstream msg;
			msg << "Missing glyph U+" << std::hex << c.get_UCS4( ) << ".";
			WARN_OR_THROW( Exceptions::FontProblem( this->PostScript_name( ), strrefdup( msg ), true ) );
		}
	if( index > 0xFFFF )
		{
			throw Exceptions::InternalError( "PDF text encoding: Glyph index is out of the two byte code range." );
		}
	/* Remember that the endianness is unknown!  Hence, don't */
	//			*reinterpret_cast< uint16_t * >( *dst_PDF ) = index;
	//			*dst_PDF += 2;
	/* ... but do */
	**dst_PDF = index / 0x100;
	++*dst_PDF;
	**dst_PDF = index % 0x100;
	++*dst_PDF;
	*out_avail -= 2;
}

RefCountPtr< const Lang::String >
Lang::FreeTypeFont::family_name( ) const
{
	return RefCountPtr< const Lang::String >( new Lang::String( face_->family_name, true ) );
}

RefCountPtr< const Lang::String >
Lang::FreeTypeFont::style_name( ) const
{
	return RefCountPtr< const Lang::String >( new Lang::String( face_->style_name, true ) );
}

RefCountPtr< const Lang::Symbol >
Lang::FreeTypeFont::PostScript_name( ) const
{
	const char * PostScriptName = FT_Get_Postscript_Name( face_ );
	if( PostScriptName == 0 )
		{
			throw Exceptions::MiscellaneousRequirement( "The FreeType font does not provide a PostScript name." );
		}
	/* It is assumed that the created symbol is not used after the face_ is destroyed! */
	return RefCountPtr< const Lang::Symbol >( new Lang::Symbol( PostScriptName ) );
}

namespace Shapes
{
	namespace Helpers
	{
		class GetGlyphState
		{
		public:
			Lang::MultiPath2D * paths_;
			Lang::ElementaryPath2D * contour_;
			Concrete::PathPoint2D * last_;
			Concrete::Length scaled_font_unit_;

			void close( )
			{
				if( contour_ != 0 )
					{
						/* We should eliminate the duplicated point where the path begins and ends. */
						contour_->pop_back( );
						if( last_->rear_ != last_->mid_ )
							{
								contour_->front( )->rear_ = last_->rear_;
								last_->rear_ = last_->mid_; /* This gives away the ownership of last_->rear_. */
							}
						delete last_;
						last_ = 0;
						contour_->close( );
						contour_ = 0;
					}
			}
			int move_to( const FT_Vector & to )
			{
				close( );
				contour_ = new Lang::ElementaryPath2D( );
				paths_->push_back( RefCountPtr< const Lang::Path2D >( contour_ ) );
				last_ = new Concrete::PathPoint2D( new Concrete::Coords2D( to.x * scaled_font_unit_, to.y * scaled_font_unit_ ) );
				contour_->push_back( last_ );
				return 0;
			}
			int line_to( const FT_Vector & to )
			{
				last_ = new Concrete::PathPoint2D( new Concrete::Coords2D( to.x * scaled_font_unit_, to.y * scaled_font_unit_ ) );
				contour_->push_back( last_ );
				return 0;
			}
			int conic_to( const FT_Vector & control, const FT_Vector & to )
			{
				Concrete::Coords2D c( control.x * scaled_font_unit_, control.y * scaled_font_unit_ );
				last_->front_ = new Concrete::Coords2D( (2./3) * c + (1./3) * *last_->mid_ );
				last_ = new Concrete::PathPoint2D( new Concrete::Coords2D( to.x * scaled_font_unit_, to.y * scaled_font_unit_ ) );
				last_->rear_ = new Concrete::Coords2D( (2./3) * c + (1./3) * *last_->mid_ );
				contour_->push_back( last_ );
				return 0;
			}
			int cubic_to( const FT_Vector & control1, const FT_Vector & control2, const FT_Vector & to )
			{
				last_->front_ = new Concrete::Coords2D( control1.x * scaled_font_unit_, control1.y * scaled_font_unit_ );
				last_ = new Concrete::PathPoint2D( new Concrete::Coords2D( to.x * scaled_font_unit_, to.y * scaled_font_unit_ ) );
				last_->rear_ = new Concrete::Coords2D( control2.x * scaled_font_unit_, control2.y * scaled_font_unit_ );
				contour_->push_back( last_ );
				return 0;
			}

			GetGlyphState( Lang::MultiPath2D * paths, Concrete::Length scaled_font_unit )
				: paths_( paths ),
					contour_( 0 ),
					last_( 0 ),
					scaled_font_unit_( scaled_font_unit )
			{ }
			~GetGlyphState( )
			{
				close( );
			}
		};
		int getGlyph_move_to( const FT_Vector * to, void * stateUntyped )
		{
			return reinterpret_cast< GetGlyphState * >( stateUntyped )->move_to( *to );
		}
		int getGlyph_line_to( const FT_Vector * to, void * stateUntyped )
		{
			return reinterpret_cast< GetGlyphState * >( stateUntyped )->line_to( *to );
		}
		int getGlyph_conic_to( const FT_Vector * control, const FT_Vector * to, void * stateUntyped )
		{
			return reinterpret_cast< GetGlyphState * >( stateUntyped )->conic_to( *control, *to );
		}
		int getGlyph_cubic_to( const FT_Vector * control1, const FT_Vector * control2, const FT_Vector * to, void * stateUntyped )
		{
			return reinterpret_cast< GetGlyphState * >( stateUntyped )->cubic_to( *control1, *control2, *to );
		}

		class ShipoutGlyphState
		{
		public:
			bool open_;
			Concrete::Coords2D last_;
			std::ostream & os_;
			Concrete::Length scaled_font_unit_;
			const Lang::Transform2D & tf_;

			void close( )
			{
				if( open_ )
					{
						os_ << "h " ;
						open_ = false;
					}
			}
			int move_to( const FT_Vector & to )
			{
				close( );
				last_ = Concrete::Coords2D( to.x * scaled_font_unit_, to.y * scaled_font_unit_ ).transformed( tf_ );
				os_ << last_ << " m " ;
				open_ = true;
				return 0;
			}
			int line_to( const FT_Vector & to )
			{
				last_ = Concrete::Coords2D( to.x * scaled_font_unit_, to.y * scaled_font_unit_ ).transformed( tf_ );
				os_ << last_ << " l " ;
				return 0;
			}
			int conic_to( const FT_Vector & control, const FT_Vector & to )
			{
				Concrete::Coords2D c = Concrete::Coords2D( control.x * scaled_font_unit_, control.y * scaled_font_unit_ ).transformed( tf_ );
				Concrete::Coords2D control1 = (2./3) * c + (1./3) * last_;
				last_ = Concrete::Coords2D( to.x * scaled_font_unit_, to.y * scaled_font_unit_ ).transformed( tf_ );
				Concrete::Coords2D control2 = (2./3) * c + (1./3) * last_;
				os_ << control1 << " " << control2 << " " << last_ << " c " ;
				return 0;
			}
			int cubic_to( const FT_Vector & control1, const FT_Vector & control2, const FT_Vector & to )
			{
				last_ = Concrete::Coords2D( to.x * scaled_font_unit_, to.y * scaled_font_unit_ ).transformed( tf_ );
				os_ << Concrete::Coords2D( control1.x * scaled_font_unit_, control1.y * scaled_font_unit_ ).transformed( tf_ )
						<< " " << Concrete::Coords2D( control2.x * scaled_font_unit_, control2.y * scaled_font_unit_ ).transformed( tf_ )
						<< " " << last_
						<< " c " ;
				return 0;
			}

			ShipoutGlyphState( std::ostream & os,
												 Concrete::Length scaled_font_unit,
												 const Lang::Transform2D & tf )
				: open_( false ),
					last_( 0, 0 ),
					os_( os ),
					scaled_font_unit_( scaled_font_unit ),
					tf_( tf )
			{ }
			~ShipoutGlyphState( )
			{
				close( );
			}
		};
		int shipoutGlyph_move_to( const FT_Vector * to, void * stateUntyped )
		{
			return reinterpret_cast< ShipoutGlyphState * >( stateUntyped )->move_to( *to );
		}
		int shipoutGlyph_line_to( const FT_Vector * to, void * stateUntyped )
		{
			return reinterpret_cast< ShipoutGlyphState * >( stateUntyped )->line_to( *to );
		}
		int shipoutGlyph_conic_to( const FT_Vector * control, const FT_Vector * to, void * stateUntyped )
		{
			return reinterpret_cast< ShipoutGlyphState * >( stateUntyped )->conic_to( *control, *to );
		}
		int shipoutGlyph_cubic_to( const FT_Vector * control1, const FT_Vector * control2, const FT_Vector * to, void * stateUntyped )
		{
			return reinterpret_cast< ShipoutGlyphState * >( stateUntyped )->cubic_to( *control1, *control2, *to );
		}
	}
}

const FT_Outline &
Lang::FreeTypeFont::getGlyphOutline( Kernel::UnicodeCodePoint c ) const
{
	FT_UInt index = FT_Get_Char_Index( face_, c.get_UCS4( ) );
	if( index == 0 )
		{
			const size_t bufsize = 10;
			char buf[ bufsize ];
			char * tmp = buf;
			size_t outavail = bufsize - 1;
			c.encode_UTF8( & tmp, & outavail );
			*tmp = '\0';
			std::ostringstream msg;
			msg << "The font " << fontName_ << " does not cover the character `" << buf << "´ (code point " << c.get_UCS4( ) << ")." ;
			throw Exceptions::OutOfRange( strrefdup( msg ) );
		}
	FT_Error error = FT_Load_Glyph( face_, index, FT_LOAD_NO_SCALE );
  if( error != 0 )
		{
			std::ostringstream msg;
			msg << "Unable to load glyph in FreeType, at code point " << c.get_UCS4( ) ;
			throw Shapes::Exceptions::OutOfRange( strrefdup( msg ) );
		}
	if( face_->glyph->format != FT_GLYPH_FORMAT_OUTLINE )
		{
			const size_t bufsize = 10;
			char buf[ bufsize ];
			char * tmp = buf;
			size_t outavail = bufsize - 1;
			c.encode_UTF8( & tmp, & outavail );
			*tmp = '\0';
			std::ostringstream msg;
			msg << "Unsupported glyph format in the font " << fontName_ << ", character `" << buf << "´ (code point " << c.get_UCS4( ) << ")." ;
			throw Exceptions::MiscellaneousRequirement( strrefdup( msg ) );
		}

	return face_->glyph->outline;
}

RefCountPtr< const Lang::Value >
Lang::FreeTypeFont::getGlyph( Kernel::UnicodeCodePoint c, Concrete::Length size ) const
{
	const FT_Outline & ol = getGlyphOutline( c );

	Helpers::FontMethod_glyph_resultFactory.set
		( "odd?", ( ( ol.flags & FT_OUTLINE_REVERSE_FILL ) == 0 ) ? Kernel::THE_FALSE_VARIABLE : Kernel::THE_TRUE_VARIABLE );
	Concrete::Length font_unit_scaled = size / static_cast< double >( face_->units_per_EM );
	Lang::MultiPath2D * pathsPtr = new Lang::MultiPath2D( );
	RefCountPtr< const Lang::MultiPath2D > paths( pathsPtr );
	{
		FT_Outline_Funcs emitters;
		emitters.move_to = Helpers::getGlyph_move_to;
		emitters.line_to = Helpers::getGlyph_line_to;
		emitters.conic_to = Helpers::getGlyph_conic_to;
		emitters.cubic_to = Helpers::getGlyph_cubic_to;
		emitters.shift = 0;
		emitters.delta = 0;
		Helpers::GetGlyphState state( pathsPtr, font_unit_scaled );
		FT_Error error = FT_Outline_Decompose( & face_->glyph->outline, & emitters, reinterpret_cast< void * >( & state ) );
		if( error != 0 )
			{
				std::ostringstream msg;
				msg << "FreeType error, FT_Outline_Decompose returned with error: " << Kernel::FreeTypeErrorMessage( error ) ;
				throw Exceptions::ExternalError( strrefdup( msg ) );
			}
	}
	Helpers::FontMethod_glyph_resultFactory.set( "paths", Kernel::VariableHandle( new Kernel::Variable( paths ) ) );

	Helpers::FontMethod_glyph_resultFactory.set( "advance", Helpers::newValHandle( new Lang::Length( face_->glyph->metrics.horiAdvance * font_unit_scaled ) ) );

	return Helpers::FontMethod_glyph_resultFactory.build( );
}

void
Lang::FreeTypeFont::shipout_glyph( Kernel::UnicodeCodePoint c, Concrete::Length size, const Lang::Transform2D & tf, std::ostream & os, bool * dstIsOdd, Concrete::Length * dstAdvance ) const
{
	const FT_Outline & ol = getGlyphOutline( c );

	Concrete::Length font_unit_scaled = size / static_cast< double >( face_->units_per_EM );

	*dstIsOdd = ( ol.flags & FT_OUTLINE_REVERSE_FILL ) != 0;
	*dstAdvance = Concrete::Length( face_->glyph->metrics.horiAdvance * font_unit_scaled ); /* Here we use that the correct glyph is in the glyph slot. */

	FT_Outline_Funcs emitters;
	emitters.move_to = Helpers::shipoutGlyph_move_to;
	emitters.line_to = Helpers::shipoutGlyph_line_to;
	emitters.conic_to = Helpers::shipoutGlyph_conic_to;
	emitters.cubic_to = Helpers::shipoutGlyph_cubic_to;
	emitters.shift = 0;
	emitters.delta = 0;
	Helpers::ShipoutGlyphState state( os, font_unit_scaled, tf );
	FT_Error error = FT_Outline_Decompose( const_cast< FT_Outline * >( & ol ), & emitters, reinterpret_cast< void * >( & state ) );
	if( error != 0 )
		{
			std::ostringstream msg;
			msg << "FreeType error, FT_Outline_Decompose returned with error: " << Kernel::FreeTypeErrorMessage( error ) ;
			throw Exceptions::ExternalError( strrefdup( msg ) );
		}
}

#endif /* End of #ifdef HAVE_FT2. */

Lang::TextOperation::TextOperation( )
{ }

Lang::TextOperation::~TextOperation( )
{ }

RefCountPtr< const Lang::Class > Lang::TextOperation::TypeID( new Lang::SystemFinalClass( strrefdup( "TextOperation" ) ) );
TYPEINFOIMPL( TextOperation );


Lang::KernedText::KernedText( const RefCountPtr< const Kernel::TextState > & textState, const RefCountPtr< const Kernel::GraphicsState > & metaState )
	: textState_( textState ), metaState_( metaState ), maxLength_( 0 )
{ }

Lang::KernedText::KernedText( const RefCountPtr< const Kernel::TextState > & textState, const RefCountPtr< const Kernel::GraphicsState > & metaState, const RefCountPtr< const Lang::String > & str)
	: textState_( textState ), metaState_( metaState ), maxLength_( 0 )
{
	pushString( str );
}

Lang::KernedText::~KernedText( )
{ }

Kernel::VariableHandle
Lang::KernedText::getField( const char * fieldID, const RefCountPtr< const Lang::Value > & selfRef ) const
{
	if( strcmp( fieldID, "list" ) == 0 )
		{
			return Kernel::VariableHandle( new Kernel::Variable( makeList( ) ) );
		}
	throw Exceptions::NonExistentMember( getTypeName( ), fieldID );
}

RefCountPtr< const Lang::SingleList >
Lang::KernedText::makeList( ) const
{
	/* The list is first computed in a bidirectional list, and then we construct the stateless cons list.
	 */

	/* I'm lazy today, so i use a cons pair to group the horizontal step with the glyph.	The better solution
	 * would be to use a structure with nicely named fields...
	 */

	std::list< RefCountPtr< const Lang::Value > > revlist;

	iconv_t converter = Helpers::requireUTF8ToUCS4Converter( );

	RefCountPtr< FontMetrics::WritingDirectionMetrics > horizontalMetrics = textState_->font_->metrics( )->horizontalMetrics_;
	if( horizontalMetrics == NullPtr< FontMetrics::WritingDirectionMetrics >( ) )
		{
			throw Exceptions::FontMetricsError( textState_->font_->fontName( ), strrefdup( "No horizontal metrics defined." ) );
		}
	const FontMetrics::CharacterMetrics * defaultCharMetrics = horizontalMetrics->default_char( );

	Concrete::Length ySize = textState_->size_;
	Concrete::Length xSize = ySize * textState_->horizontalScaling_;
	Concrete::Length characterTrackKern = textState_->horizontalScaling_ * textState_->characterSpacingConcrete( );
	Concrete::Length wordTrackKern = textState_->horizontalScaling_ * textState_->wordSpacingConcrete( );

	Concrete::Length xpos = 0;

	size_t bufSize = 4 * maxLength_;				 // This will be enough if UCS4 coding is used, since this encoding uses only one byte per character.
	char * buf = new char[ bufSize ];
	DeleteOnExit< char > bufDeleter( buf );

	FontMetrics::CharacterMetrics tmpCharMetric( 0 );

	typedef typeof strings_ ListType;
	std::list< double >::const_iterator ki = kernings_.begin( );
	for( ListType::const_iterator i = strings_.begin( ); i != strings_.end( ); ++i )
		{
			if( *i != NullPtr< const Lang::String >( ) )
				{
					const char * inbuf = (*i)->val_.getPtr( );
					if( strchr( inbuf, '\n' ) != 0 )
						{
							throw Exceptions::MiscellaneousRequirement( "Newlines cannot be represented in the pos-character list." );
						}
					char * outbuf = buf;
					size_t inbytesleft = strlen( inbuf );
					size_t outbytesleft = bufSize;
					// The ICONV_CAST macro is defined in config.h.
					size_t count = iconv( converter,
																ICONV_CAST( & inbuf ), & inbytesleft,
																& outbuf, & outbytesleft );
					if( count == (size_t)(-1) )
						{
							if( errno == EILSEQ )
								{
									throw Exceptions::MiscellaneousRequirement( "It is suspected that string data is not proper UFT-8, since conversion to UCS-4 failed." );
								}
							else if( errno == EINVAL )
								{
									throw Exceptions::MiscellaneousRequirement( "It is suspected that showed text ended with an incomplete multibyte character." );
								}
							else if( errno == E2BIG )
								{
									throw Exceptions::InternalError( "The buffer allocated for UTF-8 to UCS-4 conversion was too small." );
								}
							else
								{
									std::ostringstream msg;
									msg << "iconv failed with an unrecognized error code: " << errno ;
									throw Exceptions::InternalError( strrefdup( msg ) );
								}
						}
					for( const char * src = buf; src != outbuf; src += 4 )
						{
							Kernel::UnicodeCodePoint code;
							code.decode_UCS4( src );
							if( code == Kernel::UnicodeCodePoint::SPACE ) /* test for space */
								{
									// Observe textState_->wordSpacing_
									const FontMetrics::CharacterMetrics * charMetrics = horizontalMetrics->charByCode( code, & tmpCharMetric );
									xpos += xSize * charMetrics->horizontalCharWidthX_;
									xpos += characterTrackKern;
									xpos += wordTrackKern;
								}
							else /* there should not be any newlines in the string, so we expect this to be a non-white character */
								{
									// Observe textState_->characterSpacing_
									const FontMetrics::CharacterMetrics * charMetrics = horizontalMetrics->charByCode( code, & tmpCharMetric );
									if( Computation::fontMetricGuessIsError && charMetrics == defaultCharMetrics )
										{
											std::ostringstream msg;
											msg << "Character at offset " << src - buf << " in \"" << buf << "\" was not found in font metrics (and according to your options, guessing is not OK)." ;
											throw Exceptions::FontMetricsError( textState_->font_->fontName( ), strrefdup( msg ) );
										}
									revlist.push_back( RefCountPtr< const Lang::Value >
																		 ( new Lang::ConsPair
																			 ( Helpers::newValHandle( new Lang::Length( xpos ) ),
																				 Helpers::newValHandle( new Lang::KernedText( textState_, metaState_, oneUCS4ToUTF8( src ) ) ) ) ) );
									/* Although not as efficient, it becomes easier to use the list if the distance is
									 * measured from the start of the text rather than from the previous character.
									 */
									//										xpos = 0;
									xpos += xSize * charMetrics->horizontalCharWidthX_;
									xpos += characterTrackKern;
								}
						}
				}
			else
				{
					if( ki == kernings_.end( ) )
						{
							throw Exceptions::InternalError( "Short of kerning values in KernedText::measure." );
						}
					xpos -= xSize * *ki;
					++ki;
				}
		}
	if( ki != kernings_.end( ) )
		{
			throw Exceptions::InternalError( "Too many kerning values in KernedText::writePDFVectorTo." );
		}

	RefCountPtr< const Lang::SingleList > res = Lang::THE_CONS_NULL;
	while( ! revlist.empty( ) )
		{
			res = RefCountPtr< const Lang::SingleList >( new Lang::SingleListPair( Kernel::VariableHandle( new Kernel::Variable( revlist.back( ) ) ),
																																						 res ) );
			revlist.pop_back( );
		}
	return res;
}

RefCountPtr< const Lang::String >
Lang::KernedText::oneUCS4ToUTF8( const char * c )
{
	iconv_t converter = Helpers::requireUCS4ToUTF8Converter( );

	const size_t BUF_SIZE = 9;
	char buf[ BUF_SIZE ];

	char charbuf[ 5 ];
	charbuf[0] = *c;
	++c;
	charbuf[1] = *c;
	++c;
	charbuf[2] = *c;
	++c;
	charbuf[3] = *c;
	charbuf[4] = '\0';

	char * outbuf = buf;
	const char * inbuf = charbuf;
	size_t inbytesleft = 4;
	size_t outbytesleft = BUF_SIZE - 1;
	// The ICONV_CAST macro is defined in config.h.
	size_t count = iconv( converter,
												ICONV_CAST( & inbuf ), & inbytesleft,
												& outbuf, & outbytesleft );
	if( count == (size_t)(-1) )
		{
			throw Exceptions::ExternalError( "Conversion of one UCS-4 character to UTF-8 failed." );
		}
	*outbuf = '\0';
	return RefCountPtr< const Lang::String >( new Lang::String( strrefdup( buf ) ) );
}

void
Lang::KernedText::pushString( const RefCountPtr< const Lang::String > & str )
{
	strings_.push_back( str );
	maxLength_ = std::max( maxLength_, strlen( str->val_.getPtr( ) ) );
}

void
Lang::KernedText::pushKerning( double kerning )
{
	strings_.push_back( NullPtr< const Lang::String >( ) );
	kernings_.push_back( kerning );
}

void
Lang::KernedText::show( std::ostream & os ) const
{
	typedef typeof strings_ ListType;
	std::list< double >::const_iterator ki = kernings_.begin( );
	for( ListType::const_iterator i = strings_.begin( ); i != strings_.end( ); ++i )
		{
			if( *i != NullPtr< const Lang::String >( ) )
				{
					os << "(" << (*i)->val_.getPtr( ) << ")" ;
				}
			else
				{
					if( ki == kernings_.end( ) )
						{
							throw Exceptions::InternalError( "Short of kerning values in KernedText::show." );
						}
					os << *ki ;
					++ki;
				}
		}
	if( ki != kernings_.end( ) )
		{
			throw Exceptions::InternalError( "Too many kerning values in KernedText::show." );
		}
}


void
Lang::KernedText::shipout( std::ostream & os, Kernel::PageContentStates * pdfState, const Lang::Transform2D & tf, bool clip ) const
{
	pdfState->text_.synchAssertKnockout( os, textState_.getPtr( ), pdfState->resources_.getPtr( ), clip );
	switch( textState_->mode_ )
		{
		case Lang::TextRenderingMode::FILL:
		case Lang::TextRenderingMode::FILLCLIP:
			pdfState->graphics_.synchForNonStroke( os, metaState_.getPtr( ), pdfState->resources_.getPtr( ) );
			break;
		case Lang::TextRenderingMode::STROKE:
		case Lang::TextRenderingMode::STROKECLIP:
			pdfState->graphics_.synchForStroke( os, metaState_.getPtr( ), pdfState->resources_.getPtr( ) );
			break;
		case Lang::TextRenderingMode::FILLSTROKE:
		case Lang::TextRenderingMode::FILLSTROKECLIP:
			pdfState->graphics_.synchForStroke( os, metaState_.getPtr( ), pdfState->resources_.getPtr( ) );
			pdfState->graphics_.synchForNonStroke( os, metaState_.getPtr( ), pdfState->resources_.getPtr( ) );
			break;
		case Lang::TextRenderingMode::INVISIBLE:
		case Lang::TextRenderingMode::CLIP:
			break;
		case Lang::TextRenderingMode::UNDEFINED:
			pdfState->graphics_.synchForStroke( os, metaState_.getPtr( ), pdfState->resources_.getPtr( ) );
			pdfState->graphics_.synchForNonStroke( os, metaState_.getPtr( ), pdfState->resources_.getPtr( ) );
		default:
			throw Exceptions::InternalError( "KernedText::writePDFVectorTo:	Text rendering mode out of range." );
		}


	size_t bufSize = 4 * maxLength_;		 // Even with UTF16BE encoding, no character should use more than 4 bytes.
																			 // The extra one byte will be used to terminate the string.
	char * buf = new char[ bufSize ];
	DeleteOnExit< char > bufDeleter( buf );

	typedef typeof strings_ ListType;
	std::list< double >::const_iterator ki = kernings_.begin( );
	os << "[ " ;
	for( ListType::const_iterator i = strings_.begin( ); i != strings_.end( ); ++i )
		{
			if( *i != NullPtr< const Lang::String >( ) )
				{
					os << "(" ;
					const char * line = (*i)->val_.getPtr( );
					while( true )
						{
							const char * nextLine = strchr( line, '\n' ); /* Search for line breaks before we convert from UTF8. */
							const char * inbuf = line;
							char * outbuf = buf;
							size_t inbytesleft = ( nextLine != 0 ) ? ( nextLine - line ) : strlen( line );
							size_t outbytesleft = bufSize;
							textState_->font_->encode( & inbuf, & inbytesleft,
																				 & outbuf, & outbytesleft );
							const char * end = buf + ( bufSize - outbytesleft );
							for( const char * src = buf; src != end; ++src )
								{
									switch( *src )
										{
										case '(':
											os << "\\(" ;
											break;
										case ')':
											os << "\\)" ;
											break;
										case '\\':
											os << "\\\\" ;
											break;
										default:
											os << *src ;
										}
								}
							if( nextLine == 0 )
								{
									break;
								}
							os << ")] TJ T* [(" ;
							line = nextLine;
							++line; /* Skip the \n character */
						}
					os << ") " ;
				}
			else
				{
					if( ki == kernings_.end( ) )
						{
							throw Exceptions::InternalError( "Short of kerning values in KernedText::writePDFVectorTo." );
						}
					os << 1000 * *ki << " " ;
					++ki;
				}
		}
	if( ki != kernings_.end( ) )
		{
			throw Exceptions::InternalError( "Too many kerning values in KernedText::writePDFVectorTo." );
		}

	os << "] TJ " << std::endl ;
}

void
Lang::KernedText::shipout_outlined( std::ostream & os, Kernel::PageContentStates * pdfState, const Lang::Transform2D & tf, Lang::Transform2D * textMatrix, Lang::Transform2D * lineMatrix, const SimplePDF::PDF_Name * clipContent ) const
{
#ifndef HAVE_FT2
	throw Exceptions::BuildRequirement( Interaction::BUILD_REQ_FREETYPE, "(outlined fonts)", Ast::THE_UNKNOWN_LOCATION );
#else
	iconv_t converter = Helpers::requireUTF8ToUCS4Converter( );

	RefCountPtr< const Lang::FreeTypeFont > font = textState_->font_.down_cast< const Lang::FreeTypeFont >( );
	if( font == NullPtr< typeof *font >( ) )
		{
			throw Exceptions::MiscellaneousRequirement( "Text masking is only supported for fonts accessed through FreeType." );
		}

	RefCountPtr< FontMetrics::WritingDirectionMetrics > horizontalMetrics = font->metrics( )->horizontalMetrics_;
	if( horizontalMetrics == NullPtr< FontMetrics::WritingDirectionMetrics >( ) )
		{
			throw Exceptions::FontMetricsError( font->fontName( ), strrefdup( "No horizontal metrics defined." ) );
		}

	Concrete::Length ySize = textState_->size_;
	Concrete::Length xSize = ySize * textState_->horizontalScaling_;
	Concrete::Length characterTrackKern = textState_->horizontalScaling_ * textState_->characterSpacingConcrete( );
	Concrete::Length wordTrackKern = textState_->horizontalScaling_ * textState_->wordSpacingConcrete( );

	size_t bufSize = 4 * maxLength_;
	char * buf = new char[ bufSize ];
	DeleteOnExit< char > bufDeleter( buf );

	Concrete::Length rise = textState_->riseConcrete( );
	FontMetrics::CharacterMetrics tmpCharMetric( 0 );

	if( textState_->mode_ == Lang::TextRenderingMode::STROKE ||
			textState_->mode_ == Lang::TextRenderingMode::FILLSTROKE )
		{
			pdfState->graphics_.synchForStroke( os, metaState_.getPtr( ), pdfState->resources_.getPtr( ) );
		}
	if( textState_->mode_ == Lang::TextRenderingMode::FILL ||
			textState_->mode_ == Lang::TextRenderingMode::FILLSTROKE )
		{
			pdfState->graphics_.synchForNonStroke( os, metaState_.getPtr( ), pdfState->resources_.getPtr( ) );
		}

	typedef typeof strings_ ListType;
	std::list< double >::const_iterator ki = kernings_.begin( );
	for( ListType::const_iterator i = strings_.begin( ); i != strings_.end( ); ++i )
		{
			if( *i != NullPtr< const Lang::String >( ) )
				{
					const char * inbuf = (*i)->val_.getPtr( );
					char * outbuf = buf;
					size_t inbytesleft = strlen( inbuf );
					size_t outbytesleft = bufSize;
					// The ICONV_CAST macro is defined in config.h.
					size_t count = iconv( converter,
																ICONV_CAST( & inbuf ), & inbytesleft,
																& outbuf, & outbytesleft );
					if( count == (size_t)(-1) )
						{
							if( errno == EILSEQ )
								{
									throw Exceptions::MiscellaneousRequirement( "Not proper UFT-8?  Failed to convert to UCS-4 code points." );
								}
							else if( errno == EINVAL )
								{
									throw Exceptions::MiscellaneousRequirement( "It is suspected that showed text ended with an incomplete multibyte character." );
								}
							else if( errno == E2BIG )
								{
									throw Exceptions::InternalError( "The buffer allocated for UTF-8 to UCS-4 conversion was too small." );
								}
							else
								{
									std::ostringstream msg;
									msg << "iconv failed with an unrecognized error code: " << errno ;
									throw Exceptions::InternalError( strrefdup( msg ) );
								}
						}
					for( const char * src = buf; src != outbuf; src += 4 )
						{
							Kernel::UnicodeCodePoint code;
							code.decode_UCS4( src );
							if( code == Kernel::UnicodeCodePoint::SPACE )
								{
									// Observe textState_->wordSpacing_
									// In addition, it seems reasonable to not let the space character affect the bounding box.
									const FontMetrics::CharacterMetrics * charMetrics = horizontalMetrics->charByCode( code, & tmpCharMetric );
									textMatrix->prependXShift( xSize * charMetrics->horizontalCharWidthX_ + characterTrackKern + wordTrackKern );
								}
							else if( code == Kernel::UnicodeCodePoint::NEWLINE )
								{
									lineMatrix->prependShift( Concrete::Coords2D( 0, - textState_->leadingConcrete( ) ) );
									textMatrix->replaceBy( *lineMatrix );
								}
							else
								{
									// Observe textState_->characterSpacing_
									bool isOdd;
									Concrete::Length advance;
									Lang::Transform2D tmpTf( tf, *textMatrix );
									tmpTf.prependYShift( rise );
									tmpTf.prependXScale( textState_->horizontalScaling_ );
									if( clipContent == 0 )
										{
											/* The graphics state was synched above. */
											font->shipout_glyph( code, ySize, tmpTf, os, & isOdd, & advance );
											switch( textState_->mode_ )
												{
												case Lang::TextRenderingMode::FILL:
													os << ( isOdd ? "f*" : "f" ) << std::endl ;
													break;
												case Lang::TextRenderingMode::STROKE:
													os << "s" << std::endl ;
													break;
												case Lang::TextRenderingMode::FILLSTROKE:
													os << ( isOdd ? "B*" : "B" ) << std::endl ;
													break;
												case Lang::TextRenderingMode::INVISIBLE:
													os << "n" << std::endl ;
													break;
												case Lang::TextRenderingMode::FILLCLIP:
												case Lang::TextRenderingMode::STROKECLIP:
												case Lang::TextRenderingMode::FILLSTROKECLIP:
												case Lang::TextRenderingMode::CLIP:
													throw Exceptions::InternalError( "The clipping text rendering modes are not supposed to be used." );
												case Lang::TextRenderingMode::UNDEFINED:
													throw Exceptions::InternalError( "Undefined text rendering mode during shipout of outlined text." );
												}
										}
									else
										{
											Kernel::Auto_qQ auto_qQ( & pdfState->graphics_, & pdfState->text_, os );
											font->shipout_glyph( code, ySize, tmpTf, os, & isOdd, & advance );
											os << ( isOdd ? "W* " : "W " ) ;
											switch( textState_->mode_ )
												{
												case Lang::TextRenderingMode::FILL:
													os << ( isOdd ? "f*" : "f" ) ;
													break;
												case Lang::TextRenderingMode::STROKE:
													os << "s" ;
													break;
												case Lang::TextRenderingMode::FILLSTROKE:
													os << ( isOdd ? "B*" : "B" ) ;
													break;
												case Lang::TextRenderingMode::INVISIBLE:
													os << "n" ;
													break;
												case Lang::TextRenderingMode::FILLCLIP:
												case Lang::TextRenderingMode::STROKECLIP:
												case Lang::TextRenderingMode::FILLSTROKECLIP:
												case Lang::TextRenderingMode::CLIP:
													throw Exceptions::InternalError( "The clipping text rendering modes are not supposed to be used." );
												case Lang::TextRenderingMode::UNDEFINED:
													throw Exceptions::InternalError( "Undefined text rendering mode during shipout of outlined text." );
												}
											os << " " << *clipContent << " Do" << std::endl ;
										}

									textMatrix->prependXShift( advance * textState_->horizontalScaling_ + characterTrackKern );
								}
						}
				}
			else
				{
					if( ki == kernings_.end( ) )
						{
							throw Exceptions::InternalError( "Short of kerning values in KernedText::shipout_outlined." );
						}
					textMatrix->prependXShift( - xSize * *ki );
					++ki;
				}
		}
	if( ki != kernings_.end( ) )
		{
			throw Exceptions::InternalError( "Too many kerning values in KernedText::shipout_outlined." );
		}
#endif /* End of #ifndef HAVE_FT2 / #else. */
}

void
Lang::KernedText::measure( Lang::Transform2D * textMatrix, Lang::Transform2D * textLineMatrix, Concrete::Length * xmin, Concrete::Length * ymin, Concrete::Length * xmax, Concrete::Length * ymax ) const
{
	iconv_t converter = Helpers::requireUTF8ToUCS4Converter( );

	RefCountPtr< FontMetrics::WritingDirectionMetrics > horizontalMetrics = textState_->font_->metrics( )->horizontalMetrics_;
	if( horizontalMetrics == NullPtr< FontMetrics::WritingDirectionMetrics >( ) )
		{
			throw Exceptions::FontMetricsError( textState_->font_->fontName( ), strrefdup( "No horizontal metrics defined." ) );
		}
	const FontMetrics::CharacterMetrics * defaultCharMetrics = horizontalMetrics->default_char( );

	Concrete::Length ySize = textState_->size_;
	Concrete::Length xSize = ySize * textState_->horizontalScaling_;
	Concrete::Length characterTrackKern = textState_->horizontalScaling_ * textState_->characterSpacingConcrete( );
	Concrete::Length wordTrackKern = textState_->horizontalScaling_ * textState_->wordSpacingConcrete( );

	size_t bufSize = 4 * maxLength_;
	char * buf = new char[ bufSize ];
	DeleteOnExit< char > bufDeleter( buf );

	FontMetrics::CharacterMetrics tmpCharMetric( 0 );

	if( textLineMatrix->isTranslation( ) )
		{
			Concrete::Length x0 = textLineMatrix->xt_;
			Concrete::Length y0 = textLineMatrix->yt_ + textState_->riseConcrete( );
			Concrete::Length xpos = 0;

			typedef typeof strings_ ListType;
			std::list< double >::const_iterator ki = kernings_.begin( );
			for( ListType::const_iterator i = strings_.begin( ); i != strings_.end( ); ++i )
				{
					if( *i != NullPtr< const Lang::String >( ) )
						{
							const char * inbuf = (*i)->val_.getPtr( );
							char * outbuf = buf;
							size_t inbytesleft = strlen( inbuf );
							size_t outbytesleft = bufSize;
							// The ICONV_CAST macro is defined in config.h.
							size_t count = iconv( converter,
																		ICONV_CAST( & inbuf ), & inbytesleft,
																		& outbuf, & outbytesleft );
							if( count == (size_t)(-1) )
								{
									if( errno == EILSEQ )
										{
											throw Exceptions::MiscellaneousRequirement( "Not proper UFT-8?  Failed to convert to UCS-4 code points." );
										}
									else if( errno == EINVAL )
										{
											throw Exceptions::MiscellaneousRequirement( "It is suspected that showed text ended with an incomplete multibyte character." );
										}
									else if( errno == E2BIG )
										{
											throw Exceptions::InternalError( "The buffer allocated for UTF-8 to UCS-4 conversion was too small." );
										}
									else
										{
											std::ostringstream msg;
											msg << "iconv failed with an unrecognized error code: " << errno ;
											throw Exceptions::InternalError( strrefdup( msg ) );
										}
								}
							for( const char * src = buf; src != outbuf; src += 4 )
								{
									Kernel::UnicodeCodePoint code;
									code.decode_UCS4( src );
									if( code == Kernel::UnicodeCodePoint::SPACE )
										{
											// Observe textState_->wordSpacing_
											// In addition, it seems reasonable to not let the space character affect the bounding box.
											const FontMetrics::CharacterMetrics * charMetrics = horizontalMetrics->charByCode( code, & tmpCharMetric );
											xpos += xSize * charMetrics->horizontalCharWidthX_;
											xpos += characterTrackKern;
											xpos += wordTrackKern;
										}
									else if( code == Kernel::UnicodeCodePoint::NEWLINE )
										{
											textMatrix->prependShift( Concrete::Coords2D( 0, - textState_->leadingConcrete( ) ) );
											textLineMatrix->replaceBy( *textMatrix );
											x0 = textLineMatrix->xt_;
											y0 = textLineMatrix->yt_ + textState_->riseConcrete( );
											xpos = 0;
										}
									else
										{
											// Observe textState_->characterSpacing_
											const FontMetrics::CharacterMetrics * charMetrics = horizontalMetrics->charByCode( code, & tmpCharMetric );
											if( Computation::fontMetricGuessIsError && charMetrics == defaultCharMetrics )
												{
													std::ostringstream msg;
													msg << "Character at offset " << src - buf << " in \"" << buf << "\" was not found in font metrics (and according to your options, guessing is not OK)." ;
													throw Exceptions::FontMetricsError( textState_->font_->fontName( ), strrefdup( msg ) );
												}
											*xmin = std::min( *xmin, x0 + xpos + xSize * charMetrics->xmin_ );
											*ymin = std::min( *ymin, y0 + ySize * charMetrics->ymin_ );
											*xmax = std::max( *xmax, x0 + xpos + xSize * charMetrics->xmax_ );
											*ymax = std::max( *ymax, y0 + ySize * charMetrics->ymax_ );
											xpos += xSize * charMetrics->horizontalCharWidthX_;
											xpos += characterTrackKern;
										}
								}
						}
					else
						{
							if( ki == kernings_.end( ) )
								{
									throw Exceptions::InternalError( "Short of kerning values in KernedText::measure." );
								}
							xpos -= xSize * *ki;
							++ki;
						}
				}
			if( ki != kernings_.end( ) )
				{
					throw Exceptions::InternalError( "Too many kerning values in KernedText::writePDFVectorTo." );
				}

			textLineMatrix->prependXShift( xpos );
		}
	else
		{
			Concrete::Length rise = textState_->riseConcrete( );

			typedef typeof strings_ ListType;
			std::list< double >::const_iterator ki = kernings_.begin( );
			for( ListType::const_iterator i = strings_.begin( ); i != strings_.end( ); ++i )
				{
					if( *i != NullPtr< const Lang::String >( ) )
						{
							const char * inbuf = (*i)->val_.getPtr( );
							char * outbuf = buf;
							size_t inbytesleft = strlen( inbuf );
							size_t outbytesleft = bufSize;
							// The ICONV_CAST macro is defined in config.h.
							size_t count = iconv( converter,
																		ICONV_CAST( & inbuf ), & inbytesleft,
																		& outbuf, & outbytesleft );
							if( count == (size_t)(-1) )
								{
									if( errno == EILSEQ )
										{
											throw Exceptions::MiscellaneousRequirement( "Not proper UFT-8?  Failed to convert to UCS-4 code points." );
										}
									else if( errno == EINVAL )
										{
											throw Exceptions::MiscellaneousRequirement( "It is suspected that showed text ended with an incomplete multibyte character." );
										}
									else if( errno == E2BIG )
										{
											throw Exceptions::InternalError( "The buffer allocated for UTF-8 to UCS-4 conversion was too small." );
										}
									else
										{
											std::ostringstream msg;
											msg << "iconv failed with an unrecognized error code: " << errno ;
											throw Exceptions::InternalError( strrefdup( msg ) );
										}
								}
							for( const char * src = buf; src != outbuf; src += 4 )
								{
									Kernel::UnicodeCodePoint code;
									code.decode_UCS4( src );
									if( code == Kernel::UnicodeCodePoint::SPACE )
										{
											// Observe textState_->wordSpacing_
											// In addition, it seems reasonable to not let the space character affect the bounding box.
											const FontMetrics::CharacterMetrics * charMetrics = horizontalMetrics->charByCode( code, & tmpCharMetric );
											textLineMatrix->prependXShift( xSize * charMetrics->horizontalCharWidthX_ + characterTrackKern + wordTrackKern );
										}
									else if( code == Kernel::UnicodeCodePoint::NEWLINE )
										{
											textMatrix->prependShift( Concrete::Coords2D( 0, - textState_->leadingConcrete( ) ) );
											textLineMatrix->replaceBy( *textMatrix );
										}
									else
										{
											// Observe textState_->characterSpacing_
											const FontMetrics::CharacterMetrics * charMetrics = horizontalMetrics->charByCode( code, & tmpCharMetric );
											if( Computation::fontMetricGuessIsError && charMetrics == defaultCharMetrics )
												{
													std::ostringstream msg;
													msg << "Character at offset " << src - buf << " in \"" << buf << "\" was not found in the font metrics (and according to your options, guessing is not OK)." ;
													throw Exceptions::FontMetricsError( textState_->font_->fontName( ), strrefdup( msg ) );
												}

											Concrete::Coords2D x0y0 = Concrete::Coords2D( xSize * charMetrics->xmin_, ySize * charMetrics->ymin_ + rise ).transformed( *textLineMatrix );
											Concrete::Coords2D x0y1 = Concrete::Coords2D( xSize * charMetrics->xmin_, ySize * charMetrics->ymax_ + rise ).transformed( *textLineMatrix );
											Concrete::Coords2D x1y0 = Concrete::Coords2D( xSize * charMetrics->xmax_, ySize * charMetrics->ymin_ + rise ).transformed( *textLineMatrix );
											Concrete::Coords2D x1y1 = Concrete::Coords2D( xSize * charMetrics->xmax_, ySize * charMetrics->ymax_ + rise ).transformed( *textLineMatrix );

											*xmin = std::min( *xmin, x0y0.x_ );
											*ymin = std::min( *ymin, x0y0.y_ );
											*xmax = std::max( *xmax, x0y0.x_ );
											*ymax = std::max( *ymax, x0y0.y_ );

											*xmin = std::min( *xmin, x0y1.x_ );
											*ymin = std::min( *ymin, x0y1.y_ );
											*xmax = std::max( *xmax, x0y1.x_ );
											*ymax = std::max( *ymax, x0y1.y_ );

											*xmin = std::min( *xmin, x1y0.x_ );
											*ymin = std::min( *ymin, x1y0.y_ );
											*xmax = std::max( *xmax, x1y0.x_ );
											*ymax = std::max( *ymax, x1y0.y_ );

											*xmin = std::min( *xmin, x1y1.x_ );
											*ymin = std::min( *ymin, x1y1.y_ );
											*xmax = std::max( *xmax, x1y1.x_ );
											*ymax = std::max( *ymax, x1y1.y_ );

											textLineMatrix->prependXShift( xSize * charMetrics->horizontalCharWidthX_ + characterTrackKern );
										}
								}
						}
					else
						{
							if( ki == kernings_.end( ) )
								{
									throw Exceptions::InternalError( "Short of kerning values in KernedText::measure." );
								}
							textLineMatrix->prependXShift( - xSize * *ki );
							++ki;
						}
				}
			if( ki != kernings_.end( ) )
				{
					throw Exceptions::InternalError( "Too many kerning values in KernedText::writePDFVectorTo." );
				}
		}
}

void
Lang::KernedText::push( Lang::KernedText * dst ) const
{
	typedef typeof strings_ ListType;
	std::list< double >::const_iterator ki = kernings_.begin( );
	for( ListType::const_iterator i = strings_.begin( ); i != strings_.end( ); ++i )
		{
			if( *i != NullPtr< const Lang::String >( ) )
				{
					dst->pushString( *i );
				}
			else
				{
					dst->pushKerning( *ki );
					++ki;
				}
		}
}


void
Lang::KernedText::gcMark( Kernel::GCMarkedSet & marked )
{
	typedef typeof strings_ ListType;
	for( ListType::const_iterator i = strings_.begin( ); i != strings_.end( ); ++i )
		{
			if( *i != NullPtr< const Lang::String >( ) )
				{
					const_cast< Lang::String * >( i->getPtr( ) )->gcMark( marked );
				}
		}
}

Lang::TextNewline::TextNewline( const Concrete::Length tx, const Concrete::Length ty )
	: t_( tx, ty )
{ }

Lang::TextNewline::~TextNewline( )
{ }

void
Lang::TextNewline::show( std::ostream & os ) const
{
	os << "Newline with offset in bp: " << t_ ;
}

void
Lang::TextNewline::shipout( std::ostream & os, Kernel::PageContentStates * pdfState, const Lang::Transform2D & tf, bool clip ) const
{
	os << t_ << " Td " ;
}

void
Lang::TextNewline::shipout_outlined( std::ostream & os, Kernel::PageContentStates * pdfState, const Lang::Transform2D & tf, Lang::Transform2D * textMatrix, Lang::Transform2D * lineMatrix, const SimplePDF::PDF_Name * clipContent ) const
{
	lineMatrix->prependShift( t_ );
	textMatrix->replaceBy( *lineMatrix );
}


void
Lang::TextNewline::measure( Lang::Transform2D * textMatrix, Lang::Transform2D * textLineMatrix, Concrete::Length * xmin, Concrete::Length * ymin, Concrete::Length * xmax, Concrete::Length * ymax ) const
{
	textMatrix->prependShift( t_ );
	textLineMatrix->replaceBy( *textMatrix );
}

void
Lang::TextNewline::gcMark( Kernel::GCMarkedSet & marked )
{ }

Lang::TextMoveto::TextMoveto( const RefCountPtr< const Lang::Transform2D > & tf )
	: tf_( tf )
{ }

Lang::TextMoveto::~TextMoveto( )
{ }

void
Lang::TextMoveto::show( std::ostream & os ) const
{
	os << "Moveto command" ;
}

void
Lang::TextMoveto::shipout( std::ostream & os, Kernel::PageContentStates * pdfState, const Lang::Transform2D & tf, bool clip ) const
{
	Lang::Transform2D( tf, *tf_ ).shipout( os );
	os << " Tm" << std::endl ;
}

void
Lang::TextMoveto::shipout_outlined( std::ostream & os, Kernel::PageContentStates * pdfState, const Lang::Transform2D & tf, Lang::Transform2D * textMatrix, Lang::Transform2D * lineMatrix, const SimplePDF::PDF_Name * clipContent ) const
{
	lineMatrix->replaceBy( Lang::Transform2D( tf, *tf_ ) );
	textMatrix->replaceBy( *lineMatrix );
}

void
Lang::TextMoveto::measure( Lang::Transform2D * textMatrix, Lang::Transform2D * textLineMatrix, Concrete::Length * xmin, Concrete::Length * ymin, Concrete::Length * xmax, Concrete::Length * ymax ) const
{
	textMatrix->replaceBy( *tf_ );
	textLineMatrix->replaceBy( *textMatrix );
}

void
Lang::TextMoveto::gcMark( Kernel::GCMarkedSet & marked )
{
	const_cast< Lang::Transform2D * >( tf_.getPtr( ) )->gcMark( marked );
}



typedef enum { FILL = 0, STROKE, FILLSTROKE, INVISIBLE, FILLCLIP, STROKECLIP, FILLSTROKECLIP, CLIP, UNDEFINED } ValueType;
			ValueType mode_;

Lang::TextRenderingMode::TextRenderingMode( const ValueType & mode )
	: mode_( mode )
{ }

Lang::TextRenderingMode::TextRenderingMode( bool fill, bool stroke, bool clip )
{
	switch( ( fill	 ? 0x01 : 0x00 ) +
					( stroke ? 0x02 : 0x00 ) +
					( clip	 ? 0x04 : 0x00 ) )
		{
		case 0x00:
			mode_ = INVISIBLE;
			break;
		case 0x01:
			mode_ = FILL;
			break;
		case 0x02:
			mode_ = STROKE;
			break;
		case 0x03:
			mode_ = FILLSTROKE;
			break;
		case 0x04:
			mode_ = CLIP;
			break;
		case 0x05:
			mode_ = FILLCLIP;
			break;
		case 0x06:
			mode_ = STROKECLIP;
			break;
		case 0x07:
			mode_ = FILLSTROKECLIP;
			break;
		default:
			throw Exceptions::InternalError( "Semi-static switch out of range in TextRenderingMode::TextRenderingMode." );
		}
}

Lang::TextRenderingMode::ValueType
Lang::TextRenderingMode::clipMode( ValueType mode )
{
	switch( mode )
		{
		case INVISIBLE:
			return CLIP;
		case FILL:
			return FILLCLIP;
		case STROKE:
			return STROKECLIP;
		case FILLSTROKE:
			return FILLSTROKECLIP;
		default:
			return UNDEFINED;
		}
}

Lang::TextRenderingMode::~TextRenderingMode( )
{ }

RefCountPtr< const Lang::Class > Lang::TextRenderingMode::TypeID( new Lang::SystemFinalClass( strrefdup( "TextRenderingMode" ) ) );
TYPEINFOIMPL( TextRenderingMode );


Lang::CharacterSpacingBinding::CharacterSpacingBinding( const Ast::PlacedIdentifier * id, const Ast::DynamicBindingExpression * bindingExpr, const Concrete::Length spacing )
	: bindingExpr_( bindingExpr ), spacing_( spacing ), isRelative_( false ), id_( id )
{ }

Lang::CharacterSpacingBinding::CharacterSpacingBinding( const Ast::PlacedIdentifier * id, const Ast::DynamicBindingExpression * bindingExpr, const double r )
	: bindingExpr_( bindingExpr ), spacing_( r ), isRelative_( true ), id_( id )
{ }

Lang::CharacterSpacingBinding::~CharacterSpacingBinding( )
{ }

void
Lang::CharacterSpacingBinding::bind( MapType & bindings, Kernel::SystemDynamicVariables ** sysBindings ) const
{
	if( *sysBindings == 0 )
		{
			*sysBindings = new Kernel::SystemDynamicVariables( );
			Kernel::TextState * newState = new Kernel::TextState( );
			if( isRelative_ )
				{
					newState->setCharacterSpacing( Concrete::Length::offtype( spacing_ ) );
				}
			else
				{
					newState->setCharacterSpacing( spacing_ );
				}
			(*sysBindings)->textState_ = RefCountPtr< const Kernel::TextState >( newState );
			return;
		}

	if( (*sysBindings)->textState_ == NullPtr< const Kernel::TextState >( ) )
		{
			Kernel::TextState * newState = new Kernel::TextState( );
			if( isRelative_ )
				{
					newState->setCharacterSpacing( Concrete::Length::offtype( spacing_ ) );
				}
			else
				{
					newState->setCharacterSpacing( spacing_ );
				}
			(*sysBindings)->textState_ = RefCountPtr< const Kernel::TextState >( newState );
			return;
		}

	Kernel::TextState * newState = new Kernel::TextState( *((*sysBindings)->textState_) );

	if( newState->hasCharacterSpacing( ) )
		{
			throw Exceptions::MultipleDynamicBind( id_, bindingExpr_->idLoc( ), Ast::THE_UNKNOWN_LOCATION );
		}

	if( isRelative_ )
		{
			newState->setCharacterSpacing( Concrete::Length::offtype( spacing_ ) );
		}
	else
		{
			newState->setCharacterSpacing( spacing_ );
		}
	(*sysBindings)->textState_ = RefCountPtr< const Kernel::TextState >( newState );
}

void
Lang::CharacterSpacingBinding::show( std::ostream & os ) const
{
	os << Interaction::DYNAMIC_VARIABLE_PREFIX << id_ << ":" ;
	if( isRelative_ )
		{
			os << Concrete::Length::offtype( spacing_ ) ;
		}
	else
		{
			os << spacing_ / Interaction::displayUnit << Interaction::displayUnitName ;
		}
}

void
Lang::CharacterSpacingBinding::gcMark( Kernel::GCMarkedSet & marked )
{ }


Lang::WordSpacingBinding::WordSpacingBinding( const Ast::PlacedIdentifier * id, const Ast::DynamicBindingExpression * bindingExpr, const Concrete::Length spacing )
	: bindingExpr_( bindingExpr ), spacing_( spacing ), isRelative_( false ), id_( id )
{ }

Lang::WordSpacingBinding::WordSpacingBinding( const Ast::PlacedIdentifier * id, const Ast::DynamicBindingExpression * bindingExpr, const double r )
	: bindingExpr_( bindingExpr ), spacing_( r ), isRelative_( true ), id_( id )
{ }

Lang::WordSpacingBinding::~WordSpacingBinding( )
{ }

void
Lang::WordSpacingBinding::bind( MapType & bindings, Kernel::SystemDynamicVariables ** sysBindings ) const
{
	if( *sysBindings == 0 )
		{
			*sysBindings = new Kernel::SystemDynamicVariables( );
			Kernel::TextState * newState = new Kernel::TextState( );
			if( isRelative_ )
				{
					newState->setWordSpacing( Concrete::Length::offtype( spacing_ ) );
				}
			else
				{
					newState->setWordSpacing( spacing_ );
				}
			(*sysBindings)->textState_ = RefCountPtr< const Kernel::TextState >( newState );
			return;
		}

	if( (*sysBindings)->textState_ == NullPtr< const Kernel::TextState >( ) )
		{
			Kernel::TextState * newState = new Kernel::TextState( );
			if( isRelative_ )
				{
					newState->setWordSpacing( Concrete::Length::offtype( spacing_ ) );
				}
			else
				{
					newState->setWordSpacing( spacing_ );
				}
			(*sysBindings)->textState_ = RefCountPtr< const Kernel::TextState >( newState );
			return;
		}

	Kernel::TextState * newState = new Kernel::TextState( *((*sysBindings)->textState_) );

	if( newState->hasWordSpacing( ) )
		{
			throw Exceptions::MultipleDynamicBind( id_, bindingExpr_->idLoc( ), Ast::THE_UNKNOWN_LOCATION );
		}

	if( isRelative_ )
		{
			newState->setWordSpacing( Concrete::Length::offtype( spacing_ ) );
		}
	else
		{
			newState->setWordSpacing( spacing_ );
		}
	(*sysBindings)->textState_ = RefCountPtr< const Kernel::TextState >( newState );
}

void
Lang::WordSpacingBinding::show( std::ostream & os ) const
{
	os << Interaction::DYNAMIC_VARIABLE_PREFIX << id_ << ":" ;
	if( isRelative_ )
		{
			os << Concrete::Length::offtype( spacing_ ) ;
		}
	else
		{
			os << spacing_ / Interaction::displayUnit << Interaction::displayUnitName ;
		}
}

void
Lang::WordSpacingBinding::gcMark( Kernel::GCMarkedSet & marked )
{ }


Lang::HorizontalScalingBinding::HorizontalScalingBinding( const Ast::PlacedIdentifier * id, const Ast::DynamicBindingExpression * bindingExpr, double scaling )
	: bindingExpr_( bindingExpr ), scaling_( scaling ), id_( id )
{ }

Lang::HorizontalScalingBinding::~HorizontalScalingBinding( )
{ }

void
Lang::HorizontalScalingBinding::bind( MapType & bindings, Kernel::SystemDynamicVariables ** sysBindings ) const
{
	if( *sysBindings == 0 )
		{
			*sysBindings = new Kernel::SystemDynamicVariables( );
			Kernel::TextState * newState = new Kernel::TextState( );
			newState->horizontalScaling_ = scaling_;
			(*sysBindings)->textState_ = RefCountPtr< const Kernel::TextState >( newState );
			return;
		}

	if( (*sysBindings)->textState_ == NullPtr< const Kernel::TextState >( ) )
		{
			Kernel::TextState * newState = new Kernel::TextState( );
			newState->horizontalScaling_ = scaling_;
			(*sysBindings)->textState_ = RefCountPtr< const Kernel::TextState >( newState );
			return;
		}

	Kernel::TextState * newState = new Kernel::TextState( *((*sysBindings)->textState_) );

	if( ! IS_NAN( newState->horizontalScaling_ ) )
		{
			throw Exceptions::MultipleDynamicBind( id_, bindingExpr_->idLoc( ), Ast::THE_UNKNOWN_LOCATION );
		}

	newState->horizontalScaling_ = scaling_;
	(*sysBindings)->textState_ = RefCountPtr< const Kernel::TextState >( newState );
}

void
Lang::HorizontalScalingBinding::show( std::ostream & os ) const
{
	os << Interaction::DYNAMIC_VARIABLE_PREFIX << id_ << ":"
		 << scaling_ ;
}

void
Lang::HorizontalScalingBinding::gcMark( Kernel::GCMarkedSet & marked )
{ }


Lang::LeadingBinding::LeadingBinding( const Ast::PlacedIdentifier * id, const Ast::DynamicBindingExpression * bindingExpr, const Concrete::Length ty )
	: bindingExpr_( bindingExpr ), ty_( ty ), isRelative_( false ), id_( id )
{ }

Lang::LeadingBinding::LeadingBinding( const Ast::PlacedIdentifier * id, const Ast::DynamicBindingExpression * bindingExpr, const double r )
	: bindingExpr_( bindingExpr ), ty_( r ), isRelative_( true ), id_( id )
{ }

Lang::LeadingBinding::~LeadingBinding( )
{ }

void
Lang::LeadingBinding::bind( MapType & bindings, Kernel::SystemDynamicVariables ** sysBindings ) const
{
	if( *sysBindings == 0 )
		{
			*sysBindings = new Kernel::SystemDynamicVariables( );
			Kernel::TextState * newState = new Kernel::TextState( );
			if( isRelative_ )
				{
					newState->setLeading( Concrete::Length::offtype( ty_ ) );
				}
			else
				{
					newState->setLeading( ty_ );
				}
			(*sysBindings)->textState_ = RefCountPtr< const Kernel::TextState >( newState );
			return;
		}

	if( (*sysBindings)->textState_ == NullPtr< const Kernel::TextState >( ) )
		{
			Kernel::TextState * newState = new Kernel::TextState( );
			if( isRelative_ )
				{
					newState->setLeading( Concrete::Length::offtype( ty_ ) );
				}
			else
				{
					newState->setLeading( ty_ );
				}
			(*sysBindings)->textState_ = RefCountPtr< const Kernel::TextState >( newState );
			return;
		}

	Kernel::TextState * newState = new Kernel::TextState( *((*sysBindings)->textState_) );

	if( newState->hasLeading( ) )
		{
			throw Exceptions::MultipleDynamicBind( id_, bindingExpr_->idLoc( ), Ast::THE_UNKNOWN_LOCATION );
		}

	if( isRelative_ )
		{
			newState->setLeading( Concrete::Length::offtype( ty_ ) );
		}
	else
		{
			newState->setLeading( ty_ );
		}
	(*sysBindings)->textState_ = RefCountPtr< const Kernel::TextState >( newState );
}

void
Lang::LeadingBinding::show( std::ostream & os ) const
{
	os << Interaction::DYNAMIC_VARIABLE_PREFIX << id_ << ":" ;
	if( isRelative_ )
		{
			os << Concrete::Length::offtype( ty_ ) ;
		}
	else
		{
			os << ty_ / Interaction::displayUnit << Interaction::displayUnitName ;
		}
}

void
Lang::LeadingBinding::gcMark( Kernel::GCMarkedSet & marked )
{ }


Lang::FontBinding::FontBinding( const Ast::PlacedIdentifier * id, const Ast::DynamicBindingExpression * bindingExpr, const RefCountPtr< const Lang::Font > & font )
	: bindingExpr_( bindingExpr ), font_( font ), id_( id )
{ }

Lang::FontBinding::~FontBinding( )
{ }

void
Lang::FontBinding::bind( MapType & bindings, Kernel::SystemDynamicVariables ** sysBindings ) const
{
	if( *sysBindings == 0 )
		{
			*sysBindings = new Kernel::SystemDynamicVariables( );
			Kernel::TextState * newState = new Kernel::TextState( );
			newState->font_ = font_;
			(*sysBindings)->textState_ = RefCountPtr< const Kernel::TextState >( newState );
			return;
		}

	if( (*sysBindings)->textState_ == NullPtr< const Kernel::TextState >( ) )
		{
			Kernel::TextState * newState = new Kernel::TextState( );
			newState->font_ = font_;
			(*sysBindings)->textState_ = RefCountPtr< const Kernel::TextState >( newState );
			return;
		}

	Kernel::TextState * newState = new Kernel::TextState( *((*sysBindings)->textState_) );

	if( newState->font_ != NullPtr< const Lang::Font >( ) )
		{
			throw Exceptions::MultipleDynamicBind( id_, bindingExpr_->idLoc( ), Ast::THE_UNKNOWN_LOCATION );
		}

	newState->font_ = font_;
	(*sysBindings)->textState_ = RefCountPtr< const Kernel::TextState >( newState );
}

void
Lang::FontBinding::show( std::ostream & os ) const
{
	os << Interaction::DYNAMIC_VARIABLE_PREFIX << id_ << ":" ;
	font_->show( os );
}

void
Lang::FontBinding::gcMark( Kernel::GCMarkedSet & marked )
{
	const_cast< Lang::Font * >( font_.getPtr( ) )->gcMark( marked );
}


Lang::TextSizeBinding::TextSizeBinding( const Ast::PlacedIdentifier * id, const Ast::DynamicBindingExpression * bindingExpr, const Concrete::Length size )
	: bindingExpr_( bindingExpr ), size_( size ), id_( id )
{ }

Lang::TextSizeBinding::~TextSizeBinding( )
{ }

void
Lang::TextSizeBinding::bind( MapType & bindings, Kernel::SystemDynamicVariables ** sysBindings ) const
{
	if( *sysBindings == 0 )
		{
			*sysBindings = new Kernel::SystemDynamicVariables( );
			Kernel::TextState * newState = new Kernel::TextState( );
			newState->size_ = size_;
			(*sysBindings)->textState_ = RefCountPtr< const Kernel::TextState >( newState );
			return;
		}

	if( (*sysBindings)->textState_ == NullPtr< const Kernel::TextState >( ) )
		{
			Kernel::TextState * newState = new Kernel::TextState( );
			newState->size_ = size_;
			(*sysBindings)->textState_ = RefCountPtr< const Kernel::TextState >( newState );
			return;
		}

	Kernel::TextState * newState = new Kernel::TextState( *((*sysBindings)->textState_) );

	if( ! IS_NAN( newState->size_ ) )
		{
			throw Exceptions::MultipleDynamicBind( id_, bindingExpr_->idLoc( ), Ast::THE_UNKNOWN_LOCATION );
		}

	newState->size_ = size_;
	(*sysBindings)->textState_ = RefCountPtr< const Kernel::TextState >( newState );
}

void
Lang::TextSizeBinding::show( std::ostream & os ) const
{
	os << Interaction::DYNAMIC_VARIABLE_PREFIX << id_ << ":"
		 << size_ / Interaction::displayUnit << Interaction::displayUnitName ;
}

void
Lang::TextSizeBinding::gcMark( Kernel::GCMarkedSet & marked )
{ }


Lang::TextRenderingModeBinding::TextRenderingModeBinding( const Ast::PlacedIdentifier * id, const Ast::DynamicBindingExpression * bindingExpr, const Lang::TextRenderingMode::ValueType mode )
	: bindingExpr_( bindingExpr ), mode_( mode ), id_( id )
{ }

Lang::TextRenderingModeBinding::~TextRenderingModeBinding( )
{ }

void
Lang::TextRenderingModeBinding::bind( MapType & bindings, Kernel::SystemDynamicVariables ** sysBindings ) const
{
	if( *sysBindings == 0 )
		{
			*sysBindings = new Kernel::SystemDynamicVariables( );
			Kernel::TextState * newState = new Kernel::TextState( );
			newState->mode_ = mode_;
			(*sysBindings)->textState_ = RefCountPtr< const Kernel::TextState >( newState );
			return;
		}

	if( (*sysBindings)->textState_ == NullPtr< const Kernel::TextState >( ) )
		{
			Kernel::TextState * newState = new Kernel::TextState( );
			newState->mode_ = mode_;
			(*sysBindings)->textState_ = RefCountPtr< const Kernel::TextState >( newState );
			return;
		}

	Kernel::TextState * newState = new Kernel::TextState( *((*sysBindings)->textState_) );

	if( newState->mode_ != Lang::TextRenderingMode::UNDEFINED )
		{
			throw Exceptions::MultipleDynamicBind( id_, bindingExpr_->idLoc( ), Ast::THE_UNKNOWN_LOCATION );
		}

	newState->mode_ = mode_;
	(*sysBindings)->textState_ = RefCountPtr< const Kernel::TextState >( newState );
}

void
Lang::TextRenderingModeBinding::show( std::ostream & os ) const
{
	os << Interaction::DYNAMIC_VARIABLE_PREFIX << id_ << ":"
		 << "<internal-enum>" ;
}

void
Lang::TextRenderingModeBinding::gcMark( Kernel::GCMarkedSet & marked )
{ }


Lang::TextRiseBinding::TextRiseBinding( const Ast::PlacedIdentifier * id, const Ast::DynamicBindingExpression * bindingExpr, const Concrete::Length ty )
	: bindingExpr_( bindingExpr ), ty_( ty ), isRelative_( false ), id_( id )
{ }

Lang::TextRiseBinding::TextRiseBinding( const Ast::PlacedIdentifier * id, const Ast::DynamicBindingExpression * bindingExpr, const double r )
	: bindingExpr_( bindingExpr ), ty_( r ), isRelative_( true ), id_( id )
{ }

Lang::TextRiseBinding::~TextRiseBinding( )
{ }

void
Lang::TextRiseBinding::bind( MapType & bindings, Kernel::SystemDynamicVariables ** sysBindings ) const
{
	if( *sysBindings == 0 )
		{
			*sysBindings = new Kernel::SystemDynamicVariables( );
			Kernel::TextState * newState = new Kernel::TextState( );
			if( isRelative_ )
				{
					newState->setRise( Concrete::Length::offtype( ty_ ) );
				}
			else
				{
					newState->setRise( ty_ );
				}
			(*sysBindings)->textState_ = RefCountPtr< const Kernel::TextState >( newState );
			return;
		}

	if( (*sysBindings)->textState_ == NullPtr< const Kernel::TextState >( ) )
		{
			Kernel::TextState * newState = new Kernel::TextState( );
			if( isRelative_ )
				{
					newState->setRise( Concrete::Length::offtype( ty_ ) );
				}
			else
				{
					newState->setRise( ty_ );
				}
			(*sysBindings)->textState_ = RefCountPtr< const Kernel::TextState >( newState );
			return;
		}

	Kernel::TextState * newState = new Kernel::TextState( *((*sysBindings)->textState_) );

	if( newState->hasRise( ) )
		{
			throw Exceptions::MultipleDynamicBind( id_, bindingExpr_->idLoc( ), Ast::THE_UNKNOWN_LOCATION );
		}

	if( isRelative_ )
		{
			newState->setRise( Concrete::Length::offtype( ty_ ) );
		}
	else
		{
			newState->setRise( ty_ );
		}
	(*sysBindings)->textState_ = RefCountPtr< const Kernel::TextState >( newState );
}

void
Lang::TextRiseBinding::show( std::ostream & os ) const
{
	os << Interaction::DYNAMIC_VARIABLE_PREFIX << id_ << ":" ;
	if( isRelative_ )
		{
			os << Concrete::Length::offtype( ty_ ) ;
		}
	else
		{
			os << ty_ / Interaction::displayUnit << Interaction::displayUnitName ;
		}
}

void
Lang::TextRiseBinding::gcMark( Kernel::GCMarkedSet & marked )
{ }


Lang::TextKnockoutBinding::TextKnockoutBinding( const Ast::PlacedIdentifier * id, const Ast::DynamicBindingExpression * bindingExpr, bool knockout )
	: bindingExpr_( bindingExpr ), knockout_( knockout ), id_( id )
{ }

Lang::TextKnockoutBinding::~TextKnockoutBinding( )
{ }

void
Lang::TextKnockoutBinding::bind( MapType & bindings, Kernel::SystemDynamicVariables ** sysBindings ) const
{
	if( *sysBindings == 0 )
		{
			*sysBindings = new Kernel::SystemDynamicVariables( );
			Kernel::TextState * newState = new Kernel::TextState( );
			newState->knockout_ = knockout_;
			(*sysBindings)->textState_ = RefCountPtr< const Kernel::TextState >( newState );
			return;
		}

	if( (*sysBindings)->textState_ == NullPtr< const Kernel::TextState >( ) )
		{
			Kernel::TextState * newState = new Kernel::TextState( );
			newState->knockout_ = knockout_;
			(*sysBindings)->textState_ = RefCountPtr< const Kernel::TextState >( newState );
			return;
		}

	Kernel::TextState * newState = new Kernel::TextState( *((*sysBindings)->textState_) );

	if( ( newState->knockout_ & Kernel::TextState::KNOCKOUT_UNDEFINED_BIT ) != 0 )
		{
			throw Exceptions::MultipleDynamicBind( id_, bindingExpr_->idLoc( ), Ast::THE_UNKNOWN_LOCATION );
		}

	newState->knockout_ = ( knockout_ ? Kernel::TextState::KNOCKOUT_FLAG_BIT : 0 );
	(*sysBindings)->textState_ = RefCountPtr< const Kernel::TextState >( newState );
}

void
Lang::TextKnockoutBinding::show( std::ostream & os ) const
{
	os << Interaction::DYNAMIC_VARIABLE_PREFIX << id_ << ":"
		 << ( knockout_ ? "true" : "false" ) ;
}

void
Lang::TextKnockoutBinding::gcMark( Kernel::GCMarkedSet & marked )
{ }


Kernel::CharacterSpacingDynamicVariableProperties::CharacterSpacingDynamicVariableProperties( const Ast::PlacedIdentifier * id )
	: Kernel::DynamicVariableProperties( id )
{ }

Kernel::CharacterSpacingDynamicVariableProperties::~CharacterSpacingDynamicVariableProperties( )
{ }

Kernel::VariableHandle
Kernel::CharacterSpacingDynamicVariableProperties::fetch( const Kernel::PassedDyn & dyn ) const
{
	RefCountPtr< const Kernel::TextState > textState = dyn->getTextState( );
	return Kernel::VariableHandle( new Kernel::Variable( textState->characterSpacing( ) ) );
}

void
Kernel::CharacterSpacingDynamicVariableProperties::makeBinding( Kernel::VariableHandle val, const Ast::DynamicBindingExpression * bindingExpr, Kernel::EvalState * evalState ) const
{
	RefCountPtr< const Lang::Value > valUntyped = val->getUntyped( );

	Kernel::ContRef cont = evalState->cont_;

	try
		{
			typedef const Lang::Length ArgType;
			RefCountPtr< ArgType > spacing = Helpers::try_cast_CoreArgument< ArgType >( valUntyped );
			cont->takeValue( Kernel::ValueRef( new Lang::CharacterSpacingBinding( id_, bindingExpr, spacing->get( ) ) ),
											 evalState );
			return;
		}
	catch( const NonLocalExit::NotThisType & ball )
		{
			/* Wrong type; never mind!.. but see below!
			 */
		}

	try
		{
			typedef const Lang::Float ArgType;
			RefCountPtr< ArgType > spacing = Helpers::try_cast_CoreArgument< ArgType >( valUntyped );
			cont->takeValue( Kernel::ValueRef( new Lang::CharacterSpacingBinding( id_, bindingExpr, spacing->val_ ) ),
											 evalState );
			return;
		}
	catch( const NonLocalExit::NotThisType & ball )
		{
			/* Wrong type; never mind!.. but see below!
			 */
		}

	throw Exceptions::TypeMismatch( bindingExpr->exprLoc( ), valUntyped->getTypeName( ), Helpers::typeSetString( Lang::Length::staticTypeName( ), Lang::Float::staticTypeName( ) ) );
}


Kernel::WordSpacingDynamicVariableProperties::WordSpacingDynamicVariableProperties( const Ast::PlacedIdentifier * id )
	: Kernel::DynamicVariableProperties( id )
{ }

Kernel::WordSpacingDynamicVariableProperties::~WordSpacingDynamicVariableProperties( )
{ }

Kernel::VariableHandle
Kernel::WordSpacingDynamicVariableProperties::fetch( const Kernel::PassedDyn & dyn ) const
{
	RefCountPtr< const Kernel::TextState > textState = dyn->getTextState( );
	return Kernel::VariableHandle( new Kernel::Variable( textState->wordSpacing( ) ) );
}

void
Kernel::WordSpacingDynamicVariableProperties::makeBinding( Kernel::VariableHandle val, const Ast::DynamicBindingExpression * bindingExpr, Kernel::EvalState * evalState ) const
{
	RefCountPtr< const Lang::Value > valUntyped = val->getUntyped( );

	Kernel::ContRef cont = evalState->cont_;

	try
		{
			typedef const Lang::Length ArgType;
			RefCountPtr< ArgType > spacing = Helpers::try_cast_CoreArgument< ArgType >( valUntyped );
			cont->takeValue( Kernel::ValueRef( new Lang::WordSpacingBinding( id_, bindingExpr, spacing->get( ) ) ),
											 evalState );
			return;
		}
	catch( const NonLocalExit::NotThisType & ball )
		{
			/* Wrong type; never mind!.. but see below!
			 */
		}

	try
		{
			typedef const Lang::Float ArgType;
			RefCountPtr< ArgType > spacing = Helpers::try_cast_CoreArgument< ArgType >( valUntyped );
			cont->takeValue( Kernel::ValueRef( new Lang::WordSpacingBinding( id_, bindingExpr, spacing->val_ ) ),
											 evalState );
			return;
		}
	catch( const NonLocalExit::NotThisType & ball )
		{
			/* Wrong type; never mind!.. but see below!
			 */
		}

	throw Exceptions::TypeMismatch( bindingExpr->exprLoc( ), valUntyped->getTypeName( ), Helpers::typeSetString( Lang::Length::staticTypeName( ), Lang::Float::staticTypeName( ) ) );
}


Kernel::HorizontalScalingDynamicVariableProperties::HorizontalScalingDynamicVariableProperties( const Ast::PlacedIdentifier * id )
	: Kernel::DynamicVariableProperties( id )
{ }

Kernel::HorizontalScalingDynamicVariableProperties::~HorizontalScalingDynamicVariableProperties( )
{ }

Kernel::VariableHandle
Kernel::HorizontalScalingDynamicVariableProperties::fetch( const Kernel::PassedDyn & dyn ) const
{
	RefCountPtr< const Kernel::TextState > textState = dyn->getTextState( );
	return Kernel::VariableHandle( Shapes::Helpers::newValHandle( new Lang::Float( textState->horizontalScaling_ ) ) );
}

void
Kernel::HorizontalScalingDynamicVariableProperties::makeBinding( Kernel::VariableHandle val, const Ast::DynamicBindingExpression * bindingExpr, Kernel::EvalState * evalState ) const
{
	RefCountPtr< const Lang::Float > scaling = val->getVal< const Lang::Float >( bindingExpr->exprLoc( ) );
	Kernel::ContRef cont = evalState->cont_;
	cont->takeValue( Kernel::ValueRef( new Lang::HorizontalScalingBinding( id_, bindingExpr, scaling->val_ ) ),
									 evalState );
}


Kernel::LeadingDynamicVariableProperties::LeadingDynamicVariableProperties( const Ast::PlacedIdentifier * id )
	: Kernel::DynamicVariableProperties( id )
{ }

Kernel::LeadingDynamicVariableProperties::~LeadingDynamicVariableProperties( )
{ }

Kernel::VariableHandle
Kernel::LeadingDynamicVariableProperties::fetch( const Kernel::PassedDyn & dyn ) const
{
	RefCountPtr< const Kernel::TextState > textState = dyn->getTextState( );
	return Kernel::VariableHandle( new Kernel::Variable( textState->leading( ) ) );
}

void
Kernel::LeadingDynamicVariableProperties::makeBinding( Kernel::VariableHandle val, const Ast::DynamicBindingExpression * bindingExpr, Kernel::EvalState * evalState ) const
{
	RefCountPtr< const Lang::Value > valUntyped = val->getUntyped( );

	Kernel::ContRef cont = evalState->cont_;

	try
		{
			typedef const Lang::Length ArgType;
			RefCountPtr< ArgType > ty = Helpers::try_cast_CoreArgument< ArgType >( valUntyped );
			cont->takeValue( Kernel::ValueRef( new Lang::LeadingBinding( id_, bindingExpr, ty->get( ) ) ),
											 evalState );
			return;
		}
	catch( const NonLocalExit::NotThisType & ball )
		{
			/* Wrong type; never mind!.. but see below!
			 */
		}

	try
		{
			typedef const Lang::Float ArgType;
			RefCountPtr< ArgType > ty = Helpers::try_cast_CoreArgument< ArgType >( valUntyped );
			cont->takeValue( Kernel::ValueRef( new Lang::LeadingBinding( id_, bindingExpr, ty->val_ ) ),
											 evalState );
			return;
		}
	catch( const NonLocalExit::NotThisType & ball )
		{
			/* Wrong type; never mind!.. but see below!
			 */
		}

	throw Exceptions::TypeMismatch( bindingExpr->exprLoc( ), valUntyped->getTypeName( ), Helpers::typeSetString( Lang::Length::staticTypeName( ), Lang::Float::staticTypeName( ) ) );
}


Kernel::FontDynamicVariableProperties::FontDynamicVariableProperties( const Ast::PlacedIdentifier * id )
	: Kernel::DynamicVariableProperties( id )
{ }

Kernel::FontDynamicVariableProperties::~FontDynamicVariableProperties( )
{ }

Kernel::VariableHandle
Kernel::FontDynamicVariableProperties::fetch( const Kernel::PassedDyn & dyn ) const
{
	RefCountPtr< const Kernel::TextState > textState = dyn->getTextState( );
	return Kernel::VariableHandle( new Kernel::Variable( textState->font_ ) );
}

void
Kernel::FontDynamicVariableProperties::makeBinding( Kernel::VariableHandle val, const Ast::DynamicBindingExpression * bindingExpr, Kernel::EvalState * evalState ) const
{
	Kernel::ContRef cont = evalState->cont_;
	cont->takeValue( Kernel::ValueRef( new Lang::FontBinding( id_, bindingExpr, val->getVal< const Lang::Font >( bindingExpr->exprLoc( ) ) ) ),
									 evalState );
}


Kernel::TextSizeDynamicVariableProperties::TextSizeDynamicVariableProperties( const Ast::PlacedIdentifier * id )
	: Kernel::DynamicVariableProperties( id )
{ }

Kernel::TextSizeDynamicVariableProperties::~TextSizeDynamicVariableProperties( )
{ }

Kernel::VariableHandle
Kernel::TextSizeDynamicVariableProperties::fetch( const Kernel::PassedDyn & dyn ) const
{
	RefCountPtr< const Kernel::TextState > textState = dyn->getTextState( );
	return Kernel::VariableHandle( Shapes::Helpers::newValHandle( new Lang::Length( textState->size_ ) ) );
}

void
Kernel::TextSizeDynamicVariableProperties::makeBinding( Kernel::VariableHandle val, const Ast::DynamicBindingExpression * bindingExpr, Kernel::EvalState * evalState ) const
{
	RefCountPtr< const Lang::Length > size = val->getVal< const Lang::Length >( bindingExpr->exprLoc( ) );
	Kernel::ContRef cont = evalState->cont_;
	cont->takeValue( Kernel::ValueRef( new Lang::TextSizeBinding( id_, bindingExpr, size->get( ) ) ),
									 evalState );
}


Kernel::TextRenderingModeDynamicVariableProperties::TextRenderingModeDynamicVariableProperties( const Ast::PlacedIdentifier * id )
	: Kernel::DynamicVariableProperties( id )
{ }

Kernel::TextRenderingModeDynamicVariableProperties::~TextRenderingModeDynamicVariableProperties( )
{ }

Kernel::VariableHandle
Kernel::TextRenderingModeDynamicVariableProperties::fetch( const Kernel::PassedDyn & dyn ) const
{
	RefCountPtr< const Kernel::TextState > textState = dyn->getTextState( );
	return Kernel::VariableHandle( Shapes::Helpers::newValHandle( new Lang::TextRenderingMode( textState->mode_ ) ) );
}

void
Kernel::TextRenderingModeDynamicVariableProperties::makeBinding( Kernel::VariableHandle val, const Ast::DynamicBindingExpression * bindingExpr, Kernel::EvalState * evalState ) const
{
	RefCountPtr< const Lang::TextRenderingMode > mode = val->getVal< const Lang::TextRenderingMode >( bindingExpr->exprLoc( ) );
	Kernel::ContRef cont = evalState->cont_;
	cont->takeValue( Kernel::ValueRef( new Lang::TextRenderingModeBinding( id_, bindingExpr, mode->mode_ ) ),
									 evalState );
}


Kernel::TextRiseDynamicVariableProperties::TextRiseDynamicVariableProperties( const Ast::PlacedIdentifier * id )
	: Kernel::DynamicVariableProperties( id )
{ }

Kernel::TextRiseDynamicVariableProperties::~TextRiseDynamicVariableProperties( )
{ }

Kernel::VariableHandle
Kernel::TextRiseDynamicVariableProperties::fetch( const Kernel::PassedDyn & dyn ) const
{
	RefCountPtr< const Kernel::TextState > textState = dyn->getTextState( );
	return Kernel::VariableHandle( new Kernel::Variable( textState->rise( ) ) );
}

void
Kernel::TextRiseDynamicVariableProperties::makeBinding( Kernel::VariableHandle val, const Ast::DynamicBindingExpression * bindingExpr, Kernel::EvalState * evalState ) const
{
	RefCountPtr< const Lang::Value > valUntyped = val->getUntyped( );

	Kernel::ContRef cont = evalState->cont_;

	try
		{
			typedef const Lang::Length ArgType;
			RefCountPtr< ArgType > ty = Helpers::try_cast_CoreArgument< ArgType >( valUntyped );
			cont->takeValue( Kernel::ValueRef( new Lang::TextRiseBinding( id_, bindingExpr, ty->get( ) ) ),
											 evalState );
			return;
		}
	catch( const NonLocalExit::NotThisType & ball )
		{
			/* Wrong type; never mind!.. but see below!
			 */
		}

	try
		{
			typedef const Lang::Float ArgType;
			RefCountPtr< ArgType > ty = Helpers::try_cast_CoreArgument< ArgType >( valUntyped );
			cont->takeValue( Kernel::ValueRef( new Lang::TextRiseBinding( id_, bindingExpr, ty->val_ ) ),
											 evalState );
			return;
		}
	catch( const NonLocalExit::NotThisType & ball )
		{
			/* Wrong type; never mind!.. but see below!
			 */
		}

	throw Exceptions::TypeMismatch( bindingExpr->exprLoc( ), valUntyped->getTypeName( ), Helpers::typeSetString( Lang::Length::staticTypeName( ), Lang::Float::staticTypeName( ) ) );
}


Kernel::TextKnockoutDynamicVariableProperties::TextKnockoutDynamicVariableProperties( const Ast::PlacedIdentifier * id )
	: Kernel::DynamicVariableProperties( id )
{ }

Kernel::TextKnockoutDynamicVariableProperties::~TextKnockoutDynamicVariableProperties( )
{ }

Kernel::VariableHandle
Kernel::TextKnockoutDynamicVariableProperties::fetch( const Kernel::PassedDyn & dyn ) const
{
	RefCountPtr< const Kernel::TextState > textState = dyn->getTextState( );
	return Kernel::VariableHandle( Shapes::Helpers::newValHandle( new Lang::Boolean( ( textState->knockout_ & Kernel::TextState::KNOCKOUT_FLAG_BIT ) != 0 ) ) );
}

void
Kernel::TextKnockoutDynamicVariableProperties::makeBinding( Kernel::VariableHandle val, const Ast::DynamicBindingExpression * bindingExpr, Kernel::EvalState * evalState ) const
{
	RefCountPtr< const Lang::Boolean > mode = val->getVal< const Lang::Boolean >( bindingExpr->exprLoc( ) );
	Kernel::ContRef cont = evalState->cont_;
	cont->takeValue( Kernel::ValueRef( new Lang::TextKnockoutBinding( id_, bindingExpr, mode->val_ ) ),
									 evalState );
}



Kernel::TextState::TextState( )
	: relativeFlags_( 0 ),
		characterSpacing_( Concrete::Length( std::numeric_limits< double >::signaling_NaN( ) ) ),
		wordSpacing_( Concrete::Length( std::numeric_limits< double >::signaling_NaN( ) ) ),
		horizontalScaling_( std::numeric_limits< double >::signaling_NaN( ) ),
		leading_( Concrete::Length( std::numeric_limits< double >::signaling_NaN( ) ) ),
		rise_( Concrete::Length( std::numeric_limits< double >::signaling_NaN( ) ) ),
		font_( NullPtr< const Lang::Font >( ) ),
		size_( Concrete::Length( std::numeric_limits< double >::signaling_NaN( ) ) ),
		mode_( Lang::TextRenderingMode::UNDEFINED ),
		knockout_( KNOCKOUT_UNDEFINED_BIT )
{ }

Kernel::TextState::TextState( const Kernel::TextState & orig )
	: relativeFlags_( orig.relativeFlags_ ),
		characterSpacing_( orig.characterSpacing_ ),
		wordSpacing_( orig.wordSpacing_ ),
		horizontalScaling_( orig.horizontalScaling_ ),
		leading_( orig.leading_ ),
		rise_( orig.rise_ ),
		font_( orig.font_ ),
		size_( orig.size_ ),
		mode_( orig.mode_ ),
		knockout_( orig.knockout_ )
{ }

Kernel::TextState::TextState( const Kernel::TextState & newValues, const Kernel::TextState & oldValues )
	: relativeFlags_( oldValues.relativeFlags_ ),
		characterSpacing_( oldValues.characterSpacing_ ),
		wordSpacing_( oldValues.wordSpacing_ ),
		horizontalScaling_( oldValues.horizontalScaling_ ),
		leading_( oldValues.leading_ ),
		rise_( oldValues.rise_ ),
		font_( oldValues.font_ ),
		size_( oldValues.size_ ),
		mode_( oldValues.mode_ ),
		knockout_( oldValues.knockout_ )
{
	if( newValues.hasCharacterSpacing( ) )
		{
			characterSpacing_ = newValues.characterSpacing_;
			relativeFlags_ = ( newValues.characterSpacingIsRelative( ) ? ( relativeFlags_ | RELATIVE_CHARACTER_SPACING ) : ( relativeFlags_ & ~RELATIVE_CHARACTER_SPACING ) );
		}
	if( newValues.hasWordSpacing( ) )
		{
			wordSpacing_ = newValues.wordSpacing_;
			relativeFlags_ = ( newValues.wordSpacingIsRelative( ) ? ( relativeFlags_ | RELATIVE_WORD_SPACING ) : ( relativeFlags_ & ~RELATIVE_WORD_SPACING ) );
		}
	if( ! IS_NAN( newValues.horizontalScaling_ ) )
		{
			horizontalScaling_ = newValues.horizontalScaling_;
		}
	if( newValues.hasLeading( ) )
		{
			leading_ = newValues.leading_;
			relativeFlags_ = ( newValues.leadingIsRelative( ) ? ( relativeFlags_ | RELATIVE_LEADING ) : ( relativeFlags_ & ~RELATIVE_LEADING ) );
		}
	if( newValues.hasRise( ) )
		{
			rise_ = newValues.rise_;
			relativeFlags_ = ( newValues.riseIsRelative( ) ? ( relativeFlags_ | RELATIVE_RISE ) : ( relativeFlags_ & ~RELATIVE_RISE ) );
		}
	if( newValues.font_ != NullPtr< const Lang::Font >( ) )
		{
			font_ = newValues.font_;
		}
	if( ! IS_NAN( newValues.size_ ) )
		{
			size_ = newValues.size_;
		}
	if( newValues.mode_ != Lang::TextRenderingMode::UNDEFINED )
		{
			mode_ = newValues.mode_;
		}
	if( ! ( newValues.knockout_ & KNOCKOUT_UNDEFINED_BIT ) )
		{
			knockout_ = newValues.knockout_;
		}
}

std::map< bool, RefCountPtr< SimplePDF::PDF_Object > > Kernel::TextState::knockoutNameMap_;

Kernel::TextState::TextState( bool setDefaults )
	: relativeFlags_( RELATIVE_LEADING ),
		characterSpacing_( 0 ),
		wordSpacing_( 0 ),
		horizontalScaling_( 1 ),
		leading_( 1 ),
		rise_( 0 ),
		font_( Lang::THE_FONT_HELVETICA ),
		size_( Concrete::Length( 10 ) ),
		mode_( Lang::TextRenderingMode::FILL ),
		knockout_( KNOCKOUT_FLAG_BIT ) // this means true
{
	if( ! setDefaults )
		{
			throw Exceptions::InternalError( strrefdup( "setDefaults must be true in TextState::TextState." ) );
		}
}

Kernel::TextState::~TextState( )
{ }

void
Kernel::TextState::print( std::ostream & os, const std::string & indentation ) const
{
	if( font_ != NullPtr< const Lang::Font >( ) )
		{
			os << indentation ;
			Lang::DYNAMIC_VARIABLE_ID_TEXT_FONT.show( os, Ast::Identifier::DYNAMIC_VARIABLE );
			os << ": " << font_->fontName( ) << std::endl ;
		}
	if( ! IS_NAN( size_ ) )
		{
			os << indentation ;
			Lang::DYNAMIC_VARIABLE_ID_TEXT_SIZE.show( os, Ast::Identifier::DYNAMIC_VARIABLE );
			os << ": " << size_ / Interaction::displayUnit << Interaction::displayUnitName << std::endl ;
		}
	os << "(and a bunch of other variables...)" << std::endl ;
}


void
Kernel::TextState::setLeading( const Concrete::Length leading )
{
	relativeFlags_ &= ~ RELATIVE_LEADING;
	leading_ = leading;
}

void
Kernel::TextState::setLeading( const double relativeLeading )
{
	relativeFlags_ |= RELATIVE_LEADING;
	leading_ = Concrete::Length( relativeLeading ); /* We must keep track of this type trick by always looking at relativeFlags_ & RELATIVE_LEADING. */
}

RefCountPtr< const Lang::Value >
Kernel::TextState::leading( ) const
{
	if( leadingIsRelative( ) )
		{
			return RefCountPtr< const Lang::Value >( new Lang::Float( Concrete::Length::offtype( leading_ ) ) );
		}
	return RefCountPtr< const Lang::Value >( new Lang::Length( leading_ ) );
}

Concrete::Length
Kernel::TextState::leadingConcrete( ) const
{
	if( leadingIsRelative( ) )
		{
			return Concrete::Length::offtype( leading_ ) * size_;
		}
	return leading_;
}

bool
Kernel::TextState::hasLeading( ) const
{
	return ! IS_NAN( leading_ );
}

bool
Kernel::TextState::leadingIsRelative( ) const
{
	return ( relativeFlags_ & RELATIVE_LEADING ) != 0;
}


void
Kernel::TextState::setRise( const Concrete::Length rise )
{
	relativeFlags_ &= ~ RELATIVE_RISE;
	rise_ = rise;
}

void
Kernel::TextState::setRise( const double relativeRise )
{
	relativeFlags_ |= RELATIVE_RISE;
	rise_ = Concrete::Length( relativeRise );	// We must keep track of this type trick by always looking at relativeFlags_ & RELATIVE_RISE.
}

RefCountPtr< const Lang::Value >
Kernel::TextState::rise( ) const
{
	if( riseIsRelative( ) )
		{
			return RefCountPtr< const Lang::Value >( new Lang::Float( Concrete::Length::offtype( rise_ ) ) );
		}
	return RefCountPtr< const Lang::Value >( new Lang::Length( rise_ ) );
}

Concrete::Length
Kernel::TextState::riseConcrete( ) const
{
	if( riseIsRelative( ) )
		{
			return Concrete::Length::offtype( rise_ ) * size_;
		}
	return rise_;
}

bool
Kernel::TextState::hasRise( ) const
{
	return ! IS_NAN( rise_ );
}

bool
Kernel::TextState::riseIsRelative( ) const
{
	return ( relativeFlags_ & RELATIVE_RISE ) != 0;
}


void
Kernel::TextState::setCharacterSpacing( const Concrete::Length spacing )
{
	relativeFlags_ &= ~ RELATIVE_CHARACTER_SPACING;
	characterSpacing_ = spacing;
}

void
Kernel::TextState::setCharacterSpacing( const double relativeSpacing )
{
	relativeFlags_ |= RELATIVE_CHARACTER_SPACING;
	characterSpacing_ = Concrete::Length( relativeSpacing );	// We must keep track of this type trick by always looking at the relevant bit in relativeFlags_.
}

RefCountPtr< const Lang::Value >
Kernel::TextState::characterSpacing( ) const
{
	if( characterSpacingIsRelative( ) )
		{
			return RefCountPtr< const Lang::Value >( new Lang::Float( Concrete::Length::offtype( characterSpacing_ ) ) );
		}
	return RefCountPtr< const Lang::Value >( new Lang::Length( characterSpacing_ ) );
}

Concrete::Length
Kernel::TextState::characterSpacingConcrete( ) const
{
	if( characterSpacingIsRelative( ) )
		{
			return Concrete::Length::offtype( characterSpacing_ ) * size_;
		}
	return characterSpacing_;
}

bool
Kernel::TextState::hasCharacterSpacing( ) const
{
	return ! IS_NAN( characterSpacing_ );
}

bool
Kernel::TextState::characterSpacingIsRelative( ) const
{
	return ( relativeFlags_ & RELATIVE_CHARACTER_SPACING ) != 0;
}


void
Kernel::TextState::setWordSpacing( const Concrete::Length spacing )
{
	relativeFlags_ &= ~ RELATIVE_WORD_SPACING;
	wordSpacing_ = spacing;
}

void
Kernel::TextState::setWordSpacing( const double relativeSpacing )
{
	relativeFlags_ |= RELATIVE_WORD_SPACING;
	wordSpacing_ = Concrete::Length( relativeSpacing );	// We must keep track of this type trick by always looking at the relevant bit in relativeFlags_.
}

RefCountPtr< const Lang::Value >
Kernel::TextState::wordSpacing( ) const
{
	if( wordSpacingIsRelative( ) )
		{
			return RefCountPtr< const Lang::Value >( new Lang::Float( Concrete::Length::offtype( wordSpacing_ ) ) );
		}
	return RefCountPtr< const Lang::Value >( new Lang::Length( wordSpacing_ ) );
}

Concrete::Length
Kernel::TextState::wordSpacingConcrete( ) const
{
	if( wordSpacingIsRelative( ) )
		{
			return Concrete::Length::offtype( wordSpacing_ ) * size_;
		}
	return wordSpacing_;
}

bool
Kernel::TextState::hasWordSpacing( ) const
{
	return ! IS_NAN( wordSpacing_ );
}

bool
Kernel::TextState::wordSpacingIsRelative( ) const
{
	return ( relativeFlags_ & RELATIVE_WORD_SPACING ) != 0;
}


bool
Kernel::TextState::synchAssertKnockout( std::ostream & os, const Kernel::TextState * ref, SimplePDF::PDF_Resources * resources, bool clip, bool force )
{
	assertKnockout( ref );
	return synchButKnockout( os, ref, resources, clip, force );
}

bool
Kernel::TextState::synchCharacterSpacing( std::ostream & os, const Kernel::TextState * ref, SimplePDF::PDF_Resources * resources, bool force )
{
	if( force ||
			characterSpacing_ != ref->characterSpacing_ ||
			characterSpacingIsRelative( ) != ref->characterSpacingIsRelative( ) )
		{
			if( ! ref->hasCharacterSpacing( ) )
				{
					return false;
				}
			relativeFlags_ = ( ref->characterSpacingIsRelative( ) ? ( relativeFlags_ | RELATIVE_CHARACTER_SPACING ) : ( relativeFlags_ & ~RELATIVE_CHARACTER_SPACING ) );
			characterSpacing_ = ref->characterSpacing_;
			if( characterSpacingIsRelative( ) )
				{
					os << Concrete::Length::offtype( characterSpacing_ ) * Concrete::Length::offtype( ref->size_ ) << " Tc " ;
				}
			else
				{
					os << Concrete::Length::offtype( characterSpacing_ ) << " Tc " ;
				}
			return true;
		}
	return false;
}

bool
Kernel::TextState::synchWordSpacing( std::ostream & os, const Kernel::TextState * ref, SimplePDF::PDF_Resources * resources, bool force )
{
	if( force ||
			wordSpacing_ != ref->wordSpacing_ ||
			wordSpacingIsRelative( ) != ref->wordSpacingIsRelative( ) )
		{
			if( ! ref->hasWordSpacing( ) )
				{
					return false;
				}
			relativeFlags_ = ( ref->wordSpacingIsRelative( ) ? ( relativeFlags_ | RELATIVE_WORD_SPACING ) : ( relativeFlags_ & ~RELATIVE_WORD_SPACING ) );
			wordSpacing_ = ref->wordSpacing_;
			if( wordSpacingIsRelative( ) )
				{
					os << Concrete::Length::offtype( wordSpacing_ ) * Concrete::Length::offtype( ref->size_ ) << " Tw " ;
				}
			else
				{
					os << Concrete::Length::offtype( wordSpacing_ ) << " Tw " ;
				}
			return true;
		}
	return false;
}

bool
Kernel::TextState::synchHorizontalScaling( std::ostream & os, const Kernel::TextState * ref, SimplePDF::PDF_Resources * resources, bool force )
{
	if( force || horizontalScaling_ != ref->horizontalScaling_ )
		{
			if( IS_NAN( ref->horizontalScaling_ ) )
				{
					return false;
				}
			horizontalScaling_ = ref->horizontalScaling_;
			os << 100 * horizontalScaling_ << " Tz " ;
			return true;
		}
	return false;
}

bool
Kernel::TextState::synchLeading( std::ostream & os, const Kernel::TextState * ref, SimplePDF::PDF_Resources * resources, bool force )
{
	if( force ||
			leading_ != ref->leading_ ||
			leadingIsRelative( ) != ref->leadingIsRelative( ) )
		{
			if( ! ref->hasLeading( ) )
				{
					return false;
				}
			relativeFlags_ = ( ref->leadingIsRelative( ) ? ( relativeFlags_ | RELATIVE_LEADING ) : ( relativeFlags_ & ~RELATIVE_LEADING ) );
			leading_ = ref->leading_;
			if( leadingIsRelative( ) )
				{
					os << Concrete::Length::offtype( leading_ ) * Concrete::Length::offtype( ref->size_ ) << " TL " ;
				}
			else
				{
					os << Concrete::Length::offtype( leading_ ) << " TL " ;
				}
			return true;
		}
	return false;
}

bool
Kernel::TextState::synchFontAndSize( std::ostream & os, const Kernel::TextState * ref, SimplePDF::PDF_Resources * resources, bool force )
{
	if( force ||
			font_ != ref->font_ ||
			size_ != ref->size_ )
		{
			if( ref->font_ == NullPtr< const Lang::Font >( ) &&
					IS_NAN( ref->size_ ) )
				{
					return false;
				}
			if( ref->font_ == NullPtr< const Lang::Font >( ) ||
					IS_NAN( ref->size_ ) )
				{
					throw Exceptions::MiscellaneousRequirement( "It is impossible to leave unspecified only one of font and size." );
				}
			bool sizeChanged = ( size_ != ref->size_ );
			font_ = ref->font_;
			size_ = ref->size_;
			os << resources->nameofFont( font_->resource( ) ) << " " << Concrete::Length::offtype( size_ ) << " Tf " ;
			if( sizeChanged )
				{
					if( ref->hasLeading( ) && ref->leadingIsRelative( ) )
						{
							synchLeading( os, ref, resources, true );
						}
					if( ref->hasRise( ) && ref->riseIsRelative( ) )
						{
							synchRise( os, ref, resources, true );
						}
					if( ref->hasCharacterSpacing( ) && ref->characterSpacingIsRelative( ) )
						{
							synchCharacterSpacing( os, ref, resources, true );
						}
					if( ref->hasWordSpacing( ) && ref->wordSpacingIsRelative( ) )
						{
							synchWordSpacing( os, ref, resources, true );
						}
				}
			return true;
		}
	return false;
}

bool
Kernel::TextState::synchMode( std::ostream & os, const Kernel::TextState * ref, SimplePDF::PDF_Resources * resources, bool clip, bool force )
{
	Lang::TextRenderingMode::ValueType refMode = clip ? Lang::TextRenderingMode::clipMode( ref->mode_ ) : ref->mode_ ;
	if( force || mode_ != refMode )
		{
			if( ref->mode_ == Lang::TextRenderingMode::UNDEFINED )
				{
					return false;
				}
			mode_ = refMode;
			os << mode_ << " Tr " ;
			return true;
		}
	return false;
}

bool
Kernel::TextState::synchRise( std::ostream & os, const Kernel::TextState * ref, SimplePDF::PDF_Resources * resources, bool force )
{
	if( force ||
			rise_ != ref->rise_ ||
			riseIsRelative( ) != ref->riseIsRelative( ) )
		{
			if( ! ref->hasRise( ) )
				{
					return false;
				}
			relativeFlags_ = ( ref->riseIsRelative( ) ? ( relativeFlags_ | RELATIVE_RISE ) : ( relativeFlags_ & ~RELATIVE_RISE ) );
			rise_ = ref->rise_;
			if( riseIsRelative( ) )
				{
					os << Concrete::Length::offtype( rise_ ) * Concrete::Length::offtype( ref->size_ ) << " Ts " ;
				}
			else
				{
					os << Concrete::Length::offtype( rise_ ) << " Ts " ;
				}
			return true;
		}
	return false;
}

bool
Kernel::TextState::synchKnockout( std::ostream & os, const Kernel::TextState * ref, SimplePDF::PDF_Resources * resources, bool force )
{
	if( force || knockout_ != ref->knockout_ )
		{
			if( ( ref->knockout_ & Kernel::TextState::KNOCKOUT_UNDEFINED_BIT ) != 0 )
				{
					return false;
				}
			const SimplePDF::PDF_Version::Version KNOCKOUT_VERSION = SimplePDF::PDF_Version::PDF_1_4;
			if( ! Kernel::the_PDF_version.greaterOrEqual( KNOCKOUT_VERSION ) )
				{
					Kernel::the_PDF_version.message( KNOCKOUT_VERSION, "The text state knockout mode setting was ignored." );
				}
			knockout_ = ref->knockout_;
			typedef typeof knockoutNameMap_ MapType;
			MapType::const_iterator i = knockoutNameMap_.find( knockout_ );
			if( i != knockoutNameMap_.end( ) )
				{
					os << resources->nameofGraphicsState( i->second ) << " gs " ;
				}
			else
				{
					RefCountPtr< SimplePDF::PDF_Dictionary > dic;
					(*dic)[ "Type" ] = SimplePDF::newName( "ExtGState" );
					(*dic)[ "TK" ] = SimplePDF::newBoolean( knockout_ );

					RefCountPtr< SimplePDF::PDF_Object > indirection = SimplePDF::indirect( dic, & Kernel::theIndirectObjectCount );
					knockoutNameMap_.insert( MapType::value_type( knockout_, indirection ) );

					os << resources->nameofGraphicsState( indirection ) << " gs " ;
				}
			return true;
		}
	return false;
}

void
Kernel::TextState::assertKnockout( const Kernel::TextState * ref )
{
	if( knockout_ != ref->knockout_ )
		{
			throw Exceptions::MiscellaneousRequirement( "PDF does not allow the text knockout mode to change within a text object." );
		}
}

bool
Kernel::TextState::synchButKnockout( std::ostream & os, const Kernel::TextState * ref, SimplePDF::PDF_Resources * resources, bool clip, bool force )
{
	bool anyChange = false;
	anyChange = synchCharacterSpacing( os, ref, resources, force ) || anyChange;
	anyChange = synchWordSpacing( os, ref, resources, force ) || anyChange;
	anyChange = synchHorizontalScaling( os, ref, resources, force ) || anyChange;
	anyChange = synchFontAndSize( os, ref, resources, force ) || anyChange;
	anyChange = synchLeading( os, ref, resources, force ) || anyChange;					// It is important that this is done after synching the size!
	anyChange = synchMode( os, ref, resources, clip, force ) || anyChange;
	anyChange = synchRise( os, ref, resources, force ) || anyChange;
	if( anyChange )
		{
			os << std::endl ;
		}
	return anyChange;
}


Lang::FontMethod_glyph::FontMethod_glyph( RefCountPtr< const Lang::Font > self, const Ast::FileID * fullMethodID )
	: Lang::MethodBase< class_type >( self, fullMethodID, false, true )
{
	formals_->appendEvaluatedCoreFormal( "char", Kernel::THE_SLOT_VARIABLE );
}

Lang::FontMethod_glyph::~FontMethod_glyph( )
{ }

void
Lang::FontMethod_glyph::call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
{
	args.applyDefaults( callLoc );

	size_t argsi = 0;
	typedef const Lang::Character ArgType;

	Kernel::ContRef cont = evalState->cont_;
	cont->takeValue( self_->getGlyph( Helpers::down_cast_CoreArgument< ArgType >( coreLoc_, args, argsi, callLoc )->val_,
																		evalState->dyn_->getTextState( )->size_ ),
									 evalState );
	return;
}
