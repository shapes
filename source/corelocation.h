/* This file is part of Shapes.
 *
 * Shapes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Shapes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2015 Henrik Tidefelt
 */

#pragma once

#include "refcount.h"
#include "identifier.h"
#include "methodid.h"
#include "Shapes_Ast_decls.h"
#include "Shapes_Kernel_decls.h"

#include <iostream>

namespace Shapes
{
  namespace Interaction
  {

    class CoreLocation
    {
    public:
      virtual ~CoreLocation( );

      virtual void show( std::ostream & os ) const = 0;
    };

    std::ostream & operator << ( std::ostream & os, const CoreLocation & self );

    class CharPtrLocation : public CoreLocation
    {
      const char * loc_;
    public:
      CharPtrLocation( const char * loc );
      virtual ~CharPtrLocation( );
      virtual void show( std::ostream & os ) const;
    };

    class CharRefPtrLocation : public CoreLocation
    {
      RefCountPtr< const char > loc_;
    public:
      CharRefPtrLocation( const RefCountPtr< const char > & loc );
      virtual ~CharRefPtrLocation( );
      virtual void show( std::ostream & os ) const;
    };

    class BoundLocation : public CoreLocation
    {
      Ast::PlacedIdentifier loc_;
      Ast::Identifier::Type type_;
    public:
      BoundLocation( const Ast::PlacedIdentifier & loc, Ast::Identifier::Type type = Ast::Identifier::VARIABLE );
      virtual ~BoundLocation( );
      virtual void show( std::ostream & os ) const;
    };

    class MethodLocation : public CoreLocation
    {
      Kernel::MethodId method_;
    public:
      MethodLocation( const Kernel::MethodId method );
      virtual ~MethodLocation( );
      virtual void show( std::ostream & os ) const;
    };

    class MutatorLocation : public CoreLocation
    {
      RefCountPtr< const ::Shapes::Lang::Class > class_;
      const char * name_;
    public:
      MutatorLocation( const Kernel::State * state, const char * name );
      virtual ~MutatorLocation( );
      virtual void show( std::ostream & os ) const;
    };

    /* Location based on FileID, presenting itself by means of
     * FileID::name.
     */
    class FileIDLocation : public CoreLocation
    {
      const Ast::FileID * fileID_; /* All FileID pointers are owned by FileID. */
    public:
      FileIDLocation( const Ast::FileID * fileID );
      virtual ~FileIDLocation( );
      virtual void show( std::ostream & os ) const;
    };

  }
}
