/* This file is part of Shapes.
 *
 * Shapes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Shapes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2008, 2010 Henrik Tidefelt
 */

#pragma once

#include "refcount.h"   // Why?!  (You get strange compiler errors if you dont include refcount.h here.)

#include "FontMetrics_decls.h"
#include "config.h"

#include <iconv.h>
#include <stdint.h>

#ifdef HAVE_FT2
#include <ft2build.h>
#include FT_FREETYPE_H
#endif

namespace Shapes
{
	namespace Helpers
	{

		iconv_t requireUTF8ToMacRomanConverter( bool cleanup = false );
		iconv_t requireMacRomanToUTF8Converter( bool cleanup = false );

		iconv_t requireUTF8ToUCS4Converter( bool cleanup = false );
		iconv_t requireUCS4ToUTF8Converter( bool cleanup = false );

		iconv_t requireUTF16BEToUCS4Converter( bool cleanup = false );
		iconv_t requireUCS4ToUTF16BEConverter( bool cleanup = false );

		iconv_t requireUTF8ToASCIIConverter( bool cleanup = false );
		iconv_t requireUTF8ToWinANSIConverter( bool cleanup = false );
		iconv_t requireUTF8ToUTF16BEConverter( bool cleanup = false );

		iconv_t requireUCS4ToMacRomanConverter( bool cleanup = false );


		const FontMetrics::GlyphList & requireGlyphList( bool cleanup = false );
		const FontMetrics::CharacterEncoding & requireMacRomanEncoding( bool cleanup = false );

	}

	namespace Kernel
	{

		class UnicodeCodePoint
		{
		public:
			typedef uint32_t value_type; /* Type to hold the code point in UCS4 format. */
		private:
			value_type value_;
		public:
			UnicodeCodePoint( ){ }
			UnicodeCodePoint( value_type value )
				: value_( value )
			{ }
			UnicodeCodePoint & operator = ( const UnicodeCodePoint & orig ) { value_ = orig.value_; return *this; }
			value_type get_UCS4( ) const { return value_; }
#ifdef HAVE_FT2
			FT_ULong get_FT_charcode( ) const { return value_; }
#endif
			unsigned char get_MacRoman( ) const;
			void decode_UTF8( const char ** src, size_t * src_avail );
			void decode_UCS4( const char ** src, size_t * src_avail );
			void encode_UTF8( char ** dst, size_t * dst_avail ) const;
			void encode_UTF16BE( char ** dst, size_t * dst_avail ) const;

			void decode_UTF8( const char * src );
			void decode_UCS4( const char * src );

			void decode_glyph_name( const char * name );

			bool operator == ( const UnicodeCodePoint & other ) const { return value_ == other.value_; }
			bool operator != ( const UnicodeCodePoint & other ) const { return value_ != other.value_; }
			bool operator < ( const UnicodeCodePoint & other ) const { return value_ < other.value_; }
			bool operator <= ( const UnicodeCodePoint & other ) const { return value_ <= other.value_; }
			bool operator > ( const UnicodeCodePoint & other ) const { return value_ > other.value_; }
			bool operator >= ( const UnicodeCodePoint & other ) const { return value_ >= other.value_; }

			static UnicodeCodePoint SPACE;
			static UnicodeCodePoint NEWLINE;
		};

	}
}
