/* This file is part of Shapes.
 *
 * Shapes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Shapes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2008, 2014 Henrik Tidefelt
 */

#pragma once

#include "ast.h"

namespace Shapes
{
	namespace Ast
	{

		class Constant : public Expression
		{
			Kernel::VariableHandle val_;
		public:
			Constant( const Ast::SourceLocation & loc, const Kernel::VariableHandle & val );
			Constant( const Ast::SourceLocation & loc, RefCountPtr< const Lang::Value > val );
			Constant( const Ast::SourceLocation & loc, const Lang::Value * val );
			virtual ~Constant( );
			virtual void analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst );
			virtual void eval( Kernel::EvalState * evalState ) const;
			const Kernel::VariableHandle & val( ) const { return val_; }
		};

		class PolarHandle2DExpr : public Expression
		{
			Ast::Expression * rExpr_;
			Ast::Expression * aExpr_;
		public:
			PolarHandle2DExpr( const Ast::SourceLocation & loc, Ast::Expression * rExpr, Ast::Expression * aExpr );
			virtual ~PolarHandle2DExpr( );
			virtual void analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst );
			virtual void eval( Kernel::EvalState * evalState ) const;
		};

		class PolarHandle2DExprFree_a : public Expression
		{
			Ast::Expression * rExpr_;
		public:
			PolarHandle2DExprFree_a( const Ast::SourceLocation & loc, Ast::Expression * rExpr );
			virtual ~PolarHandle2DExprFree_a( );
			virtual void analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst );
			virtual void eval( Kernel::EvalState * evalState ) const;
		};

		class SpanLastExpr : public Expression
		{
		public:
			SpanLastExpr( const Ast::SourceLocation & loc );
			virtual ~SpanLastExpr( );
			virtual void analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst );
			virtual void eval( Kernel::EvalState * evalState ) const;
		};

		/* This class really doesn't fit in any file, but here it is!
		 */
		class EmptyExpression : public Expression
		{
		public:
			EmptyExpression( const Ast::SourceLocation & loc );
			virtual ~EmptyExpression( );
			virtual void analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst );
			virtual void eval( Kernel::EvalState * evalState ) const;
		};

	}

	namespace Kernel
	{

		class PolarHandle2DCont : public Kernel::Continuation
		{
			RefCountPtr< Kernel::PolarHandlePromise > rPromise_;
			Kernel::ContRef cont_;
		public:
			PolarHandle2DCont( const Ast::SourceLocation & _traceLoc, const RefCountPtr< Kernel::PolarHandlePromise > & rPromise, const Kernel::ContRef & cont );
			virtual ~PolarHandle2DCont( );
			virtual void takeValue( const RefCountPtr< const Lang::Value > & val, Kernel::EvalState * evalState, bool dummy ) const;
			virtual Kernel::ContRef up( ) const;
			virtual RefCountPtr< const char > description( ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
		};

	}

	namespace Helpers
	{

		/* Relax evaluation and let the evaluation engine unwind its call chain and return to the
		 * main evaluation loop.
		 * This is useful if lengthy computations involving lots of continuations never encounter the
		 * evaluation of expressions.  For example, sorting a large amount of data using a built-in
		 * comparion function.
		 * The effect of calling relax is that the given value will be passed to the current continuation,
		 * but via an expression.  The caller must allow the call chain to unwind after the call to relax,
		 * so that the main evaluation loop will evaluate the evalState->expr_ set by relax.
		 * Note that relax may change the dynamic environment.
		 */
		void relax( const RefCountPtr< const Lang::Value > & val, Kernel::EvalState * evalState );

	}
}
