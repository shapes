/* This file is part of Shapes.
 *
 * Shapes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Shapes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2008, 2010 Henrik Tidefelt
 */

#pragma once

#include "Shapes_Ast_decls.h"
#include "Shapes_Kernel_decls.h"
#include "Shapes_Lang_decls.h"
#include "Shapes_Lang_decls.h"

#include "ptrowner.h"
#include "refcount.h"
#include "environment.h"
#include "statetypes.h"
#include "elementarylength.h"
#include "concretecolors.h"
#include "elementarycoords.h"
#include "fontmetrics.h"
#include "pdfstructure.h"
#include "config.h"

#include <list>
#include <iostream>
#include <stack>
#include <set>
#include <vector>
#include <iconv.h>


namespace SimplePDF
{
	class PDF_ToUnicode : public PDF_Stream_out
	{
	public:
		typedef std::set< Shapes::Kernel::UnicodeCodePoint > CharSet;
	protected:
		RefCountPtr< CharSet > charSet_;
		RefCountPtr< const Shapes::Lang::Font > font_; /* Used to determine the encoding. */
	public:
		PDF_ToUnicode( RefCountPtr< CharSet > charSet, const RefCountPtr< const Shapes::Lang::Font > & font );
		virtual ~PDF_ToUnicode( );

		virtual void writeTo( std::ostream & os, SimplePDF::PDF_xref * xref, const RefCountPtr< const PDF_Object > & self ) const;
	};

	class PDF_Widths : public PDF_Vector
	{
	public:
		typedef std::set< Shapes::Kernel::UnicodeCodePoint > CharSet;
	protected:
		RefCountPtr< CharSet > charSet_;
		RefCountPtr< const Shapes::Lang::Font > font_; /* Used to determine the encoding as well as the metrics. */
	public:
		PDF_Widths( RefCountPtr< CharSet > charSet, const RefCountPtr< const Shapes::Lang::Font > & font );
		virtual ~PDF_Widths( );

		virtual void writeTo( std::ostream & os, SimplePDF::PDF_xref * xref, const RefCountPtr< const PDF_Object > & self ) const;
	};

}

namespace Shapes
{
	namespace Lang
	{
		class FontMethod_glyph;

		class Font : public Lang::NoOperatorOverloadValue
		{
		private:
			static std::map< RefCountPtr< const char >, RefCountPtr< SimplePDF::PDF_Object >, charRefPtrLess > theFontResourceMap_;
			static std::map< RefCountPtr< const char >, RefCountPtr< const FontMetrics::FontMetric >, charRefPtrLess > theFontMetricsMap_;
			static std::list< std::string > theFontMetricsSearchPath_;

		public:
			static void push_backFontMetricsPath( const std::string & path );
			static std::string searchGlyphList( );
			static std::string searchCharacterEncoding( const char * encodingName );
		private:
			static std::string searchFontMetrics( RefCountPtr< const char > fontName );
		protected:
			RefCountPtr< const char > fontName_;
			bool outline_;
		private:
			mutable RefCountPtr< SimplePDF::PDF_Object > resource_;
			mutable RefCountPtr< const FontMetrics::FontMetric > metrics_;
		public:
			Font( const RefCountPtr< const char > fontName, bool outline, RefCountPtr< SimplePDF::PDF_Object > & resource, RefCountPtr< const FontMetrics::FontMetric > metrics = NullPtr< const FontMetrics::FontMetric >( ) );
			Font( const RefCountPtr< const char > builtInFontName );	// It is important that this is not used with fonts that aren't built in.
			virtual ~Font( );
			virtual Kernel::VariableHandle getField( const char * fieldID, const RefCountPtr< const Lang::Value > & selfRef ) const;
			virtual void show( std::ostream & os ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
			const RefCountPtr< SimplePDF::PDF_Object > & resource( ) const;
			RefCountPtr< const char > fontName( ) const;
			bool outline( ) const;
			RefCountPtr< const FontMetrics::FontMetric > metrics( ) const;
			virtual void encode( const char ** text_UTF8, size_t * in_avail, char ** dst_PDF, size_t * out_avail ) const = 0;
			virtual void encode( Kernel::UnicodeCodePoint c, char ** dst_PDF, size_t * out_avail ) const = 0;

			TYPEINFODECL;
		protected:
			/* These member functions are only here to support getField. */
			virtual RefCountPtr< const Lang::String > family_name( ) const;
			virtual RefCountPtr< const Lang::String > style_name( ) const;
			virtual RefCountPtr< const Lang::Symbol > PostScript_name( ) const;
			virtual RefCountPtr< const Lang::Value > getGlyph( Kernel::UnicodeCodePoint c, Concrete::Length size ) const;
			friend class Lang::FontMethod_glyph;
		};

		class PDFStandardFont : public Lang::Font
		{
		public:
			// It is important that this is not used with fonts that aren't built in.
			PDFStandardFont( const RefCountPtr< const char > builtInFontName );
			virtual ~PDFStandardFont( );
			virtual void encode( const char ** text_UTF8, size_t * in_avail, char ** dst_PDF, size_t * out_avail ) const;
			virtual void encode( Kernel::UnicodeCodePoint c, char ** dst_PDF, size_t * out_avail ) const;

			/* This is the implementation of the encode method.  By providing it as a static member function, it may be used by Type3Font as well. */
			static void encode_MacRoman( const char ** text_UTF8, size_t * in_avail, char ** dst_PDF, size_t * out_avail );
			static void encode_MacRoman( Kernel::UnicodeCodePoint c, char ** dst_PDF, size_t * out_avail );
		};

		class Type3Font : public Lang::Font
		{
		public:
			Type3Font( const RefCountPtr< const char > fontName, RefCountPtr< SimplePDF::PDF_Object > & resource, RefCountPtr< const FontMetrics::FontMetric > metrics );
			virtual ~Type3Font( );
			virtual void encode( const char ** text_UTF8, size_t * in_avail, char ** dst_PDF, size_t * out_avail ) const;
			virtual void encode( Kernel::UnicodeCodePoint c, char ** dst_PDF, size_t * out_avail ) const;
		};

#ifdef HAVE_FT2
		class FreeTypeFont : public Lang::Font
		{
			FT_Face face_;
			mutable RefCountPtr< SimplePDF::PDF_ToUnicode::CharSet > usedChars_;
		public:
			FreeTypeFont( FT_Face face, const RefCountPtr< const char > fontName, bool outline, RefCountPtr< SimplePDF::PDF_Object > & resource, RefCountPtr< const FontMetrics::FontMetric > metrics, RefCountPtr< SimplePDF::PDF_ToUnicode::CharSet > & usedChars );
			virtual ~FreeTypeFont( );
			virtual void encode( const char ** text_UTF8, size_t * in_avail, char ** dst_PDF, size_t * out_avail ) const;
			virtual void encode( Kernel::UnicodeCodePoint c, char ** dst_PDF, size_t * out_avail ) const;

			void shipout_glyph( Kernel::UnicodeCodePoint c, Concrete::Length size, const Lang::Transform2D & tf, std::ostream & os, bool * dstIsOdd, Concrete::Length * dstAdvance ) const; /* The value stored in *dstAdvance accounts for <size>, but not for <tf>.  Hence, it typically needs to be scaled by the current horizontal scaling. */

		protected:
			virtual RefCountPtr< const Lang::String > family_name( ) const;
			virtual RefCountPtr< const Lang::String > style_name( ) const;
			virtual RefCountPtr< const Lang::Symbol > PostScript_name( ) const;
			virtual RefCountPtr< const Lang::Value > getGlyph( Kernel::UnicodeCodePoint c, Concrete::Length size ) const;

			const FT_Outline & getGlyphOutline( Kernel::UnicodeCodePoint c ) const; /* Note! Result is only valid until a different glyph is loaded in the glyph slot. */
		};
#endif

		class TextRenderingMode : public Lang::NoOperatorOverloadValue
		{
		public:
			typedef enum { FILL = 0, STROKE, FILLSTROKE, INVISIBLE, FILLCLIP, STROKECLIP, FILLSTROKECLIP, CLIP, UNDEFINED } ValueType;
			ValueType mode_;
			TextRenderingMode( const ValueType & mode );
			TextRenderingMode( bool fill, bool stroke, bool clip );
			virtual ~TextRenderingMode( );
			virtual void gcMark( Kernel::GCMarkedSet & marked ){ };
			TYPEINFODECL;

			static ValueType clipMode( ValueType mode );
		};

		class TextOperation : public Lang::NoOperatorOverloadValue
		{
		public:
			TextOperation();
			virtual ~TextOperation( );

			virtual void shipout( std::ostream & os, Kernel::PageContentStates * pdfState, const Lang::Transform2D & tf, bool clip = false ) const = 0;
			virtual void shipout_outlined( std::ostream & os, Kernel::PageContentStates * pdfState, const Lang::Transform2D & tf, Lang::Transform2D * textMatrix, Lang::Transform2D * lineMatrix, const SimplePDF::PDF_Name * clipContent = 0 ) const = 0; /* clipContent == 0 means that the text shall be used for painting, not clipping. */
			virtual void measure( Lang::Transform2D * textMatrix, Lang::Transform2D * textLineMatrix, Concrete::Length * xmin, Concrete::Length * ymin, Concrete::Length * xmax, Concrete::Length * ymax ) const = 0;

			TYPEINFODECL;
		};

		class KernedText : public Lang::TextOperation
		{
			RefCountPtr< const Kernel::TextState > textState_;
			RefCountPtr< const Kernel::GraphicsState > metaState_;

			// The representation is a bit artificial to be efficient:	NullPtr objects in
			// strings_ mark where kerning values are to be taken from kernings_.	Hence,
			// The number of NullPtr objects in strings_ must match the length of kernings_.
			std::list< RefCountPtr< const Lang::String > > strings_;
			std::list< double > kernings_;
			size_t maxLength_;
		public:
			KernedText( const RefCountPtr< const Kernel::TextState > & textState, const RefCountPtr< const Kernel::GraphicsState > & metaState );
			KernedText( const RefCountPtr< const Kernel::TextState > & textState, const RefCountPtr< const Kernel::GraphicsState > & metaState, const RefCountPtr< const Lang::String > & str);
			virtual ~KernedText( );
			virtual Kernel::VariableHandle getField( const char * fieldID, const RefCountPtr< const Lang::Value > & selfRef ) const;
			void pushString( const RefCountPtr< const Lang::String > & str );
			void pushKerning( double kerning );

			virtual void show( std::ostream & os ) const;
			virtual void shipout( std::ostream & os, Kernel::PageContentStates * pdfState, const Lang::Transform2D & tf, bool clip ) const;
			virtual void shipout_outlined( std::ostream & os, Kernel::PageContentStates * pdfState, const Lang::Transform2D & tf, Lang::Transform2D * textMatrix, Lang::Transform2D * lineMatrix, const SimplePDF::PDF_Name * clipContent ) const;
			virtual void measure( Lang::Transform2D * textMatrix, Lang::Transform2D * textLineMatrix, Concrete::Length * xmin, Concrete::Length * ymin, Concrete::Length * xmax, Concrete::Length * ymax ) const;

			void push( Lang::KernedText * dst ) const;

			virtual void gcMark( Kernel::GCMarkedSet & marked );

		private:
			RefCountPtr< const Lang::SingleList > makeList( ) const;
			static RefCountPtr< const Lang::String > oneUCS4ToUTF8( const char * c );
		};

		class TextNewline : public Lang::TextOperation
		{
			Concrete::Coords2D t_;
		public:
			TextNewline( const Concrete::Length tx, const Concrete::Length ty );
			virtual ~TextNewline( );

			virtual void show( std::ostream & os ) const;
			virtual void shipout( std::ostream & os, Kernel::PageContentStates * pdfState, const Lang::Transform2D & tf, bool clip ) const;
			virtual void shipout_outlined( std::ostream & os, Kernel::PageContentStates * pdfState, const Lang::Transform2D & tf, Lang::Transform2D * textMatrix, Lang::Transform2D * lineMatrix, const SimplePDF::PDF_Name * clipContent ) const;
			virtual void measure( Lang::Transform2D * textMatrix, Lang::Transform2D * textLineMatrix, Concrete::Length * xmin, Concrete::Length * ymin, Concrete::Length * xmax, Concrete::Length * ymax ) const;

			virtual void gcMark( Kernel::GCMarkedSet & marked );
		};

		class TextMoveto : public Lang::TextOperation
		{
			RefCountPtr< const Lang::Transform2D > tf_;
		public:
			TextMoveto( const RefCountPtr< const Lang::Transform2D > & tf );
			virtual ~TextMoveto( );

			virtual void show( std::ostream & os ) const;
			virtual void shipout( std::ostream & os, Kernel::PageContentStates * pdfState, const Lang::Transform2D & tf, bool clip ) const;
			virtual void shipout_outlined( std::ostream & os, Kernel::PageContentStates * pdfState, const Lang::Transform2D & tf, Lang::Transform2D * textMatrix, Lang::Transform2D * lineMatrix, const SimplePDF::PDF_Name * clipContent ) const;
			virtual void measure( Lang::Transform2D * textMatrix, Lang::Transform2D * textLineMatrix, Concrete::Length * xmin, Concrete::Length * ymin, Concrete::Length * xmax, Concrete::Length * ymax ) const;

			virtual void gcMark( Kernel::GCMarkedSet & marked );
		};

	}

	namespace Lang
	{

		class CharacterSpacingBinding : public DynamicBindings
		{
			const Ast::DynamicBindingExpression * bindingExpr_;
			Concrete::Length spacing_;
			bool isRelative_;
			const Ast::PlacedIdentifier * id_;
		public:
			CharacterSpacingBinding( const Ast::PlacedIdentifier * id, const Ast::DynamicBindingExpression * bindingExpr, const Concrete::Length spacing );
			CharacterSpacingBinding( const Ast::PlacedIdentifier * id, const Ast::DynamicBindingExpression * bindingExpr, const double r );
			virtual ~CharacterSpacingBinding( );
			virtual void bind( MapType & bindings, Kernel::SystemDynamicVariables ** sysBindings ) const;
			virtual void show( std::ostream & os ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
		};

		class WordSpacingBinding : public DynamicBindings
		{
			const Ast::DynamicBindingExpression * bindingExpr_;
			Concrete::Length spacing_;
			bool isRelative_;
			const Ast::PlacedIdentifier * id_;
		public:
			WordSpacingBinding( const Ast::PlacedIdentifier * id, const Ast::DynamicBindingExpression * bindingExpr, const Concrete::Length spacing );
			WordSpacingBinding( const Ast::PlacedIdentifier * id, const Ast::DynamicBindingExpression * bindingExpr, const double r );
			virtual ~WordSpacingBinding( );
			virtual void bind( MapType & bindings, Kernel::SystemDynamicVariables ** sysBindings ) const;
			virtual void show( std::ostream & os ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
		};

		class HorizontalScalingBinding : public DynamicBindings
		{
			const Ast::DynamicBindingExpression * bindingExpr_;
			double scaling_;
			const Ast::PlacedIdentifier * id_;
		public:
			HorizontalScalingBinding( const Ast::PlacedIdentifier * id, const Ast::DynamicBindingExpression * bindingExpr, const double scaling );
			virtual ~HorizontalScalingBinding( );
			virtual void bind( MapType & bindings, Kernel::SystemDynamicVariables ** sysBindings ) const;
			virtual void show( std::ostream & os ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
		};

		class LeadingBinding : public DynamicBindings
		{
			const Ast::DynamicBindingExpression * bindingExpr_;
			Concrete::Length ty_;
			bool isRelative_;
			const Ast::PlacedIdentifier * id_;
		public:
			LeadingBinding( const Ast::PlacedIdentifier * id, const Ast::DynamicBindingExpression * bindingExpr, const Concrete::Length ty );
			LeadingBinding( const Ast::PlacedIdentifier * id, const Ast::DynamicBindingExpression * bindingExpr, const double r );
			virtual ~LeadingBinding( );
			virtual void bind( MapType & bindings, Kernel::SystemDynamicVariables ** sysBindings ) const;
			virtual void show( std::ostream & os ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
		};

		class FontBinding : public DynamicBindings
		{
			const Ast::DynamicBindingExpression * bindingExpr_;
			RefCountPtr< const Lang::Font > font_;
			const Ast::PlacedIdentifier * id_;
		public:
			FontBinding( const Ast::PlacedIdentifier * id, const Ast::DynamicBindingExpression * bindingExpr, const RefCountPtr< const Lang::Font > & font );
			virtual ~FontBinding( );
			virtual void bind( MapType & bindings, Kernel::SystemDynamicVariables ** sysBindings ) const;
			virtual void show( std::ostream & os ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
		};

		class TextSizeBinding : public DynamicBindings
		{
			const Ast::DynamicBindingExpression * bindingExpr_;
			Concrete::Length size_;
			const Ast::PlacedIdentifier * id_;
		public:
			TextSizeBinding( const Ast::PlacedIdentifier * id, const Ast::DynamicBindingExpression * bindingExpr, const Concrete::Length size );
			virtual ~TextSizeBinding( );
			virtual void bind( MapType & bindings, Kernel::SystemDynamicVariables ** sysBindings ) const;
			virtual void show( std::ostream & os ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
		};

		class TextRenderingModeBinding : public DynamicBindings
		{
			const Ast::DynamicBindingExpression * bindingExpr_;
			Lang::TextRenderingMode::ValueType mode_;
			const Ast::PlacedIdentifier * id_;
		public:
			TextRenderingModeBinding( const Ast::PlacedIdentifier * id, const Ast::DynamicBindingExpression * bindingExpr, const Lang::TextRenderingMode::ValueType mode );
			virtual ~TextRenderingModeBinding( );
			virtual void bind( MapType & bindings, Kernel::SystemDynamicVariables ** sysBindings ) const;
			virtual void show( std::ostream & os ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
		};

		class TextRiseBinding : public DynamicBindings
		{
			const Ast::DynamicBindingExpression * bindingExpr_;
			Concrete::Length ty_;
			bool isRelative_;
			const Ast::PlacedIdentifier * id_;
		public:
			TextRiseBinding( const Ast::PlacedIdentifier * id, const Ast::DynamicBindingExpression * bindingExpr, const Concrete::Length ty );
			TextRiseBinding( const Ast::PlacedIdentifier * id, const Ast::DynamicBindingExpression * bindingExpr, const double r );
			virtual ~TextRiseBinding( );
			virtual void bind( MapType & bindings, Kernel::SystemDynamicVariables ** sysBindings ) const;
			virtual void show( std::ostream & os ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
		};

		class TextKnockoutBinding : public DynamicBindings
		{
			const Ast::DynamicBindingExpression * bindingExpr_;
			bool knockout_;
			const Ast::PlacedIdentifier * id_;
		public:
			TextKnockoutBinding( const Ast::PlacedIdentifier * id, const Ast::DynamicBindingExpression * bindingExpr, const bool knockout );
			virtual ~TextKnockoutBinding( );
			virtual void bind( MapType & bindings, Kernel::SystemDynamicVariables ** sysBindings ) const;
			virtual void show( std::ostream & os ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
		};

	}

	namespace Kernel
	{

		class CharacterSpacingDynamicVariableProperties : public Kernel::DynamicVariableProperties
		{
		public:
			CharacterSpacingDynamicVariableProperties( const Ast::PlacedIdentifier * id );
			virtual ~CharacterSpacingDynamicVariableProperties( );
			virtual Kernel::VariableHandle fetch( const Kernel::PassedDyn & dyn ) const;
			virtual void makeBinding( Kernel::VariableHandle val, const Ast::DynamicBindingExpression * bindingExpr, Kernel::EvalState * evalState ) const;
		};

		class WordSpacingDynamicVariableProperties : public Kernel::DynamicVariableProperties
		{
		public:
			WordSpacingDynamicVariableProperties( const Ast::PlacedIdentifier * id );
			virtual ~WordSpacingDynamicVariableProperties( );
			virtual Kernel::VariableHandle fetch( const Kernel::PassedDyn & dyn ) const;
			virtual void makeBinding( Kernel::VariableHandle val, const Ast::DynamicBindingExpression * bindingExpr, Kernel::EvalState * evalState ) const;
		};

		class HorizontalScalingDynamicVariableProperties : public Kernel::DynamicVariableProperties
		{
		public:
			HorizontalScalingDynamicVariableProperties( const Ast::PlacedIdentifier * id );
			virtual ~HorizontalScalingDynamicVariableProperties( );
			virtual Kernel::VariableHandle fetch( const Kernel::PassedDyn & dyn ) const;
			virtual void makeBinding( Kernel::VariableHandle val, const Ast::DynamicBindingExpression * bindingExpr, Kernel::EvalState * evalState ) const;
		};

		class LeadingDynamicVariableProperties : public Kernel::DynamicVariableProperties
		{
		public:
			LeadingDynamicVariableProperties( const Ast::PlacedIdentifier * id );
			virtual ~LeadingDynamicVariableProperties( );
			virtual Kernel::VariableHandle fetch( const Kernel::PassedDyn & dyn ) const;
			virtual void makeBinding( Kernel::VariableHandle val, const Ast::DynamicBindingExpression * bindingExpr, Kernel::EvalState * evalState ) const;
		};

		class FontDynamicVariableProperties : public Kernel::DynamicVariableProperties
		{
		public:
			FontDynamicVariableProperties( const Ast::PlacedIdentifier * id );
			virtual ~FontDynamicVariableProperties( );
			virtual Kernel::VariableHandle fetch( const Kernel::PassedDyn & dyn ) const;
			virtual void makeBinding( Kernel::VariableHandle val, const Ast::DynamicBindingExpression * bindingExpr, Kernel::EvalState * evalState ) const;
		};

		class TextSizeDynamicVariableProperties : public Kernel::DynamicVariableProperties
		{
		public:
			TextSizeDynamicVariableProperties( const Ast::PlacedIdentifier * id );
			virtual ~TextSizeDynamicVariableProperties( );
			virtual Kernel::VariableHandle fetch( const Kernel::PassedDyn & dyn ) const;
			virtual void makeBinding( Kernel::VariableHandle val, const Ast::DynamicBindingExpression * bindingExpr, Kernel::EvalState * evalState ) const;
		};

		class TextRenderingModeDynamicVariableProperties : public Kernel::DynamicVariableProperties
		{
		public:
			TextRenderingModeDynamicVariableProperties( const Ast::PlacedIdentifier * id );
			virtual ~TextRenderingModeDynamicVariableProperties( );
			virtual Kernel::VariableHandle fetch( const Kernel::PassedDyn & dyn ) const;
			virtual void makeBinding( Kernel::VariableHandle val, const Ast::DynamicBindingExpression * bindingExpr, Kernel::EvalState * evalState ) const;
		};

		class TextRiseDynamicVariableProperties : public Kernel::DynamicVariableProperties
		{
		public:
			TextRiseDynamicVariableProperties( const Ast::PlacedIdentifier * id );
			virtual ~TextRiseDynamicVariableProperties( );
			virtual Kernel::VariableHandle fetch( const Kernel::PassedDyn & dyn ) const;
			virtual void makeBinding( Kernel::VariableHandle val, const Ast::DynamicBindingExpression * bindingExpr, Kernel::EvalState * evalState ) const;
		};

		class TextKnockoutDynamicVariableProperties : public Kernel::DynamicVariableProperties
		{
		public:
			TextKnockoutDynamicVariableProperties( const Ast::PlacedIdentifier * id );
			virtual ~TextKnockoutDynamicVariableProperties( );
			virtual Kernel::VariableHandle fetch( const Kernel::PassedDyn & dyn ) const;
			virtual void makeBinding( Kernel::VariableHandle val, const Ast::DynamicBindingExpression * bindingExpr, Kernel::EvalState * evalState ) const;
		};

		class TextState
		{
		private:
			static const char RELATIVE_LEADING = 0x01;
			static const char RELATIVE_RISE = 0x02;
			static const char RELATIVE_CHARACTER_SPACING = 0x04;
			static const char RELATIVE_WORD_SPACING = 0x08;
		public:
			unsigned char relativeFlags_;

		public:
			static const char KNOCKOUT_FLAG_BIT = 0x01;
			static const char KNOCKOUT_UNDEFINED_BIT = 0x02;
		private:
			Concrete::Length characterSpacing_;												// Use NaN for undefined.
			Concrete::Length wordSpacing_;														 // Use NaN for undefined.
		public:
			double horizontalScaling_;																 // Use NaN for undefined.
		private:
			Concrete::Length leading_;																 // Use NaN for undefined.	Offtype in case leadingIsRelative_.
			Concrete::Length rise_;																		// Use NaN for undefined.	Offtype in case riseIsRelative_.
		public:
			RefCountPtr< const Lang::Font > font_;										 // Use NullPtr for undefined.
			Concrete::Length size_;																		// Use NaN for undefined.
			Lang::TextRenderingMode::ValueType mode_;									// Use UNDEFINED for undefined.
			char knockout_;																						// Set the bit KNOCKOUT_UNDEFINED for undefined.
		public:
			TextState( );
			explicit TextState( const Kernel::TextState & orig );	 // explicit, since reference counting shall be used in most cases
			TextState( const Kernel::TextState & newValues, const Kernel::TextState & oldValues );
			TextState( bool setDefaults );
			~TextState( );

			void setLeading( const Concrete::Length leading );
			void setLeading( const double relativeLeading );
			bool hasLeading( ) const;
			inline bool leadingIsRelative( ) const;
			Concrete::Length leadingConcrete( ) const;
			RefCountPtr< const Lang::Value > leading( ) const;

			void setRise( const Concrete::Length rise );
			void setRise( const double relativeRise );
			bool hasRise( ) const;
			bool riseIsRelative( ) const;
			Concrete::Length riseConcrete( ) const;
			RefCountPtr< const Lang::Value > rise( ) const;

			void setCharacterSpacing( const Concrete::Length spacing );
			void setCharacterSpacing( const double relativeSpacing );
			bool hasCharacterSpacing( ) const;
			bool characterSpacingIsRelative( ) const;
			Concrete::Length characterSpacingConcrete( ) const;
			RefCountPtr< const Lang::Value > characterSpacing( ) const;

			void setWordSpacing( const Concrete::Length spacing );
			void setWordSpacing( const double relativeSpacing );
			bool hasWordSpacing( ) const;
			bool wordSpacingIsRelative( ) const;
			Concrete::Length wordSpacingConcrete( ) const;
			RefCountPtr< const Lang::Value > wordSpacing( ) const;

			bool synchKnockout( std::ostream & os, const Kernel::TextState * ref, SimplePDF::PDF_Resources * resources, bool force = false );
			bool synchAssertKnockout( std::ostream & os, const Kernel::TextState * ref, SimplePDF::PDF_Resources * resources, bool clip, bool force = false );

			void print( std::ostream & os, const std::string & indentation ) const;

		private:
			static std::map< bool, RefCountPtr< SimplePDF::PDF_Object > > knockoutNameMap_;

			bool synchCharacterSpacing( std::ostream & os, const Kernel::TextState * ref, SimplePDF::PDF_Resources * resources, bool force = false );
			bool synchWordSpacing( std::ostream & os, const Kernel::TextState * ref, SimplePDF::PDF_Resources * resources, bool force = false );
			bool synchHorizontalScaling( std::ostream & os, const Kernel::TextState * ref, SimplePDF::PDF_Resources * resources, bool force = false );
			bool synchLeading( std::ostream & os, const Kernel::TextState * ref, SimplePDF::PDF_Resources * resources, bool force = false );
			bool synchFontAndSize( std::ostream & os, const Kernel::TextState * ref, SimplePDF::PDF_Resources * resources, bool force = false );
			bool synchMode( std::ostream & os, const Kernel::TextState * ref, SimplePDF::PDF_Resources * resources, bool clip, bool force = false );
			bool synchRise( std::ostream & os, const Kernel::TextState * ref, SimplePDF::PDF_Resources * resources, bool force = false );

			void assertKnockout( const Kernel::TextState * ref );

			bool synchButKnockout( std::ostream & os, const Kernel::TextState * ref, SimplePDF::PDF_Resources * resources, bool clip, bool force = false );

		};

	}
}
