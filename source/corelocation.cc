 /* This file is part of Shapes.
 *
 * Shapes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Shapes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2015 Henrik Tidefelt
 */

#include "corelocation.h"
#include "sourcelocation.h"
#include "environment.h"
#include "classtypes.h"

using namespace Shapes;


Interaction::CoreLocation::~CoreLocation( )
{ }


std::ostream &
Interaction::operator << ( std::ostream & os, const Interaction::CoreLocation & self )
{
  self.show( os );
  return os;
}


Interaction::CharPtrLocation::CharPtrLocation( const char * loc )
: loc_( loc )
{ }

Interaction::CharPtrLocation::~CharPtrLocation( )
{ }

void
Interaction::CharPtrLocation::show( std::ostream & os ) const
{
  os << loc_ ;
}


Interaction::CharRefPtrLocation::CharRefPtrLocation( const RefCountPtr< const char > & loc )
: loc_( loc )
{ }

Interaction::CharRefPtrLocation::~CharRefPtrLocation( )
{ }

void
Interaction::CharRefPtrLocation::show( std::ostream & os ) const
{
  os << loc_ ;
}


Interaction::BoundLocation::BoundLocation( const Ast::PlacedIdentifier & loc, Ast::Identifier::Type type )
: loc_( loc ), type_( type )
{ }

Interaction::BoundLocation::~BoundLocation( )
{ }

void
Interaction::BoundLocation::show( std::ostream & os ) const
{
  loc_.show( os, type_ );
}


Interaction::MethodLocation::MethodLocation( const Kernel::MethodId method )
: method_( method )
{ }

Interaction::MethodLocation::~MethodLocation( )
{ }

void
Interaction::MethodLocation::show( std::ostream & os ) const
{
  os << method_.prettyName( ) ;
}


Interaction::MutatorLocation::MutatorLocation( const Kernel::State * state, const char * name )
: class_( state->getClass( ) ), name_( name )
{ }

Interaction::MutatorLocation::~MutatorLocation( )
{ }

void
Interaction::MutatorLocation::show( std::ostream & os ) const
{
  os << class_->getTypeName( ) << "." << name_ ;
}


Interaction::FileIDLocation::FileIDLocation( const Ast::FileID * fileID )
: fileID_( fileID )
{ }

Interaction::FileIDLocation::~FileIDLocation( )
{ }

void
Interaction::FileIDLocation::show( std::ostream & os ) const
{
  os << fileID_->name( ) ;
}
