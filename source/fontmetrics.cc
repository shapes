/* This file is part of Shapes.
 *
 * Shapes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Shapes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2008 Henrik Tidefelt
 */

#include "fontmetrics.h"
#include "strrefdup.h"
#include "shapesexceptions.h"


FontMetrics::CharacterMetrics::~CharacterMetrics( )
{
	if( ligatureSetupMap_ != 0 )
		{
			delete ligatureSetupMap_;
		}
}

bool
FontMetrics::CharacterMetrics::isEmpty( ) const
{
	return xmax_ == 0 && ymax_ == 0&& xmin_ == 0 && ymin_ == 0;
}

bool
FontMetrics::CharacterMetrics::hasLigature( size_t otherInternalPosition, size_t * ligatureInternalPosition ) const
{
	typedef typeof( ligatures_ ) MapType;
	MapType::const_iterator i = ligatures_.find( otherInternalPosition );
	if( i == ligatures_.end( ) )
		{
			return false;
		}
	*ligatureInternalPosition = i->second;
	return true;
}

void
FontMetrics::CharacterMetrics::addLigature( RefCountPtr< const char > otherName, RefCountPtr< const char > ligatureName )
{
	typedef typeof( *ligatureSetupMap_ ) MapType;
	if( ligatureSetupMap_ == 0 )
		{
			ligatureSetupMap_ = new MapType;;
		}
	ligatureSetupMap_->insert( MapType::value_type( otherName, ligatureName ) );
}

void
FontMetrics::CharacterMetrics::setupLigatures( const std::map< RefCountPtr< const char >, size_t, charRefPtrLess > & nameMap ) const
{
	if( ligatureSetupMap_ == 0 )
		{
			return;
		}

	typedef typeof( *ligatureSetupMap_ ) MapType;
	typedef typeof( nameMap ) NameMapType;
	for( MapType::const_iterator i = ligatureSetupMap_->begin( ); i != ligatureSetupMap_->end( ); ++i )
		{
			NameMapType::const_iterator iFirst = nameMap.find( i->first );
			NameMapType::const_iterator iSecond = nameMap.find( i->second );
			if( iFirst == nameMap.end( ) || iSecond == nameMap.end( ) )
				{
					throw strrefdup( "Font metrics ligature uses undefined character name." );
				}
			ligatures_[ iFirst->second ] = iSecond->second;
		}

	delete ligatureSetupMap_;
	ligatureSetupMap_ = 0;
}

void
FontMetrics::CharacterMetrics::display( std::ostream & os ) const
{
		os << "[" << internalPosition_ << "] "
			 << " C " << characterCode_ << ";"
			 << " W0 " << horizontalCharWidthX_ << " " << horizontalCharWidthY_ << ";"
			 << " B " << xmin_ << " " << ymin_ << " " << xmax_ << " " << ymax_ << ";" ;
}

FontMetrics::WritingDirectionMetrics::WritingDirectionMetrics( )
{ }

FontMetrics::WritingDirectionMetrics::~WritingDirectionMetrics( )
{ }

FontMetrics::SingleByte_WritingDirectionMetrics::SingleByte_WritingDirectionMetrics( )
{
	// charData_ begins with a default entry.  This makes it possible to use 0 for "undefined" in codeMap_.
	FontMetrics::CharacterMetrics * defaultChar = new FontMetrics::CharacterMetrics( charData_.size( ) );
	charData_.push_back( defaultChar );
	// The values of defaultChar are not set here, since we need both horizontal and vertical information for that.

	codeMap_.resize( 256, 0 );
}

FontMetrics::SingleByte_WritingDirectionMetrics::~SingleByte_WritingDirectionMetrics( )
{ }

void
FontMetrics::SingleByte_WritingDirectionMetrics::setupLigatures( )
{
	typedef typeof( charData_ ) ListType;
	for( ListType::iterator i = charData_.begin( ); i != charData_.end( ); ++i )
		{
			(*i)->setupLigatures( nameMap_ );
		}
}

const FontMetrics::CharacterMetrics *
FontMetrics::SingleByte_WritingDirectionMetrics::charByName( const char * name ) const
{
	typedef typeof( nameMap_ ) NameMapType;
	NameMapType::const_iterator i = nameMap_.find( strrefdup( name ) );
	if( i == nameMap_.end( ) )
		{
			throw "No character by that name in font metrics.";
		}
	return charData_[ i->second ];
}

const FontMetrics::CharacterMetrics *
FontMetrics::SingleByte_WritingDirectionMetrics::charByCode_MacRoman( unsigned char code ) const
{
	return charData_[ codeMap_[ code ] ];
}

const FontMetrics::CharacterMetrics *
FontMetrics::SingleByte_WritingDirectionMetrics::charByCode( Shapes::Kernel::UnicodeCodePoint code, FontMetrics::CharacterMetrics * mem ) const
{
	return charByCode_MacRoman( code.get_MacRoman( ) );
}

const FontMetrics::CharacterMetrics *
FontMetrics::SingleByte_WritingDirectionMetrics::default_char( ) const
{
	return charByCode_MacRoman( 0 );
}


void
FontMetrics::SingleByte_WritingDirectionMetrics::display( std::ostream & os ) const
{
	typedef typeof( nameMap_ ) NameMapType;
	for( NameMapType::const_iterator i = nameMap_.begin( ); i != nameMap_.end( ); ++i )
		{
			os << "\"" << i->first << "\" " ;
			charData_[ i->second ]->display( os );
			os << std::endl ;
		}
}

FontMetrics::TrackKerning::TrackKerning( double sizeLow, double trackLow, double sizeHigh, double trackHigh )
	: sizeLow_( sizeLow ), trackLow_( trackLow ), sizeHigh_( sizeHigh ), trackHigh_( trackHigh )
{ }

double
FontMetrics::TrackKerning::operator () ( double sz ) const
{
	if( sz < sizeLow_ )
		{
			return trackLow_;
		}
	if( sz > sizeHigh_ )
		{
			return trackHigh_;
		}
	return trackLow_ + ( ( sz - sizeLow_ ) / ( sizeHigh_ - sizeLow_ ) ) * ( trackHigh_ - trackLow_ );
}

FontMetrics::FontMetric::~FontMetric( )
{ }

FontMetrics::AFM::AFM( )
	: FontMetric( false ),
		version_( NullPtr< const char >( ) ),
		notice_( NullPtr< const char >( ) ),
		encodingScheme_( NullPtr< const char >( ) ),
		characterSet_( NullPtr< const char >( ) ),
		isFixedV_( false )
{
	hasKerning_ = true;
}

FontMetrics::AFM::~AFM( )
{ }

double
FontMetrics::AFM::getHorizontalKernPairXByCode( Shapes::Kernel::UnicodeCodePoint code1, Shapes::Kernel::UnicodeCodePoint code2 ) const
{
	KernPairMap::const_iterator i = horizontalKernPairsX_.find( KernPairMap::key_type( code1, code2 ) );
	if( i == horizontalKernPairsX_.end( ) )
		{
			return 0;
		}
	return i->second;
}

#ifdef HAVE_FT2

FontMetrics::FreeType2_Metric::FreeType2_Metric( FT_Face face )
	: FontMetric( true ),
		face_( face ),
		font_unit_( 1 / static_cast< double >( face_->units_per_EM ) )
{
	familyName_ = strrefdup( face->family_name );
	if( face->style_name != 0 )
		{
			weight_ = strrefdup( face->style_name );
			std::ostringstream oss;
			oss << face->family_name << "-" << face->style_name ;
			fontName_ = strrefdup( oss );
		}
	else
		{
			weight_ = strrefdup( "" );
			fontName_ = familyName_;
		}
	fullName_ = fontName_;

	fontBBoxXMin_ = face->bbox.xMin * font_unit_;
	fontBBoxYMin_ = face->bbox.yMin * font_unit_;
	fontBBoxXMax_ = face->bbox.xMax * font_unit_;
	fontBBoxYMax_ = face->bbox.yMax * font_unit_;

	ascender_ = face->ascender * font_unit_;
	descender_ = face->descender * font_unit_;
	leading_ = face->height * font_unit_;

	if( FT_HAS_HORIZONTAL( face ) )
		{
			horizontalMetrics_ = RefCountPtr< FontMetrics::FreeType2_WritingDirectionMetrics >( new FontMetrics::FreeType2_WritingDirectionMetrics( face, *this, false ) );
		}
	if( FT_HAS_VERTICAL( face ) )
		{
			verticalMetrics_ = RefCountPtr< FontMetrics::FreeType2_WritingDirectionMetrics >( new FontMetrics::FreeType2_WritingDirectionMetrics( face, *this, true ) );
		}

#ifdef HAVE_FT2_10
	isCIDFont_ = FT_IS_CID_KEYED( face_ ); /* I can't see why we should care about this value. */
	hasKerning_ = FT_HAS_KERNING( face_ );
#else
	isCIDFont_ = false;
	hasKerning_ = false;
#endif
}

FontMetrics::FreeType2_Metric::~FreeType2_Metric( )
{ }

double
FontMetrics::FreeType2_Metric::getHorizontalKernPairXByCode( Shapes::Kernel::UnicodeCodePoint code1, Shapes::Kernel::UnicodeCodePoint code2 ) const
{
	if( ! FT_HAS_KERNING( face_ ) )
		{
			throw Shapes::Exceptions::OutOfRange( "FreeType says that the current font does not support automatic kerning." );
		}
	FT_Vector k;
	FT_Error error = FT_Get_Kerning( face_, FT_Get_Char_Index( face_, code1.get_UCS4( ) ), FT_Get_Char_Index( face_, code2.get_UCS4( ) ),
																	 FT_KERNING_UNSCALED, & k );
	if( error != 0 )
		{
			std::ostringstream msg;
			msg << "Unable obtain kern value from FreeType, between code point " << code1.get_UCS4( ) << " and code point " << code2.get_UCS4( ) ;
			throw Shapes::Exceptions::ExternalError( strrefdup( msg ) );
		}
	return k.x * font_unit_;
}

FontMetrics::FreeType2_WritingDirectionMetrics::FreeType2_WritingDirectionMetrics( FT_Face face, const FontMetric & parent, bool vertical )
	: face_( face ),
		default_char_( 0 ),
		vertical_( vertical ),
		font_unit_( 1 / static_cast< double >( face_->units_per_EM ) )
{
	default_char_.horizontalCharWidthX_ = parent.fontBBoxXMax_ - parent.fontBBoxXMin_;
	default_char_.horizontalCharWidthY_ = parent.fontBBoxYMax_ - parent.fontBBoxYMin_;
	default_char_.verticalCharWidthX_ = parent.fontBBoxXMax_ - parent.fontBBoxXMin_;
	default_char_.verticalCharWidthY_ = parent.fontBBoxYMax_ - parent.fontBBoxYMin_;
	default_char_.xmin_ = parent.fontBBoxXMin_;
	default_char_.ymin_ = parent.fontBBoxYMin_;
	default_char_.xmax_ = parent.fontBBoxXMax_;
	default_char_.ymax_ = parent.fontBBoxYMax_;
	default_char_.vX_ = 0; /* What is this?! */
	default_char_.vY_ = 0; /* What is this?! */
}

FontMetrics::FreeType2_WritingDirectionMetrics::~FreeType2_WritingDirectionMetrics( )
{ }

const FontMetrics::CharacterMetrics *
FontMetrics::FreeType2_WritingDirectionMetrics::charByCode( Shapes::Kernel::UnicodeCodePoint code, FontMetrics::CharacterMetrics * mem ) const
{
	FT_UInt index = FT_Get_Char_Index( face_, code.get_UCS4( ) );
	if( index == 0 )
		{
			return & default_char_;
		}
	FT_Error error = FT_Load_Glyph( face_, index, FT_LOAD_NO_SCALE );
  if( error != 0 )
		{
			std::ostringstream msg;
			msg << "Unable to load glyph in FreeType, at code point " << code.get_UCS4( ) ;
			throw Shapes::Exceptions::OutOfRange( strrefdup( msg ) );
		}
	FT_Glyph_Metrics m = face_->glyph->metrics;
	if( vertical_ )
		{
			mem->xmin_ = m.vertBearingX * font_unit_;
			mem->ymax_ = m.vertBearingY * font_unit_;
			mem->xmax_ = mem->xmin_ + m.width * font_unit_;
			mem->ymin_ = mem->ymax_ - m.height * font_unit_;
			mem->verticalCharWidthY_ = m.vertAdvance * font_unit_;
		}
	else
		{
			mem->xmin_ = m.horiBearingX * font_unit_;
			mem->ymax_ = m.horiBearingY * font_unit_;
			mem->xmax_ = mem->xmin_ + m.width * font_unit_;
			mem->ymin_ = mem->ymax_ - m.height * font_unit_;
			mem->horizontalCharWidthX_ = m.horiAdvance * font_unit_;
		}
	return mem;
}

const FontMetrics::CharacterMetrics *
FontMetrics::FreeType2_WritingDirectionMetrics::default_char( ) const
{
	return & default_char_;
}

#endif
