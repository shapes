/* This file is part of Shapes.
 *
 * Shapes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Shapes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2013, 2014 Henrik Tidefelt
 */

template< class T >
Shapes::Kernel::Node::ListRef
Shapes::Kernel::Node::prepend_edges( const T & edges, bool directed, const ListRef & rest, const RefCountPtr< const Lang::Graph > & graph, const Kernel::EdgePredicate * filter )
{
	Kernel::Node::ListRef result = rest;
	if( filter == NULL ){
		for( typename T::const_iterator e = edges.begin( ); e != edges.end( ); ++e ){
			result = Helpers::SingleList_cons( static_cast< Lang::Value * >( new Lang::Edge( graph, *e, directed ) ), result );
		}
	}else{
		for( typename T::const_iterator e = edges.begin( ); e != edges.end( ); ++e ){
			if( (*filter)( **e ) ){
				result = Helpers::SingleList_cons( static_cast< Lang::Value * >( new Lang::Edge( graph, *e, directed ) ), result );
			}
		}
	}
	return result;
}

template< class T >
Shapes::Kernel::Node::ListRef
Shapes::Kernel::Node::prepend_edges_neighbor( Kernel::Node * neighbor, const T & edges, bool directed, const ListRef & rest, const RefCountPtr< const Lang::Graph > & graph, const Kernel::EdgePredicate * filter )
{
	/* Out of all four arguments to the Kernel::Edge constructor, only one of the first two and the third will be used. */
	Kernel::Edge edgeLow( neighbor, neighbor, 0, true );
	Kernel::Edge edgeHigh( neighbor, neighbor, std::numeric_limits< size_t >::max( ), true );
	Kernel::Node::ListRef result = rest;
	typename T::const_iterator eBegin = edges.lower_bound( & edgeLow );
	typename T::const_iterator eEnd = edges.upper_bound( & edgeHigh );
	if( filter == NULL ){
		for( typename T::const_iterator e = eBegin; e != eEnd; ++e ){
			result = Helpers::SingleList_cons( static_cast< Lang::Value * >( new Lang::Edge( graph, *e, directed ) ), result );
		}
	}else{
		for( typename T::const_iterator e = eBegin; e != eEnd; ++e ){
			if( (*filter)( **e ) ){
				result = Helpers::SingleList_cons( static_cast< Lang::Value * >( new Lang::Edge( graph, *e, directed ) ), result );
			}
		}
	}
	return result;
}


template< class T >
Shapes::Kernel::EdgeLabelPredicate< T >::EdgeLabelPredicate( const RefCountPtr< std::vector< Kernel::ValueRef > > & edgeLabels, const T & label )
	: edgeLabels_( edgeLabels ), label_( label )
{
	if( edgeLabels_ == NullPtr< std::vector< Kernel::ValueRef > >( ) )
		always_false_ = true;
}

template< class T >
Shapes::Kernel::EdgeLabelPredicate< T >::~EdgeLabelPredicate( )
{ }

template< class T >
bool
Shapes::Kernel::EdgeLabelPredicate< T >::operator () ( const Edge & e ) const
{
	if( always_false_ )
		return false;
	const T * ptr = dynamic_cast< const T * >( (*edgeLabels_)[ e.index( ) ].getPtr( ) );
	if( ptr == NULL )
		return false;
	return *ptr == label_;
}
