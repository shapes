/* This file is part of Shapes.
 *
 * Shapes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Shapes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2008 Henrik Tidefelt
 */

#pragma once

#include "Shapes_Lang_decls.h"

#include "refcount.h"

namespace Shapes
{
	namespace Kernel
	{

		class MethodId
		{
			RefCountPtr< const Lang::Class > myClass_;
			const char * name_;
		public:
			MethodId( const RefCountPtr< const Lang::Class > & myClass, const char * name );
			~MethodId( );
			RefCountPtr< const char > prettyName( ) const;
			const RefCountPtr< const Lang::Class > & getClass( ) const { return myClass_; }
			const char * getIdentifier( ) const;
			friend bool operator < ( const MethodId & mid1, const MethodId & mid2 );
		};

		bool operator < ( const MethodId & mid1, const MethodId & mid2 );

	}
}
