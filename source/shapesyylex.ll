/* This file is part of Shapes.
 *
 * Shapes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Shapes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2008, 2010, 2013, 2014 Henrik Tidefelt
 */

%{

#include <cmath>

#include "shapesscanner.h"
#include "shapesvalue.h"
#include "shapestypes.h"
#include "astflow.h"
#include "astvar.h"
#include "astclass.h"
#include "shapesexceptions.h"
#include "texlabelmanager.h"
#include "globals.h"
#include "exitcodes.h"

using namespace Shapes;
#include "yyltype.h"
#include "shapesparser.hh"

#include <string>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <cstdio> /* This is a workaround for a bug in Flex. */

#define YY_USER_ACTION doBeforeEachAction( );
#define YY_EXIT_FAILURE Shapes::Interaction::EXIT_INTERNAL_ERROR

double shapes_strtod( char * str, char ** end );
char shapes_hexToChar( char c1, char c2 );

%}

WhiteSpace [ ]

LexDirPrefix ^[ \t]*"##"
Float [~]?[0-9]+([.][0-9]*)?("*^"[~]?[0-9]+)?
Greek "α"|"β"|"γ"|"Γ"|"δ"|"Δ"|"ε"|"ζ"|"η"|"Θ"|"ι"|"κ"|"λ"|"Λ"|"μ"|"ν"|"χ"|"Ξ"|"π"|"Π"|"ρ"|"σ"|"Σ"|"τ"|"ϕ"|"ω"|"Ω"
LowerCaseLetter [a-z_?]
UpperCaseLetter [A-Z]
Letter {LowerCaseLetter}|{UpperCaseLetter}|{Greek}
SimpleIdentifier {Letter}({Letter}|[0-9])*
Encapsulation "^^"
NamespaceSep ".."
NonEmptyNamespacePath ({Encapsulation}|{NamespaceSep})?({SimpleIdentifier}{NamespaceSep})*{SimpleIdentifier}
UTF8char [\x00-\x7F]|([\xC0-\xDF][\x80-\xBF])|([\xE0-\xEF][\x80-\xBF][\x80-\xBF])|([\xF0-\xF7][\x80-\xBF][\x80-\xBF][\x80-\xBF])
DynamicMark "@"
StateMark "#"|"•"
TypeMark "//"|"§"


%option c++
%option noyywrap
%option yylineno

%option prefix="shapes"
%option yyclass="ShapesScanner"

%x DiscardRestOfLineAndBEGIN_INITIAL
%x Incl
%x Needs
%x InclOptFile
%x InclFile
%x InclFrom
%x InclPath
%x NamespacePrefix
%x String
%x PoorMansString
%x DataStringPlain
%x DataStringHex
%x Comment
%x LaTeXOption
%x LaTeXClass
%x LaTeXPreamble
%x LaTeXDocumentTop
%x RandSeed
%x NewUnitName
%x NewUnitEqual
%x NewUnitValue
%x Author
%x Echo
%x Push
%x Pop
%x Lookin
%x TreeNamespaceAlias
%x TreeNamespaceEqual
%x TreeNamespaceOriginal


%%

{LexDirPrefix}"classoption"[ \t]+ { BEGIN( LaTeXOption ); }
{LexDirPrefix}"documentclass"[ \t]+ { BEGIN( LaTeXClass ); }
{LexDirPrefix}"preamble"[ \t] { BEGIN( LaTeXPreamble ); }
{LexDirPrefix}"documenttop"[ \t] { BEGIN( LaTeXDocumentTop ); }
{LexDirPrefix}"no-lmodernT1"[\n] { Kernel::theTeXLabelManager.setlmodernT1( shapeslloc, false ); endOfLine( ); }
{LexDirPrefix}"no-utf8"[\n] { Kernel::theTeXLabelManager.setutf8( shapeslloc, false ); endOfLine( ); }
{LexDirPrefix}"seed"[ \t]+ { BEGIN( RandSeed ); }
{LexDirPrefix}"unit"[ \t]+ { BEGIN( NewUnitName ); }
{LexDirPrefix}"author"[ \t] { BEGIN( Author ); }
{LexDirPrefix}"include"[ \t]+ { BEGIN( Incl ); return T_srcLoc; }
{LexDirPrefix}"needs"[ \t]+ { BEGIN( Needs ); return T_srcLoc; }
{LexDirPrefix}"echo"[ \t] { BEGIN( Echo ); }
{LexDirPrefix}"push"[ \t]+ { BEGIN( Push ); }
{LexDirPrefix}"pop"[ \t]+ { BEGIN( Pop ); }
{LexDirPrefix}"lookin"[ \t]+ { BEGIN( Lookin ); }
{LexDirPrefix}"alias"[ \t]+ { BEGIN( TreeNamespaceAlias ); }
{LexDirPrefix} {
	Ast::theAnalysisErrorsList.push_back( new Exceptions::ScannerError( shapeslloc, strrefdup( "Unrecognized lexer directive (all lines beginning with ## are lexer directives)." ) ) );
	BEGIN( DiscardRestOfLineAndBEGIN_INITIAL );
}

<DiscardRestOfLineAndBEGIN_INITIAL>.* {
	BEGIN( INITIAL );
}
<DiscardRestOfLineAndBEGIN_INITIAL>[\n] {
  endOfLine( );
	BEGIN( INITIAL );
}

<LaTeXOption>[^,\n]+ { Kernel::theTeXLabelManager.addDocumentOption( shapeslloc, yytext ); }
<LaTeXOption>[\n] {
  endOfLine( );
	BEGIN( INITIAL );
}

<LaTeXClass>.+ { Kernel::theTeXLabelManager.setDocumentClass( shapeslloc, yytext ); }
<LaTeXClass>[\n] {
  endOfLine( );
	BEGIN( INITIAL );
}

<LaTeXPreamble>.* { Kernel::theTeXLabelManager.addPreambleLine( shapeslloc, yytext ); }
<LaTeXPreamble>[\n] {
  endOfLine( );
	BEGIN( INITIAL );
}

<LaTeXDocumentTop>.* { Kernel::theTeXLabelManager.addDocumentTopLine( shapeslloc, yytext ); }
<LaTeXDocumentTop>[\n] {
  endOfLine( );
	BEGIN( INITIAL );
}

<RandSeed>.* {
	char * end;
	long int s = strtol( yytext, & end, 10 );
	if( *end != '\0' )
		{
			Ast::theAnalysisErrorsList.push_back( new Exceptions::ScannerError( shapeslloc, strrefdup( "Failed to scan integer." ) ) );
		}
	else
		{
			if( s < 0 )
				{
					Ast::theAnalysisErrorsList.push_back( new Exceptions::ScannerError( shapeslloc, strrefdup( "Random nuber generator seed must not be negative." ) ) );
				}
			else
				{
					srand( s );
				}
		}
}
<RandSeed>[\n] {
  endOfLine( );
	BEGIN( INITIAL );
}

<NewUnitName>{SimpleIdentifier} {
	newUnitName_ = strdup( yytext );
	BEGIN( NewUnitEqual );
}
<NewUnitName>. {
	Ast::theAnalysisErrorsList.push_back( new Exceptions::ScannerError( shapeslloc, strrefdup( "Expected identifier." ) ) );
	BEGIN( DiscardRestOfLineAndBEGIN_INITIAL );
}
<NewUnitEqual>[ \t]*"="[ \t]* { BEGIN( NewUnitValue ); }
<NewUnitEqual>. {
	Ast::theAnalysisErrorsList.push_back( new Exceptions::ScannerError( shapeslloc, strrefdup( "Expected '='." ) ) );
	BEGIN( DiscardRestOfLineAndBEGIN_INITIAL );
 }
<NewUnitValue>{Float}{SimpleIdentifier} {
	char * end;
	double val = shapes_strtod( yytext, & end );

	typedef typeof lengthUnits_ MapType;
	MapType::const_iterator i;
	if( ( i = lengthUnits_.find( end ) ) != lengthUnits_.end( ) )
		{
			double newUnitValue = val * i->second;

			if( ( i = lengthUnits_.find( newUnitName_ ) ) != lengthUnits_.end( ) )
				{
					if( i->second != newUnitValue )
						{
							Ast::theAnalysisErrorsList.push_back( new Exceptions::IntroducingExistingUnit( shapeslloc, strrefdup( newUnitName_ ) ) );
						}
				}
			else if( ( i = angleUnits_.find( newUnitName_ ) ) != angleUnits_.end( ) )
				{
					Ast::theAnalysisErrorsList.push_back( new Exceptions::IntroducingExistingUnit( shapeslloc, strrefdup( newUnitName_ ) ) );
				}
			else
				{
					lengthUnits_[ newUnitName_ ] = newUnitValue;
				}
		}
	else if( ( i = angleUnits_.find( end ) ) != angleUnits_.end( ) )
		{
			double newUnitValue = val * i->second;

			if( ( i = angleUnits_.find( newUnitName_ ) ) != angleUnits_.end( ) )
				{
					if( i->second != newUnitValue )
						{
							Ast::theAnalysisErrorsList.push_back( new Exceptions::IntroducingExistingUnit( shapeslloc, strrefdup( newUnitName_ ) ) );
						}
				}
			else if( ( i = lengthUnits_.find( newUnitName_ ) ) != lengthUnits_.end( ) )
				{
					Ast::theAnalysisErrorsList.push_back( new Exceptions::IntroducingExistingUnit( shapeslloc, strrefdup( newUnitName_ ) ) );
				}
			else
				{
					angleUnits_[ newUnitName_ ] = newUnitValue;
				}
		}
	else
		{
			Ast::theAnalysisErrorsList.push_back( new Exceptions::LookupUnknownUnit( shapeslloc, strrefdup( end ) ) );
		}

	BEGIN( INITIAL );
}
<NewUnitValue>{Float}"°" {
	char * end;
	double val = shapes_strtod( yytext, & end );

	double newUnitValue = val * ( M_PI / 180 );

	typedef typeof lengthUnits_ MapType;
	MapType::const_iterator i;

	if( ( i = angleUnits_.find( newUnitName_ ) ) != angleUnits_.end( ) )
		{
			if( i->second != newUnitValue )
				{
					Ast::theAnalysisErrorsList.push_back( new Exceptions::IntroducingExistingUnit( shapeslloc, strrefdup( newUnitName_ ) ) );
				}
		}
	else if( ( i = lengthUnits_.find( newUnitName_ ) ) != lengthUnits_.end( ) )
		{
			Ast::theAnalysisErrorsList.push_back( new Exceptions::IntroducingExistingUnit( shapeslloc, strrefdup( newUnitName_ ) ) );
		}
	else
		{
			angleUnits_[ newUnitName_ ] = newUnitValue;
		}

	BEGIN( INITIAL );
}
<NewUnitValue>. {
	Ast::theAnalysisErrorsList.push_back( new Exceptions::ScannerError( shapeslloc, strrefdup( "Expected a length." ) ) );
	BEGIN( DiscardRestOfLineAndBEGIN_INITIAL );
}


{Float}"rad" {
	char * end;
	shapeslval.floatVal = shapes_strtod( yytext, & end );
	return T_float;
}

{Float}"°" |
{Float}"deg" {
	char * end;
	shapeslval.floatVal = M_PI / 180 * shapes_strtod( yytext, & end );
	return T_float;
}

{Float}"grad" {
	char * end;
	shapeslval.floatVal = M_PI / 200 * shapes_strtod( yytext, & end );
	return T_float;
}

{Float}"bp" {
	char * end;
	shapeslval.floatVal = shapes_strtod( yytext, & end );
	return T_length;
}

{Float}"mm" {
	char * end;
	shapeslval.floatVal = shapes_strtod( yytext, & end ) * ( 0.1 * 72 / 2.54 );
	return T_length;
}

{Float}"cm" {
	char * end;
	shapeslval.floatVal = shapes_strtod( yytext, & end ) * ( 72 / 2.54 );
	return T_length;
}

{Float}"m" {
	char * end;
	shapeslval.floatVal = shapes_strtod( yytext, & end ) * ( 100 * 72 / 2.54 );
	return T_length;
}

{Float}"in" {
	char * end;
	shapeslval.floatVal = shapes_strtod( yytext, & end ) * 72;
	return T_length;
}

{Float}{SimpleIdentifier} {
	char * end;
	double val = shapes_strtod( yytext, & end );

	typedef typeof lengthUnits_ MapType;
	MapType::const_iterator i;
	if( ( i = lengthUnits_.find( end ) ) != lengthUnits_.end( ) )
		{
			shapeslval.floatVal = val * i->second;
			return T_length;
		}
	if( ( i = angleUnits_.find( end ) ) != angleUnits_.end( ) )
		{
			shapeslval.floatVal = val * i->second;
			return T_float;
		}

	Ast::theAnalysisErrorsList.push_back( new Exceptions::LookupUnknownUnit( shapeslloc, strrefdup( end ) ) );
	return T_length; /* Just a guess, it won't really matter since things have gone wrong anyway. */
}

{Float}[\%][D0] {
	char * end;
	double val = shapes_strtod( yytext, & end );
	shapeslval.expr = new Ast::SpecialLength( shapeslloc, val, Computation::SPECIALU_DIST );
	return T_speciallength;
}

{Float}[\%][C1] {
	char * end;
	double val = shapes_strtod( yytext, & end );
	shapeslval.expr = new Ast::SpecialLength( shapeslloc, val, Computation::SPECIALU_DIST | Computation::SPECIALU_CIRC );
	return T_speciallength;
}

{Float}[\%][M2] {
	char * end;
	double val = shapes_strtod( yytext, & end );
	shapeslval.expr = new Ast::SpecialLength( shapeslloc, val, Computation::SPECIALU_DIST | Computation::SPECIALU_CORR );
	return T_speciallength;
}

{Float}[\%][F3] {
	char * end;
	double val = shapes_strtod( yytext, & end );
	shapeslval.expr = new Ast::SpecialLength( shapeslloc, val, Computation::SPECIALU_DIST | Computation::SPECIALU_CIRC | Computation::SPECIALU_CORR );
	return T_speciallength;
}

{Float}[\%][d4] {
	char * end;
	double val = shapes_strtod( yytext, & end );
	shapeslval.expr = new Ast::SpecialLength( shapeslloc, val, Computation::SPECIALU_DIST | Computation::SPECIALU_NOINFLEX );
	return T_speciallength;
}

{Float}[\%][c5] {
	char * end;
	double val = shapes_strtod( yytext, & end );
	shapeslval.expr = new Ast::SpecialLength( shapeslloc, val, Computation::SPECIALU_DIST | Computation::SPECIALU_CIRC | Computation::SPECIALU_NOINFLEX );
	return T_speciallength;
}

{Float}[\%][m6] {
	char * end;
	double val = shapes_strtod( yytext, & end );
	shapeslval.expr = new Ast::SpecialLength( shapeslloc, val, Computation::SPECIALU_DIST | Computation::SPECIALU_CORR | Computation::SPECIALU_NOINFLEX );
	return T_speciallength;
}

{Float}[\%][f7] {
	char * end;
	double val = shapes_strtod( yytext, & end );
	shapeslval.expr = new Ast::SpecialLength( shapeslloc, val, Computation::SPECIALU_DIST | Computation::SPECIALU_CIRC | Computation::SPECIALU_CORR | Computation::SPECIALU_NOINFLEX );
	return T_speciallength;
}

{Float}[\%][i9] {
	char * end;
	double val = shapes_strtod( yytext, & end );
	shapeslval.expr = new Ast::SpecialLength( shapeslloc, val, Computation::SPECIALU_NOINFLEX );
	return T_speciallength;
}


{Float} {
	char * end;
	shapeslval.floatVal = shapes_strtod( yytext, & end );
	return T_float;
}

"\'"[~]?[0-9]+ {
	const char * src = yytext + 1;
	bool negative = false;
	if( *src == '~' )
		{
			negative = true;
			++src;
		}
	char * end;
	int absval = strtol( src, & end, 10 );
	if( negative )
		{
			shapeslval.intVal = - absval;
		}
	else
		{
			shapeslval.intVal = absval;
		}
	return T_int;
}

"\'0x"[0-9A-F]+ {
	char * end;
	shapeslval.intVal = strtol( yytext + 3, & end, 16 );
	return T_int;
}

"\'0b"[0-1]+ {
	char * end;
	shapeslval.intVal = strtol( yytext + 3, & end, 2 );
	return T_int;
}

"\'\""{UTF8char} {
	const char * src = yytext + 2;
	if( *src >= 0 )
		{
			shapeslval.uintVal = *src;
		}
	else
		{
			switch( 0xF0 & *src )
				{
				case 0xE0:
					shapeslval.uintVal = *src & 0x0F;
					shapeslval.uintVal *= 0x40;
					++src;
					shapeslval.uintVal += *src & 0x3F;
					shapeslval.uintVal *= 0x40;
					++src;
					shapeslval.uintVal += *src & 0x3F;
					break;
				case 0xF0:
					shapeslval.uintVal = *src & 0x07;
					shapeslval.uintVal *= 0x40;
					++src;
					shapeslval.uintVal += *src & 0x3F;
					shapeslval.uintVal *= 0x40;
					++src;
					shapeslval.uintVal += *src & 0x3F;
					shapeslval.uintVal *= 0x40;
					++src;
					shapeslval.uintVal += *src & 0x3F;
					break;
				default:
					shapeslval.uintVal = *src & 0x1F;
					shapeslval.uintVal *= 0x40;
					++src;
					shapeslval.uintVal += *src & 0x3F;
					break;
				}
		}
	return T_char;
}

"\'&U+"[0-9A-F]+";" {
	char * end;
	int absval = strtol( yytext + 4, & end, 16 );
	if( *end != ';' )
		{
			Ast::theAnalysisErrorsList.push_back( new Exceptions::ScannerError( shapeslloc, strrefdup( "Malformed hexadecimal number in character literal." ) ) );
		}
	shapeslval.uintVal = absval;
	return T_char;
}

"\'&G+"{SimpleIdentifier}";" {
	yytext[ yyleng - 1 ] = '\0'; /* Cut away the terminating ';' */
	shapeslval.uintVal = unicodeFromGlyphname( yytext + 4 );
	return T_char;
}

"∞" {
	shapeslval.floatVal = HUGE_VAL;
	return T_float;
}

"true" {
	shapeslval.boolVal = true;
	return T_bool;
}

"false" {
	shapeslval.boolVal = false;
	return T_bool;
}

"%last" {
	return T_last;
}

"--" { return T_minusminus; }
"++" { return T_plusplus; }
"..." { return T_dddot; }

"::" { return T_declaretype; }
":=" { return T_assign; }
"=" { return T_eqeq; }
"/="|"≠" { return T_eqneq; }

"->"|"→" { return T_mapsto; }
"../" { return T_surrounding; }
"[]" { return T_emptybrackets; }
"[...]" { return T_dddotbrackets; }
"()"|"⊙" { return T_compose; }

"(>" { return T_unionLeft; }
"<)" { return T_unionRight; }
"<>" { return T_split; }
"(<" { return T_splitLeft; }
">)" { return T_splitRight; }
"(|" { return T_absLeft; }
"|)" { return T_absRight; }

"(<)" { return T_paren_less; }
"(>)" { return T_paren_greater; }
"(<=)" { return T_paren_lesseq; }
"(>=)" { return T_paren_greatereq; }

[\(\)\[\]\<\>] { return yytext[0]; }
[\.\,\;\:\_\@\!\#\&\|\^\-\+\'\"\\] { return yytext[0]; }
[*/~=] { return yytext[0]; }

[\{] {
	namespaceLimits_.push( searchContext_->lexicalPath( )->size( ) );
	++bracketDepth_;
	return yytext[0];
}
[\}] {
	/* The closing brace is inbalanced if the namespaceLimits_ stack is empty,
	 * but we leave the reporting of that error to the parser and just treat
	 * the balanced case here.
	 */
	if( ! namespaceLimits_.empty( ) )
		namespaceLimits_.pop( );
	/* Likewise, we let the parser report the error in case of an unbalanced closing brace.
	 */
	if( bracketDepth_ > 0 ){
		--bracketDepth_;
	}
	return yytext[0];
}

"*/"|"∥" { return T_projection; }
"/_"|"∠" { return T_angle; }
"&|" { return T_ampersandMore; }

"<="|"≤" { return T_lesseq; }
">="|"≥" { return T_greatereq; }

"<<"|"≪" { return T_llthan; }
">>"|"≫" { return T_ggthan; }

"@@" {
	/* This is a bit of a hack.  The parser needs to know the namespace
	 * at this point, so we embedd this information in a placed identifier.
	 */
	shapeslval.placedIdent = placedIdentifier( yytext );
	return T_atat;
}

"and"|"⋀" { return T_and; }
"or"|"⋁" { return T_or; }
"xor"|"⊻" { return T_xor; }
"not"|"¬" { return T_not; }

"dynamic" { return T_dynamic; }

"cycle" { return T_cycle; }

"indexof" { return T_indexof; }
"depthof" { return T_depthof; }
"VARNAME" { return T_variableName; }
"resolved_identifier_string" { return T_absrefof; }

"continuation" { return T_continuation; /* Reserved for future use */ }
"continue" { return T_continue; /* Reserved for future use */ }
"escape_continuation" { return T_esc_continuation; }
"escape_continue" { return T_esc_continue; }
"escape_backtrace" { return T_esc_backtrace; }

"freeze" { return T_freeze; }

"class" { return T_class; }
"__members__" { return T_members; }
"__prepare__" { return T_prepare; }
"__abstract__" { return T_abstract; }
"__overrides<" { return T_overrides; }
">__" { return T_gr__; }

{WhiteSpace}+ ;
[\t]+ {
	Ast::theAnalysisErrorsList.push_back( new Exceptions::ScannerError( shapeslloc, strrefdup( "Illegal use of reserved tab character." ) ) );
}

[\n] {
  endOfLine( );
}

<INITIAL>"|**".*[\n] { ++shapeslloc.lastLine; shapeslloc.lastColumn = 0; }
<INITIAL>"/**" { quoteDepth_ = 1; BEGIN( Comment ); }
<INITIAL>"**/" { throw Exceptions::ScannerError( shapeslloc, strrefdup( "Found closing comment delimiter outside comment." ) ); }
<Comment>"/**" { ++quoteDepth_; more( ); }
<Comment>"**/" {
	--quoteDepth_;
	if( quoteDepth_ == 0 )
		{
			BEGIN( INITIAL );
		}
	else
		{
			more( );
		}
}
<Comment>. { }
<Comment>[\n] { ++shapeslloc.lastLine; shapeslloc.lastColumn = 0; }
<Comment><<EOF>> {
	/* It seems like YY_USER_ACTION is not invoked at EOF, so we do this manually,
	 * however ignornig yyleng (which has the value 1).
	 */
	shapeslloc.firstColumn = shapeslloc.lastColumn;
	throw Exceptions::ScannerError( shapeslloc, strrefdup( "Found EOF while scanning comment." ) );
 }

<INITIAL>[`][\n]? {
	if( yyleng > 1 )
		{
			++shapeslloc.lastLine;
			shapeslloc.lastColumn = 0;
		}
	quoteDepth_ = 1;
	BEGIN( String );
}
<INITIAL>"´" { throw Exceptions::ScannerError( shapeslloc, strrefdup( "Found closing quote outside string." ) ); }
<String>"`" { ++quoteDepth_; more( ); }
<String>"´" {
	--quoteDepth_;
	if( quoteDepth_ == 0 )
		{
			yytext[ yyleng - 2 ] = '\0';
			rinseString( );
			--shapeslloc.firstColumn;
			BEGIN( INITIAL );
			return T_string;
		}
	else
		{
			more( );
			yymore( ); /* The purpose of this line is only to let flex know that we use yy_more_flag */
		}
}

<String,PoorMansString>[\n] { ++shapeslloc.lastLine; shapeslloc.lastColumn = 0; more( ); }
<String,PoorMansString>. { more( ); }
<String,PoorMansString><<EOF>> {
	/* It seems like YY_USER_ACTION is not invoked at EOF, so we do this manually,
	 * however ignornig yyleng (which has the value 1).
	 */
	shapeslloc.firstColumn = shapeslloc.lastColumn;
	throw Exceptions::ScannerError( shapeslloc, strrefdup( "Found EOF while scanning string." ) );
 }

<INITIAL>"(\""[\n]? {
	if( yyleng > 2 )
		{
			++shapeslloc.lastLine;
			shapeslloc.lastColumn = 0;
		}
	quoteDepth_ = 1;
	BEGIN( PoorMansString );
}
<INITIAL>"\")" { throw Exceptions::ScannerError( shapeslloc, strrefdup( "Found closing poor man's quote outside string." ) ); }
<PoorMansString>"\")" {
	yytext[ yyleng - 2 ] = '\0';
	rinseString( );
	--shapeslloc.firstColumn;
	BEGIN( INITIAL );
	return T_string;
}

<INITIAL>"\"{" {
	while( ! dataStringChunks_.empty( ) )
		{
			delete dataStringChunks_.back( ).first;
			dataStringChunks_.pop_back( );
		}
	dataStringTotalLength_ = 0;
	BEGIN( DataStringHex );
}
<DataStringPlain,DataStringHex>[\n] { ++shapeslloc.lastLine; shapeslloc.lastColumn = 0; }
<DataStringPlain>[ -z]+ {
	dataStringChunks_.push_back( std::pair< char *, size_t >( strdup( yytext ), yyleng ) );
	dataStringTotalLength_ += yyleng;
}
<DataStringHex>[ \t]+ { }
<DataStringHex>(([A-F0-9]{2})|[a-z])+ {
	char * res = new char[ yyleng + 1 ];
	char * dst = res;
	for( const char * src = yytext; *src != '\0'; ++dst )
		{
			if( 'a' <= *src && *src <= 'z' )
				{
					switch( *src )
						{
						case 'n':
							*dst = '\n';
							break;
						case 't':
							*dst = '\t';
							break;
						default:
							*dst = '\0';
							Ast::theAnalysisErrorsList.push_back( new Exceptions::ScannerError( shapeslloc, strrefdup( std::string( "Invalid character name in escape mode: " ) + *src ) ) );
						}
					src += 1;
				}
			else
				{
					*dst = shapes_hexToChar( src[0], src[1] );
					 src += 2;
				}
		}
	dataStringChunks_.push_back( std::pair< char *, size_t >( res, dst - res ) );
	dataStringTotalLength_ += dst - res;
}
<DataStringHex>[{] { BEGIN( DataStringPlain ); }
<DataStringPlain>[}] { BEGIN( DataStringHex ); }
<DataStringHex>[}] {
	concatenateDataString( );
	BEGIN( INITIAL );
	return T_string;
}
<DataStringPlain,DataStringHex>. {
	throw Exceptions::ScannerError( shapeslloc, strrefdup( "Stray character in \"{...} string." ) );
}

<Incl>[^ \t\n]+ {
	if( searchContext_->insidePrivateNamespace( ) ){
		/* Including files from within a private namespace is forbidden to ensure that it will be possible to enter new namespaces
		 * in the included file.
		 * Report error, but still do the inclusion to avoid introducing additional errors.
		 */
		Ast::theAnalysisErrorsList.push_back( new Exceptions::ScannerError( shapeslloc, strrefdup( "Illegal to include files from within private namespace." ) ) );
	}
	currentNeedFile = yytext;
	currentNeedPushCount = 0;
	currentNeedIsNeed = false;
	currentNeedHasNamespace = false;
	BEGIN( InclFrom );
}

<Needs>[^ \t\n]+".shext" {
	yytext[ yyleng - 6 ] = '\0'; /* Drop the filename suffix. */
	if( searchContext_->insidePrivateNamespace( ) ){
		/* See comment for <Incl>.
		 */
		Ast::theAnalysisErrorsList.push_back( new Exceptions::ScannerError( shapeslloc, strrefdup( "Illegal to include files from within private namespace." ) ) );
	}
	if( bracketDepth_ > 0 ){
		/* Report error, but still do the inclusion to avoid introducing additional errors.
		 */
		Ast::theAnalysisErrorsList.push_back( new Exceptions::ScannerError( shapeslloc, strrefdup( "##needs is only allowed in the global scope." ) ) );
	}

	currentNeedFile = yytext;
	currentNeedPushCount = 0;
	currentNeedIsNeed = true;
	currentNeedHasNamespace = false;
	BEGIN( InclFrom );
}
<Needs>{NonEmptyNamespacePath} {
	setNamespace( yytext );
	if( searchContext_->insidePrivateNamespace( ) ){
		/* See comment for <Incl>.
		 */
		Ast::theAnalysisErrorsList.push_back( new Exceptions::ScannerError( shapeslloc, strrefdup( "Illegal to include files from within private namespace." ) ) );
	}
	if( bracketDepth_ > 0 ){
		/* Report error, but still do the inclusion to avoid introducing additional errors.
		 */
		Ast::theAnalysisErrorsList.push_back( new Exceptions::ScannerError( shapeslloc, strrefdup( "##needs is only allowed in the global scope." ) ) );
	}

	currentNeedPushCount = 0;
	currentNeedIsNeed = true;
	currentNeedHasNamespace = true;
	currentNeedFile = "";
	BEGIN( InclOptFile );
}
<Needs>"/"[ \t]* {
	namespace_ = Ast::NamespaceReference( Ast::NamespaceReference::RELATIVE, Ast::THE_EMPTY_NAMESPACE_PATH );
	if( searchContext_->insidePrivateNamespace( ) ){
		/* See comment for <Incl>.
		 */
		Ast::theAnalysisErrorsList.push_back( new Exceptions::ScannerError( shapeslloc, strrefdup( "Illegal to include files from within private namespace." ) ) );
	}
	if( bracketDepth_ > 0 ){
		/* Report error, but still do the inclusion to avoid introducing additional errors.
		 */
		Ast::theAnalysisErrorsList.push_back( new Exceptions::ScannerError( shapeslloc, strrefdup( "##needs is only allowed in the global scope." ) ) );
	}

	currentNeedPushCount = 0;
	currentNeedIsNeed = true;
	currentNeedHasNamespace = true;
	currentNeedFile = "";
	BEGIN( InclFile );
}
<Needs>. {
	Ast::theAnalysisErrorsList.push_back( new Exceptions::ScannerError( shapeslloc, strrefdup( "Expected namespace name or filename with suffix \".shext\"." ) ) );
	BEGIN( DiscardRestOfLineAndBEGIN_INITIAL );
}

<InclOptFile>[ \t]+ { }
<InclOptFile>[\n] { doInclusion( ); }
<InclOptFile>"/"[ \t]* { BEGIN( InclFile ); }
<InclOptFile>":"[ \t]+ { BEGIN( InclPath ); }
<InclOptFile>":". { throw Exceptions::ScannerError( shapeslloc, strrefdup( "The \":\" must be followed by whitespace." ) ); }
<InclOptFile>. { throw Exceptions::ScannerError( shapeslloc, strrefdup( "Expected \"/\" or \":\"." ) ); }

<InclFile>[^ \t\n]+ { currentNeedFile = yytext; BEGIN( InclFrom ); }
<InclFile>.|[n] { throw Exceptions::ScannerError( shapeslloc, strrefdup( "Expected filename base after \"/\"." ) ); }

<InclFrom>[ \t]+ { }
<InclFrom>[\n] { doInclusion( ); }
<InclFrom>":"[ \t]+ { BEGIN( InclPath ); }
<InclFrom>":". { throw Exceptions::ScannerError( shapeslloc, strrefdup( "The \":\" must be followed by whitespace." ) ); }
<InclFrom>. { throw Exceptions::ScannerError( shapeslloc, strrefdup( "Expected \":\"." ) ); }

<InclPath>.+ {
	push_frontNeedPath( yytext );
	++currentNeedPushCount;
}
<InclPath>[\n] {
	if( currentNeedPushCount == 0 )
		{
			throw Exceptions::ScannerError( shapeslloc, strrefdup( "Missing path after \":\"." ) );
		}
	doInclusion( );
}

<Echo>.* {
	std::cerr << yytext << std::endl ;
	BEGIN( INITIAL );
}

<Author>.* {
	if( ! stateStack_.empty( ) )
		{
			Kernel::theDocInfo.addExtensionAuthorString( shapeslloc.file_->name( ) + std::string( " by " ) + yytext );
		}
	else
		{
			if( ! Kernel::theDocInfo.addInfo( "Author", SimplePDF::newString( yytext ) ) )
				{
					Ast::theAnalysisErrorsList.push_back( new Exceptions::ScannerError( shapeslloc, strrefdup( "Multiply specified ##author." ) ) );
				}
		}
	BEGIN( INITIAL );
}

<Push>{NonEmptyNamespacePath} {
	setNamespace( yytext );
	if( namespace_.base( ) != Ast::NamespaceReference::RELATIVE ){
		Ast::theAnalysisErrorsList.push_back( new Exceptions::ScannerError( shapeslloc, strrefdup( "Pushed namespace path must be relative." ) ) );
	}else if( searchContext_->insidePrivateNamespace( ) ){
		Ast::theAnalysisErrorsList.push_back( new Exceptions::ScannerError( shapeslloc, strrefdup( "Cannot push inside private namespace." ) ) );
	}else{
		searchContextStack_.push( searchContext_ );
		Ast::NamespacePath * newNamespacePrefixMem = new Ast::NamespacePath( *searchContext_->lexicalPath( ) );
		for( Shapes::Ast::NamespacePath::const_iterator i = namespace_.path( ).begin( ); i != namespace_.path( ).end( ); ++i ){
			newNamespacePrefixMem->push_back( strdup( *i ) );
		}
		RefCountPtr< const Ast::NamespacePath > newNamespacePrefix( newNamespacePrefixMem );
		RefCountPtr< const char > privateName( Ast::SearchContext::makePrivateName( uniqueNamespaceNumber_ ) );
		++uniqueNamespaceNumber_;
		searchContext_ = RefCountPtr< const Ast::SearchContext >( new Ast::SearchContext( newNamespacePrefix, searchContext_->encapsulationPath( ), searchContext_->lookinStack( ), privateName ) );
	}
	BEGIN( INITIAL );
	return T_namespaceSpecial;
}
<Push>{Encapsulation} {
	if( searchContext_->insidePrivateNamespace( ) ){
		Ast::theAnalysisErrorsList.push_back( new Exceptions::ScannerError( shapeslloc, strrefdup( "Cannot push inside private namespace." ) ) );
	}else{
		searchContextStack_.push( searchContext_ );
		RefCountPtr< const char > encapsulationName( newEncapsulationName( ) );
		RefCountPtr< const char > privateName( newPrivateName( ) );
		Ast::NamespacePath * newNamespacePrefixMem = new Ast::NamespacePath( *searchContext_->lexicalPath( ) );
		newNamespacePrefixMem->push_back( strdup( encapsulationName.getPtr( ) ) );
		RefCountPtr< const Ast::NamespacePath > newNamespacePrefix( newNamespacePrefixMem );
		searchContext_ = RefCountPtr< const Ast::SearchContext >( new Ast::SearchContext( newNamespacePrefix, newNamespacePrefix, searchContext_->lookinStack( ), privateName ) );
	}
	BEGIN( INITIAL );
	return T_namespaceSpecial;
}
<Push>"-" {
	if( searchContext_->insidePrivateNamespace( ) ){
		Ast::theAnalysisErrorsList.push_back( new Exceptions::ScannerError( shapeslloc, strrefdup( "Cannot push inside private namespace." ) ) );
	}else{
		searchContextStack_.push( searchContext_ );
		Ast::NamespacePath * newNamespacePrefixMem = new Ast::NamespacePath( *searchContext_->lexicalPath( ) );
		newNamespacePrefixMem->push_back( strdup( searchContext_->privateName( ).getPtr( ) ) );
		RefCountPtr< const Ast::NamespacePath > newNamespacePrefix( newNamespacePrefixMem );
		searchContext_ = RefCountPtr< const Ast::SearchContext >( new Ast::SearchContext( newNamespacePrefix, searchContext_->encapsulationPath( ), searchContext_->lookinStack( ), RefCountPtr< const char >( NullPtr< const char >( ) ) ) );
	}
	BEGIN( INITIAL );
	return T_namespaceSpecial;
}
<Push>. {
	Ast::theAnalysisErrorsList.push_back( new Exceptions::ScannerError( shapeslloc, strrefdup( "Expected namespace name or \"-\"." ) ) );
	BEGIN( DiscardRestOfLineAndBEGIN_INITIAL );
}

<Pop>{NonEmptyNamespacePath} {
	setNamespace( yytext );
	RefCountPtr< const Ast::NamespacePath > lexicalPath = searchContext_->lexicalPath( );
	if( namespace_.base( ) != Ast::NamespaceReference::RELATIVE ){
		Ast::theAnalysisErrorsList.push_back( new Exceptions::ScannerError( shapeslloc, strrefdup( "Popped namespace path must be relative." ) ) );
	}else if( lexicalPath->size( ) < namespace_.path( ).size( ) ){
		Ast::theAnalysisErrorsList.push_back( new Exceptions::ScannerError( shapeslloc, strrefdup( "Popping more levels than current depth of namespace stack." ) ) );
	}else if( lexicalPath->size( ) == namespaceLimits_.top( ) ){
		Ast::theAnalysisErrorsList.push_back( new Exceptions::ScannerError( shapeslloc, strrefdup( "Illegal to pop externally defined namespace stack." ) ) );
	}else if( searchContext_->insidePrivateNamespace( ) ){
		Ast::theAnalysisErrorsList.push_back( new Exceptions::ScannerError( shapeslloc, strrefdup( "Mismatched pop of named namespace; expected pop of private namespace." ) ) );
	}else if( searchContext_->insideEncapsulationNamespace( ) ){
		Ast::theAnalysisErrorsList.push_back( new Exceptions::ScannerError( shapeslloc, strrefdup( "Mismatched pop of named namespace; expected pop of encapsulation namespace." ) ) );
	}else{
		bool match = true;
		Ast::NamespacePath::const_reverse_iterator j = lexicalPath->rbegin( );
		for( Shapes::Ast::NamespacePath::const_reverse_iterator i = namespace_.path( ).rbegin( ); i != namespace_.path( ).rend( ); ++i, ++j ){
			if( strcmp( *j, *i ) != 0 ){
				match = false;
				break;
			}
		}
		if( ! match ){
			std::stack< const char * > revStack;
			Ast::NamespacePath::const_reverse_iterator j = lexicalPath->rbegin( );
			for( size_t i = 0; i < namespace_.path( ).size( ); ++i, ++j ){
				revStack.push( *j );
			}
			std::ostringstream oss;
			oss << "Namespace stack mismatched pop; expected " ;
			bool first = true;
			while( ! revStack.empty( ) ){
				if( first ){
					first = false;
				}else{
					oss << Interaction::NAMESPACE_SEPARATOR ;
				}
				oss << revStack.top( ) ;
				revStack.pop( );
			}
			Ast::theAnalysisErrorsList.push_back( new Exceptions::ScannerError( shapeslloc, strrefdup( oss ) ) );
		}
		size_t expectedDepth = lexicalPath->size( ) - namespace_.path( ).size( );
		searchContext_ = searchContextStack_.top( );
		searchContextStack_.pop( );
		if( searchContext_->lexicalPath( )->size( ) != expectedDepth ){
			std::ostringstream oss;
			oss << "Expected pop of " << ( lexicalPath->size( ) - searchContext_->lexicalPath( )->size( ) ) << " namespace levels, not " << namespace_.path( ).size( ) << "." ;
			Ast::theAnalysisErrorsList.push_back( new Exceptions::ScannerError( shapeslloc, strrefdup( oss ) ) );
		}
	}
	BEGIN( INITIAL );
	return T_namespaceSpecial;
}
<Pop>{Encapsulation} {
	RefCountPtr< const Ast::NamespacePath > lexicalPath = searchContext_->lexicalPath( );
	if( lexicalPath->size( ) == namespaceLimits_.top( ) ){
		Ast::theAnalysisErrorsList.push_back( new Exceptions::ScannerError( shapeslloc, strrefdup( "Illegal to pop externally defined namespace stack." ) ) );
	}else if( searchContext_->insidePrivateNamespace( ) ){
		Ast::theAnalysisErrorsList.push_back( new Exceptions::ScannerError( shapeslloc, strrefdup( "Mismatched pop of encapsulation namespace; expected pop of private namespace." ) ) );
	}else if( ! searchContext_->insideEncapsulationNamespace( ) ){
		Ast::theAnalysisErrorsList.push_back( new Exceptions::ScannerError( shapeslloc, strrefdup( "Mismatched pop of encapsulation namespace; expected pop of named namespace." ) ) );
	}else{
		searchContext_ = searchContextStack_.top( );
		searchContextStack_.pop( );
	}
	BEGIN( INITIAL );
	return T_namespaceSpecial;
}
<Pop>"-" {
	RefCountPtr< const Ast::NamespacePath > lexicalPath = searchContext_->lexicalPath( );
	if( lexicalPath->size( ) == namespaceLimits_.top( ) ){
		Ast::theAnalysisErrorsList.push_back( new Exceptions::ScannerError( shapeslloc, strrefdup( "Illegal to pop externally defined namespace stack." ) ) );
	}else if( searchContext_->insideEncapsulationNamespace( ) ){
		Ast::theAnalysisErrorsList.push_back( new Exceptions::ScannerError( shapeslloc, strrefdup( "Mismatched pop of private namespace; expected pop of encapsulation namespace." ) ) );
	}else if( ! searchContext_->insidePrivateNamespace( ) ){
		Ast::theAnalysisErrorsList.push_back( new Exceptions::ScannerError( shapeslloc, strrefdup( "Mismatched pop of private namespace; expected pop of named namespace." ) ) );
	}else{
		searchContext_ = searchContextStack_.top( );
		searchContextStack_.pop( );
	}
	BEGIN( INITIAL );
	return T_namespaceSpecial;
}
<Pop>. {
	Ast::theAnalysisErrorsList.push_back( new Exceptions::ScannerError( shapeslloc, strrefdup( "Expected namespace name or \"-\"." ) ) );
	BEGIN( DiscardRestOfLineAndBEGIN_INITIAL );
}

<Lookin>{NonEmptyNamespacePath} {
	setNamespace( yytext );
	if( namespace_.path( ).empty( ) ){
		Ast::theAnalysisErrorsList.push_back( new Exceptions::ScannerError( shapeslloc, strrefdup( "Malformed namespace path." ) ) );
	}else{
		RefCountPtr< const Ast::LookinPathStack > newLookinStack( new Ast::LookinPathStack( resolvedNamespace( ), searchContext_->lookinStack( ) ) );
		searchContext_ = RefCountPtr< const Ast::SearchContext >( new Ast::SearchContext( searchContext_->lexicalPath( ), searchContext_->encapsulationPath( ), newLookinStack, searchContext_->privateName( ) ) );
	}
	BEGIN( INITIAL );
	return T_namespaceSpecial;
}
<Lookin>. {
	Ast::theAnalysisErrorsList.push_back( new Exceptions::ScannerError( shapeslloc, strrefdup( "Expected namespace path." ) ) );
	BEGIN( DiscardRestOfLineAndBEGIN_INITIAL );
}

<TreeNamespaceAlias>{SimpleIdentifier} {
	treeNamespaceAlias_ = placedIdentifier( yytext );
	aliasLoc_ = shapeslloc;
	BEGIN( TreeNamespaceEqual );
}
<TreeNamespaceAlias>. {
	Ast::theAnalysisErrorsList.push_back( new Exceptions::ScannerError( shapeslloc, strrefdup( "Expected namespace identifier." ) ) );
	BEGIN( DiscardRestOfLineAndBEGIN_INITIAL );
}
<TreeNamespaceEqual>[ \t]*"="[ \t]* { BEGIN( TreeNamespaceOriginal ); }
<TreeNamespaceEqual>. {
	Ast::theAnalysisErrorsList.push_back( new Exceptions::ScannerError( shapeslloc, strrefdup( "Expected '='." ) ) );
	BEGIN( DiscardRestOfLineAndBEGIN_INITIAL );
}
<TreeNamespaceOriginal>{NonEmptyNamespacePath} {
	setNamespace( yytext );
	shapeslval.node = new Ast::DefineAlias( aliasLoc_, shapeslloc, treeNamespaceAlias_, namespace_ );
	BEGIN( INITIAL );
	return T_alias;
}
<TreeNamespaceOriginal>. {
	Ast::theAnalysisErrorsList.push_back( new Exceptions::ScannerError( shapeslloc, strrefdup( "Expected namespace path." ) ) );
	BEGIN( DiscardRestOfLineAndBEGIN_INITIAL );
}

<<EOF>> {
  /* It seems like YY_USER_ACTION is not invoked at EOF, so we do this manually,
   * however ignornig yyleng (which has the value 1).
   */
  shapeslloc.firstColumn = shapeslloc.lastColumn;

  while( true ){

    if( stateStack_.size( ) > stateStackSizeStack_.top( ) ){
      yy_delete_buffer( YY_CURRENT_BUFFER );
      yy_switch_to_buffer( stateStack_.top( ) );
      stateStack_.pop( );
      shapeslloc.swap( locStack_.top( ) );
      delete locStack_.top( );
      locStack_.pop( );
      for( size_t tmp = pathCountStack_.top( ); tmp > 0; --tmp )
        {
          pop_frontNeedPath( );
        }
      pathCountStack_.pop( );
      namespaceLimits_.pop( );
      searchContext_ = searchContextStack_.top( );
      searchContextStack_.pop( );
      goto done;
    }

    while( loadStack_.size( ) > loadStackSizeStack_.top( ) )
      {
        const Ast::FileID *fileID = loadStack_.topFileID( );
        RefCountPtr< const Ast::NamespacePath > lexicalPath = loadStack_.topNamespace( );
        loadStack_.pop( );
        NeededFile neededFile( lexicalPath, fileID );
        if( neededFiles_.find( neededFile ) != neededFiles_.end( ) )
          continue;
        neededFiles_.insert( neededFile );
        std::ifstream * iFile = new std::ifstream( fileID->name( ) );
        if( ! iFile->good( ) )
          {
            delete iFile;
            throw Exceptions::FileReadOpenError( shapeslloc, strrefdup( fileID->name( ) ), NULL, NULL );
          }
        yy_delete_buffer( YY_CURRENT_BUFFER );
        yy_switch_to_buffer( yy_create_buffer( iFile, YY_BUF_SIZE ) );
        shapeslloc = Ast::SourceLocation( fileID );
        searchContext_ = RefCountPtr< const Ast::SearchContext >( new Ast::SearchContext( lexicalPath, Ast::THE_EMPTY_NAMESPACE_PATH, RefCountPtr< const Ast::LookinPathStack >( NullPtr< const Ast::LookinPathStack >( ) ), newPrivateName( ) ) );
        goto done;
      }

    if( loadStackSizeStack_.size( ) == 1 )
      break; /* Both size stacks have a zero as the bottom element, so both stateStack_ and loadStack_ arete be empty. */

    loadStackSizeStack_.pop( );
    stateStackSizeStack_.pop( );

   }

  if( ! yyinQueue_.empty( ) )
    {
      yy_delete_buffer( YY_CURRENT_BUFFER );
      yy_switch_to_buffer( yy_create_buffer( yyinQueue_.front( ).first, YY_BUF_SIZE ) );
      shapeslloc = Ast::SourceLocation( yyinQueue_.front( ).second );
      searchContext_ = RefCountPtr< const Ast::SearchContext >( new Ast::SearchContext( Ast::THE_EMPTY_NAMESPACE_PATH, Ast::THE_EMPTY_NAMESPACE_PATH, RefCountPtr< const Ast::LookinPathStack >( NullPtr< const Ast::LookinPathStack >( ) ), newPrivateName( ) ) );
      yyinQueue_.pop_front( );
      if( inPrelude_ )
        {
          inPrelude_ = false;
          if( interactive_ ){
            return T_interactiveMark;
          }else{
            return T_preludesep;
          }
        }
    }
  else if( inPrelude_ )
    {
      inPrelude_ = false;
      return T_preludesep;
    }
  else
    {
      return T_EOF;
    }

  done:
    ;
}

{NonEmptyNamespacePath}{NamespaceSep} {
	setNamespace( yytext );
	BEGIN( NamespacePrefix );
	namespace_yyleng_ = yyleng;
	more( );
}
{NamespaceSep} {
	setNamespace( yytext );
	BEGIN( NamespacePrefix );
	namespace_yyleng_ = yyleng;
	more( );
}
{Encapsulation} {
	setNamespace( yytext );
	BEGIN( NamespacePrefix );
	namespace_yyleng_ = yyleng;
	more( );
}

"TeX" {
	shapeslval.ident = simpleIdentifier( yytext );
	return T_identifier_tex;
}
{SimpleIdentifier} {
	shapeslval.ident = simpleIdentifier( yytext );
	return T_identifier_except_tex;
}
"'"{SimpleIdentifier} {
	shapeslval.char_p = strdup( yytext + 1 );
	return T_symbol;
}
<NamespacePrefix>{SimpleIdentifier} {
	shapeslval.ident = prependNamespace( yytext + namespace_yyleng_ );
	BEGIN( INITIAL );
	return T_pathed_identifier;
}

{TypeMark}{SimpleIdentifier} {
	shapeslval.ident = simpleIdentifier( yytext + 2 ); /* The type mark is allways 2 bytes. */
	return T_typename;
}
<NamespacePrefix>{TypeMark}{SimpleIdentifier} {
	shapeslval.ident = prependNamespace( yytext + namespace_yyleng_ + 2 ); /* The type mark is allways 2 bytes. */
	BEGIN( INITIAL );
	return T_typename;
}

{DynamicMark}{SimpleIdentifier} {
	shapeslval.ident = simpleIdentifier( yytext + 1 );
	return T_dynamic_identifier;
}
<NamespacePrefix>{DynamicMark}{SimpleIdentifier} {
	shapeslval.ident = prependNamespace( yytext + namespace_yyleng_ + 1 );
	BEGIN( INITIAL );
	return T_dynamic_identifier;
}

{StateMark}{SimpleIdentifier} {
	const char * id = yytext;
	/* Depending on which state mark is used, we must skip different number of bytes. */
	if( *id == '#' )
		{
			id += 1;
		}
	else /* The state mark is '•' */
		{
			id += 3;
		}
	shapeslval.ident = simpleIdentifier( id );
	return T_state_identifier;
}
<NamespacePrefix>{StateMark}{SimpleIdentifier} {
	char * id = yytext + namespace_yyleng_;
	/* Depending on which state mark is used, we must skip different number of bytes. */
	if( *id == '#' )
		{
			id += 1;
		}
	else /* The state mark is '•' */
		{
			id += 3;
		}
	shapeslval.ident = prependNamespace( id );
	BEGIN( INITIAL );
	return T_state_identifier;
}

{DynamicMark}{StateMark}{SimpleIdentifier} {
	const char * id = yytext + 1; /* The dynamic mark is allways 1 byte. */
	/* Depending on which state mark is used, we must skip different number of bytes. */
	if( *id == '#' )
		{
			id += 1;
		}
	else /* The state mark is '•' */
		{
			id += 3;
		}
	shapeslval.ident = simpleIdentifier( id );
	return T_dynamic_state_identifier;
}
<NamespacePrefix>{DynamicMark}{StateMark}{SimpleIdentifier} {
	char * id = yytext + namespace_yyleng_ + 1; /* The dynamic mark is allways 1 byte. */
	/* Depending on which state mark is used, we must skip different number of bytes. */
	if( *id == '#' )
		{
			id += 1;
		}
	else /* The state mark is '•' */
		{
			id += 3;
		}
	shapeslval.ident = prependNamespace( id );
	BEGIN( INITIAL );
	return T_dynamic_state_identifier;
}

<NamespacePrefix>. {
	Ast::theAnalysisErrorsList.push_back( new Exceptions::ScannerError( shapeslloc, strrefdup( "Expected identifier following namespace path." ) ) );
	BEGIN( DiscardRestOfLineAndBEGIN_INITIAL );
}

. {
	Ast::theAnalysisErrorsList.push_back( new Exceptions::ScannerError( shapeslloc, strrefdup( ( std::string( "Scanner found unrecognized token: " ) + yytext ).c_str( ) ) ) );
	BEGIN( DiscardRestOfLineAndBEGIN_INITIAL );
}

%%
/* The closing %% above marks the end of the Rules section and the beginning
 * of the User Subroutines section. All text from here to the end of the
 * file is copied verbatim to the end of the generated lex.pdf.c file.
 * This section is where you put definitions of helper functions.
 */

double
shapes_strtod( char * str, char ** end )
{
	char termTmp;
	char * term = str;
	for( ; *term != '\0'; ++term )
		{
			switch( *term )
				{
				case '0':
				case '1':
				case '2':
				case '3':
				case '4':
				case '5':
				case '6':
				case '7':
				case '8':
				case '9':
				case '.':
					continue;
				case '~':
					*term = '-';
					continue;
				case '*':
					/* We replace the "*^" by something in the notation of strtod */
					*term = 'e';
					if( *(term+2) == '~' )
						{
							*(term+1) = '-';
							*(term+2) = '0';
							term += 3;
						}
					else
						{
							*(term+1) = '0';
							term += 2;
						}
					continue;
				}
			break;
		}
	termTmp = *term;
	*term = '\0';
	double val = strtod( str, end );
	*term = termTmp;
	return val;
}

char
shapes_hexToChar( char c1, char c2 )
{
	return
		static_cast< char >
		( 16 * ( ( c1 < 'A' ) ? static_cast< unsigned char >( c1 - '0' ) : static_cast< unsigned char >( c1 - 'A' + 10 ) )
			+
			( ( c2 < 'A' ) ? static_cast< unsigned char >( c2 - '0' ) : static_cast< unsigned char >( c2 - 'A' + 10 ) ) );
}

void
ShapesScanner::start( )
{
	static const Ast::FileID * START_NULL_FILE = Ast::FileID::build_internal( "<start-null>" );
	inPrelude_ = true;
	if( ! interactive_ )
		{
			loader_.pushPreludeFiles( & loadStack_ );
		}
	std::istream * iFile = new std::istringstream( "" ); /* This will result in EOF immediately, and then we turn to the first real file. */
	yy_switch_to_buffer( yy_create_buffer( iFile, YY_BUF_SIZE ) );
	shapeslloc.file_ = START_NULL_FILE;
}

void
ShapesScanner::endOfLine( )
{
	shapeslloc.firstLine = shapeslloc.lastLine + 1;
	shapeslloc.lastLine = shapeslloc.firstLine;
	shapeslloc.lastColumn = 0;
}

void
ShapesScanner::doInclusion( )
{
	const Ast::FileID * fileID = NULL;
	std::ifstream * iFile = NULL;
	RefCountPtr< const Ast::NamespacePath > lexicalPath = searchContext_->lexicalPath( );

	if( ! currentNeedFile.empty( ) ){

		if( currentNeedHasNamespace ) {

			if( currentNeedIsNeed ) {
				lexicalPath = resolvedNamespace( );
				fileID = loader_.resolveSingleFile( *lexicalPath, currentNeedFile + ".shext", shapeslloc );
			}else{
				throw Exceptions::InternalError( shapeslloc, "Unexpected namespace form of unconditional file inclusion." );
			}

		}else{

		std::string path;
			if( currentNeedIsNeed )
				path = searchFile( currentNeedFile + ".shext" );
			else
				path = searchFile( currentNeedFile + ".shape" );

			struct stat theStat;
			if( stat( path.c_str( ), & theStat ) != 0 )
				{
					throw Exceptions::FileReadOpenError( shapeslloc, strrefdup( path.c_str( ) ), 0, 0, Exceptions::FileReadOpenError::STAT );
				}
			fileID = Ast::FileID::build_stat( theStat, path );

		}

		if( currentNeedIsNeed )
			{
				NeededFile neededFile( lexicalPath, fileID );
				if( neededFiles_.find( neededFile ) != neededFiles_.end( ) )
					{
						shapeslloc.firstLine = shapeslloc.lastLine + 1;
						shapeslloc.lastLine = shapeslloc.firstLine;
						shapeslloc.lastColumn = 0;
						BEGIN( INITIAL );
						return;
					}
				neededFiles_.insert( neededFile );
			}

		iFile = new std::ifstream( fileID->name( ) );
		if( ! iFile->good( ) )
			{
				delete iFile;
				throw Exceptions::FileReadOpenError( shapeslloc, strrefdup( fileID->name( ) ), 0, 0 );
			}

	}else{

		loadStackSizeStack_.push( loadStack_.size( ) );
		stateStackSizeStack_.push( stateStack_.size( ) + 1 ); /* 1 for the current state which is about to be pushed. */
		loader_.pushNamespaceFiles( *resolvedNamespace( ), & loadStack_ );
		while( loadStack_.size( ) > loadStackSizeStack_.top( ) ){
			fileID = loadStack_.topFileID( );
			lexicalPath = loadStack_.topNamespace( );
			loadStack_.pop( );
			NeededFile neededFile( lexicalPath, fileID );
			if( neededFiles_.find( neededFile ) != neededFiles_.end( ) )
				continue;
			neededFiles_.insert( neededFile );
			iFile = new std::ifstream( fileID->name( ) );
			if( ! iFile->good( ) )
				{
					delete iFile;
					throw Exceptions::FileReadOpenError( shapeslloc, strrefdup( fileID->name( ) ), NULL, NULL );
				}
			break;
		}
		if( iFile == NULL ){
			/* All of the files pushed to the loadStack were already needed. */
			loadStackSizeStack_.pop( );
			stateStackSizeStack_.pop( );
			shapeslloc.firstLine = shapeslloc.lastLine + 1;
			shapeslloc.lastLine = shapeslloc.firstLine;
			shapeslloc.lastColumn = 0;
			BEGIN( INITIAL );
			return;
		}
	}

	shapeslloc.firstLine = shapeslloc.lastLine + 1;
	shapeslloc.lastLine = shapeslloc.firstLine;
	shapeslloc.lastColumn = 0;

  Ast::SourceLocation * locPtr = new Ast::SourceLocation( fileID );
  locPtr->swap( & shapeslloc );
  locStack_.push( locPtr );
	stateStack_.push( YY_CURRENT_BUFFER );
	yy_switch_to_buffer( yy_create_buffer( iFile, YY_BUF_SIZE ) );
	pathCountStack_.push( currentNeedPushCount );
	namespaceLimits_.push( searchContext_->lexicalPath( )->size( ) );

	/* Prevent look-in stack to be carried over to the new file by setting up a new search context.
	 */
	searchContextStack_.push( searchContext_ );
	RefCountPtr< const Shapes::Ast::NamespacePath > encapsulationPath( ( ( ! currentNeedHasNamespace ) || namespace_.base( ) != Ast::NamespaceReference::ABSOLUTE ) ? searchContext_->encapsulationPath( ) : Ast::THE_EMPTY_NAMESPACE_PATH );
	searchContext_ = RefCountPtr< const Ast::SearchContext >( new Ast::SearchContext( lexicalPath, encapsulationPath, RefCountPtr< const Ast::LookinPathStack >( NullPtr< const Ast::LookinPathStack >( ) ), newPrivateName( ) ) );

	BEGIN( INITIAL );
}

void
ShapesScanner::queueStream( std::istream * is, const Ast::FileID * yyinFile )
{
	yyinQueue_.push_back( std::pair< std::istream * , const Ast::FileID * >( is, yyinFile ) );
}
