/* This file is part of Shapes.
 *
 * Shapes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Shapes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2008 Henrik Tidefelt
 */

#include <cmath>

#include "Shapes_Helpers_decls.h"

#include "shapescore.h"
#include "globals.h"
#include "shapesexceptions.h"
#include "astexpr.h"
#include "astfun.h"
#include "consts.h"
#include "simplepdfi.h"

#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include <stdio.h>

using namespace Shapes;


RefCountPtr< const Lang::ElementaryPath2D >
Helpers::elementaryPathCast2D( const Ast::PlacedIdentifier & calleeId, Kernel::Arguments & args, size_t argNo, const Ast::SourceLocation & callLoc )
{
	RefCountPtr< const Lang::Value > ptr = args.getValue( argNo );
	typedef const Lang::ElementaryPath2D ArgType;
	RefCountPtr< ArgType > res = ptr.down_cast< ArgType >( );
	if( res != NullPtr< ArgType >( ) )
		{
			return res;
		}

	typedef const Lang::CompositePath2D ArgType2;
	ArgType2 * res2 = dynamic_cast< ArgType2 * >( ptr.getPtr( ) );
	if( res2 == 0 )
		{
			throw Exceptions::CoreTypeMismatch( callLoc, calleeId, args, argNo, ArgType::staticTypeName( ) );
		}

	return res2->getElementaryPath( );
}

RefCountPtr< const Lang::ElementaryPath2D >
Helpers::elementaryPathCast2D( const RefCountPtr< const Lang::Value > & ptr, const Kernel::Continuation * loc )
{
	typedef const Lang::ElementaryPath2D ArgType;
	RefCountPtr< ArgType > res = ptr.down_cast< ArgType >( );
	if( res != NullPtr< ArgType >( ) )
		{
			return res;
		}

	typedef const Lang::CompositePath2D ArgType2;
	ArgType2 * res2 = dynamic_cast< ArgType2 * >( ptr.getPtr( ) );
	if( res2 == 0 )
		{
			throw Exceptions::ContinuationTypeMismatch( loc, ptr->getTypeName( ), ArgType::staticTypeName( ) );
		}

	return res2->getElementaryPath( );
}

RefCountPtr< const Lang::ElementaryPath2D >
Helpers::elementaryPathTry2D( const RefCountPtr< const Lang::Value > & ptr )
{
	typedef const Lang::ElementaryPath2D ArgType;
	RefCountPtr< ArgType > res = ptr.down_cast< ArgType >( );
	if( res != NullPtr< ArgType >( ) )
		{
			return res;
		}

	typedef const Lang::CompositePath2D ArgType2;
	ArgType2 * res2 = dynamic_cast< ArgType2 * >( ptr.getPtr( ) );
	if( res2 == 0 )
		{
			throw NonLocalExit::NotThisType( );
		}

	return res2->getElementaryPath( );
}


Concrete::SplineTime
Helpers::pathTimeCast( const Ast::PlacedIdentifier & calleeId, const RefCountPtr< const Lang::ElementaryPath2D > & pRef, Kernel::Arguments & args, size_t argNo, const Ast::SourceLocation & callLoc )
{
	const Lang::ElementaryPath2D * p = pRef.getPtr( );
	const Lang::Value * tPtr = args.getValue( argNo ).getPtr( );
	typedef const Lang::Float ArgType;
	ArgType * res = dynamic_cast< ArgType * >( tPtr );
	if( res != 0 )
		{
			return Concrete::Time( res->val_ );
		}

	typedef const Lang::Length ArgType2;
	ArgType2 * res2 = dynamic_cast< ArgType2 * >( tPtr );
	if( res2 == 0 )
		{
			throw Exceptions::CoreTypeMismatch( callLoc, calleeId, args, argNo, typeSetString( ArgType::staticTypeName( ), ArgType::staticTypeName( ) ) );
		}

	return p->arcTime( res2->get( ) );
}

Concrete::SplineTime
Helpers::pathTimeCast( const Lang::ElementaryPath2D * p, const Lang::Value * tPtr, const Kernel::Continuation * loc )
{
	typedef const Lang::Float ArgType;
	ArgType * res = dynamic_cast< ArgType * >( tPtr );
	if( res != 0 )
		{
			return Concrete::Time( res->val_ );
		}

	typedef const Lang::Length ArgType2;
	ArgType2 * res2 = dynamic_cast< ArgType2 * >( tPtr );
	if( res2 == 0 )
		{
			throw Exceptions::ContinuationTypeMismatch( loc, tPtr->getTypeName( ), typeSetString( ArgType::staticTypeName( ), ArgType2::staticTypeName( ) ) );
		}

	return p->arcTime( res2->get( ) );
}


RefCountPtr< const Lang::ElementaryPath3D >
Helpers::elementaryPathCast3D( const Ast::PlacedIdentifier & calleeId, Kernel::Arguments & args, size_t argNo, const Ast::SourceLocation & callLoc )
{
	RefCountPtr< const Lang::Value > ptr = args.getValue( argNo );
	typedef const Lang::ElementaryPath3D ArgType;
	RefCountPtr< ArgType > res = ptr.down_cast< ArgType >( );
	if( res != NullPtr< ArgType >( ) )
		{
			return res;
		}

	typedef const Lang::CompositePath3D ArgType2;
	ArgType2 * res2 = dynamic_cast< ArgType2 * >( ptr.getPtr( ) );
	if( res2 == 0 )
		{
			throw Exceptions::CoreTypeMismatch( callLoc, calleeId, args, argNo, ArgType::staticTypeName( ) );
		}

	return res2->getElementaryPath( );
}

RefCountPtr< const Lang::ElementaryPath3D >
Helpers::elementaryPathCast3D( const RefCountPtr< const Lang::Value > & ptr, const Kernel::Continuation * loc )
{
	typedef const Lang::ElementaryPath3D ArgType;
	RefCountPtr< ArgType > res = ptr.down_cast< ArgType >( );
	if( res != NullPtr< ArgType >( ) )
		{
			return res;
		}

	typedef const Lang::CompositePath3D ArgType2;
	ArgType2 * res2 = dynamic_cast< ArgType2 * >( ptr.getPtr( ) );
	if( res2 == 0 )
		{
			throw Exceptions::ContinuationTypeMismatch( loc, ptr->getTypeName( ), ArgType::staticTypeName( ) );
		}

	return res2->getElementaryPath( );
}

RefCountPtr< const Lang::ElementaryPath3D >
Helpers::elementaryPathTry3D( const RefCountPtr< const Lang::Value > & ptr )
{
	typedef const Lang::ElementaryPath3D ArgType;
	RefCountPtr< ArgType > res = ptr.down_cast< ArgType >( );
	if( res != NullPtr< ArgType >( ) )
		{
			return res;
		}

	typedef const Lang::CompositePath3D ArgType2;
	ArgType2 * res2 = dynamic_cast< ArgType2 * >( ptr.getPtr( ) );
	if( res2 == 0 )
		{
			throw NonLocalExit::NotThisType( );
		}

	return res2->getElementaryPath( );
}


Concrete::SplineTime
Helpers::pathTimeCast( const Ast::PlacedIdentifier & calleeId, const RefCountPtr< const Lang::ElementaryPath3D > & pRef, Kernel::Arguments & args, size_t argNo, const Ast::SourceLocation & callLoc )
{
	const Lang::ElementaryPath3D * p = pRef.getPtr( );
	const Lang::Value * tPtr = args.getValue( argNo ).getPtr( );
	typedef const Lang::Float ArgType;
	ArgType * res = dynamic_cast< ArgType * >( tPtr );
	if( res != 0 )
		{
			return Concrete::Time( res->val_ );
		}

	typedef const Lang::Length ArgType2;
	ArgType2 * res2 = dynamic_cast< ArgType2 * >( tPtr );
	if( res2 == 0 )
		{
			throw Exceptions::CoreTypeMismatch( callLoc, calleeId, args, argNo, typeSetString( ArgType::staticTypeName( ), ArgType::staticTypeName( ) ) );
		}

	return p->arcTime( res2->get( ) );
}

Concrete::SplineTime
Helpers::pathTimeCast( const Lang::ElementaryPath3D * p, const Lang::Value * tPtr, const Kernel::Continuation * loc )
{
	typedef const Lang::Float ArgType;
	ArgType * res = dynamic_cast< ArgType * >( tPtr );
	if( res != 0 )
		{
			return Concrete::Time( res->val_ );
		}

	typedef const Lang::Length ArgType2;
	ArgType2 * res2 = dynamic_cast< ArgType2 * >( tPtr );
	if( res2 == 0 )
		{
			throw Exceptions::ContinuationTypeMismatch( loc, tPtr->getTypeName( ), typeSetString( ArgType::staticTypeName( ), ArgType2::staticTypeName( ) ) );
		}

	return p->arcTime( res2->get( ) );
}


RefCountPtr< const char >
Helpers::typeSetString( RefCountPtr< const char > type1, RefCountPtr< const char > type2 )
{
	std::list< RefCountPtr< const char > > types;
	types.push_back( type1 );
	types.push_back( type2 );
	return typeSetString( types );
}

RefCountPtr< const char >
Helpers::typeSetString( RefCountPtr< const char > type1, RefCountPtr< const char > type2, RefCountPtr< const char > type3 )
{
	std::list< RefCountPtr< const char > > types;
	types.push_back( type1 );
	types.push_back( type2 );
	types.push_back( type3 );
	return typeSetString( types );
}

RefCountPtr< const char >
Helpers::typeSetString( RefCountPtr< const char > type1, RefCountPtr< const char > type2, RefCountPtr< const char > type3, RefCountPtr< const char > type4 )
{
	std::list< RefCountPtr< const char > > types;
	types.push_back( type1 );
	types.push_back( type2 );
	types.push_back( type3 );
	types.push_back( type4 );
	return typeSetString( types );
}

RefCountPtr< const char >
Helpers::typeSetString( RefCountPtr< const char > type1, RefCountPtr< const char > type2, RefCountPtr< const char > type3, RefCountPtr< const char > type4, RefCountPtr< const char > type5 )
{
	std::list< RefCountPtr< const char > > types;
	types.push_back( type1 );
	types.push_back( type2 );
	types.push_back( type3 );
	types.push_back( type4 );
	types.push_back( type5 );
	return typeSetString( types );
}

RefCountPtr< const char >
Helpers::typeSetString( const std::list< RefCountPtr< const char > > types )
{
	std::ostringstream oss;
	oss << "any of {" ;
	if( ! types.empty( ) )
		{
			typedef typeof types ListType;
			ListType::const_iterator i = types.begin( );
			oss << " " << *i ;
			++i;
			for( ; i != types.end( ); ++i )
				{
					oss << ", " << *i ;
				}
		}
	oss << " }" ;
	return strrefdup( oss );
}


Lang::CoreFunction::CoreFunction( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name )
  : Lang::Function( new Kernel::EvaluatedFormals( Ast::FileID::build_internal( name ), true ) ), id_( ns, name )
{ }

Lang::CoreFunction::CoreFunction( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name, Kernel::EvaluatedFormals * formals )
  : Lang::Function( formals ), id_( ns, name )
{ }

void
Lang::CoreFunction::analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst )
{
  /* Nothing to be done! */
}

bool
Lang::CoreFunction::isTransforming( ) const
{
  return false;
}

const Ast::PlacedIdentifier &
Lang::CoreFunction::id( ) const
{
  return id_;
}

void
Lang::CoreFunction::show( std::ostream & os ) const
{
  os << "< core function: " ;
  id_.show( os, Ast::Identifier::VARIABLE );
  os << " >" ;
}


Lang::CoreMutator::CoreMutator( const char * name )
  : Lang::Function( new Kernel::EvaluatedFormals( Ast::FileID::build_internal( name ), true ) ), name_( name )
{ }

Lang::CoreMutator::CoreMutator( const char * name, Kernel::EvaluatedFormals * formals )
  : Lang::Function( formals ), name_( name )
{ }

void
Lang::CoreMutator::analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst )
{
  /* Nothing to be done! */
}

bool
Lang::CoreMutator::isTransforming( ) const
{
  return false;
}

const char *
Lang::CoreMutator::name( ) const
{
  return name_;
}

void
Lang::CoreMutator::show( std::ostream & os ) const
{
  os << "< core mutator: " << name_ << " >" ;
}
