/* This file is part of Shapes.
 *
 * Shapes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Shapes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2008, 2009, 2010, 2013, 2014 Henrik Tidefelt
 */

#include <cmath>

#include "Shapes_Helpers_decls.h"

#include "coreconstruct_impl.h"
#include "singlelistrange.h"
#include "shapescore.h"
#include "globals.h"
#include "shapesexceptions.h"
#include "consts.h"
#include "simplepdfi.h"
#include "simplepdfo.h"
#include "astfun.h"
#include "tagtypes.h"
#include "multipage.h"
#include "charconverters.h"
#include "pagecontentstates.h"
#include "texlabelmanager.h"
#include "autoonoff.h"
#include "timetypes.h"
#include "graphtypes.h"
#include "shapesscanner.h"
#include "rasterloaders.h"
#include "glyphlist.h"
#include "warn.h"
#include "config.h"

#ifdef HAVE_FT2
#include <ft2build.h>
#include FT_FREETYPE_H
#endif
#ifdef HAVE_FONTCONFIG
#include <fontconfig/fontconfig.h>
#endif

#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include <stdio.h>
#include <ctime>

using namespace Shapes;


namespace Shapes
{
	namespace Helpers
	{
		RasterLoader_PNG theRasterLoader_PNG;
		RasterLoader_JPEG theRasterLoader_JPEG;
#ifdef HAVE_FONTCONFIG
		class AutoDestroyFcPattern
		{
			FcPattern * pat_;
		public:
			AutoDestroyFcPattern( FcPattern * pat )
				: pat_( pat )
			{ }
			~AutoDestroyFcPattern( )
			{
				if( pat_ != 0 )
					{
						FcPatternDestroy( pat_ );
					}
			}
			void deactivate( )
			{
				pat_ = 0;
			}
		};
		class AutoDestroyFcFontSet
		{
			FcFontSet * set_;
		public:
			AutoDestroyFcFontSet( FcFontSet * set )
				: set_( set )
			{ }
			~AutoDestroyFcFontSet( )
			{
				if( set_ != 0 )
					{
						FcFontSetDestroy( set_ );
					}
			}
			void deactivate( )
			{
				set_ = 0;
			}
		};
#endif
	}

	namespace Lang
	{

		class Core_tag : public Lang::CoreFunction
		{
		public:
			Core_tag( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name )
				: CoreFunction( ns, name, new Kernel::EvaluatedFormals( Ast::FileID::build_internal( name ), true ) )
			{
				formals_->appendEvaluatedCoreFormal( "key", Kernel::THE_SLOT_VARIABLE );
				formals_->appendEvaluatedCoreFormal( "obj", Kernel::THE_SLOT_VARIABLE );
				formals_->appendEvaluatedCoreFormal( "transform", Kernel::THE_TRUE_VARIABLE ); /* this argument means "transform if applicable" */
				formals_->appendEvaluatedCoreFormal( "draw", Kernel::THE_TRUE_VARIABLE ); /* this argument means "draw if applicable" */
			}

			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				args.applyDefaults( callLoc );

				typedef const Lang::Symbol KeyType;
				RefCountPtr< KeyType > key = Helpers::down_cast_CoreArgument< KeyType >( id_, args, 0, callLoc );
				bool tryTransform = Helpers::down_cast_CoreArgument< const Lang::Boolean >( id_, args, 2, callLoc )->val_;
				bool tryDraw = Helpers::down_cast_CoreArgument< const Lang::Boolean >( id_, args, 3, callLoc )->val_;

				if( tryDraw && ! tryTransform )
					{
						throw Exceptions::CoreOutOfRange( id_, args, 3, "A tagged object which does not transform cannot be drawn." );
					}

				size_t argsi = 1;

				if( tryDraw )
					{
						try
							{
								typedef const Lang::Drawable2D ArgType;

								RefCountPtr< ArgType > obj = Helpers::try_cast_CoreArgument< ArgType >( args.getValue( argsi ) );

								Kernel::ContRef cont = evalState->cont_;
								cont->takeValue( RefCountPtr< const Lang::Value >( new Lang::TaggedDrawable2D( key, obj ) ),
																 evalState );
								return;
							}
						catch( const NonLocalExit::NotThisType & ball )
							{
								/* Never mind, see below. */
							}

						try
							{
								typedef const Lang::Drawable3D ArgType;

								RefCountPtr< ArgType > obj = Helpers::try_cast_CoreArgument< ArgType >( args.getValue( argsi ) );

								Kernel::ContRef cont = evalState->cont_;
								cont->takeValue( RefCountPtr< const Lang::Value >( new Lang::TaggedDrawable3D( key, obj ) ),
																 evalState );
								return;
							}
						catch( const NonLocalExit::NotThisType & ball )
							{
								/* Never mind, see below. */
							}
					}

				if( tryTransform )
					{
						try
							{
								typedef const Lang::Geometric2D ArgType;

								RefCountPtr< ArgType > obj = Helpers::try_cast_CoreArgument< ArgType >( args.getValue( argsi ) );

								Kernel::ContRef cont = evalState->cont_;
								cont->takeValue( RefCountPtr< const Lang::Value >( new Lang::TaggedGeometric2D( key, obj ) ),
																 evalState );
								return;
							}
						catch( const NonLocalExit::NotThisType & ball )
							{
								/* Never mind, see below. */
							}

						try
							{
								typedef const Lang::Geometric3D ArgType;

								RefCountPtr< ArgType > obj = Helpers::try_cast_CoreArgument< ArgType >( args.getValue( argsi ) );

								Kernel::ContRef cont = evalState->cont_;
								cont->takeValue( RefCountPtr< const Lang::Value >( new Lang::TaggedGeometric3D( key, obj ) ),
																 evalState );
								return;
							}
						catch( const NonLocalExit::NotThisType & ball )
							{
								/* Never mind, see below. */
							}
					}

				{
					// As a last resort, we tag any value.
					// We return a Drawable2D.	Use immerse to make it 3D.
					Kernel::ContRef cont = evalState->cont_;
					cont->takeValue( RefCountPtr< const Lang::Value >( new Lang::TaggedValue2D( key, args.getValue( argsi ) ) ),
													 evalState );
				}
			}
		};

		// This function is in this file just because it i so related to Core_tag.
		class Core_find : public Lang::CoreFunction
		{
		public:
			Core_find( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name )
				: CoreFunction( ns, name, new Kernel::EvaluatedFormals( Ast::FileID::build_internal( name ), true ) )
			{
				formals_->appendEvaluatedCoreFormal( "container", Kernel::THE_SLOT_VARIABLE );
				formals_->appendEvaluatedCoreFormal( "key", Kernel::THE_SLOT_VARIABLE );
			}

			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				args.applyDefaults( callLoc );

				typedef const Lang::Symbol KeyType;
				RefCountPtr< KeyType > key = Helpers::down_cast_CoreArgument< KeyType >( id_, args, 1, callLoc );

				size_t argsi = 0;

				try
					{
						typedef const Lang::Drawable2D ContainerType;

						RefCountPtr< ContainerType > container = Helpers::try_cast_CoreArgument< ContainerType >( args.getValue( argsi ) );

						if( ! container->findOneTag( evalState, key->getKey( ), Lang::THE_2D_IDENTITY ) )
							{
								throw Exceptions::CoreOutOfRange( id_, args, 1, "Key not found." );
							}
						return;
					}
				catch( const NonLocalExit::NotThisType & ball )
					{
						/* Never mind, see below. */
					}

				try
					{
						typedef const Lang::Drawable3D ContainerType;

						RefCountPtr< ContainerType > container = Helpers::try_cast_CoreArgument< ContainerType >( args.getValue( argsi ) );

						if( ! container->findOneTag( evalState, key->getKey( ), Lang::THE_3D_IDENTITY ) )
							{
								throw Exceptions::CoreOutOfRange( id_, args, 1, "Key not found." );
							}
						return;
					}
				catch( const NonLocalExit::NotThisType & ball )
					{
						/* Never mind, see below. */
					}

				throw Exceptions::CoreTypeMismatch( callLoc, id_, args, argsi, Helpers::typeSetString( Lang::Drawable2D::staticTypeName( ), Lang::Drawable3D::staticTypeName( ) ) );
			}
		};

		// This function is in this file just because it i so related to Core_tag.
		class Core_findall : public Lang::CoreFunction
		{
		public:
			Core_findall( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name )
				: CoreFunction( ns, name, new Kernel::EvaluatedFormals( Ast::FileID::build_internal( name ), true ) )
			{
				formals_->appendEvaluatedCoreFormal( "container", Kernel::THE_SLOT_VARIABLE );
				formals_->appendEvaluatedCoreFormal( "key", Kernel::THE_SLOT_VARIABLE );
			}

			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				args.applyDefaults( callLoc );

				typedef const Lang::Symbol KeyType;
				RefCountPtr< KeyType > key = Helpers::down_cast_CoreArgument< KeyType >( id_, args, 1, callLoc );

				size_t argsi = 0;

				try
					{
						typedef const Lang::Drawable2D ContainerType;

						RefCountPtr< ContainerType > container = Helpers::try_cast_CoreArgument< ContainerType >( args.getValue( argsi ) );

						std::vector< Kernel::ValueRef > * dst = new std::vector< Kernel::ValueRef >;
						container->findTags( dst, evalState->dyn_, key->getKey( ), Lang::THE_2D_IDENTITY );

						Kernel::ContRef cont = evalState->cont_;
						cont->takeValue( RefCountPtr< const Lang::Value >( new Lang::VectorFunction( dst ) ),
														 evalState );
						return;
					}
				catch( const NonLocalExit::NotThisType & ball )
					{
						/* Never mind, see below. */
					}

				try
					{
						typedef const Lang::Drawable3D ContainerType;

						RefCountPtr< ContainerType > container = Helpers::try_cast_CoreArgument< ContainerType >( args.getValue( argsi ) );

						std::vector< Kernel::ValueRef > * dst = new std::vector< Kernel::ValueRef >;
						container->findTags( dst, evalState->dyn_, key->getKey( ), Lang::THE_3D_IDENTITY );

						Kernel::ContRef cont = evalState->cont_;
						cont->takeValue( RefCountPtr< const Lang::Value >( new Lang::VectorFunction( dst ) ),
														 evalState );
						return;
					}
				catch( const NonLocalExit::NotThisType & ball )
					{
						/* Never mind, see below. */
					}

				throw Exceptions::CoreTypeMismatch( callLoc, id_, args, argsi, Helpers::typeSetString( Lang::Drawable2D::staticTypeName( ), Lang::Drawable3D::staticTypeName( ) ) );
			}
		};

		class Core_phong : public Lang::CoreFunction
		{
		public:
			Core_phong( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name ) : CoreFunction( ns, name ) { }
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				const size_t ARITY = 1;
				CHECK_ARITY( args, ARITY, id_ );

				typedef const Lang::Float ArgType;
				double exponent = Helpers::down_cast_CoreArgument< ArgType >( id_, args, 0, callLoc )->val_;
				if( exponent < 0 )
					{
						throw Exceptions::CoreOutOfRange( id_, args, 0, "The Phong exponent should be greater than 0." );
					}

				Kernel::ContRef cont = evalState->cont_;
				cont->takeValue( Kernel::ValueRef( new Lang::SpecularReflectionTerm( 1., exponent ) ),
												 evalState );
			}
		};

		class Core_cons : public Lang::CoreFunction
		{
		public:
			Core_cons( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name )
				: CoreFunction( ns, name, new Kernel::EvaluatedFormals( Ast::FileID::build_internal( name ), false ) )
			{
				formals_->appendEvaluatedCoreFormal( "car", Kernel::THE_SLOT_VARIABLE );
				formals_->appendEvaluatedCoreFormal( "cdr", Kernel::THE_SLOT_VARIABLE );
			}
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				const size_t ARITY = 2;
				CHECK_ARITY( args, ARITY, id_ );

				Kernel::VariableHandle cdr = args.getHandle( 1 );
				if( ! cdr->isThunk( ) ){
					RefCountPtr< const Lang::Value > cdrVal = cdr->getUntyped( );
					RefCountPtr< const Lang::SingleList > cdrValTyped = cdrVal.down_cast< const Lang::SingleList >( );
					if( cdrValTyped != NullPtr< const Lang::SingleList >( ) ){
						Kernel::ContRef cont = evalState->cont_;
						cont->takeValue( Kernel::ValueRef( new Lang::SingleListPair( args.getHandle( 0 ),
																																				 cdrValTyped ) ),
														 evalState );
						return;
					}
				}

				Kernel::ContRef cont = evalState->cont_;
				cont->takeValue( Kernel::ValueRef( new Lang::ConsPair( args.getHandle( 0 ),
																															 cdr ) ),
												 evalState );
			}
		};

		class Core_list : public Lang::CoreFunction
		{
		public:
			Core_list( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name ) : CoreFunction( ns, name ) { }
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				RefCountPtr< const Lang::SingleList > res = Lang::THE_CONS_NULL;
				if( ! args.empty( ) )
					{
						for( size_t i = args.size( ) - 1; ; --i )
							{
								res = RefCountPtr< const Lang::SingleList >( new Lang::SingleListPair( args.getHandle( i ), res ) );
								if( i == 0 )
									{
										break;
									}
							}
					}

				Kernel::ContRef cont = evalState->cont_;
				cont->takeValue( res,
												 evalState );
			}
		};

		class Core_unlist : public Lang::CoreFunction
		{
			Kernel::UnnamedStructureFactory argListFactory_;
		public:
			Core_unlist( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name ) : CoreFunction( ns, name ) { }
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				const size_t ARITY = 1;
				CHECK_ARITY( args, ARITY, id_ );

				typedef const Lang::SingleList ArgType;

				Kernel::ContRef cont = evalState->cont_;
				cont->takeValue( argListFactory_.build( Helpers::down_cast_CoreArgument< ArgType >( id_, args, 0, callLoc ) ),
												 evalState );
			}
		};

		class Core_isnil : public Lang::CoreFunction
		{
		public:
			Core_isnil( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name ) : CoreFunction( ns, name ) { }
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				const size_t ARITY = 1;
				CHECK_ARITY( args, ARITY, id_ );

				RefCountPtr< const Lang::Value > aUntyped = args.getValue( 0 );

				Kernel::ContRef cont = evalState->cont_;
				cont->takeValue( RefCountPtr< const Lang::Value >( new Lang::Boolean( dynamic_cast< const Lang::SingleListNull * >( args.getValue( 0 ).getPtr( ) ) != 0 ) ),
												 evalState );
			}
		};

		class Core_span : public Lang::CoreFunction
		{
		public:
			Core_span( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name )
				: CoreFunction( ns, name, new Kernel::EvaluatedFormals( Ast::FileID::build_internal( name ), false ) )
			{
				formals_->appendEvaluatedCoreFormal( "begin", Kernel::THE_VOID_VARIABLE );
				formals_->appendEvaluatedCoreFormal( "end", Kernel::THE_VOID_VARIABLE );
				formals_->appendEvaluatedCoreFormal( "step", Kernel::THE_VOID_VARIABLE );
				formals_->appendEvaluatedCoreFormal( "count", Kernel::THE_VOID_VARIABLE );
			}
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				args.applyDefaults( callLoc );

				Kernel::ContRef cont = evalState->cont_;
				cont->takeValue( Kernel::ValueRef( new Lang::Span( args.getHandle( 0 ), args.getHandle( 1 ), args.getHandle( 2 ), args.getHandle( 3 ) ) ),
												 evalState );
			}
		};

		class Core_affinetransform : public Lang::CoreFunction
		{
		public:
			Core_affinetransform( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name ) : CoreFunction( ns, name ) { }
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				const size_t ARITY = 3;
				CHECK_ARITY( args, ARITY, id_ );

				typedef const Lang::FloatPair ArgType0;
				typedef const Lang::FloatPair ArgType1;
				typedef const Lang::Coords2D ArgType2;

				RefCountPtr< ArgType0 > argx = Helpers::down_cast_CoreArgument< ArgType0 >( id_, args, 0, callLoc );
				RefCountPtr< ArgType1 > argy = Helpers::down_cast_CoreArgument< ArgType1 >( id_, args, 1, callLoc );
				RefCountPtr< ArgType2 > arg1 = Helpers::down_cast_CoreArgument< ArgType2 >( id_, args, 2, callLoc );

				Kernel::ContRef cont = evalState->cont_;
				cont->takeValue( Kernel::ValueRef( new Lang::Transform2D( argx->x_, argx->y_, argy->x_, argy->y_, arg1->x_.get( ), arg1->y_.get( ) ) ),
												 evalState );
			}
		};

		class Core_affinetransform3d : public Lang::CoreFunction
		{
		public:
			Core_affinetransform3d( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name ) : CoreFunction( ns, name ) { }
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				const size_t ARITY = 4;
				CHECK_ARITY( args, ARITY, id_ );

				typedef const Lang::FloatTriple ArgType0;
				typedef const Lang::FloatTriple ArgType1;
				typedef const Lang::FloatTriple ArgType2;
				typedef const Lang::Coords3D ArgType3;

				RefCountPtr< ArgType0 > argx = Helpers::down_cast_CoreArgument< ArgType0 >( id_, args, 0, callLoc );
				RefCountPtr< ArgType1 > argy = Helpers::down_cast_CoreArgument< ArgType1 >( id_, args, 1, callLoc );
				RefCountPtr< ArgType2 > argz = Helpers::down_cast_CoreArgument< ArgType2 >( id_, args, 2, callLoc );
				RefCountPtr< ArgType3 > arg1 = Helpers::down_cast_CoreArgument< ArgType3 >( id_, args, 3, callLoc );

				Kernel::ContRef cont = evalState->cont_;
				cont->takeValue( Kernel::ValueRef( new Lang::Transform3D( argx->x_, argx->y_, argx->z_,
																																	argy->x_, argy->y_, argy->z_,
																																	argz->x_, argz->y_, argz->z_,
																																	arg1->x_.get( ), arg1->y_.get( ), arg1->z_.get( ) ) ),
												 evalState );
			}
		};

		class Core_shift : public Lang::CoreFunction
		{
		public:
			Core_shift( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name ) : CoreFunction( ns, name ) { }
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				const size_t ARITY = 1;
				CHECK_ARITY( args, ARITY, id_ );

				Kernel::ContRef cont = evalState->cont_;

				size_t i = 0;

				typedef const Lang::Coords2D ArgType1;
				RefCountPtr< ArgType1 > arg1 = Helpers::down_cast_CoreArgument< ArgType1 >( id_, args, i, callLoc );
				cont->takeValue( Kernel::ValueRef( new Lang::Transform2D( 1, 0, 0, 1, arg1->x_.get( ), arg1->y_.get( ) ) ),
												 evalState );
			}
		};

		class Core_shift3d : public Lang::CoreFunction
		{
		public:
			Core_shift3d( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name ) : CoreFunction( ns, name ) { }
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				const size_t ARITY = 1;
				CHECK_ARITY( args, ARITY, id_ );

				Kernel::ContRef cont = evalState->cont_;

				size_t i = 0;

				typedef const Lang::Coords3D ArgType1;
				RefCountPtr< ArgType1 > arg1 = Helpers::down_cast_CoreArgument< ArgType1 >( id_, args, i, callLoc );
				cont->takeValue( Kernel::ValueRef( new Lang::Transform3D( 1, 0, 0, 0, 1, 0, 0, 0, 1, arg1->x_.get( ), arg1->y_.get( ), arg1->z_.get( ) ) ),
												 evalState );
			}
		};

		class Core_rotate : public Lang::CoreFunction
		{
		public:
			Core_rotate( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name )
				: CoreFunction( ns, name, new Kernel::EvaluatedFormals( Ast::FileID::build_internal( name ), true ) )
			{
				formals_->appendEvaluatedCoreFormal( "angle", Kernel::THE_SLOT_VARIABLE );
			}
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				args.applyDefaults( callLoc );

				size_t i = 0;

				typedef const Lang::Float ArgType1;
				RefCountPtr< ArgType1 > arg1 = Helpers::down_cast_CoreArgument< ArgType1 >( id_, args, i, callLoc );

				double c = cos( arg1->val_ );
				double s = sin( arg1->val_ );
				Kernel::ContRef cont = evalState->cont_;
				cont->takeValue( Kernel::ValueRef( new Lang::Transform2D( c, s, -s, c, 0, 0 ) ),
												 evalState );
			}
		};

		class Core_rotate3d : public Lang::CoreFunction
		{
		public:
			Core_rotate3d( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name )
				: CoreFunction( ns, name, new Kernel::EvaluatedFormals( Ast::FileID::build_internal( name ), true ) )
			{
				formals_->appendEvaluatedCoreFormal( "dir", Kernel::THE_SLOT_VARIABLE );
				formals_->appendEvaluatedCoreFormal( "angle", Kernel::THE_SLOT_VARIABLE );
			}
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				args.applyDefaults( callLoc );

				size_t i = 0;

				typedef const Lang::FloatTriple ArgType1;
				RefCountPtr< ArgType1 > dir = Helpers::down_cast_CoreArgument< ArgType1 >( id_, args, i, callLoc );
				if( dir->x_ == 0 && dir->y_ == 0 && dir->z_ == 0 )
					{
						throw Exceptions::CoreOutOfRange( id_, args, i, "The rotation direction is degenerate, that is (0,0,0)." );
					}

				++i;

				typedef const Lang::Float ArgType2;
				RefCountPtr< ArgType2 > angle = Helpers::down_cast_CoreArgument< ArgType2 >( id_, args, i, callLoc );

				double r = sqrt( dir->x_ * dir->x_ + dir->y_ * dir->y_ + dir->z_ * dir->z_ );
				double x = dir->x_ / r;
				double y = dir->y_ / r;
				double z = dir->z_ / r;
				double x2 = x * x;
				double y2 = y * y;
				double z2 = z * z;
				double c = cos( angle->val_ );
				double s = sin( angle->val_ );
				Kernel::ContRef cont = evalState->cont_;
				cont->takeValue( Kernel::ValueRef( new Lang::Transform3D( x2+(y2+z2)*c,	x*y*(1-c)+z*s, x*z*(1-c)-y*s,
																																	x*y*(1-c)-z*s, y2+(x2+z2)*c,	y*z*(1-c)+x*s,
																																	x*z*(1-c)+y*s, y*z*(1-c)-x*s, z2+(x2+y2)*c,
																																	0, 0, 0 ) ),
												 evalState );
			}
		};

		class Core_scale : public Lang::CoreFunction
		{
		public:
			Core_scale( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name )
				: CoreFunction( ns, name, new Kernel::EvaluatedFormals( Ast::FileID::build_internal( name ), true ) )
			{
				Kernel::VariableHandle one( new Kernel::Variable( RefCountPtr< const Lang::Value >( new Lang::Float( 1 ) ) ) );

				formals_->appendEvaluatedCoreFormal( "r", one );
				formals_->appendEvaluatedCoreFormal( "x", one );
				formals_->appendEvaluatedCoreFormal( "y", one );
			}
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				args.applyDefaults( callLoc );

				typedef const Lang::Float ArgType;
				RefCountPtr< ArgType > argr = Helpers::down_cast_CoreArgument< ArgType >( id_, args, 0, callLoc );
				RefCountPtr< ArgType > argx = Helpers::down_cast_CoreArgument< ArgType >( id_, args, 1, callLoc );
				RefCountPtr< ArgType > argy = Helpers::down_cast_CoreArgument< ArgType >( id_, args, 2, callLoc );

				Kernel::ContRef cont = evalState->cont_;
				cont->takeValue( Kernel::ValueRef( new Lang::Transform2D( argr->val_ * argx->val_, 0,
																																	0, argr->val_ * argy->val_,
																																	0, 0 ) ),
												 evalState );
			}
		};

		class Core_scale3d : public Lang::CoreFunction
		{
		public:
			Core_scale3d( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name )
				: CoreFunction( ns, name, new Kernel::EvaluatedFormals( Ast::FileID::build_internal( name ), true ) )
			{
				Kernel::VariableHandle one( new Kernel::Variable( RefCountPtr< const Lang::Value >( new Lang::Float( 1 ) ) ) );

				formals_->appendEvaluatedCoreFormal( "r", one );
				formals_->appendEvaluatedCoreFormal( "x", one );
				formals_->appendEvaluatedCoreFormal( "y", one );
				formals_->appendEvaluatedCoreFormal( "z", one );
			}
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				args.applyDefaults( callLoc );

				typedef const Lang::Float ArgType;
				RefCountPtr< ArgType > argr = Helpers::down_cast_CoreArgument< ArgType >( id_, args, 0, callLoc );
				RefCountPtr< ArgType > argx = Helpers::down_cast_CoreArgument< ArgType >( id_, args, 1, callLoc );
				RefCountPtr< ArgType > argy = Helpers::down_cast_CoreArgument< ArgType >( id_, args, 2, callLoc );
				RefCountPtr< ArgType > argz = Helpers::down_cast_CoreArgument< ArgType >( id_, args, 3, callLoc );

				Kernel::ContRef cont = evalState->cont_;
				cont->takeValue( Kernel::ValueRef( new Lang::Transform3D( argr->val_ * argx->val_, 0, 0,
																																	0, argr->val_ * argy->val_, 0,
																																	0, 0, argr->val_ * argz->val_,
																																	0, 0, 0 ) ),
												 evalState );
			}
		};

		class Core_inverse : public Lang::CoreFunction
		{
		public:
			Core_inverse( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name ) : CoreFunction( ns, name ) { }
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				const size_t ARITY = 1;
				CHECK_ARITY( args, ARITY, id_ );

				size_t i = 0;

				typedef const Lang::Transform2D ArgType1;
				RefCountPtr< ArgType1 > tf = Helpers::down_cast_CoreArgument< ArgType1 >( id_, args, i, callLoc );
				double det = tf->xx_ * tf->yy_ - tf->xy_ * tf->yx_;
				if( fabs( det ) < Computation::SINGULAR_TRANSFORM_LIMIT )
					{
						throw Exceptions::CoreOutOfRange( id_, args, 0, "Singular transforms cannot be inverted." );
					}
				double idet = 1 / det;
				double ixx = idet * tf->yy_;
				double ixy = - idet * tf->xy_;
				double iyx = - idet * tf->yx_;
				double iyy = idet * tf->xx_;
				Kernel::ContRef cont = evalState->cont_;
				cont->takeValue( Kernel::ValueRef( new Lang::Transform2D( ixx, iyx,
																																	ixy, iyy,
																																	-( ixx * tf->xt_ + ixy * tf->yt_ ), -( iyx * tf->xt_ + iyy * tf->yt_ ) ) ),
												 evalState );
			}
		};

		class Core_inverse3d : public Lang::CoreFunction
		{
		public:
			Core_inverse3d( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name ) : CoreFunction( ns, name ) { }
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				const size_t ARITY = 1;
				CHECK_ARITY( args, ARITY, id_ );

				size_t i = 0;

				typedef const Lang::Transform3D ArgType1;
				RefCountPtr< ArgType1 > tf = Helpers::down_cast_CoreArgument< ArgType1 >( id_, args, i, callLoc );
				double det =
					tf->xx_ * ( tf->yy_ * tf->zz_ - tf->yz_ * tf->zy_ )
					- tf->xy_ * ( tf->yx_ * tf->zz_ - tf->yz_ * tf->zx_ )
					+ tf->xz_ * ( tf->yx_ * tf->zy_ - tf->yy_ * tf->zx_ );
				if( fabs( det ) < Computation::SINGULAR_TRANSFORM_LIMIT )
					{
						throw Exceptions::CoreOutOfRange( id_, args, 0, "Singular transforms cannot be inverted." );
					}
				double idet = 1 / det;
				double ixx = idet * ( tf->yy_ * tf->zz_ - tf->yz_ * tf->zy_ );
				double ixy = - idet * ( tf->xy_ * tf->zz_ - tf->xz_ * tf->zy_ );
				double ixz = idet * ( tf->xy_ * tf->yz_ - tf->xz_ * tf->yy_ );
				double iyx = - idet * ( tf->yx_ * tf->zz_ - tf->yz_ * tf->zx_ );
				double iyy = idet * ( tf->xx_ * tf->zz_ - tf->xz_ * tf->zx_ );
				double iyz = - idet * ( tf->xx_ * tf->yz_ - tf->xz_ * tf->yx_ );
				double izx = idet * ( tf->yx_ * tf->zy_ - tf->yy_ * tf->zx_ );
				double izy = - idet * ( tf->xx_ * tf->zy_ - tf->xy_ * tf->zx_ );
				double izz = idet * ( tf->xx_ * tf->yy_ - tf->xy_ * tf->yx_ );
				Kernel::ContRef cont = evalState->cont_;
				cont->takeValue( Kernel::ValueRef( new Lang::Transform3D( ixx, iyx, izx,
																																	ixy, iyy, izy,
																																	ixz, iyz, izz,
																																	-( ixx * tf->xt_ + ixy * tf->yt_ + ixz * tf->zt_ ),
																																	-( iyx * tf->xt_ + iyy * tf->yt_ + iyz * tf->zt_ ),
																																	-( izx * tf->xt_ + izy * tf->yt_ + izz * tf->zt_ ) ) ),
												 evalState );
			}
		};

		class Core_formxo : public Lang::CoreFunction
		{
		public:
			Core_formxo( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name ) : CoreFunction( ns, name ) { }
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				const size_t ARITY = 1;
				CHECK_ARITY( args, ARITY, id_ );

				typedef const Lang::Drawable2D ArgType;
				RefCountPtr< ArgType > arg = Helpers::down_cast_CoreArgument< ArgType >( id_, args, 0, callLoc );

				RefCountPtr< const Lang::ElementaryPath2D > theBBox = arg->bbox( Lang::Drawable2D::BOUNDING );
				Concrete::Coords2D llcorner( 0, 0 );
				Concrete::Coords2D urcorner( 0, 0 );
				if( ! theBBox->boundingRectangle( & llcorner, & urcorner ) )
					{
						throw Exceptions::InternalErrorIn( id_, "The object has no bounding box." );
					}


				RefCountPtr< SimplePDF::PDF_Stream_out > form;
				RefCountPtr< SimplePDF::PDF_Object > indirection = SimplePDF::indirect( form, & Kernel::theIndirectObjectCount );

				RefCountPtr< SimplePDF::PDF_Resources > resources;
				(*form)[ "Resources" ] = SimplePDF::indirect( resources, & Kernel::theIndirectObjectCount );

				(*form)[ "Subtype" ] = SimplePDF::newName( "Form" );
				(*form)[ "FormType" ] = SimplePDF::newInt( 1 );
				(*form)[ "BBox" ] = RefCountPtr< SimplePDF::PDF_Vector >( new SimplePDF::PDF_Vector( llcorner.x_.offtype< 1, 0 >( ), llcorner.y_.offtype< 1, 0 >( ),
																																														 urcorner.x_.offtype< 1, 0 >( ), urcorner.y_.offtype< 1, 0 >( ) ) );

				/* There's a possibility of adding a transformation matrix entry in the dictionary here, but it is not used, not even
				 * for transformed drawables.
				 */
				//	(*markForm)[ "Matrix" ] = RefCountPtr<PDF_Object>( new PDF_Vector( 1, 0, 0, 1, -30, -30 ) );

				Kernel::PageContentStates pdfState( resources );
				arg->shipout( form->data, & pdfState, Lang::Transform2D( 1, 0, 0, 1, 0, 0 ) );


				Lang::XObject * res = new Lang::XObject( indirection,
																								 theBBox );
				res->setDebugStr( "user form" );
				Kernel::ContRef cont = evalState->cont_;
				cont->takeValue( Kernel::ValueRef( res ),
												 evalState );
			}
		};

		class Core_transparencygroup : public Lang::CoreFunction
		{
		public:
			Core_transparencygroup( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name )
				: CoreFunction( ns, name, new Kernel::EvaluatedFormals( Ast::FileID::build_internal( name ), true ) )
			{
				formals_->appendEvaluatedCoreFormal( "content", Kernel::THE_SLOT_VARIABLE );
				formals_->appendEvaluatedCoreFormal( "isolated", Kernel::THE_FALSE_VARIABLE );
				formals_->appendEvaluatedCoreFormal( "knockout", Kernel::THE_FALSE_VARIABLE );
			}
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				args.applyDefaults( callLoc );

				typedef const Lang::Group2D ArgType0;
				typedef const Lang::Boolean ArgType1;
				typedef const Lang::Boolean ArgType2;
				RefCountPtr< ArgType0 > content = Helpers::down_cast_CoreArgument< ArgType0 >( id_, args, 0, callLoc );
				RefCountPtr< ArgType1 > isolated = Helpers::down_cast_CoreArgument< ArgType1 >( id_, args, 1, callLoc );
				RefCountPtr< ArgType2 > knockout = Helpers::down_cast_CoreArgument< ArgType2 >( id_, args, 2, callLoc );

				RefCountPtr< const Lang::ColorSpace > blendSpace = evalState->dyn_->getBlendSpace( );

				Kernel::ContRef cont = evalState->cont_;
				cont->takeValue( Helpers::newTransparencyGroup( content, isolated->val_, knockout->val_, blendSpace ),
												 evalState );
			}
		};

		class Core_importRasterImage : public Lang::CoreFunction
		{
			std::map< const char *, const Helpers::RasterLoader *, charPtrLess > loaders_;
		public:
			Core_importRasterImage( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name )
				: CoreFunction( ns, name, new Kernel::EvaluatedFormals( Ast::FileID::build_internal( name ), true ) )
			{
				formals_->appendEvaluatedCoreFormal( "filename", Kernel::THE_SLOT_VARIABLE );
				formals_->appendEvaluatedCoreFormal( "resolution", Helpers::newValHandle( new Lang::Length( Concrete::Length( 1 ) ) ) );
				formals_->appendEvaluatedCoreFormal( "override", Kernel::THE_TRUE_VARIABLE );
				formals_->appendEvaluatedCoreFormal( "kind", Kernel::THE_VOID_VARIABLE );

				loaders_[ "png" ] = & Helpers::theRasterLoader_PNG;
				loaders_[ "PNG" ] = & Helpers::theRasterLoader_PNG;

				loaders_[ "jpg" ] = & Helpers::theRasterLoader_JPEG;
				loaders_[ "JPG" ] = & Helpers::theRasterLoader_JPEG;
				loaders_[ "jpeg" ] = & Helpers::theRasterLoader_JPEG;
				loaders_[ "JPEG" ] = & Helpers::theRasterLoader_JPEG;

				/* It would seem natural to also support "JIF"-images, but currently the JPEG library we use won't open such files.
				 */
			}
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				args.applyDefaults( callLoc );

				size_t argsi = 0;

				typedef const Lang::String ArgType;
				RefCountPtr< ArgType > filename = Helpers::down_cast_CoreArgument< ArgType >( id_, args, argsi, callLoc );
				size_t argsi_filename = argsi;

				std::string full_filename;
				try
					{
						full_filename = Ast::theShapesScanner.searchFile( filename->val_.getPtr( ), true ); /* true means that errors should be handled in "runtime mode". */
					}
				catch( RefCountPtr< const char > & msg )
					{
						throw Exceptions::CoreOutOfRange( id_, args, argsi_filename, msg );
					}

				RefCountPtr< std::ifstream > iFile( new std::ifstream( full_filename.c_str( ) ) );
				if( ! iFile->good( ) )
					{
						std::ostringstream msg;
						msg << "Failed to open file for input: " << full_filename.c_str( ) ;
						throw Exceptions::CoreOutOfRange( id_, args, argsi_filename, strrefdup( msg ) );
					}

				++argsi;
				typedef const Lang::Length ResolutionType;
				Concrete::Length resolution = Helpers::down_cast_CoreArgument< ResolutionType >( id_, args, argsi, callLoc )->get( );
				if( resolution <= Concrete::ZERO_LENGTH )
					{
						throw Exceptions::CoreOutOfRange( id_, args, argsi, strrefdup( "The resolution must be positive." ) );
					}

				++argsi;
				typedef const Lang::Boolean OverrideType;
				bool override = Helpers::down_cast_CoreArgument< OverrideType >( id_, args, argsi, callLoc )->val_;

				++argsi;
				typedef const Lang::Symbol KindType;
				RefCountPtr< KindType > kind = Helpers::down_cast_CoreArgument< KindType >( id_, args, argsi, callLoc, true );
				const Helpers::RasterLoader * loader = 0;
				if( kind == NullPtr< KindType >( ) )
					{
						const char * begin = filename->val_.getPtr( );
						const char * ext = filename->val_.getPtr( ) + filename->bytecount_ - 1;
						for( ; ext > begin; --ext )
							{
								if( *ext == '.' )
									{
										typedef typeof loaders_ MapType;
										MapType::const_iterator i = loaders_.find( ext + 1 );
										if( i == loaders_.end( ) )
											{
												std::ostringstream msg;
												msg << "Filename extension '" << ext << "' has no associated raster loader." ;
												throw Exceptions::CoreOutOfRange( id_, args, argsi_filename, strrefdup( msg ) );
											}
										loader = i->second;
										break;
									}
							}
						if( loader == 0 )
							{
								std::ostringstream msg;
								msg << "Filename without extension must be combined with explicit file kind: " << filename->val_.getPtr( ) ;
								throw Exceptions::CoreOutOfRange( id_, args, argsi_filename, strrefdup( msg ) );
							}
					}
				else
					{
						static Lang::Symbol PNG( "PNG" );
						static Lang::Symbol JPEG( "JPEG" );
						if( *kind == PNG )
							{
								loader = & Helpers::theRasterLoader_PNG;
							}
						else if( *kind == JPEG )
							{
								loader = & Helpers::theRasterLoader_JPEG;
							}
						else
							{
								std::ostringstream msg;
								msg << "The only allowed file kind symbols are { " ;
								PNG.show( msg );
								msg << ", " ;
								JPEG.show( msg );
								msg << " }." ;
								throw Exceptions::CoreOutOfRange( id_, args, argsi, strrefdup( msg ) );
							}
					}

				Kernel::ContRef cont = evalState->cont_;
				cont->takeValue( loader->load( iFile, full_filename, resolution, override, id_, callLoc ),
												 evalState );
			}
		};

		class Core_array : public Lang::CoreFunction
		{
		public:
			Core_array( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name ) : CoreFunction( ns, name ) { }
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				std::vector< RefCountPtr< const Lang::Value > > * res = new std::vector< RefCountPtr< const Lang::Value > >;
				res->reserve( args.size( ) );

				for( size_t i = 0; i != args.size( ); ++i )
					{
						res->push_back( args.getValue( i ) );
					}

				Kernel::ContRef cont = evalState->cont_;
				cont->takeValue( Kernel::ValueRef( new Lang::VectorFunction( res ) ),
												 evalState );
			}
		};

		class Core_graph : public Lang::CoreFunction
		{
		public:
			Core_graph( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name )
				: CoreFunction( ns, name, new Kernel::EvaluatedFormals( Ast::FileID::build_internal( name ), true ) )
			{
				formals_->appendEvaluatedCoreFormal( "nodes", Kernel::THE_NIL_VARIABLE );
				formals_->appendEvaluatedCoreFormal( "edges", Kernel::THE_NIL_VARIABLE );
				formals_->appendEvaluatedCoreFormal( "directed", Kernel::THE_FALSE_VARIABLE );
				formals_->appendEvaluatedCoreFormal( "undirected", Kernel::THE_FALSE_VARIABLE );
				formals_->appendEvaluatedCoreFormal( "loops", Kernel::THE_FALSE_VARIABLE );
				formals_->appendEvaluatedCoreFormal( "parallel", Kernel::THE_FALSE_VARIABLE );
				formals_->appendEvaluatedCoreFormal( "partitions", Kernel::THE_VOID_VARIABLE );
			}
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				args.applyDefaults( callLoc );

				size_t argsi = 0;
				size_t nodesi = argsi;
				RefCountPtr< const Lang::Value > nodes = args.getValue( argsi );
				++argsi;
				size_t edgesi = argsi;
				RefCountPtr< const Lang::Value > edges = args.getValue( argsi );

				Kernel::GraphDomain * domainPtr = new Kernel::GraphDomain( );
				RefCountPtr< const Kernel::GraphDomain > domain = RefCountPtr< const Kernel::GraphDomain >( domainPtr );
				typedef const Lang::Boolean FlagType;
				++argsi;
				if( Helpers::down_cast_CoreArgument< FlagType >( id_, args, argsi, callLoc )->val_ ){
					domainPtr->allowDirected( );
				}
				++argsi;
				if( Helpers::down_cast_CoreArgument< FlagType >( id_, args, argsi, callLoc )->val_ ){
					domainPtr->allowUndirected( );
				}
				++argsi;
				if( Helpers::down_cast_CoreArgument< FlagType >( id_, args, argsi, callLoc )->val_ ){
					domainPtr->allowLoops( );
				}
				++argsi;
				if( Helpers::down_cast_CoreArgument< FlagType >( id_, args, argsi, callLoc )->val_ ){
					domainPtr->allowParallel( );
				}

				++argsi;
				size_t partitionsi = argsi;
				RefCountPtr< const Lang::Value > partitions = args.getValue( argsi );

				if( partitions == Lang::THE_VOID ){
					RefCountPtr< const Lang::SingleList > noPartitions = RefCountPtr< const Lang::SingleList >( NullPtr< const Lang::SingleList >( ) );
					evalState->cont_ = Kernel::ContRef( new Kernel::ForcingListContinuation
																							( Kernel::ContRef( new Kernel::Core_graph_cont_nodes( edges, args.getLoc( edgesi ), domain, noPartitions, evalState->cont_, args.getLoc( partitionsi ), args.getLoc( nodesi ) ) ),
																								args.getLoc( nodesi ),
																								true ) );
					Kernel::ContRef cont = evalState->cont_;
					cont->takeValue( nodes, evalState );
				}else{
					evalState->cont_ = Kernel::ContRef( new Kernel::ForcingListContinuation
																							( Kernel::ContRef( new Kernel::Core_graph_cont_partitions( nodes, args.getLoc( nodesi ), edges, args.getLoc( edgesi ), domain, evalState->cont_, args.getLoc( partitionsi ) ) ),
																								args.getLoc( partitionsi ),
																								true ) );
					Kernel::ContRef cont = evalState->cont_;
					cont->takeValue( partitions, evalState );
				}
			}
		};

		class Core_walk : public Lang::CoreFunction
		{
		public:
			Core_walk( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name )
				: CoreFunction( ns, name, new Kernel::EvaluatedFormals( Ast::FileID::build_internal( name ), true ) )
			{
				formals_->appendEvaluatedCoreFormal( "edges", Kernel::THE_NIL_VARIABLE );
				formals_->appendEvaluatedCoreFormal( "start", Kernel::THE_VOID_VARIABLE );
				formals_->appendEvaluatedCoreFormal( "end", Kernel::THE_VOID_VARIABLE );
			}
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				args.applyDefaults( callLoc );

				size_t argsi = 0;
				typedef const Lang::SingleList ListType;
				size_t edgesi = argsi;
				RefCountPtr< ListType > edges = Helpers::down_cast_CoreArgument< ListType >( id_, args, argsi, callLoc );

				++argsi;
				typedef const Lang::Node NodeType;
				/* The start and end may be null pointers, and it is not until the Lang::Graph constructor that
				 * these will be replaced by automatically determined nodes.
				 */
				RefCountPtr< NodeType > start = Helpers::down_cast_CoreArgument< NodeType >( id_, args, argsi, callLoc, true );

				++argsi;
				size_t endi = argsi;
				RefCountPtr< NodeType > end = Helpers::down_cast_CoreArgument< NodeType >( id_, args, argsi, callLoc, true );

				RefCountPtr< const Lang::Graph > graph = RefCountPtr< const Lang::Graph >( NullPtr< const Lang::Graph >( ) );
				if( start != NullPtr< const Lang::Node >( ) ){
					graph = start->graph( );
					if( end != NullPtr< const Lang::Node >( ) &&
							end->graph( ) != graph ){
						throw Exceptions::CoreRequirement( "The end node must belong to the same graph as the start node.", id_, args.getLoc( endi ) );
					}
				}else if( end != NullPtr< const Lang::Node >( ) ){
					graph = end->graph( );
				}else if( const Lang::SingleListPair * ptr = dynamic_cast< const Lang::SingleListPair * >( edges.getPtr( ) ) ){
					RefCountPtr< const Lang::Value > firstEdgeVal = ptr->car_->getUntyped( );
					const Lang::Edge * firstEdge = dynamic_cast< const Lang::Edge * >( firstEdgeVal.getPtr( ) );
					if( firstEdge == 0 ){
						throw Exceptions::TypeMismatch( callLoc, "First edge", firstEdgeVal->getTypeName( ), Lang::Edge::staticTypeName( ) );
					}
					graph = firstEdge->graph( );
				}else{
					throw Exceptions::CoreRequirement( "An empty walk must be specified with a start or end node.", id_, callLoc );
				}

				evalState->cont_ = Kernel::ContRef( new Kernel::ForcingListContinuation
																						( Kernel::ContRef( new Kernel::Core_walk_cont_edges( graph, start, end, args.getLoc( edgesi ), evalState->cont_, callLoc ) ),
																							args.getLoc( edgesi ),
																							true ) );
				Kernel::ContRef cont = evalState->cont_;
				cont->takeValue( edges, evalState );
			}
		};

		class Core_interpolate : public Lang::CoreFunction
		{
		public:
			Core_interpolate( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name )
				: CoreFunction( ns, name, new Kernel::EvaluatedFormals( Ast::FileID::build_internal( name ), true ) )
			{
				formals_->appendEvaluatedCoreFormal( "function", Kernel::THE_SLOT_VARIABLE );
				formals_->appendEvaluatedCoreFormal( "domain", Kernel::THE_SLOT_VARIABLE );
				formals_->appendEvaluatedCoreFormal( "size", Kernel::THE_SLOT_VARIABLE );
				formals_->appendEvaluatedCoreFormal( "range", Kernel::THE_SLOT_VARIABLE );
				formals_->appendEvaluatedCoreFormal( "encode", Kernel::THE_VOID_VARIABLE );
				formals_->appendEvaluatedCoreFormal( "decode", Kernel::THE_VOID_VARIABLE );
			}
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				args.applyDefaults( callLoc );

				throw Exceptions::NotImplemented( "Core_interpolate::call" );
			}
		};

		class Core_importPDFpages : public Lang::CoreFunction
		{
		public:
			Core_importPDFpages( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name ) : CoreFunction( ns, name ) { }
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				const size_t ARITY = 1;
				CHECK_ARITY( args, ARITY, id_ );

				typedef const Lang::String ArgType;
				RefCountPtr< ArgType > arg = Helpers::down_cast_CoreArgument< ArgType >( id_, args, 0, callLoc );

				std::string filename;
				try
					{
						filename = Ast::theShapesScanner.searchFile( arg->val_.getPtr( ), true ); /* true means that errors should be handled in "runtime mode". */
					}
				catch( RefCountPtr< const char > & msg )
					{
						throw Exceptions::CoreOutOfRange( id_, args, 0, msg );
					}

				RefCountPtr< std::ifstream > iFile( new std::ifstream( filename.c_str( ) ) );
				if( ! iFile->good( ) )
					{
						std::ostringstream msg;
						msg << "Failed to open file for input: " << arg->val_.getPtr( ) ;
						throw Exceptions::CoreOutOfRange( id_, args, 0, strrefdup( msg ) );
					}
				RefCountPtr< SimplePDF::PDF_in > pdfi( new SimplePDF::PDF_in( iFile ) );

				RefCountPtr< const std::vector< RefCountPtr< const Lang::XObject > > > typedRes = RefCountPtr< const std::vector< RefCountPtr< const Lang::XObject > > >( NullPtr< std::vector< RefCountPtr< const Lang::XObject > > >( ) );
				try
					{
						typedRes = Kernel::thePDFImporter.addPagesAsXObjects( pdfi );
					}
				catch( const char * ball )
					{
						throw Exceptions::InternalError( strrefdup( ( std::string( "An error occurred while importing " ) + arg->val_.getPtr( ) + ": " + ball ).c_str( ) ) );
					}

				std::vector< RefCountPtr< const Lang::Value > > * untypedRes = new std::vector< RefCountPtr< const Lang::Value > >;
				untypedRes->reserve( typedRes->size( ) );
				typedef typeof *typedRes ListType;
				for( ListType::const_iterator i = typedRes->begin( ); i != typedRes->end( ); ++i )
					{
						untypedRes->push_back( *i );
					}

				Kernel::ContRef cont = evalState->cont_;
				cont->takeValue( Kernel::ValueRef( new Lang::VectorFunction( untypedRes ) ),
												 evalState );
			}
		};

		namespace Implementation
		{
			double
			svg_path_strtod( const char ** src )
			{
				const char * start = *src;
				char * endp;
				double res = strtod( start, & endp );
				*src = endp;
				if( **src != ' ' )
					{
						for( ; *start == ' '; ++start )
							;
						throw std::string( start, strchr( start, ' ' ) - start );
					}
				return res;
			}
		}
		class Core_svg_path : public Lang::CoreFunction
		{
		public:
			Core_svg_path( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name )
				: CoreFunction( ns, name, new Kernel::EvaluatedFormals( Ast::FileID::build_internal( name ), true ) )
			{
				formals_->appendEvaluatedCoreFormal( "d", Kernel::THE_SLOT_VARIABLE );
				formals_->appendEvaluatedCoreFormal( "xunit", Helpers::newValHandle( new Lang::Length( Concrete::Length( 1 ) ) ) );
				formals_->appendEvaluatedCoreFormal( "yunit", Helpers::newValHandle( new Lang::Length( Concrete::Length( 1 ) ) ) );
				formals_->appendEvaluatedCoreFormal( "multi", Kernel::THE_VOID_VARIABLE );
				formals_->appendEvaluatedCoreFormal( "singletons", Kernel::THE_TRUE_VARIABLE );
			}
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				args.applyDefaults( callLoc );

				size_t argsi = 0;
				typedef const Lang::String StrType;
				RefCountPtr< StrType > arg = Helpers::down_cast_CoreArgument< StrType >( id_, args, argsi, callLoc );

				typedef const Lang::Length ScaleType;
				++argsi;
				Concrete::Length dx = Helpers::down_cast_CoreArgument< ScaleType >( id_, args, argsi, callLoc )->get( );
				++argsi;
				Concrete::Length dy = Helpers::down_cast_CoreArgument< ScaleType >( id_, args, argsi, callLoc )->get( );

				typedef const Lang::Boolean FlagType;
				++argsi;
				RefCountPtr< FlagType > multi = Helpers::down_cast_CoreArgument< FlagType >( id_, args, argsi, callLoc, true );
				++argsi;
				bool singletons = Helpers::down_cast_CoreArgument< FlagType >( id_, args, argsi, callLoc )->val_;

				RefCountPtr< char > buf = RefCountPtr< char >( new char[ 2 * strlen( arg->val_.getPtr( ) + 1 ) ] );
				{
					/* Get rid of any whitespace that isn't a plain space, and make sure every number is terminated by whitespace.
					 */
					char * dst = buf.getPtr( );
					for( const char * src = arg->val_.getPtr( ); *src != '\0'; ++src, ++dst )
						{
							switch( *src )
								{
								case '\t':
								case '\n':
								case ',':
									*dst = ' ';
									continue;
								}
							if( ( 'A' <= *src && *src <= 'Z' ) || ( 'a' <= *src && *src <= 'z' ) )
								{
									/* Insert whitespace before command to ensure command is not immediately following number.
									 */
									*dst = ' ';
									++dst;
								}
							*dst = *src;
						}
					*dst = ' ';
					++dst;
					*dst = '\0';
				}

				RefCountPtr< Lang::MultiPath2D > multiPath = RefCountPtr< Lang::MultiPath2D >( new Lang::MultiPath2D );
				RefCountPtr< Lang::ElementaryPath2D > elemPath = RefCountPtr< Lang::ElementaryPath2D >( NullPtr< Lang::ElementaryPath2D >( ) );

				try
					{
						char cmd = '\0';
						Concrete::PathPoint2D originPathPoint( new Concrete::Coords2D( 0, 0 ) );
						Concrete::PathPoint2D * first = & originPathPoint;
						Concrete::PathPoint2D * last = & originPathPoint;
						const char * srcEnd = buf.getPtr( ) + strlen( buf.getPtr( ) );
						for( const char * src = buf.getPtr( ); src < srcEnd; ++src )
							{
								for( ; *src == ' '; ++src )
									;
								if( *src == '\0' )
									{
										break;
									}
								switch( *src )
									{
									case 'M':
									case 'm':
									case 'Z':
									case 'z':
									case 'L':
									case 'l':
									case 'H':
									case 'h':
									case 'V':
									case 'v':
									case 'C':
									case 'c':
									case 'S':
									case 's':
										{
											cmd = *src;
											++src;
											break;
										}
									case 'Q':
									case 'q':
									case 'T':
									case 't':
									case 'A':
									case 'a':
										{
											throw Exceptions::CoreOutOfRange( id_, args, 0, strrefdup( std::string( "SVG path command not compatible with cubic splines: " ) + *src ) );
										}
									}
								switch( cmd )
									{
									case 'M':
										{
											if( elemPath != NullPtr< Lang::ElementaryPath2D >( ) && ( singletons || elemPath->duration( ) >= 1 ) )
												{
													multiPath->push_back( elemPath );
												}
											Concrete::Length x = dx * Implementation::svg_path_strtod( & src );
											Concrete::Length y = - dy * Implementation::svg_path_strtod( & src );
											first = new Concrete::PathPoint2D( new Concrete::Coords2D( x, y ) );
											last = first;
											elemPath = RefCountPtr< Lang::ElementaryPath2D >( new Lang::ElementaryPath2D );
											elemPath->push_back( last );
											cmd = 'L';
											break;
										}
									case 'm':
										{
											if( elemPath != NullPtr< Lang::ElementaryPath2D >( ) && ( singletons || elemPath->duration( ) >= 1 ) )
												{
													multiPath->push_back( elemPath );
												}
											Concrete::Length x = last->mid_->x_ + dx * Implementation::svg_path_strtod( & src );
											Concrete::Length y = last->mid_->y_ - dy * Implementation::svg_path_strtod( & src );
											first = new Concrete::PathPoint2D( new Concrete::Coords2D( x, y ) );
											last = first;
											elemPath = RefCountPtr< Lang::ElementaryPath2D >( new Lang::ElementaryPath2D );
											elemPath->push_back( last );
											cmd = 'l';
											break;
										}
									case 'Z':
										{
											elemPath->close( );
											last = first;
											break;
										}
									case 'z':
										{
											elemPath->close( );
											last = first;
											break;
										}
									case 'L':
										{
											Concrete::Length x = dx * Implementation::svg_path_strtod( & src );
											Concrete::Length y = - dy * Implementation::svg_path_strtod( & src );
											last = new Concrete::PathPoint2D( new Concrete::Coords2D( x, y ) );
											elemPath->push_back( last );
											break;
										}
									case 'l':
										{
											Concrete::Length x = last->mid_->x_ + dx * Implementation::svg_path_strtod( & src );
											Concrete::Length y = last->mid_->y_ - dy * Implementation::svg_path_strtod( & src );
											last = new Concrete::PathPoint2D( new Concrete::Coords2D( x, y ) );
											elemPath->push_back( last );
											break;
										}
									case 'H':
										{
											Concrete::Length x = dx * Implementation::svg_path_strtod( & src );
											last = new Concrete::PathPoint2D( new Concrete::Coords2D( x, last->mid_->y_ ) );
											elemPath->push_back( last );
											break;
										}
									case 'h':
										{
											Concrete::Length x = last->mid_->x_ + dx * Implementation::svg_path_strtod( & src );
											last = new Concrete::PathPoint2D( new Concrete::Coords2D( x, last->mid_->y_ ) );
											elemPath->push_back( last );
											break;
										}
									case 'V':
										{
											Concrete::Length y = - dy * Implementation::svg_path_strtod( & src );
											last = new Concrete::PathPoint2D( new Concrete::Coords2D( last->mid_->x_, y ) );
											elemPath->push_back( last );
											break;
										}
									case 'v':
										{
											Concrete::Length y = last->mid_->y_ - dy * Implementation::svg_path_strtod( & src );
											last = new Concrete::PathPoint2D( new Concrete::Coords2D( last->mid_->x_, y ) );
											elemPath->push_back( last );
											break;
										}
									case 'C':
										{
											Concrete::Length x1 = dx * Implementation::svg_path_strtod( & src );
											Concrete::Length y1 = - dy * Implementation::svg_path_strtod( & src );
											Concrete::Length x2 = dx * Implementation::svg_path_strtod( & src );
											Concrete::Length y2 = - dy * Implementation::svg_path_strtod( & src );
											Concrete::Length x3 = dx * Implementation::svg_path_strtod( & src );
											Concrete::Length y3 = - dy * Implementation::svg_path_strtod( & src );
											last->front_ = new Concrete::Coords2D( x1, y1 );
											last = new Concrete::PathPoint2D( new Concrete::Coords2D( x3, y3 ) );
											last->rear_ = new Concrete::Coords2D( x2, y2 );
											elemPath->push_back( last );
											break;
										}
									case 'c':
										{
											Concrete::Length x1 = last->mid_->x_ + dx * Implementation::svg_path_strtod( & src );
											Concrete::Length y1 = last->mid_->y_ - dy * Implementation::svg_path_strtod( & src );
											Concrete::Length x2 = last->mid_->x_ + dx * Implementation::svg_path_strtod( & src );
											Concrete::Length y2 = last->mid_->y_ - dy * Implementation::svg_path_strtod( & src );
											Concrete::Length x3 = last->mid_->x_ + dx * Implementation::svg_path_strtod( & src );
											Concrete::Length y3 = last->mid_->y_ - dy * Implementation::svg_path_strtod( & src );
											last->front_ = new Concrete::Coords2D( x1, y1 );
											last = new Concrete::PathPoint2D( new Concrete::Coords2D( x3, y3 ) );
											last->rear_ = new Concrete::Coords2D( x2, y2 );
											elemPath->push_back( last );
											break;
										}
									case 'S':
										{
											Concrete::Length x2 = dx * Implementation::svg_path_strtod( & src );
											Concrete::Length y2 = - dy * Implementation::svg_path_strtod( & src );
											Concrete::Length x3 = dx * Implementation::svg_path_strtod( & src );
											Concrete::Length y3 = - dy * Implementation::svg_path_strtod( & src );
											last->front_ = new Concrete::Coords2D( 2 * *(last->mid_) - *(last->rear_) );
											last = new Concrete::PathPoint2D( new Concrete::Coords2D( x3, y3 ) );
											last->rear_ = new Concrete::Coords2D( x2, y2 );
											elemPath->push_back( last );
											break;
										}
									case 's':
										{
											Concrete::Length x2 = last->mid_->x_ + dx * Implementation::svg_path_strtod( & src );
											Concrete::Length y2 = last->mid_->y_ - dy * Implementation::svg_path_strtod( & src );
											Concrete::Length x3 = last->mid_->x_ + dx * Implementation::svg_path_strtod( & src );
											Concrete::Length y3 = last->mid_->y_ - dy * Implementation::svg_path_strtod( & src );
											last->front_ = new Concrete::Coords2D( 2 * *(last->mid_) - *(last->rear_) );
											last = new Concrete::PathPoint2D( new Concrete::Coords2D( x3, y3 ) );
											last->rear_ = new Concrete::Coords2D( x2, y2 );
											elemPath->push_back( last );
											break;
										}
									case '\0':
										throw Exceptions::CoreOutOfRange( id_, args, 0, "Malformed SVG path string (failed to initialize command character)." );
									default:
										{
											throw Exceptions::InternalError( "While reading SVG path string: Command character out of range." );
										}
									}
							}
					}
				catch( const std::string & badNumber )
					{
						throw Exceptions::CoreOutOfRange( id_, args, 0, strrefdup( std::string( "Ill-formed number: " ) + badNumber ) );
					}

				if( elemPath != NullPtr< Lang::ElementaryPath2D >( ) && ( singletons || elemPath->duration( ) >= 1 ) )
					{
						multiPath->push_back( RefCountPtr< const Lang::Path2D >( elemPath ) );
					}

				if( multiPath->size( ) == 0 )
					{
						throw Exceptions::CoreOutOfRange( id_, args, 0, "No path was produced." );
					}
				if( multiPath->size( ) == 1 )
					{
						Kernel::ContRef cont = evalState->cont_;
						if( multi != NullPtr< FlagType >( ) && multi->val_ )
							{
								cont->takeValue( Kernel::ValueRef( multiPath ),
																 evalState );
							}
						else
							{
								cont->takeValue( Kernel::ValueRef( multiPath->front( ) ),
																 evalState );
							}
					}
				else
					{
						if( multi != NullPtr< FlagType >( ) && ! multi->val_ )
							{
								throw Exceptions::CoreOutOfRange( id_, args, 0, "More than one sub-path conflicts with false for <multi>." );
							}
						Kernel::ContRef cont = evalState->cont_;
						cont->takeValue( Kernel::ValueRef( multiPath ),
														 evalState );
					}
			}
		};

		class Core_sprintf : public Lang::CoreFunction
		{
		public:
			Core_sprintf( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name ) : CoreFunction( ns, name ) { }
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				if( args.empty( ) )
					{
						throw Exceptions::CoreArityMismatch( id_, 1, args.size( ) );
					}
				size_t i = 0;
				typedef const Lang::String Arg1Type;
				RefCountPtr< Arg1Type > arg1 = Helpers::down_cast_CoreArgument< Arg1Type >( id_, args, i, callLoc );

				/* snprintf( 0, 0, ... ) does not seem to work properly on some systems.
				 * Therefore, I resort to the use of a dummy string, and "n = 1".
				 */
				char * res = 0;
				int status;
				char dummy[1];
				switch( args.size( ) )
					{
					case 1:
						{
							size_t sz = snprintf( dummy, 1, arg1->val_.getPtr( ), 0 ); /* The final 0 is just a dummy argument that makes the compiler relax. */
							res = new char[ sz + 1 ];
							status = sprintf( res, arg1->val_.getPtr( ), 0 ); /* The final 0 is just a dummy argument that makes the compiler relax. */
						}
						break;
					case 2:
						{
							++i;
							{
								typedef const Lang::String Arg2Type;
								Arg2Type * arg2 = dynamic_cast< Arg2Type * >( args.getValue( i ).getPtr( ) );
								if( arg2 != 0 )
									{
										size_t sz = snprintf( dummy, 1, arg1->val_.getPtr( ), arg2->val_.getPtr( ) );
										res = new char[ sz + 1 ];
										status = sprintf( res, arg1->val_.getPtr( ), arg2->val_.getPtr( ) );
										break;
									}
							}
							{
								typedef const Lang::Float Arg2Type;
								Arg2Type * arg2 = dynamic_cast< Arg2Type * >( args.getValue( i ).getPtr( ) );
								if( arg2 != 0 )
									{
										size_t sz = snprintf( dummy, 1, arg1->val_.getPtr( ), arg2->val_ );
										res = new char[ sz + 1 ];
										status = sprintf( res, arg1->val_.getPtr( ), arg2->val_ );
										break;
									}
							}
							{
								typedef const Lang::Integer Arg2Type;
								Arg2Type * arg2 = dynamic_cast< Arg2Type * >( args.getValue( i ).getPtr( ) );
								if( arg2 != 0 )
									{
										size_t sz = snprintf( dummy, 1, arg1->val_.getPtr( ), arg2->val_ );
										res = new char[ sz + 1 ];
										status = sprintf( res, arg1->val_.getPtr( ), arg2->val_ );
										break;
									}
							}
							{
								typedef const Lang::ChronologicalTime Arg2Type;
								Arg2Type * arg2 = dynamic_cast< Arg2Type * >( args.getValue( i ).getPtr( ) );
								if( arg2 != 0 )
									{
										const char * fmt = arg1->val_.getPtr( );
										const struct tm * tmp = arg2->temporary_localtime( );
										size_t sz = strlen( fmt ) * 2;
										res = new char[ sz ];
										while( strftime( res, sz, fmt, tmp ) == 0 )
											{
												delete res;
												sz *= 2;
												res = new char[ sz ];
											}
										status = 0; // Here, I'd like to check some error condition instead...
										break;
									}
							}
							throw Exceptions::CoreTypeMismatch( callLoc, id_, args, i, Interaction::SEVERAL_TYPES );
						}
						break;
					default:
						throw Exceptions::CoreOutOfRange( id_, args, 0, "The number of arguments is out of the implemented range." );
					}

				if( res == 0 )
					{
						throw Exceptions::InternalError( "Failed to assign to res in sprintf." );
					}

				if( status < 0 )
					{
						std::ostringstream oss;
						oss << "Call to library routine returned negative value indicating an error: " << status << ".";
						throw Exceptions::CoreOutOfRange( id_, args, 0, strrefdup( oss ) );
					}

				Kernel::ContRef cont = evalState->cont_;
				cont->takeValue( Kernel::ValueRef( new Lang::String( res, false ) ),
												 evalState );
			}
		};

		class Core_strftime : public Lang::CoreFunction
		{
		public:
			Core_strftime( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name ) : CoreFunction( ns, name ) { }
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				const size_t ARITY = 0;
				CHECK_ARITY( args, ARITY, id_ );

				time_t t;
				tm * timeInfo;
				t = time( 0 );
				timeInfo = localtime( & t );
				std::ostringstream res;
				res << timeInfo->tm_hour << ":" << timeInfo->tm_min << ":" << timeInfo->tm_sec ;

				Kernel::ContRef cont = evalState->cont_;
				cont->takeValue( Kernel::ValueRef( new Lang::String( strrefdup( res ) ) ),
												 evalState );
			}
		};

		class Core_ambient_light : public Lang::CoreFunction
		{
		public:
			Core_ambient_light( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name )
				: CoreFunction( ns, name, new Kernel::EvaluatedFormals( Ast::FileID::build_internal( name ), true ) )
			{
				formals_->appendEvaluatedCoreFormal( "color", Kernel::THE_SLOT_VARIABLE );
			}
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				args.applyDefaults( callLoc );

				{
					typedef const Lang::Gray ArgType;
					ArgType * col = dynamic_cast< ArgType * >( args.getValue( 0 ).getPtr( ) );
					if( col != 0 )
						{
							Kernel::ContRef cont = evalState->cont_;
							cont->takeValue( Kernel::ValueRef( new Lang::AmbientLightGray( col->components( ) ) ),
															 evalState );
							return;
						}
				}

				{
					typedef const Lang::RGB ArgType;
					ArgType * col = dynamic_cast< ArgType * >( args.getValue( 0 ).getPtr( ) );
					if( col != 0 )
						{
							Kernel::ContRef cont = evalState->cont_;
							cont->takeValue( Kernel::ValueRef( new Lang::AmbientLightRGB( col->components( ) ) ),
															 evalState );
							return;
						}
				}

				throw Exceptions::CoreTypeMismatch( callLoc, id_, args, 0, Helpers::typeSetString( Lang::Gray::staticTypeName( ), Lang::RGB::staticTypeName( ) ) );
			}
		};

		class Core_specular_light : public Lang::CoreFunction
		{
		public:
			Core_specular_light( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name )
				: CoreFunction( ns, name, new Kernel::EvaluatedFormals( Ast::FileID::build_internal( name ), true ) )
			{
				formals_->appendEvaluatedCoreFormal( "color", Kernel::THE_SLOT_VARIABLE );
				formals_->appendEvaluatedCoreFormal( "radius", Helpers::newValHandle( new Lang::Length( HUGE_VAL ) ) );
				formals_->appendEvaluatedCoreFormal( "shadows", Kernel::THE_FALSE_VARIABLE );
			}
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				args.applyDefaults( callLoc );

				typedef const Lang::Length RadiusType;
				RefCountPtr< RadiusType > radius = Helpers::down_cast_CoreArgument< RadiusType >( id_, args, 1, callLoc );
				typedef const Lang::Boolean ShadowsType;
				RefCountPtr< ShadowsType > shadows = Helpers::down_cast_CoreArgument< ShadowsType >( id_, args, 2, callLoc );

				{
					typedef const Lang::Gray ArgType;
					ArgType * col = dynamic_cast< ArgType * >( args.getValue( 0 ).getPtr( ) );
					if( col != 0 )
						{
							Kernel::ContRef cont = evalState->cont_;
							cont->takeValue( Kernel::ValueRef( new Lang::SpecularLightGray( Concrete::Coords3D( 0, 0, 0 ),
																																							col->components( ),
																																							radius->get( ),
																																							shadows->val_ ) ),
															 evalState );
							return;
						}
				}

				{
					typedef const Lang::RGB ArgType;
					ArgType * col = dynamic_cast< ArgType * >( args.getValue( 0 ).getPtr( ) );
					if( col != 0 )
						{
							Kernel::ContRef cont = evalState->cont_;
							cont->takeValue( Kernel::ValueRef( new Lang::SpecularLightRGB( Concrete::Coords3D( 0, 0, 0 ),
																																						 col->components( ),
																																						 radius->get( ),
																																						 shadows->val_ ) ),
															 evalState );
							return;
						}
				}

				throw Exceptions::CoreTypeMismatch( callLoc, id_, args, 0, Helpers::typeSetString( Lang::Gray::staticTypeName( ), Lang::RGB::staticTypeName( ) ) );
			}
		};

		class Core_distant_light : public Lang::CoreFunction
		{
		public:
			Core_distant_light( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name )
				: CoreFunction( ns, name, new Kernel::EvaluatedFormals( Ast::FileID::build_internal( name ), true ) )
			{
				formals_->appendEvaluatedCoreFormal( "color", Kernel::THE_SLOT_VARIABLE );
				formals_->appendEvaluatedCoreFormal( "shadows", Kernel::THE_FALSE_VARIABLE );
			}
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				args.applyDefaults( callLoc );

				typedef const Lang::Boolean ShadowsType;
				RefCountPtr< ShadowsType > shadows = Helpers::down_cast_CoreArgument< ShadowsType >( id_, args, 1, callLoc );

				{
					typedef const Lang::Gray ArgType;
					ArgType * col = dynamic_cast< ArgType * >( args.getValue( 0 ).getPtr( ) );
					if( col != 0 )
						{
							Kernel::ContRef cont = evalState->cont_;
							cont->takeValue( Kernel::ValueRef( new Lang::DistantLightGray( Concrete::UnitFloatTriple( 0., 0., -1. ),
																																						 col->components( ),
																																						 shadows->val_ ) ),
															 evalState );
							return;
						}
				}

				{
					typedef const Lang::RGB ArgType;
					ArgType * col = dynamic_cast< ArgType * >( args.getValue( 0 ).getPtr( ) );
					if( col != 0 )
						{
							Kernel::ContRef cont = evalState->cont_;
							cont->takeValue( Kernel::ValueRef( new Lang::DistantLightRGB( Concrete::UnitFloatTriple( 0., 0., -1. ),
																																						col->components( ),
																																						shadows->val_ ) ),
															 evalState );
							return;
						}
				}

				throw Exceptions::CoreTypeMismatch( callLoc, id_, args, 0, Helpers::typeSetString( Lang::Gray::staticTypeName( ), Lang::RGB::staticTypeName( ) ) );
			}
		};

		class Core_textrenderingmode : public Lang::CoreFunction
		{
		public:
			Core_textrenderingmode( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name )
				: CoreFunction( ns, name, new Kernel::EvaluatedFormals( Ast::FileID::build_internal( name ), true ) )
			{
				formals_->appendEvaluatedCoreFormal( "fill", Kernel::THE_FALSE_VARIABLE );
				formals_->appendEvaluatedCoreFormal( "stroke", Kernel::THE_FALSE_VARIABLE );
			}
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				args.applyDefaults( callLoc );

				typedef const Lang::Boolean FlagType;
				RefCountPtr< FlagType > fill =	 Helpers::down_cast_CoreArgument< FlagType >( id_, args, 0, callLoc );
				RefCountPtr< FlagType > stroke = Helpers::down_cast_CoreArgument< FlagType >( id_, args, 1, callLoc );

				Kernel::ContRef cont = evalState->cont_;
				cont->takeValue( Kernel::ValueRef( new Lang::TextRenderingMode( fill->val_, stroke->val_, false ) ),
												 evalState );
			}
		};

		class Core_manualkern : public Lang::CoreFunction
		{
		public:
			Core_manualkern( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name ) : CoreFunction( ns, name ) { }
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				// Note that res is _not_ yet const.	We use a RefCountPtr to take care of memory deallocation in case some argument has the wrong type and
				// the result is not used.
				RefCountPtr< Lang::KernedText > res( new Lang::KernedText( evalState->dyn_->getTextState( ), evalState->dyn_->getGraphicsState( ) ) );

				for( size_t i = 0; i != args.size( ); ++i )
					{
						try
							{
								typedef const Lang::String ArgType;
								res->pushString( Helpers::try_cast_CoreArgument< ArgType >( args.getValue( i ) ) );
								continue;
							}
						catch( const NonLocalExit::NotThisType & ball )
							{
								/* Never mind, see below. */
							}

						try
							{
								typedef const Lang::Float ArgType;
								res->pushKerning( Helpers::try_cast_CoreArgument< ArgType >( args.getValue( i ) )->val_ );
								continue;
							}
						catch( const NonLocalExit::NotThisType & ball )
							{
								/* Never mind, see below. */
							}

						try
							{
								typedef const Lang::KernedText ArgType;
								Helpers::try_cast_CoreArgument< ArgType >( args.getValue( i ) )->push( res.getPtr( ) );
								continue;
							}
						catch( const NonLocalExit::NotThisType & ball )
							{
								/* Never mind, see below. */
							}

						throw Exceptions::CoreTypeMismatch( callLoc, id_, args, i, Helpers::typeSetString( Lang::String::staticTypeName( ), Lang::Float::staticTypeName( ), Lang::KernedText::staticTypeName( ) ) );
					}

				Kernel::ContRef cont = evalState->cont_;
				cont->takeValue( res,
												 evalState );
			}
		};

		class Core_automatickern : public Lang::CoreFunction
		{
		public:
			Core_automatickern( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name ) : CoreFunction( ns, name ) { }
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				iconv_t converter = Helpers::requireUTF8ToUCS4Converter( );
				iconv_t backconverter = Helpers::requireUCS4ToUTF8Converter( );

				RefCountPtr< const FontMetrics::FontMetric > metrics = evalState->dyn_->getTextState( )->font_->metrics( );
				if( metrics->horizontalMetrics_ == NullPtr< FontMetrics::WritingDirectionMetrics >( ) )
					{
						throw Exceptions::FontMetricsError( evalState->dyn_->getTextState( )->font_->fontName( ), strrefdup( "No horizontal metrics defined." ) );
					}

				// Note that res is _not_ yet const.	We use a RefCountPtr to take care of memory deallocation in case some argument has the wrong type and
				// the result is not used.
				RefCountPtr< Lang::KernedText > res( new Lang::KernedText( evalState->dyn_->getTextState( ), evalState->dyn_->getGraphicsState( ) ) );

				std::ostringstream pendingChars;
				Kernel::UnicodeCodePoint prevChar( 0 );
				double pendingKerning = 0;

				const size_t backbufSize = 5;
				char backbuf[ backbufSize ];

				for( size_t i = 0; i != args.size( ); ++i )
					{
						try
							{
								typedef const Lang::String ArgType;
								RefCountPtr< ArgType > str = Helpers::try_cast_CoreArgument< ArgType >( args.getValue( i ) );

								const char * inbuf = str->val_.getPtr( );
								size_t inbytesleft = strlen( inbuf );

								size_t bufSize = 4 * inbytesleft;
								char * buf = new char[ bufSize ];
								DeleteOnExit< char > bufDeleter( buf );

								char * outbuf = buf;
								size_t outbytesleft = bufSize;
								// The ICONV_CAST macro is defined in config.h.
								size_t count = iconv( converter,
																			ICONV_CAST( & inbuf ), & inbytesleft,
																			& outbuf, & outbytesleft );
								if( count == (size_t)(-1) )
									{
										if( errno == EILSEQ )
											{
												throw Exceptions::MiscellaneousRequirement( "It is suspected that one of the UFT-8 characters used in showed text has no UCS-4 representation." );
											}
										else if( errno == EINVAL )
											{
												throw Exceptions::MiscellaneousRequirement( "It is suspected that showed text ended with an incomplete multibyte character." );
											}
										else if( errno == E2BIG )
											{
												throw Exceptions::InternalError( "Core_automatickern: The buffer allocated for UTF-8 to UCS-4 conversion was too small." );
											}
										else
											{
												std::ostringstream msg;
												msg << "iconv failed with an unrecognized error code: " << errno ;
												throw Exceptions::InternalError( strrefdup( msg ) );
											}
									}
								for( const char * src = buf; src != outbuf; src += 4 )
									{
										Kernel::UnicodeCodePoint currentChar;
										currentChar.decode_UCS4( src );
										double currentKerning = pendingKerning - metrics->getHorizontalKernPairXByCode( prevChar, currentChar );
										prevChar = currentChar;
										pendingKerning = 0;
										if( currentKerning != 0 )
											{
												if( ! pendingChars.str( ).empty( ) )
													{
														res->pushString( RefCountPtr< const Lang::String >( new Lang::String( strrefdup( pendingChars ) ) ) );
														pendingChars.str( "" );
													}
												res->pushKerning( currentKerning );
											}

										// Copy the current (multibyte) character to the character queue
										{
											const char * inbuf = src;
											char * outbuf = backbuf;
											size_t inbytesleft = 4;
											size_t outbytesleft = backbufSize;
											// The ICONV_CAST macro is defined in config.h.
											size_t count = iconv( backconverter,
																						ICONV_CAST( & inbuf ), & inbytesleft,
																						& outbuf, & outbytesleft );
											if( count == (size_t)(-1) )
												{
													if( errno == EILSEQ )
														{
															throw Exceptions::ExternalError( "A character converted from UTF-8 could not be converted back to UFT-8." );
														}
													else if( errno == EINVAL )
														{
															throw Exceptions::ExternalError( "A character converted from UTF-8 was deemed incomplete." );
														}
													else if( errno == E2BIG )
														{
															throw Exceptions::InternalError( "The buffer allocated for conversion of a single character back to UTF-8 was too small." );
														}
													else
														{
															std::ostringstream msg;
															msg << "iconv failed with an unrecognized error code: " << errno ;
															throw Exceptions::InternalError( strrefdup( msg ) );
														}
												}
											*outbuf = '\0';
											pendingChars << backbuf ;
										}
									}
								continue;
							}
						catch( const NonLocalExit::NotThisType & ball )
							{
								/* Never mind, see below. */
							}

						try
							{
								typedef const Lang::Float ArgType;
								pendingKerning += Helpers::try_cast_CoreArgument< ArgType >( args.getValue( i ) )->val_;
								continue;
							}
						catch( const NonLocalExit::NotThisType & ball )
							{
								/* Never mind, see below. */
							}

						throw Exceptions::CoreTypeMismatch( callLoc, id_, args, i, Helpers::typeSetString( Lang::String::staticTypeName( ), Lang::Float::staticTypeName( ) ) );
					}

				if( ! pendingChars.str( ).empty( ) )
					{
						res->pushString( RefCountPtr< const Lang::String >( new Lang::String( strrefdup( pendingChars ) ) ) );
						pendingChars.str( "" );
					}

				Kernel::ContRef cont = evalState->cont_;
				cont->takeValue( res,
												 evalState );
			}
		};

		class Core_newrandom : public Lang::CoreFunction
		{
		public:
			Core_newrandom( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name )
				: CoreFunction( ns, name, new Kernel::EvaluatedFormals( Ast::FileID::build_internal( name ), true ) )
			{
				formals_->appendEvaluatedCoreFormal( "seed", Kernel::THE_SLOT_VARIABLE );
				formals_->appendEvaluatedCoreFormal( "size", Helpers::newValHandle( new Lang::Integer( 32 ) ) );
			}
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				args.applyDefaults( callLoc );

				typedef const Lang::Integer SizeType;
				Lang::Integer::ValueType sz = Helpers::down_cast_CoreArgument< SizeType >( id_, args, 1, callLoc )->val_;
				if( sz < 8 )
					{
						throw Exceptions::CoreOutOfRange( id_, args, 1, "The size must be at least 8." );
					}
				if( sz > 256 )
					{
						throw Exceptions::CoreOutOfRange( id_, args, 1, "The size must at most 256." );
					}

				size_t argsi = 0;

				try
					{
						typedef const Lang::ChronologicalTime SeedType;

						RefCountPtr< SeedType > seed = Helpers::try_cast_CoreArgument< SeedType >( args.getValue( argsi ) );

						Kernel::ContRef cont = evalState->cont_;
						cont->takeValue( RefCountPtr< const Lang::Value >( new Lang::HotRandomSeed( sz, seed->val( ) ) ),
														 evalState );
						return;
					}
				catch( const NonLocalExit::NotThisType & ball )
					{
						/* Never mind, see below. */
					}

				try
					{
						typedef const Lang::Integer SeedType;

						RefCountPtr< SeedType > seed = Helpers::try_cast_CoreArgument< SeedType >( args.getValue( argsi ) );

						Kernel::ContRef cont = evalState->cont_;
						cont->takeValue( RefCountPtr< const Lang::Value >( new Lang::HotRandomSeed( sz, seed->val_ ) ),
														 evalState );
						return;
					}
				catch( const NonLocalExit::NotThisType & ball )
					{
						/* Never mind, see below. */
					}

				throw Exceptions::CoreTypeMismatch( callLoc, id_, args, argsi, Helpers::typeSetString( Lang::Integer::staticTypeName( ), Lang::ChronologicalTime::staticTypeName( ) ) );
			}
		};

		class Core_devrandom : public Lang::CoreFunction
		{
		public:
			Core_devrandom( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name )
				: CoreFunction( ns, name, new Kernel::EvaluatedFormals( Ast::FileID::build_internal( name ), true ) )
			{
				formals_->appendCoreStateFormal( "device" );
				formals_->appendEvaluatedCoreFormal( "size", Helpers::newValHandle( new Lang::Integer( 32 ) ) );
			}
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				args.applyDefaults( callLoc );

				typedef const Lang::Integer SizeType;
				Lang::Integer::ValueType sz = Helpers::down_cast_CoreArgument< SizeType >( id_, args, 0, callLoc )->val_;
				if( sz < 8 )
					{
						throw Exceptions::CoreOutOfRange( id_, args, 0, "The size must be at least 8." );
					}
				if( sz > 256 )
					{
						throw Exceptions::CoreOutOfRange( id_, args, 0, "The size must at most 256." );
					}

				typedef Kernel::WarmRandomDevice GeneratorType;
				GeneratorType * gen = Helpers::down_cast_CoreState< GeneratorType >( id_, args, 0, callLoc );

				Kernel::ContRef cont = evalState->cont_;
				cont->takeValue( RefCountPtr< const Lang::Value >( new Lang::HotRandomSeed( static_cast< size_t >( sz ), gen ) ),
												 evalState );
			}
		};

		class Core_destination : public Lang::CoreFunction
		{
		public:
			Core_destination( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name )
				: CoreFunction( ns, name, new Kernel::EvaluatedFormals( Ast::FileID::build_internal( name ), true ) )
			{
				formals_->appendEvaluatedCoreFormal( "remote", Kernel::THE_FALSE_VARIABLE );
				formals_->appendEvaluatedCoreFormal( "name", Kernel::THE_VOID_VARIABLE );
				formals_->appendEvaluatedCoreFormal( "level", Kernel::THE_VOID_VARIABLE );
				formals_->appendEvaluatedCoreFormal( "text", Kernel::THE_VOID_VARIABLE );
				formals_->appendEvaluatedCoreFormal( "open", Kernel::THE_FALSE_VARIABLE );
				formals_->appendEvaluatedCoreFormal( "bold", Kernel::THE_FALSE_VARIABLE );
				formals_->appendEvaluatedCoreFormal( "italic", Kernel::THE_FALSE_VARIABLE );
				formals_->appendEvaluatedCoreFormal( "color", Helpers::newValHandle( new Lang::RGB( Concrete::RGB( 0, 0, 0 ) ) ) );
				formals_->appendEvaluatedCoreFormal( "sides", Kernel::THE_VOID_VARIABLE );
				formals_->appendEvaluatedCoreFormal( "target", Kernel::THE_VOID_VARIABLE );
				formals_->appendEvaluatedCoreFormal( "fittobbox", Kernel::THE_VOID_VARIABLE );
				formals_->appendEvaluatedCoreFormal( "zoom", Kernel::THE_VOID_VARIABLE );
				formals_->appendEvaluatedCoreFormal( "transform", Kernel::THE_TRUE_VARIABLE );
			}
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				args.applyDefaults( callLoc );

				size_t argsi = 0;
				const size_t remove_i = argsi;
				typedef const Lang::Boolean RemoteType;
				bool remote = Helpers::down_cast_CoreArgument< RemoteType >( id_, args, argsi, callLoc )->val_;

				++argsi;
				typedef const Lang::String NameType;
				RefCountPtr< NameType > nameVal = Helpers::down_cast_CoreArgument< NameType >( id_, args, argsi, callLoc, true );
				RefCountPtr< const char > name = RefCountPtr< const char >( NullPtr< const char >( ) );
				if( nameVal != NullPtr< NameType >( ) )
					{
						const SimplePDF::PDF_Version::Version STRINGDESTS_VERSION = SimplePDF::PDF_Version::PDF_1_2;
						if( Kernel::the_PDF_version.greaterOrEqual( STRINGDESTS_VERSION ) )
							{
								name = nameVal->val_;
							}
						else
							{
								Kernel::the_PDF_version.message( STRINGDESTS_VERSION, "The naming of a destination was ignored." );
								// Note that this will leave name being null, and hence generate further errors if remote_.
							}
					}
				if( nameVal == NullPtr< NameType >( ) )	// Note why this is not an else clause!
					{
						if( remote )
							{
								throw Exceptions::CoreOutOfRange( id_, args, remove_i, "The destination cannot be remote if no name is given." );
							}
					}

				++argsi;
				const size_t outlineLevel_i = argsi;
				typedef const Lang::Integer OutlineLevelType;
				RefCountPtr< OutlineLevelType > levelVal = Helpers::down_cast_CoreArgument< OutlineLevelType >( id_, args, argsi, callLoc, true );
				int outlineLevel = -1; // This will remain negative only if the level is not present.
				if( levelVal != NullPtr< OutlineLevelType >( ) )
					{
						outlineLevel = levelVal->val_;
						if( outlineLevel < 0 )
							{
								throw Exceptions::CoreOutOfRange( id_, args, argsi, "The outline level must be non-negative." );
							}
					}

				++argsi;
				typedef const Lang::String OutlineTextType;
				RefCountPtr< OutlineTextType > textVal = Helpers::down_cast_CoreArgument< OutlineTextType >( id_, args, argsi, callLoc, true );
				RefCountPtr< const char > outlineText = RefCountPtr< const char >( NullPtr< const char >( ) );
				if( textVal != NullPtr< OutlineTextType >( ) )
					{
						outlineText = textVal->val_;
					}
				else
					{
						if( outlineLevel >= 0 )
							{
								throw Exceptions::CoreOutOfRange( id_, args, outlineLevel_i, "Without an outline text, it is not allowed to make an outline item." );
							}
					}

				++argsi;
				typedef const Lang::Boolean OutlineOpenType;
				bool outlineOpen = Helpers::down_cast_CoreArgument< OutlineOpenType >( id_, args, argsi, callLoc )->val_;

				++argsi;
				typedef const Lang::Boolean OutlineBoldType;
				bool outlineBold = Helpers::down_cast_CoreArgument< OutlineBoldType >( id_, args, argsi, callLoc )->val_;

				++argsi;
				typedef const Lang::Boolean OutlineItalicType;
				bool outlineItalic = Helpers::down_cast_CoreArgument< OutlineItalicType >( id_, args, argsi, callLoc )->val_;

				++argsi;
				typedef const Lang::RGB OutlineColorType;
				Concrete::RGB outlineColor = Helpers::down_cast_CoreArgument< OutlineColorType >( id_, args, argsi, callLoc )->components( );

				++argsi;
				const size_t sidesMode_i = argsi;
				typedef const Lang::Symbol SidesModeType;
				RefCountPtr< SidesModeType > sidesVal = Helpers::down_cast_CoreArgument< SidesModeType >( id_, args, argsi, callLoc, true );

				++argsi;
				const size_t target_i = argsi;
				typedef const Lang::Drawable2D TargetType;
				RefCountPtr< TargetType > target = Helpers::down_cast_CoreArgument< TargetType >( id_, args, argsi, callLoc, true );

				Lang::DocumentDestination::Sides sides = Lang::DocumentDestination::PAGE; // Defaults to false, unless a target is given.
				if( target != NullPtr< TargetType >( ) )
					{
						sides = Lang::DocumentDestination::TOPLEFT;
						if( remote )
							{
								throw Exceptions::CoreOutOfRange( id_, args, target_i, "The target can not be specified for remote destinations." );
							}
					}
				static Lang::Symbol SIDES_TopLeft( "topleft" );
				static Lang::Symbol SIDES_Page( "page" );
				static Lang::Symbol SIDES_Top( "top" );
				static Lang::Symbol SIDES_Left( "left" );
				static Lang::Symbol SIDES_Rectangle( "rectangle" );
				if( sidesVal != NullPtr< SidesModeType >( ) )
					{
						if( *sidesVal == SIDES_TopLeft )
							{
								sides = Lang::DocumentDestination::TOPLEFT;
							}
						else if( *sidesVal == SIDES_Page )
							{
								if( target != NullPtr< TargetType >( ) )
									{
										throw Exceptions::CoreOutOfRange( id_, args, sidesMode_i, "The sides mode cannot be page when a target object is present." );
									}
								sides = Lang::DocumentDestination::PAGE;
							}
						else if( *sidesVal == SIDES_Top )
							{
								sides = Lang::DocumentDestination::TOP;
							}
						else if( *sidesVal == SIDES_Left )
							{
								sides = Lang::DocumentDestination::LEFT;
							}
						else if( *sidesVal == SIDES_Rectangle )
							{
								sides = Lang::DocumentDestination::RECTANGLE;
							}
						else
							{
								std::ostringstream oss;
								oss << "Valid sides modes are the symbols { "
										<< SIDES_TopLeft.name( ).getPtr( ) << ", "
										<< SIDES_Page.name( ).getPtr( ) << ", "
										<< SIDES_Top.name( ).getPtr( ) << ", "
										<< SIDES_Left.name( ).getPtr( ) << ", "
										<< SIDES_Rectangle.name( ).getPtr( )
										<< " }." ;
								throw Exceptions::CoreOutOfRange( id_, args, sidesMode_i, strrefdup( oss ) );
							}
					}

				++argsi;
				typedef const Lang::Boolean FitToType;
				RefCountPtr< FitToType > fittobboxVal = Helpers::down_cast_CoreArgument< FitToType >( id_, args, argsi, callLoc, true );
				bool fittobbox = false;
				if( fittobboxVal != NullPtr< FitToType >( ) )
					{
						if( remote || sides == Lang::DocumentDestination::TOPLEFT || sides == Lang::DocumentDestination::RECTANGLE )
							{
								throw Exceptions::CoreOutOfRange( id_, args, argsi, "The fit-to-bbox flag cannot be specified in this mode." );
							}
						fittobbox = fittobboxVal->val_;
					}

				++argsi;
				typedef const Lang::Float ZoomType;
				RefCountPtr< ZoomType > zoomVal = Helpers::down_cast_CoreArgument< ZoomType >( id_, args, argsi, callLoc, true );
				double zoom = 0; // This will remain zero only if the zoom argument is not specified.
				if( zoomVal != NullPtr< ZoomType >( ) )
					{
						if( remote || sides != Lang::DocumentDestination::TOPLEFT )
							{
								throw Exceptions::CoreOutOfRange( id_, args, argsi, "The zoom can only be specified when using the top-left sides." );
							}
						zoom = zoomVal->val_;
						if( zoom <= 0 )
							{
								throw Exceptions::CoreOutOfRange( id_, args, argsi, "The zoom value must be positive." );
							}
					}

				++argsi;
				typedef const Lang::Boolean DoTransformType;
				bool doTransform = Helpers::down_cast_CoreArgument< DoTransformType >( id_, args, argsi, callLoc )->val_;

				Kernel::ContRef cont = evalState->cont_;
				if( remote )
					{
						RefCountPtr< const Lang::DocumentDestination >
							taggedObj( new Lang::DocumentDestination( remote, name, outlineLevel,
																												outlineText, outlineOpen, outlineBold, outlineItalic, outlineColor ) );
						cont->takeValue( RefCountPtr< const Lang::Value >
														 ( new Lang::TaggedValue2D( Kernel::THE_NAVIGATION_SYMBOL, taggedObj ) ),
														 evalState );
					}
				else
					{
						RefCountPtr< const Lang::DocumentDestination >
							taggedObj( new Lang::DocumentDestination( name, outlineLevel,
																												outlineText, outlineOpen, outlineBold, outlineItalic, outlineColor,
																												sides, target, fittobbox, zoom ) );
						if( doTransform )
							{
								cont->takeValue( RefCountPtr< const Lang::Value >
																 ( new Lang::TaggedGeometric2D( Kernel::THE_NAVIGATION_SYMBOL, taggedObj ) ),
																 evalState );
							}
						else
							{
								cont->takeValue( RefCountPtr< const Lang::Value >
																 ( new Lang::TaggedValue2D( Kernel::THE_NAVIGATION_SYMBOL, taggedObj ) ),
																 evalState );
							}
					}
			}
		};

		class Core_importFont : public Lang::CoreFunction
		{
		public:
			struct CacheKey
			{
				bool outline_;
				RefCountPtr< const char > filename_;
				CacheKey( bool outline, const RefCountPtr< const char > & filename )
					: outline_( outline ), filename_( filename )
				{ }
				~CacheKey( )
				{ }
				bool operator < ( const CacheKey & other ) const
				{
					if( ! outline_ && other.outline_ )
						{
							return true;
						}
					if( outline_ && ! other.outline_ )
						{
							return false;
						}
					return strcmp( filename_.getPtr( ), other.filename_.getPtr( ) ) < 0;
				}
			};
		private:
			static std::map< CacheKey, RefCountPtr< const Lang::Value > > cache;
		public:
			Core_importFont( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name )
				: CoreFunction( ns, name, new Kernel::EvaluatedFormals( Ast::FileID::build_internal( name ), true ) )
			{
				formals_->appendEvaluatedCoreFormal( "family", Kernel::THE_SLOT_VARIABLE );
				formals_->appendEvaluatedCoreFormal( "style", Kernel::THE_VOID_VARIABLE );
				formals_->appendEvaluatedCoreFormal( "outline", Kernel::THE_FALSE_VARIABLE );
			}
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
#ifndef HAVE_FT2
				throw Exceptions::BuildRequirement( Interaction::BUILD_REQ_FREETYPE, id_, callLoc );
#else
				args.applyDefaults( callLoc );

				size_t argsi = 0;
				std::string full_filename;
#ifndef HAVE_FONTCONFIG
				/* Here, it would be possible to allow the user to point out the font file directly instead of using FontConfig,
				 * but currently there is no such option.
				 */
				throw Exceptions::BuildRequirement( Interaction::BUILD_REQ_FONTCONFIG, id_, callLoc );
#else
				FcPattern * pat = FcPatternCreate( );
				Helpers::AutoDestroyFcPattern destroy_pat( pat );

				FcValue fc_fontformat;
				fc_fontformat.type = FcTypeString;
				fc_fontformat.u.s = reinterpret_cast< const FcChar8 * >( "TrueType" );
				FcPatternAdd( pat, "fontformat", fc_fontformat, FcFalse );

				size_t argsi_family = argsi;
				typedef const Lang::String ArgType;
				RefCountPtr< ArgType > family = Helpers::down_cast_CoreArgument< ArgType >( id_, args, argsi_family, callLoc );
				FcValue fc_family;
				fc_family.type = FcTypeString;
				fc_family.u.s = reinterpret_cast< const FcChar8 * >( family->val_.getPtr( ) );
				FcPatternAdd( pat, "family", fc_family, FcFalse );

				++argsi;
				size_t argsi_style = argsi;
				RefCountPtr< ArgType > style = Helpers::down_cast_CoreArgument< ArgType >( id_, args, argsi_style, callLoc, true );
				FcValue fc_style;
				if( style != NullPtr< ArgType >( ) )
					{
						fc_style.type = FcTypeString;
						fc_style.u.s = reinterpret_cast< const FcChar8 * >( style->val_.getPtr( ) );
						FcPatternAdd( pat, "style", fc_family, FcFalse );
					}

				FcFontSet * sysFonts = FcConfigGetFonts( NULL, FcSetSystem );

				FcResult fc_res;

				FcDefaultSubstitute( pat );
				FcConfigSubstitute( NULL, pat, FcMatchPattern ); /* Not sure if FcMatchPattern is the correct value to pass here.  There are two more possible values, and no documentation.  See fontconfig/fontconfig.h. */
				FcPattern * match_res = FcFontSetMatch( NULL, & sysFonts, 1, pat, & fc_res );
				if( match_res == 0 )
					{
						throw Exceptions::ExternalError( "FontConfig' FcFontSetMatch returned with failure." );
					}
				Helpers::AutoDestroyFcPattern destroy_match_res( match_res );

				switch( fc_res )
					{
					case FcResultMatch:
						// OK, just continue;
						break;
					case FcResultNoMatch:
						throw Exceptions::OutOfRange( callLoc, "FontConfig returned no matching font." );
					case FcResultOutOfMemory:
						throw Exceptions::ExternalError( "FontConfig got out of memory." );
					case FcResultTypeMismatch:
						// {
						// 	std::ostringstream msg;
						// 	msg << "Unexpected FcResult from FcFontSetMatch: " << "FcResultTypeMismatch" << " (ignored)." ;
						// 	WARN_OR_THROW( Exceptions::InternalError( strrefdup( msg ), true ) );
						// }
						break;
					case FcResultNoId:
						// {
						// 	std::ostringstream msg;
						// 	msg << "Unexpected FcResult from FcFontSetMatch: " << "FcResultNoId" << " (ignored)." ;
						// 	WARN_OR_THROW( Exceptions::InternalError( strrefdup( msg ), true ) );
						// }
						break;
					default:
						// {
						// 	std::ostringstream msg;
						// 	msg << "Unexpected FcResult from FcFontSetMatch: " << fc_res << " (ignored)." ;
						// 	WARN_OR_THROW( Exceptions::InternalError( strrefdup( msg ), true ) );
						// }
						//						throw Exceptions::InternalError( "Unexpected FcResult from FcFontSetMatch." );
						break;
					}
				//				FcPatternPrint( match_res );

				FcChar8 * fc_tmp_string;
				switch( FcPatternGetString( match_res, "fontformat", 0, & fc_tmp_string ) )
					{
					case FcResultMatch:
						// OK, just continue;
						break;
					case FcResultNoMatch:
						throw Exceptions::ExternalError( "FontConfig says that the font does not have a 'fontformat'." );
					case FcResultTypeMismatch:
						throw Exceptions::ExternalError( "FontConfig says 'fontformat' has the wrong type." );
					case FcResultNoId:
						throw Exceptions::ExternalError( "FontConfig says 'fontformat' is missing something." );
					case FcResultOutOfMemory:
						throw Exceptions::ExternalError( "FontConfig got out of memory." );
					}
				if( strcmp( reinterpret_cast< const char * >( fc_tmp_string ), "TrueType" ) != 0 ) // This is OK, since "TrueType" is in the ASCII range.
					{
						throw Exceptions::OutOfRange( callLoc, "Font file format not supported by Shapes, only TrueType is supported at the moment." );
					}

				switch( FcPatternGetString( match_res, "file", 0, & fc_tmp_string ) )
					{
					case FcResultMatch:
						// OK, just continue;
						break;
					case FcResultNoMatch:
						throw Exceptions::ExternalError( "FontConfig says that the font does not have a 'file' (filename)." );
					case FcResultTypeMismatch:
						throw Exceptions::ExternalError( "FontConfig says 'file' has the wrong type." );
					case FcResultNoId:
						throw Exceptions::ExternalError( "FontConfig says 'file' is missing something." );
					case FcResultOutOfMemory:
						throw Exceptions::ExternalError( "FontConfig got out of memory." );
					}
				full_filename = reinterpret_cast< const char * >( fc_tmp_string ); /* Let us hope that this string is in the ASCII range! */
#endif

				++argsi;
				size_t argsi_outline = argsi;
				bool outline = Helpers::down_cast_CoreArgument< const Lang::Boolean >( id_, args, argsi_outline, callLoc )->val_;
				CacheKey cacheKey( outline, strrefdup( full_filename.c_str( ) ) );
				{
					typedef typeof cache MapType;
					MapType::const_iterator i = cache.find( cacheKey );
					if( i != cache.end( ) )
						{
							Kernel::ContRef cont = evalState->cont_;
							cont->takeValue( i->second,
															 evalState );
							return;
						}
				}


				FT_Face face;
				{
					FT_Error error = FT_New_Face( Kernel::theFreeType,
																				full_filename.c_str( ),
																				0,
																				& face );
					if( error == FT_Err_Unknown_File_Format )
						{
							throw Exceptions::ExternalError( "Font file format not supported by FreeType." );
						}
					else if( error != 0 )
						{
							std::ostringstream msg;
							msg << "FreeType failed to open " << full_filename << ", reason: " << Kernel::FreeTypeErrorMessage( error ) ;
							throw Exceptions::ExternalError( strrefdup( msg ) );
						}
				}
				const char * PostScriptName = FT_Get_Postscript_Name( face ); /* See comment on the next line! */
				std::ostringstream baseFontName; /* The PostScriptName pointer shall only be used as long as this object is in scope.  */
				if( PostScriptName == 0 )
					{
						for( const char * src = full_filename.c_str( ); *src != '\0'; ++src )
							{
								if( *src == '.' )
									{
										break;
									}
								if( *src != ' ' )
									{
										baseFontName << *src ;
									}
							}
						PostScriptName = baseFontName.str( ).c_str( );
					}

				if( ! FT_IS_SFNT( face ) )
					{
						throw Exceptions::OutOfRange( callLoc, "Font file format not supported by Shapes, only TrueType is supported at the moment." );
					}
				if( ! outline )
					{
#ifdef HAVE_FT2_10
						FT_UShort fsType = FT_Get_FSType_Flags( face );
						if( ( fsType & ( FT_FSTYPE_INSTALLABLE_EMBEDDING |
														 FT_FSTYPE_PREVIEW_AND_PRINT_EMBEDDING |
														 FT_FSTYPE_EDITABLE_EMBEDDING ) )
								== 0 )
							{
								std::ostringstream msg;
								msg << "The font " << PostScriptName << " does not permit embedding, and cannot be used with Shapes.  Flags: " << std::hex << static_cast< int >( fsType ) << "." ;
								throw Exceptions::MiscellaneousRequirement( strrefdup( msg ) );
							}
#else
						throw Exceptions::MiscellaneousRequirement( strrefdup( "Shapes does not permit embedding of fonts, since your FreeType library is too old to provide licensing information about your fonts." ) );
#endif
					}

				FontMetrics::FreeType2_Metric * newMetrics = new FontMetrics::FreeType2_Metric( face );
				RefCountPtr< const FontMetrics::FontMetric > metrics = RefCountPtr< const FontMetrics::FontMetric >( newMetrics );

				RefCountPtr< SimplePDF::PDF_Dictionary > dic;
				RefCountPtr< SimplePDF::PDF_Object > resource = SimplePDF::indirect( dic, & Kernel::theIndirectObjectCount );
				(*dic)[ "Type" ] = SimplePDF::newName( "Font" );
				(*dic)[ "Subtype" ] = SimplePDF::newName( "Type0" );

				RefCountPtr< SimplePDF::PDF_Dictionary > descendant;
				RefCountPtr< SimplePDF::PDF_Vector > descendants;
				descendants->vec.push_back( descendant );
				(*dic)[ "DescendantFonts" ] = descendants;

				RefCountPtr< SimplePDF::PDF_ToUnicode::CharSet > usedChars;

				(*descendant)[ "Type" ] = SimplePDF::newName( "Font" );
				(*descendant)[ "Subtype" ] = SimplePDF::newName( "CIDFontType2" );

				RefCountPtr< SimplePDF::PDF_Name > fontName = RefCountPtr< SimplePDF::PDF_Name >( new SimplePDF::PDF_Name( PostScriptName ) );
				(*dic)[ "BaseFont" ] = fontName;
				(*descendant)[ "BaseFont" ] = fontName;
				(*dic)[ "Encoding" ] = SimplePDF::newName( "Identity-H" );
				(*descendant)[ "BaseFont" ] = fontName;

				RefCountPtr< SimplePDF::PDF_Dictionary > CIDSystemInfo;
				(*CIDSystemInfo)[ "Registry" ] = SimplePDF::newString( "Adobe" );
				(*CIDSystemInfo)[ "Ordering" ] = SimplePDF::newString( "Identity" );
				(*CIDSystemInfo)[ "Supplement" ] = SimplePDF::newString( "0" );
				(*descendant)[ "CIDSystemInfo" ] = CIDSystemInfo;
				(*descendant)[ "CIDToGIDMap" ] = SimplePDF::newName( "Identity" );

				(*descendant)[ "DW" ] = SimplePDF::newFloat( 0.7 * 1000 ); /* Default width. */

				RefCountPtr< SimplePDF::PDF_Dictionary > fontDescriptor;
				(*fontDescriptor)[ "Type" ] = SimplePDF::newName( "FontDescriptor" );
				(*fontDescriptor)[ "FontName" ] = fontName;
				(*fontDescriptor)[ "Flags" ] = SimplePDF::newInt( (size_t)(1) << ( 6 - 1 ) ); // This is the "Nonsymbolic" flag, although we don't know if it really is a non-symbolic font...
				(*fontDescriptor)[ "FontBBox" ] =
					RefCountPtr< SimplePDF::PDF_Vector >( new SimplePDF::PDF_Vector( newMetrics->fontBBoxXMin_ * 1000, newMetrics->fontBBoxYMin_ * 1000,
																																					 newMetrics->fontBBoxXMax_ * 1000, newMetrics->fontBBoxYMax_ * 1000 ) );
				(*fontDescriptor)[ "ItalicAngle" ] = SimplePDF::newFloat( 0 );
				(*fontDescriptor)[ "Ascent" ] = SimplePDF::newFloat( 1 * 1000 );
				(*fontDescriptor)[ "Descent" ] = SimplePDF::newFloat( -0.5 * 1000 );

				RefCountPtr< SimplePDF::PDF_Stream_out > fontFile = RefCountPtr< SimplePDF::PDF_Stream_out >( new SimplePDF::PDF_Stream_out( ) );
				(*fontFile)[ "Filter" ] = SimplePDF::newName( "FlateDecode" );
				std::ifstream iFile( full_filename.c_str( ) );
				if( ! iFile.is_open( ) )
					{
						throw Exceptions::FileReadOpenError( callLoc, strrefdup( full_filename ), 0, 0 );
					}
				{
					iFile.seekg( 0, std::ios::end );
					std::streamoff tmp = iFile.tellg( );
					std::streamsize size = tmp - static_cast< std::streamoff >( 0 );
					(*fontFile)[ "Length1" ] = SimplePDF::newInt( size );
					char * buf = new char[ size ];
					iFile.seekg( 0, std::ios::beg );
					iFile.read( buf, size );
					fontFile->data.write( buf, size );
					delete buf;
				}
				(*fontDescriptor)[ "FontFile2" ] = SimplePDF::indirect( fontFile, & Kernel::theIndirectObjectCount );

				(*descendant)[ "FontDescriptor" ] = fontDescriptor;

				RefCountPtr< const Lang::Font > res = RefCountPtr< const Lang::Font >( new Lang::FreeTypeFont( face,
																																																			 newMetrics->fontName_,
																																																			 outline,
																																																			 resource,
																																																			 metrics,
																																																			 usedChars ) );
				RefCountPtr< SimplePDF::PDF_ToUnicode > toUnicode( new SimplePDF::PDF_ToUnicode( usedChars, res ) );
				(*dic)[ "ToUnicode" ] = SimplePDF::indirect( toUnicode, & Kernel::theIndirectObjectCount );
				RefCountPtr< SimplePDF::PDF_Widths > widths( new SimplePDF::PDF_Widths( usedChars, res ) );
				(*descendant)[ "W" ] = SimplePDF::indirect( widths, & Kernel::theIndirectObjectCount );

				{
					typedef typeof cache MapType;
					cache.insert( MapType::value_type( cacheKey, res ) );
				}
				Kernel::ContRef cont = evalState->cont_;
				cont->takeValue( res,
												 evalState );
#endif
			}
		};

		std::map< Core_importFont::CacheKey, RefCountPtr< const Lang::Value > > Core_importFont::cache;

		class Core_Unicode : public Lang::CoreFunction
		{
		public:
			Core_Unicode( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name )
				: CoreFunction( ns, name )
			{ }
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				const size_t ARITY = 1;
				CHECK_ARITY( args, ARITY, id_ );

				size_t argsi = 0;
				typedef const Lang::Integer ArgType;
				ArgType::ValueType arg = Helpers::down_cast_CoreArgument< ArgType >( id_, args, argsi, callLoc )->val_;
				if( arg < 0 )
					{
						throw Exceptions::CoreOutOfRange( id_, args, argsi, "Unicode code points cannot be negative." );
					}

				Kernel::ContRef cont = evalState->cont_;
				cont->takeValue( Kernel::ValueRef( new Lang::Character( arg ) ),
												 evalState );
			}
		};

		class Core_AdobeGlyphList : public Lang::CoreFunction
		{
		public:
			Core_AdobeGlyphList( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name )
				: CoreFunction( ns, name )
			{ }
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				const size_t ARITY = 1;
				CHECK_ARITY( args, ARITY, id_ );

				size_t argsi = 0;
				typedef const Lang::Symbol ArgType;
				RefCountPtr< ArgType > arg = Helpers::down_cast_CoreArgument< ArgType >( id_, args, argsi, callLoc );
				if( arg->isUnique( ) )
					{
						throw Exceptions::CoreOutOfRange( id_, args, argsi, "Unique symbols do not correspond to any glyphs." );
					}
				FontMetrics::GlyphList::UnicodeType res;
				const FontMetrics::GlyphList & glyphList = Helpers::requireGlyphList( );
				if( ! glyphList.name_to_UCS4( arg->name( ).getPtr( ), & res ) )
					{
						std::ostringstream msg;
						msg << "Unrecognized glyph name: " ;
						arg->show( msg );
						throw Exceptions::CoreOutOfRange( id_, args, argsi, strrefdup( msg ) );
					}
				Kernel::ContRef cont = evalState->cont_;
				cont->takeValue( Kernel::ValueRef( new Lang::Character( res ) ),
												 evalState );
			}
		};

	}
}


Lang::Core_range::Core_range( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name )
	: CoreFunction( ns, name, new Kernel::EvaluatedFormals( Ast::FileID::build_internal( name ), true ) )
{
	formals_->appendEvaluatedCoreFormal( "begin", Kernel::THE_VOID_VARIABLE );
	formals_->appendEvaluatedCoreFormal( "end", Kernel::THE_VOID_VARIABLE );
	formals_->appendEvaluatedCoreFormal( "step", Kernel::THE_VOID_VARIABLE );
	formals_->appendEvaluatedCoreFormal( "count", Kernel::THE_VOID_VARIABLE );
}

void
Lang::Core_range::call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
{
	RefCountPtr< const Interaction::CoreLocation > coreLoc( new Interaction::BoundLocation( id_ ) );
	makeRange( coreLoc, evalState, args, callLoc );
}

void
Lang::Core_range::makeRange( const RefCountPtr< const Interaction::CoreLocation > & coreLoc, Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc, bool isSpan, RefCountPtr< const Lang::Value > lastPtr )
{
	const size_t argsi_begin = 0;
	const size_t argsi_end = 1;
	const size_t argsi_step = 2;
	const size_t argsi_count = 3;

	typedef const Lang::Integer CountType;

	args.applyDefaults( callLoc );

	try
		{
			typedef const Lang::Integer ArgType;

			RefCountPtr< ArgType > beginPtr = Helpers::try_cast_CoreArgument< ArgType >( args.getValue( argsi_begin ), true );
			RefCountPtr< ArgType > endPtr = Helpers::down_cast_CoreArgument< ArgType >( coreLoc, args, argsi_end, callLoc, true );
			if( beginPtr == NullPtr< ArgType >( ) && endPtr == NullPtr< ArgType >( ) )
				{
					throw Exceptions::CoreRequirement( "At least one of the arguments <begin> and <end> must be provided.", coreLoc, callLoc );
				}
			RefCountPtr< ArgType > stepPtr = Helpers::down_cast_CoreArgument< ArgType >( coreLoc, args, argsi_step, callLoc, true );
			RefCountPtr< CountType > countPtr = Helpers::down_cast_CoreArgument< CountType >( coreLoc, args, argsi_count, callLoc, true );

			if( beginPtr != NullPtr< ArgType >( ) && endPtr != NullPtr< ArgType >( ) &&
					stepPtr != NullPtr< ArgType >( ) && countPtr != NullPtr< CountType >( ) )
				{
					throw Exceptions::CoreRequirement( "At least one of the arguments must be omitted.", coreLoc, callLoc );
				}

			ArgType::ValueType begin = 0;
			ArgType::ValueType step = 1;
			ArgType::ValueType last = isSpan ? lastPtr.down_cast< ArgType >( )->val_ : 0;
			size_t count = 0;

			if( stepPtr != NullPtr< ArgType >( ) )
				{
					step = stepPtr->val_;
				}
			if( countPtr != NullPtr< CountType >( ) )
				{
					if( countPtr->val_ < 0 )
						{
							throw Exceptions::CoreOutOfRange( coreLoc, args, argsi_count, "The <count> must not be negative." );
						}
					count = countPtr->val_;
				}

			if( beginPtr == NullPtr< ArgType >( ) )
				{
					if( countPtr == NullPtr< CountType >( ) )
						{
							if( isSpan )
								{
									if( endPtr->val_ < 0 )
										{
											throw Exceptions::CoreRequirement( "The <count> must be provided when constructing a span of integers and <begin> is omitted while <end> is negative.", coreLoc, callLoc );
										}
									count = 1 + endPtr->val_ / step;
								}
							else
								{
									throw Exceptions::CoreRequirement( "The <count> must be provided when constructing a range of integers and <begin> is omitted.", coreLoc, callLoc );
								}
						}
					begin = endPtr->val_ - ( count - 1 ) * step;
				}
			else if( endPtr == NullPtr< ArgType >( ) )
				{
					begin = beginPtr->val_;
					if( countPtr == NullPtr< CountType >( ) )
						{
							if( isSpan )
								{
									if( last < begin )
										{
											throw Exceptions::CoreRequirement( "The <count> must be provided when constructing a range of integers and <end> is omitted while <begin> exceeds %last.", coreLoc, callLoc );
										}
									count = ( last - begin ) / step + 1;
								}
							else
								{
									throw Exceptions::CoreRequirement( "The <count> must be provided when constructing a range of integers and <end> is omitted.", coreLoc, callLoc );
								}
						}
				}
			else
				{
					if( countPtr != NullPtr< CountType >( ) )
						{
							throw Exceptions::CoreRequirement( "It is an error to provide the <count> argument when constructing a range of integers from <begin> to <end>.", coreLoc, args.getLoc( argsi_count ) );
						}

					begin = beginPtr->val_;
					if( ( endPtr->val_ >= begin && step <= 0 )
							||
							( endPtr->val_ <= begin && step >= 0 ) )
						{
							throw Exceptions::CoreOutOfRange( coreLoc, args, argsi_step, "When <count> is omitted, the sign of <step> must agree with the order of <begin> and <end>." );
						}
					count = 1 + ( endPtr->val_ - begin ) / step;
				}

			Kernel::ContRef cont = evalState->cont_;
			if( count == 0 )
				{
					cont->takeValue( Lang::THE_CONS_NULL,
													 evalState );
				}
			else
				{
					cont->takeValue( RefCountPtr< const Lang::SingleList >( new Lang::SingleListRange< ArgType >( begin, step, count ) ),
													 evalState );
				}
			return;
		}
	catch( const NonLocalExit::NotThisType & ball )
		{
			/* Never mind, see below. */
		}

	try
		{
			typedef const Lang::Float ArgType;

			RefCountPtr< ArgType > beginPtr = Helpers::try_cast_CoreArgument< ArgType >( args.getValue( argsi_begin ), true );
			RefCountPtr< ArgType > endPtr = Helpers::down_cast_CoreArgument< ArgType >( coreLoc, args, argsi_end, callLoc, true );
			if( beginPtr == NullPtr< ArgType >( ) && endPtr == NullPtr< ArgType >( ) )
				{
					throw Exceptions::CoreRequirement( "At least one of the arguments <begin> and <end> must be provided.", coreLoc, callLoc );
				}
			RefCountPtr< ArgType > stepPtr = Helpers::down_cast_CoreArgument< ArgType >( coreLoc, args, argsi_step, callLoc, true );
			RefCountPtr< CountType > countPtr = Helpers::down_cast_CoreArgument< CountType >( coreLoc, args, argsi_count, callLoc, true );

			if( beginPtr != NullPtr< ArgType >( ) && endPtr != NullPtr< ArgType >( ) &&
					stepPtr != NullPtr< ArgType >( ) && countPtr != NullPtr< CountType >( ) )
				{
					throw Exceptions::CoreRequirement( "At least one of the arguments must be omitted.", coreLoc, callLoc );
				}

			ArgType::ValueType begin = 0;
			ArgType::ValueType step = 1;
			ArgType::ValueType last = isSpan ? lastPtr.down_cast< ArgType >( )->val_ : 0;
			size_t count = 0;

			if( stepPtr != NullPtr< ArgType >( ) )
				{
					step = stepPtr->val_;
				}
			if( countPtr != NullPtr< CountType >( ) )
				{
					if( countPtr->val_ < 0 )
						{
							throw Exceptions::CoreOutOfRange( coreLoc, args, argsi_count, "The <count> must not be negative." );
						}
					count = countPtr->val_;
				}

			if( beginPtr == NullPtr< ArgType >( ) )
				{
					if( countPtr == NullPtr< CountType >( ) )
						{
							if( isSpan )
								{
									if( endPtr->val_ < 0 )
										{
											throw Exceptions::CoreRequirement( "The <count> must be provided when constructing a span of floats and <begin> is omitted while <end> is negative.", coreLoc, callLoc );
										}
									count = 1 + static_cast< size_t >( endPtr->val_ / step );
								}
							else
								{
									throw Exceptions::CoreRequirement( "The <count> must be provided when constructing a range of floats and <begin> is omitted.", coreLoc, callLoc );
								}
						}
					begin = endPtr->val_ - ( count - 1 ) * step;
				}
			else if( endPtr == NullPtr< ArgType >( ) )
				{
					begin = beginPtr->val_;
					if( countPtr == NullPtr< CountType >( ) )
						{
							if( isSpan )
								{
									if( last < begin )
										{
											throw Exceptions::CoreRequirement( "The <count> must be provided when constructing a range of floats and <end> is omitted while <begin> exceeds %last.", coreLoc, callLoc );
										}
									count = 1 + static_cast< size_t >( ( last - begin ) / step );
								}
							else
								{
									throw Exceptions::CoreRequirement( "The <count> must be provided when constructing a range of floats and <end> is omitted.", coreLoc, callLoc );
								}
						}
				}
			else
				{
					begin = beginPtr->val_;
					// When we reach here, we know that at most one of <step> and <count> is provided.
					if( countPtr != NullPtr< CountType >( ) )
						{
							if( count < 2 )
								{
									throw Exceptions::CoreOutOfRange( coreLoc, args, argsi_count, "When <step> is omitted, the <count> be at least 2." );
								}
							step = ( endPtr->val_ - begin ) / ( count - 1 );
						}
					else
						{
							/* If <step> is not provided, it defaults to 1, see above. */
							if( step == 0 )
								{
									throw Exceptions::CoreOutOfRange( coreLoc, args, argsi_step, "When <count> is omitted, <step> must not be zero." );
								}
							ArgType::ValueType tmpCount = 1 + ( endPtr->val_ - begin ) / step;
							if( tmpCount < 1 )
								{
									count = 0;
								}
							else
								{
									if( tmpCount > std::numeric_limits< size_t >::max( ) )
										{
											throw Exceptions::CoreOutOfRange( coreLoc, args, argsi_step, "When <count> is omitted, too small <step> values cause overflow in the number of elements." );
										}
									count = static_cast< size_t >( tmpCount );
								}
						}
				}

			Kernel::ContRef cont = evalState->cont_;
			if( count == 0 )
				{
					cont->takeValue( Lang::THE_CONS_NULL,
													 evalState );
				}
			else
				{
					cont->takeValue( RefCountPtr< const Lang::SingleList >( new Lang::SingleListRange< ArgType >( begin, step, count ) ),
													 evalState );
				}
			return;
		}
	catch( const NonLocalExit::NotThisType & ball )
		{
			/* Never mind, see below. */
		}

	try
		{
			typedef const Lang::Length ArgType;

			RefCountPtr< ArgType > beginPtr = Helpers::try_cast_CoreArgument< ArgType >( args.getValue( argsi_begin ), true );
			RefCountPtr< ArgType > endPtr = Helpers::down_cast_CoreArgument< ArgType >( coreLoc, args, argsi_end, callLoc, true );
			if( beginPtr == NullPtr< ArgType >( ) && endPtr == NullPtr< ArgType >( ) )
				{
					throw Exceptions::CoreRequirement( "At least one of the arguments <begin> and <end> must be provided.", coreLoc, callLoc );
				}
			RefCountPtr< ArgType > stepPtr = Helpers::down_cast_CoreArgument< ArgType >( coreLoc, args, argsi_step, callLoc, true );
			RefCountPtr< CountType > countPtr = Helpers::down_cast_CoreArgument< CountType >( coreLoc, args, argsi_count, callLoc, true );

			if( beginPtr != NullPtr< ArgType >( ) && endPtr != NullPtr< ArgType >( ) &&
					stepPtr != NullPtr< ArgType >( ) && countPtr != NullPtr< CountType >( ) )
				{
					throw Exceptions::CoreRequirement( "At least one of the arguments must be omitted.", coreLoc, callLoc );
				}

			ArgType::ValueType begin = ArgType::ValueType( 0 );
			ArgType::ValueType step = ArgType::ValueType( 1 ); /* This is a "default" value that one is not allowed to use! */
			ArgType::ValueType last = isSpan ? lastPtr.down_cast< ArgType >( )->get( ) : Concrete::ZERO_LENGTH;
			size_t count = 0;

			if( stepPtr != NullPtr< ArgType >( ) )
				{
					step = stepPtr->get( );
				}
			if( countPtr != NullPtr< CountType >( ) )
				{
					if( countPtr->val_ < 0 )
						{
							throw Exceptions::CoreOutOfRange( coreLoc, args, argsi_count, "The <count> must not be negative." );
						}
					count = countPtr->val_;
				}

			if( beginPtr == NullPtr< ArgType >( ) )
				{
					if( stepPtr == NullPtr< ArgType >( ) )
						{
							throw Exceptions::CoreRequirement( "The <step> must be provided when constructing a range of lengths when <begin> is omitted.", coreLoc, callLoc );
						}
					if( countPtr == NullPtr< CountType >( ) )
						{
							if( isSpan )
								{
									if( endPtr->get( ) < Concrete::ZERO_LENGTH )
										{
											throw Exceptions::CoreRequirement( "The <count> must be provided when constructing a span of lengths and <begin> is omitted while <end> is negative.", coreLoc, callLoc );
										}
									count = 1 + static_cast< size_t >( endPtr->get( ) / step );
								}
							else
								{
									throw Exceptions::CoreRequirement( "The <count> must be provided when constructing a range of lengths and <begin> is omitted.", coreLoc, callLoc );
								}
						}
					begin = endPtr->get( ) - ( count - 1 ) * step;
				}
			else if( endPtr == NullPtr< ArgType >( ) )
				{
					begin = beginPtr->get( );
					if( stepPtr == NullPtr< ArgType >( ) )
						{
							throw Exceptions::CoreRequirement( "The <step> must be provided when constructing a range of lengths when <end> is omitted.", coreLoc, callLoc );
						}
					if( countPtr == NullPtr< CountType >( ) )
						{
							if( isSpan )
								{
									if( last < begin )
										{
											throw Exceptions::CoreRequirement( "The <count> must be provided when constructing a range of lengths and <end> is omitted while <begin> exceeds %last.", coreLoc, callLoc );
										}
									count = static_cast< size_t >( ( last - begin ) / step ) + 1;
								}
							else
								{
									throw Exceptions::CoreRequirement( "The <count> must be provided when constructing a range of lengths and <end> is omitted.", coreLoc, callLoc );
								}
						}
				}
			else
				{
					begin = beginPtr->get( );
					if( stepPtr == NullPtr< ArgType >( ) && countPtr == NullPtr< CountType >( ) )
						{
							throw Exceptions::CoreRequirement( "Either of the <step> and <count> arguments must be provided when constructing a range of lengths from <begin> to <end>.", coreLoc, callLoc );
						}
					// When we reach here, we know that exactly one of <step> and <count> is provided.
					if( stepPtr != NullPtr< ArgType >( ) )
						{
							if( step == 0 )
								{
									throw Exceptions::CoreOutOfRange( coreLoc, args, argsi_step, "When <count> is omitted, <step> must not be zero." );
								}
							double tmpCount = 1 + ( endPtr->get( ) - begin ) / step;
							if( tmpCount < 1 )
								{
									count = 0;
								}
							else
								{
									if( tmpCount > std::numeric_limits< size_t >::max( ) )
										{
											throw Exceptions::CoreOutOfRange( coreLoc, args, argsi_step, "When <count> is omitted, too small <step> values cause overflow in the number of elements." );
										}
									count = static_cast< size_t >( tmpCount );
								}
						}
					else
						{
							if( count < 2 )
								{
									throw Exceptions::CoreOutOfRange( coreLoc, args, argsi_count, "When <step> is omitted, the <count> be at least 2." );
								}
							step = ( endPtr->get( ) - begin ) / ( count - 1 );
						}
				}

			Kernel::ContRef cont = evalState->cont_;
			if( count == 0 )
				{
					cont->takeValue( Lang::THE_CONS_NULL,
													 evalState );
				}
			else
				{
					cont->takeValue( RefCountPtr< const Lang::SingleList >( new Lang::SingleListRange< ArgType >( begin, step, count ) ),
													 evalState );
				}
			return;
		}
	catch( const NonLocalExit::NotThisType & ball )
		{
			/* Never mind, see below. */
		}

	throw Exceptions::CoreTypeMismatch( callLoc, coreLoc, args, 0, Helpers::typeSetString( Lang::Integer::staticTypeName( ), Lang::Float::staticTypeName( ), Lang::Length::staticTypeName( ) ) );
}


Kernel::Core_graph_cont_partitions::Core_graph_cont_partitions( RefCountPtr< const Lang::Value > nodes, const Ast::SourceLocation & nodesLoc, RefCountPtr< const Lang::Value > edges, const Ast::SourceLocation & edgesLoc, RefCountPtr< const Kernel::GraphDomain > domain, const Kernel::ContRef & cont, const Ast::SourceLocation & partitionsLoc )
	: Kernel::Continuation( partitionsLoc ), nodes_( nodes ), nodesLoc_( nodesLoc ), edges_( edges ), edgesLoc_( edgesLoc ), domain_( domain ), cont_( cont )
{ }

Kernel::Core_graph_cont_partitions::~Core_graph_cont_partitions( )
{ }

void
Kernel::Core_graph_cont_partitions::takeValue( const RefCountPtr< const Lang::Value > & partitionsUntyped, Kernel::EvalState * evalState, bool dummy ) const
{
	typedef const Lang::SingleList ArgType;
	RefCountPtr< ArgType > partitions = Helpers::down_cast< ArgType >( partitionsUntyped, "< Internal error situation in Core_graph_cont_partitions >" );

	evalState->cont_ = Kernel::ContRef( new Kernel::ForcingListContinuation
																			( Kernel::ContRef( new Kernel::Core_graph_cont_nodes( edges_, edgesLoc_, domain_, partitions, cont_, traceLoc_, nodesLoc_ ) ),
																				nodesLoc_,
																				true  ) );
	Kernel::ContRef cont = evalState->cont_;
	cont->takeValue( nodes_, evalState );
}

Kernel::ContRef
Kernel::Core_graph_cont_partitions::up( ) const
{
	return cont_;
}

RefCountPtr< const char >
Kernel::Core_graph_cont_partitions::description( ) const
{
	return strrefdup( "force list of partitions data" );
}

void
Kernel::Core_graph_cont_partitions::gcMark( Kernel::GCMarkedSet & marked )
{
	const_cast< Lang::Value * >( nodes_.getPtr( ) )->gcMark( marked );
	const_cast< Lang::Value * >( edges_.getPtr( ) )->gcMark( marked );
	cont_->gcMark( marked );
}


Kernel::Core_graph_cont_nodes::Core_graph_cont_nodes( RefCountPtr< const Lang::Value > edges, const Ast::SourceLocation & edgesLoc, RefCountPtr< const Kernel::GraphDomain > domain, const RefCountPtr< const Lang::SingleList > & partitions, const Kernel::ContRef & cont, const Ast::SourceLocation & partitionsLoc, const Ast::SourceLocation & nodesLoc )
	: Kernel::Continuation( nodesLoc ), edges_( edges ), edgesLoc_( edgesLoc ), domain_( domain ), partitions_( partitions ), partitionsLoc_( partitionsLoc ), cont_( cont )
{ }

Kernel::Core_graph_cont_nodes::~Core_graph_cont_nodes( )
{ }

void
Kernel::Core_graph_cont_nodes::takeValue( const RefCountPtr< const Lang::Value > & nodesUntyped, Kernel::EvalState * evalState, bool dummy ) const
{
	typedef const Lang::SingleList ArgType;
	RefCountPtr< ArgType > nodes = Helpers::down_cast< ArgType >( nodesUntyped, "< Internal error situation in Core_graph_cont_nodes >" );

	evalState->cont_ = Kernel::ContRef( new Kernel::ForcingListContinuation
																			( Kernel::ContRef( new Kernel::Core_graph_cont_edges( nodes, domain_, partitions_, cont_, traceLoc_, partitionsLoc_, edgesLoc_ ) ),
																				edgesLoc_,
																				true ) );
	Kernel::ContRef cont = evalState->cont_;
	cont->takeValue( edges_, evalState );
}

Kernel::ContRef
Kernel::Core_graph_cont_nodes::up( ) const
{
	return cont_;
}

RefCountPtr< const char >
Kernel::Core_graph_cont_nodes::description( ) const
{
	return strrefdup( "force list of node data" );
}

void
Kernel::Core_graph_cont_nodes::gcMark( Kernel::GCMarkedSet & marked )
{
	const_cast< Lang::Value * >( edges_.getPtr( ) )->gcMark( marked );
	cont_->gcMark( marked );
}


Kernel::Core_graph_cont_edges::Core_graph_cont_edges( RefCountPtr< const Lang::SingleList > nodes, RefCountPtr< const Kernel::GraphDomain > domain, const RefCountPtr< const Lang::SingleList > & partitions, const Kernel::ContRef & cont, const Ast::SourceLocation & nodesLoc, const Ast::SourceLocation & partitionsLoc, const Ast::SourceLocation & edgesLoc )
	: Kernel::Continuation( edgesLoc ), nodes_( nodes ), domain_( domain ), partitions_( partitions ), nodesLoc_( nodesLoc ), partitionsLoc_( partitionsLoc ), cont_( cont )
{ }

Kernel::Core_graph_cont_edges::~Core_graph_cont_edges( )
{ }

void
Kernel::Core_graph_cont_edges::takeValue( const RefCountPtr< const Lang::Value > & edgesUntyped, Kernel::EvalState * evalState, bool dummy ) const
{
	typedef const Lang::SingleList ArgType;
	RefCountPtr< ArgType > edges = Helpers::down_cast< ArgType >( edgesUntyped, "< Internal error situation in Core_graph_cont_edges >" );

	RefCountPtr< const Lang::Graph > graph = constructGraph( edges, evalState->env_, evalState->dyn_ );

	evalState->cont_ = cont_;
	cont_->takeValue( graph,
										evalState );
}

Kernel::ContRef
Kernel::Core_graph_cont_edges::up( ) const
{
	return cont_;
}

RefCountPtr< const char >
Kernel::Core_graph_cont_edges::description( ) const
{
	return strrefdup( "force list of edge data" );
}

void
Kernel::Core_graph_cont_edges::gcMark( Kernel::GCMarkedSet & marked )
{
	const_cast< Lang::SingleList * >( nodes_.getPtr( ) )->gcMark( marked );
	cont_->gcMark( marked );
}

namespace Shapes
{
	namespace Kernel
	{

		NodeDataReceiverFormals theNodeDataReceiverFormals;
		EdgeDataReceiverFormals theEdgeDataReceiverFormals;

	}
}

RefCountPtr< const Lang::Graph >
Kernel::Core_graph_cont_edges::constructGraph( const RefCountPtr< const Lang::SingleList > & edges, Kernel::PassedEnv env, Kernel::PassedDyn dyn ) const
{
	std::list< Kernel::NodeData > nodeData;
	std::list< Kernel::EdgeData > edgeData;

	{
		RefCountPtr< const Lang::SingleListPair > p = nodes_.down_cast< const Lang::SingleListPair >( );
		while( p != NullPtr< const Lang::SingleListPair >( ) ){
			RefCountPtr< const Lang::Value > key = RefCountPtr< const Lang::Value >( NullPtr< const Lang::Value >( ) );
			RefCountPtr< const Lang::Value > value = Lang::THE_VOID;
			RefCountPtr< const Lang::Value > partition = Lang::THE_VOID;

			try
				{
					key = p->car_->tryVal< const Lang::Symbol >( );
					goto keyDone;
				}
			catch( const NonLocalExit::NotThisType & ball )
				{
					/* Never mind, see below. */
				}

			try
				{
					key = p->car_->tryVal< const Lang::Integer >( );
					goto keyDone;
				}
			catch( const NonLocalExit::NotThisType & ball )
				{
					/* Never mind, see below. */
				}

			try
				{
					RefCountPtr< const Lang::Structure > node = p->car_->tryVal< const Lang::Structure >( );
					/* Argument 0: key
					 * Argument 1: value
					 * Argument 2: partition
					 */
					Kernel::Arguments args( & theNodeDataReceiverFormals );
					node->argList_->bind( & args, node->values_, env, dyn );
					args.applyDefaults( nodesLoc_ );
					key = args.getValue( 0 );
					value = args.getValue( 1 );
					partition = args.getValue( 2 );
					goto keyDone;
				}
			catch( const NonLocalExit::NotThisType & ball )
				{
					/* Never mind, see below. */
				}

			throw Exceptions::TypeMismatch( traceLoc_, "Node element in graph construction", p->car_->getUntyped( )->getTypeName( ), Exceptions::GraphKeyTypeMismatch::expectedTypeOrStructure );

		keyDone:
			nodeData.push_back( Kernel::NodeData( key, value, partition ) );
			p = p->cdr_.down_cast< const Lang::SingleListPair >( );
		}
	}

	{
		RefCountPtr< const Lang::SingleListPair > p = edges.down_cast< const Lang::SingleListPair >( );
		while( p != NullPtr< const Lang::SingleListPair >( ) ){
			RefCountPtr< const Lang::Structure > edge = p->car_->getVal< const Lang::Structure >( "EdgeData for graph construction." );
			/* Argument 0: source
			 * Argument 1: target
			 * Argument 2: value
			 * Argument 3: directed
			 */
			Kernel::Arguments args( & theEdgeDataReceiverFormals );
			edge->argList_->bind( & args, edge->values_, env, dyn );
			args.applyDefaults( traceLoc_ );
			bool directed;
			{
				RefCountPtr< const Lang::Boolean > directedBoolean = args.getValue( 4 ).down_cast< const Lang::Boolean >( );
				if( directedBoolean != NullPtr< const Lang::Boolean >( ) ){
					directed = directedBoolean->val_;
				}else{
					RefCountPtr< const Lang::Void > directedVoid = args.getValue( 4 ).down_cast< const Lang::Void >( );
					if( directedVoid != NullPtr< const Lang::Void >( ) ){
						if( domain_->directed( ) && not domain_->undirected( ) )
							directed = true;
						else if( not domain_->directed( ) && domain_->undirected( ) )
							directed = false;
						else
							throw Exceptions::MiscellaneousRequirement( traceLoc_, "The EdgeData field 'directed' must be specified since the graph domain does not determine a default interpretation." );
					}else{
						throw Exceptions::TypeMismatch( traceLoc_, "EdgeData field 'directed'.", args.getValue( 3 )->getTypeName( ), Helpers::typeSetString( Lang::Boolean::staticTypeName( ), Lang::Void::staticTypeName( ) ) );
					}
				}
			}

			RefCountPtr< const Lang::Value > sourceKey = args.getValue( 0 );
			RefCountPtr< const Lang::Value > targetKey = args.getValue( 1 );
			RefCountPtr< const Lang::Value > value = args.getValue( 2 );
			RefCountPtr< const Lang::Value > label = args.getValue( 3 );
			edgeData.push_back( Kernel::EdgeData( directed, sourceKey, targetKey, value, label ) );
			p = p->cdr_.down_cast< const Lang::SingleListPair >( );
		}
	}

	return Helpers::graphFromNodeEdgeData( *domain_, partitions_, nodeData, edgeData, partitionsLoc_, traceLoc_ );
}


Kernel::Core_walk_cont_edges::Core_walk_cont_edges( const RefCountPtr< const Lang::Graph > & graph, const RefCountPtr< const Lang::Node > & first, const RefCountPtr< const Lang::Node > & last, const Ast::SourceLocation & edgesLoc, const Kernel::ContRef & cont, const Ast::SourceLocation & callLoc )
	: Kernel::Continuation( edgesLoc ), graph_( graph ), first_( first ), last_( last ), callLoc_( callLoc ), cont_( cont )
{ }

Kernel::Core_walk_cont_edges::~Core_walk_cont_edges( )
{ }

void
Kernel::Core_walk_cont_edges::takeValue( const RefCountPtr< const Lang::Value > & edgesUntyped, Kernel::EvalState * evalState, bool dummy ) const
{
	typedef const Lang::SingleList ArgType;
	RefCountPtr< ArgType > edges = Helpers::down_cast< ArgType >( edgesUntyped, "< Internal error situation in Core_walk_cont_edges >" );

	RefCountPtr< const Lang::Walk > walk( new Lang::Walk( graph_, first_, last_, edges, traceLoc_, callLoc_ ) );

	evalState->cont_ = cont_;
	cont_->takeValue( walk,
										evalState );
}

Kernel::ContRef
Kernel::Core_walk_cont_edges::up( ) const
{
	return cont_;
}

RefCountPtr< const char >
Kernel::Core_walk_cont_edges::description( ) const
{
	return strrefdup( "force list of walk edges" );
}

void
Kernel::Core_walk_cont_edges::gcMark( Kernel::GCMarkedSet & marked )
{
	const_cast< Lang::Graph * >( graph_.getPtr( ) )->gcMark( marked );
	const_cast< Lang::Node * >( first_.getPtr( ) )->gcMark( marked );
	const_cast< Lang::Node * >( last_.getPtr( ) )->gcMark( marked );
	cont_->gcMark( marked );
}


/*
 * To avoid problems with initialization order (Kernel::THE_VOID_VARIABLE has to be initialized first), we first
 * initialize with an ugly null value, and then set the correct value when the real function is registered below.
 */
RefCountPtr< const Lang::CoreFunction > Lang::THE_FUNCTION_RANGE = RefCountPtr< const Lang::CoreFunction >( NullPtr< const Lang::CoreFunction >( ) );

void
Kernel::registerCore_construct( Kernel::Environment * env )
{
	Lang::THE_FUNCTION_RANGE = RefCountPtr< const Lang::CoreFunction >( new Lang::Core_range( Lang::THE_NAMESPACE_Shapes_Data, "range" ) );

	env->initDefineCoreFunction( new Lang::Core_cons( Lang::THE_NAMESPACE_Shapes_Data, "cons" ) );
	env->initDefineCoreFunction( new Lang::Core_list( Lang::THE_NAMESPACE_Shapes_Data, "list" ) );
	env->initDefineCoreFunction( new Lang::Core_unlist( Lang::THE_NAMESPACE_Shapes_Data, "unlist" ) );
	env->initDefineCoreFunction( new Lang::Core_isnil( Lang::THE_NAMESPACE_Shapes_Data, "nil?" ) );
	env->initDefineCoreFunction( Lang::THE_FUNCTION_RANGE );
	env->initDefineCoreFunction( new Lang::Core_span( Lang::THE_NAMESPACE_Shapes_Data, "span" ) );
	env->initDefineCoreFunction( new Lang::Core_shift( Lang::THE_NAMESPACE_Shapes_Geometry, "shift" ) );
	env->initDefineCoreFunction( new Lang::Core_shift3d( Lang::THE_NAMESPACE_Shapes_Geometry3D, "shift" ) );
	env->initDefineCoreFunction( new Lang::Core_rotate( Lang::THE_NAMESPACE_Shapes_Geometry, "rotate" ) );
	env->initDefineCoreFunction( new Lang::Core_rotate3d( Lang::THE_NAMESPACE_Shapes_Geometry3D, "rotate" ) );
	env->initDefineCoreFunction( new Lang::Core_scale( Lang::THE_NAMESPACE_Shapes_Geometry, "scale" ) );
	env->initDefineCoreFunction( new Lang::Core_scale3d( Lang::THE_NAMESPACE_Shapes_Geometry3D, "scale" ) );
	env->initDefineCoreFunction( new Lang::Core_affinetransform( Lang::THE_NAMESPACE_Shapes_Geometry, "affinetransform" ) );
	env->initDefineCoreFunction( new Lang::Core_affinetransform3d( Lang::THE_NAMESPACE_Shapes_Geometry3D, "affinetransform" ) );
	env->initDefineCoreFunction( new Lang::Core_inverse( Lang::THE_NAMESPACE_Shapes_Geometry, "inverse" ) );
	env->initDefineCoreFunction( new Lang::Core_inverse3d( Lang::THE_NAMESPACE_Shapes_Geometry3D, "inverse" ) );

	env->initDefineCoreFunction( new Lang::Core_formxo( Lang::THE_NAMESPACE_Shapes_Graphics_PDF, "formxo" ) );
	env->initDefineCoreFunction( new Lang::Core_importRasterImage( Lang::THE_NAMESPACE_Shapes_Graphics, "import_raster" ) );
	env->initDefineCoreFunction( new Lang::Core_transparencygroup( Lang::THE_NAMESPACE_Shapes_Graphics, "tgroup" ) );

	env->initDefineCoreFunction( new Lang::Core_tag( Lang::THE_NAMESPACE_Shapes_Graphics_Tag, "tag" ) );
	env->initDefineCoreFunction( new Lang::Core_find( Lang::THE_NAMESPACE_Shapes_Graphics_Tag, "find" ) );
	env->initDefineCoreFunction( new Lang::Core_findall( Lang::THE_NAMESPACE_Shapes_Graphics_Tag, "findall" ) );

	env->initDefineCoreFunction( new Lang::Core_ambient_light( Lang::THE_NAMESPACE_Shapes_Traits_Light, "ambient_light" ) );
	env->initDefineCoreFunction( new Lang::Core_specular_light( Lang::THE_NAMESPACE_Shapes_Traits_Light, "specular_light" ) );
	env->initDefineCoreFunction( new Lang::Core_distant_light( Lang::THE_NAMESPACE_Shapes_Traits_Light, "distant_light" ) );
	env->initDefineCoreFunction( new Lang::Core_phong( Lang::THE_NAMESPACE_Shapes_Traits_Light, "phong" ) );

	env->initDefineCoreFunction( new Lang::Core_array( Lang::THE_NAMESPACE_Shapes_Data, "array" ) );
	env->initDefineCoreFunction( new Lang::Core_graph( Lang::THE_NAMESPACE_Shapes_Data, "graph" ) );
	env->initDefineCoreFunction( new Lang::Core_walk( Lang::THE_NAMESPACE_Shapes_Data, "walk" ) );
	env->initDefineCoreFunction( new Lang::Core_importPDFpages( Lang::THE_NAMESPACE_Shapes_Graphics, "import" ) );

	env->initDefineCoreFunction( new Lang::Core_svg_path( Lang::THE_NAMESPACE_Shapes_Geometry, "svg_path" ) );

	env->initDefineCoreFunction( new Lang::Core_sprintf( Lang::THE_NAMESPACE_Shapes_String, "sprintf" ) );
	env->initDefineCoreFunction( new Lang::Core_strftime( Lang::THE_NAMESPACE_Shapes_String, "strftime" ) );

	env->initDefineCoreFunction( new Lang::Core_newrandom( Lang::THE_NAMESPACE_Shapes_Numeric_Random, "newRandom" ) );
	env->initDefineCoreFunction( new Lang::Core_devrandom( Lang::THE_NAMESPACE_Shapes_Numeric_Random, "devRandom" ) );

	env->initDefineCoreFunction( new Lang::Core_destination( Lang::THE_NAMESPACE_Shapes_Graphics_PDF, "destination" ) );

	env->initDefineCoreFunction( new Lang::Core_textrenderingmode( Lang::THE_NAMESPACE_Shapes_Text, "textmode" ) );
	env->initDefineCoreFunction( new Lang::Core_manualkern( Lang::THE_NAMESPACE_Shapes_Text, "kerning" ) );
	env->initDefineCoreFunction( new Lang::Core_automatickern( Lang::THE_NAMESPACE_Shapes_Text, "kern" ) );

	env->initDefineCoreFunction( new Lang::Core_importFont( Lang::THE_NAMESPACE_Shapes_Text, "font" ) );

	env->initDefineCoreFunction( new Lang::Core_Unicode( Lang::THE_NAMESPACE_Shapes_String, "Unicode" ) );
	env->initDefineCoreFunction( new Lang::Core_AdobeGlyphList( Lang::THE_NAMESPACE_Shapes_String, "AdobeGlyphList" ) );
}
