/* This file is part of Shapes.
 *
 * Shapes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Shapes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2009 Henrik Tidefelt
 */

#pragma once

#include <string>
#include <gsl/gsl_matrix.h>

namespace Shapes
{
	namespace Helpers
	{
		std::string mathematicaFormat( size_t n1, size_t n2, size_t tda, const double * data );
		std::string mathematicaFormatTransposed( size_t n1, size_t n2, size_t tda, const double * data );
		std::string mathematicaFormat( size_t n, size_t stride, const double ** begin, const double ** end );
		std::string mathematicaFormat( size_t n, size_t stride, const double * data );
		std::string mathematicaFormat( const gsl_vector & x );
		std::string mathematicaFormat( const gsl_matrix & x, bool transpose = false );
	}
}
