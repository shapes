/* This file is part of Shapes.
 *
 * Shapes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Shapes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2008, 2009, 2014 Henrik Tidefelt
 */

#pragma once

#include <stddef.h>
#include <iostream>
#include <vector>
#include <list>
#include <sys/types.h>
#include <sys/stat.h>

#include "Shapes_Ast_decls.h"

namespace Shapes
{
	namespace Ast
	{
		class FileInfo
		{
		public:
			std::string name_;
			std::vector< Ast::Node * > topLevelNodes_;

			FileInfo( const std::string & name );
		};

		class FileID
		{
			static const char * INPUTLIST_PREFIX;
			static const size_t INPUTLIST_PREFIX_LENGTH;

		public:
			typedef enum { NORMAL_FILE, IN_MEMORY, SPECIAL, INTERNAL } Kind;
		private:
			Kind kind_;
			dev_t st_dev_;
			ino_t st_ino_; /* Also used to index an IN_MEMORY */
			const char * strData_; /* A complete filename for a NORMAL_FILE, the identifier of a SPECIAL file, and unused for IN_MEMORY data. */
			FileID( Kind kind, dev_t st_dev, ino_t st_ino, const char * strData );
		public:
			static const FileID * build_stat( const struct stat & stat, const std::string & filename );
			static const FileID * build_inMemory( ino_t index, const std::string & sourceName );
			static const FileID * build_fresh_inMemory( );
			static const FileID * build_special( const char * specialName );
			static const FileID * build_internal( const char * locationName ); /* Does not deep-copy locationName.  Returns a new object each time, no matter if locationName has been used before! */
			static bool is_inMemory( const std::string & filename, const FileID ** fileDst );

			std::vector< Ast::Node * > & nodes( ) const;
			const char * name( ) const;
			bool hasPosition( ) const;
			std::istream * open( std::ifstream * ifsMem, std::istringstream * issMem ) const;
			void setMem( const char * data ); /* Applicable only to IN_MEMORY objects. */
			int inMemoryIndex( ) const;
			bool operator < ( const FileID & other ) const;
			size_t byteColumnToUTF8Column( size_t line, size_t byteCol ) const;
			size_t UTF8ColumnTobyteColumn( size_t line, size_t utf8Col ) const;

			static size_t inputlistPrefixLength( );
			static size_t UTF8ColumnTobyteColumn( const std::string & line, size_t utf8Col );
			static size_t byteColumnToUTF8Column( const std::string & line, size_t byteCol );

		private:
			void initInfo( const std::string & name ) const;
		};

		class FileIDPtrLess
		{
		public:
			bool operator () ( const FileID * fid1, const FileID * fid2 ) const
			{
				return (*fid1) < (*fid2);
			}
		};

		/* Important note on the storage of SourceLocation instances:
		 * The basic rule is that all SourceLocation instances should remain in memory
		 * until the end of program execution.  This allows any classes with shorter life
		 * cycles to use references instead of copies.  However, to avoid the risk of
		 * creating references to instances that won't live forever, it is crucial that
		 * the actual instances are *exactly* the onces that will last till the end.
		 * In other words, classes whose instances won't last till the end *must* use
		 * references instead of keeping their own copes.
		 */
		class SourceLocation
		{
		public:
			static const FileID * UNKNOWN_FILE;

			const FileID * file_;
			size_t firstLine;
			size_t firstColumn; /* In bytes, shall be converted to utf-8 position when displaying. */
			size_t lastLine;
			size_t lastColumn; /* In bytes, shall be converted to utf-8 position when displaying. */

			/* The lifetime of a SourceLocation instance should pretty much last for the entire program execution,
			 * so that instead of making copies all the time, we can use members of type const SourceLocation &.
			 * To avoid accidentally initializing our const SourceLocation & members with instances that won't last the
			 * entire program execution, we remove access to the copy constructor.
			 *
			 * At the end of parsing, SourceLocation instances should only exist in three places:
			 *   1) The AST, that is in instances of classes derived from Ast::Node.
			 *   2) The global storage for auxilliary source locations (created using Ast::theSourceLocationFactory).
			 *   3) Static variables in functions (only for "internal" locations constructed with FileID::build_internal).
			 * All other classes should only keep const SourceLocation & members.  Any class that violates this rule
			 * and whose instances won't live for the rest of the program execution, is a potential source of use-after-free
			 * memory bugs.
			 *
			 * When building the AST, the scanner's SourceLocation instance (shapeslloc) obviously need to be copied each time
			 * a node is constructed.  For that purpose, a special constructor taking an additional dummy bool argument is provided.
			 */

		private:
			/* Prevent accidental use of copy constructor. */
			SourceLocation( const SourceLocation & orig );
		public:
			SourceLocation( ); /* A default constructor is required by Bison.  Besides that, it should not be used! */
			SourceLocation( const FileID * file ); /* New location for first position in file. */
			SourceLocation( const SourceLocation & firstLoc, const Ast::SourceLocation & lastLoc ); /* New location spanning the ones given. */
			/* Disguised copy constructor.  Use only to construct copies that will live as long as the AST.
			 * The dummy argument is there to force the user of this constructor to think twice.
			 */
			SourceLocation( const SourceLocation & orig, bool dummy );
			/* Assigning source locations is considered relatively safe as long as the above rules about storage is followed.
			 * Just note that it is probably not a good idea to have const SourceLocation & references around pointing to the
			 * instance being assigned.
			 */
			SourceLocation & operator = ( const SourceLocation & orig );
			void swap( SourceLocation * other );

			bool isUnknown( ) const;
			bool contains( const SourceLocation & loc2 ) const;
			friend std::ostream & operator << ( std::ostream & os, const SourceLocation & self );

			void copy( std::ostream * os ) const;
			Ast::Expression * findExpression( ) const;
		};

		class SourceLocationFactory
		{
			std::list< Ast::SourceLocation * > mem_;
		public:
			SourceLocationFactory( );
			~SourceLocationFactory( );
			const Ast::SourceLocation & construct( const FileID * file );
			const Ast::SourceLocation & construct_internal( const char * locationName ); /* Does not deep-copy locationName. */
		};

		extern const SourceLocation THE_UNKNOWN_LOCATION;
		extern SourceLocationFactory theSourceLocationFactory;

		std::ostream & operator << ( std::ostream & os, const SourceLocation & self );

	}

}
