/* This file is part of Shapes.
 *
 * Shapes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Shapes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2008, 2013, 2014 Henrik Tidefelt
 */

#pragma once

#include "ast.h"

namespace Shapes
{
	namespace Helpers
	{

		template< class T >
		RefCountPtr< T >
		down_cast_ContinuationArgument( const RefCountPtr< const Lang::Value > & val, const Kernel::Continuation * locCont, bool voidIsNull = false )
		{
			RefCountPtr< T > res = val.down_cast< T >( );
			if( res == NullPtr< T >( ) )
				{
					if( ! voidIsNull ||
							dynamic_cast< const Lang::Void * >( val.getPtr( ) ) == 0 )
						{
							throw Exceptions::ContinuationTypeMismatch( locCont, val->getTypeName( ), T::staticTypeName( ) );
						}
				}
			return res;
		}

	}

	namespace Kernel
	{

		class ExitContinuation : public Kernel::Continuation
		{
			bool * done_;
		public:
			ExitContinuation( bool * done, const Ast::SourceLocation & traceLoc );
			~ExitContinuation( );
			virtual void takeHandle( Kernel::VariableHandle val, Kernel::EvalState * evalState, bool dummy ) const;
			virtual Kernel::ContRef up( ) const;
			virtual RefCountPtr< const char > description( ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
		};

		class DefaultErrorContinuation : public Kernel::Continuation
		{
		public:
			DefaultErrorContinuation( const Ast::SourceLocation & traceLoc );
			~DefaultErrorContinuation( );
			virtual void takeValue( const RefCountPtr< const Lang::Value > & val, Kernel::EvalState * evalState, bool dummy ) const;
			virtual Kernel::ContRef up( ) const;
			virtual RefCountPtr< const char > description( ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
		};

		class IfContinuation : public Kernel::Continuation
		{
			Kernel::VariableHandle consequence_;
			Kernel::VariableHandle alternative_;
			Kernel::ContRef cont_;
		public:
			IfContinuation( const Kernel::VariableHandle & consequence, const Kernel::VariableHandle & alternative, const Kernel::ContRef & cont, const Ast::SourceLocation & traceLoc );
			~IfContinuation( );
			virtual void takeValue( const RefCountPtr< const Lang::Value > & val, Kernel::EvalState * evalState, bool dummy ) const;
			virtual Kernel::ContRef up( ) const;
			virtual RefCountPtr< const char > description( ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
		};

		/* IgnoreContinuation is used by when forcing thunks for immediate right hand sides of bind nodes.
		 */
		class IgnoreContinuation : public Kernel::Continuation
		{
			Kernel::ContRef cont_;
		public:
			IgnoreContinuation( const Kernel::ContRef & cont, const Ast::SourceLocation & traceLoc );
			~IgnoreContinuation( );
			virtual void takeHandle( Kernel::VariableHandle val, Kernel::EvalState * evalState, bool dummy ) const;
			virtual Kernel::ContRef up( ) const;
			virtual RefCountPtr< const char > description( ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
		};

		/* IntroduceStateContinuation is used by the expression IntroduceState.
		 */
		class IntroduceStateContinuation : public Kernel::Continuation
		{
			Kernel::PassedEnv env_;
			size_t * pos_;
			Kernel::ContRef cont_;
		public:
			IntroduceStateContinuation( const Kernel::PassedEnv & _env, size_t * _pos, const Kernel::ContRef & _cont, const Ast::SourceLocation & _traceLoc );
			~IntroduceStateContinuation( );
			virtual void takeValue( const RefCountPtr< const Lang::Value > & val, Kernel::EvalState * evalState, bool dummy ) const;
			virtual Kernel::ContRef up( ) const;
			virtual RefCountPtr< const char > description( ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
		};

		/* StoreValueContinuation will store the returned object at the specified address, and then pass it on
		 * to the next continuation.
		 * Note that it is the responsibility of someone else to make sure that *res still exists when the continuation is invoked.
		 * Generally, StoreVariableContinuation shall be preferred since it has no such problem.
		 */
		class StoreValueContinuation : public Kernel::Continuation
		{
			Kernel::ValueRef * res_;
			Kernel::ContRef cont_;
		public:
			StoreValueContinuation( Kernel::ValueRef * res, const Kernel::ContRef & cont, const Ast::SourceLocation & traceLoc );
			~StoreValueContinuation( );
			virtual void takeHandle( Kernel::VariableHandle val, Kernel::EvalState * evalState, bool dummy ) const;
			virtual Kernel::ContRef up( ) const;
			virtual RefCountPtr< const char > description( ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
		};

		class StoreVariableContinuation : public Kernel::Continuation
		{
			Kernel::VariableHandle dst_;
			Kernel::ContRef cont_;
		public:
			StoreVariableContinuation( const Kernel::VariableHandle & res, const Kernel::ContRef & cont, const Ast::SourceLocation & traceLoc );
			~StoreVariableContinuation( );
			virtual void takeHandle( Kernel::VariableHandle val, Kernel::EvalState * evalState, bool dummy ) const;
			virtual Kernel::ContRef up( ) const;
			virtual RefCountPtr< const char > description( ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
		};

		/* The InsertionContinuation sends the returned value to a warm variable, and passes THE_SLOT_VARIABLE (a null value) to the next continuation.
		 */
		class InsertionContinuation : public Kernel::Continuation
		{
			mutable Kernel::StateHandle dst_; /* This being mutable is actually quite ugly... */
			Kernel::PassedDyn dyn_;
			Kernel::ContRef cont_;
		public:
			InsertionContinuation( const Kernel::StateHandle & dst, const Kernel::ContRef & cont, const Kernel::PassedDyn & dyn, const Ast::SourceLocation & traceLoc );
			~InsertionContinuation( );
			virtual void takeValue( const RefCountPtr< const Lang::Value > & val, Kernel::EvalState * evalState, bool dummy ) const;
			virtual Kernel::ContRef up( ) const;
			virtual RefCountPtr< const char > description( ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
		};

		/* StmtStoreValueContinuation is like StoreValueContinuation, except that it passes THE_SLOT_VARIABLE (a null value) to the next continuation.
		 */
		class StmtStoreValueContinuation : public Kernel::Continuation
		{
			Kernel::ValueRef * res_;
			Kernel::ContRef cont_;
		public:
			StmtStoreValueContinuation( Kernel::ValueRef * res, const Kernel::ContRef & cont, const Ast::SourceLocation & traceLoc );
			~StmtStoreValueContinuation( );
			virtual void takeValue( const RefCountPtr< const Lang::Value > & val, Kernel::EvalState * evalState, bool dummy ) const;
			virtual Kernel::ContRef up( ) const;
			virtual RefCountPtr< const char > description( ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
		};

		class StmtStoreVariableContinuation : public Kernel::Continuation
		{
			Kernel::VariableHandle dst_;
			Kernel::ContRef cont_;
		public:
			StmtStoreVariableContinuation( const Kernel::VariableHandle & res, const Kernel::ContRef & cont, const Ast::SourceLocation & traceLoc );
			~StmtStoreVariableContinuation( );
			virtual void takeValue( const RefCountPtr< const Lang::Value > & val, Kernel::EvalState * evalState, bool dummy ) const;
			virtual Kernel::ContRef up( ) const;
			virtual RefCountPtr< const char > description( ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
		};

		class ForcingContinuation : public Kernel::Continuation
		{
			Kernel::ContRef cont_;
		public:
			ForcingContinuation( const Kernel::ContRef & cont, const Ast::SourceLocation & traceLoc );
			~ForcingContinuation( );
			virtual void takeValue( const RefCountPtr< const Lang::Value > & val, Kernel::EvalState * evalState, bool dummy ) const;
			virtual Kernel::ContRef up( ) const;
			virtual RefCountPtr< const char > description( ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
		};

		class ForcingListContinuation : public Kernel::Continuation
		{
			Kernel::ContRef cont_;
			bool forceStructures_; /* Force any list element that is a Structure. */
			bool consify_; /* Expand resulting list to only use the SingleListPair and SingleListNull subtypes of SingleList. */
			size_t index_; /* Index of element being forced, initially 0. */
			RefCountPtr< const Lang::SingleList > pile_; /* Accumulator for forced result. */
		public:
			ForcingListContinuation( const Kernel::ContRef & cont, const Ast::SourceLocation & traceLoc, bool forceStructures = false, bool consify = false );
			ForcingListContinuation( bool forceStructures, bool consify, size_t index, const RefCountPtr< const Lang::SingleList > & pile, const Kernel::ContRef & cont, const Ast::SourceLocation & traceLoc );
			~ForcingListContinuation( );
			virtual void takeValue( const RefCountPtr< const Lang::Value > & val, Kernel::EvalState * evalState, bool dummy ) const;
			virtual Kernel::ContRef up( ) const;
			virtual RefCountPtr< const char > description( ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
		};

		class ForcingListCarContinuation : public Kernel::Continuation
		{
			Kernel::ContRef cont_;
			bool forceStructures_; /* If forced value is Structure, force its fields. */
			bool consify_; /* Expand resulting list to only use the SingleListPair and SingleListNull subtypes of SingleList. */
			size_t index_; /* Index of element being forced, initially 0. */
			RefCountPtr< const Lang::SingleList > cdr_;
			RefCountPtr< const Lang::SingleList > pile_; /* Accumulator for forced result. */
		public:
			ForcingListCarContinuation( bool forceStructures, bool consify, size_t index, const RefCountPtr< const Lang::SingleList > & cdr, const RefCountPtr< const Lang::SingleList > & pile, const Kernel::ContRef & cont, const Ast::SourceLocation & traceLoc );
			~ForcingListCarContinuation( );
			virtual void takeValue( const RefCountPtr< const Lang::Value > & val, Kernel::EvalState * evalState, bool dummy ) const;
			virtual Kernel::ContRef up( ) const;
			virtual RefCountPtr< const char > description( ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
		};

		class ForcingListCarStructureContinuation : public Kernel::ForcedStructureContinuation
		{
			Kernel::ContRef cont_;
			bool consify_; /* Expand resulting list to only use the SingleListPair and SingleListNull subtypes of SingleList. */
			size_t index_; /* Index of element being forced, initially 0. */
			RefCountPtr< const Lang::SingleList > cdr_;
			RefCountPtr< const Lang::SingleList > pile_; /* Accumulator for forced result. */
		public:
			ForcingListCarStructureContinuation( bool consify, size_t index, const RefCountPtr< const Lang::SingleList > & cdr, const RefCountPtr< const Lang::SingleList > & pile, const Kernel::ContRef & cont, const Ast::SourceLocation & traceLoc );
			~ForcingListCarStructureContinuation( );
			virtual void takeStructure( const RefCountPtr< const Lang::Structure > & structure, Kernel::EvalState * evalState ) const;
			virtual Kernel::ContRef up( ) const;
			virtual RefCountPtr< const char > description( ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
		};

		class ForcingConsCarContinuation : public Kernel::Continuation
		{
			Kernel::ContRef cont_;
			bool forceStructures_; /* If forced value is Structure, force its fields. */
			bool consify_; /* Expand resulting list to only use the SingleListPair and SingleListNull subtypes of SingleList. */
			size_t index_; /* Index of element being forced, initially 0. */
			Kernel::VariableHandle cdr_;
			RefCountPtr< const Lang::SingleList > pile_; /* Accumulator for forced result. */
		public:
			ForcingConsCarContinuation( bool forceStructures, bool consify, size_t index, const Kernel::VariableHandle & cdr, const RefCountPtr< const Lang::SingleList > & pile, const Kernel::ContRef & cont, const Ast::SourceLocation & traceLoc );
			~ForcingConsCarContinuation( );
			virtual void takeValue( const RefCountPtr< const Lang::Value > & val, Kernel::EvalState * evalState, bool dummy ) const;
			virtual Kernel::ContRef up( ) const;
			virtual RefCountPtr< const char > description( ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
		};

		class ForcingConsCarStructureContinuation : public Kernel::ForcedStructureContinuation
		{
			Kernel::ContRef cont_;
			bool consify_; /* Expand resulting list to only use the SingleListPair and SingleListNull subtypes of SingleList. */
			size_t index_; /* Index of element being forced, initially 0. */
			Kernel::VariableHandle cdr_;
			RefCountPtr< const Lang::SingleList > pile_; /* Accumulator for forced result. */
		public:
			ForcingConsCarStructureContinuation( bool consify, size_t index, const Kernel::VariableHandle & cdr, const RefCountPtr< const Lang::SingleList > & pile, const Kernel::ContRef & cont, const Ast::SourceLocation & traceLoc );
			~ForcingConsCarStructureContinuation( );
			virtual void takeStructure( const RefCountPtr< const Lang::Structure > & structure, Kernel::EvalState * evalState ) const;
			virtual Kernel::ContRef up( ) const;
			virtual RefCountPtr< const char > description( ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
		};

		class Transform2DCont : public Kernel::Continuation
		{
			Lang::Transform2D tf_;
			Kernel::ContRef cont_;
		public:
			Transform2DCont( Lang::Transform2D tf, const Kernel::ContRef & cont, const Ast::SourceLocation & traceLoc );
			virtual ~Transform2DCont( );
			virtual void takeValue( const RefCountPtr< const Lang::Value > & val, Kernel::EvalState * evalState, bool dummy ) const;
			virtual Kernel::ContRef up( ) const;
			virtual RefCountPtr< const char > description( ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
		};

		class Transform3DCont : public Kernel::Continuation
		{
			Lang::Transform3D tf_;
			Kernel::ContRef cont_;
		public:
			Transform3DCont( Lang::Transform3D tf, const Kernel::ContRef & cont, const Ast::SourceLocation & traceLoc );
			virtual ~Transform3DCont( );
			virtual void takeValue( const RefCountPtr< const Lang::Value > & val, Kernel::EvalState * evalState, bool dummy ) const;
			virtual Kernel::ContRef up( ) const;
			virtual RefCountPtr< const char > description( ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
		};

		class PathApplication2DCont : public Kernel::Continuation
		{
			RefCountPtr< const Lang::ElementaryPath2D > path_;
			Kernel::ContRef cont_;
		public:
			PathApplication2DCont( RefCountPtr< const Lang::ElementaryPath2D > path, const Kernel::ContRef & cont, const Ast::SourceLocation & traceLoc );
			virtual ~PathApplication2DCont( );
			virtual void takeValue( const RefCountPtr< const Lang::Value > & val, Kernel::EvalState * evalState, bool dummy ) const;
			virtual Kernel::ContRef up( ) const;
			virtual RefCountPtr< const char > description( ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
		};

		class PathApplication3DCont : public Kernel::Continuation
		{
			RefCountPtr< const Lang::ElementaryPath3D > path_;
			Kernel::ContRef cont_;
		public:
			PathApplication3DCont( RefCountPtr< const Lang::ElementaryPath3D > path, const Kernel::ContRef & cont, const Ast::SourceLocation & traceLoc );
			virtual ~PathApplication3DCont( );
			virtual void takeValue( const RefCountPtr< const Lang::Value > & val, Kernel::EvalState * evalState, bool dummy ) const;
			virtual Kernel::ContRef up( ) const;
			virtual RefCountPtr< const char > description( ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
		};

		class ComposedFunctionCall_cont : public Kernel::Continuation
		{
			RefCountPtr< const Lang::Function > second_;
			Kernel::PassedDyn dyn_;
			Kernel::ContRef cont_;
		public:
			ComposedFunctionCall_cont( const RefCountPtr< const Lang::Function > & second, const Kernel::PassedDyn & dyn, const Kernel::ContRef & cont, const Ast::SourceLocation & callLoc );
			virtual ~ComposedFunctionCall_cont( );
			virtual void takeHandle( Kernel::VariableHandle val, Kernel::EvalState * evalState, bool dummy ) const;
			virtual Kernel::ContRef up( ) const;
			virtual RefCountPtr< const char > description( ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
		};

		class ForceFunctionAndCall_2_args_cont : public Kernel::Continuation
		{
			Kernel::VariableHandle arg1_;
			Kernel::VariableHandle arg2_;
			Kernel::PassedDyn dyn_;
			Kernel::ContRef cont_;
		public:
			ForceFunctionAndCall_2_args_cont( const Kernel::VariableHandle & arg1, const Kernel::VariableHandle & arg2, const Kernel::PassedDyn & dyn, const Kernel::ContRef & cont, const Ast::SourceLocation & callLoc );
			virtual ~ForceFunctionAndCall_2_args_cont( );
			virtual void takeValue( const RefCountPtr< const Lang::Value > & val, Kernel::EvalState * evalState, bool dummy ) const;
			virtual Kernel::ContRef up( ) const;
			virtual RefCountPtr< const char > description( ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
		};

		class SingleFoldLCont : public Kernel::Continuation
		{
			RefCountPtr< const Lang::SingleList > cdr_;
			RefCountPtr< const Lang::Function > op_;
			Kernel::PassedDyn dyn_;
			Kernel::ContRef cont_;
		public:
			SingleFoldLCont( const RefCountPtr< const Lang::SingleList > & cdr, const RefCountPtr< const Lang::Function > & op, const Kernel::PassedDyn & dyn, Kernel::ContRef cont, const Ast::SourceLocation & traceLoc );
			virtual ~SingleFoldLCont( );
			virtual void takeHandle( Kernel::VariableHandle val, Kernel::EvalState * evalState, bool dummy ) const;
			virtual Kernel::ContRef up( ) const;
			virtual RefCountPtr< const char > description( ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
		};

		class SingleFoldRCont : public Kernel::Continuation
		{
			Kernel::VariableHandle car_;
			RefCountPtr< const Lang::Function > op_;
			Kernel::PassedDyn dyn_;
			Kernel::ContRef cont_;
		public:
			SingleFoldRCont( const Kernel::VariableHandle & car, const RefCountPtr< const Lang::Function > & op, const Kernel::PassedDyn & dyn, Kernel::ContRef cont, const Ast::SourceLocation & traceLoc );
			virtual ~SingleFoldRCont( );
			virtual void takeHandle( Kernel::VariableHandle val, Kernel::EvalState * evalState, bool dummy ) const;
			virtual Kernel::ContRef up( ) const;
			virtual RefCountPtr< const char > description( ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
		};

		class SingleFoldSLCont : public Kernel::Continuation
		{
			RefCountPtr< const Lang::SingleList > cdr_;
			RefCountPtr< const Lang::Function > op_;
			Kernel::StateHandle state_;
			Kernel::PassedDyn dyn_;
			Kernel::ContRef cont_;
		public:
			SingleFoldSLCont( const RefCountPtr< const Lang::SingleList > & cdr, const RefCountPtr< const Lang::Function > & op, Kernel::StateHandle state, const Kernel::PassedDyn & dyn, Kernel::ContRef cont, const Ast::SourceLocation & traceLoc );
			virtual ~SingleFoldSLCont( );
			virtual void takeHandle( Kernel::VariableHandle val, Kernel::EvalState * evalState, bool dummy ) const;
			virtual Kernel::ContRef up( ) const;
			virtual RefCountPtr< const char > description( ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
		};

		class SingleFoldSRCont : public Kernel::Continuation
		{
			Kernel::VariableHandle car_;
			RefCountPtr< const Lang::Function > op_;
			Kernel::StateHandle state_;
			Kernel::PassedDyn dyn_;
			Kernel::ContRef cont_;
		public:
			SingleFoldSRCont( const Kernel::VariableHandle & car, const RefCountPtr< const Lang::Function > & op, Kernel::StateHandle state, const Kernel::PassedDyn & dyn, Kernel::ContRef cont, const Ast::SourceLocation & traceLoc );
			virtual ~SingleFoldSRCont( );
			virtual void takeHandle( Kernel::VariableHandle val, Kernel::EvalState * evalState, bool dummy ) const;
			virtual Kernel::ContRef up( ) const;
			virtual RefCountPtr< const char > description( ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
		};

		class RelaxContinuation : public Kernel::Continuation
		{
			Kernel::ContRef cont_;
		public:
			RelaxContinuation( Kernel::ContRef & cont );
			virtual ~RelaxContinuation( );
			virtual void takeValue( const RefCountPtr< const Lang::Value > & val, Kernel::EvalState * evalState, bool dummy ) const;
			virtual Kernel::ContRef up( ) const;
			virtual RefCountPtr< const char > description( ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
		};

	}

}
