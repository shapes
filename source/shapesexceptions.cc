/* This file is part of Shapes.
 *
 * Shapes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Shapes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2008, 2009, 2013, 2014 Henrik Tidefelt
 */

#include "shapesexceptions.h"
#include "ast.h"
#include "astclass.h"
#include "globals.h"
#include "functiontypes.h"

using namespace Shapes;
using namespace std;


const std::string Exceptions::additionalLinesPrefix = "| ";


Interaction::MessageString::MessageString( )
: msg_( NULL ), msgMem_( NullPtr< const char >( ) )
{ }

Interaction::MessageString::MessageString( const char * msg )
: msg_( msg ), msgMem_( NullPtr< const char >( ) )
{ }

Interaction::MessageString::MessageString( const RefCountPtr< const char > & msg )
: msg_( msg.getPtr( ) ), msgMem_( msg )
{ }

void
Interaction::MessageString::show( std::ostream & os ) const
{
  os << msg_ ;
}

void
Interaction::MessageString::show( const std::string & eachLinePrefix, std::ostream & os ) const
{
  if( null( ) )
    return;

  std::istringstream is( msg_ );
  std::string line;
  for( std::getline( is, line ); ! is.eof( ); std::getline( is, line ) )
    {
      os << eachLinePrefix << line << std::endl ;
    }
}


std::ostream &
Interaction::operator << ( std::ostream & os, const Interaction::MessageString & self )
{
  self.show( os );
  return os;
}


Exceptions::NotImplemented::NotImplemented( const char * functionality )
	: functionality_( functionality )
{ }

Exceptions::NotImplemented::~NotImplemented( )
{ }

void
Exceptions::NotImplemented::display( std::ostream & os ) const
{
	os << "Under construction: " << functionality_ << " is not implemented." << std::endl ;
}

const char * Exceptions::Exception::locsep = ": ";

Exceptions::Exception::Exception( )
{ }

Exceptions::Exception::~Exception( )
{ }


Exceptions::MiscellaneousRequirement::MiscellaneousRequirement( const Ast::SourceLocation & loc, const Interaction::MessageString & msg )
	: Exceptions::CatchableError( loc ), msg_( msg )
{ }

Exceptions::MiscellaneousRequirement::MiscellaneousRequirement( const Interaction::MessageString & msg )
	: Exceptions::CatchableError( Ast::THE_UNKNOWN_LOCATION ), msg_( msg )
{ }

Exceptions::MiscellaneousRequirement::~MiscellaneousRequirement( )
{ }

void
Exceptions::MiscellaneousRequirement::display( ostream & os ) const
{
	os << "Failed to meet requirement: " << msg_ << std::endl ;
}

RefCountPtr< const Lang::Exception >
Exceptions::MiscellaneousRequirement::clone( const Kernel::ContRef & cont ) const
{
	static RefCountPtr< const Lang::Symbol > kind( new Lang::Symbol( "misc" ) );
	std::ostringstream os;
	display( os );
	return RefCountPtr< const Lang::Exception >( new Lang::Exception( this->getLoc( ),
																																		kind,
																																		RefCountPtr< const Lang::String >( new Lang::String( "(core, generic)", true ) ),
																																		Lang::THE_VOID,
																																		strrefdup( os ),
																																		cont,
																																		this->exitCode( ) ) );
}


Exceptions::HandlerError::HandlerError( const Interaction::MessageString & msg )
	: Exceptions::CatchableError( Ast::THE_UNKNOWN_LOCATION ), msg_( msg )
{ }

Exceptions::HandlerError::~HandlerError( )
{ }

void
Exceptions::HandlerError::display( ostream & os ) const
{
	os << "Error handler message: " << msg_ << std::endl ;
}

RefCountPtr< const Lang::Exception >
Exceptions::HandlerError::clone( const Kernel::ContRef & cont ) const
{
	static RefCountPtr< const Lang::Symbol > kind( new Lang::Symbol( "misc" ) );
	std::ostringstream os;
	display( os );
	return RefCountPtr< const Lang::Exception >( new Lang::Exception( this->getLoc( ),
																																		kind,
																																		RefCountPtr< const Lang::String >( new Lang::String( "(default error handler)", true ) ),
																																		Lang::THE_VOID,
																																		strrefdup( os ),
																																		cont,
																																		this->exitCode( ) ) );
}


Exceptions::MiscellaneousStaticInconsistency::MiscellaneousStaticInconsistency( const Ast::SourceLocation & loc, const Interaction::MessageString & msg )
	: Exceptions::StaticInconsistency( loc ), msg_( msg )
{ }

Exceptions::MiscellaneousStaticInconsistency::~MiscellaneousStaticInconsistency( )
{ }

void
Exceptions::MiscellaneousStaticInconsistency::display( ostream & os ) const
{
	os << "Misc error: " << msg_ << std::endl ;
}


Exceptions::NamespaceDirectoryError::NamespaceDirectoryError( const Ast::SourceLocation & loc, const Interaction::MessageString & msg )
  : Exceptions::StaticInconsistency( loc ), msg_( msg )
{ }

Exceptions::NamespaceDirectoryError::~NamespaceDirectoryError( )
{ }

void
Exceptions::NamespaceDirectoryError::display( ostream & os ) const
{
  os << "Error in namespace directory: " << msg_ << std::endl ;
}


Exceptions::AmbiguousNamespaceDirectory::AmbiguousNamespaceDirectory( const std::string & previousName, const std::string & currentDir )
  : Exceptions::StaticInconsistency( Ast::THE_UNKNOWN_LOCATION ), previousName_( previousName ), currentDir_( currentDir )
{ }

Exceptions::AmbiguousNamespaceDirectory::~AmbiguousNamespaceDirectory( )
{ }

void
Exceptions::AmbiguousNamespaceDirectory::display( ostream & os ) const
{
  os << "Ambiguous filesystem location of namespace first found at " << previousName_ << ", and later at " << currentDir_ << "." << std::endl ;
}


Exceptions::NamespaceDirectoryLookupError::NamespaceDirectoryLookupError( const Ast::SourceLocation & loc, Type type, const Ast::NamespacePath::const_iterator & nsBegin, const Ast::NamespacePath::const_iterator & nsEnd )
  : Exceptions::StaticInconsistency( loc ),
  type_( type )
{
  for( Ast::NamespacePath::const_iterator i = nsBegin; i != nsEnd; ++i ){
    namespacePath_.push_back( *i );
  }
}

Exceptions::NamespaceDirectoryLookupError::NamespaceDirectoryLookupError( Type type, const Ast::NamespacePath::const_iterator & nsBegin, const Ast::NamespacePath::const_iterator & nsEnd )
  : Exceptions::StaticInconsistency( Ast::THE_UNKNOWN_LOCATION ),
  type_( type )
{
  for( Ast::NamespacePath::const_iterator i = nsBegin; i != nsEnd; ++i ){
    namespacePath_.push_back( *i );
  }
}

Exceptions::NamespaceDirectoryLookupError::~NamespaceDirectoryLookupError( )
{ }

void
Exceptions::NamespaceDirectoryLookupError::display( ostream & os ) const
{
  switch( type_ ){
    case NOT_FOUND:
      os << "Unable to locate namespace directory for " ;
      for( Ast::NamespacePath::const_iterator i = namespacePath_.begin( ); i != namespacePath_.end( ); ++i ){
        if( ! Ast::SearchContext::isEncapsulationName( *i ) )
          os << Interaction::NAMESPACE_SEPARATOR << *i ;
      }
      os << "." << std::endl ;
      break;
    case WRONG_TYPE:
      os << "Encountered non-directory when resolving namespace directory for " ;
      for( Ast::NamespacePath::const_iterator i = namespacePath_.begin( ); i != namespacePath_.end( ); ++i ){
        if( ! Ast::SearchContext::isEncapsulationName( *i ) )
          os << Interaction::NAMESPACE_SEPARATOR << *i ;
      }
      os << "." << std::endl ;
      break;
    case GLOBAL:
      os << "The global namespace has no corresponding namespace directory." << std::endl ;
      break;
  }
}


Exceptions::NamespaceDirectoryFileError::NamespaceDirectoryFileError( const Ast::SourceLocation & loc, Type type, const Ast::NamespacePath & namespacePath, const std::string & filename )
  : Exceptions::StaticInconsistency( loc ),
  type_( type ),
  namespacePath_( namespacePath ),
  filename_( filename )
{ }

Exceptions::NamespaceDirectoryFileError::NamespaceDirectoryFileError( Type type, const Ast::NamespacePath & namespacePath, const std::string & filename )
  : Exceptions::StaticInconsistency( Ast::THE_UNKNOWN_LOCATION ),
  type_( type ),
  namespacePath_( namespacePath ),
  filename_( filename )
{ }

Exceptions::NamespaceDirectoryFileError::~NamespaceDirectoryFileError( )
{ }

void
Exceptions::NamespaceDirectoryFileError::display( ostream & os ) const
{
  switch( type_ ){
    case NOT_FOUND:
      os << "Could not find " << filename_ << " in the namespace directory of " ;
      for( Ast::NamespacePath::const_iterator i = namespacePath_.begin( ); i != namespacePath_.end( ); ++i ){
        if( ! Ast::SearchContext::isEncapsulationName( *i ) )
          os << Interaction::NAMESPACE_SEPARATOR << *i ;
      }
      os << "." << std::endl ;
      break;
    case WRONG_TYPE:
      os << "Expected " << filename_ << " to be a Shapes extension file in the namespace directory of " ;
      for( Ast::NamespacePath::const_iterator i = namespacePath_.begin( ); i != namespacePath_.end( ); ++i ){
        if( ! Ast::SearchContext::isEncapsulationName( *i ) )
          os << Interaction::NAMESPACE_SEPARATOR << *i ;
      }
      os << "." << std::endl ;
      break;
  }
}


Exceptions::NamespaceDirectoryInvalidEntries::NamespaceDirectoryInvalidEntries( const RefCountPtr< const Ast::NamespacePath > & namespacePath, const std::set< std::string > & invalidEntries )
  : Exceptions::StaticInconsistency( Ast::THE_UNKNOWN_LOCATION ),
  namespacePath_( namespacePath ), invalidEntries_( invalidEntries )
{ }

Exceptions::NamespaceDirectoryInvalidEntries::~NamespaceDirectoryInvalidEntries( )
{ }

void
Exceptions::NamespaceDirectoryInvalidEntries::display( ostream & os ) const
{
  os << "Namespace directory of " ;
  for( Ast::NamespacePath::const_iterator i = namespacePath_->begin( ); i != namespacePath_->end( ); ++i ){
    if( ! Ast::SearchContext::isEncapsulationName( *i ) )
      os << Interaction::NAMESPACE_SEPARATOR << *i ;
  }
  os << " is referring to invalid entries: {" ;
  typedef typeof invalidEntries_ SetType;
  for( SetType::const_iterator i = invalidEntries_.begin( ); i != invalidEntries_.end( ); ++i ){
    os << " " << *i ;
  }
  os << " }." << std::endl ;
}


Exceptions::NamespaceDirectoryCircularOrdering::NamespaceDirectoryCircularOrdering( const RefCountPtr< const Ast::NamespacePath > & namespacePath, const std::string & cycleEntry )
  : Exceptions::StaticInconsistency( Ast::THE_UNKNOWN_LOCATION ),
  namespacePath_( namespacePath ), cycleEntry_( cycleEntry )
{ }

Exceptions::NamespaceDirectoryCircularOrdering::~NamespaceDirectoryCircularOrdering( )
{ }

void
Exceptions::NamespaceDirectoryCircularOrdering::display( ostream & os ) const
{
  os << "Namespace directory has circular order constraints between entries in " ;
  for( Ast::NamespacePath::const_iterator i = namespacePath_->begin( ); i != namespacePath_->end( ); ++i ){
    if( ! Ast::SearchContext::isEncapsulationName( *i ) )
      os << Interaction::NAMESPACE_SEPARATOR << *i ;
  }
  os << " with " << cycleEntry_ << " appearing on a cycle." << std::endl ;
}


Exceptions::ScannerError::ScannerError( const Ast::SourceLocation & loc, const Interaction::MessageString & msg )
	: Exceptions::StaticInconsistency( loc ), msg_( msg )
{ }

Exceptions::ScannerError::~ScannerError( )
{ }

void
Exceptions::ScannerError::display( ostream & os ) const
{
	os << "Scanner error: " << msg_ << std::endl ;
}


Exceptions::ParserError::ParserError( const Ast::SourceLocation & loc, const Interaction::MessageString & msg, bool justAWarning )
	: Exceptions::StaticInconsistency( loc ), msg_( msg ), justAWarning_( justAWarning )
{ }

Exceptions::ParserError::~ParserError( )
{ }

void
Exceptions::ParserError::display( ostream & os ) const
{
	os << "Parser " << ( justAWarning_ ? "warning" : "error" ) << ": " << msg_ << std::endl ;
}


Exceptions::MemberAlsoAbstract::MemberAlsoAbstract( const Ast::SourceLocation & memberLoc, const char * id, const Ast::SourceLocation & abstractLoc )
	: Exceptions::StaticInconsistency( memberLoc ), id_( id ), abstractLoc_( abstractLoc )
{ }

Exceptions::MemberAlsoAbstract::~MemberAlsoAbstract( )
{ }

void
Exceptions::MemberAlsoAbstract::display( ostream & os ) const
{
	os << "The member " << id_ << " is also declared abstract, at " << abstractLoc_ << "." << std::endl ;
}


Exceptions::MemberAlreadyDeclared::MemberAlreadyDeclared( const Ast::SourceLocation & memberLoc, const char * id, const Ast::SourceLocation & oldLoc )
	: Exceptions::StaticInconsistency( memberLoc ), id_( id ), oldLoc_( oldLoc )
{ }

Exceptions::MemberAlreadyDeclared::~MemberAlreadyDeclared( )
{ }

void
Exceptions::MemberAlreadyDeclared::display( ostream & os ) const
{
	os << "The member " << id_ << " has already been declared, at " << oldLoc_ << "." << std::endl ;
}


Exceptions::PublicGetSetInNonfinalClass::PublicGetSetInNonfinalClass( const Ast::SourceLocation & memberLoc, const char * id )
	: Exceptions::StaticInconsistency( memberLoc ), id_( id )
{ }

Exceptions::PublicGetSetInNonfinalClass::~PublicGetSetInNonfinalClass( )
{ }

void
Exceptions::PublicGetSetInNonfinalClass::display( ostream & os ) const
{
	os << "Only members of final classes may have the public get or set specifier.  Member in question: " << id_ << "." << std::endl ;
}


Exceptions::TransformingMemberInNonfinalClass::TransformingMemberInNonfinalClass( const Ast::SourceLocation & memberLoc, const char * id )
	: Exceptions::StaticInconsistency( memberLoc ), id_( id )
{ }

Exceptions::TransformingMemberInNonfinalClass::~TransformingMemberInNonfinalClass( )
{ }

void
Exceptions::TransformingMemberInNonfinalClass::display( ostream & os ) const
{
	os << "Only members of final classes may have the <transforming> specifier.  Member in question: " << id_ << "." << std::endl ;
}


Exceptions::RepeatedFormal::RepeatedFormal( const Ast::SourceLocation & loc, const char * id )
	: Exceptions::StaticInconsistency( loc ), id_( id )
{ }

Exceptions::RepeatedFormal::~RepeatedFormal( )
{ }

void
Exceptions::RepeatedFormal::display( ostream & os ) const
{
	os << "Repeated formal: " << id_ << std::endl ;
}


Exceptions::StateUninitializedUse::StateUninitializedUse( const Ast::SourceLocation & loc, const Ast::PlacedIdentifier * id, const Ast::Node * decl )
  : Exceptions::StaticInconsistency( loc ), id_( id ), decl_( decl )
{ }

Exceptions::StateUninitializedUse::~StateUninitializedUse( )
{ }

void
Exceptions::StateUninitializedUse::display( ostream & os ) const
{
  os << "Using state " ;
  id_->show( os, Ast::Identifier::STATE );
  os << " before its point of declaration at " << decl_->loc( ) << "." << std::endl ;
}


Exceptions::IllegalFreeStates::IllegalFreeStates( const Ast::SourceLocation & loc, Ast::StateIDSet * freeStates, const char * reason )
	: Exceptions::StaticInconsistency( loc ), freeStates_( freeStates ), reason_( reason )
{ }

Exceptions::IllegalFreeStates::~IllegalFreeStates( )
{ }

void
Exceptions::IllegalFreeStates::display( ostream & os ) const
{
	os << "Free states are illegal here (" << reason_ << "):" ;
	typedef typeof *freeStates_ SetType;
	for( SetType::const_iterator i = freeStates_->begin( ); i != freeStates_->end( ); ++i )
		{
			os << " " << *i ;
		}
	os << std::endl ;
}


Exceptions::FreeStateIsAlsoPassed::FreeStateIsAlsoPassed( const Ast::SourceLocation & loc, Ast::StateID stateID )
	: Exceptions::StaticInconsistency( loc ), stateID_( stateID )
{ }

Exceptions::FreeStateIsAlsoPassed::~FreeStateIsAlsoPassed( )
{ }

void
Exceptions::FreeStateIsAlsoPassed::display( ostream & os ) const
{
	os << "Non-pure, delayed, expressions may not have free states (state " << stateID_ << ") which are also passed as state arguments in the same non-pure function application.  Please consider forcing immediate evaluation." << std::endl ;
}


Exceptions::RepeatedStateArgument::RepeatedStateArgument( const Ast::SourceLocation & loc, const RefCountPtr< Ast::StateIDSet > & passedStates )
	: Exceptions::StaticInconsistency( loc ), passedStates_( passedStates )
{ }

Exceptions::RepeatedStateArgument::~RepeatedStateArgument( )
{ }

void
Exceptions::RepeatedStateArgument::display( ostream & os ) const
{
	os << "Some of these states are being passed in more than one state argument position:" ;
	typedef typeof *passedStates_ SetType;
	for( SetType::const_iterator i = passedStates_->begin( ); i != passedStates_->end( ); ++i )
		{
			os << " " << *i ;
		}
	os << std::endl ;
}


Exceptions::PassingStateOut::PassingStateOut( const Ast::SourceLocation & loc, const char * id )
	: Exceptions::StaticInconsistency( loc ), id_( id )
{ }

Exceptions::PassingStateOut::~PassingStateOut( )
{ }

void
Exceptions::PassingStateOut::display( std::ostream & os ) const
{
	os << "States may not be returned, cannot return state " << id_ << "." << std::endl ;
}


Exceptions::IntroducingExisting::IntroducingExisting( const Ast::SourceLocation & loc, const Ast::PlacedIdentifier & id )
	: Exceptions::StaticInconsistency( loc ), id_( id )
{ }

Exceptions::IntroducingExisting::~IntroducingExisting( )
{ }

void
Exceptions::IntroducingExisting::display( std::ostream & os ) const
{
	os << "The variable " ;
	id_.show( os, Ast::Identifier::VARIABLE );
	os << " is already introduced in this scope." << std::endl ;
}


Exceptions::NamespaceAliasCollision::NamespaceAliasCollision( const Ast::SourceLocation & loc, const Ast::IdentifierTree * tree, const char * name )
	: Exceptions::StaticInconsistency( loc ), tree_( tree ), name_( name )
{ }

Exceptions::NamespaceAliasCollision::~NamespaceAliasCollision( )
{ }

void
Exceptions::NamespaceAliasCollision::display( std::ostream & os ) const
{
	os << "The introduced namespace alias name " ;
	tree_->showPath( os );
	os << Interaction::NAMESPACE_SEPARATOR << name_ << " is already in use." << std::endl ;
}


Exceptions::EncapsulationIntroducingExisting::EncapsulationIntroducingExisting( const Ast::SourceLocation & loc, const char * id )
	: Exceptions::StaticInconsistency( loc ), id_( id )
{ }

Exceptions::EncapsulationIntroducingExisting::~EncapsulationIntroducingExisting( )
{ }

void
Exceptions::EncapsulationIntroducingExisting::display( std::ostream & os ) const
{
	os << "The identifier " << id_ << " cannot be introduced in the encapsulation namespace since the name is occupied in the surrounding namespace." << std::endl ;
}


Exceptions::EncapsulationNamespaceAliasCollision::EncapsulationNamespaceAliasCollision( const Ast::SourceLocation & loc, const char * id )
	: Exceptions::StaticInconsistency( loc ), id_( id )
{ }

Exceptions::EncapsulationNamespaceAliasCollision::~EncapsulationNamespaceAliasCollision( )
{ }

void
Exceptions::EncapsulationNamespaceAliasCollision::display( std::ostream & os ) const
{
	os << "The namespace " << id_ << " cannot be introduced in the encapsulation namespace since the name is occupied in the surrounding namespace." << std::endl ;
}


Exceptions::ExpectedImmediate::ExpectedImmediate( const Ast::SourceLocation & loc )
	: Exceptions::StaticInconsistency( loc )
{ }

Exceptions::ExpectedImmediate::~ExpectedImmediate( )
{ }

void
Exceptions::ExpectedImmediate::display( std::ostream & os ) const
{
	os << "It makes no sense to put a pure expression before the end of a code bracket." << std::endl ;
}


Exceptions::IllegalImperative::IllegalImperative( const Ast::SourceLocation & loc )
	: Exceptions::StaticInconsistency( loc )
{ }

Exceptions::IllegalImperative::~IllegalImperative( )
{ }

void
Exceptions::IllegalImperative::display( std::ostream & os ) const
{
	os << "Imperative expressions are not allowed here." << std::endl ;
}


Exceptions::FreezingUndefined::FreezingUndefined( const Ast::SourceLocation & loc, const Ast::PlacedIdentifier & id )
	: Exceptions::StaticInconsistency( loc ), id_( id )
{ }

Exceptions::FreezingUndefined::~FreezingUndefined( )
{ }

void
Exceptions::FreezingUndefined::display( std::ostream & os ) const
{
	os << "Internal error:  The state " ;
	id_.show( os, Ast::Identifier::STATE );
	os << " is freezed before it was defined.  Either this should be caught by during the parse, or it is due to an error in the setup of the initial environment." << std::endl ;
}


Exceptions::UndefinedNamespace::UndefinedNamespace( const Ast::SourceLocation & loc, const Ast::NamespaceReference & id )
	: Exceptions::StaticInconsistency( loc ), id_( id )
{ }

Exceptions::UndefinedNamespace::~UndefinedNamespace( )
{ }

void
Exceptions::UndefinedNamespace::display( std::ostream & os ) const
{
	os << "The namespace reference " ;
	id_.show( os );
	os << " is not referring to an existing namespace." << std::endl ;
}


Exceptions::FileReadOpenError::FileReadOpenError( const Ast::SourceLocation & loc, RefCountPtr< const char > filename, const std::string * sourceDir, const std::list< std::string > * searchPath, Type type )
	: loc_( loc ), filename_( filename ), type_( type ), sourceDir_( sourceDir ), searchPath_( searchPath )
{ }

Exceptions::FileReadOpenError::~FileReadOpenError( )
{ }

void
Exceptions::FileReadOpenError::display( std::ostream & os ) const
{
	switch( type_ )
		{
		case OPEN:
			os << loc_ << locsep << "Could not open file: " << filename_ ;
			break;
		case STAT:
			os << loc_ << locsep << "Could not stat() file: " << filename_ ;
			break;
		}
	if( searchPath_ != 0 )
		{
			os << " searching \"" ;
			typedef typeof *searchPath_ ListType;
			for( ListType::const_iterator i = searchPath_->begin( ); i != searchPath_->end( ); ++i )
				{
					if( i != searchPath_->begin( ) )
						{
							os << ":" ;
						}
					if( sourceDir_ != 0 && (*i)[0] != '/' )
						{
							os << *sourceDir_ ;
						}
					os << *i ;
				}
			os << "\"" ;
		}
	os << std::endl ;
}


Exceptions::FileWriteOpenError::FileWriteOpenError( const Ast::SourceLocation & loc, RefCountPtr< const char > filename, const char * purpose )
	: loc_( loc ), filename_( filename ), purpose_( purpose )
{ }

Exceptions::FileWriteOpenError::~FileWriteOpenError( )
{ }

void
Exceptions::FileWriteOpenError::display( std::ostream & os ) const
{
	os << loc_ << locsep << "Could not open " ;
	if( purpose_ != 0 )
		{
			os << purpose_ ;
		}
	os << " file for write: " << filename_ ;
	os << std::endl ;
}


Exceptions::TeXSetupTooLate::TeXSetupTooLate( const Ast::SourceLocation & loc )
	: loc_( loc )
{ }

Exceptions::TeXSetupTooLate::~TeXSetupTooLate( )
{ }

void
Exceptions::TeXSetupTooLate::display( std::ostream & os ) const
{
	os << loc_ << locsep << "It is too late to make modifications to the TeX context." << std::endl ;
}


Exceptions::EmptyFinalPicture::EmptyFinalPicture( )
{ }

Exceptions::EmptyFinalPicture::~EmptyFinalPicture( )
{ }

void
Exceptions::EmptyFinalPicture::display( std::ostream & os ) const
{
	os << "Nothing was ever put in the global " ;
	Lang::CATALOG_ID.show( os, Ast::Identifier::STATE );
	os << " or drawn to the global " ;
	Lang::CANVAS_ID.show( os, Ast::Identifier::STATE );
	os << "." << std::endl ;
}


Exceptions::TeXSetupHasChanged::TeXSetupHasChanged( )
	: Shapes::Exceptions::InternalError( "The TeX context check failed while loading fresh labels." )
{ }

Exceptions::TeXSetupHasChanged::~TeXSetupHasChanged( )
{ }


Exceptions::TypeMismatch::TypeMismatch( const Ast::SourceLocation & loc, RefCountPtr< const char > valueType, RefCountPtr< const char > expectedType )
	: Exceptions::RuntimeError( loc ), hint_( 0 ), valueType_( valueType ), expectedType_( expectedType )
{ }

Exceptions::TypeMismatch::TypeMismatch( const Ast::SourceLocation & loc, const char * hint, RefCountPtr< const char > valueType, RefCountPtr< const char > expectedType )
	: Exceptions::RuntimeError( loc ), hint_( hint ), valueType_( valueType ), expectedType_( expectedType )
{ }

Exceptions::TypeMismatch::~TypeMismatch( )
{ }

void
Exceptions::TypeMismatch::display( std::ostream & os ) const
{
	if( hint_ != 0 )
		{
			os << hint_ << ": " ;
		}
	os << "Expected a " << expectedType_ << ", got a " << valueType_ << "." << std::endl ;
}

Exceptions::ExpectedList::ExpectedList( const Ast::SourceLocation & loc, size_t index, RefCountPtr< const char > nonListType )
	: Exceptions::RuntimeError( loc ), index_( index ), nonListType_( nonListType )
{ }

Exceptions::ExpectedList::~ExpectedList( )
{ }

void
Exceptions::ExpectedList::display( std::ostream & os ) const
{
	os << "Expected a singly linked list, but encountered a " << nonListType_ << " after " << index_ << " elements." << std::endl ;
}

Exceptions::PDFVersionError::PDFVersionError( SimplePDF::PDF_Version::Version version, SimplePDF::PDF_Version::Version required, const Interaction::MessageString & msg, bool justAWarning )
	: Exceptions::CatchableError( Ast::THE_UNKNOWN_LOCATION ), version_( version ), required_( required ), msg_( msg ), justAWarning_( justAWarning )
{ }

Exceptions::PDFVersionError::~PDFVersionError( )
{ }

void
Exceptions::PDFVersionError::display( std::ostream & os ) const
{
	os << SimplePDF::PDF_Version::toString( required_ ) << " " << ( justAWarning_ ? "warning" : "error" ) << " for target version " << SimplePDF::PDF_Version::toString( version_ ) << ": " << msg_ << std::endl ;
}

RefCountPtr< const Lang::Exception >
Exceptions::PDFVersionError::clone( const Kernel::ContRef & cont ) const
{
	static RefCountPtr< const Lang::Symbol > kind( new Lang::Symbol( "PDF_version" ) );
	std::ostringstream os;
	display( os );
	return RefCountPtr< const Lang::Exception >( new Lang::Exception( this->getLoc( ),
																																		kind,
																																		RefCountPtr< const Lang::String >( new Lang::String( "(core PDF backend)", true ) ),
																																		Lang::THE_VOID,
																																		strrefdup( os ),
																																		cont,
																																		this->exitCode( ) ) );
}


Exceptions::RedefiningLexical::RedefiningLexical( const Ast::PlacedIdentifier & id )
	: Exceptions::RuntimeError( Ast::THE_UNKNOWN_LOCATION ), id_( id )
{ }

Exceptions::RedefiningLexical::~RedefiningLexical( )
{ }

void
Exceptions::RedefiningLexical::display( std::ostream & os ) const
{
	os << "Redefining " ;
	id_.show( os, Ast::Identifier::VARIABLE );
	os << std::endl ;
}


Exceptions::RedefiningDynamic::RedefiningDynamic( const Ast::PlacedIdentifier & id )
	: Exceptions::RuntimeError( Ast::THE_UNKNOWN_LOCATION ), id_( id )
{ }

Exceptions::RedefiningDynamic::~RedefiningDynamic( )
{ }

void
Exceptions::RedefiningDynamic::display( std::ostream & os ) const
{
	os << "Redefining " ;
	id_.show( os, Ast::Identifier::DYNAMIC_VARIABLE );
	os << std::endl ;
}


Exceptions::NonObjectMemberAssignment::NonObjectMemberAssignment( const Ast::SourceLocation & loc, RefCountPtr< const char > valueType )
	: Exceptions::RuntimeError( loc ), valueType_( valueType )
{ }

Exceptions::NonObjectMemberAssignment::~NonObjectMemberAssignment( )
{ }

void
Exceptions::NonObjectMemberAssignment::display( std::ostream & os ) const
{
	os << "Only object oriented values have fields that can be assigned.  This value is of type " << valueType_ << "." << std::endl ;
}


Exceptions::ElementaryWithout::ElementaryWithout( Kind kind, Ref ref, RefCountPtr< const char > valueType )
	: Exceptions::RuntimeError( Ast::THE_UNKNOWN_LOCATION ), kind_( kind ), ref_( ref ), valueType_( valueType )
{ }

Exceptions::ElementaryWithout::~ElementaryWithout( )
{ }

void
Exceptions::ElementaryWithout::display( std::ostream & os ) const
{
	os << "This " ;
	switch( kind_ )
		{
		case VALUE:
			os << "value" ;
			break;
		case STATE:
			os << "state" ;
			break;
		}
	os << " is of the elementary type " << valueType_ << ", which has no " ;
	switch( ref_ )
		{
		case FIELD:
			os << "fields" ;
			break;
		case MUTATOR:
			os << "mutators" ;
			break;
		}
		os << "." << std::endl ;
}


Exceptions::NonExistentPosition::NonExistentPosition( size_t pos, size_t maxPos )
	: Exceptions::RuntimeError( Ast::THE_UNKNOWN_LOCATION ), pos_( pos ), maxPos_( maxPos )
{ }

Exceptions::NonExistentPosition::~NonExistentPosition( )
{ }

void
Exceptions::NonExistentPosition::display( std::ostream & os ) const
{
	os << "Referencing field at position " << pos_ << " is an error since the user struct has only " << maxPos_ << " ordered fields." << std::endl ;
}


Exceptions::UnaryPrefixNotApplicable::UnaryPrefixNotApplicable( const Ast::SourceLocation & loc, const Ast::Expression * expr, RefCountPtr< const char > valueType )
	: Exceptions::RuntimeError( loc ), expr_( expr ), valueType_( valueType ), coreLoc_( new Interaction::CharPtrLocation( "< ? >" ) )
{ }

Exceptions::UnaryPrefixNotApplicable::~UnaryPrefixNotApplicable( )
{ }

void
Exceptions::UnaryPrefixNotApplicable::setCoreLocation( const RefCountPtr< const Interaction::CoreLocation > & coreLoc )
{
	coreLoc_ = coreLoc;
}

void
Exceptions::UnaryPrefixNotApplicable::display( std::ostream & os ) const
{
	if( expr_ != 0 )
		{
			os << "Operator " ;
			getLoc( ).copy( & os );
			os << " not defined for " << valueType_ << " at " << expr_->loc( ) << "." << std::endl ;
		}
	else
		{
			os << "Operator " << coreLoc_ << " not defined for " << valueType_ << "." << std::endl ;
		}
}


Exceptions::UnaryPostfixNotApplicable::UnaryPostfixNotApplicable( const Ast::SourceLocation & loc, const Ast::Expression * expr, RefCountPtr< const char > valueType )
	: Exceptions::RuntimeError( loc ), expr_( expr ), valueType_( valueType ), coreLoc_( new Interaction::CharPtrLocation( "< ? >" ) )
{ }

Exceptions::UnaryPostfixNotApplicable::~UnaryPostfixNotApplicable( )
{ }

void
Exceptions::UnaryPostfixNotApplicable::setCoreLocation( const RefCountPtr< const Interaction::CoreLocation > & coreLoc )
{
	coreLoc_ = coreLoc;
}

void
Exceptions::UnaryPostfixNotApplicable::display( std::ostream & os ) const
{
	if( expr_ != 0 )
		{
			os << "Operator " ;
			getLoc( ).copy( & os );
			os << " not defined for " << valueType_ << " at " << expr_->loc( ) << "." << std::endl ;
		}
	else
		{
			os << "Operator " << coreLoc_ << " not defined for " << valueType_ << "." << std::endl ;
		}
}


Exceptions::BinaryInfixNotApplicable::BinaryInfixNotApplicable( const Ast::SourceLocation & loc, const Ast::Expression * expr1, RefCountPtr< const char > valueType1, const Ast::Expression * expr2, RefCountPtr< const char > valueType2 )
	: Exceptions::RuntimeError( loc ), expr1_( expr1 ), valueType1_( valueType1 ), expr2_( expr2 ), valueType2_( valueType2 ), coreLoc_( new Interaction::CharPtrLocation( "< ? >" ) )
{ }

Exceptions::BinaryInfixNotApplicable::~BinaryInfixNotApplicable( )
{ }

void
Exceptions::BinaryInfixNotApplicable::setCoreLocation( const RefCountPtr< const Interaction::CoreLocation > & coreLoc )
{
	coreLoc_ = coreLoc;
}

void
Exceptions::BinaryInfixNotApplicable::display( std::ostream & os ) const
{
	if( expr1_ != 0 && expr2_ != 0 )
		{
			os << "Operator " ;
			getLoc( ).copy( & os );
			os << " not defined for (" << valueType1_ << "," << valueType2_ << ") at " << expr1_->loc( ) << ", " << expr2_->loc( ) << "." << std::endl ;
		}
	else
		{
			os << "Operator " << coreLoc_ << " not defined for (" << valueType1_ << "," << valueType2_ << ")." << std::endl ;
		}
}


Exceptions::LookupUnknown::LookupUnknown( const Ast::SourceLocation & loc, const Ast::Identifier & id, Ast::Identifier::Type type )
	: Exceptions::StaticInconsistency( loc ), id_( id ), type_( type )
{ }

Exceptions::LookupUnknown::~LookupUnknown( )
{ }

void
Exceptions::LookupUnknown::display( std::ostream & os ) const
{
	switch( type_ )
		{
		case Ast::Identifier::VARIABLE:
			os << "The variable " ;
			id_.show( os, type_ );
			os << " is unbound." << std::endl ;
			break;
		case Ast::Identifier::STATE:
			os << "There is no state called " ;
			id_.show( os, type_ );
			os << " around." << std::endl ;
			break;
		case Ast::Identifier::DYNAMIC_VARIABLE:
			os << "There is no dynamic variable called " ;
			id_.show( os, type_ );
			os << " around." << std::endl ;
			break;
		case Ast::Identifier::DYNAMIC_STATE:
			os << "There is no dynamic state called " ;
			id_.show( os, type_ );
			os << " around." << std::endl ;
			break;
		case Ast::Identifier::TYPE:
			os << "There is no type called " ;
			id_.show( os, type_ );
			os << " around." << std::endl ;
			break;
		}
}


Exceptions::LookupUnknownPrivateAlias::LookupUnknownPrivateAlias( const Ast::SourceLocation & loc, const Ast::PlacedIdentifier & id, Ast::Identifier::Type type )
	: Exceptions::StaticInconsistency( loc ), id_( id ), type_( type )
{ }

Exceptions::LookupUnknownPrivateAlias::~LookupUnknownPrivateAlias( )
{ }

void
Exceptions::LookupUnknownPrivateAlias::display( std::ostream & os ) const
{
	os << "There is no alias of " ;
	id_.show( os, type_ );
	os << " defined in the private namespace." << std::endl ;
}


Exceptions::StateBeyondFunctionBoundary::StateBeyondFunctionBoundary( const Ast::SourceLocation & loc, const Ast::Identifier & id )
	: Exceptions::StaticInconsistency( loc ), id_( id )
{ }

Exceptions::StateBeyondFunctionBoundary::~StateBeyondFunctionBoundary( )
{ }

void
Exceptions::StateBeyondFunctionBoundary::display( std::ostream & os ) const
{
	os << "The state " ;
	id_.show( os, Ast::Identifier::STATE );
	os << " resides outside a function boundary." << std::endl ;
}


Exceptions::IntroducingExistingUnit::IntroducingExistingUnit( const Ast::SourceLocation & loc, RefCountPtr< const char > id )
	: Exceptions::StaticInconsistency( loc ), id_( id )
{ }

Exceptions::IntroducingExistingUnit::~IntroducingExistingUnit( )
{ }

void
Exceptions::IntroducingExistingUnit::display( std::ostream & os ) const
{
	os << "Inconsistent definition of the existing unit " << id_ << "." << std::endl ;
}


Exceptions::LookupUnknownUnit::LookupUnknownUnit( const Ast::SourceLocation & loc, RefCountPtr< const char > id )
	: Exceptions::StaticInconsistency( loc ), id_( id )
{ }

Exceptions::LookupUnknownUnit::~LookupUnknownUnit( )
{ }

void
Exceptions::LookupUnknownUnit::display( std::ostream & os ) const
{
	os << "The unit " << id_ << " is unbound" << std::endl ;
}


Exceptions::OutOfRange::OutOfRange( const Interaction::MessageString & msg )
	: Exceptions::CatchableError( Ast::THE_UNKNOWN_LOCATION ), msg_( msg )
{ }

Exceptions::OutOfRange::OutOfRange( const Ast::SourceLocation & loc, const Interaction::MessageString & msg )
	: Exceptions::CatchableError( loc ), msg_( msg )
{ }

Exceptions::OutOfRange::~OutOfRange( )
{ }

void
Exceptions::OutOfRange::display( std::ostream & os ) const
{
	os << "Out-of-range error: " << msg_ << std::endl ;
}

RefCountPtr< const Lang::Exception >
Exceptions::OutOfRange::clone( const Kernel::ContRef & cont ) const
{
	static RefCountPtr< const Lang::Symbol > kind( new Lang::Symbol( "out_of_range" ) );
	std::ostringstream os;
	display( os );
	return RefCountPtr< const Lang::Exception >( new Lang::Exception( this->getLoc( ),
																																		kind,
																																		RefCountPtr< const Lang::String >( new Lang::String( "(core, generic)", true ) ),
																																		Lang::THE_VOID,
																																		strrefdup( os ),
																																		cont,
																																		this->exitCode( ) ) );
}


Exceptions::FontProblem::FontProblem( const RefCountPtr< const Lang::Symbol > & PostScript_name, const Interaction::MessageString & msg, bool justAWarning )
	: Exceptions::CatchableError( Ast::THE_UNKNOWN_LOCATION ), PostScript_name_( PostScript_name ), msg_( msg ), justAWarning_( justAWarning )
{ }

Exceptions::FontProblem::FontProblem( const RefCountPtr< const Lang::Symbol > & PostScript_name, const Ast::SourceLocation & loc, const Interaction::MessageString & msg, bool justAWarning )
	: Exceptions::CatchableError( loc ), PostScript_name_( PostScript_name ), msg_( msg ), justAWarning_( justAWarning )
{ }

Exceptions::FontProblem::~FontProblem( )
{ }

void
Exceptions::FontProblem::display( std::ostream & os ) const
{
	os << "Font " << ( justAWarning_ ? "warning" : "error" ) << " using " << PostScript_name_->name( ) << ": " << msg_ << std::endl ;
}

RefCountPtr< const Lang::Exception >
Exceptions::FontProblem::clone( const Kernel::ContRef & cont ) const
{
	static RefCountPtr< const Lang::Symbol > kind( new Lang::Symbol( "misc" ) );
	std::ostringstream os;
	display( os );
	return RefCountPtr< const Lang::Exception >( new Lang::Exception( this->getLoc( ),
																																		kind,
																																		RefCountPtr< const Lang::String >( new Lang::String( "(font problem)", true ) ),
																																		Lang::THE_VOID,
																																		strrefdup( os ),
																																		cont,
																																		this->exitCode( ) ) );
}


Exceptions::NonVoidStatement::NonVoidStatement( const Ast::SourceLocation & loc, RefCountPtr< const Lang::Value > val )
	: Exceptions::RuntimeError( loc ), val_( val )
{ }

Exceptions::NonVoidStatement::~NonVoidStatement( )
{ }

void
Exceptions::NonVoidStatement::display( std::ostream & os ) const
{
	os << "No implicit ignore of non-void value (of type " << val_->getTypeName( ) << ")" << std::endl ;
}

// Exceptions::RuntimeError::RuntimeError( )
//	 : loc_( "< unlocated runtime error >" )
// { }

Exceptions::RuntimeError::~RuntimeError( )
{ }

Exceptions::RuntimeError::RuntimeError( const Ast::SourceLocation & loc )
	: loc_( loc )
{ }

Exceptions::RuntimeError::RuntimeError( Ast::Expression * expr )
	: loc_( expr->loc( ) )
{ }

const Ast::SourceLocation &
Exceptions::RuntimeError::getLoc( ) const
{
	return loc_;
}


Exceptions::CatchableError::CatchableError( const Ast::SourceLocation & loc )
	: Exceptions::RuntimeError( loc )
{ }

Exceptions::CatchableError::CatchableError( Ast::Expression * expr )
	: Exceptions::RuntimeError( expr )
{ }

Exceptions::CatchableError::~CatchableError( )
{ }


Exceptions::UncaughtError::UncaughtError( const RefCountPtr< const Lang::Exception > & ball )
	: Exceptions::RuntimeError( ball->loc( ) ), ball_( ball )
{ }

Exceptions::UncaughtError::~UncaughtError( )
{ }

void
Exceptions::UncaughtError::display( std::ostream & os ) const
{
	os << "Uncaught error: " ;
	ball_->show( os );
	os << std::endl ;
}

Interaction::ExitCode
Exceptions::UncaughtError::exitCode( ) const
{
	return ball_->exitCode( );
}


Exceptions::UserError::UserError( const RefCountPtr< const Lang::Symbol > & kind, const RefCountPtr< const Lang::String > & source, const RefCountPtr< const Lang::Value > & details, const Interaction::MessageString & message )
	: Exceptions::CatchableError( Ast::THE_UNKNOWN_LOCATION ), kind_( kind ), source_( source ), details_( details ), message_( message )
{ }

Exceptions::UserError::~UserError( )
{ }

void
Exceptions::UserError::display( ostream & os ) const
{
	os << "'" ;
	kind_->show( os );
	os << " error in " << source_->val_ << ": " << message_ << std::endl ;
}

RefCountPtr< const Lang::Exception >
Exceptions::UserError::clone( const Kernel::ContRef & cont ) const
{
	std::ostringstream os;
	display( os );
	return RefCountPtr< const Lang::Exception >( new Lang::Exception( this->getLoc( ),
																																		kind_,
																																		source_,
																																		details_,
																																		strrefdup( os ),
																																		cont,
																																		this->exitCode( ) ) );
}


Exceptions::UserOutOfRange::UserOutOfRange( const RefCountPtr< const Lang::Symbol > & kind, const RefCountPtr< const Lang::String > & source, const RefCountPtr< const Lang::Integer > & details, const Interaction::MessageString & message )
	: Exceptions::CatchableError( Ast::THE_UNKNOWN_LOCATION ), kind_( kind ), source_( source ), details_( details ), message_( message )
{ }

Exceptions::UserOutOfRange::~UserOutOfRange( )
{ }

void
Exceptions::UserOutOfRange::display( ostream & os ) const
{
	os << "In " << source_->val_ << ", argument " << details_->val_
		 << ", out-of-range: " << message_ << std::endl ;
}

RefCountPtr< const Lang::Exception >
Exceptions::UserOutOfRange::clone( const Kernel::ContRef & cont ) const
{
	std::ostringstream os;
	display( os );
	return RefCountPtr< const Lang::Exception >( new Lang::Exception( this->getLoc( ),
																																		kind_,
																																		source_,
																																		details_,
																																		strrefdup( os ),
																																		cont,
																																		this->exitCode( ) ) );
}


Exceptions::UserTypeMismatch::UserTypeMismatch( const RefCountPtr< const Lang::Symbol > & kind, const RefCountPtr< const Lang::String > & source, const RefCountPtr< const Lang::Integer > & details, const RefCountPtr< const char > & expectedType, const RefCountPtr< const char > & valueType )
	: Exceptions::CatchableError( Ast::THE_UNKNOWN_LOCATION ), kind_( kind ), source_( source ), details_( details ), expectedType_( expectedType ), valueType_( valueType )
{ }

Exceptions::UserTypeMismatch::~UserTypeMismatch( )
{ }

void
Exceptions::UserTypeMismatch::display( ostream & os ) const
{
	os << "In " << source_->val_ << ", argument " << details_->val_
		 << ": expected a " << expectedType_ << ", got a " << valueType_ << "." << std::endl ;
}

RefCountPtr< const Lang::Exception >
Exceptions::UserTypeMismatch::clone( const Kernel::ContRef & cont ) const
{
	std::ostringstream os;
	display( os );
	return RefCountPtr< const Lang::Exception >( new Lang::Exception( this->getLoc( ),
																																		kind_,
																																		source_,
																																		details_,
																																		strrefdup( os ),
																																		cont,
																																		this->exitCode( ) ) );
}


Exceptions::InternalError::InternalError( const Ast::SourceLocation & loc, const Interaction::MessageString & msg, bool justAWarning )
	: Exceptions::RuntimeError( loc ), msg_( msg ), justAWarning_( justAWarning )
{ }

Exceptions::InternalError::InternalError( const Interaction::MessageString & msg, bool justAWarning )
	: Exceptions::RuntimeError( Ast::THE_UNKNOWN_LOCATION ), msg_( msg ), justAWarning_( justAWarning )
{ }

Exceptions::InternalError::InternalError( const std::ostringstream & msg, bool justAWarning )
	: Exceptions::RuntimeError( Ast::THE_UNKNOWN_LOCATION ), msg_( strdup( msg.str( ).c_str( ) ) ), justAWarning_( justAWarning )
{ }

Exceptions::InternalError::~InternalError( )
{ }

void
Exceptions::InternalError::display( ostream & os ) const
{
	os << "Internal " << ( justAWarning_ ? "warning" : "error" ) << ": " << msg_ << std::endl ;
}


Exceptions::ExternalError::ExternalError( const Interaction::MessageString & msg, bool justAWarning )
	: Exceptions::CatchableError( Ast::THE_UNKNOWN_LOCATION ), msg_( msg ), justAWarning_( justAWarning )
{ }

Exceptions::ExternalError::~ExternalError( )
{ }

void
Exceptions::ExternalError::display( ostream & os ) const
{
	os << "External " << ( justAWarning_ ? "warning" : "error" ) << ": " << msg_ << std::endl ;
}

RefCountPtr< const Lang::Exception >
Exceptions::ExternalError::clone( const Kernel::ContRef & cont ) const
{
	static RefCountPtr< const Lang::Symbol > kind( new Lang::Symbol( "external" ) );
	std::ostringstream os;
	display( os );
	return RefCountPtr< const Lang::Exception >( new Lang::Exception( this->getLoc( ),
																																		kind,
																																		RefCountPtr< const Lang::String >( new Lang::String( "(external error)", true ) ),
																																		Lang::THE_VOID,
																																		strrefdup( os ),
																																		cont,
																																		this->exitCode( ) ) );
}

Exceptions::ExternalError *
Exceptions::ExternalError::cloneTyped( ) const
{
	return new Exceptions::ExternalError( msg_ );
}


Exceptions::InternalErrorIn::InternalErrorIn( const Ast::PlacedIdentifier & id, const Interaction::MessageString & msg, bool justAWarning )
  : Exceptions::InternalError( Ast::THE_UNKNOWN_LOCATION, msg, justAWarning ), id_( id )
{ }

Exceptions::InternalErrorIn::~InternalErrorIn( )
{ }

void
Exceptions::InternalErrorIn::display( ostream & os ) const
{
  os << "Internal " << ( justAWarning_ ? "warning" : "error" ) << " in " ;
  id_.show( os, Ast::Identifier::VARIABLE );
  os << ": " << msg_ << std::endl ;
}


Exceptions::UserArityMismatch::UserArityMismatch( const Ast::SourceLocation & callLoc, const Ast::SourceLocation & formalsLoc, size_t functionArity, size_t callArity, Type type )
	: Exceptions::RuntimeError( callLoc ), formalsLoc_( formalsLoc ), functionArity_( functionArity ), callArity_( callArity ), type_( type )
{ }

Exceptions::UserArityMismatch::~UserArityMismatch( )
{ }

void
Exceptions::UserArityMismatch::display( std::ostream & os ) const
{
	os << "Function with formals at " << formalsLoc_ << " expects " << functionArity_ ;
	switch( type_ )
		{
		case VARIABLE:
			os << " value arguments" ;
			break;
		case STATE:
			os << " state arguments" ;
			break;
		}
	os << ", not " << callArity_ << std::endl ;
}

Exceptions::SinkRequired::SinkRequired( const Ast::SourceLocation & formalsLoc, size_t formalsArity, size_t callArity )
	: Exceptions::RuntimeError( Ast::THE_UNKNOWN_LOCATION ), formalsLoc_( formalsLoc ), formalsArity_( formalsArity ), callArity_( callArity )
{ }

Exceptions::SinkRequired::~SinkRequired( )
{ }

void
Exceptions::SinkRequired::display( std::ostream & os ) const
{
	os << "Formals at " << formalsLoc_ << " contains no sink, so passing " << callArity_ << " ordered arguments is " << callArity_ - formalsArity_ << " too many." << std::endl ;
}


Exceptions::NamedFormalMismatch::NamedFormalMismatch( const Ast::SourceLocation & formalsLoc, RefCountPtr< const char > name, Exceptions::NamedFormalMismatch::Type type )
	: Exceptions::RuntimeError( Ast::THE_UNKNOWN_LOCATION ), formalsLoc_( formalsLoc ), name_( name ), type_( type )
{ }

Exceptions::NamedFormalMismatch::~NamedFormalMismatch( )
{ }

void
Exceptions::NamedFormalMismatch::display( std::ostream & os ) const
{
	os << "Function with formals at " << formalsLoc_ << " has no named ";
	switch( type_ )
		{
		case VARIABLE:
			os << "variable" ;
			break;
		case STATE:
			os << "state" ;
			break;
		}
	os << " called " << name_ << "." << std::endl ;
}


Exceptions::NamedFormalAlreadySpecified::NamedFormalAlreadySpecified( const Ast::SourceLocation & formalsLoc, RefCountPtr< const char > name, size_t pos, Exceptions::NamedFormalAlreadySpecified::Type type )
	: Exceptions::RuntimeError( Ast::THE_UNKNOWN_LOCATION ), formalsLoc_( formalsLoc ), name_( name ), pos_( pos ), type_( type )
{ }

Exceptions::NamedFormalAlreadySpecified::~NamedFormalAlreadySpecified( )
{ }

void
Exceptions::NamedFormalAlreadySpecified::display( std::ostream & os ) const
{
	os << "The formal " ;
	switch( type_ )
		{
		case VARIABLE:
			os << "variable" ;
			break;
		case STATE:
			os << "state" ;
			break;
		}
	os << " named " << name_ << ", defined at " << formalsLoc_ << " is already defined by order, at position " << pos_ << "." << std::endl ;
}

Exceptions::MissingArguments::MissingArguments( const Ast::SourceLocation & callLoc, const Ast::SourceLocation & formalsLoc, std::map< size_t, RefCountPtr< const char > > * missingVariables, std::map< size_t, RefCountPtr< const char > > * missingStates )
	: Exceptions::RuntimeError( callLoc ), formalsLoc_( formalsLoc ), missingVariables_( missingVariables ), missingStates_( missingStates )
{ }

Exceptions::MissingArguments::~MissingArguments( )
{ }

void
Exceptions::MissingArguments::display( std::ostream & os ) const
{
	os << "Among the formals at " << formalsLoc_ ;
	if( missingVariables_ != NullPtr< typeof *missingVariables_ >( ) )
		{
			os << ", the following variables are missing:" ;
			typedef typeof *missingVariables_ MapType;
			for( MapType::const_iterator i = missingVariables_->begin( ); i != missingVariables_->end( ); ++i )
				{
					if( i != missingVariables_->begin( ) )
						{
							os << "," ;
						}
					os << " (" << i->first << ")" << i->second.getPtr( ) ;
				}
		}
	if( missingStates_ != NullPtr< typeof *missingStates_ >( ) )
		{
			os << ", " ;
			if( missingVariables_ != NullPtr< typeof *missingVariables_ >( ) )
				{
					os << "and " ;
				}
			os << "the following states are missing:" ;
			typedef typeof *missingStates_ MapType;
			for( MapType::const_iterator i = missingStates_->begin( ); i != missingStates_->end( ); ++i )
				{
					if( i != missingStates_->begin( ) )
						{
							os << "," ;
						}
					os << " (" << i->first << ")" << i->second.getPtr( ) ;
				}
		}
	os << "." << std::endl ;
}


Exceptions::CoreArityMismatch::CoreArityMismatch( const RefCountPtr< const Interaction::CoreLocation > & coreLoc, size_t functionArity, size_t callArity )
  : Exceptions::RuntimeError( Ast::THE_UNKNOWN_LOCATION ), coreLoc_( coreLoc ), functionArityLow_( functionArity ), functionArityHigh_( functionArity ), callArity_( callArity )
{ }

Exceptions::CoreArityMismatch::CoreArityMismatch( Interaction::CoreLocation * coreLoc, size_t functionArity, size_t callArity )
  : Exceptions::RuntimeError( Ast::THE_UNKNOWN_LOCATION ), coreLoc_( coreLoc ), functionArityLow_( functionArity ), functionArityHigh_( functionArity ), callArity_( callArity )
{ }

Exceptions::CoreArityMismatch::CoreArityMismatch( const RefCountPtr< const Interaction::CoreLocation > & coreLoc, size_t functionArityLow, size_t functionArityHigh, size_t callArity )
  : Exceptions::RuntimeError( Ast::THE_UNKNOWN_LOCATION ), coreLoc_( coreLoc ), functionArityLow_( functionArityLow ), functionArityHigh_( functionArityHigh ), callArity_( callArity )
{ }

Exceptions::CoreArityMismatch::CoreArityMismatch( const Ast::PlacedIdentifier & coreId, size_t functionArity, size_t callArity )
  : Exceptions::RuntimeError( Ast::THE_UNKNOWN_LOCATION ), coreLoc_( new Interaction::BoundLocation( coreId ) ), functionArityLow_( functionArity ), functionArityHigh_( functionArity ), callArity_( callArity )
{ }

Exceptions::CoreArityMismatch::CoreArityMismatch( const Ast::PlacedIdentifier & coreId, size_t functionArityLow, size_t functionArityHigh, size_t callArity )
  : Exceptions::RuntimeError( Ast::THE_UNKNOWN_LOCATION ), coreLoc_( new Interaction::BoundLocation( coreId ) ), functionArityLow_( functionArityLow ), functionArityHigh_( functionArityHigh ), callArity_( callArity )
{ }

Exceptions::CoreArityMismatch::CoreArityMismatch( const char * coreSyntax, size_t functionArity, size_t callArity )
  : Exceptions::RuntimeError( Ast::THE_UNKNOWN_LOCATION ), coreLoc_( new Interaction::CharPtrLocation( coreSyntax ) ), functionArityLow_( functionArity ), functionArityHigh_( functionArity ), callArity_( callArity )
{ }

Exceptions::CoreArityMismatch::~CoreArityMismatch( )
{ }

void
Exceptions::CoreArityMismatch::display( std::ostream & os ) const
{
  if( functionArityLow_ == functionArityHigh_ ){
    os << "Core function " << coreLoc_ << " expects " << functionArityLow_ << " arguments, not " << callArity_ << "." << std::endl ;
  }else{
    os << "Core function " << coreLoc_ << " expects [" << functionArityLow_ << ", " << functionArityHigh_ << "] arguments, not " << callArity_ << "." << std::endl ;
  }
}


Exceptions::CoreNoNamedFormals::CoreNoNamedFormals( const char * what )
  : Exceptions::RuntimeError( Ast::THE_UNKNOWN_LOCATION ), what_( what )
{ }

Exceptions::CoreNoNamedFormals::~CoreNoNamedFormals( )
{ }

void
Exceptions::CoreNoNamedFormals::display( std::ostream & os ) const
{
  os << "The core operation " << what_ << " does not accept named arguments." << std::endl ;
}


Exceptions::CoreTypeMismatch::CoreTypeMismatch( const Ast::SourceLocation & callLoc,
																						 Interaction::CoreLocation * coreLoc,
																						 const Ast::SourceLocation & argLoc,
																						 RefCountPtr< const char > valueType,
																						 RefCountPtr< const char > expectedType )
	: Exceptions::RuntimeError( callLoc ),
	  coreLoc_( coreLoc ),
		argLoc_( argLoc ),
		valueType_( valueType ),
		expectedType_( expectedType )
{ }

Exceptions::CoreTypeMismatch::CoreTypeMismatch( const Ast::SourceLocation & callLoc,
																								const RefCountPtr< const Interaction::CoreLocation > & coreLoc,
																								Kernel::Arguments & args,
																								size_t argNo,
																								RefCountPtr< const char > expectedType )
	: Exceptions::RuntimeError( callLoc ),
		coreLoc_( coreLoc ),
		argLoc_( args.getLoc( argNo ) ),
		valueType_( args.getValue( argNo )->getTypeName( ) ),
		expectedType_( expectedType )
{ }

Exceptions::CoreTypeMismatch::CoreTypeMismatch( const Ast::SourceLocation & callLoc,
																								Interaction::CoreLocation * coreLoc,
																								Kernel::Arguments & args,
																								size_t argNo,
																								RefCountPtr< const char > expectedType )
	: Exceptions::RuntimeError( callLoc ),
		coreLoc_( coreLoc ),
		argLoc_( args.getLoc( argNo ) ),
		valueType_( args.getValue( argNo )->getTypeName( ) ),
		expectedType_( expectedType )
{ }

Exceptions::CoreTypeMismatch::CoreTypeMismatch( const Ast::SourceLocation & callLoc,
																								const Ast::PlacedIdentifier & coreId,
																								Kernel::Arguments & args,
																								size_t argNo,
																								RefCountPtr< const char > expectedType )
	: Exceptions::RuntimeError( callLoc ),
		coreLoc_( new Interaction::BoundLocation( coreId ) ),
		argLoc_( args.getLoc( argNo ) ),
		valueType_( args.getValue( argNo )->getTypeName( ) ),
		expectedType_( expectedType )
{ }

Exceptions::CoreTypeMismatch::~CoreTypeMismatch( )
{ }

void
Exceptions::CoreTypeMismatch::display( std::ostream & os ) const
{
	if( dynamic_cast< const Interaction::BoundLocation * >( coreLoc_.getPtr( ) ) != NULL ){
		os << "Core function " ;
	}else{
		/* In this case, title_ shouldn't be a function name, but something else, like
		 * a piece of syntax or a the name of an internal continuation.
		 */
		os << "In " ;
	}
	os << *coreLoc_ << ", argument " ;
	//	if( argName_ != 0 )
	//		{
	//			os << "\"" << argName_ << "\" " ;
	//		}
	os << "at " << argLoc_ << ": expected a " << expectedType_ << ", got a " << valueType_ << "." << std::endl ;
}


Exceptions::CoreStateTypeMismatch::CoreStateTypeMismatch( const Ast::SourceLocation & callLoc,
																						 const Ast::PlacedIdentifier & coreId,
																						 const Ast::SourceLocation & argLoc,
																						 RefCountPtr< const char > valueType,
																						 RefCountPtr< const char > expectedType )
	: Exceptions::RuntimeError( callLoc ),
		coreLoc_( new Interaction::BoundLocation( coreId ) ),
		argLoc_( argLoc ),
		valueType_( valueType ),
		expectedType_( expectedType )
{ }

Exceptions::CoreStateTypeMismatch::~CoreStateTypeMismatch( )
{ }

void
Exceptions::CoreStateTypeMismatch::display( std::ostream & os ) const
{
	os << "Core function " << *coreLoc_ << ", state " ;
	//	if( argName_ != 0 )
	//		{
	//			os << "\"" << argName_ << "\" " ;
	//		}
	os << "at " << argLoc_ << ": expected a " << expectedType_ << ", got a " << valueType_ << "." << std::endl ;
}


Exceptions::CoreDynamicTypeMismatch::CoreDynamicTypeMismatch( const Ast::SourceLocation & callLoc,
																															const Ast::PlacedIdentifier & coreId,
																															const Ast::PlacedIdentifier & id,
																															RefCountPtr< const char > valueType,
																															RefCountPtr< const char > expectedType )
	: Exceptions::RuntimeError( callLoc ),
		coreLoc_( new Interaction::BoundLocation( coreId ) ),
		id_( id ),
		valueType_( valueType ),
		expectedType_( expectedType )
{ }

Exceptions::CoreDynamicTypeMismatch::~CoreDynamicTypeMismatch( )
{ }

void
Exceptions::CoreDynamicTypeMismatch::display( std::ostream & os ) const
{
	os << "Core function " << *coreLoc_ << " encountered a type mismatch in the dynamic variable " ;
	id_.show( os, Ast::Identifier::DYNAMIC_VARIABLE );
	os << ": expected a " << expectedType_ << ", got a " << valueType_ << "." << std::endl ;
}

Exceptions::ContinuationTypeMismatch::ContinuationTypeMismatch( const Kernel::Continuation * locationCont,
																																RefCountPtr< const char > valueType,
																																RefCountPtr< const char > expectedType )
	: Exceptions::RuntimeError( locationCont->traceLoc( ) ),
		valueType_( valueType ),
		expectedType_( expectedType )
{ }

Exceptions::ContinuationTypeMismatch::~ContinuationTypeMismatch( )
{ }

void
Exceptions::ContinuationTypeMismatch::display( std::ostream & os ) const
{
	os << "The continuation expected a " << expectedType_ << ", got a " << valueType_ << "." << std::endl ;
}


Exceptions::CoreOutOfRange::CoreOutOfRange( const Ast::SourceLocation & callLoc, const Ast::PlacedIdentifier & coreId, Kernel::Arguments & args, size_t argNo, const Interaction::MessageString & msg )
	: Exceptions::CatchableError( callLoc ),
		coreLoc_( new Interaction::BoundLocation( coreId ) ),
	argNo_( argNo ), argLoc_( args.getLoc( argNo ) ), msg_( msg )
{ }

Exceptions::CoreOutOfRange::CoreOutOfRange( const Ast::PlacedIdentifier & coreId, Kernel::Arguments & args, size_t argNo, const Interaction::MessageString & msg )
	: Exceptions::CatchableError( Ast::THE_UNKNOWN_LOCATION ),
		coreLoc_( new Interaction::BoundLocation( coreId ) ),
	argNo_( argNo ), argLoc_( args.getLoc( argNo ) ), msg_( msg )
{ }

Exceptions::CoreOutOfRange::CoreOutOfRange( const Ast::SourceLocation & callLoc, const RefCountPtr< const Interaction::CoreLocation > & coreLoc, Kernel::Arguments & args, size_t argNo, const Interaction::MessageString & msg )
	: Exceptions::CatchableError( callLoc ),
		coreLoc_( coreLoc ),
		argNo_( argNo ), argLoc_( args.getLoc( argNo ) ), msg_( msg )
{ }

Exceptions::CoreOutOfRange::CoreOutOfRange( const RefCountPtr< const Interaction::CoreLocation > & coreLoc, Kernel::Arguments & args, size_t argNo, const Interaction::MessageString & msg )
	: Exceptions::CatchableError( Ast::THE_UNKNOWN_LOCATION ),
		coreLoc_( coreLoc ),
		argNo_( argNo ), argLoc_( args.getLoc( argNo ) ), msg_( msg )
{ }

Exceptions::CoreOutOfRange::CoreOutOfRange( Interaction::CoreLocation * coreLoc, Kernel::Arguments & args, size_t argNo, const Interaction::MessageString & msg )
	: Exceptions::CatchableError( Ast::THE_UNKNOWN_LOCATION ),
		coreLoc_( coreLoc ),
		argNo_( argNo ), argLoc_( args.getLoc( argNo ) ), msg_( msg )
{ }

Exceptions::CoreOutOfRange::~CoreOutOfRange( )
{ }

void
Exceptions::CoreOutOfRange::setMessage( const Interaction::MessageString & msg )
{
  msg_ = msg;
}

void
Exceptions::CoreOutOfRange::display( std::ostream & os ) const
{
	os << "Core function " << *coreLoc_ << ": " ;
	os << "Argument at " << argLoc_ << " is out of range; " << msg_ << std::endl ;
}

RefCountPtr< const Lang::Exception >
Exceptions::CoreOutOfRange::clone( const Kernel::ContRef & cont ) const
{
	static RefCountPtr< const Lang::Symbol > kind( new Lang::Symbol( "out_of_range" ) );
	std::ostringstream os;
	display( os );
	std::ostringstream osCoreLoc;
	osCoreLoc << *coreLoc_ ;
	return RefCountPtr< const Lang::Exception >( new Lang::Exception( this->getLoc( ),
																																		kind,
																																		RefCountPtr< const Lang::String >( new Lang::String( strrefdup( osCoreLoc ) ) ),
																																		RefCountPtr< const Lang::Integer >( new Lang::Integer( argNo_ ) ),
																																		strrefdup( os ),
																																		cont,
																																		this->exitCode( ) ) );
}

Exceptions::CoreRequirement::CoreRequirement( const Interaction::MessageString & msg, const RefCountPtr< const Interaction::CoreLocation > & coreLoc, const Ast::SourceLocation & callLoc )
	: Exceptions::CatchableError( callLoc ),
		coreLoc_( coreLoc ),
		msg_( msg )
{ }

Exceptions::CoreRequirement::CoreRequirement( const Interaction::MessageString & msg, Interaction::CoreLocation * coreLoc, const Ast::SourceLocation & callLoc )
	: Exceptions::CatchableError( callLoc ),
		coreLoc_( coreLoc ),
		msg_( msg )
{ }

Exceptions::CoreRequirement::CoreRequirement( const Interaction::MessageString & msg, const Ast::PlacedIdentifier & id, const Ast::SourceLocation & callLoc )
	: Exceptions::CatchableError( callLoc ),
		coreLoc_( new Interaction::BoundLocation( id ) ),
		msg_( msg )
{ }

Exceptions::CoreRequirement::~CoreRequirement( )
{ }

void
Exceptions::CoreRequirement::display( std::ostream & os ) const
{
	os << "Core function " << coreLoc_ << ": " << msg_ << std::endl ;
}

RefCountPtr< const Lang::Exception >
Exceptions::CoreRequirement::clone( const Kernel::ContRef & cont ) const
{
	static RefCountPtr< const Lang::Symbol > kind( new Lang::Symbol( "misc" ) );
	std::ostringstream os;
	display( os );
	std::ostringstream osCoreLoc;
	osCoreLoc << *coreLoc_ ;
	return RefCountPtr< const Lang::Exception >( new Lang::Exception( this->getLoc( ),
																																		kind,
																																		RefCountPtr< const Lang::String >( new Lang::String( strrefdup( osCoreLoc ) ) ),
																																		Lang::THE_VOID,
																																		strrefdup( os ),
																																		cont,
																																		this->exitCode( ) ) );
}


Exceptions::BuildRequirement::BuildRequirement( const char * dependency, const RefCountPtr< const Interaction::CoreLocation > & coreLoc, const Ast::SourceLocation & callLoc )
	: Exceptions::CatchableError( callLoc ),
		coreLoc_( coreLoc ),
		dependency_( dependency )
{ }

Exceptions::BuildRequirement::BuildRequirement( const char * dependency, const Ast::PlacedIdentifier & id, const Ast::SourceLocation & callLoc )
	: Exceptions::CatchableError( callLoc ),
		coreLoc_( new Interaction::BoundLocation( id ) ),
		dependency_( dependency )
{ }

Exceptions::BuildRequirement::~BuildRequirement( )
{ }

void
Exceptions::BuildRequirement::display( std::ostream & os ) const
{
	os << "Core function stub " << coreLoc_ << ": Shapes was built without " << dependency_ << ", so this function cannot be called." << std::endl ;
}

RefCountPtr< const Lang::Exception >
Exceptions::BuildRequirement::clone( const Kernel::ContRef & cont ) const
{
	static RefCountPtr< const Lang::Symbol > kind( new Lang::Symbol( "misc" ) );
	std::ostringstream os;
	display( os );
	std::ostringstream osCoreLoc;
	osCoreLoc << *coreLoc_ ;
	return RefCountPtr< const Lang::Exception >( new Lang::Exception( this->getLoc( ),
																																		kind,
																																		RefCountPtr< const Lang::String >( new Lang::String( strrefdup( osCoreLoc ) ) ),
																																		Lang::THE_VOID,
																																		strrefdup( os ),
																																		cont,
																																		this->exitCode( ) ) );
}


Exceptions::TeXLabelError::TeXLabelError( bool quoted, const char * region, const Interaction::MessageString & summary, const Interaction::MessageString & details, const Ast::SourceLocation & loc )
	: Exceptions::RuntimeError( loc ), strLoc_( loc ), quoted_( quoted ), region_( region ), summary_( summary ), details_( details )
{ }

Exceptions::TeXLabelError::~TeXLabelError( )
{ }

void
Exceptions::TeXLabelError::display( ostream & os ) const
{
	if( quoted_ )
		{
			os << "TeX error in " << region_ ;
		}
	else
		{
			os << "Error in TeX " << region_ ;
		}
	if( ! strLoc_.isUnknown( ) )
		{
			os << " at " << strLoc_ ;
		}
	os << ": " ;
	if( quoted_ )
		{
			os << "\"" ;
		}
	os << summary_ ;
	if( quoted_ )
		{
			os << "\"" ;
		}
	os << std::endl ;
	details_.show( additionalLinesPrefix, os );
}


Exceptions::StaticTeXLabelError::StaticTeXLabelError( bool quoted, const char * region, const Interaction::MessageString & summary, const Interaction::MessageString & details, const Ast::SourceLocation & loc )
	: Exceptions::StaticInconsistency( loc ), quoted_( quoted ), region_( region ), summary_( summary ), details_( details )
{ }

Exceptions::StaticTeXLabelError::~StaticTeXLabelError( )
{ }

void
Exceptions::StaticTeXLabelError::display( ostream & os ) const
{
	if( quoted_ )
		{
			os << "TeX error in " << region_ ;
		}
	else
		{
			os << "Error in TeX " << region_ ;
		}
	os << ": " ;
	if( quoted_ )
		{
			os << "\"" ;
		}
	os << summary_ ;
	if( quoted_ )
		{
			os << "\"" ;
		}
	os << std::endl ;
	details_.show( additionalLinesPrefix, os );
}


Exceptions::MultipleDynamicBind::MultipleDynamicBind( const Ast::PlacedIdentifier * id, const Ast::SourceLocation & loc, const Ast::SourceLocation & prevLoc )
	: Exceptions::RuntimeError( loc ), id_( id ), prevLoc_( prevLoc )
{ }

Exceptions::MultipleDynamicBind::~MultipleDynamicBind( )
{ }

void
Exceptions::MultipleDynamicBind::display( std::ostream & os ) const
{
	os << "This dynamic binding of " ;
	id_->show( os, Ast::Identifier::DYNAMIC_VARIABLE );
	os << " is merged with the conflicting binding at " << prevLoc_ << "." << std::endl ;
}


Exceptions::UndefinedEscapeContinuation::UndefinedEscapeContinuation( const char * id, const Ast::SourceLocation & loc )
	: Exceptions::RuntimeError( loc ), id_( id )
{ }

Exceptions::UndefinedEscapeContinuation::~UndefinedEscapeContinuation( )
{ }

void
Exceptions::UndefinedEscapeContinuation::display( std::ostream & os ) const
{
	os << "The escape continuation " << id_ << " was not defined in the dynamic context." << std::endl ;
}


Exceptions::DeadStateAccess::DeadStateAccess( )
	: Exceptions::RuntimeError( Ast::THE_UNKNOWN_LOCATION )
{ }

Exceptions::DeadStateAccess::~DeadStateAccess( )
{ }

void
Exceptions::DeadStateAccess::display( std::ostream & os ) const
{
	os << "Accessing dead state." << std::endl ;
}


Exceptions::UnFreezable::UnFreezable( )
	: Exceptions::RuntimeError( Ast::THE_UNKNOWN_LOCATION )
{ }

Exceptions::UnFreezable::~UnFreezable( )
{ }

void
Exceptions::UnFreezable::display( std::ostream & os ) const
{
	os << "The state cannot be frozen." << std::endl ;
}


Exceptions::UnPeekable::UnPeekable( )
	: Exceptions::RuntimeError( Ast::THE_UNKNOWN_LOCATION )
{ }

Exceptions::UnPeekable::~UnPeekable( )
{ }

void
Exceptions::UnPeekable::display( std::ostream & os ) const
{
	os << "The state cannot be peeked.  Consider freezing it." << std::endl ;
}


Exceptions::UninitializedAccess::UninitializedAccess( )
	: Exceptions::RuntimeError( Ast::THE_UNKNOWN_LOCATION )
{ }

Exceptions::UninitializedAccess::~UninitializedAccess( )
{ }

void
Exceptions::UninitializedAccess::display( std::ostream & os ) const
{
	os << "The accessed variable or state has not been initialized yet." << std::endl ;
}


Exceptions::DtMinError::DtMinError( double dt )
	: Exceptions::CatchableError( Ast::THE_UNKNOWN_LOCATION ), dt_( dt )
{ }

Exceptions::DtMinError::~DtMinError( )
{ }

void
Exceptions::DtMinError::display( std::ostream & os ) const
{
	os << "Path segment too long in relation to arcdelta: dt = " << dt_ << " < " << Computation::the_dtMin << ".  Either increase arcdelta, or inhibit this error." << std::endl ;
}

RefCountPtr< const Lang::Exception >
Exceptions::DtMinError::clone( const Kernel::ContRef & cont ) const
{
	static RefCountPtr< const Lang::Symbol > kind( new Lang::Symbol( "dtmin" ) );
	std::ostringstream os;
	display( os );
	return RefCountPtr< const Lang::Exception >( new Lang::Exception( this->getLoc( ),
																																		kind,
																																		RefCountPtr< const Lang::String >( new Lang::String( "(core path computation)", true ) ),
																																		RefCountPtr< const Lang::Float >( new Lang::Float( dt_ / Computation::the_dtMin ) ),
																																		strrefdup( os ),
																																		cont,
																																		this->exitCode( ) ) );
}


Exceptions::AffineTransformKillsPlane::AffineTransformKillsPlane( double sigma2 )
	: Exceptions::CatchableError( Ast::THE_UNKNOWN_LOCATION ), sigma2_( sigma2 )
{ }

Exceptions::AffineTransformKillsPlane::~AffineTransformKillsPlane( )
{ }

void
Exceptions::AffineTransformKillsPlane::display( std::ostream & os ) const
{
	os << "When transforming a plane normal, it was found that the affine transform is too singular, with a second largest singular value of " << sigma2_ << "." << std::endl ;
}

RefCountPtr< const Lang::Exception >
Exceptions::AffineTransformKillsPlane::clone( const Kernel::ContRef & cont ) const
{
	static RefCountPtr< const Lang::Symbol > kind( new Lang::Symbol( "numeric" ) );
	std::ostringstream os;
	display( os );
	return RefCountPtr< const Lang::Exception >( new Lang::Exception( this->getLoc( ),
																																		kind,
																																		RefCountPtr< const Lang::String >( new Lang::String( "(core float triple computation)", true ) ),
																																		Lang::THE_VOID,
																																		strrefdup( os ),
																																		cont,
																																		this->exitCode( ) ) );
}

Exceptions::MissingFontMetrics::MissingFontMetrics( RefCountPtr< const char > fontname, const std::list< std::string > * searchPath )
	: Exceptions::RuntimeError( Ast::THE_UNKNOWN_LOCATION ), fontname_( fontname ), searchPath_( searchPath )
{ }

Exceptions::MissingFontMetrics::~MissingFontMetrics( )
{ }

void
Exceptions::MissingFontMetrics::display( std::ostream & os ) const
{
	os << "The font metrics for " << fontname_ << ", named " << fontname_ << ".afm, could not be found in {" ;
	typedef typeof *searchPath_ ListType;
	for( ListType::const_iterator i = searchPath_->begin( ); i != searchPath_->end( ); ++i )
		{
			os << " " << *i ;
		}
	os << " }." << std::endl ;
}


Exceptions::FontMetricsError::FontMetricsError( const RefCountPtr< const char > & fontname, const Interaction::MessageString & message )
	: Exceptions::RuntimeError( Ast::THE_UNKNOWN_LOCATION ), fontname_( fontname ), message_( message )
{ }

Exceptions::FontMetricsError::~FontMetricsError( )
{ }

void
Exceptions::FontMetricsError::display( std::ostream & os ) const
{
	os << "There was a problem with the font metrics for " << fontname_ << ": " << message_ << std::endl ;
}


Exceptions::InsertingEmptyPage::InsertingEmptyPage( const Ast::SourceLocation & loc )
	: Exceptions::CatchableError( loc )
{ }

Exceptions::InsertingEmptyPage::~InsertingEmptyPage( )
{ }

void
Exceptions::InsertingEmptyPage::display( std::ostream & os ) const
{
	os << "Attempt to place empty page in catalog." << std::endl ;
}

RefCountPtr< const Lang::Exception >
Exceptions::InsertingEmptyPage::clone( const Kernel::ContRef & cont ) const
{
	static RefCountPtr< const Lang::Symbol > kind( new Lang::Symbol( "misc" ) );
	std::ostringstream os;
	display( os );
	return RefCountPtr< const Lang::Exception >( new Lang::Exception( this->getLoc( ),
																																		kind,
																																		RefCountPtr< const Lang::String >( new Lang::String( "(core exception)", true ) ),
																																		Lang::THE_VOID,
																																		strrefdup( os ),
																																		cont,
																																		this->exitCode( ) ) );
}


Exceptions::InvalidGraphKeyType::InvalidGraphKeyType( )
	: Shapes::Exceptions::InternalError( "Graph key was not of type §Symbol or §Integer." )
{ }

Exceptions::InvalidGraphKeyType::~InvalidGraphKeyType( )
{ }

Exceptions::GraphKeyTypeMismatch::GraphKeyTypeMismatch( const Ast::SourceLocation & loc, const char * hint, RefCountPtr< const char > valueType )
	: Shapes::Exceptions::TypeMismatch( loc, hint, valueType, expectedType )
{ }

Exceptions::GraphKeyTypeMismatch::~GraphKeyTypeMismatch( )
{ }

RefCountPtr< const char > Exceptions::GraphKeyTypeMismatch::expectedType = Helpers::typeSetString( Lang::Symbol::staticTypeName( ), Lang::Integer::staticTypeName( ) );
RefCountPtr< const char > Exceptions::GraphKeyTypeMismatch::expectedTypeOrNode = Helpers::typeSetString( Lang::Node::staticTypeName( ), Lang::Symbol::staticTypeName( ), Lang::Integer::staticTypeName( ) );
RefCountPtr< const char > Exceptions::GraphKeyTypeMismatch::expectedTypeOrStructure = Helpers::typeSetString( Lang::Structure::staticTypeName( ), Lang::Symbol::staticTypeName( ), Lang::Integer::staticTypeName( ) );


Exceptions::InvalidGraphKey::InvalidGraphKey( Type type )
	: Shapes::Exceptions::InternalError( (type == NODE) ? "Graph key does not belong to any node in the graph." : "Graph key does not belong to any partition in the graph." )
{ }

Exceptions::InvalidGraphKey::~InvalidGraphKey( )
{ }

Exceptions::GraphKeyOutOfRange::GraphKeyOutOfRange( Type type, const Ast::SourceLocation & callLoc, const Ast::PlacedIdentifier & coreId, Kernel::Arguments & args, size_t argNo )
	: Shapes::Exceptions::CoreOutOfRange( callLoc, coreId, args, argNo, "" ), type_( type )
{
	initMessage( args, argNo );
}

Exceptions::GraphKeyOutOfRange::GraphKeyOutOfRange( Type type, const Ast::SourceLocation & callLoc, const RefCountPtr< const Interaction::CoreLocation > & coreLoc, Kernel::Arguments & args, size_t argNo )
	: Shapes::Exceptions::CoreOutOfRange( callLoc, coreLoc, args, argNo, "" ), type_( type )
{
	initMessage( args, argNo );
}

void
Exceptions::GraphKeyOutOfRange::initMessage( Kernel::Arguments & args, size_t argNo )
{
	RefCountPtr< const Lang::Value > key = args.getValue( argNo );
	std::ostringstream oss;
	switch( type_ )
		{
		case NODE:
			oss << "The node key " ;
			key->show( oss );
			oss << " does not belong to any node in the graph." ;
			break;
		case PARTITION:
			oss << "The partition key " ;
			key->show( oss );
			oss << " does not belong to any partition in the graph." ;
			break;
		}
	setMessage( strrefdup( oss ) );
}

Exceptions::GraphKeyOutOfRange::~GraphKeyOutOfRange( )
{ }


Exceptions::InvalidGraphElement::InvalidGraphElement( Type type, const Ast::SourceLocation & loc )
	: Exceptions::CatchableError( loc ), type_( type )
{ }

Exceptions::InvalidGraphElement::~InvalidGraphElement( )
{ }

void
Exceptions::InvalidGraphElement::display( std::ostream & os ) const
{
	os << "The " ;
	switch( type_ )
		{
		case NODE:
			os << "node" ;
			break;
		case EDGE:
			os << "edge" ;
			break;
		}
	os << " does not belong to the graph." << std::endl ;
}

RefCountPtr< const Lang::Exception >
Exceptions::InvalidGraphElement::clone( const Kernel::ContRef & cont ) const
{
	static RefCountPtr< const Lang::Symbol > kind( new Lang::Symbol( "type_mismatch" ) );
	std::ostringstream os;
	display( os );
	return RefCountPtr< const Lang::Exception >( new Lang::Exception( this->getLoc( ),
																																		kind,
																																		RefCountPtr< const Lang::String >( new Lang::String( "(graph operation)", true ) ),
																																		Lang::THE_VOID,
																																		strrefdup( os ),
																																		cont,
																																		this->exitCode( ) ) );
}

Exceptions::EdgeTraceError::EdgeTraceError( Type type, const RefCountPtr< const Lang::Value > & edgeKeyFrom, const RefCountPtr< const Lang::Value > & edgeKeyTo, const RefCountPtr< const Lang::Value > & nodeKey, const Ast::SourceLocation & loc )
	: Exceptions::CatchableError( loc ), type_( type ), edgeKeyFrom_( edgeKeyFrom ), edgeKeyTo_( edgeKeyTo ), nodeKey_( nodeKey )
{ }

Exceptions::EdgeTraceError::~EdgeTraceError( )
{ }

void
Exceptions::EdgeTraceError::display( std::ostream & os ) const
{
	switch( type_ )
		{
		case UNDIRECTED:
			os << "The undirected edge with node keys " ;
			edgeKeyFrom_->show( os );
			os << " and " ;
			edgeKeyTo_->show( os );
			os << " cannot be traced (or backtraced) from the node with key " ;
			nodeKey_->show( os );
			os << "." << std::endl ;
			break;
		case TRACE:
		case BACKTRACE:
			os << "The directed edge with source node key " ;
			edgeKeyFrom_->show( os );
			os << " and target node key " ;
			edgeKeyTo_->show( os );
			os << " cannot be " << ((type_ == TRACE) ? "traced" : "backtraced") << " from the node with key " ;
			nodeKey_->show( os );
			os << "." << std::endl ;
			break;
		}
}

RefCountPtr< const Lang::Exception >
Exceptions::EdgeTraceError::clone( const Kernel::ContRef & cont ) const
{
	static RefCountPtr< const Lang::Symbol > kind( new Lang::Symbol( "misc" ) );
	std::ostringstream os;
	display( os );
	return RefCountPtr< const Lang::Exception >( new Lang::Exception( this->getLoc( ),
																																		kind,
																																		RefCountPtr< const Lang::String >( new Lang::String( "(edge tracing)", true ) ),
																																		Lang::THE_VOID,
																																		strrefdup( os ),
																																		cont,
																																		this->exitCode( ) ) );
}

Exceptions::GraphDomainError::GraphDomainError( Type type, bool directed, const RefCountPtr< const Lang::Value > & edgeKeyFrom, const RefCountPtr< const Lang::Value > & edgeKeyTo, const Ast::SourceLocation & loc )
	: Exceptions::CatchableError( loc ), type_( type ), directed_( directed ), edgeKeyFrom_( edgeKeyFrom ), edgeKeyTo_( edgeKeyTo )
{ }

Exceptions::GraphDomainError::~GraphDomainError( )
{ }

void
Exceptions::GraphDomainError::display( std::ostream & os ) const
{
	if( directed_ ){
		os << "The directed edge from " ;
		edgeKeyFrom_->show( os );
		os << " to " ;
		edgeKeyTo_->show( os );
	}else{
		os << "The undirected edge between " ;
		edgeKeyFrom_->show( os );
		os << " and " ;
		edgeKeyTo_->show( os );
	}
	os << " is not allowed since the graph domain does not permit " ;
	switch( type_ ){
	case DIRECTED:
		os << "directed" ;
		break;
	case UNDIRECTED:
		os << "undirected" ;
		break;
	case LOOPS:
		os << "loop" ;
		break;
	case PARALLEL:
		os << "parallel" ;
		break;
	}
	os << " edges." << std::endl ;
}

RefCountPtr< const Lang::Exception >
Exceptions::GraphDomainError::clone( const Kernel::ContRef & cont ) const
{
	static RefCountPtr< const Lang::Symbol > kind( new Lang::Symbol( "misc" ) );
	std::ostringstream os;
	display( os );
	return RefCountPtr< const Lang::Exception >( new Lang::Exception( this->getLoc( ),
																																		kind,
																																		RefCountPtr< const Lang::String >( new Lang::String( "(graph domain)", true ) ),
																																		Lang::THE_VOID,
																																		strrefdup( os ),
																																		cont,
																																		this->exitCode( ) ) );
}


Exceptions::InvalidGraphPartition::InvalidGraphPartition( const RefCountPtr< const Lang::Value > & partition, const RefCountPtr< const Lang::SingleList > & partitions, const Ast::SourceLocation & loc )
	: Exceptions::CatchableError( loc ), partition_( partition ), partitions_( partitions )
{ }

Exceptions::InvalidGraphPartition::~InvalidGraphPartition( )
{ }

void
Exceptions::InvalidGraphPartition::display( std::ostream & os ) const
{
	os << "The partition key " ;
	partition_->show( os );
	os << " is not among the declared keys: {" ;
	{
		RefCountPtr< const Lang::SingleListPair > p = partitions_.down_cast< const Lang::SingleListPair >( );
		while( p != NullPtr< const Lang::SingleListPair >( ) ){
			RefCountPtr< const Lang::Value > val = p->car_->getUntyped( );
			os << " " ;
			val->show( os );
			p = p->cdr_.down_cast< const Lang::SingleListPair >( );
		}
	}
	os << " }." << std::endl ;
}

RefCountPtr< const Lang::Exception >
Exceptions::InvalidGraphPartition::clone( const Kernel::ContRef & cont ) const
{
	static RefCountPtr< const Lang::Symbol > kind( new Lang::Symbol( "misc" ) );
	std::ostringstream os;
	display( os );
	return RefCountPtr< const Lang::Exception >( new Lang::Exception( this->getLoc( ),
																																		kind,
																																		RefCountPtr< const Lang::String >( new Lang::String( "(graph partition)", true ) ),
																																		Lang::THE_VOID,
																																		strrefdup( os ),
																																		cont,
																																		this->exitCode( ) ) );
}


Exceptions::GraphPartitionViolation::GraphPartitionViolation( const RefCountPtr< const Lang::Value > & sourceKey, const RefCountPtr< const Lang::Value > & targetKey, const RefCountPtr< const Lang::Value > & partition, const Ast::SourceLocation & loc )
	: Exceptions::CatchableError( loc ), sourceKey_( sourceKey ), targetKey_( targetKey ), partition_( partition )
{ }

Exceptions::GraphPartitionViolation::~GraphPartitionViolation( )
{ }

void
Exceptions::GraphPartitionViolation::display( std::ostream & os ) const
{
	os << "Edge incident to nodes with keys " ;
	sourceKey_->show( os );
	os << " and " ;
	targetKey_->show( os );
	os << " violates the graph partitioning, with both nodes having the partition key " ;
	partition_->show( os );
	os << "." << std::endl ;
}

RefCountPtr< const Lang::Exception >
Exceptions::GraphPartitionViolation::clone( const Kernel::ContRef & cont ) const
{
	static RefCountPtr< const Lang::Symbol > kind( new Lang::Symbol( "misc" ) );
	std::ostringstream os;
	display( os );
	return RefCountPtr< const Lang::Exception >( new Lang::Exception( this->getLoc( ),
																																		kind,
																																		RefCountPtr< const Lang::String >( new Lang::String( "(graph partition violation)", true ) ),
																																		Lang::THE_VOID,
																																		strrefdup( os ),
																																		cont,
																																		this->exitCode( ) ) );
}


Exceptions::UndefinedCrossRef::UndefinedCrossRef( const Ast::SourceLocation & loc, RefCountPtr< const char > ref )
	: Exceptions::PostCondition( loc ), ref_( ref )
{ }

Exceptions::UndefinedCrossRef::~UndefinedCrossRef( )
{ }

void
Exceptions::UndefinedCrossRef::display( std::ostream & os ) const
{
	os << "The cross reference \"" << ref_ << "\" is not defined." << std::endl ;
}

Exceptions::BadSetValueState::BadSetValueState( )
	: Exceptions::InternalError( "Setting the value of a variable which is not in the right state." )
{ }

Exceptions::BadSetValueState::~BadSetValueState( )
{ }


Exceptions::InvocationError::InvocationError( const Interaction::MessageString & msg, bool justAWarning )
  : msg_( msg ), justAWarning_( justAWarning )
{ }

Exceptions::InvocationError::~InvocationError( )
{ }

void
Exceptions::InvocationError::display( std::ostream & os ) const
{
  os << "Invocation " << ( justAWarning_ ? "warning" : "error" ) << ": " << msg_ << std::endl ;
}
