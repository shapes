/* This file is part of Shapes.
 *
 * Shapes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Shapes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2008, 2013, 2014 Henrik Tidefelt
 */

#include <cmath>
#include <iomanip>

#include "environment.h"
#include "shapesexceptions.h"
#include "shapescore.h"
#include "consts.h"
#include "globals.h"
#include "shapesvalue.h"
#include "classtypes.h"
#include "hottypes.h"
#include "continuations.h"
#include "statetypes.h"
#include "multipage.h"
#include "errorhandlers.h"
#include "debuglog.h"
#include "check.h"

using namespace Shapes;


size_t Kernel::Environment::createdCount = 0;
size_t Kernel::Environment::liveCount = 0;
Kernel::Environment::LexicalKey Kernel::Environment::theMissingKey( 0, std::numeric_limits< size_t >::max( ) );
const Ast::SourceLocation Kernel::Environment::THE_INITIALIZATION_LOCATION( Ast::FileID::build_internal( "< Initialization >" ) );
template< class T >
class Binary
{
public:
  T val;
  Binary( const T & _val ) : val( _val ) { }
};

template< class T >
std::ostream &
operator << ( std::ostream & os, const Binary< T > & self )
{
  for( int i = 8*sizeof( T ) - 1; i >= 0; --i )
    {
      if( (( self.val & (T(1)<<i) )) != 0 )
        {
          os << 1 ;
        }
      else
        {
          os << '_' ;
        }
    }
  return os;
}



Kernel::Thunk::Thunk( Kernel::PassedEnv env, Kernel::PassedDyn dyn, Ast::Expression * expr )
  : env_( env ), dyn_( dyn ), expr_( expr ), forced_( false )
{ }

void
Kernel::Thunk::force( Kernel::EvalState * evalState, bool onlyOnce ) const
{
  if( onlyOnce )
    {
      if( forced_ )
        {
          throw Exceptions::InternalError( "It's unwise to force a thunk twice!  It should be deleted right after the first force." );
        }
      forced_ = true;
    }

  evalState->expr_ = expr_;
  evalState->env_ = env_;
  evalState->dyn_ = dyn_;

  /* The continuation is not se by the thunk! */
}

Kernel::Thunk *
Kernel::Thunk::deepCopy( )
{
  return new Thunk( env_, dyn_, expr_ );
}


void
Kernel::Thunk::gcMark( Kernel::GCMarkedSet & marked )
{
  if( ! forced_ )
    {
      env_->gcMark( marked );
      dyn_->gcMark( marked );
    }
}

Ast::Expression *
Kernel::Thunk::getExpr( )
{
  return expr_;
}

void
Kernel::Thunk::printEnv( std::ostream & os ) const
{
  env_->print( os );
}


Kernel::Variable::Variable( const RefCountPtr< const Lang::Value > & val )
  : thunk_( 0 ), val_( val ), state_( Kernel::Variable::COLD )
{ }

Kernel::Variable::Variable( Kernel::Thunk * thunk )
  : thunk_( thunk ), val_( NullPtr< const Lang::Value >( ) ), state_( Kernel::Variable::THUNK )
{ }

Kernel::Variable::~Variable( )
{
  if( thunk_ != NULL )
    {
      delete thunk_;
    }
}

Kernel::State::State( )
  : alive_( true )
{ }

Kernel::State::~State( )
{ }

void
Kernel::State::tackOn( Kernel::EvalState * evalState, const RefCountPtr< const Lang::Value > & piece, const Ast::SourceLocation & callLoc )
{
  if( ! alive_ )
    {
      throw Exceptions::DeadStateAccess( );
    }
  this->tackOnImpl( evalState, piece, callLoc );
}

void
Kernel::State::peek( Kernel::EvalState * evalState, const Ast::SourceLocation & callLoc )
{
  if( ! alive_ )
    {
      throw Exceptions::DeadStateAccess( );
    }
  this->peekImpl( evalState, callLoc );
}

void
Kernel::State::freeze( Kernel::EvalState * evalState, const Ast::SourceLocation & callLoc )
{
  if( ! alive_ )
    {
      throw Exceptions::DeadStateAccess( );
    }
  /* It would make sense to have an intermediate state here.
   */
  alive_ = false;

  // Perhaps, it would be smart to use a continuation to erase the implementation once it is used...
  //  evalState->cont_ = Kernel::ContRef( new Kernel::StmtStoreVariableContinuation( selfRef, evalState->cont_, callLoc ) );

  this->freezeImpl( evalState, callLoc );
}

RefCountPtr< const Lang::Function >
Kernel::State::getMutator( const char * mutatorID )
{
  return getClass( )->getMutator( mutatorID );
}

RefCountPtr< const char >
Kernel::State::getTypeName( ) const
{
  return this->getClass( )->getPrettyName( );
}

bool
Kernel::State::isAlive( ) const
{
  return alive_;
}


void
Kernel::Variable::force( const Kernel::VariableHandle & selfRef, Kernel::EvalState * evalState ) const
{
  /* The reason that selfRef is passed as const reference is that this is a const method, and it is expected that selfRef actually is a handle to *this.  Hence, it does not make sense to not allow the selfRef to be passed by const reference.  On the other hand, we actually do need to do some non-const things with selfRef, and therefore we resort to some ugly const_casting this time.
   */
  if( state_ == Kernel::Variable::THUNK )
    {
      state_ = Kernel::Variable::FORCING;
      evalState->cont_ = Kernel::ContRef( new Kernel::StoreVariableContinuation( const_cast< Kernel::VariableHandle & >( selfRef ), evalState->cont_, thunk_->getExpr( )->loc( ) ) );
      thunk_->force( evalState );
      delete thunk_;
      thunk_ = 0;
      return;
    }
  if( val_ == NullPtr< const Lang::Value >( ) )
    {
      if( state_ == Kernel::Variable::FORCING )
        {
          throw Exceptions::MiscellaneousRequirement( "Cyclic forcing." );
        }
      throw Exceptions::InternalError( "Force failed.  No value, not forcing." );
    }
  Kernel::ContRef cont = evalState->cont_;
  cont->takeHandle( const_cast< Kernel::VariableHandle & >( selfRef ), evalState );
}

RefCountPtr< const Lang::Value > &
Kernel::Variable::getUntyped( ) const
{
  if( val_ == NullPtr< const Lang::Value >( ) )
    {
      throw Exceptions::InternalError( "The value is not ready to be get without continuation." );
    }
  return val_;
}


bool
Kernel::Variable::isThunk( ) const
{
  return ( state_ & Kernel::Variable::FORCING ) != 0;
}

bool
Kernel::Variable::isThunk( Ast::Expression * expr ) const
{
  return state_ == Kernel::Variable::THUNK && thunk_->getExpr( ) == expr;
}

Kernel::Thunk *
Kernel::Variable::copyThunk( ) const
{
  if( thunk_ == NULL )
    {
      throw Exceptions::InternalError( "Variable::copyThunk: There was no thunk to copy." );
    }
  return thunk_->deepCopy( );
}

void
Kernel::Variable::gcMark( Kernel::GCMarkedSet & marked )
{
  if( thunk_ != NULL )
    {
      thunk_->gcMark( marked );
      return;
    }
  if( val_ != NullPtr< const Lang::Value >( ) )
    {
      const_cast< Lang::Value * >( val_.getPtr( ) )->gcMark( marked );
    }
}

void
Kernel::Variable::setValue( const RefCountPtr< const Lang::Value > & val ) const
{
  if( state_ != Kernel::Variable::FORCING )
    throw Exceptions::BadSetValueState( );
  val_ = val;
  state_ = Kernel::Variable::COLD;
}


Kernel::DynamicVariableProperties::~DynamicVariableProperties( )
{
  /* We do not own id_.
   */
}

Kernel::DynamicStateProperties::~DynamicStateProperties( )
{
  /* We do not own id_.
   */
}


void
Kernel::Environment::initDefineHandle( const Ast::PlacedIdentifier & id, const Kernel::VariableHandle & val )
{
  if( Interaction::logGlobals )
    {
      std::ostream & os = Kernel::theDebugLog.os( );
      os << "--log-globals> variable: " ;
      id.show( os, Ast::Identifier::VARIABLE );
      os << std::endl ;
    }

  if( ! bindings_->insert( id, THE_INITIALIZATION_LOCATION ) )
    {
      /* An error message has already been generated. */
      return;
    }
  values_->push_back( val );

  CHECK( /* Assume that matching element counts insure that the identifier is mapped to the correct position. */
    if( values_->size( ) != bindings_->size( ) ){
      throw Exceptions::InternalError( "Kernel::Environment::initDefineHandle: Inconsistent number of values." );
    } );
}

void
Kernel::Environment::initDefineCoreFunction( Lang::CoreFunction * fun )
{
  initDefineHandle( fun->id( ), Kernel::VariableHandle( new Kernel::Variable( RefCountPtr< const Lang::Value >( fun ) ) ) );
}

void
Kernel::Environment::initDefineCoreFunction( RefCountPtr< const Lang::CoreFunction > fun )
{
  initDefineHandle( fun->id( ), Kernel::VariableHandle( new Kernel::Variable( fun ) ) );
}

void
Kernel::Environment::initDefine( const Ast::PlacedIdentifier & id, const RefCountPtr< const Lang::Value > & val )
{
  initDefineHandle( id, Kernel::VariableHandle( new Kernel::Variable( val ) ) );
}

void
Kernel::Environment::initDefine( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name, const RefCountPtr< const Lang::Value > & val )
{
  Ast::PlacedIdentifier id( ns, name);
  initDefineHandle( id, Kernel::VariableHandle( new Kernel::Variable( val ) ) );
}

void
Kernel::Environment::initDefine( const Ast::PlacedIdentifier & id, Kernel::StateHandle state )
{
  if( Interaction::logGlobals )
    {
      std::ostream & os = Kernel::theDebugLog.os( );
      os << "--log-globals> state: " ;
      id.show( os, Ast::Identifier::STATE );
      os << std::endl ;
    }

  if( ! stateBindings_->insert( id, THE_INITIALIZATION_LOCATION ) )
    {
      /* An error message has already been generated. */
      return;
    }
  states_->push_back( state );

  CHECK( /* Assume that matching element counts insure that the identifier is mapped to the correct position. */
    if( states_->size( ) != stateBindings_->size( ) ){
      throw Exceptions::InternalError( "Kernel::Environment::initDefine: Inconsistent number of states." );
    } );
}

void
Kernel::Environment::initDefine( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name, Kernel::StateHandle state )
{
  Ast::PlacedIdentifier id( ns, name);
  initDefine( id, state );
}

void
Kernel::Environment::initDefineClass( const RefCountPtr< const Lang::Class > & cls )
{
  RefCountPtr< const char > idRef = cls->getPrettyName( );
  const char * simpleId = strdup( idRef.getPtr( ) );
  charPtrDeletionList_.push_back( simpleId );
  initDefineHandle( Ast::PlacedIdentifier( Lang::THE_NAMESPACE_Shapes_Data_Type, simpleId ), Kernel::VariableHandle( new Kernel::Variable( cls ) ) );
}

void
Kernel::Environment::initDefineDynamic( DynamicVariableProperties * dynProps )
{
  const Ast::PlacedIdentifier & id( dynProps->id( ) );

  if( Interaction::logGlobals )
    {
      Kernel::theDebugLog.os( ) << "--log-globals> dynamic: " ;
      id.show( Kernel::theDebugLog.os( ), Ast::Identifier::DYNAMIC_VARIABLE );
      Kernel::theDebugLog.os( ) << std::endl ;
    }

  if( ! dynamicKeyBindings_->insert( id, THE_INITIALIZATION_LOCATION ) )
    {
      /* An error message has already been generated. */
      return;
    }
  dynamicKeyValues_->push_back( dynProps );

  CHECK( /* Assume that matching element counts insure that the identifier is mapped to the correct position. */
    if( dynamicKeyValues_->size( ) != dynamicKeyBindings_->size( ) ){
      throw Exceptions::InternalError( "Kernel::Environment::initDefineDynamic: Inconsistent number of dynamic keys (1 argument)." );
    } );
}

void
Kernel::Environment::initDefineDynamic( const Ast::PlacedIdentifier & id, const RefCountPtr< const Lang::Function > & filter, const Kernel::VariableHandle & defaultVal )
{
  initDefineDynamic( newPlacedIdentifier( id ), filter, defaultVal );
}

void
Kernel::Environment::initDefineDynamic( const Ast::PlacedIdentifier * id, const RefCountPtr< const Lang::Function > & filter, const Kernel::VariableHandle & defaultVal )
{
  if( Interaction::logGlobals )
    {
      Kernel::theDebugLog.os( ) << "--log-globals> dynamic: " ;
      id->show( Kernel::theDebugLog.os( ), Ast::Identifier::DYNAMIC_VARIABLE );
      Kernel::theDebugLog.os( ) << std::endl ;
    }

  if( ! dynamicKeyBindings_->insert( *id, THE_INITIALIZATION_LOCATION ) )
    {
      /* An error message has already been generated. */
      return;
    }
  Kernel::DynamicEnvironmentKeyType key = dynamicKeyValues_->size( );
  dynamicKeyValues_->push_back( new Kernel::UserDynamicVariableProperties( id, key, filter, defaultVal ) );

  CHECK( /* Assume that matching element counts insure that the identifier is mapped to the correct position. */
    if( dynamicKeyValues_->size( ) != dynamicKeyBindings_->size( ) ){
      throw Exceptions::InternalError( "Kernel::Environment::initDefineDynamic: Inconsistent number of dynamic keys (3 arguments)." );
    } );
}

void
Kernel::Environment::initDefineDynamicHandler( const Ast::PlacedIdentifier & id, const char * msg )
{
  initDefineDynamic( id.clone( ),
                     Lang::THE_IDENTITY,
                     Helpers::newValHandle( new Lang::ExceptionWrapper< Exceptions::HandlerError >( id, msg ) ) );
}

PtrOwner_back_Access< std::list< const Ast::PlacedIdentifier * > > Kernel::Environment::coreIdentifiers;

const Ast::PlacedIdentifier *
Kernel::Environment::newPlacedIdentifier( const RefCountPtr< const Ast::NamespacePath > & path, const char * simpleId )
{
  const Ast::PlacedIdentifier * id = new Ast::PlacedIdentifier( path, simpleId );
  coreIdentifiers.push_back( id );
  return id;
}

const Ast::PlacedIdentifier *
Kernel::Environment::newPlacedIdentifier( const Ast::PlacedIdentifier & id )
{
  return newPlacedIdentifier( id.absolutePathRef( ), id.simpleId( ) );
}

Kernel::Environment::Environment( std::list< Kernel::Environment * > & garbageArea, const char * debugName )
  : parent_( 0 ),
    bindings_( new Ast::IdentifierTree( NULL, false ) ),
    values_( new std::vector< Kernel::VariableHandle >( ) ),
    dynamicKeyBindings_( new Ast::IdentifierTree( NULL, false ) ),
    dynamicKeyValues_( new std::vector< DynamicVariableProperties * > ),
    stateBindings_( new Ast::IdentifierTree( NULL, false ) ),
    states_( new std::vector< Kernel::StateHandle >( ) ),
    dynamicStateKeyBindings_( new Ast::IdentifierTree( NULL, false ) ),
    dynamicStateKeyValues_( new std::vector< DynamicStateProperties * > ),
    debugName_( debugName )
{
  garbageArea.push_back( this );
  ++createdCount;
  ++liveCount;
}

Kernel::Environment::Environment( std::list< Kernel::Environment * > & garbageArea, Environment * parent, Ast::IdentifierTree * bindings, const RefCountPtr< std::vector< VariableHandle > > & values, Ast::IdentifierTree * stateBindings, const RefCountPtr< std::vector< StateHandle > > & states, const char * debugName )
  : parent_( parent ),
    bindings_( bindings ), values_( values ), dynamicKeyBindings_( NULL ),
    stateBindings_( stateBindings ), states_( states ), dynamicStateKeyBindings_( NULL ),
    debugName_( debugName )
                                 //, unitMap_( NullPtr< Kernel::Environment::UnitMapType >( ) )
{
  garbageArea.push_back( this );
  //  if( parent_ != NULL )
  //    {
  //      unitMap_ = parent->unitMap_;
  //    }
  ++createdCount;
  ++liveCount;
}

Kernel::Environment::~Environment( )
{
  clear( );
  if( dynamicKeyBindings_ != NULL )
    {
      if( parent_ == NULL )
        {
          /* The condition means that this is the global evironment, which created its own map.
           */
          delete dynamicKeyBindings_;
        }
      /* However, the values will always be owned by the environment itself, and be defined whenever dynamicKeyBindings_ != NULL
       */
      for( std::vector< DynamicVariableProperties * >::iterator i = dynamicKeyValues_->begin( ); i != dynamicKeyValues_->end( ); ++i )
        {
          if( *i != NULL )
            {
              delete *i;
            }
        }
      delete dynamicKeyValues_;
    }
  if( stateBindings_ != NULL )
    {
      if( parent_ == NULL )
        {
          /* The condition means that this is the global evironment, which created its own map.
           */
          delete stateBindings_;
        }
      /* However, the values will always be owned by the environment itself, and be defined whenever dynamicKeyBindings_ != NULL
       */
      for( std::vector< StateHandle >::iterator i = states_->begin( ); i != states_->end( ); ++i )
        {
          if( *i != NULL )
            {
              delete *i;
            }
        }
    }
  if( dynamicStateKeyBindings_ != NULL )
    {
      if( parent_ == NULL )
        {
          /* The condition means that this is the global evironment, which created its own map.
           */
          delete dynamicStateKeyBindings_;
        }
      /* However, the values will always be owned by the environment itself, and be defined whenever dynamicKeyBindings_ != NULL
       */
      for( std::vector< DynamicStateProperties * >::iterator i = dynamicStateKeyValues_->begin( ); i != dynamicStateKeyValues_->end( ); ++i )
        {
          if( *i != NULL )
            {
              delete *i;
            }
        }
      delete dynamicStateKeyValues_;
    }
  --liveCount;
}

void
Kernel::Environment::setParent( Kernel::Environment * parent )
{
  parent_ = parent;
  //  unitMap_ = parent.unitMap_;
}

Kernel::Environment *
Kernel::Environment::getParent( )
{
  if( isBaseEnvironment( ) )
    {
      throw Exceptions::MiscellaneousRequirement( "Trying to find the parent of the top level environment." );
    }
  return parent_;
}

const Kernel::Environment *
Kernel::Environment::getParent( ) const
{
  if( isBaseEnvironment( ) )
    {
      throw Exceptions::MiscellaneousRequirement( "Trying to find the parent of the top level environment." );
    }
  return parent_;
}

void
Kernel::Environment::setupDynamicKeyVariables( Ast::IdentifierTree * dynamicKeyBindings )
{
  if( dynamicKeyBindings_ == NULL )
    {
      dynamicKeyBindings_ = dynamicKeyBindings;
      dynamicKeyValues_ = new std::vector< DynamicVariableProperties * >;
    }
  CHECK(
  else
    {
      if( dynamicKeyBindings_ != dynamicKeyBindings )
        {
          throw Exceptions::InternalError( "Kernel::Environment::setupDynamicKeyVariables: dynamicKeyBindings_ != dynamicKeyBindings" );
        }
    }
        );
  size_t theSize = dynamicKeyBindings_->size( );
  dynamicKeyValues_->reserve( theSize );
  while( dynamicKeyValues_->size( ) < theSize )
    {
      dynamicKeyValues_->push_back( 0 );
    }
}

void
Kernel::Environment::setupDynamicStateKeyVariables( Ast::IdentifierTree * dynamicStateKeyBindings )
{
  if( dynamicStateKeyBindings_ == NULL )
    {
      dynamicStateKeyBindings_ = dynamicStateKeyBindings;
      dynamicStateKeyValues_ = new std::vector< DynamicStateProperties * >;
    }
  CHECK(
  else
    {
      if( dynamicStateKeyBindings_ != dynamicStateKeyBindings )
        {
          throw Exceptions::InternalError( "Kernel::Environment::setupDynamicStateKeyVariables: dynamicStateKeyBindings_ != dynamicStateKeyBindings" );
        }
    }
        );
  size_t theSize = dynamicStateKeyBindings_->size( );
  dynamicStateKeyValues_->reserve( theSize );
  while( dynamicStateKeyValues_->size( ) < theSize )
    {
      dynamicStateKeyValues_->push_back( 0 );
    }
}

void
Kernel::Environment::extendVectors( )
{
  values_->reserve( bindings_->size( ) );
  while( values_->size( ) < bindings_->size( ) )
    {
      values_->push_back( NullPtr< Kernel::Variable >( ) );
    }

  states_->reserve( stateBindings_->size( ) );
  while( states_->size( ) < stateBindings_->size( ) )
    {
      states_->push_back( NullPtr< Kernel::State >( ) );
    }

}

void
Kernel::Environment::clear( )
{
  /* Strange that this is empty...
   * The idea, though, is that the reference counting of this->values shall take care of deletion.
   */
}

Ast::AnalysisEnvironment *
Kernel::Environment::newAnalysisEnvironment( Ast::AnalysisEnvironment * parent ) const
{
  Ast::AnalysisEnvironment * res = new Ast::AnalysisEnvironment( Ast::theAnalysisEnvironmentList, parent, bindings_, stateBindings_ );
  if( dynamicKeyBindings_ != NULL )
    {
      res->setupDynamicKeyVariables( dynamicKeyBindings_ );
    }
  if( dynamicStateKeyBindings_ != NULL )
    {
      res->setupDynamicStateKeyVariables( dynamicStateKeyBindings_ );
    }

  return res;
}

void
Kernel::Environment::gcMark( Kernel::GCMarkedSet & marked )
{
  if( gcMarked_ )
    {
      return;
    }
  gcMarked_ = true;
  marked.insert( marked.begin( ), this );

  values_->clear( );
}

void
Kernel::Environment::collect( std::list< Kernel::Environment * > & garbageArea )
{
  for( std::list< Kernel::Environment * >::iterator i = garbageArea.begin( ); i != garbageArea.end( ); ++i )
    {
      (*i)->clear_gcMarked( );
    }

  Kernel::GCMarkedSet marked;
  for( std::list< Kernel::Environment * >::iterator i = garbageArea.begin( ); i != garbageArea.end( ); ++i )
    {
      (*i)->gcMark( marked );
    }

  for( std::list< Kernel::Environment * >::iterator i = garbageArea.begin( ); i != garbageArea.end( ); )
    {
      if( (*i)->gcMarked( ) )
        {
          ++i;
          continue;
        }
      std::list< Kernel::Environment * >::iterator tmp = i;
      ++i;
      delete *tmp;
      garbageArea.erase( tmp );
    }
}


const size_t Ast::IdentifierTree::NOT_FOUND = std::numeric_limits< size_t >::max( );

Ast::IdentifierTree::IdentifierTree( IdentifierTree * parent, bool encapsulation )
  : size_( 0 ), root_( ( parent == NULL ) ? this : ( parent->root( ) ) ), encap_( ( encapsulation || parent == NULL ) ? this : ( parent->encap( ) ) ), encapParent_( encapsulation ? parent : NULL )
{ }

Ast::IdentifierTree::~IdentifierTree( )
{
  typedef typeof namespaces_ MapType;
  for( MapType::iterator i = namespaces_.begin( ); i != namespaces_.end( ); ++i )
  {
    if( i->second.kind( ) != NamespaceLink::PRIMARY )
      continue;
    delete i->second.node( );
  }
}

Ast::IdentifierTree *
Ast::IdentifierTree::base( Ast::NamespaceReference::Base b )
{
  switch( b ){
  case Ast::NamespaceReference::ABSOLUTE:
    return root_;
  case Ast::NamespaceReference::LOCAL:
    return encap_;
  case Ast::NamespaceReference::RELATIVE:
    return this;
  }
}

const Ast::IdentifierTree *
Ast::IdentifierTree::base( Ast::NamespaceReference::Base b ) const
{
  switch( b ){
  case Ast::NamespaceReference::ABSOLUTE:
    return root_;
  case Ast::NamespaceReference::LOCAL:
    return encap_;
  case Ast::NamespaceReference::RELATIVE:
    return this;
  }
}

Ast::IdentifierTree *
Ast::IdentifierTree::newChild( const char * key, const Ast::SourceLocation & loc )
{
  IdentifierTree * tree = new Ast::IdentifierTree( this, Ast::SearchContext::isEncapsulationName( key ) );
  typedef typeof namespaces_ MapType;
  namespaces_.insert( MapType::value_type( key, NamespaceLink( NamespaceLink::PRIMARY, tree ) ) );
  if( encapParent_ != NULL ){
    encapParent_->insertEncapsulationLink( key, tree, loc );
  }
  return tree;
}

bool
Ast::IdentifierTree::insert( const Ast::PlacedIdentifier & id, const Ast::SourceLocation & loc )
{
  return insert( id.absolutePath( ).begin( ), id.absolutePath( ).end( ), id.simpleId( ), size_, loc );
}

bool
Ast::IdentifierTree::insertAlias( const Ast::PlacedIdentifier & id, const Ast::NamespaceReference & expansion, const Ast::SourceLocation & loc )
{
  return insertAlias( id.absolutePath( ).begin( ), id.absolutePath( ).end( ), id.simpleId( ), expansion, loc );
}

bool
Ast::IdentifierTree::insert( Ast::NamespacePath::const_iterator begin, const Ast::NamespacePath::const_iterator & end, const char * simpleId, size_t pos, const Ast::SourceLocation & loc )
{
  if( begin == end ){

    typedef typeof simpleIdentifiers_ MapType;
    if( simpleIdentifiers_.find( simpleId ) != simpleIdentifiers_.end( ) )
      return false;
    simpleIdentifiers_.insert( MapType::value_type( simpleId, pos ) );
    ++size_;
    if( encapParent_ != NULL ){
      encapParent_->insertEncapsulationLink( simpleId, pos, loc );
    }
    return true;

  }else{

    typedef typeof namespaces_ MapType;
    MapType::iterator j = namespaces_.find( *begin );
    if( j != namespaces_.end( ) ){

      bool result = j->second.node( )->insert( ++begin, end, simpleId, pos, loc );
      if (result)
        ++size_;
      return result;

    }else{

      IdentifierTree * tree = newChild( *begin, loc );
      bool result = tree->insert( ++begin, end, simpleId, pos, loc );
      if (result)
        ++size_;
      return result;

    }

  }
}

bool
Ast::IdentifierTree::insertAlias( Ast::NamespacePath::const_iterator begin, const Ast::NamespacePath::const_iterator & end, const char * simpleId, const Ast::NamespaceReference & expansion, const Ast::SourceLocation & loc )
{
  if( begin == end ){

    bool found;
    IdentifierTree * tree = resolveAlias( expansion, & found, loc );

    typedef typeof namespaces_ MapType;
    MapType::iterator j = namespaces_.find( simpleId );
    if( j != namespaces_.end( ) ){
      Ast::theAnalysisErrorsList.push_back( new Exceptions::NamespaceAliasCollision( loc, this, simpleId ) );
    }else{
      namespaces_.insert( MapType::value_type( simpleId, NamespaceLink( NamespaceLink::RESTRICTED, tree ) ) );
    }

    return found;

  }else{

    typedef typeof namespaces_ MapType;
    MapType::iterator j = namespaces_.find( *begin );
    if( j != namespaces_.end( ) ){

      return j->second.node( )->insertAlias( ++begin, end, simpleId, expansion, loc );

    }else{

      IdentifierTree * tree = newChild( *begin, loc );
      return tree->insertAlias( ++begin, end, simpleId, expansion, loc );

    }

  }
}

Ast::IdentifierTree *
Ast::IdentifierTree::resolveAlias( const Ast::NamespaceReference & expansion, bool * found, const Ast::SourceLocation & loc )
{
  *found = true;
  return base( expansion.base( ) )->resolveAlias( expansion.path( ).begin( ), expansion.path( ).end( ), found, loc );
}

Ast::IdentifierTree *
Ast::IdentifierTree::resolveAlias( const Ast::NamespacePath::const_iterator & begin, const Ast::NamespacePath::const_iterator & end, bool * found, const Ast::SourceLocation & loc )
{
  if( begin == end ){

    return this;

  }else{

    Ast::NamespacePath::const_iterator next(begin);
    ++next;
    typedef typeof namespaces_ MapType;
    MapType::const_iterator j = namespaces_.find( *begin );
    if( j == namespaces_.end( ) ){
      *found = false;
      IdentifierTree * tree = newChild( *begin, loc );
      return tree->resolveAlias( next, end, found, loc );
    }

    return j->second.node( )->resolveAlias( next, end, found, loc );

  }
}

void
Ast::IdentifierTree::insertEncapsulationLink( const char * key, size_t pos, const Ast::SourceLocation & loc )
{
  typedef typeof simpleIdentifiers_ MapType;
  if( simpleIdentifiers_.find( key ) != simpleIdentifiers_.end( ) ){
    Ast::theAnalysisErrorsList.push_back( new Exceptions::EncapsulationIntroducingExisting( loc, key ) );
    return;
  }
  simpleIdentifiers_.insert( MapType::value_type( key, pos ) );
}

void
Ast::IdentifierTree::insertEncapsulationLink( const char * key, Ast::IdentifierTree * tree, const Ast::SourceLocation & loc )
{
  typedef typeof namespaces_ MapType;
  if( namespaces_.find( key ) != namespaces_.end( ) ){
    Ast::theAnalysisErrorsList.push_back( new Exceptions::EncapsulationNamespaceAliasCollision( loc, key ) );
    return;
  }
  namespaces_.insert( MapType::value_type( key, NamespaceLink( NamespaceLink::SECONDARY, tree ) ) );
}

size_t
Ast::IdentifierTree::lookup( const Ast::Identifier & id ) const
{
  const Ast::SearchContext & context = id.searchContext( );

  switch( id.base( ) ){
  case Ast::NamespaceReference::ABSOLUTE:
    return lookupSuffix( false, id.path( ).begin( ), id.path( ).end( ), id.simpleId( ) );

  case Ast::NamespaceReference::LOCAL:
    return lookupInside( context.encapsulationPath( )->begin( ), context.encapsulationPath( )->end( ), id );

  case Ast::NamespaceReference::RELATIVE:
    {
      size_t pos = lookupPrefix( context.lexicalPath( )->begin( ), context.lexicalPath( )->end( ), context.privateName( ).getPtr( ), id );
      if( pos != NOT_FOUND )
        return pos;
    }
    for( RefCountPtr< const Ast::LookinPathStack > i( context.lookinStack( ) ); i != NullPtr< const Ast::LookinPathStack >( ); i = i->cdr( ) ){
      size_t pos = lookupInside( i->car( )->begin( ), i->car( )->end( ), id );
      if( pos != NOT_FOUND )
        return pos;
    }
    break;
  }

  return NOT_FOUND;
}

size_t
Ast::IdentifierTree::lookup( const Ast::PlacedIdentifier & id ) const
{
  return lookupSuffix( false, id.absolutePath( ).begin( ), id.absolutePath( ).end( ), id.simpleId( ) );
}

size_t
Ast::IdentifierTree::lookupPrivateAlias( const Ast::PlacedIdentifier & id, const char * privateName ) const
{
  /* Mimic how a relative identifier would be looked up with lookupPrefix, but only consider the private namespace.
   */
  if( privateName == NULL )
    return NOT_FOUND;

  const Ast::IdentifierTree * tree = this;
  Ast::NamespacePath::const_iterator i = id.absolutePath( ).begin( );
  Ast::NamespacePath::const_iterator end = id.absolutePath( ).end( );
  for( ; i != end; ++i ){
    typedef typeof namespaces_ MapType;
    MapType::const_iterator j = tree->namespaces_.find( *i );
    if( j == tree->namespaces_.end( ) )
      break;
    tree = j->second.node( );
  }
  if( i != end )
    return NOT_FOUND;

  typedef typeof namespaces_ MapType;
  MapType::const_iterator j = tree->namespaces_.find( privateName );
  if( j == tree->namespaces_.end( ) )
    return NOT_FOUND;

  /* We must pass an empty NamespacePath range, and id.absolutePath( ).begin( ) .. id.absolutePath( ).begin( )
   * happens to be available.
   */
  return j->second.node( )->lookupSuffix( false, id.absolutePath( ).begin( ), id.absolutePath( ).begin( ), id.simpleId( ) );
}

size_t
Ast::IdentifierTree::lookupPrefix( const Ast::NamespacePath::const_iterator & begin, const Ast::NamespacePath::const_iterator & end, const char * privateName, const Ast::Identifier & id ) const
{
  std::stack< const Ast::IdentifierTree * > path;

  const Ast::IdentifierTree * tree = this;
  path.push( tree );
  Ast::NamespacePath::const_iterator i = begin;
  for( ; i != end; ++i ){
    typedef typeof namespaces_ MapType;
    MapType::const_iterator j = tree->namespaces_.find( *i );
    if( j == tree->namespaces_.end( ) )
      break;
    tree = j->second.node( );
    path.push( tree );
  }

  /* Look in local namespace.
   * Only do this for identifiers with empty relative path, and inside
   * the current namespace.
   */
  if( i == end && id.path( ).empty( ) && privateName != NULL ){
    typedef typeof namespaces_ MapType;
    MapType::const_iterator j = tree->namespaces_.find( privateName );
    if( j != tree->namespaces_.end( ) ){

      size_t pos = j->second.node( )->lookupSuffix( false, id.path( ).begin( ), id.path( ).end( ), id.simpleId( ) );
      if( pos != NOT_FOUND )
        return pos;

    }
  }

  while( ! path.empty( ) ){
    tree = path.top( );
    path.pop( );
      size_t pos = tree->lookupSuffix( true, id.path( ).begin( ), id.path( ).end( ), id.simpleId( ) );
      if( pos != NOT_FOUND )
        return pos;
    if( tree->encapParent_ != NULL )
      break;
  }
  return NOT_FOUND;
}

size_t Ast::IdentifierTree::lookupInside( const Ast::NamespacePath::const_iterator & begin, const Ast::NamespacePath::const_iterator & end, const Ast::Identifier & id ) const
{
  if( begin == end ){
    return lookupSuffix( false, id.path( ).begin( ), id.path( ).end( ), id.simpleId( ) );
  }

  typedef typeof namespaces_ MapType;
  MapType::const_iterator i = namespaces_.find( *begin );
  if( i == namespaces_.end( ) )
    return NOT_FOUND;
  if( i->second.kind( ) != NamespaceLink::PRIMARY )
    return NOT_FOUND;

  Ast::NamespacePath::const_iterator next = begin;
  ++next;
  return i->second.node( )->lookupInside( next, end, id );
}

size_t
Ast::IdentifierTree::lookupSuffix( bool followRestricted, const Ast::NamespacePath::const_iterator & begin, const Ast::NamespacePath::const_iterator & end, const char * simpleId ) const
{
  if( begin == end ){

    typedef typeof simpleIdentifiers_ MapType;
    MapType::const_iterator i = simpleIdentifiers_.find( simpleId );
    if( i == simpleIdentifiers_.end( ) )
      return NOT_FOUND;

    return i->second;

  }else{

    const char * front = *begin;
    typedef typeof namespaces_ MapType;
    MapType::const_iterator i = namespaces_.find( front );
    if( i == namespaces_.end( ) )
      return NOT_FOUND;
    if( ! followRestricted && i->second.kind( ) == NamespaceLink::RESTRICTED )
      return NOT_FOUND;

    Ast::NamespacePath::const_iterator next(begin);
    ++next;
    return i->second.node( )->lookupSuffix( false, next, end, simpleId );

  }
}


Ast::PlacedIdentifier
Ast::IdentifierTree::reverseMap( size_t pos ) const
{
  const char * simpleId;
  std::list< const char * > * absolutePath = reverseMapHelper( pos, & simpleId );
  if( absolutePath == NULL )
    throw Exceptions::InternalError( "Ast::IdentifierTree::reverseMap failure." );
  return Ast::PlacedIdentifier( absolutePath, simpleId );
}

std::list< const char * > *
Ast::IdentifierTree::reverseMapHelper( size_t pos, const char ** simpleIdDst ) const
{
  {
    typedef typeof simpleIdentifiers_ MapType;
    for( MapType::const_iterator i = simpleIdentifiers_.begin( ); i != simpleIdentifiers_.end( ); ++i ){
      if( i->second == pos )
        {
          std::list< const char * > * result( new std::list< const char * > );
          *simpleIdDst = i->first;
          return result;
        }
    }
  }

  {
    typedef typeof namespaces_ MapType;
    for( MapType::const_iterator i = namespaces_.begin( ); i != namespaces_.end( ); ++i ){
      if( i->second.kind( ) != NamespaceLink::PRIMARY )
        continue;
      std::list< const char * > * path = i->second.node( )->reverseMapHelper( pos, simpleIdDst );
      if( path == NULL )
        continue;
      path->push_front( i->first );
      return path;
    }
  }

  return NULL;
}

void
Ast::IdentifierTree::dumpMap( std::map< Ast::PlacedIdentifier, size_t > * dst ) const
{
  Ast::NamespacePath path;
  dumpMap( dst, & path );
}

void
Ast::IdentifierTree::dumpMap( std::map< Ast::PlacedIdentifier, size_t > * dst, Ast::NamespacePath * path ) const
{
  {
    typedef typeof simpleIdentifiers_ MapType;
    typedef typeof *dst DstType;
    for( MapType::const_iterator i = simpleIdentifiers_.begin( ); i != simpleIdentifiers_.end( ); ++i ){
      dst->insert( DstType::value_type( Ast::PlacedIdentifier( *path, i->first ), i->second ) );
    }
  }

  {
    typedef typeof namespaces_ MapType;
    for( MapType::const_iterator i = namespaces_.begin( ); i != namespaces_.end( ); ++i ){
      path->push_back( i->first );
      i->second.node( )->dumpMap( dst, path );
      path->pop_back( );
    }
  }
}

void
Ast::IdentifierTree::showPath( std::ostream & os ) const
{
  typedef std::list< const char * > ListType;
  ListType path;
  if( ! root_->pushPath( this, & path ) ) {
    throw Exceptions::InternalError( "IdentifierTree::showPath failed to find namespace path." );
  }
  for (ListType::const_iterator i = path.begin( ); i != path.end( ); ++i ){
    os << Interaction::NAMESPACE_SEPARATOR << *i ;
  }
}

bool
Ast::IdentifierTree::pushPath( const Ast::IdentifierTree * tree, std::list< const char * > * dst ) const
{
  /* Special case for empty path. */
  if( tree == this )
    return true;

  typedef typeof namespaces_ MapType;
  for( MapType::const_iterator i = namespaces_.begin( ); i != namespaces_.end( ); ++i ){
    if( i->second.kind( ) != NamespaceLink::PRIMARY )
      continue;
    const Ast::IdentifierTree * node( i->second.node( ) );
    if( node == tree ){
      if( node->encapParent_ == NULL )
        dst->push_front( i->first );
      return true;
    }
    if( ! node->pushPath( tree, dst ) )
      continue;
    if( node->encapParent_ == NULL )
      dst->push_front( i->first );
    return true;
  }

  return false;
}


Ast::StateID Ast::AnalysisEnvironment::nextStateID = 1;

Ast::AnalysisEnvironment::AnalysisEnvironment( PtrOwner_back_Access< std::list< Ast::AnalysisEnvironment * > > & deleter, Ast::AnalysisEnvironment * parent, const Ast::IdentifierTree * bindings, const Ast::IdentifierTree * stateBindings )
  : parent_( parent ),
    level_( ( parent_ == NULL ) ? 0 : ( parent_->level_ + 1 ) ),
    bindings_( bindings ), dynamicKeyBindings_( NULL ),
    stateBindings_( stateBindings ), dynamicStateKeyBindings_( NULL ),
    functionBoundary_( false )
{
  deleter.push_back( this );
  updateBindings( );
}

Ast::AnalysisEnvironment::~AnalysisEnvironment( )
{ }

void
Ast::AnalysisEnvironment::updateBindings( )
{
  states_.reserve( stateBindings_->size( ) );
  while( states_.size( ) < stateBindings_->size( ) )
    {
      states_.push_back( nextStateID );
      ++nextStateID;
    }

  statesFirstUse_.resize( stateBindings_->size( ), NULL );
}

Ast::AnalysisEnvironment *
Ast::AnalysisEnvironment::getParent( ) const
{
  if( isBaseEnvironment( ) )
    {
      throw Exceptions::MiscellaneousRequirement( "Trying to find the parent of the top level analysis environment." );
    }
  return parent_;
}

void
Ast::AnalysisEnvironment::activateFunctionBoundary( )
{
  functionBoundary_ = true;
}

void
Ast::AnalysisEnvironment::setupDynamicKeyVariables( const Ast::IdentifierTree * dynamicKeyBindings )
{
  dynamicKeyBindings_ = dynamicKeyBindings;
}

void
Ast::AnalysisEnvironment::setupDynamicStateKeyVariables( const Ast::IdentifierTree * dynamicStateKeyBindings )
{
  dynamicStateKeyBindings_ = dynamicStateKeyBindings;
}

size_t
Ast::AnalysisEnvironment::findLocalVariablePosition( const Ast::SourceLocation & loc, const Ast::PlacedIdentifier & id ) const
{
  size_t pos = bindings_->lookup( id );
  if( pos == Ast::IdentifierTree::NOT_FOUND )
    {
      throw Exceptions::InternalError( loc, "Environment::findLocalPosition failed" );
    }
  return pos;
}

void
Kernel::Environment::define( size_t pos, const Kernel::VariableHandle & val )
{
  if( (*values_)[ pos ] != NullPtr< Kernel::Variable >( ) )
    {
      throw Exceptions::RedefiningLexical( bindings_->reverseMap( pos ) );
    }

  (*values_)[ pos ] = val;
}

Kernel::Environment::LexicalKey
Ast::AnalysisEnvironment::findLexicalVariableKey( const Ast::SourceLocation & loc, const Ast::Identifier & id ) const
{
  size_t pos = bindings_->lookup( id );
  if( pos == Ast::IdentifierTree::NOT_FOUND )
    {
      if( isBaseEnvironment( ) )
        {
          Ast::theAnalysisErrorsList.push_back( new Exceptions::LookupUnknown( loc, id, Ast::Identifier::VARIABLE ) );
          return Kernel::Environment::theMissingKey;
        }
      return parent_->findLexicalVariableKey( loc, id ).oneAbove( );
    }

  return LexicalKey( 0, pos );
}

Kernel::Environment::LexicalKey
Ast::AnalysisEnvironment::findPrivateAliasVariableKey( const Ast::SourceLocation & loc, const Ast::PlacedIdentifier & id, const char * privateName ) const
{
  size_t pos = bindings_->lookupPrivateAlias( id, privateName );
  if( pos == Ast::IdentifierTree::NOT_FOUND ){
    Ast::theAnalysisErrorsList.push_back( new Exceptions::LookupUnknownPrivateAlias( loc, id, Ast::Identifier::VARIABLE ) );
    return Kernel::Environment::theMissingKey;
  }

  return LexicalKey( 0, pos );
}

Ast::PlacedIdentifier
Ast::AnalysisEnvironment::reverseMapLexicalVariable( const LexicalKey & lexKey ) const
{
  const AnalysisEnvironment * env = this;
  for( size_t i = lexKey.up_; i > 0; --i )
    {
      env = env->getParent( );
    }

    return env->bindings_->reverseMap( lexKey.pos_ );
}

void
Kernel::Environment::lookup( const Kernel::Environment::LexicalKey & lexKey, Kernel::EvalState * evalState ) const
{
  const Environment * env = this;
  for( size_t i = lexKey.up_; i > 0; --i )
    {
      env = env->getParent( );
    }

  env->lookup( lexKey.pos_, evalState );
}

void
Kernel::Environment::lookup( size_t pos, Kernel::EvalState * evalState ) const
{
  Kernel::VariableHandle res = (*values_)[ pos ];
  if( res == NullPtr< Kernel::Variable >( ) )
    {
      throw Exceptions::UninitializedAccess( );
    }

  Kernel::ContRef cont = evalState->cont_;
  cont->takeHandle( res, evalState );
}

Kernel::VariableHandle
Kernel::Environment::getVarHandle( const Kernel::Environment::LexicalKey & lexKey )
{
  Environment * env = this;
  for( size_t i = lexKey.up_; i > 0; --i )
    {
      env = env->getParent( );
    }

  return env->getVarHandle( lexKey.pos_ );
}

Kernel::VariableHandle
Kernel::Environment::getVarHandle( size_t pos )
{
  return (*values_)[ pos ];
}

Kernel::Environment::LexicalKey
Ast::AnalysisEnvironment::findLexicalTypeKey( const Ast::SourceLocation & loc, const Ast::Identifier & id ) const
{
  size_t pos = bindings_->lookup( id );
  if( pos == Ast::IdentifierTree::NOT_FOUND )
    {
      if( isBaseEnvironment( ) )
        {
          Ast::theAnalysisErrorsList.push_back( new Exceptions::LookupUnknown( loc, id, Ast::Identifier::TYPE ) );
          return Kernel::Environment::theMissingKey;
        }
      return parent_->findLexicalTypeKey( loc, id ).oneAbove( );
    }

  return LexicalKey( 0, pos );
}


size_t
Ast::AnalysisEnvironment::findLocalStatePosition( const Ast::SourceLocation & loc, const Ast::PlacedIdentifier & id ) const
{
  size_t pos = stateBindings_->lookup( id );
  if( pos == Ast::IdentifierTree::NOT_FOUND )
    {
      throw Exceptions::InternalError( loc, "Environment::findLocalStatePosition failed" );
    }
  return pos;
}

Kernel::Environment::LexicalKey
Ast::AnalysisEnvironment::findLexicalStateKey( const Ast::SourceLocation & loc, const Ast::Identifier & id, const Ast::Node * node )
{
  size_t pos = stateBindings_->lookup( id );
  if( pos == Ast::IdentifierTree::NOT_FOUND )
    {
      if( isBaseEnvironment( ) )
        {
          Ast::theAnalysisErrorsList.push_back( new Exceptions::LookupUnknown( loc, id, Ast::Identifier::STATE ) );
          return Kernel::Environment::theMissingKey;
        }
      if( functionBoundary_ )
        {
          // If the state is not found at all, this will throw an error.
          parent_->findLexicalStateKey( loc, id, node ).oneAbove( );  // Ignore the result!
          // If no error is thrown, we inform the user that the state is outside a function boundary.
          Ast::theAnalysisErrorsList.push_back( new Exceptions::StateBeyondFunctionBoundary( loc, id ) );
          return Kernel::Environment::theMissingKey;
        }
      return parent_->findLexicalStateKey( loc, id, node ).oneAbove( );
    }

  if( statesFirstUse_[ pos ] == NULL ){
    statesFirstUse_[ pos ] = node;
  }

  return LexicalKey( 0, pos );
}

Ast::StateID
Ast::AnalysisEnvironment::getStateID( const LexicalKey & lexKey ) const
{
  const Ast::AnalysisEnvironment * env = this;
  for( size_t i = lexKey.up_; i > 0; --i )
    {
      env = env->getParent( );
    }

  return env->getStateID( lexKey.pos_ );
}

Ast::StateID
Ast::AnalysisEnvironment::getStateID( size_t pos ) const
{
  return states_[ pos ];
}

Ast::StateID
Ast::AnalysisEnvironment::getTheAnyStateID( )
{
  return -1;
}

const Ast::Node *
Ast::AnalysisEnvironment::getStateFirstUse( size_t pos ) const
{
  return statesFirstUse_[ pos ];
}

void
Kernel::Environment::introduceState( size_t pos, Kernel::State * state )
{
  if( (*states_)[ pos ] != NullPtr< Kernel::State >( ) )
    {
      throw Exceptions::InternalError( "Better error message needed when a state is introduced more than once." );
      //      throw Exceptions::RedefiningLexical( reverseMap( pos ) );
    }

  (*states_)[ pos ] = state;
}

void
Kernel::Environment::freeze( size_t pos, Kernel::EvalState * evalState, const Ast::SourceLocation & loc )
{
  if( (*states_)[ pos ] == NullPtr< Kernel::State >( ) )
    {
      /* This is a static inconsistency, so it should be detected before we reach here... */
      throw Exceptions::FreezingUndefined( loc, stateBindings_->reverseMap( pos ) );
    }

  Kernel::StateHandle & state = (*states_)[ pos ];

  state->freeze( evalState, loc );
}

void
Kernel::Environment::peek( const LexicalKey & lexKey, Kernel::EvalState * evalState, const Ast::SourceLocation & loc )
{
  getStateHandle( lexKey )->peek( evalState, loc );
}

void
Kernel::Environment::tackOn( const LexicalKey & lexKey, Kernel::EvalState * evalState, const RefCountPtr< const Lang::Value > & piece, const Ast::SourceLocation & callLoc )
{
  getStateHandle( lexKey )->tackOn( evalState, piece, callLoc );
}

Kernel::StateHandle
Kernel::Environment::getStateHandle( const LexicalKey & lexKey )
{
  Environment * env = this;
  for( size_t i = lexKey.up_; i > 0; --i )
    {
      env = env->getParent( );
    }

  return env->getStateHandle( lexKey.pos_ );
}

Kernel::StateHandle
Kernel::Environment::getStateHandle( size_t pos )
{
  Kernel::StateHandle res = (*states_)[ pos ];
  if( res == NullPtr< Kernel::State >( ) )
    {
      throw Exceptions::UninitializedAccess( );
    }
  return res;
}



size_t
Ast::AnalysisEnvironment::findLocalDynamicPosition( const Ast::SourceLocation & loc, const Ast::PlacedIdentifier & id ) const
{
  if( dynamicKeyBindings_ == NULL )
    {
      throw Exceptions::InternalError( "Environment::findLocalDynamicPosition called with dynamicKeyBindings_ == NULL." );
    }
  size_t pos = dynamicKeyBindings_->lookup( id );
  if( pos == Ast::IdentifierTree::NOT_FOUND )
    {
      throw Exceptions::InternalError( loc, "Environment::findLocalDynamicPosition failed" );
    }
  return pos;
}

void
Kernel::Environment::defineDynamic( const Ast::PlacedIdentifier * debugId, size_t pos, const RefCountPtr< const Lang::Function > & filter, const Kernel::VariableHandle & defaultVal )
{
  if( dynamicKeyValues_ == NULL )
    {
      throw Exceptions::InternalError( "Environment::defineDynamic called with dynamicKeyValues_ == NULL." );
    }
  if( pos > dynamicKeyValues_->size( ) )
    {
      throw Exceptions::InternalError( "Environment::defineDynamic called with pos out of range." );
    }
  if( (*dynamicKeyValues_)[ pos ] != NULL )
    {
      throw Exceptions::RedefiningDynamic( dynamicKeyBindings_->reverseMap( pos ) );
    }

  (*dynamicKeyValues_)[ pos ] = new Kernel::UserDynamicVariableProperties( debugId,
                                                                           Kernel::DynamicEnvironment::getFreshKey( ),
                                                                           filter,
                                                                           defaultVal );
}

Kernel::Environment::LexicalKey
Ast::AnalysisEnvironment::findLexicalDynamicKey( const Ast::SourceLocation & loc, const Ast::Identifier & id ) const
{
  if( dynamicKeyBindings_ == NULL )
    {
      if( isBaseEnvironment( ) )
        {
          goto error;
        }
      return parent_->findLexicalDynamicKey( loc, id ).oneAbove( );
    }
  {
    size_t pos = dynamicKeyBindings_->lookup( id );
    if( pos == Ast::IdentifierTree::NOT_FOUND )
      {
        if( isBaseEnvironment( ) )
          {
            goto error;
          }
        return parent_->findLexicalDynamicKey( loc, id ).oneAbove( );
      }

    return LexicalKey( 0, pos );
  }

 error:
  Ast::theAnalysisErrorsList.push_back( new Exceptions::LookupUnknown( loc, id, Ast::Identifier::DYNAMIC_VARIABLE ) );
  return Kernel::Environment::theMissingKey;
}

const Kernel::DynamicVariableProperties &
Kernel::Environment::lookupDynamicVariable( const LexicalKey & lexKey ) const
{
  const Environment * env = this;
  for( size_t i = lexKey.up_; i > 0; --i )
    {
      env = env->getParent( );
    }

  return env->lookupDynamicVariable( lexKey.pos_ );
}

const Kernel::DynamicVariableProperties &
Kernel::Environment::lookupDynamicVariable( size_t pos ) const
{
  const DynamicVariableProperties * res = (*dynamicKeyValues_)[ pos ];
  if( res == NULL )
    {
      throw Exceptions::UninitializedAccess( );
    }
  return *res;
}

size_t
Ast::AnalysisEnvironment::findLocalDynamicStatePosition( const Ast::SourceLocation & loc, const Ast::PlacedIdentifier & id ) const
{
  if( dynamicStateKeyBindings_ == NULL )
    {
      throw Exceptions::InternalError( "Environment::findLocalDynamicStatePosition called with dynamicStateKeyBindings_ == NULL." );
    }
  size_t pos = dynamicStateKeyBindings_->lookup( id );
  if( pos == Ast::IdentifierTree::NOT_FOUND )
    {
      throw Exceptions::InternalError( loc, "Environment::findLocalDynamicStatePosition failed" );
    }
  return pos;
}

void
Kernel::Environment::defineDynamicState( const Ast::PlacedIdentifier * debugId, size_t pos, Kernel::PassedEnv env, Kernel::PassedDyn dyn, Ast::StateReference * defaultState )
{
  if( dynamicStateKeyValues_ == NULL )
    {
      throw Exceptions::InternalError( "Environment::defineDynamicState called with dynamicStateKeyValues_ == NULL." );
    }
  if( pos > dynamicStateKeyValues_->size( ) )
    {
      throw Exceptions::InternalError( "Environment::defineDynamicState called with pos out of range." );
    }
  if( (*dynamicStateKeyValues_)[ pos ] != NULL )
    {
      throw Exceptions::RedefiningDynamic( dynamicStateKeyBindings_->reverseMap( pos ) );
    }

  (*dynamicStateKeyValues_)[ pos ] = new Kernel::UserDynamicStateProperties( debugId, env, dyn, defaultState );
}


Kernel::Environment::LexicalKey
Ast::AnalysisEnvironment::findLexicalDynamicStateKey( const Ast::SourceLocation & loc, const Ast::Identifier & id ) const
{
  if( dynamicStateKeyBindings_ == NULL )
    {
      return parent_->findLexicalDynamicStateKey( loc, id ).oneAbove( );
    }

  size_t pos = dynamicStateKeyBindings_->lookup( id );
  if( pos == Ast::IdentifierTree::NOT_FOUND )
    {
      if( isBaseEnvironment( ) )
        {
          Ast::theAnalysisErrorsList.push_back( new Exceptions::LookupUnknown( loc, id, Ast::Identifier::DYNAMIC_STATE ) );
          return Kernel::Environment::theMissingKey;
        }
      return parent_->findLexicalDynamicStateKey( loc, id ).oneAbove( );
    }

  return LexicalKey( 0, pos );
}

const Kernel::DynamicStateProperties &
Kernel::Environment::lookupDynamicState( const LexicalKey & lexKey ) const
{
  const Environment * env = this;
  for( size_t i = lexKey.up_; i > 0; --i )
    {
      env = env->getParent( );
    }

  return env->lookupDynamicState( lexKey.pos_ );
}

const Kernel::DynamicStateProperties &
Kernel::Environment::lookupDynamicState( size_t pos ) const
{
  const DynamicStateProperties * res = (*dynamicStateKeyValues_)[ pos ];
  if( res == NULL )
    {
      throw Exceptions::UninitializedAccess( );
    }
  return *res;
}


size_t
Kernel::Environment::size( ) const
{
  return bindings_->size( );
}


void
Kernel::Environment::print( std::ostream & os ) const
{
  std::set< Ast::PlacedIdentifier > shadowed;
  std::set< Ast::PlacedIdentifier > shadowedStates;
  recursivePrint( os, & shadowed, & shadowedStates );
}

size_t
Kernel::Environment::recursivePrint( std::ostream & os, std::set< Ast::PlacedIdentifier > * shadowed, std::set< Ast::PlacedIdentifier > * shadowedStates ) const
{
  typedef std::map< Ast::PlacedIdentifier, size_t > IdMap;

  std::set< Ast::PlacedIdentifier > shadowedBefore( *shadowed );
  std::set< Ast::PlacedIdentifier > shadowedStatesBefore( *shadowedStates );

  size_t depth = 0;
  if( ! isBaseEnvironment( ) )
    {
      {
        IdMap tmp;
        bindings_->dumpMap( & tmp );
        for( IdMap::const_iterator i = tmp.begin( ); i != tmp.end( ); ++i ){
          shadowed->insert( shadowed->begin( ), i->first );
        }
      }
      {
        IdMap tmp;
        stateBindings_->dumpMap( & tmp );
        for( IdMap::const_iterator i = tmp.begin( ); i != tmp.end( ); ++i ){
          shadowedStates->insert( shadowedStates->begin( ), i->first );
        }
      }
      depth = parent_->recursivePrint( os, shadowed, shadowedStates ) + 1;
    }

  std::string indentation = std::string( depth, ' ' );

  size_t width = 1;
  for( size_t tmp = bindings_->size( ); tmp >= 10; tmp /= 10, ++width )
    ;

  {
    size_t tmp = ( 29 - 4 ) - strlen( debugName_ );
    if( tmp < 2 || tmp > 29 ) /* the latter case corresponds to an underflow in the subtraction */
      {
        tmp = 2;
      }
    os << indentation << std::string( tmp / 2, '-' ) << "( " << debugName_ << " )" << std::string( tmp - tmp / 2, '-' ) << std::endl ;
  }
  if( ! isBaseEnvironment( ) )
    {
      {
        IdMap tmp;
        bindings_->dumpMap( & tmp );
        for( IdMap::const_iterator i = tmp.begin( ); i != tmp.end( ); ++i ){
          os << indentation << std::setw(width) << i->second ;
          if( shadowedBefore.find( i->first ) != shadowedBefore.end( ) )
            {
              os << "^" ;
            }
          else
            {
              os << " " ;
            }
          os << " " ;
          i->first.show( os, Ast::Identifier::VARIABLE );
          os << " : " ;
          if( (*values_)[ i->second ] == NullPtr< Kernel::Variable >( ) )
            {
              os << "< Uninitialized >" ;
            }
          else if( (*values_)[ i->second ]->isThunk( ) )
            {
              os << "< thunk >" ;
            }
          else if( dynamic_cast< const Lang::Instance * >( (*values_)[ i->second ]->getUntyped( ).getPtr( ) ) == NULL )
            {
              (*values_)[ i->second ]->getUntyped( )->show( os );
            }
          else
            {
              os << "..." ;
            }
          os << std::endl ;
        }
      }
      {
        IdMap tmp;
        stateBindings_->dumpMap( & tmp );
        for( IdMap::const_iterator i = tmp.begin( ); i != tmp.end( ); ++i ){
          os << indentation << std::setw(width) << i->second ;
          if( shadowedStatesBefore.find( i->first ) != shadowedStatesBefore.end( ) )
            {
              os << "^" ;
            }
          else
            {
              os << " " ;
            }
          os << " " ;
          i->first.show( os, Ast::Identifier::STATE );
          os << " :: " ;
          if( (*states_)[ i->second ] == NullPtr< Kernel::State >( ) )
            {
              os << "< Uninitialized >" ;
            }
          else
            {
              os << (*states_)[ i->second ]->getTypeName( ) ;
            }
          os << std::endl ;
        }
      }
    }
  else
    {
      os << indentation << "<< global env >>" << std::endl ;
    }

  os << indentation << "-- -- -- -- -- -- -- -- -- --" << std::endl ;

  return depth;
}
