/* This file is part of Shapes.
 *
 * Shapes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Shapes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2008, 2014 Henrik Tidefelt
 */

#pragma once

#include "Shapes_Ast_decls.h"
#include "Shapes_Kernel_decls.h"
#include "Shapes_Lang_decls.h"
#include "Shapes_Computation_decls.h"

#include "ptrowner.h"
#include "refcount.h"
#include "environment.h"
#include "statetypes.h"
#include "elementarylength.h"
#include "concretecolors.h"

#include <list>
#include <iostream>
#include <stack>
#include <set>


namespace Shapes
{
	namespace Lang
	{

		class ReflectionsBinding : public DynamicBindings
		{
			const Ast::DynamicBindingExpression * bindingExpr_;
			RefCountPtr< const Lang::SpecularReflection > reflections_;
			const Ast::PlacedIdentifier * id_;
		public:
			ReflectionsBinding( const Ast::PlacedIdentifier * id, const Ast::DynamicBindingExpression * bindingExpr, const RefCountPtr< const Lang::SpecularReflection > & reflections );
			virtual ~ReflectionsBinding( );
			virtual void bind( MapType & bindings, Kernel::SystemDynamicVariables ** sysBindings ) const;
			virtual void show( std::ostream & os ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
		};

		class AutoIntensityBinding : public DynamicBindings
		{
			const Ast::DynamicBindingExpression * bindingExpr_;
			RefCountPtr< const Lang::Color > color_;
			const Ast::PlacedIdentifier * id_;
		public:
			AutoIntensityBinding( const Ast::PlacedIdentifier * id, const Ast::DynamicBindingExpression * bindingExpr, const RefCountPtr< const Lang::Color > & color );
			virtual ~AutoIntensityBinding( );
			virtual void bind( MapType & bindings, Kernel::SystemDynamicVariables ** sysBindings ) const;
			virtual void show( std::ostream & os ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
		};

		class AutoScatteringBinding : public DynamicBindings
		{
			const Ast::DynamicBindingExpression * bindingExpr_;
			RefCountPtr< const Lang::SpecularReflection > reflections_;
			const Ast::PlacedIdentifier * id_;
		public:
			AutoScatteringBinding( const Ast::PlacedIdentifier * id, const Ast::DynamicBindingExpression * bindingExpr, const RefCountPtr< const Lang::SpecularReflection > & reflections );
			virtual ~AutoScatteringBinding( );
			virtual void bind( MapType & bindings, Kernel::SystemDynamicVariables ** sysBindings ) const;
			virtual void show( std::ostream & os ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
		};

		class ViewResolutionBinding : public DynamicBindings
		{
			const Ast::DynamicBindingExpression * bindingExpr_;
			Concrete::Length resolution_;
			const Ast::PlacedIdentifier * id_;
		public:
			ViewResolutionBinding( const Ast::PlacedIdentifier * id, const Ast::DynamicBindingExpression * bindingExpr, const Concrete::Length resolution );
			virtual ~ViewResolutionBinding( );
			virtual void bind( MapType & bindings, Kernel::SystemDynamicVariables ** sysBindings ) const;
			virtual void show( std::ostream & os ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
		};

		class ShadeOrderBinding : public DynamicBindings
		{
			const Ast::DynamicBindingExpression * bindingExpr_;
			Computation::FacetShadeOrder order_;
			const Ast::PlacedIdentifier * id_;
		public:
			ShadeOrderBinding( const Ast::PlacedIdentifier * id, const Ast::DynamicBindingExpression * bindingExpr, const Computation::FacetShadeOrder order );
			virtual ~ShadeOrderBinding( );
			virtual void bind( MapType & bindings, Kernel::SystemDynamicVariables ** sysBindings ) const;
			virtual void show( std::ostream & os ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
		};

	}

	namespace Kernel
	{

		class ReflectionsDynamicVariableProperties : public Kernel::DynamicVariableProperties
		{
		public:
			ReflectionsDynamicVariableProperties( const Ast::PlacedIdentifier * id );
			virtual ~ReflectionsDynamicVariableProperties( );
			virtual Kernel::VariableHandle fetch( const Kernel::PassedDyn & dyn ) const;
			virtual void makeBinding( Kernel::VariableHandle val, const Ast::DynamicBindingExpression * bindingExpr, Kernel::EvalState * evalState ) const;
		};

		class AutoIntensityDynamicVariableProperties : public Kernel::DynamicVariableProperties
		{
		public:
			AutoIntensityDynamicVariableProperties( const Ast::PlacedIdentifier * id );
			virtual ~AutoIntensityDynamicVariableProperties( );
			virtual Kernel::VariableHandle fetch( const Kernel::PassedDyn & dyn ) const;
			virtual void makeBinding( Kernel::VariableHandle val, const Ast::DynamicBindingExpression * bindingExpr, Kernel::EvalState * evalState ) const;
		};

		class AutoScatteringDynamicVariableProperties : public Kernel::DynamicVariableProperties
		{
		public:
			AutoScatteringDynamicVariableProperties( const Ast::PlacedIdentifier * id );
			virtual ~AutoScatteringDynamicVariableProperties( );
			virtual Kernel::VariableHandle fetch( const Kernel::PassedDyn & dyn ) const;
			virtual void makeBinding( Kernel::VariableHandle val, const Ast::DynamicBindingExpression * bindingExpr, Kernel::EvalState * evalState ) const;
		};

		class ViewResolutionDynamicVariableProperties : public Kernel::DynamicVariableProperties
		{
		public:
			ViewResolutionDynamicVariableProperties( const Ast::PlacedIdentifier * id );
			virtual ~ViewResolutionDynamicVariableProperties( );
			virtual Kernel::VariableHandle fetch( const Kernel::PassedDyn & dyn ) const;
			virtual void makeBinding( Kernel::VariableHandle val, const Ast::DynamicBindingExpression * bindingExpr, Kernel::EvalState * evalState ) const;
		};

		class ShadeOrderDynamicVariableProperties : public Kernel::DynamicVariableProperties
		{
		public:
			ShadeOrderDynamicVariableProperties( const Ast::PlacedIdentifier * id );
			virtual ~ShadeOrderDynamicVariableProperties( );
			virtual Kernel::VariableHandle fetch( const Kernel::PassedDyn & dyn ) const;
			virtual void makeBinding( Kernel::VariableHandle val, const Ast::DynamicBindingExpression * bindingExpr, Kernel::EvalState * evalState ) const;
		};

		class FacetState
		{
		public:
			RefCountPtr< const Lang::SpecularReflection > reflections_;		 // Use SpecularReflectionNumm for no-op, and NullPtr for undefined
			RefCountPtr< const Lang::Color > autoIntensity_;								// Use Gray( -1 ) for no-op, and NullPtr for undefined
			RefCountPtr< const Lang::SpecularReflection > autoScattering_;	// Use SpecularReflectionNumm for no-op, and NullPtr for undefined
			Concrete::Length viewResolution_;															 // Use NaN for undefined
			Computation::FacetShadeOrder shadeOrder_;											 // Use -1 for undefined
		public:
			FacetState( );
			explicit FacetState( const Kernel::FacetState & orig );	 // explicit, since reference counting shall be used in most cases
			FacetState( const Kernel::FacetState & newValues, const Kernel::FacetState & oldValues );
			FacetState( bool setDefaults );
			~FacetState( );

			void print( std::ostream & os, const std::string & indentation ) const;
		};

	}

}
