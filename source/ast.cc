/* This file is part of Shapes.
 *
 * Shapes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Shapes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2008, 2009, 2014 Henrik Tidefelt
 */

#include "ast.h"
#include "globals.h"
#include "check.h"

using namespace Shapes;
using namespace std;


void
Kernel::Continuation::takeHandle( Kernel::VariableHandle val, Kernel::EvalState * evalState, bool callingMyself ) const
{
	if( val->isThunk( ) )
		{
			val->force( val, evalState );
		}
	else if( callingMyself )
		{
			throw Exceptions::InternalError( strrefdup( "Continuation is just calling itself..." ) );
		}
	else
		{
			this->takeValue( val->getUntyped( ), evalState, true );
		}
}

void
Kernel::Continuation::takeValue( const RefCountPtr< const Lang::Value > & val, Kernel::EvalState * evalState, bool callingMyself ) const
{
	if( callingMyself )
		{
			throw Exceptions::InternalError( strrefdup( "Continuation is just calling itself..." ) );
		}
	this->takeHandle( Kernel::VariableHandle( new Kernel::Variable( val ) ), evalState, true );
}

const Ast::SourceLocation &
Kernel::Continuation::traceLoc( ) const
{
	return traceLoc_;
}

void
Kernel::Continuation::backTrace( std::ostream & os ) const
{
	typedef std::list< std::pair< const Kernel::Continuation *, RefCountPtr< const char > > > ListType;
	ListType trace;
	{
		const Kernel::Continuation * tmp = this;
		while( tmp != 0 )
			{
				trace.push_front( ListType::value_type( tmp, tmp->description( ) ) );
				tmp = tmp->up( ).getPtr( );
			}
	}
	ListType::const_iterator i = trace.begin( );
	ListType::const_iterator next = i;
	++next;
	for( ; i != trace.end( ); ++i, ++next )
		{
			if( next == trace.end( ) ||
					not i->first->traceLoc( ).contains( next->first->traceLoc( ) ) )
				{
					os << " " << i->first->traceLoc( ) << "\t" << i->second << std::endl ;
				}
		}
}

namespace Shapes
{
	namespace Kernel
	{

		class ForcedStructureContinuationHelper : public Kernel::Continuation
		{
			const ForcedStructureContinuation * cont_;	// This does not have proper memory management...
			Kernel::ContRef contMem_;	// and this is not properly down-casted.
			RefCountPtr< const Lang::Structure > structure_;
			RefCountPtr< const Lang::SingleList > lst_;
		public:
			ForcedStructureContinuationHelper( const ForcedStructureContinuation * cont, Kernel::ContRef contMem, const RefCountPtr< const Lang::Structure > & structure, const RefCountPtr< const Lang::SingleList > & lst, const Ast::SourceLocation & traceLoc )
				: Kernel::Continuation( traceLoc ), cont_( cont ), contMem_( contMem ), structure_( structure ), lst_( lst )
			{ }
			virtual ~ForcedStructureContinuationHelper( )
			{ }
			virtual void takeValue( const RefCountPtr< const Lang::Value > & val, Kernel::EvalState * evalState, bool dummy ) const
			{
				// Getting here means that some value that _we_ don't care about has been forced.

				RefCountPtr< const Lang::SingleList > firstUnforced = Kernel::ForcedStructureContinuation::findUnforced( lst_ );
				if( firstUnforced->isNull( ) )
					{
						cont_->takeStructure( structure_, evalState );
					}
				else
					{
						typedef const Lang::SingleListPair ArgType;
						RefCountPtr< ArgType > p = Helpers::down_cast< ArgType >( firstUnforced, "< internal error: SingleListPair contradicting isNull( )" );
						evalState->cont_ = Kernel::ContRef( new Kernel::ForcedStructureContinuationHelper( cont_, contMem_, structure_, p->cdr_, traceLoc_ ) );
						p->car_->force( const_cast< Kernel::VariableHandle & >( p->car_ ), evalState );
					}

			}
			virtual Kernel::ContRef up( ) const
			{
				return contMem_;
			}
			virtual RefCountPtr< const char > description( ) const
			{
				return strrefdup( "< Forcing structure >" );
			}
			virtual void gcMark( Kernel::GCMarkedSet & marked )
			{
				const_cast< Lang::Structure * >( structure_.getPtr( ) )->gcMark( marked );
				const_cast< Lang::SingleList * >( lst_.getPtr( ) )->gcMark( marked );
				contMem_->gcMark( marked );
			}
		};

	}
}


Kernel::ForcedStructureContinuation::ForcedStructureContinuation( const char * continuationName, const Ast::SourceLocation & traceLoc )
	: Kernel::Continuation( traceLoc ), continuationName_( continuationName )
{ }

Kernel::ForcedStructureContinuation::~ForcedStructureContinuation( )
{ }

void
Kernel::ForcedStructureContinuation::takeValue( const RefCountPtr< const Lang::Value > & val, Kernel::EvalState * evalState, bool dummy ) const
{
	typedef const Lang::Structure ArgType;
	RefCountPtr< ArgType > structure = Helpers::down_cast< ArgType >( val, continuationName_ );

	RefCountPtr< const Lang::SingleList > firstUnforced = findUnforced( structure->values_ );
	if( firstUnforced->isNull( ) )
		{
			this->takeStructure( structure, evalState );
		}
	else
		{
			typedef const Lang::SingleListPair ArgType;
			RefCountPtr< ArgType > p = Helpers::down_cast< ArgType >( structure->values_, "< internal error: SingleListPair contradicting isNull( )" );
			evalState->cont_ = Kernel::ContRef( new Kernel::ForcedStructureContinuationHelper( this, evalState->cont_, structure, p->cdr_, traceLoc_ ) );
			p->car_->force( const_cast< Kernel::VariableHandle & >( p->car_ ), evalState );
		}
}

RefCountPtr< const Lang::SingleList >
Kernel::ForcedStructureContinuation::findUnforced( RefCountPtr< const Lang::SingleList > lst )
{
	try
		{
			while( true )
				{
					typedef const Lang::SingleListPair ArgType;
					RefCountPtr< ArgType > p = Helpers::try_cast_CoreArgument< ArgType >( lst );
					if( p->car_->isThunk( ) )
						{
							return lst;
						}
					lst = p->cdr_;
				}
		}
	catch( const NonLocalExit::NotThisType & ball )
		{
			// This means we reached the end of the list.
		}
	return lst;
}


Ast::Node::Node( const Ast::SourceLocation & loc )
	: parent_( NULL ), analysisEnv_( NULL ), loc_( loc, bool( ) )
{ }

Ast::Node::Node( const Ast::SourceLocation & firstLoc, const Ast::SourceLocation & lastLoc )
	: parent_( NULL ), analysisEnv_( NULL ), loc_( firstLoc, lastLoc )
{ }

Ast::Node::~Node( )
{ }

void
Ast::Node::analyze( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst )
{
	setParent( parent, env );
	this->analyze_impl( parent, env, freeStatesDst );
}

void
Ast::Node::setParent( Ast::Node * parent, Ast::AnalysisEnvironment * env )
{
	CHECK( if( parent_ != NULL && parent != parent_ )
					 {
						 throw Exceptions::InternalError( "Ast::Node::setParent: parent_ has already been set." );
					 } );
	parent_ = parent;
	CHECK( if( analysisEnv_ != NULL && env != analysisEnv_ )
					 {
						 throw Exceptions::InternalError( "Ast::Node::setParent: analysisEnv_ has already been set." );
					 } );
	analysisEnv_ = env;

	if( parent != NULL ){
		parent->children_.push_back( this );
		if( parent->loc_.file_ != loc_.file_ &&
				loc_.file_->hasPosition( ) )
			{
				loc_.file_->nodes( ).push_back( this );
			}
	}
}

void
Ast::Node::changeParent( Ast::Node * parent )
{
	CHECK( if( parent_ == NULL )
					 {
						 throw Exceptions::InternalError( "Ast::Node::changeParent: parent_ hasn't been set." );
					 } );

	Ast::Node * parentLastChild = parent_->children_.back( );
	parent_->children_.pop_back( );
	if( parentLastChild != this ){
		throw Exceptions::InternalError( "Ast::Node::changeParent: Not the latest child of the old parent node." );
	}

	parent_ = parent;
	parent_->children_.push_back( this );

	/* Not sure if the loc_.file_->nodes( ) should also be updated here.
	 * Compare Ast::Node::analyze.
	 */
}

Ast::Node *
Ast::Node::parent( )
{
	return parent_;
}

Ast::Node::ChildrenType &
Ast::Node::children( )
{
	return children_;
}

Ast::AnalysisEnvironment *
Ast::Node::getEnv( )
{
	return analysisEnv_;
}

Ast::Expression *
Ast::Node::findExpressionSameFile( const Ast::SourceLocation & loc )
{
	if( loc_.file_ != loc.file_ )
		{
			return 0;
		}
	if( ! loc_.contains( loc ) )
		{
			return 0;
		}
	/* If we get here, we have a match, but we should try to find the deepest match...
	 */
	typedef typeof children_ ListType;
	for( ListType::iterator i = children_.begin( ); i != children_.end( ); ++i )
		{
			Ast::Expression * tmp = (*i)->findExpressionSameFile( loc );
			if( tmp != 0 )
				{
					return tmp;
				}
		}
	/* None of our children was a match, so then this is the deepest match.
	 * If we're not an Expression, some of our parents should be the sought expression.
	 * Returning 0 in that case will be the correct thing to do!
	 */
	return dynamic_cast< Ast::Expression * >( this );
}


Ast::Expression::Expression( const Ast::SourceLocation & loc )
	: Ast::Node( loc ), immediate_( false ), breakpoint_( 0 )
{ }

Ast::Expression::Expression( const Ast::SourceLocation & firstLoc, const Ast::SourceLocation & lastLoc )
	: Ast::Node( firstLoc, lastLoc ), immediate_( false ), breakpoint_( 0 )
{ }

Ast::Expression::~Expression( )
{ }


const Ast::SourceLocation &
Ast::Node::loc( ) const
{
	return loc_;
}


Ast::BindNode::BindNode( const Ast::SourceLocation & loc, const Ast::SourceLocation & idLoc, const Ast::PlacedIdentifier * id )
	: Ast::Node( loc ), idLoc_( idLoc, bool( ) ), id_( id )
{ }

Ast::BindNode::BindNode( const Ast::SourceLocation & firstLoc, const Ast::SourceLocation & lastLoc, const Ast::SourceLocation & idLoc, const Ast::PlacedIdentifier * id )
	: Ast::Node( firstLoc, lastLoc ), idLoc_( idLoc, bool( ) ), id_( id )
{ }

Ast::BindNode::~BindNode( )
{
	if( id_ != 0 )
		{
			delete id_;
		}
}

const Ast::PlacedIdentifier *
Ast::BindNode::id( ) const
{
	return id_;
}

const Ast::SourceLocation &
Ast::BindNode::idLoc( ) const
{
	return idLoc_;
}


Ast::Assertion::Assertion( const Ast::SourceLocation & loc )
	: Ast::Expression( loc )
{
	immediate_ = true;
}

Ast::Assertion::~Assertion( )
{ }


Ast::ErrorExpression::ErrorExpression( const Ast::SourceLocation & loc )
	: Ast::Expression( loc )
{ }

Ast::ErrorExpression::~ErrorExpression( )
{ }

void
Ast::ErrorExpression::analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst )
{
	/*
		That this is an error should have generated the appropriate messages elsewhere.
	 */
}

void
Ast::ErrorExpression::eval( Kernel::EvalState * evalState ) const
{
	throw Exceptions::InternalError( loc_, "An ErrorExpression was evaluated" );
}

