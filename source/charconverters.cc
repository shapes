/* This file is part of Shapes.
 *
 * Shapes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Shapes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2008, 2010 Henrik Tidefelt
 */

#include "strrefdup.h"
#include "charconverters.h"
#include "shapesexceptions.h"
#include "glyphlist.h"
#include "characterencoding.h"
#include "texttypes.h"


#include <iconv.h>
#include <errno.h>// How come iconv is not enough?
#include <string>
#include <fstream>
#include <sstream>



using namespace Shapes;

void
iconv_maybe_open( iconv_t * converter, const char * to_encoding, const char * from_encoding )
{
	if( *converter == (iconv_t)( -1 ) )
		{
			*converter = iconv_open( to_encoding, from_encoding );
			if( *converter == (iconv_t)( -1 ) )
				{
					std::ostringstream msg;
					msg << "iconv_open failed to create converter from " << from_encoding << " to " << to_encoding << "." ;
					throw Exceptions::ExternalError( strrefdup( msg ) );
				}
		}
}

iconv_t
Helpers::requireUTF8ToMacRomanConverter( bool cleanup )
{
	static iconv_t converter = (iconv_t)( - 1 );
	if( cleanup )
		{
			if( converter != (iconv_t)( -1 ) )
				{
					iconv_close( converter );
					converter = (iconv_t)( -1 );
				}
		}
	else
		{
			iconv_maybe_open( & converter,
												MAC_ROMAN, // This is meant to be what is called MacRoman in PDF.
												"UTF-8" );
		}
	return converter;
}

iconv_t
Helpers::requireMacRomanToUTF8Converter( bool cleanup )
{
	static iconv_t converter = (iconv_t)( - 1 );
	if( cleanup )
		{
			if( converter != (iconv_t)( -1 ) )
				{
					iconv_close( converter );
					converter = (iconv_t)( -1 );
				}
		}
	else
		{
			iconv_maybe_open( & converter,
												"UTF-8",
												MAC_ROMAN ); // This is meant to be what is called MacRoman in PDF.
		}
	return converter;
}

iconv_t
Helpers::requireUTF8ToASCIIConverter( bool cleanup )
{
	static iconv_t converter = (iconv_t)( - 1 );
	if( cleanup )
		{
			if( converter != (iconv_t)( -1 ) )
				{
					iconv_close( converter );
					converter = (iconv_t)( -1 );
				}
		}
	else
		{
			iconv_maybe_open( & converter,
												"ASCII",	// This is used for the names of glyphs in a font
												"UTF-8" );
		}
	return converter;
}

iconv_t
Helpers::requireUTF8ToUCS4Converter( bool cleanup )
{
	static iconv_t converter = (iconv_t)( - 1 );
	if( cleanup )
		{
			if( converter != (iconv_t)( -1 ) )
				{
					iconv_close( converter );
					converter = (iconv_t)( -1 );
				}
		}
	else
		{
			iconv_maybe_open( & converter,
												UCS_4_INTERNAL,
												"UTF-8" );
		}
	return converter;
}

iconv_t
Helpers::requireUCS4ToUTF8Converter( bool cleanup )
{
	static iconv_t converter = (iconv_t)( - 1 );
	if( cleanup )
		{
			if( converter != (iconv_t)( -1 ) )
				{
					iconv_close( converter );
					converter = (iconv_t)( -1 );
				}
		}
	else
		{
			iconv_maybe_open( & converter,
												"UTF-8",
												UCS_4_INTERNAL );
		}
	return converter;
}

iconv_t
Helpers::requireUCS4ToMacRomanConverter( bool cleanup )
{
	static iconv_t converter = (iconv_t)( - 1 );
	if( cleanup )
		{
			if( converter != (iconv_t)( -1 ) )
				{
					iconv_close( converter );
					converter = (iconv_t)( -1 );
				}
		}
	else
		{
			iconv_maybe_open( & converter,
												MAC_ROMAN,
												UCS_4_INTERNAL );
		}
	return converter;
}

iconv_t
Helpers::requireUTF16BEToUCS4Converter( bool cleanup )
{
	static iconv_t converter = (iconv_t)( - 1 );
	if( cleanup )
		{
			if( converter != (iconv_t)( -1 ) )
				{
					iconv_close( converter );
					converter = (iconv_t)( -1 );
				}
		}
	else
		{
			iconv_maybe_open( & converter,
												UCS_4_INTERNAL,
												"UTF-16BE" );
		}
	return converter;
}

iconv_t
Helpers::requireUCS4ToUTF16BEConverter( bool cleanup )
{
	static iconv_t converter = (iconv_t)( - 1 );
	if( cleanup )
		{
			if( converter != (iconv_t)( -1 ) )
				{
					iconv_close( converter );
					converter = (iconv_t)( -1 );
				}
		}
	else
		{
			iconv_maybe_open( & converter,
												"UTF-16BE",
												UCS_4_INTERNAL );
		}
	return converter;
}

iconv_t
Helpers::requireUTF8ToWinANSIConverter( bool cleanup )
{
	static iconv_t converter = (iconv_t)( - 1 );
	if( cleanup )
		{
			if( converter != (iconv_t)( -1 ) )
				{
					iconv_close( converter );
					converter = (iconv_t)( -1 );
				}
		}
	else
		{
			iconv_maybe_open( & converter,
												"LATIN1",	// This is meant to be what is called WinANSI in PDF.
												"UTF-8" );
		}
	return converter;
}

iconv_t
Helpers::requireUTF8ToUTF16BEConverter( bool cleanup )
{
	static iconv_t converter = (iconv_t)( - 1 );
	if( cleanup )
		{
			if( converter != (iconv_t)( -1 ) )
				{
					iconv_close( converter );
					converter = (iconv_t)( -1 );
				}
		}
	else
		{
			iconv_maybe_open( & converter,
												"UTF-16BE",
												"UTF-8" );
		}
	return converter;
}

const FontMetrics::GlyphList &
Helpers::requireGlyphList( bool cleanup )
{
	static const FontMetrics::GlyphList * converter = 0;
	if( cleanup )
		{
			if( converter != 0 )
				{
					delete converter;
					converter = 0;
				}
		}
	else
		{
			if( converter == 0 )
				{
					std::string filename = Lang::Font::searchGlyphList( );
					std::ifstream iFile( filename.c_str( ) );
					if( ! iFile.is_open( ) )
						{
							std::ostringstream oss;
							oss << "Could locate, but not open the glyph list " << filename ;
							throw Exceptions::ExternalError( strrefdup( oss ) );
						}
					try
						{
							converter = new FontMetrics::GlyphList( iFile );
						}
					catch( const char * ball )
						{
							std::ostringstream oss;
							oss << "Parsing the glyph list " << filename << " resulted in the error: " << ball ;
							throw Exceptions::ExternalError( strrefdup( oss ) );
						}
					catch( const std::string ball )
						{
							std::ostringstream oss;
							oss << "Parsing the glyph list " << filename << " resulted in the error: " << ball ;
							throw Exceptions::ExternalError( strrefdup( oss ) );
						}
					catch( const Shapes::Exceptions::Exception & ball )
						{
							std::cerr << "Parsing the glyph list " << filename << " resulted in an error.	Rethrowing." << std::endl ;
							throw;
						}
					catch( ... )
						{
							throw Exceptions::InternalError( "An unrecognized exception was caught from glyph list parsing." );
						}
				}
		}
	return *converter;
}

const FontMetrics::CharacterEncoding &
Helpers::requireMacRomanEncoding( bool cleanup )
{
	static const FontMetrics::CharacterEncoding * converter = 0;
	if( cleanup )
		{
			if( converter != 0 )
				{
					delete converter;
					converter = 0;
				}
		}
	else
		{
			if( converter == 0 )
				{
					std::string filename = Lang::Font::searchCharacterEncoding( "MacRoman" );
					std::ifstream iFile( filename.c_str( ) );
					if( ! iFile.is_open( ) )
						{
							std::ostringstream oss;
							oss << "Could locate, but not open the character encoding " << filename ;
							throw Exceptions::ExternalError( strrefdup( oss ) );
						}
					try
						{
							converter = new FontMetrics::CharacterEncoding( iFile );
						}
					catch( const char * ball )
						{
							std::ostringstream oss;
							oss << "Parsing the character encoding " << filename << " resulted in the error: " << ball ;
							throw Exceptions::ExternalError( strrefdup( oss ) );
						}
					catch( const std::string ball )
						{
							std::ostringstream oss;
							oss << "Parsing the character encoding " << filename << " resulted in the error: " << ball ;
							throw Exceptions::ExternalError( strrefdup( oss ) );
						}
					catch( const Shapes::Exceptions::Exception & ball )
						{
							std::cerr << "Parsing the character encoding " << filename << " resulted in an error.	Rethrowing." << std::endl ;
							throw;
						}
					catch( ... )
						{
							throw Exceptions::InternalError( "An unrecognized exception was caught from character encoding parsing." );
						}
				}
		}
	return *converter;
}

unsigned char
Kernel::UnicodeCodePoint::get_MacRoman( ) const
{
	static iconv_t converter = Helpers::requireUCS4ToMacRomanConverter( );

	const size_t BUF_SIZE = 1;
	char buf[ BUF_SIZE ];
	char * dst = buf;
	size_t outbytesleft = BUF_SIZE;

	const char * src = reinterpret_cast< const char * >( & value_ );
	size_t inbytesleft = sizeof( value_ );

	size_t count = iconv( converter,
												ICONV_CAST( & src ), & inbytesleft,
												& dst, & outbytesleft );
	if( count == (size_t)(-1) )
		{
			if( errno == EILSEQ )
				{
					std::ostringstream msg;
					msg << "The UCS-4 code point U+" << std::hex << value_ << " cannot be represented in MacRoman encodig." ;
					throw Exceptions::MiscellaneousRequirement( strrefdup( msg ) );
				}
			else if( errno == EINVAL )
				{
					throw Exceptions::InternalError( "Malformed UCS-4 value (in conversion to MacRoman)." );
				}
			else if( errno == E2BIG )
				{
					throw Exceptions::InternalError( "The MacRoman destination buffer was too small when encoding a single UCS-4 code point." );
				}
			else
				{
					std::ostringstream msg;
					msg << "iconv failed with an unrecognized error code: " << errno ;
					throw Exceptions::InternalError( strrefdup( msg ) );
				}
		}
	else if( inbytesleft != 0 )
		{
			throw Exceptions::InternalError( "Failed to use the entire UCS-4 code point when converting to MacRoman." );
		}
	return *reinterpret_cast< unsigned char * >( buf );
}

void
Kernel::UnicodeCodePoint::decode_UTF8( const char ** src, size_t * src_avail )
{
	static iconv_t converter = Helpers::requireUTF8ToUCS4Converter( );

	char * dst = reinterpret_cast< char * >( & value_ );
	size_t outbytesleft = sizeof( value_ );

	size_t tmp_src_avail;
	if( **src > 0 )
		{
			tmp_src_avail = 1;
		}
	else
		{
			switch( 0xF0 & **src )
				{
				case 0xE0: tmp_src_avail = 3; break;
				case 0xF0: tmp_src_avail = 4; break;
				default:   tmp_src_avail = 2; break;
				}
		}
	if( tmp_src_avail > *src_avail )
		{
			throw Exceptions::InternalError( "The UTF-8 source did not contain a complete character when initializing a single UCS-4 code point." );
		}
	*src_avail -= tmp_src_avail;

	size_t count = iconv( converter,
												ICONV_CAST( src ), & tmp_src_avail,
												& dst, & outbytesleft );
	if( count == (size_t)(-1) )
		{
			if( errno == EILSEQ )
				{
					throw Exceptions::InternalError( "Failed to initialize UCS-4 code point from UTF-8 data." );
				}
			else if( errno == EINVAL )
				{
					throw Exceptions::MiscellaneousRequirement( "Malformed UTF-8 value in initialization of UCS-4 code point." );
				}
			else if( errno == E2BIG )
				{
					throw Exceptions::InternalError( "The UTF-8 source buffer contained more than one character when initializing a single UCS-4 code point." );
				}
			else
				{
					std::ostringstream msg;
					msg << "iconv failed with an unrecognized error code: " << errno ;
					throw Exceptions::InternalError( strrefdup( msg ) );
				}
		}
	else if( outbytesleft != 0 )
		{
			throw Exceptions::InternalError( "Failed to initialize the entire UCS-4 code point when converting from UTF-8." );
		}
}

void
Kernel::UnicodeCodePoint::decode_UTF8( const char * src )
{
	static iconv_t converter = Helpers::requireUTF8ToUCS4Converter( );

	char * dst = reinterpret_cast< char * >( & value_ );
	size_t outbytesleft = sizeof( value_ );

	size_t tmp_src_avail;
	if( *src > 0 )
		{
			tmp_src_avail = 1;
		}
	else
		{
			switch( 0xF0 & *src )
				{
				case 0xE0: tmp_src_avail = 3; break;
				case 0xF0: tmp_src_avail = 4; break;
				default:   tmp_src_avail = 2; break;
				}
		}

	size_t count = iconv( converter,
												ICONV_CAST( & src ), & tmp_src_avail,
												& dst, & outbytesleft );
	if( count == (size_t)(-1) )
		{
			if( errno == EILSEQ )
				{
					throw Exceptions::InternalError( "Failed to initialize UCS-4 code point from UTF-8 data." );
				}
			else if( errno == EINVAL )
				{
					throw Exceptions::MiscellaneousRequirement( "Malformed UTF-8 value in initialization of UCS-4 code point." );
				}
			else if( errno == E2BIG )
				{
					throw Exceptions::InternalError( "The UTF-8 source buffer contained more than one character when initializing a single UCS-4 code point." );
				}
			else
				{
					std::ostringstream msg;
					msg << "iconv failed with an unrecognized error code: " << errno ;
					throw Exceptions::InternalError( strrefdup( msg ) );
				}
		}
	else if( outbytesleft != 0 )
		{
			throw Exceptions::InternalError( "Failed to initialize the entire UCS-4 code point when converting from UTF-8." );
		}
}

void
Kernel::UnicodeCodePoint::decode_UCS4( const char ** src, size_t * src_avail )
{
	if( *src_avail < 4 )
		{
			throw Exceptions::InternalError( "Not enough data available when initializing UCS-4 code point (needs four bytes)." );
		}
	memcpy( reinterpret_cast< char * >( & value_ ), *src, 4 );
	*src += 4;
	*src_avail -= 4;
}

void
Kernel::UnicodeCodePoint::decode_UCS4( const char * src )
{
	memcpy( reinterpret_cast< char * >( & value_ ), src, 4 );
}

void
Kernel::UnicodeCodePoint::encode_UTF8( char ** dst, size_t * dst_avail ) const
{
	static iconv_t converter = Helpers::requireUCS4ToUTF8Converter( );

	const char * src = reinterpret_cast< const char * >( & value_ );
	size_t inbytesleft = sizeof( value_ );

	size_t count = iconv( converter,
												ICONV_CAST( & src ), & inbytesleft,
												dst, dst_avail );
	if( count == (size_t)(-1) )
		{
			if( errno == EILSEQ )
				{
					throw Exceptions::InternalError( "Failed to convert UCS-4 code point to UTF-8." );
				}
			else if( errno == EINVAL )
				{
					throw Exceptions::InternalError( "Malformed UCS-4 value (in conversion to UTF-8)." );
				}
			else if( errno == E2BIG )
				{
					throw Exceptions::InternalError( "The UTF-8 destination buffer was too small when encoding a single UCS-4 code point." );
				}
			else
				{
					std::ostringstream msg;
					msg << "iconv failed with an unrecognized error code: " << errno ;
					throw Exceptions::InternalError( strrefdup( msg ) );
				}
		}
	else if( inbytesleft != 0 )
		{
			throw Exceptions::InternalError( "Failed to use the entire UCS-4 code point when converting to UTF-8." );
		}
}

void
Kernel::UnicodeCodePoint::encode_UTF16BE( char ** dst, size_t * dst_avail ) const
{
	static iconv_t converter = Helpers::requireUCS4ToUTF16BEConverter( );

	const char * src = reinterpret_cast< const char * >( & value_ );
	size_t inbytesleft = sizeof( value_ );

	size_t count = iconv( converter,
												ICONV_CAST( & src ), & inbytesleft,
												dst, dst_avail );
	if( count == (size_t)(-1) )
		{
			if( errno == EILSEQ )
				{
					throw Exceptions::InternalError( "Failed to convert UCS-4 code point to UTF-16-BE." );
				}
			else if( errno == EINVAL )
				{
					throw Exceptions::InternalError( "Malformed UCS-4 value (in conversion to UTF-16-BE)." );
				}
			else if( errno == E2BIG )
				{
					throw Exceptions::InternalError( "The UTF-16-BE destination buffer was too small when encoding a single UCS-4 code point." );
				}
			else
				{
					std::ostringstream msg;
					msg << "iconv failed with an unrecognized error code: " << errno ;
					throw Exceptions::InternalError( strrefdup( msg ) );
				}
		}
	else if( inbytesleft != 0 )
		{
			throw Exceptions::InternalError( "Failed to use the entire UCS-4 code point when converting to UTF-16-BE." );
		}
}

void
Kernel::UnicodeCodePoint::decode_glyph_name( const char * name )
{
	static const FontMetrics::GlyphList & glyphList = Helpers::requireGlyphList( );
	if( ! glyphList.name_to_UCS4( name, & value_ ) )
		{
			std::ostringstream msg;
			msg << "The glyph name \"" << name << "\" is not in the glyph list, and cannot be converted to a UCS-4 code point." ;
			throw Exceptions::InternalError( strrefdup( msg ) );
		}
}

Kernel::UnicodeCodePoint Kernel::UnicodeCodePoint::SPACE( 32 );
Kernel::UnicodeCodePoint Kernel::UnicodeCodePoint::NEWLINE( 10 );
