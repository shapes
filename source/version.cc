/* This file is part of Shapes.
 *
 * Shapes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Shapes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2008, 2010, 2014 Henrik Tidefelt
 */

#include "config.h"
#include "consts.h"

#include <stdio.h>

using namespace Shapes;

/* This file uses the c functions to make it compile as fast as possible,
 * so that this solution will never be considered a waste of time.
 */

void
printVersion( )
{
	printf( "Version:    " VERSION_NUMBER "\n" );
	printf( "Build date: " VERSION_DATE "\n" );
	printf( "Options:   " );
#ifdef HAVE_LIBPNG
	printf( " %s", Interaction::BUILD_REQ_LIBJPEG );
#endif
#ifdef HAVE_LIBJPEG
	printf( " %s", Interaction::BUILD_REQ_LIBPNG );
#endif
#ifdef HAVE_FT2
	printf( " %s", Interaction::BUILD_REQ_FREETYPE );
#endif
#ifdef HAVE_FONTCONFIG
	printf( " %s", Interaction::BUILD_REQ_FONTCONFIG );
#endif
#ifdef HAVE_OPENSSL
	printf( " %s", Interaction::BUILD_REQ_OPENSSL );
#endif
	printf( "\n" );
}
