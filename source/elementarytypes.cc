/* This file is part of Shapes.
 *
 * Shapes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Shapes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2008, 2013, 2014 Henrik Tidefelt
 */

#include <cmath>

#include "shapestypes.h"
#include "shapesexceptions.h"
#include "astexpr.h"
#include "consts.h"
#include "angleselect.h"
#include "astvar.h"
#include "astclass.h"
#include "elementarycoords.h"
#include "globals.h"
#include "singlelistrange.h"
#include "continuations.h"
#include "methodbase.h"

#include <ctype.h>
#include <stack>

using namespace Shapes;
using namespace std;


Lang::Symbol::NameTableType Lang::Symbol::nameTable;
Lang::Symbol::ReverseTableType Lang::Symbol::reverseTable;
int Lang::Symbol::nextUnique = -1;

/* The following global variables used to be in globals.cc, but now they are here to ensure they get initialized after the static variables in Lang::Symbol.
 */
RefCountPtr< const Lang::Symbol > Kernel::THE_NAVIGATION_SYMBOL( ".navigation" ); /* Note that the leading dot puts this symbol aside all user-symbols. */
RefCountPtr< const Lang::Symbol > Kernel::THE_ANNOTATION_SYMBOL( ".annotation" ); /* Note that the leading dot puts this symbol aside all user-symbols. */

Lang::Symbol::Symbol( )
{
	key_ = nextUnique;
	--nextUnique;
}

DISPATCHIMPL( Symbol );

Lang::Symbol::Symbol( int key )
{
	if( key != 0 )
		{
			throw Exceptions::InternalError( "Only the key 0 may be used when creating symbols with a given key." );
		}
	key_ = 0;
}

Lang::Symbol::Symbol( const char * name )
{
	NameTableType::const_iterator i = nameTable.find( name );
	if( i != nameTable.end( ) )
		{
			key_ = i->second;
		}
	else
		{
			const char * nameCopy = strdup( name );
			key_ = nameTable.size( ) + 1;
			nameTable[ nameCopy ] = key_;
			reverseTable.insert( ReverseTableType::value_type( key_, RefCountPtr< const char >( nameCopy ) ) );
		}
}

bool
Lang::Symbol::operator == ( const Symbol & other ) const
{
	return key_ == other.key_;
}

bool
Lang::Symbol::operator != ( const Symbol & other ) const
{
	return key_ != other.key_;
}

bool
Lang::Symbol::operator < ( const Symbol & other ) const
{
	return key_ < other.key_;
}

bool
Lang::Symbol::operator > ( const Symbol & other ) const
{
	return key_ > other.key_;
}

bool
Lang::Symbol::operator <= ( const Symbol & other ) const
{
	return key_ <= other.key_;
}

bool
Lang::Symbol::operator >= ( const Symbol & other ) const
{
	return key_ >= other.key_;
}

bool
Lang::Symbol::isUnique( ) const
{
	return key_ < 0;
}

RefCountPtr< const char >
Lang::Symbol::name( ) const
{
	if( key_ > 0 )
		{
			ReverseTableType::const_iterator i = reverseTable.find( key_ );
			if( i == reverseTable.end( ) )
				{
					throw Exceptions::InternalError( "The reverse symbol table did not include the sought key." );
				}
			return i->second;
		}
	else if( key_ < 0 )
		{
			return strrefdup( "<unique>" );
		}
	return strrefdup( "<dummy>" );
}

RefCountPtr< const char >
Lang::Symbol::nameFromKey( KeyType key )
{
	if( key <= 0 )
		{
			throw Exceptions::MiscellaneousRequirement( "It is forbidden to ask for the name of a unique symbol." );
		}
	ReverseTableType::const_iterator i = reverseTable.find( key );
	if( i == reverseTable.end( ) )
		{
			throw Exceptions::InternalError( "The reverse symbol table did not include the sought key." );
		}
	return i->second;
}


RefCountPtr< const Lang::Class > Lang::Symbol::TypeID( new Lang::SystemFinalClass( strrefdup( "Symbol" ) ) );
TYPEINFOIMPL( Symbol );

void
Lang::Symbol::show( std::ostream & os ) const
{
	if( isUnique( ) )
		{
			os << "'<unique>" ;
		}
	else
		{
			os << "'" << name( ) ;
		}
}


DISPATCHIMPL( Float );

RefCountPtr< const Lang::Class > Lang::Float::TypeID( new Lang::SystemFinalClass( strrefdup( "Float" ) ) );
TYPEINFOIMPL( Float );

void
Lang::Float::show( std::ostream & os ) const
{
	if( val_ >= 0.0 ){
		os << val_ ;
	}else{
		os << "~" << -val_ ;
	}
}


DISPATCHIMPL( Integer );

RefCountPtr< const Lang::Class > Lang::Integer::TypeID( new Lang::SystemFinalClass( strrefdup( "Integer" ) ) );
TYPEINFOIMPL( Integer );

bool
Lang::Integer::operator == ( const Integer & other ) const
{
	return val_ == other.val_;
}

bool
Lang::Integer::operator != ( const Integer & other ) const
{
	return val_ != other.val_;
}

bool
Lang::Integer::operator < ( const Integer & other ) const
{
	return val_ < other.val_;
}

bool
Lang::Integer::operator > ( const Integer & other ) const
{
	return val_ > other.val_;
}

bool
Lang::Integer::operator <= ( const Integer & other ) const
{
	return val_ <= other.val_;
}

bool
Lang::Integer::operator >= ( const Integer & other ) const
{
	return val_ >= other.val_;
}

Kernel::VariableHandle
Lang::Integer::getField( const char * fieldID, const RefCountPtr< const Lang::Value > & selfRef ) const
{
	if( strcmp( fieldID, "hex" ) == 0 )
		{
			std::ostringstream oss;
			ValueType tmp = val_;
			if( tmp < 0 )
				{
					oss << "~" ;
					tmp = -tmp;
				}
			oss << std::hex << tmp ;
			return Helpers::newValHandle( new Lang::String( strrefdup( oss ) ) );
		}
	if( strcmp( fieldID, "oct" ) == 0 )
		{
			std::ostringstream oss;
			ValueType tmp = val_;
			if( tmp < 0 )
				{
					oss << "~" ;
					tmp = -tmp;
				}
			oss << std::oct << tmp ;
			return Helpers::newValHandle( new Lang::String( strrefdup( oss ) ) );
		}
	if( strcmp( fieldID, "bin" ) == 0 )
		{
			std::ostringstream oss;
			ValueType tmp = val_;
			if( tmp < 0 )
				{
					oss << "~" ;
					tmp = -tmp;
				}
			char buf[ 8 * sizeof( ValueType ) ]; /* Since ValueType is signed, there is enough memory to hold the terminating null character. */
			char * dst = buf;
			ValueType twopow = 1;
			while( twopow < tmp )
				{
					twopow *= 2;
				}
			for( ; twopow > 0; twopow /= 2, ++dst )
				{
					if( tmp >= twopow )
						{
							*dst = '1';
							tmp -= twopow;
						}
					else
						{
							*dst = '0';
						}
				}
			*dst = '\0';
			oss << buf ;
			return Helpers::newValHandle( new Lang::String( strrefdup( oss ) ) );
		}
	if( strcmp( fieldID, "bits_bin" ) == 0 )
		{
			std::ostringstream oss;
			ValueType_unsigned tmp = *reinterpret_cast< const ValueType_unsigned * >( & val_ );
			char buf[ 8 * sizeof( ValueType ) + 1];
			char * dst = buf + 8 * sizeof( ValueType );
			*dst = '\0';
			for( ; dst > buf; )
				{
					--dst;
					*dst = ( ( tmp % 2 ) != 0 ) ? '1': '0';
					tmp /= 2;
				}
			oss << buf ;
			return Helpers::newValHandle( new Lang::String( strrefdup( oss ) ) );
		}
	if( strcmp( fieldID, "bits_hex" ) == 0 )
		{
			std::ostringstream oss;
			ValueType_unsigned tmp = *reinterpret_cast< const ValueType_unsigned * >( & val_ );
			char buf[ 2 * sizeof( ValueType ) + 1];
			char * dst = buf + 2 * sizeof( ValueType );
			*dst = '\0';
			for( ; dst > buf; )
				{
					--dst;
					unsigned char d = tmp % 0x10;
					if( d < 10 )
						{
							*dst = '0' + d;
						}
					else
						{
							*dst = 'A' + ( d - 10 );
						}
					tmp /= 0x10;
				}
			oss << buf ;
			return Helpers::newValHandle( new Lang::String( strrefdup( oss ) ) );
		}
	if( strcmp( fieldID, "dec" ) == 0 )
		{
			std::ostringstream oss;
			ValueType tmp = val_;
			if( tmp < 0 )
				{
					oss << "~" ;
					tmp = -tmp;
				}
			oss << std::dec << tmp ;
			return Helpers::newValHandle( new Lang::String( strrefdup( oss ) ) );
		}

	throw Exceptions::NonExistentMember( getTypeName( ), fieldID );
}

void
Lang::Integer::show( std::ostream & os ) const
{
	if( val_ >= 0 ){
		os << "'" << val_ ;
	}else{
		os << "'~" << -val_ ;
	}
}

DISPATCHIMPL( Character );

RefCountPtr< const Lang::Class > Lang::Character::TypeID( new Lang::SystemFinalClass( strrefdup( "Character" ) ) );
TYPEINFOIMPL( Character );

void
Lang::Character::show( std::ostream & os ) const
{
	const size_t bufSize = 8;
	char buf[ bufSize ];
	char * dst = buf;
	size_t out_avail = bufSize - 1;
	val_.encode_UTF8( & dst, & out_avail );
	*dst = '\0';
	os << buf ;
}

Kernel::VariableHandle
Lang::Character::getField( const char * fieldID, const RefCountPtr< const Lang::Value > & selfRef ) const
{
	if( strcmp( fieldID, "code" ) == 0 )
		{
			return Helpers::newValHandle( new Lang::Integer( val_.get_UCS4( ) ) );
		}

	throw Exceptions::NonExistentMember( getTypeName( ), fieldID );
}


//Lang::Length::Length( double val )
//	: isOffset_( false ), val_( val )
//{ }
//
//Lang::Length::Length( bool isOffset, double val )
//	: isOffset_( _isOffset ), val_( _val )
//{ }

DISPATCHIMPL( Length );

Concrete::Length
Lang::Length::get( Concrete::Length baseLength ) const
{
	if( isOffset_ )
		{
			return baseLength + val_;
		}
	return val_;
}

Concrete::Length
Lang::Length::get( ) const
{
	if( isOffset_ )
		{
			throw Exceptions::MiscellaneousRequirement( "Offset lengths are not allowed here." );
		}
	return val_;
}

double
Lang::Length::getScalar( Concrete::Length baseLength ) const
{
	if( isOffset_ )
		{
			return ( baseLength + val_ ).offtype< 1, 0 >( );
		}
	return val_.offtype< 1, 0 >( );
}

double
Lang::Length::getScalar( ) const
{
	if( isOffset_ )
		{
			throw Exceptions::MiscellaneousRequirement( "Offset lengths are not allowed here." );
		}
	return val_.offtype< 1, 0 >( );
}

Lang::Length
Lang::Length::operator + ( const Lang::Length & term ) const
{
	if( term.isOffset_ )
		{
			throw Exceptions::MiscellaneousRequirement( "The right term in a length addition must not be offset." );
		}
	return Lang::Length( isOffset_, val_ + term.val_ );
}

Lang::Length
Lang::Length::operator - ( const Lang::Length & term ) const
{
	if( term.isOffset_ )
		{
			throw Exceptions::MiscellaneousRequirement( "The right term in a length subtraction must not be offset." );
		}
	return Lang::Length( isOffset_, val_ - term.val_ );
}

RefCountPtr< const Lang::Class > Lang::Length::TypeID( new Lang::SystemFinalClass( strrefdup( "Length" ) ) );
TYPEINFOIMPL( Length );

void
Lang::Length::show( std::ostream & os ) const
{
	os << *this ;
}


std::ostream &
Lang::operator << ( std::ostream & os, const Lang::Length & self )
{
	if( self.isOffset_ )
		{
			os << "(+" ;
		}
	double lengthVal = static_cast< double >( self.val_ / Interaction::displayUnit );
	if( lengthVal >= 0 ){
		os << lengthVal ;
	}else{
		os << "~" << -lengthVal ;
	}
	os << Interaction::displayUnitName ;
	if( self.isOffset_ )
		{
			os << ")" ;
		}
	return os;
}


DISPATCHIMPL( Boolean );

RefCountPtr< const Lang::Class > Lang::Boolean::TypeID( new Lang::SystemFinalClass( strrefdup( "Boolean" ) ) );
TYPEINFOIMPL( Boolean );

void
Lang::Boolean::show( std::ostream & os ) const
{
	if( val_ )
		{
			os << "true" ;
		}
	else
		{
			os << "false" ;
		}
}


namespace Shapes
{

	namespace Lang
	{

		class StringMethod_byteselect : public Lang::MethodBase< Lang::String >
		{
		public:
			StringMethod_byteselect( RefCountPtr< const Lang::String > _self, const Ast::FileID * fullMethodID );
			virtual ~StringMethod_byteselect( );
			virtual void call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const;
			static const char * staticFieldID( ) { return "byteselect"; }
		};

		class StringMethod_UTF8select : public Lang::MethodBase< Lang::String >
		{
		public:
			StringMethod_UTF8select( RefCountPtr< const Lang::String > _self, const Ast::FileID * fullMethodID );
			virtual ~StringMethod_UTF8select( );
			virtual void call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const;
			static const char * staticFieldID( ) { return "UTF8select"; }
		};

	}
}

void
String_register_methods( Lang::SystemFinalClass * dstClass )
{
	dstClass->registerMethod( new Kernel::MethodFactory< Lang::String, Lang::StringMethod_byteselect >( ) );
	dstClass->registerMethod( new Kernel::MethodFactory< Lang::String, Lang::StringMethod_UTF8select >( ) );
}

Lang::String::~String( )
{ }

DISPATCHIMPL( String );

RefCountPtr< const Lang::Class > Lang::String::TypeID( new Lang::SystemFinalClass( strrefdup( "String" ), String_register_methods ) );
TYPEINFOIMPL( String );

Kernel::VariableHandle
Lang::String::getField( const char * fieldID, const RefCountPtr< const Lang::Value > & selfRef ) const
{
	if( strcmp( fieldID, "bytecount" ) == 0 )
		{
			return Helpers::newValHandle( new Lang::Integer( bytecount_ ) );
		}
	if( strcmp( fieldID, "UTF8count" ) == 0 )
		{
			return Helpers::newValHandle( new Lang::Integer( UTF8count( ) ) );
		}
	if( strcmp( fieldID, "UTF8?" ) == 0 )
		{
			const unsigned char * src = reinterpret_cast< const unsigned char * >( val_.getPtr( ) );
			const unsigned char * end = src + bytecount_;
			bool res = true;
			for( ; src < end; ++src )
				{
					if( ( ( *src ^ 0x00 ) & 0x80 ) == 0 )
						{
							continue;
						}
					if( ( ( *src ^ 0xC0 ) & 0xE0 ) == 0 )
						{
							++src;
							if( ( ( *src ^ 0x80 ) & 0xC0 ) != 0 )
								{
									res = false;
									break;
								}
							continue;
						}
					if( ( ( *src ^ 0xE0 ) & 0xF0 ) == 0 )
						{
							++src;
							if( ( ( *src ^ 0x80 ) & 0xC0 ) != 0 )
								{
									res = false;
									break;
								}
							++src;
							if( ( ( *src ^ 0x80 ) & 0xC0 ) != 0 )
								{
									res = false;
									break;
								}
							continue;
						}
					if( ( ( *src ^ 0xE0 ) & 0xF8 ) == 0 )
						{
							++src;
							if( ( ( *src ^ 0x80 ) & 0xC0 ) != 0 )
								{
									res = false;
									break;
								}
							++src;
							if( ( ( *src ^ 0x80 ) & 0xC0 ) != 0 )
								{
									res = false;
									break;
								}
							++src;
							if( ( ( *src ^ 0x80 ) & 0xC0 ) != 0 )
								{
									res = false;
									break;
								}
							continue;
						}
					res = false;
					break;
				}
			if( src != end || *src != '\0' )
				{
					res = false;
				}
			return Helpers::newValHandle( new Lang::Boolean( res ) );
		}

	return TypeID->getMethod( selfRef, fieldID ); /* This will throw if there is no such method. */
}

size_t
Lang::String::UTF8count( ) const
{
	const char * src = val_.getPtr( );
	const char * end = src + bytecount_;

	const char * ASCIIstart = src;
	size_t count = 0;

	while( *src > 0 )
		{
		ASCII:
			++src;
		}

	count += src - ASCIIstart;
	while( *src != 0 && src < end )
		{
			if( *src > 0 )
				{
					ASCIIstart = src;
					goto ASCII;
				}
			else
				{
					switch( 0xF0 & *src )
						{
						case 0xE0: src += 3; break;
						case 0xF0: src += 4; break;
						default:   src += 2; break;
						}
				}
			++count;
		}
	if( *src != 0 || src != end )
		{
			throw Exceptions::MiscellaneousRequirement( "The string is not valid UTF-8, so the number of UTF-8 characters could not be counted." );
		}

	return count;
}

void
Lang::String::show( std::ostream & os ) const
{
	os << val_.getPtr( ) ;
}


Lang::Continuation::Continuation( const Kernel::ContRef & cont )
	: cont_( cont )
{ }

Lang::Continuation::~Continuation( )
{ }

RefCountPtr< const Lang::Class > Lang::Continuation::TypeID( new Lang::SystemFinalClass( strrefdup( "Backtrace" ) ) );
TYPEINFOIMPL( Continuation );

Kernel::VariableHandle
Lang::Continuation::getField( const char * fieldID, const RefCountPtr< const Lang::Value > & selfRef ) const
{
	if( strcmp( fieldID, "up" ) == 0 )
		{
			Kernel::ContRef res = cont_->up( );
			if( res == NullPtr< Kernel::Continuation >( ) )
				{
					throw Exceptions::MiscellaneousRequirement( "The top continuation has no parent." );
				}
			return Helpers::newValHandle( new Lang::Continuation( res ) );
		}
	else if( strcmp( fieldID, "top?" ) == 0 )
		{
			Kernel::ContRef res = cont_->up( );
			return ( res == NullPtr< Kernel::Continuation >( ) ) ? Kernel::THE_TRUE_VARIABLE : Kernel::THE_FALSE_VARIABLE;
		}
	throw Exceptions::NonExistentMember( getTypeName( ), fieldID );
}

void
Lang::Continuation::show( std::ostream & os ) const
{
	os << "( backtrace to use in error messages )" ;
}

void
Lang::Continuation::gcMark( Kernel::GCMarkedSet & marked )
{
	cont_->gcMark( marked );
}


Lang::Exception::Exception( const Ast::SourceLocation & loc, const RefCountPtr< const Lang::Symbol > & kind, const RefCountPtr< const Lang::String > & source, const RefCountPtr< const Lang::Value > & details, const RefCountPtr< const char > & message, const Kernel::ContRef & cont, Interaction::ExitCode exitCode )
	: loc_( loc ), kind_( kind ), source_( source ), details_( details ), message_( message ), cont_( cont ), exitCode_( exitCode )
{ }

Lang::Exception::Exception( const RefCountPtr< const Lang::Symbol > & kind, const RefCountPtr< const Lang::String > & source, const RefCountPtr< const Lang::Value > & details, const RefCountPtr< const char > & message, const Kernel::ContRef & cont, Interaction::ExitCode exitCode )
	: loc_( cont->traceLoc( ) ), kind_( kind ), source_( source ), details_( details ), message_( message ), cont_( cont ), exitCode_( exitCode )
{ }

Lang::Exception::~Exception( )
{ }

RefCountPtr< const Lang::Class > Lang::Exception::TypeID( new Lang::SystemFinalClass( strrefdup( "Exception" ) ) );
TYPEINFOIMPL( Exception );

Kernel::VariableHandle
Lang::Exception::getField( const char * fieldID, const RefCountPtr< const Lang::Value > & selfRef ) const
{
	if( strcmp( fieldID, "kind" ) == 0 )
		{
			return Kernel::VariableHandle( new Kernel::Variable( kind_ ) );
		}
	else if( strcmp( fieldID, "source" ) == 0 )
		{
			return Kernel::VariableHandle( new Kernel::Variable( source_ ) );
		}
	else if( strcmp( fieldID, "details" ) == 0 )
		{
			return Kernel::VariableHandle( new Kernel::Variable( details_ ) );
		}
	else if( strcmp( fieldID, "backtrace" ) == 0 )
		{
			return Helpers::newValHandle( new Lang::Continuation( cont_ ) );
		}
	throw Exceptions::NonExistentMember( getTypeName( ), fieldID );
}

void
Lang::Exception::show( std::ostream & os ) const
{
	os << message_ ;
}

void
Lang::Exception::gcMark( Kernel::GCMarkedSet & marked )
{
	const_cast< Lang::Value * >( details_.getPtr( ) )->gcMark( marked );
	const_cast< Kernel::Continuation * >( cont_.getPtr( ) )->gcMark( marked );
}


Lang::FloatPair::FloatPair( const Concrete::UnitFloatPair & orig )
	: x_( orig.x_ ), y_( orig.y_ )
{ }


DISPATCHIMPL( FloatPair );

Kernel::VariableHandle
Lang::FloatPair::getField( const char * fieldID, const RefCountPtr< const Lang::Value > & selfRef ) const
{
	if( strcmp( fieldID, "x" ) == 0 )
		{
			return Helpers::newValHandle( new Lang::Float( x_ ) );
		}
	if( strcmp( fieldID, "y" ) == 0 )
		{
			return Helpers::newValHandle( new Lang::Float( y_ ) );
		}
	throw Exceptions::NonExistentMember( getTypeName( ), fieldID );
}

RefCountPtr< const Lang::Class > Lang::FloatPair::TypeID( new Lang::SystemFinalClass( strrefdup( "FloatPair" ) ) );
TYPEINFOIMPL( FloatPair );

RefCountPtr< const Lang::FloatPair >
Lang::FloatPair::transformed( const Lang::Transform2D & tf ) const
{
	return RefCountPtr< const Lang::FloatPair >
		( new Lang::FloatPair( tf.xx_ * x_ + tf.xy_ * y_, tf.yx_ * x_ + tf.yy_ * y_ ) );
}

void
Lang::FloatPair::show( std::ostream & os ) const
{
	os << "( " << x_ << ", " << y_ << " )" ;
}

Lang::FloatTriple::FloatTriple( const Concrete::UnitFloatTriple & orig )
	: x_( orig.x_ ), y_( orig.y_ ), z_( orig.z_ )
{ }

DISPATCHIMPL( FloatTriple );

Kernel::VariableHandle
Lang::FloatTriple::getField( const char * fieldID, const RefCountPtr< const Lang::Value > & selfRef ) const
{
	if( strcmp( fieldID, "x" ) == 0 )
		{
			return Helpers::newValHandle( new Lang::Float( x_ ) );
		}
	if( strcmp( fieldID, "y" ) == 0 )
		{
			return Helpers::newValHandle( new Lang::Float( y_ ) );
		}
	if( strcmp( fieldID, "z" ) == 0 )
		{
			return Helpers::newValHandle( new Lang::Float( z_ ) );
		}
	throw Exceptions::NonExistentMember( getTypeName( ), fieldID );
}

RefCountPtr< const Lang::Class > Lang::FloatTriple::TypeID( new Lang::SystemFinalClass( strrefdup( "FloatTriple" ) ) );
TYPEINFOIMPL( FloatTriple );

RefCountPtr< const Lang::FloatTriple >
Lang::FloatTriple::transformed( const Lang::Transform3D & tf ) const
{
	return RefCountPtr< const Lang::FloatTriple >
		( new Lang::FloatTriple( tf.xx_ * x_ + tf.xy_ * y_ + tf.xz_ * z_,
														 tf.yx_ * x_ + tf.yy_ * y_ + tf.yz_ * z_,
														 tf.zx_ * x_ + tf.zy_ * y_ + tf.zz_ * z_ ) );
}

void
Lang::FloatTriple::show( std::ostream & os ) const
{
	os << "( " << x_ << ", " << y_ << ", " << z_ << " )" ;
}


Lang::Coords2D::Coords2D( const Lang::Coords2D & orig )
	: x_( orig.x_ ), y_( orig.y_ )
{ }

Lang::Coords2D::Coords2D( const Lang::Length & x, const Lang::Length & y )
	: x_( x ), y_( y )
{ }

Lang::Coords2D::Coords2D( const Concrete::Length & x, const Concrete::Length & y )
	: x_( x ), y_( y )
{ }

DISPATCHIMPL( Coords2D );

Kernel::VariableHandle
Lang::Coords2D::getField( const char * fieldID, const RefCountPtr< const Lang::Value > & selfRef ) const
{
	if( strcmp( fieldID, "x" ) == 0 )
		{
			return Helpers::newValHandle( new Lang::Length( x_ ) );
		}
	if( strcmp( fieldID, "y" ) == 0 )
		{
			return Helpers::newValHandle( new Lang::Length( y_ ) );
		}
	throw Exceptions::NonExistentMember( getTypeName( ), fieldID );
}

Lang::Coords2D *
Lang::Coords2D::transformedPtr( const Lang::Transform2D & tf ) const
{
	Concrete::Length tmpx = x_.get( );
	Concrete::Length tmpy = y_.get( );
	return new Lang::Coords2D( tf.xx_ * tmpx + tf.xy_ * tmpy + tf.xt_, tf.yx_ * tmpx + tf.yy_ * tmpy + tf.yt_ );
}

RefCountPtr< const Lang::Geometric2D >
Lang::Coords2D::transformed( const Lang::Transform2D & tf, const RefCountPtr< const Lang::Geometric2D > & self ) const
{
	return RefCountPtr< const Lang::Geometric2D >( transformedPtr( tf ) );
}

RefCountPtr< const Lang::Geometric3D >
Lang::Coords2D::to3D( const RefCountPtr< const Lang::Geometric2D > & self ) const
{
	return RefCountPtr< const Lang::Coords3D >( new Lang::Coords3D( x_, y_, Lang::Length( Concrete::Length( 0 ) ) ) );
}

RefCountPtr< const Lang::Class > Lang::Coords2D::TypeID( new Lang::SystemFinalClass( strrefdup( "Coords2D" ) ) );
TYPEINFOIMPL( Coords2D );

void
Lang::Coords2D::show( std::ostream & os ) const
{
	os << "( " << x_ << ", " << y_ << " )" ;
}

std::ostream &
Lang::operator << ( std::ostream & os, const Lang::Coords2D & self )
{
	os << "( " << self.x_ << ", " << self.y_ << " )" ;
	return os;
}


Lang::CornerCoords2D::CornerCoords2D( const Lang::Length & x, const Lang::Length & y, double a )
	: Lang::Coords2D( x, y ), a_( a )
{ }

Lang::CornerCoords2D::CornerCoords2D( const Concrete::Length & x, const Concrete::Length & y, double a )
	: Lang::Coords2D( x, y ), a_( a )
{ }

RefCountPtr< const Lang::Class > Lang::CornerCoords2D::TypeID( new Lang::SystemFinalClass( strrefdup( "CornerCoords2D" ) ) );
DISPATCHIMPL( CornerCoords2D );

Kernel::VariableHandle
Lang::CornerCoords2D::getField( const char * fieldID, const RefCountPtr< const Lang::Value > & selfRef ) const
{
	if( strcmp( fieldID, "x" ) == 0 )
		{
			return Helpers::newValHandle( new Lang::Length( x_ ) );
		}
	if( strcmp( fieldID, "y" ) == 0 )
		{
			return Helpers::newValHandle( new Lang::Length( y_ ) );
		}
	if( strcmp( fieldID, "a" ) == 0 )
		{
			return Helpers::newValHandle( new Lang::Float( a_ ) );
		}
	throw Exceptions::NonExistentMember( getTypeName( ), fieldID );
}

Lang::CornerCoords2D *
Lang::CornerCoords2D::transformedPtr( const Lang::Transform2D & tf ) const
{
	Concrete::Length tmpx = x_.get( );
	Concrete::Length tmpy = y_.get( );
	return new Lang::CornerCoords2D( tf.xx_ * tmpx + tf.xy_ * tmpy + tf.xt_, tf.yx_ * tmpx + tf.yy_ * tmpy + tf.yt_, a_ );
}

RefCountPtr< const Lang::Geometric2D >
Lang::CornerCoords2D::transformed( const Lang::Transform2D & tf, const RefCountPtr< const Lang::Geometric2D > & self ) const
{
	return RefCountPtr< const Lang::Geometric2D >( transformedPtr( tf ) );
}

RefCountPtr< const Lang::Geometric3D >
Lang::CornerCoords2D::to3D( const RefCountPtr< const Lang::Geometric2D > & self ) const
{
	throw Exceptions::MiscellaneousRequirement( "Corner coordinates cannot move into 3D space." );
}

TYPEINFOIMPL( CornerCoords2D );


Lang::Coords3D::Coords3D( const Lang::Coords3D & orig )
	: x_( orig.x_ ), y_( orig.y_ ), z_( orig.z_ )
{ }

Lang::Coords3D::Coords3D( const Lang::Length & x, const Lang::Length & y, const Lang::Length & z )
	: x_( x ), y_( y ), z_( z )
{ }

Lang::Coords3D::Coords3D( const Concrete::Length & x, const Concrete::Length & y, const Concrete::Length & z )
	: x_( x ), y_( y ), z_( z )
{ }

DISPATCHIMPL( Coords3D );

Kernel::VariableHandle
Lang::Coords3D::getField( const char * fieldID, const RefCountPtr< const Lang::Value > & selfRef ) const
{
	if( strcmp( fieldID, "x" ) == 0 )
		{
			return Helpers::newValHandle( new Lang::Length( x_ ) );
		}
	if( strcmp( fieldID, "y" ) == 0 )
		{
			return Helpers::newValHandle( new Lang::Length( y_ ) );
		}
	if( strcmp( fieldID, "z" ) == 0 )
		{
			return Helpers::newValHandle( new Lang::Length( z_ ) );
		}
	throw Exceptions::NonExistentMember( getTypeName( ), fieldID );
}

Lang::Coords3D *
Lang::Coords3D::transformedPtr( const Lang::Transform3D & tf ) const
{
	Concrete::Length tmpx = x_.get( );
	Concrete::Length tmpy = y_.get( );
	Concrete::Length tmpz = z_.get( );
	return new Lang::Coords3D( tf.xx_ * tmpx + tf.xy_ * tmpy + tf.xz_ * tmpz + tf.xt_,
														 tf.yx_ * tmpx + tf.yy_ * tmpy + tf.yz_ * tmpz + tf.yt_,
														 tf.zx_ * tmpx + tf.zy_ * tmpy + tf.zz_ * tmpz + tf.zt_ );
}

RefCountPtr< const Lang::Geometric3D >
Lang::Coords3D::transformed( const Lang::Transform3D & tf, const RefCountPtr< const Lang::Geometric3D > & self ) const
{
	return RefCountPtr< const Lang::Geometric3D >( transformedPtr( tf ) );
}

RefCountPtr< const Lang::Coords2D >
Lang::Coords3D::make2D( Concrete::Length eyez ) const
{
	if( eyez < Concrete::HUGE_LENGTH )
		{
			return RefCountPtr< const Lang::Coords2D >( new Lang::Coords2D( x_.get( ) * ( eyez / ( eyez - z_.get( ) ) ),
																																			y_.get( ) * ( eyez / ( eyez - z_.get( ) ) ) ) );
		}
	return RefCountPtr< const Lang::Coords2D >( new Lang::Coords2D( x_.get( ),
																																	y_.get( ) ) );
}


RefCountPtr< const Lang::Geometric2D >
Lang::Coords3D::to2D( const Kernel::PassedDyn & dyn, const RefCountPtr< const Lang::Geometric3D > & self ) const
{
	return make2D( dyn->getEyeZ( ) );
}

RefCountPtr< const Lang::Class > Lang::Coords3D::TypeID( new Lang::SystemFinalClass( strrefdup( "Coords3D" ) ) );
TYPEINFOIMPL( Coords3D );

void
Lang::Coords3D::show( std::ostream & os ) const
{
	os << "( " << x_ << ", " << y_ << ", " << z_ << " )" ;
}

std::ostream &
Lang::operator << ( std::ostream & os, const Lang::Coords3D & self )
{
	os << "( " << self.x_ << ", " << self.y_ << ", " << self.z_ << " )" ;
	return os;
}


namespace Shapes
{
	namespace Kernel
	{
		class StringMethod_select_cont : public Kernel::Continuation
		{
			RefCountPtr< const Lang::String > self_;
			bool UTF8select_;
			Kernel::ContRef cont_;
		public:
			StringMethod_select_cont( const RefCountPtr< const Lang::String > & self, bool UTF8select, Kernel::ContRef cont, const Ast::SourceLocation & traceLoc )
				: Kernel::Continuation( traceLoc ), self_( self ), UTF8select_( UTF8select ), cont_( cont )
			{ }
			virtual ~StringMethod_select_cont( ) { }
			virtual void takeValue( const RefCountPtr< const Lang::Value > & posUntyped, Kernel::EvalState * evalState, bool dummy ) const
			{
				RefCountPtr< const Lang::SingleList > pos = posUntyped.down_cast< const Lang::SingleList >( );
				if( pos == NullPtr< const Lang::SingleList >( ) )
					throw Exceptions::InternalError( "StringMethod_select_cont::takeValue did not get a SingleList value." );

				Lang::Integer::ValueType posEnd = UTF8select_ ? self_->UTF8count( ) : self_->bytecount_;

				typedef std::vector< Lang::Integer::ValueType > PosVecType;
				PosVecType posTyped;

				RefCountPtr< const Lang::SingleList > src = pos;
				while( ! src->isNull( ) )
					{
						try
							{
								typedef const Lang::SingleListPair ArgType;
								RefCountPtr< ArgType > p = Helpers::try_cast_CoreArgument< ArgType >( src );
								posTyped.push_back( p->car_->getVal< const Lang::Integer >( traceLoc_ )->val_ );
								src = p->cdr_;
								continue;
							}
						catch( const NonLocalExit::NotThisType & ball )
							{
								/* Wrong type; never mind!.. but see below!
								 */
							}
						try
							{
								typedef const Lang::SingleListRange< const Lang::Integer > ArgType;
								RefCountPtr< ArgType > p = Helpers::try_cast_CoreArgument< ArgType >( src );
								p->expand( & posTyped );
								break;
							}
						catch( const NonLocalExit::NotThisType & ball )
							{
								/* Wrong type; never mind!.. but see below!
								 */
							}
						{
							std::ostringstream msg;
							msg << "When selecting bytes/characters in a string, all positions must be of type " << Lang::Integer::staticTypeName( ) << "." ;
							throw Exceptions::MiscellaneousRequirement( strrefdup( msg ) );
						}
					}

				for( PosVecType::const_iterator i = posTyped.begin( ); i != posTyped.end( ); ++i )
					{
						if( *i < 0 )
							{
								throw Exceptions::OutOfRange( "A negative position was encountered." );
							}
						if( *i >= posEnd )
							{
								throw Exceptions::OutOfRange( "A position beyond the end of the string was encountered." );
							}
					}

				RefCountPtr< const char > resMem = RefCountPtr< const char >( NullPtr< const char >( ) );
				size_t resSize;

				if( UTF8select_ )
					{
						std::vector< Kernel::UnicodeCodePoint > selfChars;
						selfChars.reserve( posEnd );
						/* Compare String::UTF8count! */
						{
							Kernel::UnicodeCodePoint c;
							const char * src = self_->val_.getPtr( );
							const char * end = src + self_->bytecount_;

							while( *src > 0 )
								{
								ASCII:
									size_t avail = 1;
									c.decode_UTF8( & src, & avail );
									selfChars.push_back( c );
								}

							while( *src != 0 && src < end )
								{
									if( *src > 0 )
										{
											goto ASCII;
										}
									else
										{
											switch( 0xF0 & *src )
												{
												case 0xE0:
													{
														size_t avail = 3;
														c.decode_UTF8( & src, & avail );
														selfChars.push_back( c );
													}
													break;
												case 0xF0:
													{
														size_t avail = 4;
														c.decode_UTF8( & src, & avail );
														selfChars.push_back( c );
													}
													break;
												default:
													{
														size_t avail = 2;
														c.decode_UTF8( & src, & avail );
														selfChars.push_back( c );
													}
													break;
												}
										}
								}
							if( *src != 0 || src != end )
								{
									throw Exceptions::InternalError( "The string is not valid UTF-8, but this should have been detected when the characters were counted." );
								}
						}

						std::ostringstream dst;
						const size_t bufSize = 10;
						char buf[ bufSize ];
						for( PosVecType::const_iterator i = posTyped.begin( ); i != posTyped.end( ); ++i )
							{
								char * tmpBuf = buf;
								size_t outAvail = bufSize;
								selfChars[ *i ].encode_UTF8( & tmpBuf, & outAvail );
								*tmpBuf = '\0';
								dst << buf;
							}
						resMem = strrefdup( dst );
						resSize = dst.str( ).size( );
					}
				else
					{
						resSize = posEnd;
						char * dst = new char[ resSize + 1 ];
						resMem = RefCountPtr< const char >( dst );
						const char * srcBegin = self_->val_.getPtr( );
						for( PosVecType::const_iterator i = posTyped.begin( ); i != posTyped.end( ); ++i, ++dst )
							{
								*dst = *( srcBegin + *i );
							}
						*dst = '\0';
					}

				evalState->cont_ = cont_;
				cont_->takeValue( RefCountPtr< const Lang::String >( new Lang::String( resMem, resSize ) ), evalState );
			}
			virtual Kernel::ContRef up( ) const
			{
				return cont_;
			}
			virtual RefCountPtr< const char > description( ) const
			{
				return strrefdup( "Select bytes/characters from String, after forcing positions." );
			}
			virtual void gcMark( Kernel::GCMarkedSet & marked )
			{
				const_cast< Lang::String * >( self_.getPtr( ) )->gcMark( marked );
				cont_->gcMark( marked );
			}
		};

		class StringMethod_select_span_cont : public Kernel::Continuation
		{
			RefCountPtr< const Lang::String > self_;
			bool UTF8select_;
			Kernel::ContRef cont_;
		public:
			StringMethod_select_span_cont( const RefCountPtr< const Lang::String > & self, bool UTF8select, Kernel::ContRef cont, const Ast::SourceLocation & traceLoc )
				: Kernel::Continuation( traceLoc ), self_( self ), UTF8select_( UTF8select ), cont_( cont )
			{ }
			virtual ~StringMethod_select_span_cont( ) { }
			virtual void takeValue( const RefCountPtr< const Lang::Value > & val, Kernel::EvalState * evalState, bool dummy ) const
			{
				typedef const Lang::SingleList ArgType;
				RefCountPtr< ArgType > pos( Helpers::down_cast_ContinuationArgument< ArgType >( val, this ) );

				evalState->cont_ = Kernel::ContRef( new Kernel::ForcingListContinuation( Kernel::ContRef( new Kernel::StringMethod_select_cont( self_, UTF8select_, cont_, traceLoc_ ) ), traceLoc_ ) );
				Kernel::ContRef cont = evalState->cont_;
				cont->takeValue( pos, evalState );
			}
			virtual Kernel::ContRef up( ) const
			{
				return cont_;
			}
			virtual RefCountPtr< const char > description( ) const
			{
				return strrefdup( "Select bytes/characters from String, expand span." );
			}
			virtual void gcMark( Kernel::GCMarkedSet & marked )
			{
				const_cast< Lang::String * >( self_.getPtr( ) )->gcMark( marked );
				cont_->gcMark( marked );
			}
		};

	}
}


Lang::StringMethod_byteselect::StringMethod_byteselect( RefCountPtr< const Lang::String > self, const Ast::FileID * fullMethodID )
	: Lang::MethodBase< class_type >( self, fullMethodID, false, true )
{
	formals_->appendEvaluatedCoreFormal( "pos", Kernel::THE_SLOT_VARIABLE );
}

Lang::StringMethod_byteselect::~StringMethod_byteselect( )
{ }

void
Lang::StringMethod_byteselect::call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
{
	args.applyDefaults( callLoc );

	size_t argsi = 0;
	try
		{
			typedef const Lang::SingleList ArgType;
			RefCountPtr< ArgType > pos = Helpers::try_cast_CoreArgument< ArgType >( args.getValue( argsi ) );

			evalState->cont_ = Kernel::ContRef( new Kernel::ForcingListContinuation( Kernel::ContRef( new Kernel::StringMethod_select_cont( self_, false, evalState->cont_, callLoc ) ), callLoc ) );
			Kernel::ContRef cont = evalState->cont_;
			cont->takeValue( pos, evalState );
			return;
		}
	catch( const NonLocalExit::NotThisType & ball )
		{
			/* Wrong type; never mind!.. but see below!
			 */
		}

	try
		{
			typedef const Lang::Span ArgType;
			RefCountPtr< ArgType > pos = Helpers::try_cast_CoreArgument< ArgType >( args.getValue( argsi ) );

			evalState->cont_ = Kernel::ContRef( new Kernel::StringMethod_select_span_cont( self_, false, evalState->cont_, callLoc ) );
			pos->Lang::Function::call( evalState, RefCountPtr< const Lang::Value >( new Lang::Integer( self_->bytecount_ - 1 ) ), callLoc );
			return;
		}
	catch( const NonLocalExit::NotThisType & ball )
		{
			/* Wrong type; never mind!.. but see below!
			 */
		}

	try
		{
			typedef const Lang::Integer ArgType;
			ArgType::ValueType pos = Helpers::try_cast_CoreArgument< ArgType >( args.getValue( argsi ) )->val_;
			if( pos < 0 )
				{
					throw Exceptions::CoreOutOfRange( coreLoc_, args, argsi, "The position is negative." );
				}
			size_t upos = pos;
			if( upos >= self_->bytecount_ )
				{
					throw Exceptions::CoreOutOfRange( coreLoc_, args, argsi, "The position is beyond the end of the string." );
				}
			Kernel::ContRef cont = evalState->cont_;
			cont->takeValue( Kernel::ValueRef( new Lang::Integer( *reinterpret_cast< const unsigned char * >( self_->val_.getPtr( ) + upos ) ) ),
											 evalState );
			return;
		}
	catch( const NonLocalExit::NotThisType & ball )
		{
			/* Wrong type; never mind!.. but see below!
			 */
		}

	throw Exceptions::CoreTypeMismatch( callLoc, coreLoc_, args, argsi, Helpers::typeSetString( Lang::SingleList::staticTypeName( ), Lang::Span::staticTypeName( ) ) );
}

Lang::StringMethod_UTF8select::StringMethod_UTF8select( RefCountPtr< const Lang::String > self, const Ast::FileID * fullMethodID )
	: Lang::MethodBase< class_type >( self, fullMethodID, false, true )
{
	formals_->appendEvaluatedCoreFormal( "pos", Kernel::THE_SLOT_VARIABLE );
}

Lang::StringMethod_UTF8select::~StringMethod_UTF8select( )
{ }

void
Lang::StringMethod_UTF8select::call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
{
	args.applyDefaults( callLoc );

	size_t argsi = 0;
	try
		{
			typedef const Lang::SingleList ArgType;
			RefCountPtr< ArgType > pos = Helpers::try_cast_CoreArgument< ArgType >( args.getValue( argsi ) );

			evalState->cont_ = Kernel::ContRef( new Kernel::ForcingListContinuation( Kernel::ContRef( new Kernel::StringMethod_select_cont( self_, true, evalState->cont_, callLoc ) ), callLoc ) );
			Kernel::ContRef cont = evalState->cont_;
			cont->takeValue( pos, evalState );
			return;
		}
	catch( const NonLocalExit::NotThisType & ball )
		{
			/* Wrong type; never mind!.. but see below!
			 */
		}

	try
		{
			typedef const Lang::Span ArgType;
			RefCountPtr< ArgType > pos = Helpers::try_cast_CoreArgument< ArgType >( args.getValue( argsi ) );

			evalState->cont_ = Kernel::ContRef( new Kernel::StringMethod_select_span_cont( self_, true, evalState->cont_, callLoc ) );
			pos->Lang::Function::call( evalState, RefCountPtr< const Lang::Value >( new Lang::Integer( self_->UTF8count( ) - 1 ) ), callLoc );
			return;
		}
	catch( const NonLocalExit::NotThisType & ball )
		{
			/* Wrong type; never mind!.. but see below!
			 */
		}

	try
		{
			typedef const Lang::Integer ArgType;
			ArgType::ValueType pos = Helpers::try_cast_CoreArgument< ArgType >( args.getValue( argsi ) )->val_;
			if( pos < 0 )
				{
					throw Exceptions::CoreOutOfRange( coreLoc_, args, argsi, "The position is negative." );
				}
			size_t upos = pos;
			if( upos >= self_->UTF8count( ) )
				{
					throw Exceptions::CoreOutOfRange( coreLoc_, args, argsi, "The position is beyond the end of the string." );
				}

			/* Compare String::UTF8count ! */

			const char * src = self_->val_.getPtr( );
			const char * end = src + self_->bytecount_;

			const char * ASCIIstart = src;
			size_t count = 0;
			if( count == upos )
				{
					goto done;
				}
			while( *src > 0 )
				{
				ASCII:
					++src;
				}
			count += src - ASCIIstart;
			if( count >= upos )
				{
					src -= ( count - upos );
					goto done;
				}
			while( *src != 0 && src < end )
				{
					if( *src > 0 )
						{
							ASCIIstart = src;
							goto ASCII;
						}
					else
						{
							switch( 0xF0 & *src )
								{
								case 0xE0: src += 3; break;
								case 0xF0: src += 4; break;
								default:   src += 2; break;
								}
						}
					++count;
					if( count == upos )
						{
							goto done;
						}
				}
			if( *src != 0 || src != end )
				{
					throw Exceptions::InternalError( "Failed to located character at given position in string." );
				}

		done:
			Kernel::UnicodeCodePoint c;
			c.decode_UTF8( src );
			Kernel::ContRef cont = evalState->cont_;
			cont->takeValue( Kernel::ValueRef( new Lang::Character( c ) ),
											 evalState );
			return;
		}
	catch( const NonLocalExit::NotThisType & ball )
		{
			/* Wrong type; never mind!.. but see below!
			 */
		}

	throw Exceptions::CoreTypeMismatch( callLoc, coreLoc_, args, argsi, Helpers::typeSetString( Lang::SingleList::staticTypeName( ), Lang::Span::staticTypeName( ) ) );
}
