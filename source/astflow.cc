/* This file is part of Shapes.
 *
 * Shapes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Shapes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2008, 2013, 2014 Henrik Tidefelt
 */

#include "astflow.h"
#include "globals.h"
#include "shapesexceptions.h"
#include "astfun.h"
#include "continuations.h"

using namespace Shapes;
using namespace std;


// Shapes::IfExpr::IfExpr( const Ast::SourceLocation & _loc, Ast::Expression * _predicate, Ast::Expression * _consequence, Ast::Expression * _alternative )
//	 : Ast::Expression( _loc ), predicate( _predicate ), consequence( _consequence ), alternative( _alternative )
// {
//	 predicate->setParent( this );
//	 consequence->setParent( this );
//	 alternative->setParent( this );
// }

// Shapes::IfExpr::IfExpr( const Ast::SourceLocation & _loc, Ast::Expression * _predicate, Ast::Expression * _consequence )
//	 : loc( _loc ), predicate( _predicate ), consequence( _consequence ), alternative( 0 )
// {
//	 predicate->setParent( this );
//	 consequence->setParent( this );
// }

// Shapes::IfExpr::~IfExpr( )
// {
//	 delete predicate;
//	 delete consequence;
//	 if( alternative != 0 )
//		 {
//			 delete alternative;
//		 }
// }

// RefCountPtr< const Lang::Value >
// Shapes::IfExpr::value( Kernel::Environment::VariableHandle dstgroup, SimplePDF::PDF_out * pdfo, Kernel::GraphicsState * metaState, Kernel::PassedEnv env ) const
// {
//	 RefCountPtr< const Lang::Value > predUntyped( predicate->value( dstgroup, pdfo, metaState, env ) );
//	 typedef const Lang::Boolean PredType;
//	 PredType * predVal = dynamic_cast< PredType * >( predUntyped.getPtr( ) );
//	 if( predVal == 0 )
//		 {
//			 throw Exceptions::TypeMismatch( predicate, predVal->getTypeName( ), PredType::staticTypeName( ) );
//		 }
//	 if( predVal->val )
//		 {
//			 return consequence->value( dstgroup, pdfo, metaState, env );
//		 }
//	 else if( alternative != 0 )
//		 {
//			 return alternative->value( dstgroup, pdfo, metaState, env );
//		 }
//	 else
//		 {
//			 return Shapes::THE_VOID;
//		 }
// }


Ast::ForceExpr::ForceExpr( const Ast::SourceLocation & loc, Ast::Expression * expr )
  : Ast::Expression( loc ), expr_( expr )
{
  immediate_ = true;
}

Ast::ForceExpr::~ForceExpr( )
{ }

void
Ast::ForceExpr::analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst )
{
  expr_->analyze( this, env, freeStatesDst );
}

void
Ast::ForceExpr::eval( Kernel::EvalState * evalState ) const
{
  evalState->cont_ = Kernel::ContRef( new Kernel::ForcingContinuation( evalState->cont_, expr_->loc( ) ) );
  evalState->expr_ = expr_;
}


Ast::LetDynamicECExpr::LetDynamicECExpr( const Ast::SourceLocation & loc, const Ast::SourceLocation & idLoc, const char * id, Ast::Expression * expr )
	: Ast::Expression( loc ), idLoc_( idLoc, bool( ) ), id_( id ), expr_( expr )
{
  /* Should this expression have
   *   immediate_ = true;
   * or not?  Looking at an example like the following, taken from the amb.blank example,
   *   (escape_continuation return
   *     [( \ dummy → nil )
   *      !(escape_continuation amb_fail
   *         (escape_continue return [fun]))])
   * it might be tempting to make the explicit forcing superflous by setting the immediate_ flag.
   * However, it would then also be required that the expression inside is also immediate, which it
   * is not (see comment in shapesparser.yy).  This setting immediate_ for this expression wouldn't
   * solve the problem, and not being immediate_ is the normal state for an expression, this should
   * be the best choice for this expression as well.
   *
   * What matters is not whether this expression is immedate or not.  What matters is that expr_ is
   * forced. If expr_ is not forced, this expression may first return with a thunk, and later when
   * that thunk is forced, this expression may return again as a consequence of the escape continuation
   * being invoked inside the thunk.  Returning more than once from an expression seems like a very
   * bad idea.
   *
   * It may even be considered a good thing that the explicit forcing is needed, making it clearly
   * visible that tricky things are going on here.
   */
}

Ast::LetDynamicECExpr::~LetDynamicECExpr( )
{
	delete id_;
	delete expr_;
}

void
Ast::LetDynamicECExpr::analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst )
{
	expr_->analyze( this, env, freeStatesDst );
}

void
Ast::LetDynamicECExpr::eval( Kernel::EvalState * evalState ) const
{
	evalState->dyn_ = Kernel::PassedDyn( new Kernel::DynamicEnvironment( evalState->dyn_, id_, evalState->cont_ ) );
	evalState->cont_ = Kernel::ContRef( new Kernel::ForcingContinuation( evalState->cont_, expr_->loc( ) ) );
	evalState->expr_ = expr_;
}

Ast::ContinueDynamicECFunction::ContinueDynamicECFunction( const Ast::SourceLocation & idLoc, const char * id, Ast::Expression * expr )
	: Lang::Function( new Kernel::EvaluatedFormals( Ast::FileID::build_internal( "< dynamic escape continuation >" ), true ) ), idLoc_( idLoc, bool( ) ), id_( id ), expr_( expr )
{ }

Ast::ContinueDynamicECFunction::~ContinueDynamicECFunction( )
{
	delete expr_;
	delete id_;
}

void
Ast::ContinueDynamicECFunction::push_exprs( Ast::ArgListExprs * args ) const
{
	args->orderedExprs_->push_back( expr_ );
}

void
Ast::ContinueDynamicECFunction::analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst )
{
	/* Nothing to analyze?
	 */

	/* expr_ shall be analyzed from the calling expression.
	 * Here, it is only used to locate errors.
	 */
}

void
Ast::ContinueDynamicECFunction::call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
{
	Kernel::ContRef cont = evalState->dyn_->getEscapeContinuation( id_, idLoc_ );
	evalState->cont_ = cont;
	evalState->expr_ = 0; /* If the next call fails, it may be very confusing if expr_ points to something completely irrelevant to the error. */
	cont->takeHandle( args.getHandle( 0 ),
										evalState );
}

Ast::GetECBacktraceExpr::GetECBacktraceExpr( const Ast::SourceLocation & idLoc, const char * id )
	: Ast::Expression( idLoc ), id_( id )
{ }

Ast::GetECBacktraceExpr::~GetECBacktraceExpr( )
{ }

void
Ast::GetECBacktraceExpr::analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst )
{
	/* Nothing to do. */
}

void
Ast::GetECBacktraceExpr::eval( Kernel::EvalState * evalState ) const
{
	Kernel::ContRef cont = evalState->cont_;
	cont->takeValue( Kernel::ValueRef( new Lang::Continuation( evalState->dyn_->getEscapeContinuation( id_, loc_ ) ) ),
									 evalState );
}
