/* This file is part of Shapes.
 *
 * Shapes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Shapes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2008, 2009, 2013, 2014, 2015 Henrik Tidefelt
 */

#pragma once

#include "Shapes_Ast_decls.h"
#include "Shapes_Lang_decls.h"
#include "Shapes_Kernel_decls.h"
#include "Shapes_Exceptions_decls.h"

#include "sourcelocation.h"
#include "corelocation.h"
#include "identifier.h"
#include "exitcodes.h"
#include "methodid.h"
#include "simplepdfo.h"
#include "strrefdup.h" /* this is not needed in this file, but it is convenient to get these declarations for users of this file. */
#include "pdfversion.h"

#include <exception>
#include <iostream>
#include <sstream>
#include <list>
#include <set>
#include <map>


namespace Shapes
{

  namespace Interaction
  {

    class MessageString
    {
      const char * msg_;
      RefCountPtr< const char > msgMem_;
    public:
      MessageString( );
      MessageString( const char * msg );
      MessageString( const RefCountPtr< const char > & msg );

      bool null( ) const { return msg_ == NULL; }
      void show( std::ostream & os ) const;
      void show( const std::string & eachLinePrefix, std::ostream & os ) const;
    };

    std::ostream & operator << ( std::ostream & os, const MessageString & self );

  }

	namespace Exceptions
	{
		extern const std::string additionalLinesPrefix;

		class Exception
		{
		public:
			Exception( );
			virtual ~Exception( );
			virtual void display( std::ostream & os ) const = 0;
			virtual Interaction::ExitCode exitCode( ) const = 0;
			static const char * locsep;
		};

		class NotImplemented : public Exception
		{
			const char * functionality_;
		public:
			NotImplemented( const char * functionality );
			virtual ~NotImplemented( );
			virtual void display( std::ostream & os ) const;
			virtual Interaction::ExitCode exitCode( ) const { return Interaction::EXIT_NOT_IMPLEMENTED; }
		};

		class StaticInconsistency : public Exception
		{
		protected:
			const Ast::SourceLocation & primaryLoc_;
		public:
			StaticInconsistency( const Ast::SourceLocation & primaryLoc ) : primaryLoc_( primaryLoc ) { }
			virtual ~StaticInconsistency( ) { }
			const Ast::SourceLocation & loc( ) const { return primaryLoc_; }
			virtual Interaction::ExitCode exitCode( ) const { return Interaction::EXIT_USER_ERROR; }
		};

		class PostCondition : public Exception
		{
		protected:
			const Ast::SourceLocation & primaryLoc_;
		public:
			PostCondition( const Ast::SourceLocation & primaryLoc ) : primaryLoc_( primaryLoc ) { }
			virtual ~PostCondition( ) { }
			const Ast::SourceLocation & loc( ) const { return primaryLoc_; }
			virtual Interaction::ExitCode exitCode( ) const { return Interaction::EXIT_USER_ERROR; }
		};

		class MiscellaneousStaticInconsistency : public StaticInconsistency
		{
			Interaction::MessageString msg_;
		public:
			MiscellaneousStaticInconsistency( const Ast::SourceLocation & loc, const Interaction::MessageString & msg );
			virtual ~MiscellaneousStaticInconsistency( );
			virtual void display( std::ostream & os ) const;
		};

		class NamespaceDirectoryError : public StaticInconsistency
		{
			Interaction::MessageString msg_;
		public:
			NamespaceDirectoryError( const Ast::SourceLocation & loc, const Interaction::MessageString & msg );
			virtual ~NamespaceDirectoryError( );
			virtual void display( std::ostream & os ) const;
		};

		class AmbiguousNamespaceDirectory : public StaticInconsistency
		{
			std::string previousName_;
			std::string currentDir_;
		public:
			AmbiguousNamespaceDirectory( const std::string & previousName, const std::string & currentDir );
			virtual ~AmbiguousNamespaceDirectory( );
			virtual void display( std::ostream & os ) const;
		};

		class NamespaceDirectoryLookupError : public StaticInconsistency
		{
		public:
			enum Type{ NOT_FOUND, WRONG_TYPE, GLOBAL };
		private:
			Type type_;
			Ast::NamespacePath namespacePath_;
		public:
			NamespaceDirectoryLookupError( const Ast::SourceLocation & loc, Type type, const Ast::NamespacePath::const_iterator & nsBegin, const Ast::NamespacePath::const_iterator & nsEnd );
			NamespaceDirectoryLookupError( Type type, const Ast::NamespacePath::const_iterator & nsBegin, const Ast::NamespacePath::const_iterator & nsEnd );
			virtual ~NamespaceDirectoryLookupError( );
			virtual void display( std::ostream & os ) const;
		};

		class NamespaceDirectoryFileError : public StaticInconsistency
		{
		public:
			enum Type{ NOT_FOUND, WRONG_TYPE };
		private:
			Type type_;
			Ast::NamespacePath namespacePath_;
			std::string filename_;
		public:
			NamespaceDirectoryFileError( const Ast::SourceLocation & loc, Type type, const Ast::NamespacePath & namespacePath, const std::string & filename );
			NamespaceDirectoryFileError( Type type, const Ast::NamespacePath & namespacePath, const std::string & filename );
			virtual ~NamespaceDirectoryFileError( );
			virtual void display( std::ostream & os ) const;
		};

		class NamespaceDirectoryInvalidEntries : public StaticInconsistency
		{
			RefCountPtr< const Ast::NamespacePath > namespacePath_;
		  std::set< std::string > invalidEntries_;
		public:
			NamespaceDirectoryInvalidEntries( const RefCountPtr< const Ast::NamespacePath > & namespacePath, const std::set< std::string > & invalidEntries );
			virtual ~NamespaceDirectoryInvalidEntries( );
			virtual void display( std::ostream & os ) const;
		};

		class NamespaceDirectoryCircularOrdering : public StaticInconsistency
		{
			RefCountPtr< const Ast::NamespacePath > namespacePath_;
			std::string cycleEntry_;
		public:
			NamespaceDirectoryCircularOrdering( const RefCountPtr< const Ast::NamespacePath > & namespacePath, const std::string & cycleEntry );
			virtual ~NamespaceDirectoryCircularOrdering( );
			virtual void display( std::ostream & os ) const;
		};

		class ScannerError : public StaticInconsistency
		{
			Interaction::MessageString msg_;
		public:
			ScannerError( const Ast::SourceLocation & loc, const Interaction::MessageString & msg );
			virtual ~ScannerError( );
			virtual void display( std::ostream & os ) const;
		};

		class ParserError : public StaticInconsistency
		{
			Interaction::MessageString msg_;
			bool justAWarning_;
		public:
			ParserError( const Ast::SourceLocation & loc, const Interaction::MessageString & msg, bool justAWarning = false );
			virtual ~ParserError( );
			virtual void display( std::ostream & os ) const;
		};

		class MemberAlsoAbstract : public StaticInconsistency
		{
			const char * id_;
			const Ast::SourceLocation & abstractLoc_;
		public:
			MemberAlsoAbstract( const Ast::SourceLocation & memberLoc, const char * id, const Ast::SourceLocation & abstractLoc );
			virtual ~MemberAlsoAbstract( );
			virtual void display( std::ostream & os ) const;
		};

		class MemberAlreadyDeclared : public StaticInconsistency
		{
			const char * id_;
			const Ast::SourceLocation & oldLoc_;
		public:
			MemberAlreadyDeclared( const Ast::SourceLocation & memberLoc, const char * id, const Ast::SourceLocation & oldLoc );
			virtual ~MemberAlreadyDeclared( );
			virtual void display( std::ostream & os ) const;
		};

		class PublicGetSetInNonfinalClass : public StaticInconsistency
		{
			const char * id_;
		public:
			PublicGetSetInNonfinalClass( const Ast::SourceLocation & memberLoc, const char * id );
			virtual ~PublicGetSetInNonfinalClass( );
			virtual void display( std::ostream & os ) const;
		};

		class TransformingMemberInNonfinalClass : public StaticInconsistency
		{
			const char * id_;
		public:
			TransformingMemberInNonfinalClass( const Ast::SourceLocation & memberLoc, const char * id );
			virtual ~TransformingMemberInNonfinalClass( );
			virtual void display( std::ostream & os ) const;
		};

		class RepeatedFormal : public StaticInconsistency
		{
			const char * id_;
		public:
			RepeatedFormal( const Ast::SourceLocation & loc, const char * id );
			virtual ~RepeatedFormal( );
			virtual void display( std::ostream & os ) const;
		};

    class StateUninitializedUse : public StaticInconsistency
    {
      const Ast::PlacedIdentifier * id_;
      const Ast::Node * decl_;
    public:
      StateUninitializedUse( const Ast::SourceLocation & loc, const Ast::PlacedIdentifier * id, const Ast::Node * decl );
      virtual ~StateUninitializedUse( );
      virtual void display( std::ostream & os ) const;
    };

		class IllegalFreeStates : public StaticInconsistency
		{
			Ast::StateIDSet * freeStates_;
			const char * reason_;
		public:
			IllegalFreeStates( const Ast::SourceLocation & loc, Ast::StateIDSet * freeStates, const char * reason );
			virtual ~IllegalFreeStates( );
			virtual void display( std::ostream & os ) const;
		};

		class FreeStateIsAlsoPassed : public StaticInconsistency
		{
			Ast::StateID stateID_;
		public:
			FreeStateIsAlsoPassed( const Ast::SourceLocation & loc, Ast::StateID stateID );
			virtual ~FreeStateIsAlsoPassed( );
			virtual void display( std::ostream & os ) const;
		};

		class RepeatedStateArgument : public StaticInconsistency
		{
			RefCountPtr< Ast::StateIDSet > passedStates_;
		public:
			RepeatedStateArgument( const Ast::SourceLocation & loc, const RefCountPtr< Ast::StateIDSet > & passedStates );
			virtual ~RepeatedStateArgument( );
			virtual void display( std::ostream & os ) const;
		};

		class PassingStateOut : public StaticInconsistency
		{
			const char * id_;
		public:
			PassingStateOut( const Ast::SourceLocation & loc, const char * id );
			virtual ~PassingStateOut( );
			virtual void display( std::ostream & os ) const;
		};

		class IntroducingExisting : public StaticInconsistency
		{
			Ast::PlacedIdentifier id_;
		public:
			IntroducingExisting( const Ast::SourceLocation & loc, const Ast::PlacedIdentifier & id );
			virtual ~IntroducingExisting( );
			virtual void display( std::ostream & os ) const;
		};

		class NamespaceAliasCollision : public StaticInconsistency
		{
			const Ast::IdentifierTree * tree_;
			const char * name_;
		public:
			NamespaceAliasCollision( const Ast::SourceLocation & loc, const Ast::IdentifierTree * tree, const char * name );
			virtual ~NamespaceAliasCollision( );
			virtual void display( std::ostream & os ) const;
		};

		class EncapsulationIntroducingExisting : public StaticInconsistency
		{
			const char * id_;
		public:
			EncapsulationIntroducingExisting( const Ast::SourceLocation & loc, const char * id );
			virtual ~EncapsulationIntroducingExisting( );
			virtual void display( std::ostream & os ) const;
		};

		class EncapsulationNamespaceAliasCollision : public StaticInconsistency
		{
			const char * id_;
		public:
			EncapsulationNamespaceAliasCollision( const Ast::SourceLocation & loc, const char * id );
			virtual ~EncapsulationNamespaceAliasCollision( );
			virtual void display( std::ostream & os ) const;
		};

		class FreezingUndefined : public StaticInconsistency
		{
			Ast::PlacedIdentifier id_;
		public:
			FreezingUndefined( const Ast::SourceLocation & loc, const Ast::PlacedIdentifier & id );
			virtual ~FreezingUndefined( );
			virtual void display( std::ostream & os ) const;
		};

		class UndefinedNamespace : public StaticInconsistency
		{
			Ast::NamespaceReference id_;
		public:
			UndefinedNamespace( const Ast::SourceLocation & loc, const Ast::NamespaceReference & id );
			virtual ~UndefinedNamespace( );
			virtual void display( std::ostream & os ) const;
		};

		class ExpectedImmediate : public StaticInconsistency
		{
		public:
			ExpectedImmediate( const Ast::SourceLocation & loc );
			virtual ~ExpectedImmediate( );
			virtual void display( std::ostream & os ) const;
		};

		class IllegalImperative : public StaticInconsistency
		{
		public:
			IllegalImperative( const Ast::SourceLocation & loc );
			virtual ~IllegalImperative( );
			virtual void display( std::ostream & os ) const;
		};

		class FileReadOpenError : public Exception
		{
		public:
			enum Type{ OPEN, STAT };
		private:
			const Ast::SourceLocation & loc_;
			RefCountPtr< const char > filename_;
			Type type_;
			const std::string * sourceDir_;
			const std::list< std::string > * searchPath_;
		public:
			FileReadOpenError( const Ast::SourceLocation & loc, RefCountPtr< const char > filename, const std::string * sourceDir, const std::list< std::string > * searchPath, Type type = OPEN );
			virtual ~FileReadOpenError( );
			virtual void display( std::ostream & os ) const;
			virtual Interaction::ExitCode exitCode( ) const { return Interaction::EXIT_INPUT_FILE_ERROR; }
		};

		class FileWriteOpenError : public Exception
		{
		private:
			const Ast::SourceLocation & loc_;
			RefCountPtr< const char > filename_;
			const char * purpose_;
		public:
			FileWriteOpenError( const Ast::SourceLocation & loc, RefCountPtr< const char > filename, const char * purpose = 0 );
			virtual ~FileWriteOpenError( );
			virtual void display( std::ostream & os ) const;
			virtual Interaction::ExitCode exitCode( ) const { return Interaction::EXIT_OUTPUT_FILE_ERROR; }
		};

		class TeXSetupTooLate : public Exception
		{
			const Ast::SourceLocation & loc_;
		public:
			TeXSetupTooLate( const Ast::SourceLocation & loc );
			virtual ~TeXSetupTooLate( );
			virtual void display( std::ostream & os ) const;
			virtual Interaction::ExitCode exitCode( ) const { return Interaction::EXIT_USER_ERROR; }
		};

		class EmptyFinalPicture : public Exception
		{
		public:
			EmptyFinalPicture( );
			virtual ~EmptyFinalPicture( );
			virtual void display( std::ostream & os ) const;
			virtual Interaction::ExitCode exitCode( ) const { return Interaction::EXIT_USER_ERROR; }
		};

		class RuntimeError : public Exception
		{
		protected:
			const Ast::SourceLocation & loc_;
		public:
			RuntimeError( const Ast::SourceLocation & loc );
			RuntimeError( Ast::Expression * expr );
			const Ast::SourceLocation & getLoc( ) const;
			virtual ~RuntimeError( );
			virtual Interaction::ExitCode exitCode( ) const { return Interaction::EXIT_USER_ERROR; } /* This should be overridden when apropriate! */
		};

		class CatchableError : public RuntimeError
		{
		public:
			CatchableError( const Ast::SourceLocation & loc );
			CatchableError( Ast::Expression * expr );
			virtual ~CatchableError( );
			virtual RefCountPtr< const Lang::Exception > clone( const Kernel::ContRef & cont ) const = 0;
		};

		class UncaughtError : public RuntimeError
		{
			RefCountPtr< const Lang::Exception > ball_;
		public:
			UncaughtError( const RefCountPtr< const Lang::Exception > & ball );
			virtual ~UncaughtError( );
			virtual void display( std::ostream & os ) const;
			virtual Interaction::ExitCode exitCode( ) const;
			const RefCountPtr< const Lang::Exception > & ball( ) const { return ball_; }
		};

		class UserError : public CatchableError
		{
			RefCountPtr< const Lang::Symbol > kind_;
			RefCountPtr< const Lang::String > source_;
			RefCountPtr< const Lang::Value > details_;
			Interaction::MessageString message_;
		public:
			UserError( const RefCountPtr< const Lang::Symbol > & kind, const RefCountPtr< const Lang::String > & source, const RefCountPtr< const Lang::Value > & details, const Interaction::MessageString & message );
			virtual ~UserError( );
			virtual void display( std::ostream & os ) const;
			virtual RefCountPtr< const Lang::Exception > clone( const Kernel::ContRef & cont ) const;
			virtual Interaction::ExitCode exitCode( ) const { return Interaction::EXIT_USER_DETECTED_ERROR; }
		};

		class InternalError : public RuntimeError
		{
		protected:
      Interaction::MessageString msg_;
			bool justAWarning_;
		public:
			InternalError( const Ast::SourceLocation & loc, const Interaction::MessageString & msg, bool justAWarning = false );
			InternalError( const Interaction::MessageString & msg, bool justAWarning = false );
			InternalError( const std::ostringstream & msg, bool justAWarning = false );
			virtual ~InternalError( );
			virtual void display( std::ostream & os ) const;
			virtual Interaction::ExitCode exitCode( ) const { return Interaction::EXIT_INTERNAL_ERROR; }
		};

		class ExternalError : public CatchableError
		{
      Interaction::MessageString msg_;
			bool justAWarning_;
		public:
			ExternalError( const Interaction::MessageString & msg, bool justAWarning = false );
			virtual ~ExternalError( );
			virtual void display( std::ostream & os ) const;
			virtual RefCountPtr< const Lang::Exception > clone( const Kernel::ContRef & cont ) const;
			virtual Interaction::ExitCode exitCode( ) const { return Interaction::EXIT_EXTERNAL_ERROR; }
			ExternalError * cloneTyped( ) const;
		};

		class InternalErrorIn : public InternalError
		{
			Ast::PlacedIdentifier id_;
		public:
			InternalErrorIn( const Ast::PlacedIdentifier & id, const Interaction::MessageString & msg, bool justAWarning = false );
			virtual ~InternalErrorIn( );
			virtual void display( std::ostream & os ) const;
			virtual Interaction::ExitCode exitCode( ) const { return Interaction::EXIT_INTERNAL_ERROR; }
		};

		class TeXSetupHasChanged : public InternalError
		{
		public:
			TeXSetupHasChanged( );
			virtual ~TeXSetupHasChanged( );
		};

		class MiscellaneousRequirement : public CatchableError
		{
      Interaction::MessageString msg_;
		public:
			MiscellaneousRequirement( const Ast::SourceLocation & loc, const Interaction::MessageString & msg );
			MiscellaneousRequirement( const Interaction::MessageString & msg );
			virtual ~MiscellaneousRequirement( );
			virtual void display( std::ostream & os ) const;
			virtual RefCountPtr< const Lang::Exception > clone( const Kernel::ContRef & cont ) const;
		};

		class HandlerError : public CatchableError
		{
      Interaction::MessageString msg_;
		public:
			HandlerError( const Interaction::MessageString & msg );
			virtual ~HandlerError( );
			virtual void display( std::ostream & os ) const;
			virtual RefCountPtr< const Lang::Exception > clone( const Kernel::ContRef & cont ) const;
		};

		class PDFVersionError : public CatchableError
		{
			SimplePDF::PDF_Version::Version version_;
			SimplePDF::PDF_Version::Version required_;
      Interaction::MessageString msg_;
			bool justAWarning_;
		public:
			PDFVersionError( SimplePDF::PDF_Version::Version version, SimplePDF::PDF_Version::Version required, const Interaction::MessageString & msg, bool justAWarning = false );
			virtual ~PDFVersionError( );
			virtual void display( std::ostream & os ) const;
			virtual RefCountPtr< const Lang::Exception > clone( const Kernel::ContRef & cont ) const;
		};

		class RedefiningLexical : public RuntimeError
		{
			Ast::PlacedIdentifier id_;
		public:
			RedefiningLexical( const Ast::PlacedIdentifier & id );
			virtual ~RedefiningLexical( );
			virtual void display( std::ostream & os ) const;
		};

		class RedefiningDynamic : public RuntimeError
		{
			Ast::PlacedIdentifier id_;
		public:
			RedefiningDynamic( const Ast::PlacedIdentifier & id );
			virtual ~RedefiningDynamic( );
			virtual void display( std::ostream & os ) const;
		};

		class TypeMismatch : public RuntimeError
		{
			const char * hint_;
			RefCountPtr< const char > valueType_;
			RefCountPtr< const char > expectedType_;
		public:
			TypeMismatch( const Ast::SourceLocation & loc, const char * hint, RefCountPtr< const char > valueType, RefCountPtr< const char > expectedType );
			TypeMismatch( const Ast::SourceLocation & loc, RefCountPtr< const char > valueType, RefCountPtr< const char > expectedType );
			virtual ~TypeMismatch( );
			virtual void display( std::ostream & os ) const;
		};

		class ExpectedList : public RuntimeError
		{
			size_t index_;
			RefCountPtr< const char > nonListType_;
		public:
			ExpectedList( const Ast::SourceLocation & loc, size_t index, RefCountPtr< const char > nonListType );
			virtual ~ExpectedList( );
			virtual void display( std::ostream & os ) const;
		};

		class NonObjectMemberAssignment : public RuntimeError
		{
			RefCountPtr< const char > valueType_;
		public:
			NonObjectMemberAssignment( const Ast::SourceLocation & loc, RefCountPtr< const char > valueType );
			virtual ~NonObjectMemberAssignment( );
			virtual void display( std::ostream & os ) const;
		};

		class ElementaryWithout : public RuntimeError
		{
		public:
			enum Kind{ VALUE, STATE };
			enum Ref{ FIELD, MUTATOR };
		private:
			Kind kind_;
			Ref ref_;
			RefCountPtr< const char > valueType_;
		public:
			ElementaryWithout( Kind kind, Ref ref, RefCountPtr< const char > valueType );
			virtual ~ElementaryWithout( );
			virtual void display( std::ostream & os ) const;
		};

		class NonExistentMember : public RuntimeError
		{
			RefCountPtr< const char > valueType_;
			const char * fieldID_;
		public:
			NonExistentMember( RefCountPtr< const char > valueType, const char * fieldID );
			virtual ~NonExistentMember( );
			virtual void display( std::ostream & os ) const;
		};

		class NonExistentMutator : public RuntimeError
		{
			RefCountPtr< const char > valueType_;
			const char * mutatorID_;
		public:
			NonExistentMutator( RefCountPtr< const char > valueType, const char * mutatorID );
			virtual ~NonExistentMutator( );
			virtual void display( std::ostream & os ) const;
		};

		class NonExistentPosition : public RuntimeError
		{
			size_t pos_;
			size_t maxPos_;
		public:
			NonExistentPosition( size_t pos, size_t maxPos );
			virtual ~NonExistentPosition( );
			virtual void display( std::ostream & os ) const;
		};

		class UnaryPrefixNotApplicable : public RuntimeError
		{
			const Ast::Expression * expr_;
			RefCountPtr< const char > valueType_;
			RefCountPtr< const Interaction::CoreLocation > coreLoc_;
		public:
			UnaryPrefixNotApplicable( const Ast::SourceLocation & loc, const Ast::Expression * expr, RefCountPtr< const char > valueType );
			virtual ~UnaryPrefixNotApplicable( );
			void setCoreLocation( const RefCountPtr< const Interaction::CoreLocation > & coreLoc );
			virtual void display( std::ostream & os ) const;
		};

		class UnaryPostfixNotApplicable : public RuntimeError
		{
			const Ast::Expression * expr_;
			RefCountPtr< const char > valueType_;
			RefCountPtr< const Interaction::CoreLocation > coreLoc_;
		public:
			UnaryPostfixNotApplicable( const Ast::SourceLocation & loc, const Ast::Expression * expr, RefCountPtr< const char > valueType );
			virtual ~UnaryPostfixNotApplicable( );
			void setCoreLocation( const RefCountPtr< const Interaction::CoreLocation > & coreLoc );
			virtual void display( std::ostream & os ) const;
		};

		class BinaryInfixNotApplicable : public RuntimeError
		{
			const Ast::Expression * expr1_;
			RefCountPtr< const char > valueType1_;
			const Ast::Expression * expr2_;
			RefCountPtr< const char > valueType2_;
			RefCountPtr< const Interaction::CoreLocation > coreLoc_;
		public:
			BinaryInfixNotApplicable( const Ast::SourceLocation & loc, const Ast::Expression * expr1, RefCountPtr< const char > valueType1, const Ast::Expression * expr2, RefCountPtr< const char > valueType2 );
			virtual ~BinaryInfixNotApplicable( );
			void setCoreLocation( const RefCountPtr< const Interaction::CoreLocation > & coreLoc );
			virtual void display( std::ostream & os ) const;
		};

		class LookupUnknown : public StaticInconsistency
		{
			Ast::Identifier id_;
			Ast::Identifier::Type type_;
		public:
			LookupUnknown( const Ast::SourceLocation & loc, const Ast::Identifier & id, Ast::Identifier::Type type );
			virtual ~LookupUnknown( );
			virtual void display( std::ostream & os ) const;
		};

		class LookupUnknownPrivateAlias : public StaticInconsistency
		{
			Ast::PlacedIdentifier id_;
			Ast::Identifier::Type type_;
		public:
			LookupUnknownPrivateAlias( const Ast::SourceLocation & loc, const Ast::PlacedIdentifier & id, Ast::Identifier::Type type );
			virtual ~LookupUnknownPrivateAlias( );
			virtual void display( std::ostream & os ) const;
		};

		class StateBeyondFunctionBoundary : public StaticInconsistency
		{
			Ast::Identifier id_;
		public:
			StateBeyondFunctionBoundary( const Ast::SourceLocation & loc, const Ast::Identifier & id );
			virtual ~StateBeyondFunctionBoundary( );
			virtual void display( std::ostream & os ) const;
		};

		class IntroducingExistingUnit : public StaticInconsistency
		{
			RefCountPtr< const char > id_;
		public:
			IntroducingExistingUnit( const Ast::SourceLocation & loc, RefCountPtr< const char > id );
			virtual ~IntroducingExistingUnit( );
			virtual void display( std::ostream & os ) const;
		};

		class LookupUnknownUnit : public StaticInconsistency
		{
			RefCountPtr< const char > id_;
		public:
			LookupUnknownUnit( const Ast::SourceLocation & loc, RefCountPtr< const char > id );
			virtual ~LookupUnknownUnit( );
			virtual void display( std::ostream & os ) const;
		};

		class OutOfRange : public CatchableError
		{
      Interaction::MessageString msg_;
		public:
			OutOfRange( const Interaction::MessageString & msg );
			OutOfRange( const Ast::SourceLocation & loc, const Interaction::MessageString & msg );
			virtual ~OutOfRange( );
			virtual void display( std::ostream & os ) const;
			virtual RefCountPtr< const Lang::Exception > clone( const Kernel::ContRef & cont ) const;
		};

		class FontProblem : public CatchableError
		{
			RefCountPtr< const Lang::Symbol > PostScript_name_;
      Interaction::MessageString msg_;
			bool justAWarning_;
		public:
			FontProblem( const RefCountPtr< const Lang::Symbol > & PostScript_name, const Interaction::MessageString & msg, bool justAWarning = false );
			FontProblem( const RefCountPtr< const Lang::Symbol > & PostScript_name, const Ast::SourceLocation & loc, const Interaction::MessageString & msg, bool justAWarning = false );
			virtual ~FontProblem( );
			virtual void display( std::ostream & os ) const;
			virtual RefCountPtr< const Lang::Exception > clone( const Kernel::ContRef & cont ) const;
		};

		class NonVoidStatement : public RuntimeError
		{
			RefCountPtr< const Lang::Value > val_;
		public:
			NonVoidStatement( const Ast::SourceLocation & loc, RefCountPtr< const Lang::Value > val );
			virtual ~NonVoidStatement( );
			virtual void display( std::ostream & os ) const;
		};

		class UserArityMismatch : public RuntimeError
		{
		public:
			enum Type{ VARIABLE, STATE };
		private:
			const Ast::SourceLocation & formalsLoc_;
			const size_t functionArity_;
			const size_t callArity_;
			const Type type_;
		public:
			UserArityMismatch( const Ast::SourceLocation & callLoc, const Ast::SourceLocation & formalsLoc, size_t functionArity, size_t callArity, const Type type );
			virtual ~UserArityMismatch( );
			virtual void display( std::ostream & os ) const;
		};

		class SinkRequired : public RuntimeError
		{
		private:
			const Ast::SourceLocation & formalsLoc_;
			const size_t formalsArity_;
			const size_t callArity_;
		public:
			SinkRequired( const Ast::SourceLocation & formalsLoc, size_t formalsArity, size_t callArity );
			virtual ~SinkRequired( );
			virtual void display( std::ostream & os ) const;
		};

		class NamedFormalMismatch : public RuntimeError
		{
		public:
			enum Type{ VARIABLE, STATE };
		private:
			const Ast::SourceLocation & formalsLoc_;
			RefCountPtr< const char > name_;
			Type type_;
		public:
			NamedFormalMismatch( const Ast::SourceLocation & formalsLoc, RefCountPtr< const char > name, Type type );
			virtual ~NamedFormalMismatch( );
			virtual void display( std::ostream & os ) const;
		};

		class NamedFormalAlreadySpecified : public RuntimeError
		{
		public:
			enum Type{ VARIABLE, STATE };
		private:
			const Ast::SourceLocation & formalsLoc_;
			RefCountPtr< const char > name_;
			size_t pos_;
			Type type_;
		public:
			NamedFormalAlreadySpecified( const Ast::SourceLocation & formalsLoc, RefCountPtr< const char > name, size_t pos, Type type );
			virtual ~NamedFormalAlreadySpecified( );
			virtual void display( std::ostream & os ) const;
		};

		class MissingArguments : public RuntimeError
		{
			const Ast::SourceLocation & formalsLoc_;
			RefCountPtr< const std::map< size_t, RefCountPtr< const char > > > missingVariables_;
			RefCountPtr< const std::map< size_t, RefCountPtr< const char > > > missingStates_;
		public:
			MissingArguments( const Ast::SourceLocation & callLoc, const Ast::SourceLocation & formalsLoc, std::map< size_t, RefCountPtr< const char > > * missingVariables, std::map< size_t, RefCountPtr< const char > > * missingStates );
			~MissingArguments( );
			virtual void display( std::ostream & os ) const;
		};

    class CoreArityMismatch : public RuntimeError
    {
      RefCountPtr< const Interaction::CoreLocation > coreLoc_;
      const size_t functionArityLow_;
      const size_t functionArityHigh_;
      const size_t callArity_;
    public:
      CoreArityMismatch( const RefCountPtr< const Interaction::CoreLocation > & coreLoc, size_t functionArity, size_t callArity );
      CoreArityMismatch( Interaction::CoreLocation * coreLoc, size_t functionArity, size_t callArity ); /* Transfers ownership. */
      CoreArityMismatch( const RefCountPtr< const Interaction::CoreLocation > & coreLoc, size_t functionArityLow, size_t functionArityHigh, size_t callArity );
      /* Convenience constructors for the most common use cases. */
      CoreArityMismatch( const Ast::PlacedIdentifier & coreId, size_t functionArity, size_t callArity );
      CoreArityMismatch( const Ast::PlacedIdentifier & coreId, size_t functionArityLow, size_t functionArityHigh, size_t callArity );
      CoreArityMismatch( const char * coreSyntax, size_t functionArity, size_t callArity );
      virtual ~CoreArityMismatch( );
      virtual void display( std::ostream & os ) const;
    };

    class CoreNoNamedFormals : public RuntimeError
    {
      const char * what_;
    public:
      CoreNoNamedFormals( const char * what );
      virtual ~CoreNoNamedFormals( );
      virtual void display( std::ostream & os ) const;
    };

    class CoreTypeMismatch : public RuntimeError
    {
      RefCountPtr< const Interaction::CoreLocation > coreLoc_;
      const Ast::SourceLocation & argLoc_;
      RefCountPtr< const char > valueType_;
      RefCountPtr< const char > expectedType_;
    public:
      CoreTypeMismatch( const Ast::SourceLocation & callLoc,
                        Interaction::CoreLocation * coreLoc, /* Transfers ownership. */
                        const Ast::SourceLocation & argLoc,
                        RefCountPtr< const char > valueType,
                        RefCountPtr< const char > expectedType );
      CoreTypeMismatch( const Ast::SourceLocation & callLoc,
                        const RefCountPtr< const Interaction::CoreLocation > & coreLoc,
                        Kernel::Arguments & args,
                        size_t argNo,
                        RefCountPtr< const char > expectedType );
      CoreTypeMismatch( const Ast::SourceLocation & callLoc,
                        Interaction::CoreLocation * coreLoc, /* Transfers ownership. */
                        Kernel::Arguments & args,
                        size_t argNo,
                        RefCountPtr< const char > expectedType );
      /* Convenience constructor for the most common use case. */
      CoreTypeMismatch( const Ast::SourceLocation & callLoc,
                        const Ast::PlacedIdentifier & coreId,
                        Kernel::Arguments & args,
                        size_t argNo,
                        RefCountPtr< const char > expectedType );
      virtual ~CoreTypeMismatch( );
      virtual void display( std::ostream & os ) const;
    };

    class CoreDynamicTypeMismatch : public RuntimeError
    {
      RefCountPtr< const Interaction::CoreLocation > coreLoc_;
      Ast::PlacedIdentifier id_;
      RefCountPtr< const char > valueType_;
      RefCountPtr< const char > expectedType_;
    public:
      CoreDynamicTypeMismatch( const Ast::SourceLocation & callLoc,
                               const Ast::PlacedIdentifier & coreId,
                               const Ast::PlacedIdentifier & id,
                               RefCountPtr< const char > valueType,
                               RefCountPtr< const char > expectedType );
      virtual ~CoreDynamicTypeMismatch( );
      virtual void display( std::ostream & os ) const;
    };

    class CoreStateTypeMismatch : public RuntimeError
    {
      RefCountPtr< const Interaction::CoreLocation > coreLoc_;
      const Ast::SourceLocation & argLoc_;
      RefCountPtr< const char > valueType_;
      RefCountPtr< const char > expectedType_;
    public:
      CoreStateTypeMismatch( const Ast::SourceLocation & callLoc,
                             const Ast::PlacedIdentifier & coreId,
                             const Ast::SourceLocation & argLoc,
                             RefCountPtr< const char > valueType,
                             RefCountPtr< const char > expectedType );
      virtual ~CoreStateTypeMismatch( );
      virtual void display( std::ostream & os ) const;
    };

		class ContinuationTypeMismatch : public RuntimeError
		{
			RefCountPtr< const char > valueType_;
			RefCountPtr< const char > expectedType_;
		public:
			ContinuationTypeMismatch( const Kernel::Continuation * locationCont,
																RefCountPtr< const char > valueType,
																RefCountPtr< const char > expectedType );
			virtual ~ContinuationTypeMismatch( );
			virtual void display( std::ostream & os ) const;
		};

    class CoreOutOfRange : public CatchableError
    {
      RefCountPtr< const Interaction::CoreLocation > coreLoc_;
      size_t argNo_;
      const Ast::SourceLocation & argLoc_;
      Interaction::MessageString msg_;
    public:
      CoreOutOfRange( const Ast::SourceLocation & callLoc,
                      const Ast::PlacedIdentifier & coreId,
                      Kernel::Arguments & args,
                      size_t argNo,
                      const Interaction::MessageString & msg );
      CoreOutOfRange( const Ast::PlacedIdentifier & coreId, Kernel::Arguments & args, size_t argNo, const Interaction::MessageString & msg );
      CoreOutOfRange( const Ast::SourceLocation & callLoc,
                      const RefCountPtr< const Interaction::CoreLocation > & coreLoc,
                      Kernel::Arguments & args,
                      size_t argNo,
                      const Interaction::MessageString & msg );
      CoreOutOfRange( const RefCountPtr< const Interaction::CoreLocation > & coreLoc, Kernel::Arguments & args, size_t argNo, const Interaction::MessageString & msg );
      CoreOutOfRange( Interaction::CoreLocation * coreLoc, Kernel::Arguments & args, size_t argNo, const Interaction::MessageString & msg ); /* Transfers ownership. */
      virtual ~CoreOutOfRange( );
      virtual void display( std::ostream & os ) const;
      virtual RefCountPtr< const Lang::Exception > clone( const Kernel::ContRef & cont ) const;
    protected:
      void setMessage( const Interaction::MessageString & msg );
    };

		class UserOutOfRange : public CatchableError
		{
			RefCountPtr< const Lang::Symbol > kind_;
			RefCountPtr< const Lang::String > source_;
			RefCountPtr< const Lang::Integer > details_;
			Interaction::MessageString message_;
		public:
			UserOutOfRange( const RefCountPtr< const Lang::Symbol > & kind, const RefCountPtr< const Lang::String > & source, const RefCountPtr< const Lang::Integer > & details, const Interaction::MessageString & message );
			virtual ~UserOutOfRange( );
			virtual void display( std::ostream & os ) const;
			virtual RefCountPtr< const Lang::Exception > clone( const Kernel::ContRef & cont ) const;
		};

		class UserTypeMismatch : public CatchableError
		{
			RefCountPtr< const Lang::Symbol > kind_;
			RefCountPtr< const Lang::String > source_;
			RefCountPtr< const Lang::Integer > details_;
			RefCountPtr< const char > expectedType_;
			RefCountPtr< const char > valueType_;
		public:
			UserTypeMismatch( const RefCountPtr< const Lang::Symbol > & kind, const RefCountPtr< const Lang::String > & source, const RefCountPtr< const Lang::Integer > & details, const RefCountPtr< const char > & expectedType, const RefCountPtr< const char > & valueType );
			virtual ~UserTypeMismatch( );
			virtual void display( std::ostream & os ) const;
			virtual RefCountPtr< const Lang::Exception > clone( const Kernel::ContRef & cont ) const;
		};

    class CoreRequirement : public CatchableError
    {
      RefCountPtr< const Interaction::CoreLocation > coreLoc_;
      Interaction::MessageString msg_;
    public:
      CoreRequirement( const Interaction::MessageString & msg, const RefCountPtr< const Interaction::CoreLocation > & coreLoc, const Ast::SourceLocation & callLoc );
      CoreRequirement( const Interaction::MessageString & msg, Interaction::CoreLocation * coreLoc, const Ast::SourceLocation & callLoc ); /* Transfers ownership. */
      /* Convenience constructor for the most common use case. */
      CoreRequirement( const Interaction::MessageString & msg, const Ast::PlacedIdentifier & id, const Ast::SourceLocation & callLoc );
      virtual ~CoreRequirement( );
      virtual void display( std::ostream & os ) const;
      virtual RefCountPtr< const Lang::Exception > clone( const Kernel::ContRef & cont ) const;
    };

    class BuildRequirement : public CatchableError
    {
      RefCountPtr< const Interaction::CoreLocation > coreLoc_;
      const char * dependency_;
    public:
      BuildRequirement( const char * dependency, const RefCountPtr< const Interaction::CoreLocation > & coreLoc, const Ast::SourceLocation & callLoc );
      /* Convenience constructor for the most common use case. */
      BuildRequirement( const char * dependency, const Ast::PlacedIdentifier & id, const Ast::SourceLocation & callLoc );
      virtual ~BuildRequirement( );
      virtual void display( std::ostream & os ) const;
      virtual RefCountPtr< const Lang::Exception > clone( const Kernel::ContRef & cont ) const;
    };

		class TeXLabelError : public RuntimeError
		{
			const Ast::SourceLocation & strLoc_;
			bool quoted_;
			const char * region_;
			Interaction::MessageString summary_;
			Interaction::MessageString details_;
		public:
			TeXLabelError( bool quoted, const char * region, const Interaction::MessageString & summary, const Interaction::MessageString & details, const Ast::SourceLocation & strLoc );
			virtual ~TeXLabelError( );
			virtual void display( std::ostream & os ) const;
			virtual Interaction::ExitCode exitCode( ) const { return Interaction::EXIT_TEX_ERROR; }
		};

		class StaticTeXLabelError : public StaticInconsistency
		{
			bool quoted_;
			const char * region_;
			Interaction::MessageString summary_;
			Interaction::MessageString details_;
		public:
			StaticTeXLabelError( bool quoted, const char * region, const Interaction::MessageString & summary, const Interaction::MessageString & details, const Ast::SourceLocation & strLoc );
			virtual ~StaticTeXLabelError( );
			virtual void display( std::ostream & os ) const;
			virtual Interaction::ExitCode exitCode( ) const { return Interaction::EXIT_TEX_ERROR; }
		};

		class MultipleDynamicBind : public RuntimeError
		{
			const Ast::PlacedIdentifier * id_;
			const Ast::SourceLocation & prevLoc_;
		public:
			MultipleDynamicBind( const Ast::PlacedIdentifier * id, const Ast::SourceLocation & loc, const Ast::SourceLocation & prevLoc );
			virtual ~MultipleDynamicBind( );
			virtual void display( std::ostream & os ) const;
		};

		class UndefinedEscapeContinuation : public RuntimeError
		{
			const char * id_;
		public:
			UndefinedEscapeContinuation( const char * id, const Ast::SourceLocation & loc );
			virtual ~UndefinedEscapeContinuation( );
			virtual void display( std::ostream & os ) const;
		};

		class DeadStateAccess : public RuntimeError
		{
		public:
			DeadStateAccess( );
			virtual ~DeadStateAccess( );
			virtual void display( std::ostream & os ) const;
		};

		class UnFreezable : public RuntimeError
		{
		public:
			UnFreezable( );
			virtual ~UnFreezable( );
			virtual void display( std::ostream & os ) const;
		};

		class UnPeekable : public RuntimeError
		{
		public:
			UnPeekable( );
			virtual ~UnPeekable( );
			virtual void display( std::ostream & os ) const;
		};

		class UninitializedAccess : public RuntimeError
		{
		public:
			UninitializedAccess( );
			virtual ~UninitializedAccess( );
			virtual void display( std::ostream & os ) const;
		};

		class DtMinError : public CatchableError
		{
			double dt_;
		public:
			DtMinError( double dt );
			virtual ~DtMinError( );
			virtual void display( std::ostream & os ) const;
			virtual RefCountPtr< const Lang::Exception > clone( const Kernel::ContRef & cont ) const;
			virtual Interaction::ExitCode exitCode( ) const { return Interaction::EXIT_TOLERANCE_ERROR; }
		};

		class AffineTransformKillsPlane : public CatchableError
		{
			double sigma2_;
		public:
			AffineTransformKillsPlane( double sigma2 );
			virtual ~AffineTransformKillsPlane( );
			virtual void display( std::ostream & os ) const;
			virtual RefCountPtr< const Lang::Exception > clone( const Kernel::ContRef & cont ) const;
		};

		class MissingFontMetrics : public RuntimeError
		{
			RefCountPtr< const char > fontname_;
			const std::list< std::string > * searchPath_;
		public:
			MissingFontMetrics( RefCountPtr< const char > fontname, const std::list< std::string > * searchPath );
			virtual ~MissingFontMetrics( );
			virtual void display( std::ostream & os ) const;
			virtual Interaction::ExitCode exitCode( ) const { return Interaction::EXIT_EXTERNAL_ERROR; }
		};

		class FontMetricsError : public RuntimeError
		{
			RefCountPtr< const char > fontname_;
			Interaction::MessageString message_;
		public:
			FontMetricsError( const RefCountPtr< const char > & fontname, const Interaction::MessageString & message );
			virtual ~FontMetricsError( );
			virtual void display( std::ostream & os ) const;
			virtual Interaction::ExitCode exitCode( ) const { return Interaction::EXIT_EXTERNAL_ERROR; }
		};

		class InsertingEmptyPage : public CatchableError
		{
		public:
			InsertingEmptyPage( const Ast::SourceLocation & loc );
			virtual ~InsertingEmptyPage( );
			virtual void display( std::ostream & os ) const;
			virtual RefCountPtr< const Lang::Exception > clone( const Kernel::ContRef & cont ) const;
		};

		class InvalidGraphKeyType : public InternalError
		{
		public:
			InvalidGraphKeyType( );
			virtual ~InvalidGraphKeyType( );
		};

		class GraphKeyTypeMismatch : public TypeMismatch
		{
		public:
			GraphKeyTypeMismatch( const Ast::SourceLocation & loc, const char * hint, RefCountPtr< const char > valueType );
			virtual ~GraphKeyTypeMismatch( );
			static RefCountPtr< const char > expectedType;
			static RefCountPtr< const char > expectedTypeOrNode;
			static RefCountPtr< const char > expectedTypeOrStructure;
		};

		class InvalidGraphKey : public InternalError
		{
		public:
			enum Type{ NODE, PARTITION };
		public:
			InvalidGraphKey( Type type );
			virtual ~InvalidGraphKey( );
		};

		class GraphKeyOutOfRange : public CoreOutOfRange
		{
		public:
			enum Type{ NODE, PARTITION };
		private:
			Type type_;
		public:
			GraphKeyOutOfRange( Type type, const Ast::SourceLocation & callLoc, const Ast::PlacedIdentifier & coreId, Kernel::Arguments & args, size_t argNo );
			GraphKeyOutOfRange( Type type, const Ast::SourceLocation & callLoc, const RefCountPtr< const Interaction::CoreLocation > & coreLoc, Kernel::Arguments & args, size_t argNo );
			virtual ~GraphKeyOutOfRange( );
		private:
			void initMessage( Kernel::Arguments & args, size_t argNo );
		};

		class InvalidGraphElement : public CatchableError
		{
		public:
			enum Type{ NODE, EDGE };
		private:
			Type type_;
		public:
			InvalidGraphElement( Type type, const Ast::SourceLocation & loc );
			virtual ~InvalidGraphElement( );
			virtual void display( std::ostream & os ) const;
			virtual RefCountPtr< const Lang::Exception > clone( const Kernel::ContRef & cont ) const;
		};

		class EdgeTraceError : public CatchableError
		{
		public:
			enum Type{ UNDIRECTED, TRACE, BACKTRACE };
		private:
			Type type_;
			RefCountPtr< const Lang::Value > edgeKeyFrom_;
			RefCountPtr< const Lang::Value > edgeKeyTo_;
			RefCountPtr< const Lang::Value > nodeKey_;
		public:
			EdgeTraceError( Type type, const RefCountPtr< const Lang::Value > & edgeKeyFrom, const RefCountPtr< const Lang::Value > & edgeKeyTo, const RefCountPtr< const Lang::Value > & nodeKey, const Ast::SourceLocation & loc );
			virtual ~EdgeTraceError( );
			virtual void display( std::ostream & os ) const;
			virtual RefCountPtr< const Lang::Exception > clone( const Kernel::ContRef & cont ) const;
		};

		class GraphDomainError : public CatchableError
		{
		public:
			enum Type{ DIRECTED, UNDIRECTED, LOOPS, PARALLEL };
		private:
			Type type_;
			bool directed_;
			RefCountPtr< const Lang::Value > edgeKeyFrom_;
			RefCountPtr< const Lang::Value > edgeKeyTo_;
		public:
			GraphDomainError( Type type, bool directed, const RefCountPtr< const Lang::Value > & edgeKeyFrom, const RefCountPtr< const Lang::Value > & edgeKeyTo, const Ast::SourceLocation & loc );
			virtual ~GraphDomainError( );
			virtual void display( std::ostream & os ) const;
			virtual RefCountPtr< const Lang::Exception > clone( const Kernel::ContRef & cont ) const;
		};

		class InvalidGraphPartition : public CatchableError
		{
			RefCountPtr< const Lang::Value > partition_;
			RefCountPtr< const Lang::SingleList > partitions_;
		public:
			InvalidGraphPartition( const RefCountPtr< const Lang::Value > & partition, const RefCountPtr< const Lang::SingleList > & partitions, const Ast::SourceLocation & loc );
			virtual ~InvalidGraphPartition( );
			virtual void display( std::ostream & os ) const;
			virtual RefCountPtr< const Lang::Exception > clone( const Kernel::ContRef & cont ) const;
		};

		class GraphPartitionViolation : public CatchableError
		{
			RefCountPtr< const Lang::Value > sourceKey_;
			RefCountPtr< const Lang::Value > targetKey_;
			RefCountPtr< const Lang::Value > partition_;
		public:
			GraphPartitionViolation( const RefCountPtr< const Lang::Value > & sourceKey, const RefCountPtr< const Lang::Value > & targetKey, const RefCountPtr< const Lang::Value > & partition, const Ast::SourceLocation & loc );
			virtual ~GraphPartitionViolation( );
			virtual void display( std::ostream & os ) const;
			virtual RefCountPtr< const Lang::Exception > clone( const Kernel::ContRef & cont ) const;
		};


		class UndefinedCrossRef : public PostCondition
		{
		private:
			RefCountPtr< const char > ref_;
		public:
			UndefinedCrossRef( const Ast::SourceLocation & loc, RefCountPtr< const char > ref );
			virtual ~UndefinedCrossRef( );
			virtual void display( std::ostream & os ) const;
		};

		class InvocationError : public Exception
		{
			Interaction::MessageString msg_;
			bool justAWarning_;
		public:
			InvocationError( const Interaction::MessageString & msg, bool justAWarning = false );
			virtual ~InvocationError( );
			virtual void display( std::ostream & os ) const;
			virtual Interaction::ExitCode exitCode( ) const { return Interaction::EXIT_INVOCATION_ERROR; }
		};

		class BadSetValueState : public InternalError
		{
		public:
			BadSetValueState( );
			virtual ~BadSetValueState( );
		};

	}

	namespace NonLocalExit
	{
		/* The exceptions in this namespace does not represent ordinary exception conditions in the program.
		 * Rather, they can be seen as alternative ways for particular functions to return.  Thus, the caller
		 * of such a function shall always take care of any such exceptions, and not let these be passed up.
		 * For this reason, the classes in this namespace does not inherit from Shapes::Exceptions::Exception.
		 */

		class NonLocalExitBase
		{
		public:
			NonLocalExitBase( ){ }
		};

		class DynamicBindingNotFound : public NonLocalExitBase
		{
		public:
			DynamicBindingNotFound( ){ }
		};

		class CrossDirectionOfParallel : public NonLocalExitBase
		{
		public:
			CrossDirectionOfParallel( ){ }
		};

		class NotThisType : public NonLocalExitBase
		{
		public:
			NotThisType( ){ }
		};

	}
}
