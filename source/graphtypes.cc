/* This file is part of Shapes.
 *
 * Shapes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Shapes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2013, 2014 Henrik Tidefelt
 */

#include "shapestypes.h"
#include "shapesexceptions.h"
#include "globals.h"
#include "methodbase.h"
#include "ast.h"
#include "graphtypes_impl.h"
#include "shapescore.h"
#include "continuations.h"
#include "warn.h"

using namespace Shapes;


Kernel::Edge::Edge( Kernel::Node * source, Kernel::Node * target, size_t index, bool directed )
	: source_( source ), target_( target ), index_( index )
{
	if( ! directed && source_->index( ) > target_->index( ) ){
		Kernel::Node * tmp = target_;
		target_ = source_;
		source_ = tmp;
	}
}


Kernel::EdgePredicate::EdgePredicate( )
	: always_false_( false ), always_true_( false )
{ }

Kernel::EdgePredicate::~EdgePredicate( )
{ }


void
Kernel::Node::show( std::ostream & os ) const
{
	os << "< node with key " ;
	key_->show( os );
	os << " >" ;
}

Kernel::Node::ListRef
Kernel::Node::prepend_uedges( const ListRef & rest, const RefCountPtr< const Lang::Graph > & graph, const Kernel::EdgePredicate * filter ) const
{
	Kernel::Node::ListRef lst = prepend_edges( uedgesLow_, false, rest, graph, filter );
	return prepend_edges( uedgesHigh_, false, lst, graph, filter );
}

Kernel::Node::ListRef
Kernel::Node::prepend_dedgesIn( const ListRef & rest, const RefCountPtr< const Lang::Graph > & graph, const Kernel::EdgePredicate * filter ) const
{
	return prepend_edges( dedgesIn_, true, rest, graph, filter );
}

Kernel::Node::ListRef
Kernel::Node::prepend_dedgesOut( const ListRef & rest, const RefCountPtr< const Lang::Graph > & graph, const Kernel::EdgePredicate * filter ) const
{
	return prepend_edges( dedgesOut_, true, rest, graph, filter );
}

Kernel::Node::ListRef
Kernel::Node::prepend_uloops( const ListRef & rest, const RefCountPtr< const Lang::Graph > & graph, const Kernel::EdgePredicate * filter ) const
{
	return prepend_edges( uloops_, false, rest, graph, filter );
}

Kernel::Node::ListRef
Kernel::Node::prepend_dloops( const ListRef & rest, const RefCountPtr< const Lang::Graph > & graph, const Kernel::EdgePredicate * filter ) const
{
	return prepend_edges( dloops_, true, rest, graph, filter );
}

Kernel::Node::ListRef
Kernel::Node::prepend_uedges_to( Kernel::Node * neighbor, const ListRef & rest, const RefCountPtr< const Lang::Graph > & graph, const Kernel::EdgePredicate * filter ) const
{
	if( neighbor->index( ) < index_ )
		return prepend_edges_neighbor( neighbor, uedgesLow_, false, rest, graph, filter );
	else
		return prepend_edges_neighbor( neighbor, uedgesHigh_, false, rest, graph, filter );
}

Kernel::Node::ListRef
Kernel::Node::prepend_dedges_from( Kernel::Node * source, const ListRef & rest, const RefCountPtr< const Lang::Graph > & graph, const Kernel::EdgePredicate * filter ) const
{
	return prepend_edges_neighbor( source, dedgesIn_, true, rest, graph, filter );
}

Kernel::Node::ListRef
Kernel::Node::prepend_dedges_to( Kernel::Node * target, const ListRef & rest, const RefCountPtr< const Lang::Graph > & graph, const Kernel::EdgePredicate * filter ) const
{
	return prepend_edges_neighbor( target, dedgesOut_, true, rest, graph, filter );
}

Kernel::Node::ListRef
Kernel::Node::prepend_uedgeNeighbors( const ListRef & rest, const RefCountPtr< const Lang::Graph > & graph ) const
{
	Kernel::Node::ListRef result = rest;
	for( EdgeSetSource::const_iterator e = uedgesLow_.begin( ); e != uedgesLow_.end( ); ++e ){
		result = Helpers::SingleList_cons( new Lang::Node( graph, (*e)->source( ) ), result );
	}
	for( EdgeSetTarget::const_iterator e = uedgesHigh_.begin( ); e != uedgesHigh_.end( ); ++e ){
		result = Helpers::SingleList_cons( new Lang::Node( graph, (*e)->target( ) ), result );
	}
	return result;
}

Kernel::Node::ListRef
Kernel::Node::prepend_dedgeNeighborsIn( const ListRef & rest, const RefCountPtr< const Lang::Graph > & graph ) const
{
	Kernel::Node::ListRef result = rest;
	for( EdgeSetSource::const_iterator e = dedgesIn_.begin( ); e != dedgesIn_.end( ); ++e ){
		result = Helpers::SingleList_cons( new Lang::Node( graph, (*e)->source( ) ), result );
	}
	return result;
}

Kernel::Node::ListRef
Kernel::Node::prepend_dedgeNeighborsOut( const ListRef & rest, const RefCountPtr< const Lang::Graph > & graph ) const
{
	Kernel::Node::ListRef result = rest;
	for( EdgeSetTarget::const_iterator e = dedgesOut_.begin( ); e != dedgesOut_.end( ); ++e ){
		result = Helpers::SingleList_cons( new Lang::Node( graph, (*e)->target( ) ), result );
	}
	return result;
}

Kernel::Node::ListRef
Kernel::Node::prepend_uloopNeighbors( const ListRef & rest, const RefCountPtr< const Lang::Graph > & graph ) const
{
	Kernel::Node::ListRef result = rest;
	for( EdgeSetSource::const_iterator e = uloops_.begin( ); e != uloops_.end( ); ++e ){
		result = Helpers::SingleList_cons( new Lang::Node( graph, this ), result );
		result = Helpers::SingleList_cons( new Lang::Node( graph, this ), result );
	}
	return result;
}

Kernel::Node::ListRef
Kernel::Node::prepend_dloopNeighbors( const ListRef & rest, const RefCountPtr< const Lang::Graph > & graph ) const
{
	Kernel::Node::ListRef result = rest;
	for( EdgeSetSource::const_iterator e = dloops_.begin( ); e != dloops_.end( ); ++e ){
		result = Helpers::SingleList_cons( new Lang::Node( graph, this ), result );
	}
	return result;
}

Kernel::Node::ListRef
Kernel::Node::prepend_unique_uedgeNeighbors( const ListRef & rest, const RefCountPtr< const Lang::Graph > & graph, std::set< size_t > * added ) const
{
	Kernel::Node::ListRef result = rest;
	for( EdgeSetSource::const_iterator e = uedgesLow_.begin( ); e != uedgesLow_.end( ); ++e ){
		const Kernel::Node * neighbor = (*e)->source( );
		size_t index = neighbor->index( );
		if( added->find( index ) != added->end( ) )
			continue;
		added->insert( added->begin( ), index );
		result = Helpers::SingleList_cons( new Lang::Node( graph, neighbor ), result );
	}
	for( EdgeSetTarget::const_iterator e = uedgesHigh_.begin( ); e != uedgesHigh_.end( ); ++e ){
		const Kernel::Node * neighbor = (*e)->target( );
		size_t index = neighbor->index( );
		if( added->find( index ) != added->end( ) )
			continue;
		added->insert( added->begin( ), index );
		result = Helpers::SingleList_cons( new Lang::Node( graph, neighbor ), result );
	}
	return result;
}

Kernel::Node::ListRef
Kernel::Node::prepend_unique_dedgeNeighborsIn( const ListRef & rest, const RefCountPtr< const Lang::Graph > & graph, std::set< size_t > * added ) const
{
	Kernel::Node::ListRef result = rest;
	for( EdgeSetSource::const_iterator e = dedgesIn_.begin( ); e != dedgesIn_.end( ); ++e ){
		const Kernel::Node * neighbor = (*e)->source( );
		size_t index = neighbor->index( );
		if( added->find( index ) != added->end( ) )
			continue;
		added->insert( added->begin( ), index );
		result = Helpers::SingleList_cons( new Lang::Node( graph, neighbor ), result );
	}
	return result;
}

Kernel::Node::ListRef
Kernel::Node::prepend_unique_dedgeNeighborsOut( const ListRef & rest, const RefCountPtr< const Lang::Graph > & graph, std::set< size_t > * added ) const
{
	Kernel::Node::ListRef result = rest;
	for( EdgeSetTarget::const_iterator e = dedgesOut_.begin( ); e != dedgesOut_.end( ); ++e ){
		const Kernel::Node * neighbor = (*e)->target( );
		size_t index = neighbor->index( );
		if( added->find( index ) != added->end( ) )
			continue;
		added->insert( added->begin( ), index );
		result = Helpers::SingleList_cons( new Lang::Node( graph, neighbor ), result );
	}
	return result;
}

Kernel::Node::ListRef
Kernel::Node::prepend_unique_uloopNeighbors( const ListRef & rest, const RefCountPtr< const Lang::Graph > & graph, std::set< size_t > * added ) const
{
	Kernel::Node::ListRef result = rest;
	if( not uloops_.empty( ) && added->find( index_ ) == added->end( ) ) {
		added->insert( added->begin( ), index_ );
		result = Helpers::SingleList_cons( new Lang::Node( graph, this ), result );
	}
	return result;
}

Kernel::Node::ListRef
Kernel::Node::prepend_unique_dloopNeighbors( const ListRef & rest, const RefCountPtr< const Lang::Graph > & graph, std::set< size_t > * added ) const
{
	Kernel::Node::ListRef result = rest;
	if( not dloops_.empty( ) && added->find( index_ ) == added->end( ) ) {
		added->insert( added->begin( ), index_ );
		result = Helpers::SingleList_cons( new Lang::Node( graph, this ), result );
	}
	return result;
}

Kernel::Node::ListRef
Kernel::Node::prepend_u_multiedges( const Kernel::Node::ListRef & rest, const RefCountPtr< const Lang::Graph > & graph ) const
{
	Kernel::Node::ListRef edges = rest;
	edges = prepend_u_multiedgesHigh( edges, graph );
	edges = prepend_u_multiedgesLow( edges, graph );
	return edges;
}

Kernel::Node::ListRef
Kernel::Node::prepend_d_multiedgesIn( const Kernel::Node::ListRef & rest, const RefCountPtr< const Lang::Graph > & graph ) const
{
	if( dedgesIn_.empty( ) )
		return rest;

	Kernel::Node::ListRef edges = rest;

	EdgeSetSource::const_iterator iBegin = dedgesIn_.begin( );
	EdgeSetSource::const_iterator theEnd = dedgesIn_.end( );
	while( iBegin != theEnd ){
		EdgeSetSource::const_iterator iEnd = iBegin;
		++iEnd;
		size_t count = 1;
		const Kernel::Node * source = (*iBegin)->source( );
		for( ; iEnd != theEnd && (*iEnd)->source( ) == source; ++iEnd, ++count )
			;
		edges = Helpers::SingleList_cons( new Lang::MultiEdge( graph, true, source, this, count, iBegin, iEnd ), edges );
		iBegin = iEnd;
	}

	return edges;
}

Kernel::Node::ListRef
Kernel::Node::prepend_d_multiedgeInFrom( const Kernel::Node * source, const Kernel::Node::ListRef & rest, const RefCountPtr< const Lang::Graph > & graph ) const
{
	if( dedgesIn_.empty( ) )
		return rest;

	/* Construct minimum and maximum edges defining the range in the set.
	 * The const_cast(s) are safe; the values pointed to will not be modified.
	 */
	Kernel::Edge edgeLow( const_cast< Kernel::Node * >( source ), const_cast< Kernel::Node * >( this ), 0, true );
	Kernel::Edge edgeHigh( const_cast< Kernel::Node * >( source ), const_cast< Kernel::Node * >( this ), std::numeric_limits< size_t >::max( ), true );
	EdgeSetSource::const_iterator iBegin = dedgesIn_.lower_bound( & edgeLow );
	EdgeSetSource::const_iterator iEnd = dedgesIn_.upper_bound( & edgeHigh );

	size_t count = 0;
	for( EdgeSetSource::const_iterator i = iBegin; i != iEnd; ++i, ++count )
		;

	if( count == 0 )
		return rest;

	return Helpers::SingleList_cons( new Lang::MultiEdge( graph, true, source, this, count, iBegin, iEnd ), rest );
}

Kernel::Node::ListRef
Kernel::Node::prepend_d_multiedgesOut( const Kernel::Node::ListRef & rest, const RefCountPtr< const Lang::Graph > & graph ) const
{
	if( dedgesOut_.empty( ) )
		return rest;

	Kernel::Node::ListRef edges = rest;

	EdgeSetTarget::const_iterator i = dedgesOut_.begin( );
	EdgeSetTarget::const_iterator theEnd = dedgesOut_.end( );
	while( i != theEnd ){
		const Kernel::Node * target = (*i)->target( );
		++i;
		for( ; i != theEnd && (*i)->target( ) == target; ++i )
			;
		edges = prepend_d_multiedgeInFrom( this, rest, graph );
	}

	return edges;
}

Kernel::Node::ListRef
Kernel::Node::prepend_u_multiloops( const Kernel::Node::ListRef & rest, const RefCountPtr< const Lang::Graph > & graph ) const
{
	if( uloops_.empty( ) )
		return rest;

	return Helpers::SingleList_cons( new Lang::MultiEdge( graph, false, this, this, uloops_.size( ), uloops_.begin( ), uloops_.end( ) ), rest );
}

Kernel::Node::ListRef
Kernel::Node::prepend_d_multiloops( const Kernel::Node::ListRef & rest, const RefCountPtr< const Lang::Graph > & graph ) const
{
	if( dloops_.empty( ) )
		return rest;

	return Helpers::SingleList_cons( new Lang::MultiEdge( graph, true, this, this, dloops_.size( ), dloops_.begin( ), dloops_.end( ) ), rest );
}

Kernel::Node::ListRef
Kernel::Node::prepend_u_multiedgesLow( const Kernel::Node::ListRef & rest, const RefCountPtr< const Lang::Graph > & graph ) const
{
	if( uedgesLow_.empty( ) )
		return rest;

	Kernel::Node::ListRef edges = rest;

	EdgeSetSource::const_iterator iBegin = uedgesLow_.begin( );
	EdgeSetSource::const_iterator theEnd = uedgesLow_.end( );
	while( iBegin != theEnd ){
		EdgeSetSource::const_iterator iEnd = iBegin;
		++iEnd;
		size_t count = 1;
		const Kernel::Node * source = (*iBegin)->source( );
		for( ; iEnd != theEnd && (*iEnd)->source( ) == source; ++iEnd, ++count )
			;
		edges = Helpers::SingleList_cons( new Lang::MultiEdge( graph, false, source, this, count, iBegin, iEnd ), edges );
		iBegin = iEnd;
	}

	return edges;
}

Kernel::Node::ListRef
Kernel::Node::prepend_u_multiedgeLowFrom( const Kernel::Node * source, const Kernel::Node::ListRef & rest, const RefCountPtr< const Lang::Graph > & graph ) const
{
	if( uedgesLow_.empty( ) )
		return rest;

	/* Construct minimum and maximum edges defining the range in the set.
	 * The const_cast(s) are safe; the values pointed to will not be modified.
	 */
	Kernel::Edge edgeLow( const_cast< Kernel::Node * >( source ), const_cast< Kernel::Node * >( this ), 0, false );
	Kernel::Edge edgeHigh( const_cast< Kernel::Node * >( source ), const_cast< Kernel::Node * >( this ), std::numeric_limits< size_t >::max( ), false );
	EdgeSetSource::const_iterator iBegin = uedgesLow_.lower_bound( & edgeLow );
	EdgeSetSource::const_iterator iEnd = uedgesLow_.upper_bound( & edgeHigh );

	size_t count = 0;
	for( EdgeSetSource::const_iterator i = iBegin; i != iEnd; ++i, ++count )
		;

	if( count == 0 )
		return rest;

	return Helpers::SingleList_cons( new Lang::MultiEdge( graph, false, source, this, count, iBegin, iEnd ), rest );
}

Kernel::Node::ListRef
Kernel::Node::prepend_u_multiedgesHigh( const Kernel::Node::ListRef & rest, const RefCountPtr< const Lang::Graph > & graph ) const
{
	if( uedgesHigh_.empty( ) )
		return rest;

	Kernel::Node::ListRef edges = rest;

	EdgeSetTarget::const_iterator i = uedgesHigh_.begin( );
	EdgeSetTarget::const_iterator theEnd = uedgesHigh_.end( );
	while( i != theEnd ){
		const Kernel::Node * target = (*i)->target( );
		++i;
		for( ; i != theEnd && (*i)->target( ) == target; ++i )
			;
		edges = prepend_u_multiedgeLowFrom( this, rest, graph );
	}

	return edges;
}


namespace Shapes
{
	namespace Lang
	{

		template< bool directed, bool undirected, bool in, bool out, const char * fieldID >
		class NodeMethod_edges : public Lang::MethodBase< Lang::Node >
		{
		public:
			NodeMethod_edges( RefCountPtr< const Lang::Node > self, const Ast::FileID * fullMethodID );
			virtual ~NodeMethod_edges( );
			virtual void call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const;
			static const char * staticFieldID( ) { return fieldID; }
		};

		template< bool directed, bool undirected, bool in, bool out, const char * fieldID >
		class NodeMethod_multiedges : public Lang::MethodBase< Lang::Node >
		{
		public:
			NodeMethod_multiedges( RefCountPtr< const Lang::Node > self, const Ast::FileID * fullMethodID );
			virtual ~NodeMethod_multiedges( );
			virtual void call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const;
			static const char * staticFieldID( ) { return fieldID; }
		};

		template< bool directed, bool undirected, const char * fieldID >
		class NodeMethod_loops : public Lang::MethodBase< Lang::Node >
		{
		public:
			NodeMethod_loops( RefCountPtr< const Lang::Node > self, const Ast::FileID * fullMethodID );
			virtual ~NodeMethod_loops( );
			virtual void call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const;
			static const char * staticFieldID( ) { return fieldID; }
		};

		template< bool directed, bool undirected, const char * fieldID >
		class NodeMethod_multiloops : public Lang::MethodBase< Lang::Node >
		{
		public:
			NodeMethod_multiloops( RefCountPtr< const Lang::Node > self, const Ast::FileID * fullMethodID );
			virtual ~NodeMethod_multiloops( );
			virtual void call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const;
			static const char * staticFieldID( ) { return fieldID; }
		};

		template< bool reverse, const char * fieldID >
		class NodeMethod_trace : public Lang::MethodBase< Lang::Node >
		{
		public:
			NodeMethod_trace( RefCountPtr< const Lang::Node > self, const Ast::FileID * fullMethodID );
			virtual ~NodeMethod_trace( );
			virtual void call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const;
			static const char * staticFieldID( ) { return fieldID; }
		};

		template< bool directed, bool undirected, bool in, bool out, const char * fieldID >
		class NodeMethod_neighbors : public Lang::MethodBase< Lang::Node >
		{
		public:
			NodeMethod_neighbors( RefCountPtr< const Lang::Node > self, const Ast::FileID * fullMethodID );
			virtual ~NodeMethod_neighbors( );
			virtual void call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const;
			static const char * staticFieldID( ) { return fieldID; }
		};

		template< class T >
		class Method_get_graph : public Lang::MethodBase< T >
		{
		public:
			Method_get_graph( RefCountPtr< const T > self, const Ast::FileID * fullMethodID );
			virtual ~Method_get_graph( );
			virtual void call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const;
			static const char * staticFieldID( ) { return "get_graph"; }
		};

	}

	namespace Kernel
	{
		char FIELD_ID_u_edges[]     = "u_edges";
		char FIELD_ID_d_edges_in[]  = "d_edges_in";
		char FIELD_ID_d_edges_out[] = "d_edges_out";
		char FIELD_ID_edges_in[]    =   "edges_in";
		char FIELD_ID_edges_out[]   =   "edges_out";
		char FIELD_ID_edges[]       =   "edges";

		char FIELD_ID_u_loops[] = "u_loops";
		char FIELD_ID_d_loops[] = "d_loops";
		char FIELD_ID_loops[]   =   "loops";

		char FIELD_ID_u_multiedges[]     = "u_multiedges";
		char FIELD_ID_d_multiedges_in[]  = "d_multiedges_in";
		char FIELD_ID_d_multiedges_out[] = "d_multiedges_out";
		char FIELD_ID_multiedges_in[]    =   "multiedges_in";
		char FIELD_ID_multiedges_out[]   =   "multiedges_out";
		char FIELD_ID_multiedges[]       =   "multiedges";

		char FIELD_ID_u_multiloops[] = "u_multiloops";
		char FIELD_ID_d_multiloops[] = "d_multiloops";
		char FIELD_ID_multiloops[]   =   "multiloops";

		char FIELD_ID_trace[]     = "trace";
		char FIELD_ID_backtrace[] = "backtrace";

		char FIELD_ID_u_neighbors[]     = "u_neighbors";
		char FIELD_ID_d_neighbors_in[]  = "d_neighbors_in";
		char FIELD_ID_d_neighbors_out[] = "d_neighbors_out";
		char FIELD_ID_neighbors_in[]    =   "neighbors_in";
		char FIELD_ID_neighbors_out[]   =   "neighbors_out";
		char FIELD_ID_neighbors[]       =   "neighbors";
	}
}

Lang::Node::Node( const RefCountPtr< const Lang::Graph > & graph, const Kernel::Node * node )
	: graph_( graph ), node_( node )
{ }

Lang::Node::~Node( )
{ }

void
Node_register_methods( Lang::SystemFinalClass * dstClass )
{
	dstClass->registerMethod( new Kernel::MethodFactory< Lang::Node, Lang::NodeMethod_edges< false, true, true, true, Kernel::FIELD_ID_u_edges > >( ) );
	dstClass->registerMethod( new Kernel::MethodFactory< Lang::Node, Lang::NodeMethod_edges< true, false, true, false, Kernel::FIELD_ID_d_edges_in > >( ) );
	dstClass->registerMethod( new Kernel::MethodFactory< Lang::Node, Lang::NodeMethod_edges< true, false, false, true, Kernel::FIELD_ID_d_edges_out > >( ) );
	dstClass->registerMethod( new Kernel::MethodFactory< Lang::Node, Lang::NodeMethod_edges< true, true, true, false, Kernel::FIELD_ID_edges_in > >( ) );
	dstClass->registerMethod( new Kernel::MethodFactory< Lang::Node, Lang::NodeMethod_edges< true, true, false, true, Kernel::FIELD_ID_edges_out > >( ) );
	dstClass->registerMethod( new Kernel::MethodFactory< Lang::Node, Lang::NodeMethod_edges< true, true, true, true, Kernel::FIELD_ID_edges > >( ) );

	dstClass->registerMethod( new Kernel::MethodFactory< Lang::Node, Lang::NodeMethod_loops< false, true, Kernel::FIELD_ID_u_loops > >( ) );
	dstClass->registerMethod( new Kernel::MethodFactory< Lang::Node, Lang::NodeMethod_loops< true, false, Kernel::FIELD_ID_d_loops > >( ) );
	dstClass->registerMethod( new Kernel::MethodFactory< Lang::Node, Lang::NodeMethod_loops< true, true, Kernel::FIELD_ID_loops > >( ) );

	dstClass->registerMethod( new Kernel::MethodFactory< Lang::Node, Lang::NodeMethod_multiedges< false, true, true, true, Kernel::FIELD_ID_u_multiedges > >( ) );
	dstClass->registerMethod( new Kernel::MethodFactory< Lang::Node, Lang::NodeMethod_multiedges< true, false, true, false, Kernel::FIELD_ID_d_multiedges_in > >( ) );
	dstClass->registerMethod( new Kernel::MethodFactory< Lang::Node, Lang::NodeMethod_multiedges< true, false, false, true, Kernel::FIELD_ID_d_multiedges_out > >( ) );
	dstClass->registerMethod( new Kernel::MethodFactory< Lang::Node, Lang::NodeMethod_multiedges< true, true, true, false, Kernel::FIELD_ID_multiedges_in > >( ) );
	dstClass->registerMethod( new Kernel::MethodFactory< Lang::Node, Lang::NodeMethod_multiedges< true, true, false, true, Kernel::FIELD_ID_multiedges_out > >( ) );
	dstClass->registerMethod( new Kernel::MethodFactory< Lang::Node, Lang::NodeMethod_multiedges< true, true, true, true, Kernel::FIELD_ID_multiedges > >( ) );

	dstClass->registerMethod( new Kernel::MethodFactory< Lang::Node, Lang::NodeMethod_multiloops< false, true, Kernel::FIELD_ID_u_multiloops > >( ) );
	dstClass->registerMethod( new Kernel::MethodFactory< Lang::Node, Lang::NodeMethod_multiloops< true, false, Kernel::FIELD_ID_d_multiloops > >( ) );
	dstClass->registerMethod( new Kernel::MethodFactory< Lang::Node, Lang::NodeMethod_multiloops< true, true, Kernel::FIELD_ID_multiloops > >( ) );

	dstClass->registerMethod( new Kernel::MethodFactory< Lang::Node, Lang::NodeMethod_trace< false, Kernel::FIELD_ID_trace > >( ) );
	dstClass->registerMethod( new Kernel::MethodFactory< Lang::Node, Lang::NodeMethod_trace< true, Kernel::FIELD_ID_backtrace > >( ) );

	dstClass->registerMethod( new Kernel::MethodFactory< Lang::Node, Lang::NodeMethod_neighbors< false, true, true, true, Kernel::FIELD_ID_u_neighbors > >( ) );
	dstClass->registerMethod( new Kernel::MethodFactory< Lang::Node, Lang::NodeMethod_neighbors< true, false, true, false, Kernel::FIELD_ID_d_neighbors_in > >( ) );
	dstClass->registerMethod( new Kernel::MethodFactory< Lang::Node, Lang::NodeMethod_neighbors< true, false, false, true, Kernel::FIELD_ID_d_neighbors_out > >( ) );
	dstClass->registerMethod( new Kernel::MethodFactory< Lang::Node, Lang::NodeMethod_neighbors< true, true, true, false, Kernel::FIELD_ID_neighbors_in > >( ) );
	dstClass->registerMethod( new Kernel::MethodFactory< Lang::Node, Lang::NodeMethod_neighbors< true, true, false, true, Kernel::FIELD_ID_neighbors_out > >( ) );
	dstClass->registerMethod( new Kernel::MethodFactory< Lang::Node, Lang::NodeMethod_neighbors< true, true, true, true, Kernel::FIELD_ID_neighbors > >( ) );

	dstClass->registerMethod( new Kernel::MethodFactory< Lang::Node, Lang::Method_get_graph< Lang::Node > >( ) );
}

RefCountPtr< const Lang::Class > Lang::Node::TypeID( new Lang::SystemFinalClass( strrefdup( "Node" ), Node_register_methods ) );
TYPEINFOIMPL( Node );

void
Lang::Node::show( std::ostream & os ) const
{
	node_->show( os );
}

Kernel::VariableHandle
Lang::Node::getField( const char * fieldID, const RefCountPtr< const Lang::Value > & selfRef ) const
{
	if( strcmp( fieldID, "value" ) == 0 )
		{
			return graph_->nodeValue( node_ );
		}
	if( strcmp( fieldID, "key" ) == 0 )
		{
			return Kernel::VariableHandle( new Kernel::Variable( node_->key( ) ) );
		}
	if( strcmp( fieldID, "index" ) == 0 )
		{
			return Helpers::newValHandle( new Lang::Integer( node_->index( ) ) );
		}
	if( strcmp( fieldID, "partition" ) == 0 )
		{
			return graph_->nodePartition( node_ );
		}
	if( strcmp( fieldID, "u_degree" ) == 0 )
		{
			return Helpers::newValHandle( new Lang::Integer( node_->u_degree( ) ) );
		}
	if( strcmp( fieldID, "d_degree_in" ) == 0 )
		{
			return Helpers::newValHandle( new Lang::Integer( node_->d_degree_in( ) ) );
		}
	if( strcmp( fieldID, "d_degree_out" ) == 0 )
		{
			return Helpers::newValHandle( new Lang::Integer( node_->d_degree_out( ) ) );
		}
	if( strcmp( fieldID, "d_degree" ) == 0 )
		{
			return Helpers::newValHandle( new Lang::Integer( node_->d_degree( ) ) );
		}
	if( strcmp( fieldID, "degree" ) == 0 )
		{
			return Helpers::newValHandle( new Lang::Integer( node_->degree( ) ) );
		}
	if( strcmp( fieldID, "u_loop_count" ) == 0 )
		{
			return Helpers::newValHandle( new Lang::Integer( node_->u_loop_count( ) ) );
		}
	if( strcmp( fieldID, "d_loop_count" ) == 0 )
		{
			return Helpers::newValHandle( new Lang::Integer( node_->d_loop_count( ) ) );
		}
	if( strcmp( fieldID, "loop_count" ) == 0 )
		{
			return Helpers::newValHandle( new Lang::Integer( node_->loop_count( ) ) );
		}

	return TypeID->getMethod( selfRef, fieldID ); /* This will throw if there is no such method. */
}

void
Lang::Node::gcMark( Kernel::GCMarkedSet & marked )
{
	const_cast< Lang::Graph * >( graph_.getPtr( ) )->gcMark( marked );
}


Lang::Edge::Edge( const RefCountPtr< const Lang::Graph > & graph, const Kernel::Edge * edge, bool directed )
	: graph_( graph ), edge_( edge ), directed_( directed )
{ }

Lang::Edge::~Edge( )
{ }

void
Edge_register_methods( Lang::SystemFinalClass * dstClass )
{
	dstClass->registerMethod( new Kernel::MethodFactory< Lang::Edge, Lang::Method_get_graph< Lang::Edge > >( ) );
}

RefCountPtr< const Lang::Class > Lang::Edge::TypeID( new Lang::SystemFinalClass( strrefdup( "Edge" ), Edge_register_methods ) );
TYPEINFOIMPL( Edge );

const Kernel::Node *
Lang::Edge::trace( const Kernel::Node * sourceArg, bool nullOnError ) const
{
	const Kernel::Node * source = edge_->source( );
	const Kernel::Node * target = edge_->target( );

	if( directed_ ) {
		if( sourceArg == source )
			return target;
		else if( nullOnError )
			return NULL;
		else
			throw Exceptions::InternalError( "Edge trace error." );
	} else {
		if( sourceArg == source )
			return target;
		if( sourceArg == target )
			return source;
		else if( nullOnError )
			return NULL;
		else
			throw Exceptions::InternalError( "Edge trace error." );
	}
}

const Kernel::Node *
Lang::Edge::trace( const Kernel::Node * sourceArg, const Ast::SourceLocation & callLoc ) const
{
	const Kernel::Node * res = trace( sourceArg, true );
	if( res != 0 )
		return res;
	throw Exceptions::EdgeTraceError( directed_ ? Exceptions::EdgeTraceError::TRACE : Exceptions::EdgeTraceError::UNDIRECTED,
																		edge_->source( )->key( ), edge_->target( )->key( ),
																		sourceArg->key( ),
																		callLoc );
}

const Kernel::Node *
Lang::Edge::backtrace( const Kernel::Node * targetArg, bool nullOnError ) const
{
	const Kernel::Node * source = edge_->source( );
	const Kernel::Node * target = edge_->target( );

	if( directed_ ) {
		if( targetArg == target )
			return source;
		else if( nullOnError )
			return NULL;
		else
			throw Exceptions::InternalError( "Edge trace error." );
	} else {
		if( targetArg == source )
			return target;
		if( targetArg == target )
			return source;
		else if( nullOnError )
			return NULL;
		else
			throw Exceptions::InternalError( "Edge trace error." );
	}
}

const Kernel::Node *
Lang::Edge::backtrace( const Kernel::Node * targetArg, const Ast::SourceLocation & callLoc ) const
{
	const Kernel::Node * res = backtrace( targetArg, true );
	if( res != 0 )
		return res;
	throw Exceptions::EdgeTraceError( directed_ ? Exceptions::EdgeTraceError::BACKTRACE : Exceptions::EdgeTraceError::UNDIRECTED,
																		edge_->source( )->key( ), edge_->target( )->key( ),
																		targetArg->key( ),
																		callLoc );
}

void
Lang::Edge::show( std::ostream & os ) const
{
	if( directed_ ){
		os << "< directed edge ( " ;
		edge_->source( )->key( )->show( os );
		os << ", " ;
		edge_->target( )->key( )->show( os );
		os << " ) >" ;
	}else{
		os << "< undirected edge ( " ;
		edge_->source( )->key( )->show( os );
		os << ", " ;
		edge_->target( )->key( )->show( os );
		os << " ) >" ;
	}
}

Kernel::VariableHandle
Lang::Edge::getField( const char * fieldID, const RefCountPtr< const Lang::Value > & selfRef ) const
{
	if( strcmp( fieldID, "directed?" ) == 0 )
		{
			return directed_ ? Kernel::THE_TRUE_VARIABLE : Kernel::THE_FALSE_VARIABLE;
		}
	if( strcmp( fieldID, "source" ) == 0 )
		{
			return Helpers::newValHandle( new Lang::Node( graph_, edge_->source( ) ) );
		}
	if( strcmp( fieldID, "target" ) == 0 )
		{
			return Helpers::newValHandle( new Lang::Node( graph_, edge_->target( ) ) );
		}
	if( strcmp( fieldID, "value" ) == 0 )
		{
			return graph_->edgeValue( edge_ );
		}
	if( strcmp( fieldID, "label" ) == 0 )
		{
			return graph_->edgeLabel( edge_ );
		}
	if( strcmp( fieldID, "index" ) == 0 )
		{
			return Helpers::newValHandle( new Lang::Integer( edge_->index( ) ) );
		}
	return TypeID->getMethod( selfRef, fieldID ); /* This will throw if there is no such method. */
}

void
Lang::Edge::gcMark( Kernel::GCMarkedSet & marked )
{
	const_cast< Lang::Graph * >( graph_.getPtr( ) )->gcMark( marked );
}


Lang::MultiEdge::MultiEdge( const RefCountPtr< const Lang::Graph > & graph, bool directed, const Kernel::Node * source, const Kernel::Node * target, size_t count, const SetType::const_iterator & begin, const SetType::const_iterator & end )
	: graph_( graph ), directed_( directed ), source_( source ), target_( target ), count_( count ), begin_( begin ), end_( end )
{
	if( count_ == 0 ){
		throw Exceptions::InternalError( "Lang::MultiEdge::MultiEdge: The multiplicity of a multiedge must not be zero." );
	}
	if( ! directed && source_->index( ) > target_->index( ) ){
		const Kernel::Node * tmp = target_;
		target_ = source_;
		source_ = tmp;
	}
}

Lang::MultiEdge::~MultiEdge( )
{ }

void
MultiEdge_register_methods( Lang::SystemFinalClass * dstClass )
{
	dstClass->registerMethod( new Kernel::MethodFactory< Lang::MultiEdge, Lang::Method_get_graph< Lang::MultiEdge > >( ) );
}

RefCountPtr< const Lang::Class > Lang::MultiEdge::TypeID( new Lang::SystemFinalClass( strrefdup( "MultiEdge" ), MultiEdge_register_methods ) );
TYPEINFOIMPL( MultiEdge );

const Kernel::Node *
Lang::MultiEdge::trace( const Kernel::Node * sourceArg, bool nullOnError ) const
{
	if( directed_ ) {
		if( sourceArg == source_ )
			return target_;
		else if( nullOnError )
			return NULL;
		else
			throw Exceptions::InternalError( "Edge trace error." );
	} else {
		if( sourceArg == source_ )
			return target_;
		if( sourceArg == target_ )
			return source_;
		else if( nullOnError )
			return NULL;
		else
			throw Exceptions::InternalError( "Edge trace error." );
	}
}

const Kernel::Node *
Lang::MultiEdge::trace( const Kernel::Node * sourceArg, const Ast::SourceLocation & callLoc ) const
{
	const Kernel::Node * res = trace( sourceArg, true );
	if( res != 0 )
		return res;
	throw Exceptions::EdgeTraceError( directed_ ? Exceptions::EdgeTraceError::TRACE : Exceptions::EdgeTraceError::UNDIRECTED,
																		source_->key( ), target_->key( ),
																		sourceArg->key( ),
																		callLoc );
}

const Kernel::Node *
Lang::MultiEdge::backtrace( const Kernel::Node * targetArg, bool nullOnError ) const
{
	if( directed_ ) {
		if( targetArg == target_ )
			return source_;
		else if( nullOnError )
			return NULL;
		else
			throw Exceptions::InternalError( "Edge trace error." );
	} else {
		if( targetArg == source_ )
			return target_;
		if( targetArg == target_ )
			return source_;
		else if( nullOnError )
			return NULL;
		else
			throw Exceptions::InternalError( "Edge trace error." );
	}
}

const Kernel::Node *
Lang::MultiEdge::backtrace( const Kernel::Node * targetArg, const Ast::SourceLocation & callLoc ) const
{
	const Kernel::Node * res = backtrace( targetArg, true );
	if( res != 0 )
		return res;
	throw Exceptions::EdgeTraceError( directed_ ? Exceptions::EdgeTraceError::BACKTRACE : Exceptions::EdgeTraceError::UNDIRECTED,
																		source_->key( ), target_->key( ),
																		targetArg->key( ),
																		callLoc );
}

RefCountPtr< const Lang::SingleList >
Lang::MultiEdge::edges( ) const
{
	/* Unfortunately, we can only access the edges in order of increasing node indices, and we want
	 * to return the edges in the same order.  This requires a temporary storage since the result
	 * must be built in reverse order.
	 */
	std::stack< const Kernel::Edge * > revStack;
	for( SetType::const_iterator i = begin_; i != end_; ++i ){
		revStack.push( *i );
	}
	RefCountPtr< const Lang::SingleList > res = Lang::THE_CONS_NULL;
	while( ! revStack.empty( ) ){
		res = Helpers::SingleList_cons( new Lang::Edge( graph_, revStack.top( ), directed_ ), res );
		revStack.pop( );
	}
	return res;
}

void
Lang::MultiEdge::show( std::ostream & os ) const
{
	if( directed_ ){
		os << "< directed multiedge ( " ;
		source_->key( )->show( os );
		os << ", " ;
		target_->key( )->show( os );
		os << " ) of multiplicity " << count_ << " >" ;
	}else{
		os << "< undirected multiedge ( " ;
		source_->key( )->show( os );
		os << ", " ;
		target_->key( )->show( os );
		os << " ) of multiplicity " << count_ << " >" ;
	}
}

Kernel::VariableHandle
Lang::MultiEdge::getField( const char * fieldID, const RefCountPtr< const Lang::Value > & selfRef ) const
{
	if( strcmp( fieldID, "directed?" ) == 0 )
		{
			return directed_ ? Kernel::THE_TRUE_VARIABLE : Kernel::THE_FALSE_VARIABLE;
		}
	if( strcmp( fieldID, "source" ) == 0 )
		{
			return Helpers::newValHandle( new Lang::Node( graph_, source_ ) );
		}
	if( strcmp( fieldID, "target" ) == 0 )
		{
			return Helpers::newValHandle( new Lang::Node( graph_, target_ ) );
		}
	if( strcmp( fieldID, "count" ) == 0 )
		{
			return Helpers::newValHandle( new Lang::Integer( count_ ) );
		}
	if( strcmp( fieldID, "edges" ) == 0 )
		{
			return Kernel::VariableHandle( new Kernel::Variable( edges( ) ) );
		}
	if( strcmp( fieldID, "u_edges" ) == 0 )
		{
			if( directed_ )
				throw Exceptions::MiscellaneousRequirement( "Illegal to access the u_edges field of a directed multiedge." );
			return Kernel::VariableHandle( new Kernel::Variable( edges( ) ) );
		}
	if( strcmp( fieldID, "d_edges" ) == 0 )
		{
			if( ! directed_ )
				throw Exceptions::MiscellaneousRequirement( "Illegal to access the d_edges field of an undirected multiedge." );
			return Kernel::VariableHandle( new Kernel::Variable( edges( ) ) );
		}
	return TypeID->getMethod( selfRef, fieldID ); /* This will throw if there is no such method. */
}

void
Lang::MultiEdge::gcMark( Kernel::GCMarkedSet & marked )
{
	const_cast< Lang::Graph * >( graph_.getPtr( ) )->gcMark( marked );
}


namespace Shapes
{
	namespace Lang
	{

		template< bool onlyCount, const char * fieldID >
		class GraphMethod_partition : public Lang::MethodBase< Lang::Graph >
		{
		public:
			GraphMethod_partition( RefCountPtr< const Lang::Graph > self, const Ast::FileID * fullMethodID );
			virtual ~GraphMethod_partition( );
			virtual void call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const;
			static const char * staticFieldID( ) { return fieldID; }
		};

		class GraphMethod_isNode : public Lang::MethodBase< Lang::Graph >
		{
		public:
			GraphMethod_isNode( RefCountPtr< const Lang::Graph > self, const Ast::FileID * fullMethodID );
			virtual ~GraphMethod_isNode( );
			virtual void call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const;
			static const char * staticFieldID( ) { return "node?"; }
		};

		class GraphMethod_isEdge : public Lang::MethodBase< Lang::Graph >
		{
		public:
			GraphMethod_isEdge( RefCountPtr< const Lang::Graph > self, const Ast::FileID * fullMethodID );
			virtual ~GraphMethod_isEdge( );
			virtual void call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const;
			static const char * staticFieldID( ) { return "edge?"; }
		};

		class GraphMethod_isKey : public Lang::MethodBase< Lang::Graph >
		{
		public:
			GraphMethod_isKey( RefCountPtr< const Lang::Graph > self, const Ast::FileID * fullMethodID );
			virtual ~GraphMethod_isKey( );
			virtual void call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const;
			static const char * staticFieldID( ) { return "key?"; }
		};

		class GraphMethod_indexNode : public Lang::MethodBase< Lang::Graph >
		{
		public:
			GraphMethod_indexNode( RefCountPtr< const Lang::Graph > self, const Ast::FileID * fullMethodID );
			virtual ~GraphMethod_indexNode( );
			virtual void call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const;
			static const char * staticFieldID( ) { return "index_node"; }
		};

		class GraphMethod_indexEdge : public Lang::MethodBase< Lang::Graph >
		{
		public:
			GraphMethod_indexEdge( RefCountPtr< const Lang::Graph > self, const Ast::FileID * fullMethodID );
			virtual ~GraphMethod_indexEdge( );
			virtual void call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const;
			static const char * staticFieldID( ) { return "index_edge"; }
		};

		class GraphMethod_find_node : public Lang::MethodBase< Lang::Graph >
		{
		public:
			GraphMethod_find_node( RefCountPtr< const Lang::Graph > self, const Ast::FileID * fullMethodID );
			virtual ~GraphMethod_find_node( );
			virtual void call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const;
			static const char * staticFieldID( ) { return "find_node"; }
		};

		template< bool directed, bool undirected, const char * fieldID >
		class GraphMethod_find_edges : public Lang::MethodBase< Lang::Graph >
		{
		public:
			GraphMethod_find_edges( RefCountPtr< const Lang::Graph > self, const Ast::FileID * fullMethodID );
			virtual ~GraphMethod_find_edges( );
			virtual void call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const;
			static const char * staticFieldID( ) { return fieldID; }
		};

		template< bool directed, bool undirected, const char * fieldID >
		class GraphMethod_find_multiedges : public Lang::MethodBase< Lang::Graph >
		{
		public:
			GraphMethod_find_multiedges( RefCountPtr< const Lang::Graph > self, const Ast::FileID * fullMethodID );
			virtual ~GraphMethod_find_multiedges( );
			virtual void call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const;
			static const char * staticFieldID( ) { return fieldID; }
		};

		template< bool directed, bool undirected, const char * fieldID >
		class GraphMethod_the_edge : public Lang::MethodBase< Lang::Graph >
		{
		public:
			GraphMethod_the_edge( RefCountPtr< const Lang::Graph > self, const Ast::FileID * fullMethodID );
			virtual ~GraphMethod_the_edge( );
			virtual void call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const;
			static const char * staticFieldID( ) { return fieldID; }
		};

		template< bool directed, bool undirected, const char * fieldID >
		class GraphMethod_the_multiedge : public Lang::MethodBase< Lang::Graph >
		{
		public:
			GraphMethod_the_multiedge( RefCountPtr< const Lang::Graph > self, const Ast::FileID * fullMethodID );
			virtual ~GraphMethod_the_multiedge( );
			virtual void call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const;
			static const char * staticFieldID( ) { return fieldID; }
		};

		template< Kernel::Graph::ItemType item, const char * fieldID >
		class GraphMethod_with_values : public Lang::MethodBase< Lang::Graph >
		{
		public:
			GraphMethod_with_values( RefCountPtr< const Lang::Graph > self, const Ast::FileID * fullMethodID );
			virtual ~GraphMethod_with_values( );
			virtual void call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const;
			static const char * staticFieldID( ) { return fieldID; }
		};

		class GraphMethod_rekey_with_index : public Lang::MethodBase< Lang::Graph >
		{
		public:
			GraphMethod_rekey_with_index( RefCountPtr< const Lang::Graph > self, const Ast::FileID * fullMethodID );
			virtual ~GraphMethod_rekey_with_index( );
			virtual void call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const;
			static const char * staticFieldID( ) { return "rekey_with_index"; }
		};

		template< Kernel::Graph::SubgraphKind kind, const char * fieldID >
		class GraphMethod_subgraph : public Lang::MethodBase< Lang::Graph >
		{
		public:
			GraphMethod_subgraph( RefCountPtr< const Lang::Graph > self, const Ast::FileID * fullMethodID );
			virtual ~GraphMethod_subgraph( );
			virtual void call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const;
			static const char * staticFieldID( ) { return fieldID; }
		};

	}

	namespace Kernel
	{
		char FIELD_ID_partition[]         = "partition";
		char FIELD_ID_partition_node_count[] = "partition_node_count";
		char FIELD_ID_find_u_edges[]      = "find_u_edges";
		char FIELD_ID_find_d_edges[]      = "find_d_edges";
		char FIELD_ID_the_u_edge[]        = "the_u_edge";
		char FIELD_ID_the_d_edge[]        = "the_d_edge";
		char FIELD_ID_find_u_multiedges[] = "find_u_multiedges";
		char FIELD_ID_find_d_multiedges[] = "find_d_multiedges";
		char FIELD_ID_the_u_multiedge[]   = "the_u_multiedge";
		char FIELD_ID_the_d_multiedge[]   = "the_d_multiedge";
		char FIELD_ID_with_node_values[]  = "with_node_values";
		char FIELD_ID_with_edge_values[]  = "with_edge_values";
		char FIELD_ID_induced_subgraph[]  = "induced_subgraph";
		char FIELD_ID_spanning_subgraph[] = "spanning_subgraph";
	}
}

Kernel::Graph::Graph( const Kernel::GraphDomain & domain, const ListRef & partitionKeys, const std::list< Kernel::NodeData > & nodeData, bool hasEdgeLabels, const std::list< Kernel::EdgeDataIndexed > & uedgeData, const std::list< Kernel::EdgeDataIndexed > & dedgeData, const std::list< Kernel::LoopDataIndexed > & uloopData, const std::list< Kernel::LoopDataIndexed > & dloopData, RefCountPtr< const ValueVector > * nodeValues, RefCountPtr< const ValueVector > * edgeValues, const Ast::SourceLocation & callLoc )
	: domain_( domain ), partitionKeys_( partitionKeys ), keyIsRange_( true ), keyOffset_( 0 ),
	  edgeLabels_( NullPtr< std::vector< Kernel::ValueRef > >( ) ), nodePartitions_( NullPtr< std::vector< Kernel::ValueRef > >( ) )
{
	size_t nCount = nodeData.size( );
	size_t eCount = uedgeData.size( ) + dedgeData.size( ) + uloopData.size( ) + dloopData.size( );

	bool partitionedCache = partitioned( ); /* For fast access. */

	nodes_.reserve( nCount );
	uedges_.reserve( uedgeData.size( ) );
	dedges_.reserve( dedgeData.size( ) );
	uloops_.reserve( uloopData.size( ) );
	dloops_.reserve( dloopData.size( ) );

	RefCountPtr< ValueVector > nValues = RefCountPtr< ValueVector >( NullPtr< ValueVector >( ) );
	RefCountPtr< ValueVector > eValues = RefCountPtr< ValueVector >( NullPtr< ValueVector >( ) );

	if( hasEdgeLabels ){
		edgeLabels_ = RefCountPtr< std::vector< Kernel::ValueRef > >( new std::vector< Kernel::ValueRef >( ) );
		edgeLabels_->reserve( eCount );
	}

	if( partitionedCache ){
		nodePartitions_ = RefCountPtr< ValueVector >( new ValueVector( nCount, NullPtr< const Lang::Value >( ) ) );
	}

	{
		typedef typeof nodeData ListType;
		size_t ni = 0;
		for( ListType::const_iterator n = nodeData.begin( ); n != nodeData.end( ); ++n, ++ni ){
			nodes_.push_back( new Kernel::Node( ni, n->key_ ) );
			if( n->value_ != NullPtr< const Lang::Value >( ) ){
				if( nValues == NullPtr< ValueVector >( ) ){
					nValues = RefCountPtr< ValueVector >( new ValueVector( nCount, NullPtr< const Lang::Value >( ) ) );
				}
				(*nValues)[ ni ] = n->value_;
			}
			if( partitionedCache ){
				/* The caller of this constructor is responsible for the content of the partition_ member
				 * to be correct if the graph is partitioned.
				 */
				(*nodePartitions_)[ ni ] = n->partition_;
			}
		}
	}

	/* The edge indices are determined by the order in which the various kinds of edges are processed here.
	 * Note that this order must be reflected correctly in the indexEdge method, and must match the order
	 * used in other graph constructors.
	 */
	size_t ei = 0;
	{
		typedef typeof uedgeData ListType;
		for( ListType::const_iterator e = uedgeData.begin( ); e != uedgeData.end( ); ++e, ++ei ){
			if( e->source_ >= nCount || e->target_ >= nCount )
				throw Exceptions::InternalError( "Lang::Graph::Graph: Node index in undirected edge is out of range." );
			uedges_.push_back( new Kernel::Edge( nodes_[e->source_], nodes_[e->target_], ei, false ) );
			if( e->value_ != NullPtr< const Lang::Value >( ) ){
				if( eValues == NullPtr< ValueVector >( ) ){
					eValues = RefCountPtr< ValueVector >( new ValueVector( eCount, NullPtr< const Lang::Value >( ) ) );
				}
				(*eValues)[ ei ] = e->value_;
			}
			if( hasEdgeLabels ){
				edgeLabels_->push_back( e->label_ );
			}
		}
	}
	{
		typedef typeof dedgeData ListType;
		for( ListType::const_iterator e = dedgeData.begin( ); e != dedgeData.end( ); ++e, ++ei ){
			if( e->source_ >= nCount || e->target_ >= nCount )
				throw Exceptions::InternalError( "Lang::Graph::Graph: Node index in directed edge is out of range." );
			dedges_.push_back( new Kernel::Edge( nodes_[e->source_], nodes_[e->target_], ei, true) );
			if( e->value_ != NullPtr< const Lang::Value >( ) ){
				if( eValues == NullPtr< ValueVector >( ) ){
					eValues = RefCountPtr< ValueVector >( new ValueVector( eCount, NullPtr< const Lang::Value >( ) ) );
				}
				(*eValues)[ ei ] = e->value_;
			}
			if( hasEdgeLabels ){
				edgeLabels_->push_back( e->label_ );
			}
		}
	}
	{
		typedef typeof uloopData ListType;
		for( ListType::const_iterator e = uloopData.begin( ); e != uloopData.end( ); ++e, ++ei ){
			if( e->node_ >= nCount )
				throw Exceptions::InternalError( "Lang::Graph::Graph: Node index in undirected loop is out of range." );
			uedges_.push_back( new Kernel::Edge( nodes_[e->node_], nodes_[e->node_], ei, false ) );
			if( e->value_ != NullPtr< const Lang::Value >( ) ){
				if( eValues == NullPtr< ValueVector >( ) ){
					eValues = RefCountPtr< ValueVector >( new ValueVector( eCount, NullPtr< const Lang::Value >( ) ) );
				}
				(*eValues)[ ei ] = e->value_;
			}
			if( hasEdgeLabels ){
				edgeLabels_->push_back( e->label_ );
			}
		}
	}
	{
		typedef typeof dloopData ListType;
		for( ListType::const_iterator e = dloopData.begin( ); e != dloopData.end( ); ++e, ++ei ){
			if( e->node_ >= nCount )
				throw Exceptions::InternalError( "Lang::Graph::Graph: Node index in directed loop is out of range." );
			uedges_.push_back( new Kernel::Edge( nodes_[e->node_], nodes_[e->node_], ei, true ) );
			if( e->value_ != NullPtr< const Lang::Value >( ) ){
				if( eValues == NullPtr< ValueVector >( ) ){
					eValues = RefCountPtr< ValueVector >( new ValueVector( eCount, NullPtr< const Lang::Value >( ) ) );
				}
				(*eValues)[ ei ] = e->value_;
			}
			if( hasEdgeLabels ){
				edgeLabels_->push_back( e->label_ );
			}
		}
	}

	*nodeValues = nValues;
	*edgeValues = eValues;

	initializeKeyRangeOffset( );
	initializeKeyMaps( );
	initializeAndCheckPartitions( callLoc );
	addEdgesToNodes( );
}

Kernel::Graph::Graph( const Kernel::Graph & orig, const std::set< size_t > & subgraphIndices, SubgraphKind kind,
											const RefCountPtr< const ValueVector > & origNodeValues, const RefCountPtr< const ValueVector > & origEdgeValues,
											RefCountPtr< const ValueVector > * nodeValues, RefCountPtr< const ValueVector > * edgeValues )
	: domain_( orig.domain_ ), partitionKeys_( orig.partitionKeys_ ), keyIsRange_( true ), keyOffset_( 0 ),
	  edgeLabels_( NullPtr< std::vector< Kernel::ValueRef > >( ) ), nodePartitions_( NullPtr< std::vector< Kernel::ValueRef > >( ) )
{
	switch( kind )
		{
		case INDUCED:
			initSubgraphInduced( orig, subgraphIndices, origNodeValues, origEdgeValues, nodeValues, edgeValues );
			break;
		case SPANNING:
			initSubgraphSpanning( orig, subgraphIndices, origNodeValues, origEdgeValues, nodeValues, edgeValues );
			break;
		}

	initializeKeyRangeOffset( );
	initializeKeyMaps( );
	initializeAndCheckPartitions( Ast::THE_UNKNOWN_LOCATION ); /* The call location should never be used here, since nothing can go wrong. */
	addEdgesToNodes( );
}

void
Kernel::Graph::initSubgraphInduced( const Kernel::Graph & orig, const std::set< size_t > & subgraphNodeIndices, const RefCountPtr< const ValueVector > & origNodeValues, const RefCountPtr< const ValueVector > & origEdgeValues, RefCountPtr< const ValueVector > * nodeValues, RefCountPtr< const ValueVector > * edgeValues )
{
	size_t nCount = subgraphNodeIndices.size( );

	typedef std::map< size_t, size_t > IndexMap;
	IndexMap indexMap;

	bool hasNodeValues = origNodeValues != NullPtr< const ValueVector >( );
	bool hasNodePartitions = partitioned( );
	bool hasEdgeValues = origEdgeValues != NullPtr< const ValueVector >( );
	bool hasEdgeLabels = orig.edgeLabels_ != NullPtr< std::vector< Kernel::ValueRef > >( );

	RefCountPtr< ValueVector > nValues = RefCountPtr< ValueVector >( NullPtr< ValueVector >( ) );
	RefCountPtr< ValueVector > eValues = RefCountPtr< ValueVector >( NullPtr< ValueVector >( ) );

	if( hasNodeValues ){
		nValues = RefCountPtr< ValueVector >( new ValueVector( nCount, NullPtr< const Lang::Value >( ) ) );
	}
	if( hasNodePartitions ){
		nodePartitions_ = RefCountPtr< ValueVector >( new ValueVector( nCount, NullPtr< const Lang::Value >( ) ) );
	}

	nodes_.reserve( nCount );
	{
		typedef typeof subgraphNodeIndices SetType;
		size_t ni = 0;
		for( SetType::const_iterator i = subgraphNodeIndices.begin( ); i != subgraphNodeIndices.end( ); ++i, ++ni ){
			const Kernel::Node * orign = orig.nodes_[ *i ];
			nodes_.push_back( new Kernel::Node( ni, orign->key( ) ) );
			indexMap[ *i ] = ni;
			if( hasNodeValues ){
				(*nValues)[ ni ] = (*origNodeValues)[ *i ];
			}
			if( hasNodePartitions ){
				(*nodePartitions_)[ ni ] = (*orig.nodePartitions_)[ *i ];
			}
		}
	}

	std::list< size_t > origEdgeIndices;

	/* The edge indices are determined by the order in which the various kinds of edges are processed here.
	 * Note that this order must be reflected correctly in the indexEdge method, and must match the order
	 * used in other graph constructors.
	 */
	size_t ei = 0;
	for( EdgeVector::const_iterator e = orig.uedges_.begin( ); e != orig.uedges_.end( ); ++e ){
		IndexMap::const_iterator iSource = indexMap.find( (*e)->source( )->index( ) );
		if( iSource == indexMap.end( ) )
			continue;
		IndexMap::const_iterator iTarget = indexMap.find( (*e)->target( )->index( ) );
		if( iTarget == indexMap.end( ) )
			continue;
		uedges_.push_back( new Kernel::Edge( nodes_[ iSource->second ], nodes_[ iTarget->second ], ei, false ) );
		if( hasEdgeValues ){
			origEdgeIndices.push_back( (*e)->index( ) );
		}
		++ei;
	}
	for( EdgeVector::const_iterator e = orig.dedges_.begin( ); e != orig.dedges_.end( ); ++e ){
		IndexMap::const_iterator iSource = indexMap.find( (*e)->source( )->index( ) );
		if( iSource == indexMap.end( ) )
			continue;
		IndexMap::const_iterator iTarget = indexMap.find( (*e)->target( )->index( ) );
		if( iTarget == indexMap.end( ) )
			continue;
		dedges_.push_back( new Kernel::Edge( nodes_[ iSource->second ], nodes_[ iTarget->second ], ei, true ) );
		if( hasEdgeValues ){
			origEdgeIndices.push_back( (*e)->index( ) );
		}
		++ei;
	}
	for( EdgeVector::const_iterator e = orig.uloops_.begin( ); e != orig.uloops_.end( ); ++e ){
		IndexMap::const_iterator iSource = indexMap.find( (*e)->source( )->index( ) );
		if( iSource == indexMap.end( ) )
			continue;
		uloops_.push_back( new Kernel::Edge( nodes_[ iSource->second ], nodes_[ iSource->second ], ei, false ) );
		if( hasEdgeValues ){
			origEdgeIndices.push_back( (*e)->index( ) );
		}
		++ei;
	}
	for( EdgeVector::const_iterator e = orig.dloops_.begin( ); e != orig.dloops_.end( ); ++e ){
		IndexMap::const_iterator iSource = indexMap.find( (*e)->source( )->index( ) );
		if( iSource == indexMap.end( ) )
			continue;
		dloops_.push_back( new Kernel::Edge( nodes_[ iSource->second ], nodes_[ iSource->second ], ei, true ) );
		if( hasEdgeValues ){
			origEdgeIndices.push_back( (*e)->index( ) );
		}
		++ei;
	}

	if( hasEdgeValues ){
		eValues = RefCountPtr< ValueVector >( new ValueVector( ei, NullPtr< const Lang::Value >( ) ) );
		typedef typeof origEdgeIndices ListType;
		ValueVector::iterator dst = eValues->begin( );
		for( ListType::const_iterator i = origEdgeIndices.begin( ); i != origEdgeIndices.end( ); ++i, ++dst ){
			*dst = (*origEdgeValues)[ *i ];
		}
	}
	if( hasEdgeLabels ){
		edgeLabels_ = RefCountPtr< std::vector< Kernel::ValueRef > >( new std::vector< Kernel::ValueRef >( ei, NullPtr< const Lang::Value >( ) ) );
		const std::vector< Kernel::ValueRef > & origEdgeLabels = *(orig.edgeLabels_);
		typedef typeof origEdgeIndices ListType;
		std::vector< Kernel::ValueRef >::iterator dst = edgeLabels_->begin( );
		for( ListType::const_iterator i = origEdgeIndices.begin( ); i != origEdgeIndices.end( ); ++i, ++dst ){
			*dst = origEdgeLabels[ *i ];
		}
	}

	*nodeValues = nValues;
	*edgeValues = eValues;
}

void
Kernel::Graph::initSubgraphSpanning( const Kernel::Graph & orig, const std::set< size_t > & subgraphEdgeIndices, const RefCountPtr< const ValueVector > & origNodeValues, const RefCountPtr< const ValueVector > & origEdgeValues, RefCountPtr< const ValueVector > * nodeValues, RefCountPtr< const ValueVector > * edgeValues )
{
	const std::vector< Kernel::ValueRef > * origEdgeLabels = orig.edgeLabels_.getPtr( );

	bool hasEdgeValues = origEdgeValues != NullPtr< const ValueVector >( );
	bool hasEdgeLabels = origEdgeLabels != 0;

	nodes_.reserve( orig.nodes_.size( ) );
	{
		for( NodeVector::const_iterator n = orig.nodes_.begin( ); n != orig.nodes_.end( ); ++n ) {
			nodes_.push_back( new Kernel::Node( (*n)->index( ), (*n)->key( ) ) );
		}
	}

	*nodeValues = origNodeValues;
	nodePartitions_ = orig.nodePartitions_;

	RefCountPtr< ValueVector > eValues = RefCountPtr< ValueVector >( NullPtr< ValueVector >( ) );
	if( hasEdgeValues ){
		eValues = RefCountPtr< ValueVector >( new ValueVector( subgraphEdgeIndices.size( ), NullPtr< const Lang::Value >( ) ) );
	}
	if( hasEdgeLabels ){
		edgeLabels_ = RefCountPtr< std::vector< Kernel::ValueRef > >( new std::vector< Kernel::ValueRef >( subgraphEdgeIndices.size( ), NullPtr< const Lang::Value >( ) ) );
	}

	typedef typeof subgraphEdgeIndices SetType;
	SetType::const_iterator i = subgraphEdgeIndices.begin( );

	size_t ei = 0;
	size_t offset = 0;
	size_t iMax = orig.uedges_.size( );
	for( ; i != subgraphEdgeIndices.end( ) && *i < iMax; ++i, ++ei ){
		const Kernel::Edge * orige = orig.uedges_[ *i - offset ];
		uedges_.push_back( new Kernel::Edge( nodes_[ orige->source( )->index( ) ], nodes_[ orige->target( )->index( ) ], ei, false ) );
		if( hasEdgeValues ){
			(*eValues)[ ei ] = (*origEdgeValues)[ orige->index( ) ];
		}
		if( hasEdgeLabels ){
			(*edgeLabels_)[ ei ] = (*origEdgeLabels)[ orige->index( ) ];
		}
	}

	offset = iMax;
	iMax += orig.dedges_.size( );
	for( ; i != subgraphEdgeIndices.end( ) && *i < iMax; ++i, ++ei ){
		const Kernel::Edge * orige = orig.dedges_[ *i - offset ];
		dedges_.push_back( new Kernel::Edge( nodes_[ orige->source( )->index( ) ], nodes_[ orige->target( )->index( ) ], ei, true ) );
		if( hasEdgeValues ){
			(*eValues)[ ei ] = (*origEdgeValues)[ orige->index( ) ];
		}
		if( hasEdgeLabels ){
			(*edgeLabels_)[ ei ] = (*origEdgeLabels)[ orige->index( ) ];
		}
	}

	offset = iMax;
	iMax += orig.uloops_.size( );
	for( ; i != subgraphEdgeIndices.end( ) && *i < iMax; ++i, ++ei ){
		const Kernel::Edge * orige = orig.uloops_[ *i - offset ];
		uloops_.push_back( new Kernel::Edge( nodes_[ orige->source( )->index( ) ], nodes_[ orige->target( )->index( ) ], ei, false ) );
		if( hasEdgeValues ){
			(*eValues)[ ei ] = (*origEdgeValues)[ orige->index( ) ];
		}
		if( hasEdgeLabels ){
			(*edgeLabels_)[ ei ] = (*origEdgeLabels)[ orige->index( ) ];
		}
	}

	offset = iMax;
	iMax += orig.dloops_.size( );
	for( ; i != subgraphEdgeIndices.end( ) && *i < iMax; ++i, ++ei ){
		const Kernel::Edge * orige = orig.dloops_[ *i - offset ];
		dloops_.push_back( new Kernel::Edge( nodes_[ orige->source( )->index( ) ], nodes_[ orige->target( )->index( ) ], ei, true ) );
		if( hasEdgeValues ){
			(*eValues)[ ei ] = (*origEdgeValues)[ orige->index( ) ];
		}
		if( hasEdgeLabels ){
			(*edgeLabels_)[ ei ] = (*origEdgeLabels)[ orige->index( ) ];
		}
	}

	if( i != subgraphEdgeIndices.end( ) )
		throw Exceptions::InternalError( "Lang::Graph::initSubgraphSpanning: subgraph edge indices out of range" );

	*edgeValues = eValues;
}

Kernel::Graph::Graph( const Kernel::Graph & orig, Lang::Integer::ValueType offset )
	: domain_( orig.domain_ ), partitionKeys_( orig.partitionKeys_ ), keyIsRange_( true ), keyOffset_( offset ),
	  edgeLabels_( orig.edgeLabels_ ), nodePartitions_( orig.nodePartitions_ )
{
	/* Rekey with index. */

	nodes_.reserve( orig.nodes_.size( ) );
	uedges_.reserve( orig.uedges_.size( ) );
	dedges_.reserve( orig.dedges_.size( ) );
	uloops_.reserve( orig.uloops_.size( ) );
	dloops_.reserve( orig.dloops_.size( ) );

	{
		size_t ni = 0;
		for( NodeVector::const_iterator n = orig.nodes_.begin( ); n != orig.nodes_.end( ); ++n, ++ni ) {
			nodes_.push_back( new Kernel::Node( ni, Kernel::ValueRef( new Lang::Integer( ni + offset ) ) ) );
		}
	}

	/* The edge indices are determined by the order in which the various kinds of edges are processed here.
	 * Note that this order must be reflected correctly in the indexEdge method, and must match the order
	 * used in other graph constructors.
	 */
	for( EdgeVector::const_iterator e = orig.uedges_.begin( ); e != orig.uedges_.end( ); ++e ){
		uedges_.push_back( new Kernel::Edge( nodes_[ (*e)->source( )->index( ) ], nodes_[ (*e)->target( )->index( ) ], (*e)->index( ), false ) );
	}
	for( EdgeVector::const_iterator e = orig.dedges_.begin( ); e != orig.dedges_.end( ); ++e ){
		dedges_.push_back( new Kernel::Edge( nodes_[ (*e)->source( )->index( ) ], nodes_[ (*e)->target( )->index( ) ], (*e)->index( ), true ) );
	}
	for( EdgeVector::const_iterator e = orig.uloops_.begin( ); e != orig.uloops_.end( ); ++e ){
		uloops_.push_back( new Kernel::Edge( nodes_[ (*e)->source( )->index( ) ], nodes_[ (*e)->source( )->index( ) ], (*e)->index( ), false ) );
	}
	for( EdgeVector::const_iterator e = orig.dloops_.begin( ); e != orig.dloops_.end( ); ++e ){
		dloops_.push_back( new Kernel::Edge( nodes_[ (*e)->source( )->index( ) ], nodes_[ (*e)->source( )->index( ) ], (*e)->index( ), true ) );
	}

	initializeKeyMaps( );
	initializeAndCheckPartitions( Ast::THE_UNKNOWN_LOCATION ); /* The call location should never be used here, since nothing can go wrong. */
	addEdgesToNodes( );
}

Kernel::Graph::~Graph( )
{
	for( EdgeVector::iterator e = uedges_.begin( ); e != uedges_.end( ); ++e )
		delete *e;
	for( EdgeVector::iterator e = uloops_.begin( ); e != uloops_.end( ); ++e )
		delete *e;
	for( EdgeVector::iterator e = dedges_.begin( ); e != dedges_.end( ); ++e )
		delete *e;
	for( EdgeVector::iterator e = dloops_.begin( ); e != dloops_.end( ); ++e )
		delete *e;
	for( NodeVector::iterator n = nodes_.begin( ); n != nodes_.end( ); ++n )
		delete *n;
}

void
Graph_register_methods( Lang::SystemFinalClass * dstClass )
{
	dstClass->registerMethod( new Kernel::MethodFactory< Lang::Graph, Lang::GraphMethod_partition< false, Kernel::FIELD_ID_partition > >( ) );
	dstClass->registerMethod( new Kernel::MethodFactory< Lang::Graph, Lang::GraphMethod_partition< true, Kernel::FIELD_ID_partition_node_count > >( ) );
	dstClass->registerMethod( new Kernel::MethodFactory< Lang::Graph, Lang::GraphMethod_isNode >( ) );
	dstClass->registerMethod( new Kernel::MethodFactory< Lang::Graph, Lang::GraphMethod_isEdge >( ) );
	dstClass->registerMethod( new Kernel::MethodFactory< Lang::Graph, Lang::GraphMethod_isKey >( ) );
	dstClass->registerMethod( new Kernel::MethodFactory< Lang::Graph, Lang::GraphMethod_indexNode >( ) );
	dstClass->registerMethod( new Kernel::MethodFactory< Lang::Graph, Lang::GraphMethod_indexEdge >( ) );
	dstClass->registerMethod( new Kernel::MethodFactory< Lang::Graph, Lang::GraphMethod_find_node >( ) );
	dstClass->registerMethod( new Kernel::MethodFactory< Lang::Graph, Lang::GraphMethod_find_edges< false, true, Kernel::FIELD_ID_find_u_edges > >( ) );
	dstClass->registerMethod( new Kernel::MethodFactory< Lang::Graph, Lang::GraphMethod_find_edges< true, false, Kernel::FIELD_ID_find_d_edges > >( ) );
	dstClass->registerMethod( new Kernel::MethodFactory< Lang::Graph, Lang::GraphMethod_the_edge< false, true, Kernel::FIELD_ID_the_u_edge > >( ) );
	dstClass->registerMethod( new Kernel::MethodFactory< Lang::Graph, Lang::GraphMethod_the_edge< true, false, Kernel::FIELD_ID_the_d_edge > >( ) );

	dstClass->registerMethod( new Kernel::MethodFactory< Lang::Graph, Lang::GraphMethod_find_multiedges< false, true, Kernel::FIELD_ID_find_u_multiedges > >( ) );
	dstClass->registerMethod( new Kernel::MethodFactory< Lang::Graph, Lang::GraphMethod_find_multiedges< true, false, Kernel::FIELD_ID_find_d_multiedges > >( ) );
	dstClass->registerMethod( new Kernel::MethodFactory< Lang::Graph, Lang::GraphMethod_the_multiedge< false, true, Kernel::FIELD_ID_the_u_multiedge > >( ) );
	dstClass->registerMethod( new Kernel::MethodFactory< Lang::Graph, Lang::GraphMethod_the_multiedge< true, false, Kernel::FIELD_ID_the_d_multiedge > >( ) );

	dstClass->registerMethod( new Kernel::MethodFactory< Lang::Graph, Lang::GraphMethod_with_values< Kernel::Graph::NODE, Kernel::FIELD_ID_with_node_values > >( ) );
	dstClass->registerMethod( new Kernel::MethodFactory< Lang::Graph, Lang::GraphMethod_with_values< Kernel::Graph::EDGE, Kernel::FIELD_ID_with_edge_values > >( ) );
	dstClass->registerMethod( new Kernel::MethodFactory< Lang::Graph, Lang::GraphMethod_rekey_with_index >( ) );
	dstClass->registerMethod( new Kernel::MethodFactory< Lang::Graph, Lang::GraphMethod_subgraph< Kernel::Graph::INDUCED, Kernel::FIELD_ID_induced_subgraph > >( ) );
	dstClass->registerMethod( new Kernel::MethodFactory< Lang::Graph, Lang::GraphMethod_subgraph< Kernel::Graph::SPANNING, Kernel::FIELD_ID_spanning_subgraph > >( ) );
}

RefCountPtr< const Lang::Class > Lang::Graph::TypeID( new Lang::SystemFinalClass( strrefdup( "Graph" ), Graph_register_methods ) );
TYPEINFOIMPL( Graph );

Lang::Graph::Graph( const RefCountPtr< const Kernel::Graph > & graph )
  : graph_( graph ), nodeValues_( NullPtr< const ValueVector >( ) ), edgeValues_( NullPtr< const ValueVector >( ) )
{ }

Lang::Graph::Graph( const RefCountPtr< const Kernel::Graph > & graph, const RefCountPtr< const ValueVector > & nodeValues, const RefCountPtr< const ValueVector > & edgeValues )
  : graph_( graph ), nodeValues_( nodeValues ), edgeValues_( edgeValues )
{ }

Lang::Graph::Graph( const Lang::Graph & orig, const std::set< size_t > & subgraphIndices, Kernel::Graph::SubgraphKind kind )
	: graph_( NullPtr< const Kernel::Graph >( ) ), nodeValues_( NullPtr< const ValueVector >( ) ), edgeValues_( NullPtr< const ValueVector >( ) )
{
	graph_ = RefCountPtr< const Kernel::Graph >( new Kernel::Graph( *orig.graph( ), subgraphIndices, kind, orig.nodeValues_, orig.edgeValues_, & nodeValues_, & edgeValues_ ) );
}

Lang::Graph::Graph( const Lang::Graph & orig, Lang::Integer::ValueType offset )
	: graph_( new Kernel::Graph( *orig.graph( ), offset ) ), nodeValues_( orig.nodeValues_ ), edgeValues_( orig.edgeValues_ )
{ }

Lang::Graph::~Graph( )
{ }

void
Lang::Graph::setNodeValues( const RefCountPtr< const ValueVector > nodeValues )
{
	nodeValues_ = nodeValues;
}

void
Lang::Graph::setNodeValues( const std::list< Kernel::NodeData > & nodeData )
{
	throw Exceptions::NotImplemented( "Lang::Graph::setNodeValues given NodeData" );
}

void
Lang::Graph::setEdgeValues( const RefCountPtr< const ValueVector > edgeValues )
{
	edgeValues_ = edgeValues;
}

void
Lang::Graph::setEdgeValues( const std::list< Kernel::EdgeDataIndexed > & uedgeData, const std::list< Kernel::EdgeDataIndexed > & dedgeData, const std::list< Kernel::LoopDataIndexed > & uloopData, const std::list< Kernel::LoopDataIndexed > & dloopData )
{
	throw Exceptions::NotImplemented( "Lang::Graph::setEdgeValues given EdgeData" );
}

void
Lang::Graph::show( std::ostream & os ) const
{
	graph_->show( os );
}

Kernel::VariableHandle
Lang::Graph::getField( const char * fieldID, const RefCountPtr< const Lang::Value > & selfRef ) const
{
	if( strcmp( fieldID, "undirected?" ) == 0 )
		{
			return graph_->domain( ).directed( ) ? Kernel::THE_FALSE_VARIABLE : Kernel::THE_TRUE_VARIABLE;
		}
	if( strcmp( fieldID, "directed?" ) == 0 )
		{
			return graph_->domain( ).undirected( ) ? Kernel::THE_FALSE_VARIABLE : Kernel::THE_TRUE_VARIABLE;
		}
	if( strcmp( fieldID, "mixed?" ) == 0 )
		{
			return ( graph_->domain( ).undirected( ) && graph_->domain( ).directed( ) ) ? Kernel::THE_TRUE_VARIABLE : Kernel::THE_FALSE_VARIABLE;
		}
	if( strcmp( fieldID, "loops?" ) == 0 )
		{
			return graph_->domain( ).loops( ) ? Kernel::THE_TRUE_VARIABLE : Kernel::THE_FALSE_VARIABLE;
		}
	if( strcmp( fieldID, "parallel?" ) == 0 )
		{
			return graph_->domain( ).parallel( ) ? Kernel::THE_TRUE_VARIABLE : Kernel::THE_FALSE_VARIABLE;
		}
	if( strcmp( fieldID, "partitioned?" ) == 0 )
		{
			return graph_->partitioned( ) ? Kernel::THE_TRUE_VARIABLE : Kernel::THE_FALSE_VARIABLE;
		}
	if( strcmp( fieldID, "key_is_index?" ) == 0 )
		{
			return ( graph_->keyIsRange( ) && graph_->keyOffset( ) == 0 ) ? Kernel::THE_TRUE_VARIABLE : Kernel::THE_FALSE_VARIABLE;
		}

	if( strcmp( fieldID, "node_count" ) == 0 )
		{
			return Helpers::newValHandle( new Lang::Integer( graph_->nodes( ).size( ) ) );
		}
	if( strcmp( fieldID, "u_edge_count" ) == 0 )
		{
			return Helpers::newValHandle( new Lang::Integer( graph_->uedges( ).size( ) + graph_->uloops( ).size( ) ) );
		}
	if( strcmp( fieldID, "d_edge_count" ) == 0 )
		{
			return Helpers::newValHandle( new Lang::Integer( graph_->dedges( ).size( ) + graph_->dloops( ).size( ) ) );
		}
	if( strcmp( fieldID, "edge_count" ) == 0 )
		{
			return Helpers::newValHandle( new Lang::Integer( graph_->uedges( ).size( ) + graph_->uloops( ).size( ) + graph_->dedges( ).size( ) + graph_->dloops( ).size( ) ) );
		}
	if( strcmp( fieldID, "u_loop_count" ) == 0 )
		{
			return Helpers::newValHandle( new Lang::Integer( graph_->uloops( ).size( ) ) );
		}
	if( strcmp( fieldID, "d_loop_count" ) == 0 )
		{
			return Helpers::newValHandle( new Lang::Integer( graph_->dloops( ).size( ) ) );
		}
	if( strcmp( fieldID, "loop_count" ) == 0 )
		{
			return Helpers::newValHandle( new Lang::Integer( graph_->uloops( ).size( ) + graph_->dloops( ).size( ) ) );
		}

	if( strcmp( fieldID, "partition_count" ) == 0 )
		{
			return Helpers::newValHandle( new Lang::Integer( graph_->partitioned( ) ? graph_->partitionCount( ) : 0 ) );
		}
	if( strcmp( fieldID, "partitions" ) == 0 )
		{
			if( ! graph_->partitioned( ) )
				return Kernel::THE_NIL_VARIABLE;
			return Kernel::VariableHandle( new Kernel::Variable( graph_->partitionKeys( ) ) );
		}
	if( strcmp( fieldID, "nodes" ) == 0 )
		{
			RefCountPtr< const Lang::Graph > selfRefTyped = Helpers::down_cast_internal< const Lang::Graph >( selfRef );
			Kernel::Node::ListRef nodes = Lang::THE_CONS_NULL;
			nodes = prepend_nodes( graph_->nodes( ), nodes, selfRefTyped );
			return Kernel::VariableHandle( new Kernel::Variable( nodes ) );
		}
	if( strcmp( fieldID, "u_edges" ) == 0 )
		{
			RefCountPtr< const Lang::Graph > selfRefTyped = Helpers::down_cast_internal< const Lang::Graph >( selfRef );
			ListRef edges = Lang::THE_CONS_NULL;
			edges = prepend_edges( graph_->uloops( ), false, edges, selfRefTyped );
			edges = prepend_edges( graph_->uedges( ), false, edges, selfRefTyped );
			return Kernel::VariableHandle( new Kernel::Variable( edges ) );
		}
	if( strcmp( fieldID, "u_multiedges" ) == 0 )
		{
			RefCountPtr< const Lang::Graph > selfRefTyped = Helpers::down_cast_internal< const Lang::Graph >( selfRef );
			ListRef edges = Lang::THE_CONS_NULL;
			const Kernel::Graph::NodeVector & nodes = graph_->nodes( );
			Kernel::Graph::NodeVector::const_iterator i = nodes.begin( );
			Kernel::Graph::NodeVector::const_iterator theEnd = nodes.end( );
			for( ; i != theEnd; ++i ){
				edges = (*i)->prepend_u_multiloops( edges, selfRefTyped );
				edges = (*i)->prepend_u_multiedgesLow( edges, selfRefTyped );
			}
			return Kernel::VariableHandle( new Kernel::Variable( edges ) );
		}
	if( strcmp( fieldID, "d_edges" ) == 0 )
		{
			RefCountPtr< const Lang::Graph > selfRefTyped = Helpers::down_cast_internal< const Lang::Graph >( selfRef );
			ListRef edges = Lang::THE_CONS_NULL;
			edges = prepend_edges( graph_->dloops( ), true, edges, selfRefTyped );
			edges = prepend_edges( graph_->dedges( ), true, edges, selfRefTyped );
			return Kernel::VariableHandle( new Kernel::Variable( edges ) );
		}
	if( strcmp( fieldID, "d_multiedges" ) == 0 )
		{
			RefCountPtr< const Lang::Graph > selfRefTyped = Helpers::down_cast_internal< const Lang::Graph >( selfRef );
			ListRef edges = Lang::THE_CONS_NULL;
			const Kernel::Graph::NodeVector & nodes = graph_->nodes( );
			Kernel::Graph::NodeVector::const_iterator i = nodes.begin( );
			Kernel::Graph::NodeVector::const_iterator theEnd = nodes.end( );
			for( ; i != theEnd; ++i ){
				edges = (*i)->prepend_d_multiloops( edges, selfRefTyped );
				edges = (*i)->prepend_d_multiedgesIn( edges, selfRefTyped );
			}
			return Kernel::VariableHandle( new Kernel::Variable( edges ) );
		}
	if( strcmp( fieldID, "edges" ) == 0 )
		{
			RefCountPtr< const Lang::Graph > selfRefTyped = Helpers::down_cast_internal< const Lang::Graph >( selfRef );
			ListRef edges = Lang::THE_CONS_NULL;
			edges = prepend_edges( graph_->dloops( ), true, edges, selfRefTyped );
			edges = prepend_edges( graph_->uloops( ), false, edges, selfRefTyped );
			edges = prepend_edges( graph_->dedges( ), true, edges, selfRefTyped );
			edges = prepend_edges( graph_->uedges( ), false, edges, selfRefTyped );
			return Kernel::VariableHandle( new Kernel::Variable( edges ) );
		}
	if( strcmp( fieldID, "multiedges" ) == 0 )
		{
			RefCountPtr< const Lang::Graph > selfRefTyped = Helpers::down_cast_internal< const Lang::Graph >( selfRef );
			ListRef edges = Lang::THE_CONS_NULL;
			const Kernel::Graph::NodeVector & nodes = graph_->nodes( );
			Kernel::Graph::NodeVector::const_iterator i = nodes.begin( );
			Kernel::Graph::NodeVector::const_iterator theEnd = nodes.end( );
			for( ; i != theEnd; ++i ){
				edges = (*i)->prepend_u_multiloops( edges, selfRefTyped );
				edges = (*i)->prepend_d_multiloops( edges, selfRefTyped );
				edges = (*i)->prepend_u_multiedgesLow( edges, selfRefTyped );
				edges = (*i)->prepend_d_multiedgesIn( edges, selfRefTyped );
			}
			return Kernel::VariableHandle( new Kernel::Variable( edges ) );
		}

	return TypeID->getMethod( selfRef, fieldID ); /* This will throw if there is no such method. */
}

Kernel::State *
Lang::Graph::newState( ) const
{
	return graph_->newState( nodeValues_, edgeValues_ );
}

Kernel::State *
Kernel::Graph::newState( const RefCountPtr< const ValueVector > & nodeValues, const RefCountPtr< const ValueVector > & edgeValues ) const
{
	RefCountPtr< std::list< Kernel::NodeData > > nodeDataMem;
	RefCountPtr< std::list< Kernel::EdgeData > > edgeDataMem;

	std::list< Kernel::NodeData > * nodeDataPtr = nodeDataMem.getPtr( );
	std::list< Kernel::EdgeData > * edgeDataPtr = edgeDataMem.getPtr( );

	if( nodeValues == NullPtr< const ValueVector >( ) ){
		if( nodePartitions_ == NullPtr< ValueVector >( ) ){
			for( NodeVector::const_iterator n = nodes_.begin( ); n != nodes_.end( ); ++n ) {
				nodeDataPtr->push_back( Kernel::NodeData( (*n)->key( ), Lang::THE_VOID, Lang::THE_VOID ) );
			}
		}else{
			for( NodeVector::const_iterator n = nodes_.begin( ); n != nodes_.end( ); ++n ) {
				nodeDataPtr->push_back( Kernel::NodeData( (*n)->key( ), Lang::THE_VOID, (*nodePartitions_)[ (*n)->index( ) ] ) );
			}
		}
	}else{
		if( nodePartitions_ == NullPtr< ValueVector >( ) ){
			for( NodeVector::const_iterator n = nodes_.begin( ); n != nodes_.end( ); ++n ) {
				nodeDataPtr->push_back( Kernel::NodeData( (*n)->key( ), (*nodeValues)[ (*n)->index( ) ], Lang::THE_VOID ) );
			}
		}else{
			for( NodeVector::const_iterator n = nodes_.begin( ); n != nodes_.end( ); ++n ) {
				nodeDataPtr->push_back( Kernel::NodeData( (*n)->key( ), (*nodeValues)[ (*n)->index( ) ], (*nodePartitions_)[ (*n)->index( ) ] ) );
			}
		}
	}

	if( edgeValues == NullPtr< const ValueVector >( ) ){
		if( edgeLabels_ == NullPtr< ValueVector >( ) ){
			for( EdgeVector::const_iterator e = uedges_.begin( ); e != uedges_.end( ); ++e ) {
				edgeDataPtr->push_back( Kernel::EdgeData( false, (*e)->source( )->key( ), (*e)->target( )->key( ), Lang::THE_VOID, Lang::THE_VOID ) );
			}
			for( EdgeVector::const_iterator e = dedges_.begin( ); e != dedges_.end( ); ++e ) {
				edgeDataPtr->push_back( Kernel::EdgeData( true, (*e)->source( )->key( ), (*e)->target( )->key( ), Lang::THE_VOID, Lang::THE_VOID ) );
			}
			for( EdgeVector::const_iterator e = uloops_.begin( ); e != uloops_.end( ); ++e ) {
				edgeDataPtr->push_back( Kernel::EdgeData( false, (*e)->source( )->key( ), (*e)->target( )->key( ), Lang::THE_VOID, Lang::THE_VOID ) );
			}
			for( EdgeVector::const_iterator e = dloops_.begin( ); e != dloops_.end( ); ++e ) {
				edgeDataPtr->push_back( Kernel::EdgeData( true, (*e)->source( )->key( ), (*e)->target( )->key( ), Lang::THE_VOID, Lang::THE_VOID ) );
			}
		}else{
			for( EdgeVector::const_iterator e = uedges_.begin( ); e != uedges_.end( ); ++e ) {
				edgeDataPtr->push_back( Kernel::EdgeData( false, (*e)->source( )->key( ), (*e)->target( )->key( ), Lang::THE_VOID, (*edgeLabels_)[ (*e)->index( ) ] ) );
			}
			for( EdgeVector::const_iterator e = dedges_.begin( ); e != dedges_.end( ); ++e ) {
				edgeDataPtr->push_back( Kernel::EdgeData( true, (*e)->source( )->key( ), (*e)->target( )->key( ), Lang::THE_VOID, (*edgeLabels_)[ (*e)->index( ) ] ) );
			}
			for( EdgeVector::const_iterator e = uloops_.begin( ); e != uloops_.end( ); ++e ) {
				edgeDataPtr->push_back( Kernel::EdgeData( false, (*e)->source( )->key( ), (*e)->target( )->key( ), Lang::THE_VOID, (*edgeLabels_)[ (*e)->index( ) ] ) );
			}
			for( EdgeVector::const_iterator e = dloops_.begin( ); e != dloops_.end( ); ++e ) {
				edgeDataPtr->push_back( Kernel::EdgeData( true, (*e)->source( )->key( ), (*e)->target( )->key( ), Lang::THE_VOID, (*edgeLabels_)[ (*e)->index( ) ] ) );
			}
		}
	}else{
		if( edgeLabels_ == NullPtr< ValueVector >( ) ){
			for( EdgeVector::const_iterator e = uedges_.begin( ); e != uedges_.end( ); ++e ) {
				edgeDataPtr->push_back( Kernel::EdgeData( false, (*e)->source( )->key( ), (*e)->target( )->key( ), (*edgeValues)[ (*e)->index( ) ], Lang::THE_VOID ) );
			}
			for( EdgeVector::const_iterator e = dedges_.begin( ); e != dedges_.end( ); ++e ) {
				edgeDataPtr->push_back( Kernel::EdgeData( true, (*e)->source( )->key( ), (*e)->target( )->key( ), (*edgeValues)[ (*e)->index( ) ], Lang::THE_VOID ) );
			}
			for( EdgeVector::const_iterator e = uloops_.begin( ); e != uloops_.end( ); ++e ) {
				edgeDataPtr->push_back( Kernel::EdgeData( false, (*e)->source( )->key( ), (*e)->target( )->key( ), (*edgeValues)[ (*e)->index( ) ], Lang::THE_VOID ) );
			}
			for( EdgeVector::const_iterator e = dloops_.begin( ); e != dloops_.end( ); ++e ) {
				edgeDataPtr->push_back( Kernel::EdgeData( true, (*e)->source( )->key( ), (*e)->target( )->key( ), (*edgeValues)[ (*e)->index( ) ], Lang::THE_VOID ) );
			}
		}else{
			for( EdgeVector::const_iterator e = uedges_.begin( ); e != uedges_.end( ); ++e ) {
				edgeDataPtr->push_back( Kernel::EdgeData( false, (*e)->source( )->key( ), (*e)->target( )->key( ), (*edgeValues)[ (*e)->index( ) ], (*edgeLabels_)[ (*e)->index( ) ] ) );
			}
			for( EdgeVector::const_iterator e = dedges_.begin( ); e != dedges_.end( ); ++e ) {
				edgeDataPtr->push_back( Kernel::EdgeData( true, (*e)->source( )->key( ), (*e)->target( )->key( ), (*edgeValues)[ (*e)->index( ) ], (*edgeLabels_)[ (*e)->index( ) ] ) );
			}
			for( EdgeVector::const_iterator e = uloops_.begin( ); e != uloops_.end( ); ++e ) {
				edgeDataPtr->push_back( Kernel::EdgeData( false, (*e)->source( )->key( ), (*e)->target( )->key( ), (*edgeValues)[ (*e)->index( ) ], (*edgeLabels_)[ (*e)->index( ) ] ) );
			}
			for( EdgeVector::const_iterator e = dloops_.begin( ); e != dloops_.end( ); ++e ) {
				edgeDataPtr->push_back( Kernel::EdgeData( true, (*e)->source( )->key( ), (*e)->target( )->key( ), (*edgeValues)[ (*e)->index( ) ], (*edgeLabels_)[ (*e)->index( ) ] ) );
			}
		}
	}

	return new Kernel::WarmGraph( domain_, partitionKeys_, nodeDataMem, edgeDataMem );
}

void
Lang::Graph::gcMark( Kernel::GCMarkedSet & marked )
{
	if( nodeValues_ != NullPtr< const ValueVector >( ) )
		{
			for( ValueVector::const_iterator n = nodeValues_->begin( ); n != nodeValues_->end( ); ++n ) {
				const_cast< Lang::Value * >( n->getPtr( ) )->gcMark( marked );
			}
		}
	if( edgeValues_ != NullPtr< const ValueVector >( ) )
		{
			for( ValueVector::const_iterator e = nodeValues_->begin( ); e != nodeValues_->end( ); ++e ) {
				const_cast< Lang::Value * >( e->getPtr( ) )->gcMark( marked );
			}
		}
}

Kernel::VariableHandle
Lang::Graph::nodeValue( const Kernel::Node * node ) const
{
	if( nodeValues_ == NullPtr< const ValueVector >( ) )
		{
			return Kernel::THE_VOID_VARIABLE;
		}

	return Kernel::VariableHandle( new Kernel::Variable( (*nodeValues_)[ node->index( ) ] ) );
}

Kernel::VariableHandle
Lang::Graph::edgeValue( const Kernel::Edge * edge ) const
{
	if( edgeValues_ == NullPtr< const ValueVector >( ) )
		{
			return Kernel::THE_VOID_VARIABLE;
		}

	return Kernel::VariableHandle( new Kernel::Variable( (*edgeValues_)[ edge->index( ) ] ) );
}

Kernel::VariableHandle
Lang::Graph::nodePartition( const Kernel::Node * node ) const
{
	return graph_->nodePartition( node );
}

Kernel::VariableHandle
Kernel::Graph::nodePartition( const Kernel::Node * node ) const
{
	if( nodePartitions_ == NullPtr< ValueVector >( ) )
		{
			return Kernel::THE_VOID_VARIABLE;
		}

	return Kernel::VariableHandle( new Kernel::Variable( (*nodePartitions_)[ node->index( ) ] ) );
}


Kernel::VariableHandle
Lang::Graph::edgeLabel( const Kernel::Edge * edge ) const
{
	return graph_->edgeLabel( edge );
}

Kernel::VariableHandle
Kernel::Graph::edgeLabel( const Kernel::Edge * edge ) const
{
	if( edgeLabels_ == NullPtr< ValueVector >( ) )
		{
			return Kernel::THE_VOID_VARIABLE;
		}

	return Kernel::VariableHandle( new Kernel::Variable( (*edgeLabels_)[ edge->index( ) ] ) );
}


const Kernel::Graph::NodeVector *
Kernel::Graph::getPartition( const Kernel::ValueRef & key ) const
{
	if( ! partitioned( ) ){
		throw Exceptions::InternalError( "Kernel::Graph::getPartition invoked on non-partitioned graph." );
	}

	if( const Lang::Symbol * ptr = dynamic_cast< const Lang::Symbol * >( key.getPtr( ) ) ){
		Lang::Symbol::KeyType key = ptr->getKey( );
		typedef typeof symbolKeyPartitionMap_ MapType;
		MapType::const_iterator i = symbolKeyPartitionMap_.find( key );
		if( i == symbolKeyPartitionMap_.end( ) ){
			throw Exceptions::InvalidGraphKey( Exceptions::InvalidGraphKey::PARTITION );
		}
		return i->second;
	}

	if( const Lang::Integer * ptr = dynamic_cast< const Lang::Integer * >( key.getPtr( ) ) ){
		Lang::Integer::ValueType key = ptr->val_;
		typedef typeof integerKeyPartitionMap_ MapType;
		MapType::const_iterator i = integerKeyPartitionMap_.find( key );
		if( i == integerKeyPartitionMap_.end( ) ){
			throw Exceptions::InvalidGraphKey( Exceptions::InvalidGraphKey::PARTITION );
		}
		return i->second;
	}

	throw Exceptions::InvalidGraphKeyType( );
}

RefCountPtr< const Lang::SingleList >
Lang::Graph::getPartition( const Kernel::ValueRef & key, const RefCountPtr< const Lang::Graph > & selfRef ) const
{
	const Kernel::Graph::NodeVector * partition = graph_->getPartition( key );
	Kernel::Node::ListRef nodes = Lang::THE_CONS_NULL;
	nodes = prepend_nodes( *partition, nodes, selfRef );
	return nodes;
}

size_t
Lang::Graph::getPartitionSize( const Kernel::ValueRef & key ) const
{
	const Kernel::Graph::NodeVector * partition = graph_->getPartition( key );
	return partition->size( );
}




void
Kernel::Graph::show( std::ostream & os ) const
{
	os << "<graph {" ;

	if( ! domain_.directed( ) && ! domain_.undirected( ) )
		os << "empty" ;
	else if( ! domain_.directed( ) )
		os << "undirected" ;
	else if( ! domain_.undirected( ) )
		os << "directed" ;
	else
		os << "mixed" ;

	if( domain_.loops( ) )
		os << ", loops" ;

	if( domain_.parallel( ) )
		os << ", parallel" ;

	os << "}, " ;

	os << nodeCount( ) << " nodes and " << edgeCount( ) << " edges" ;

	os << ">" ;
}

size_t
Kernel::Graph::edgeCount( ) const
{
	return uedges_.size( ) + dedges_.size( ) + uloops_.size( ) + dloops_.size( );
}

bool
Kernel::Graph::isKey( const RefCountPtr< const Lang::Value > & key ) const
{
	try
		{
			typedef const Lang::Symbol ArgType;
			RefCountPtr< ArgType > symKey = Helpers::try_cast_CoreArgument< ArgType >( key );
			return symbolKeyMap_.find( symKey->getKey( ) ) != symbolKeyMap_.end( );
		}
	catch( const NonLocalExit::NotThisType & ball )
		{
			/* Never mind, try next type. */
		}

	try
		{
			typedef const Lang::Integer ArgType;
			RefCountPtr< ArgType > intKey = Helpers::try_cast_CoreArgument< ArgType >( key );
			return integerKeyMap_.find( intKey->val_ ) != integerKeyMap_.end( );
		}
	catch( const NonLocalExit::NotThisType & ball )
		{
			/* Never mind, try next type. */
		}

	throw Exceptions::InvalidGraphKeyType( );
}

RefCountPtr< const Lang::Node >
Kernel::Graph::indexNode( const RefCountPtr< const Lang::Graph > & selfRef, size_t index ) const
{
	return RefCountPtr< const Lang::Node >( new Lang::Node( selfRef, nodes_[ index ] ) );
}

RefCountPtr< const Lang::Edge >
Kernel::Graph::indexEdge( const RefCountPtr< const Lang::Graph > & selfRef, size_t index ) const
{
	size_t iMax = uedges_.size( );
	if( index < iMax )
		return RefCountPtr< const Lang::Edge >( new Lang::Edge( selfRef, uedges_[ index ], false ) );
	index -= iMax;

	iMax = dedges_.size( );
	if( index < iMax )
		return RefCountPtr< const Lang::Edge >( new Lang::Edge( selfRef, dedges_[ index ], true ) );
	index -= iMax;

	iMax = uloops_.size( );
	if( index < iMax )
		return RefCountPtr< const Lang::Edge >( new Lang::Edge( selfRef, uloops_[ index ], false ) );
	index -= iMax;

	iMax = dloops_.size( );
	if( index < iMax )
		return RefCountPtr< const Lang::Edge >( new Lang::Edge( selfRef, dloops_[ index ], true ) );

	throw Exceptions::InternalError( "Lang::Graph::indexEdge: index is out of range." );
}

RefCountPtr< const Lang::Node >
Kernel::Graph::findNode( const RefCountPtr< const Lang::Graph > & selfRef, const RefCountPtr< const Lang::Value > & key ) const
{
	if( ! keyIsRange_ ){
		try
			{
				typedef const Lang::Symbol ArgType;
				RefCountPtr< ArgType > symKey = Helpers::try_cast_CoreArgument< ArgType >( key );
				SymbolKeyMap::const_iterator i = symbolKeyMap_.find( symKey->getKey( ) );
				if( i == symbolKeyMap_.end( ) )
					throw Exceptions::InvalidGraphKey( Exceptions::InvalidGraphKey::NODE );
				return RefCountPtr< const Lang::Node >( new Lang::Node( selfRef, i->second ) );
			}
		catch( const NonLocalExit::NotThisType & ball )
			{
				/* Never mind, try next type. */
			}
	}

	try
		{
			typedef const Lang::Integer ArgType;
			ArgType::ValueType intKey = Helpers::try_cast_CoreArgument< ArgType >( key )->val_;
			if( keyIsRange_ ){
				if( intKey < keyOffset_ )
					throw Exceptions::InvalidGraphKey( Exceptions::InvalidGraphKey::NODE );
				size_t i = intKey - keyOffset_;
				if( i >= nodes_.size( ) )
					throw Exceptions::InvalidGraphKey( Exceptions::InvalidGraphKey::NODE );
				return RefCountPtr< const Lang::Node >( new Lang::Node( selfRef, nodes_[ i ] ) );
			}else{
				SymbolKeyMap::const_iterator i = integerKeyMap_.find( intKey );
				if( i == integerKeyMap_.end( ) )
					throw Exceptions::InvalidGraphKey( Exceptions::InvalidGraphKey::NODE );
				return RefCountPtr< const Lang::Node >( new Lang::Node( selfRef, i->second ) );
			}
		}
	catch( const NonLocalExit::NotThisType & ball )
		{
			/* Never mind, try next type. */
		}

	throw Exceptions::InvalidGraphKeyType( );
}

size_t
Kernel::Graph::findIndex( const RefCountPtr< const Lang::Value > & key ) const
{
	try
		{
			typedef const Lang::Symbol ArgType;
			RefCountPtr< ArgType > symKey = Helpers::try_cast_CoreArgument< ArgType >( key );
			SymbolKeyMap::const_iterator i = symbolKeyMap_.find( symKey->getKey( ) );
			if( i == symbolKeyMap_.end( ) )
				throw Exceptions::InvalidGraphKey( Exceptions::InvalidGraphKey::NODE );
			return i->second->index( );
		}
	catch( const NonLocalExit::NotThisType & ball )
		{
			/* Never mind, try next type. */
		}

	try
		{
			typedef const Lang::Integer ArgType;
			RefCountPtr< ArgType > intKey = Helpers::try_cast_CoreArgument< ArgType >( key );
			SymbolKeyMap::const_iterator i = integerKeyMap_.find( intKey->val_ );
			if( i == integerKeyMap_.end( ) )
				throw Exceptions::InvalidGraphKey( Exceptions::InvalidGraphKey::NODE );
			return i->second->index( );
		}
	catch( const NonLocalExit::NotThisType & ball )
		{
			/* Never mind, try next type. */
		}

	throw Exceptions::InvalidGraphKeyType( );
}

Kernel::Graph::ListRef
Kernel::Graph::findEdges( const RefCountPtr< const Lang::Graph > & selfRef, Kernel::Node * source, Kernel::Node * target, bool directed, bool undirected, bool loops, const Kernel::ValueRef & label ) const
{
	Kernel::Node::ListRef edges = Lang::THE_CONS_NULL;

	RefCountPtr< const Kernel::EdgePredicate > filter = RefCountPtr< const Kernel::EdgePredicate >( NullPtr< const Kernel::EdgePredicate >( ) );
	if( const Lang::Symbol * ptr = dynamic_cast< const Lang::Symbol * >( label.getPtr( ) ) ){
		filter = RefCountPtr< const Kernel::EdgePredicate >( new Kernel::EdgeLabelPredicate< Lang::Symbol >( edgeLabels_, *ptr ) );
	}else if( const Lang::Integer * ptr = dynamic_cast< const Lang::Integer * >( label.getPtr( ) ) ){
		filter = RefCountPtr< const Kernel::EdgePredicate >( new Kernel::EdgeLabelPredicate< Lang::Integer >( edgeLabels_, *ptr ) );
	}

	if( filter != NullPtr< const Kernel::EdgePredicate >( ) && filter->always_false( ) ){
		return edges;
	}

	if( source != 0 && target != 0 ){

		if( source == target ){
			if( loops ){
				if( undirected )
					edges = source->prepend_uloops( edges, selfRef, filter.getPtr( ) );
				if( directed )
					edges = source->prepend_dloops( edges, selfRef, filter.getPtr( ) );
			}
		}else{
			if( undirected )
				edges = source->prepend_uedges_to( target, edges, selfRef, filter.getPtr( ) );
			if( directed )
				edges = source->prepend_dedges_to( target, edges, selfRef, filter.getPtr( ) );
		}

	}else if( source != 0 ){

		if( loops ){
			if( undirected )
				edges = source->prepend_uloops( edges, selfRef, filter.getPtr( ) );
			if( directed )
				edges = source->prepend_dloops( edges, selfRef, filter.getPtr( ) );
		}
		if( undirected )
			edges = source->prepend_uedges( edges, selfRef, filter.getPtr( ) );
		if( directed )
			edges = source->prepend_dedgesOut( edges, selfRef, filter.getPtr( ) );

	}else if( target != 0 ){

		if( loops ){
			if( undirected )
				edges = target->prepend_uloops( edges, selfRef, filter.getPtr( ) );
			if( directed )
				edges = target->prepend_dloops( edges, selfRef, filter.getPtr( ) );
		}
		if( undirected )
			edges = target->prepend_uedges( edges, selfRef, filter.getPtr( ) );
		if( directed )
			edges = target->prepend_dedgesIn( edges, selfRef, filter.getPtr( ) );

	}else{

		if( loops ){
			if( undirected )
				edges = Lang::Graph::prepend_edges( uloops_, false, edges, selfRef, filter.getPtr( ) );
			if( directed )
				edges = Lang::Graph::prepend_edges( dloops_, true, edges, selfRef, filter.getPtr( ) );
		}
		if( undirected )
			edges = Lang::Graph::prepend_edges( uedges_, false, edges, selfRef, filter.getPtr( ) );
		if( directed )
			edges = Lang::Graph::prepend_edges( dedges_, true, edges, selfRef, filter.getPtr( ) );

	}

	return edges;
}

Kernel::Graph::ListRef
Kernel::Graph::findMultiEdges( const RefCountPtr< const Lang::Graph > & selfRef, const Kernel::Node * source, const Kernel::Node * target, bool directed, bool undirected, bool loops ) const
{
	Kernel::Node::ListRef edges = Lang::THE_CONS_NULL;

	if( ! directed && source->index( ) > target->index( ) ){
		const Kernel::Node * tmp = target;
		target = source;
		source = tmp;
	}

	if( source != 0 && target != 0 ){

		if( source == target ){
			if( loops ){
				if( undirected )
					edges = source->prepend_u_multiloops( edges, selfRef );
				if( directed )
					edges = source->prepend_d_multiloops( edges, selfRef );
			}
		}else{
			if( undirected )
				edges = target->prepend_u_multiedgeLowFrom( source, edges, selfRef );
			if( directed )
				edges = target->prepend_d_multiedgeInFrom( source, edges, selfRef );
		}

	}else if( source != 0 ){

		if( loops ){
			if( undirected )
				edges = source->prepend_u_multiloops( edges, selfRef );
			if( directed )
				edges = source->prepend_d_multiloops( edges, selfRef );
		}
		if( undirected )
			edges = source->prepend_u_multiedges( edges, selfRef );
		if( directed )
			edges = source->prepend_d_multiedgesOut( edges, selfRef );

	}else if( target != 0 ){

		if( loops ){
			if( undirected )
				edges = source->prepend_u_multiloops( edges, selfRef );
			if( directed )
				edges = source->prepend_d_multiloops( edges, selfRef );
		}
		if( undirected )
			edges = source->prepend_u_multiedges( edges, selfRef );
		if( directed )
			edges = source->prepend_d_multiedgesIn( edges, selfRef );

	}else{

		Kernel::Graph::NodeVector::const_iterator i = nodes_.begin( );
		Kernel::Graph::NodeVector::const_iterator theEnd = nodes_.end( );
		for( ; i != theEnd; ++i ){
			if( loops ){
				if( undirected )
					edges = (*i)->prepend_u_multiloops( edges, selfRef );
				if( directed )
					edges = (*i)->prepend_d_multiloops( edges, selfRef );
			}
			if( undirected )
				edges = (*i)->prepend_u_multiedgesLow( edges, selfRef );
			if( directed )
				edges = (*i)->prepend_d_multiedgesIn( edges, selfRef );
		}

	}

	return edges;
}

Kernel::Node *
Lang::Graph::kernelNodeByValue( Kernel::Arguments & args, size_t argsi, bool voidIsNull, const RefCountPtr< const Interaction::CoreLocation > & coreLoc, const Ast::SourceLocation & callLoc ) const
{
	RefCountPtr< const Lang::Value > arg = args.getValue( argsi );

	if( arg == Lang::THE_VOID ){
		if( voidIsNull )
			return 0;
		throw Exceptions::CoreTypeMismatch( callLoc, coreLoc, args, argsi, Exceptions::GraphKeyTypeMismatch::expectedTypeOrNode );
	}

	RefCountPtr< const Lang::Node > node = arg.down_cast< const Lang::Node >( );
	if( node != NullPtr< const Lang::Node >( ) ){
		if( node->graph( ).getPtr( ) != this ){
			throw Exceptions::InvalidGraphElement( Exceptions::InvalidGraphElement::NODE, args.getLoc( argsi ) );
		}
		/* The const_cast below is really just a shortcut for:
		 *   nodes_[ node->index( ) ]
		 */
		return const_cast< Kernel::Node * >( node->node( ) );
	}

	try
		{
			return graph_->nodes( )[ graph_->findIndex( arg ) ];
		}
	catch( const Exceptions::InvalidGraphKeyType & ball )
		{
			throw Exceptions::CoreTypeMismatch( callLoc, coreLoc, args, argsi, Exceptions::GraphKeyTypeMismatch::expectedTypeOrNode );
		}
	catch( const Exceptions::InvalidGraphKey & ball )
		{
			throw Exceptions::GraphKeyOutOfRange( Exceptions::GraphKeyOutOfRange::NODE, callLoc, coreLoc, args, argsi );
		}
}


void
Kernel::Graph::initializeKeyRangeOffset( )
{
	keyIsRange_ = true;
	keyOffset_ = 0;

	Lang::Integer::ValueType ni = 0;
	for( NodeVector::const_iterator n = nodes_.begin( ); n != nodes_.end( ); ++n, ++ni ) {
		try
			{
				Lang::Integer::ValueType i = Helpers::try_cast_CoreArgument< const Lang::Integer >( (*n)->key( ) )->val_;
				if( i != ni + keyOffset_ ) {
					if( ni == 0 ){
						keyOffset_ = i;
					}else{
						keyIsRange_ = false;
						break;
					}
				}
			}
		catch( const NonLocalExit::NotThisType & ball )
			{
				keyIsRange_ = false;
				break;
			}
	}
}

void
Kernel::Graph::initializeKeyMaps( )
{
	if( keyIsRange_ )
		return;
	for( NodeVector::const_iterator n = nodes_.begin( ); n != nodes_.end( ); ++n ){
		const Lang::Value * key = (*n)->key( ).getPtr( );
		{
			const Lang::Symbol * symKey = dynamic_cast< const Lang::Symbol * >( key );
			if( symKey != NullPtr< const Lang::Symbol >( ) ) {
				symbolKeyMap_[ symKey->getKey( ) ] = *n;
				continue;
			}
		}
		{
			const Lang::Integer * intKey = dynamic_cast< const Lang::Integer * >( key );
			if( intKey != NullPtr< const Lang::Integer >( ) ) {
				integerKeyMap_[ intKey->val_ ] = *n;
				continue;
			}
		}
		throw Exceptions::InternalError( "Lang::Graph initialized with key of bad type." );
	}
}

void
Kernel::Graph::initializeAndCheckPartitions( const Ast::SourceLocation & callLoc )
{
	if( partitionKeys_ == NullPtr< const Lang::SingleList >( ) )
		return;

	{
		RefCountPtr< const Lang::SingleListPair > p = partitionKeys_.down_cast< const Lang::SingleListPair >( );
		while( p != NullPtr< const Lang::SingleListPair >( ) ){
			RefCountPtr< const Lang::Value > val = p->car_->getUntyped( );

			do{

				if( const Lang::Symbol * ptr = dynamic_cast< const Lang::Symbol * >( val.getPtr( ) ) ){
					Lang::Symbol::KeyType key = ptr->getKey( );
					NodeVector * newPartition = new NodeVector( );
					partitions_.push_back( newPartition );
					symbolKeyPartitionMap_[ key ] = newPartition;
					break;
				}

				if( const Lang::Integer * ptr = dynamic_cast< const Lang::Integer * >( val.getPtr( ) ) ){
					Lang::Integer::ValueType key = ptr->val_;
					NodeVector * newPartition = new NodeVector( );
					partitions_.push_back( newPartition );
					integerKeyPartitionMap_[ key ] = newPartition;
					break;
				}

				throw Exceptions::InternalError( "Kernel::Graph::initializeAndCheckPartitions: Unexcpected partition key type." );

			}while( false );

			p = p->cdr_.down_cast< const Lang::SingleListPair >( );
		}
	}

	ValueVector::const_iterator p = nodePartitions_->begin( );
	for( NodeVector::const_iterator n = nodes_.begin( ); n != nodes_.end( ); ++n, ++p ){

		do{

			if( const Lang::Symbol * ptr = dynamic_cast< const Lang::Symbol * >( p->getPtr( ) ) ){
				Lang::Symbol::KeyType key = ptr->getKey( );
				symbolKeyPartitionMap_[ key ]->push_back( *n );
				break;
			}

			if( const Lang::Integer * ptr = dynamic_cast< const Lang::Integer * >( p->getPtr( ) ) ){
				Lang::Integer::ValueType key = ptr->val_;
				integerKeyPartitionMap_[ key ]->push_back( *n );
				break;
			}

			throw Exceptions::InternalError( "Kernel::Graph::initializeAndCheckPartitions: Unexcpected node partition type." );

		}while( false );

	}

	for( EdgeVector::iterator e = uedges_.begin( ); e != uedges_.end( ); ++e ){
		/* By using getPartition to map a partition key to a pointer to the vector of nodes in that partiion
		 * we obtain a canonicalization.  (Note that a pointer comparison of the partiion keys themselves does
		 * not work since there may be more than one Lang::Value with the same content.)
		 */
		const NodeVector * sourcePartition = getPartition( (*nodePartitions_)[ (*e)->source( )->index( ) ] );
		const NodeVector * targetPartition = getPartition( (*nodePartitions_)[ (*e)->target( )->index( ) ] );
		if( sourcePartition == targetPartition ){
			throw Exceptions::GraphPartitionViolation( (*e)->source( )->key( ), (*e)->target( )->key( ), (*nodePartitions_)[ (*e)->source( )->index( ) ], callLoc );
		}
	}
	for( EdgeVector::iterator e = dedges_.begin( ); e != dedges_.end( ); ++e ){
		const NodeVector * sourcePartition = getPartition( (*nodePartitions_)[ (*e)->source( )->index( ) ] );
		const NodeVector * targetPartition = getPartition( (*nodePartitions_)[ (*e)->target( )->index( ) ] );
		if( sourcePartition == targetPartition ){
			throw Exceptions::GraphPartitionViolation( (*e)->source( )->key( ), (*e)->target( )->key( ), (*nodePartitions_)[ (*e)->source( )->index( ) ], callLoc );
		}
	}

}

void
Kernel::Graph::addEdgesToNodes( )
{
	for( EdgeVector::iterator e = uedges_.begin( ); e != uedges_.end( ); ++e ){
		(*e)->source( )->add_uedgeHigh( *e );
		(*e)->target( )->add_uedgeLow( *e );
	}
	for( EdgeVector::iterator e = uloops_.begin( ); e != uloops_.end( ); ++e ){
		(*e)->source( )->add_uloop( *e );
	}
	for( EdgeVector::iterator e = dedges_.begin( ); e != dedges_.end( ); ++e ){
		(*e)->source( )->add_dedgeOut( *e );
		(*e)->target( )->add_dedgeIn( *e );
	}
	for( EdgeVector::iterator e = dloops_.begin( ); e != dloops_.end( ); ++e ){
		(*e)->source( )->add_dloop( *e );
	}
}

Lang::Graph::ListRef
Lang::Graph::prepend_nodes( const Kernel::Graph::NodeVector & nodes, const ListRef & rest, const RefCountPtr< const Lang::Graph > & selfRef )
{
	Lang::Graph::ListRef result = rest;
	for( Kernel::Graph::NodeVector::const_reverse_iterator n = nodes.rbegin( ); n != nodes.rend( ); ++n ){
		result = Helpers::SingleList_cons( new Lang::Node( selfRef, *n ), result );
	}
	return result;
}

Lang::Graph::ListRef
Lang::Graph::prepend_edges( const Kernel::Graph::EdgeVector & edges, bool directed, const ListRef & rest, const RefCountPtr< const Lang::Graph > & selfRef, const Kernel::EdgePredicate * filter )
{
	Lang::Graph::ListRef result = rest;
	if( filter == 0 ){
		for( Kernel::Graph::EdgeVector::const_reverse_iterator e = edges.rbegin( ); e != edges.rend( ); ++e ){
			result = Helpers::SingleList_cons( new Lang::Edge( selfRef, *e, directed ), result );
		}
	}else{
		for( Kernel::Graph::EdgeVector::const_reverse_iterator e = edges.rbegin( ); e != edges.rend( ); ++e ){
			if( (*filter)( **e ) ){
				result = Helpers::SingleList_cons( new Lang::Edge( selfRef, *e, directed ), result );
			}
		}
	}
	return result;
}


template< bool directed, bool undirected, bool in, bool out, const char * fieldID >
Lang::NodeMethod_edges< directed, undirected, in, out, fieldID >::NodeMethod_edges( RefCountPtr< const Lang::Node > self, const Ast::FileID * fullMethodID )
	: Lang::MethodBase< Lang::Node >( self, fullMethodID, false, true )
{
	formals_->appendEvaluatedCoreFormal( "loops", Kernel::THE_TRUE_VARIABLE );
}

template< bool directed, bool undirected, bool in, bool out, const char * fieldID >
Lang::NodeMethod_edges< directed, undirected, in, out, fieldID >::~NodeMethod_edges( )
{ }

template< bool directed, bool undirected, bool in, bool out, const char * fieldID >
void
Lang::NodeMethod_edges< directed, undirected, in, out, fieldID >::call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
{
	args.applyDefaults( callLoc );

	typedef const Lang::Boolean ArgType;
	bool loops = Helpers::down_cast_CoreArgument< ArgType >( coreLoc_, args, 0, callLoc )->val_;

	Kernel::Node::ListRef edges = Lang::THE_CONS_NULL;
	if( loops ){
		if( undirected )
			edges = self_->node( )->prepend_uloops( edges, self_->graph( ) );
		if( directed )
			edges = self_->node( )->prepend_dloops( edges, self_->graph( ) );
	}
	if( undirected )
		edges = self_->node( )->prepend_uedges( edges, self_->graph( ) );
	if( directed ){
		if( out )
			edges = self_->node( )->prepend_dedgesOut( edges, self_->graph( ) );
		if( in )
			edges = self_->node( )->prepend_dedgesIn( edges, self_->graph( ) );
	}

	Kernel::ContRef cont = evalState->cont_;
	cont->takeValue( edges,
									 evalState );
}


template< bool directed, bool undirected, bool in, bool out, const char * fieldID >
Lang::NodeMethod_multiedges< directed, undirected, in, out, fieldID >::NodeMethod_multiedges( RefCountPtr< const Lang::Node > self, const Ast::FileID * fullMethodID )
	: Lang::MethodBase< Lang::Node >( self, fullMethodID, false, true )
{
	formals_->appendEvaluatedCoreFormal( "loops", Kernel::THE_TRUE_VARIABLE );
}

template< bool directed, bool undirected, bool in, bool out, const char * fieldID >
Lang::NodeMethod_multiedges< directed, undirected, in, out, fieldID >::~NodeMethod_multiedges( )
{ }

template< bool directed, bool undirected, bool in, bool out, const char * fieldID >
void
Lang::NodeMethod_multiedges< directed, undirected, in, out, fieldID >::call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
{
	args.applyDefaults( callLoc );

	typedef const Lang::Boolean ArgType;
	bool loops = Helpers::down_cast_CoreArgument< ArgType >( coreLoc_, args, 0, callLoc )->val_;

	Kernel::Node::ListRef edges = Lang::THE_CONS_NULL;
	if( loops ){
		if( undirected )
			edges = self_->node( )->prepend_u_multiloops( edges, self_->graph( ) );
		if( directed )
			edges = self_->node( )->prepend_d_multiloops( edges, self_->graph( ) );
	}
	if( undirected ){
		edges = self_->node( )->prepend_u_multiedges( edges, self_->graph( ) );
	}
	if( directed ){
		if( out )
			edges = self_->node( )->prepend_d_multiedgesOut( edges, self_->graph( ) );
		if( in )
			edges = self_->node( )->prepend_d_multiedgesIn( edges, self_->graph( ) );
	}

	Kernel::ContRef cont = evalState->cont_;
	cont->takeValue( edges,
									 evalState );
}


template< bool directed, bool undirected, const char * fieldID >
Lang::NodeMethod_loops< directed, undirected, fieldID >::NodeMethod_loops( RefCountPtr< const Lang::Node > self, const Ast::FileID * fullMethodID )
	: Lang::MethodBase< Lang::Node >( self, fullMethodID, false, true )
{
	formals_->appendEvaluatedCoreFormal( "loops", Kernel::THE_TRUE_VARIABLE );
}

template< bool directed, bool undirected, const char * fieldID >
Lang::NodeMethod_loops< directed, undirected, fieldID >::~NodeMethod_loops( )
{ }

template< bool directed, bool undirected, const char * fieldID >
void
Lang::NodeMethod_loops< directed, undirected, fieldID >::call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
{
	Kernel::Node::ListRef edges = Lang::THE_CONS_NULL;
	if( undirected )
		edges = self_->node( )->prepend_uloops( edges, self_->graph( ) );
	if( directed )
		edges = self_->node( )->prepend_dloops( edges, self_->graph( ) );

	Kernel::ContRef cont = evalState->cont_;
	cont->takeValue( edges,
									 evalState );
}


template< bool directed, bool undirected, const char * fieldID >
Lang::NodeMethod_multiloops< directed, undirected, fieldID >::NodeMethod_multiloops( RefCountPtr< const Lang::Node > self, const Ast::FileID * fullMethodID )
	: Lang::MethodBase< Lang::Node >( self, fullMethodID, false, true )
{
	formals_->appendEvaluatedCoreFormal( "loops", Kernel::THE_TRUE_VARIABLE );
}

template< bool directed, bool undirected, const char * fieldID >
Lang::NodeMethod_multiloops< directed, undirected, fieldID >::~NodeMethod_multiloops( )
{ }

template< bool directed, bool undirected, const char * fieldID >
void
Lang::NodeMethod_multiloops< directed, undirected, fieldID >::call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
{
	Kernel::Node::ListRef edges = Lang::THE_CONS_NULL;
	if( undirected )
		edges = self_->node( )->prepend_u_multiloops( edges, self_->graph( ) );
	if( directed )
		edges = self_->node( )->prepend_d_multiloops( edges, self_->graph( ) );

	Kernel::ContRef cont = evalState->cont_;
	cont->takeValue( edges,
									 evalState );
}


template< bool reverse, const char * fieldID >
Lang::NodeMethod_trace< reverse, fieldID >::NodeMethod_trace( RefCountPtr< const Lang::Node > self, const Ast::FileID * fullMethodID )
	: Lang::MethodBase< Lang::Node >( self, fullMethodID, false, true )
{
	formals_->appendEvaluatedCoreFormal( "edge", Kernel::THE_SLOT_VARIABLE );
}

template< bool reverse, const char * fieldID >
Lang::NodeMethod_trace< reverse, fieldID >::~NodeMethod_trace( )
{ }

template< bool reverse, const char * fieldID >
void
Lang::NodeMethod_trace< reverse, fieldID >::call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
{
	args.applyDefaults( callLoc );

	const Kernel::Node * other;

	size_t argsi = 0;
	do{
		try{
			typedef const Lang::Edge ArgType;
			RefCountPtr< ArgType > edge = Helpers::try_cast_CoreArgument< ArgType >( args.getValue( argsi ) );
			other = reverse ? edge->backtrace( self_->node( ), callLoc ) : edge->trace( self_->node( ), callLoc );
			break;
		}catch( const NonLocalExit::NotThisType & ball ){
			/* Never mind, try next type. */
		}
		try{
			typedef const Lang::MultiEdge ArgType;
			RefCountPtr< ArgType > edge = Helpers::try_cast_CoreArgument< ArgType >( args.getValue( argsi ) );
			other = reverse ? edge->backtrace( self_->node( ), callLoc ) : edge->trace( self_->node( ), callLoc );
			break;
		}catch( const NonLocalExit::NotThisType & ball ){
			/* Never mind, try next type. */
		}
		throw Exceptions::CoreTypeMismatch( callLoc, coreLoc_, args, argsi, Helpers::typeSetString( Lang::Edge::staticTypeName( ), Lang::MultiEdge::staticTypeName( ) ) );
	}while( false );

	Kernel::ContRef cont = evalState->cont_;
	cont->takeValue( Kernel::ValueRef( new Lang::Node( self_->graph( ), other ) ),
									 evalState );
}


template< bool directed, bool undirected, bool in, bool out, const char * fieldID >
Lang::NodeMethod_neighbors< directed, undirected, in, out, fieldID >::NodeMethod_neighbors( RefCountPtr< const Lang::Node > self, const Ast::FileID * fullMethodID )
	: Lang::MethodBase< Lang::Node >( self, fullMethodID, false, true )
{
	formals_->appendEvaluatedCoreFormal( "loops", Kernel::THE_TRUE_VARIABLE );
	formals_->appendEvaluatedCoreFormal( "parallel", Kernel::THE_TRUE_VARIABLE );
}

template< bool directed, bool undirected, bool in, bool out, const char * fieldID >
Lang::NodeMethod_neighbors< directed, undirected, in, out, fieldID >::~NodeMethod_neighbors( )
{ }

template< bool directed, bool undirected, bool in, bool out, const char * fieldID >
void
Lang::NodeMethod_neighbors< directed, undirected, in, out, fieldID >::call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
{
	args.applyDefaults( callLoc );

	typedef const Lang::Boolean ArgType;
	bool loops = Helpers::down_cast_CoreArgument< ArgType >( coreLoc_, args, 0, callLoc )->val_;
	bool parallel = Helpers::down_cast_CoreArgument< ArgType >( coreLoc_, args, 1, callLoc )->val_;

	Kernel::Node::ListRef neighbors = Lang::THE_CONS_NULL;
	if( parallel ){
		if( loops ){
			if( undirected )
				neighbors = self_->node( )->prepend_uloopNeighbors( neighbors, self_->graph( ) );
			if( directed )
				neighbors = self_->node( )->prepend_dloopNeighbors( neighbors, self_->graph( ) );
		}
		if( undirected )
			neighbors = self_->node( )->prepend_uedgeNeighbors( neighbors, self_->graph( ) );
		if( directed ){
			if( out )
				neighbors = self_->node( )->prepend_dedgeNeighborsOut( neighbors, self_->graph( ) );
			if( in )
			neighbors = self_->node( )->prepend_dedgeNeighborsIn( neighbors, self_->graph( ) );
		}
	}else{
		std::set< size_t > added;
		if( loops ) {
			if( undirected )
				neighbors = self_->node( )->prepend_unique_uloopNeighbors( neighbors, self_->graph( ), & added );
			if( directed )
				neighbors = self_->node( )->prepend_unique_dloopNeighbors( neighbors, self_->graph( ), & added );
		}
		if( undirected )
			neighbors = self_->node( )->prepend_unique_uedgeNeighbors( neighbors, self_->graph( ), & added );
		if( directed ){
			if( out )
				neighbors = self_->node( )->prepend_unique_dedgeNeighborsOut( neighbors, self_->graph( ), & added );
			if( in )
				neighbors = self_->node( )->prepend_unique_dedgeNeighborsIn( neighbors, self_->graph( ), & added );
		}
	}

	Kernel::ContRef cont = evalState->cont_;
	cont->takeValue( neighbors,
									 evalState );
}


template< class T >
Lang::Method_get_graph< T >::Method_get_graph( RefCountPtr< const T > self, const Ast::FileID * fullMethodID )
	: Lang::MethodBase< T >( self, fullMethodID, false, true )
{ }

template< class T >
Lang::Method_get_graph< T >::~Method_get_graph( )
{ }

template< class T >
void
Lang::Method_get_graph< T >::call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
{
	Kernel::ContRef cont = evalState->cont_;
	cont->takeValue( Lang::MethodBase< T >::self_->graph( ),
									 evalState );
}


template< bool onlyCount, const char * fieldID >
Lang::GraphMethod_partition< onlyCount, fieldID >::GraphMethod_partition( RefCountPtr< const Lang::Graph > self, const Ast::FileID * fullMethodID )
	: Lang::MethodBase< Lang::Graph >( self, fullMethodID, false, true )
{
	formals_->appendEvaluatedCoreFormal( "key", Kernel::THE_SLOT_VARIABLE );
}

template< bool onlyCount, const char * fieldID >
Lang::GraphMethod_partition< onlyCount, fieldID >::~GraphMethod_partition( )
{ }

template< bool onlyCount, const char * fieldID >
void
Lang::GraphMethod_partition< onlyCount, fieldID >::call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
{
	args.applyDefaults( callLoc );

	if( ! self_->graph( )->partitioned( ) ){
		throw Exceptions::CoreRequirement( "Requesting partiion in a graph which is not partitioned.", coreLoc_, callLoc );
	}

	try
		{
			Kernel::ContRef cont = evalState->cont_;
			if( onlyCount )
				cont->takeValue( Kernel::ValueRef( new Lang::Integer( static_cast< Lang::Integer::ValueType >( self_->getPartitionSize( args.getValue( 0 ) ) ) ) ),
												 evalState );
			else
				cont->takeValue( self_->getPartition( args.getValue( 0 ), self_ ),
												 evalState );
		}
	catch( const Exceptions::InvalidGraphKeyType & ball )
		{
			throw Exceptions::CoreTypeMismatch( callLoc, coreLoc_, args, 0, Exceptions::GraphKeyTypeMismatch::expectedType );
		}
	catch( const Exceptions::InvalidGraphKey & ball )
		{
			throw Exceptions::GraphKeyOutOfRange( Exceptions::GraphKeyOutOfRange::PARTITION, callLoc, coreLoc_, args, 0 );
		}
}


Lang::GraphMethod_isNode::GraphMethod_isNode( RefCountPtr< const Lang::Graph > self, const Ast::FileID * fullMethodID )
	: Lang::MethodBase< Lang::Graph >( self, fullMethodID, false, true )
{
	formals_->appendEvaluatedCoreFormal( "node", Kernel::THE_SLOT_VARIABLE );
}

Lang::GraphMethod_isNode::~GraphMethod_isNode( )
{ }

void
Lang::GraphMethod_isNode::call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
{
	args.applyDefaults( callLoc );

	typedef const Lang::Node ArgType;
	RefCountPtr< ArgType > node = Helpers::down_cast_CoreArgument< ArgType >( coreLoc_, args, 0, callLoc );

	Kernel::ContRef cont = evalState->cont_;
	cont->takeValue( Kernel::ValueRef( new Lang::Boolean( self_ == node->graph( ) ) ),
									 evalState );
}

Lang::GraphMethod_isEdge::GraphMethod_isEdge( RefCountPtr< const Lang::Graph > self, const Ast::FileID * fullMethodID )
	: Lang::MethodBase< Lang::Graph >( self, fullMethodID, false, true )
{
	formals_->appendEvaluatedCoreFormal( "edge", Kernel::THE_SLOT_VARIABLE );
}

Lang::GraphMethod_isEdge::~GraphMethod_isEdge( )
{ }

void
Lang::GraphMethod_isEdge::call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
{
	args.applyDefaults( callLoc );

	typedef const Lang::Edge ArgType;
	RefCountPtr< ArgType > edge = Helpers::down_cast_CoreArgument< ArgType >( coreLoc_, args, 0, callLoc );

	Kernel::ContRef cont = evalState->cont_;
	cont->takeValue( Kernel::ValueRef( new Lang::Boolean( self_ == edge->graph( ) ) ),
									 evalState );
}

Lang::GraphMethod_isKey::GraphMethod_isKey( RefCountPtr< const Lang::Graph > self, const Ast::FileID * fullMethodID )
	: Lang::MethodBase< Lang::Graph >( self, fullMethodID, false, true )
{
	formals_->appendEvaluatedCoreFormal( "key", Kernel::THE_SLOT_VARIABLE );
}

Lang::GraphMethod_isKey::~GraphMethod_isKey( )
{ }

void
Lang::GraphMethod_isKey::call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
{
	args.applyDefaults( callLoc );

	try
		{
			Kernel::ContRef cont = evalState->cont_;
			cont->takeValue( Kernel::ValueRef( new Lang::Boolean( self_->graph( )->isKey( args.getValue( 0 ) ) ) ),
											 evalState );

		}
	catch( const Exceptions::InvalidGraphKeyType & ball )
		{
			throw Exceptions::CoreTypeMismatch( callLoc, coreLoc_, args, 0, Exceptions::GraphKeyTypeMismatch::expectedType );
		}
}

Lang::GraphMethod_indexNode::GraphMethod_indexNode( RefCountPtr< const Lang::Graph > self, const Ast::FileID * fullMethodID )
	: Lang::MethodBase< Lang::Graph >( self, fullMethodID, false, true )
{
	formals_->appendEvaluatedCoreFormal( "index", Kernel::THE_SLOT_VARIABLE );
}

Lang::GraphMethod_indexNode::~GraphMethod_indexNode( )
{ }

void
Lang::GraphMethod_indexNode::call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
{
	args.applyDefaults( callLoc );

	size_t argsi = 0;
	typedef const Lang::Integer ArgType;
	ArgType::ValueType indexSigned = Helpers::down_cast_CoreArgument< ArgType >( coreLoc_, args, argsi, callLoc )->val_;

	if( indexSigned < 0 )
		throw Exceptions::CoreOutOfRange( callLoc, coreLoc_, args, argsi, "The node index cannot be negative." );
	size_t index = static_cast< size_t >( indexSigned );
	if( self_->graph( )->nodeCount( ) <= index )
		throw Exceptions::CoreOutOfRange( callLoc, coreLoc_, args, argsi, "The node index must be less than the number of nodes in the graph." );

	RefCountPtr< const Lang::Node > node = self_->graph( )->indexNode( self_, index );
	Kernel::ContRef cont = evalState->cont_;
	cont->takeValue( node, evalState );
}

Lang::GraphMethod_indexEdge::GraphMethod_indexEdge( RefCountPtr< const Lang::Graph > self, const Ast::FileID * fullMethodID )
	: Lang::MethodBase< Lang::Graph >( self, fullMethodID, false, true )
{
	formals_->appendEvaluatedCoreFormal( "index", Kernel::THE_SLOT_VARIABLE );
}

Lang::GraphMethod_indexEdge::~GraphMethod_indexEdge( )
{ }

void
Lang::GraphMethod_indexEdge::call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
{
	args.applyDefaults( callLoc );

	size_t argsi = 0;
	typedef const Lang::Integer ArgType;
	ArgType::ValueType indexSigned = Helpers::down_cast_CoreArgument< ArgType >( coreLoc_, args, argsi, callLoc )->val_;

	if( indexSigned < 0 )
		throw Exceptions::CoreOutOfRange( callLoc, coreLoc_, args, argsi, "The edge index cannot be negative." );
	size_t index = static_cast< size_t >( indexSigned );
	if( self_->graph( )->edgeCount( ) <= index )
		throw Exceptions::CoreOutOfRange( callLoc, coreLoc_, args, argsi, "The edge index must be less than the number of edges in the graph." );

	RefCountPtr< const Lang::Edge > edge = self_->graph( )->indexEdge( self_, index );
	Kernel::ContRef cont = evalState->cont_;
	cont->takeValue( edge, evalState );
}

Lang::GraphMethod_find_node::GraphMethod_find_node( RefCountPtr< const Lang::Graph > self, const Ast::FileID * fullMethodID )
	: Lang::MethodBase< Lang::Graph >( self, fullMethodID, false, true )
{
	formals_->appendEvaluatedCoreFormal( "key", Kernel::THE_SLOT_VARIABLE );
}

Lang::GraphMethod_find_node::~GraphMethod_find_node( )
{ }

void
Lang::GraphMethod_find_node::call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
{
	args.applyDefaults( callLoc );

	try
		{
			RefCountPtr< const Lang::Node > node = self_->graph( )->findNode( self_, args.getValue( 0 ) );
			Kernel::ContRef cont = evalState->cont_;
			cont->takeValue( node, evalState );
		}
	catch( const Exceptions::InvalidGraphKeyType & ball )
		{
			throw Exceptions::CoreTypeMismatch( callLoc, coreLoc_, args, 0, Exceptions::GraphKeyTypeMismatch::expectedType );
		}
	catch( const Exceptions::InvalidGraphKey & ball )
		{
			throw Exceptions::GraphKeyOutOfRange( Exceptions::GraphKeyOutOfRange::NODE, callLoc, coreLoc_, args, 0 );
		}
}


template< bool directed, bool undirected, const char * fieldID >
Lang::GraphMethod_find_edges< directed, undirected, fieldID >::GraphMethod_find_edges( RefCountPtr< const Lang::Graph > self, const Ast::FileID * fullMethodID )
	: Lang::MethodBase< Lang::Graph >( self, fullMethodID, false, true )
{
	formals_->appendEvaluatedCoreFormal( "source", Kernel::THE_VOID_VARIABLE );
	formals_->appendEvaluatedCoreFormal( "target", Kernel::THE_VOID_VARIABLE );
	formals_->appendEvaluatedCoreFormal( "label", Kernel::THE_VOID_VARIABLE );
	formals_->appendEvaluatedCoreFormal( "loops", Kernel::THE_TRUE_VARIABLE );
}

template< bool directed, bool undirected, const char * fieldID >
Lang::GraphMethod_find_edges< directed, undirected, fieldID >::~GraphMethod_find_edges( )
{ }

template< bool directed, bool undirected, const char * fieldID >
void
Lang::GraphMethod_find_edges< directed, undirected, fieldID >::call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
{
	args.applyDefaults( callLoc );

	if( ! ( ( directed && self_->graph( )->domain( ).directed( ) ) || ( undirected && self_->graph( )->domain( ).undirected( ) ) ) ){
		if( directed && undirected )
			WARN_OR_THROW( Exceptions::CoreRequirement( "The graph domain does not permit neither directed nor undirected edges.", coreLoc_, callLoc ) );
		if( directed )
			WARN_OR_THROW( Exceptions::CoreRequirement( "The graph domain does not permit directed edges.", coreLoc_, callLoc ) );
		if( undirected )
			WARN_OR_THROW( Exceptions::CoreRequirement( "The graph domain does not permit undirected edges.", coreLoc_, callLoc ) );
		Kernel::ContRef cont = evalState->cont_;
		cont->takeValue( Lang::THE_CONS_NULL,
										 evalState );
		return;
	}

	size_t argsi = 0;
	Kernel::Node * source = self_->kernelNodeByValue( args, argsi, true, coreLoc_, callLoc );

	++argsi;
	Kernel::Node * target = self_->kernelNodeByValue( args, argsi, true, coreLoc_, callLoc );

	++argsi;
	Kernel::ValueRef label = args.getValue( argsi );
	if( ! ( label == Lang::THE_VOID
					|| dynamic_cast< const Lang::Symbol * >( label.getPtr( ) ) != 0
					|| dynamic_cast< const Lang::Integer * >( label.getPtr( ) ) != 0 ) ){
		throw Exceptions::CoreTypeMismatch( callLoc, coreLoc_, args, argsi, Exceptions::GraphKeyTypeMismatch::expectedType );
	}

	++argsi;
	typedef const Lang::Boolean ArgType;
	bool loops = Helpers::down_cast_CoreArgument< ArgType >( coreLoc_, args, argsi, callLoc )->val_;

	Lang::Graph::ListRef res = self_->graph( )->findEdges( self_, source, target, directed, undirected, loops, label );

	Kernel::ContRef cont = evalState->cont_;
	cont->takeValue( res,
									 evalState );
}


template< bool directed, bool undirected, const char * fieldID >
Lang::GraphMethod_find_multiedges< directed, undirected, fieldID >::GraphMethod_find_multiedges( RefCountPtr< const Lang::Graph > self, const Ast::FileID * fullMethodID )
	: Lang::MethodBase< Lang::Graph >( self, fullMethodID, false, true )
{
	formals_->appendEvaluatedCoreFormal( "source", Kernel::THE_VOID_VARIABLE );
	formals_->appendEvaluatedCoreFormal( "target", Kernel::THE_VOID_VARIABLE );
	formals_->appendEvaluatedCoreFormal( "loops", Kernel::THE_TRUE_VARIABLE );
}

template< bool directed, bool undirected, const char * fieldID >
Lang::GraphMethod_find_multiedges< directed, undirected, fieldID >::~GraphMethod_find_multiedges( )
{ }

template< bool directed, bool undirected, const char * fieldID >
void
Lang::GraphMethod_find_multiedges< directed, undirected, fieldID >::call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
{
	args.applyDefaults( callLoc );

	if( ! ( ( directed && self_->graph( )->domain( ).directed( ) ) || ( undirected && self_->graph( )->domain( ).undirected( ) ) ) ){
		if( directed && undirected )
			WARN_OR_THROW( Exceptions::CoreRequirement( "The graph domain does not permit neither directed nor undirected edges.", coreLoc_, callLoc ) );
		if( directed )
			WARN_OR_THROW( Exceptions::CoreRequirement( "The graph domain does not permit directed edges.", coreLoc_, callLoc ) );
		if( undirected )
			WARN_OR_THROW( Exceptions::CoreRequirement( "The graph domain does not permit undirected edges.", coreLoc_, callLoc ) );
		Kernel::ContRef cont = evalState->cont_;
		cont->takeValue( Lang::THE_CONS_NULL,
										 evalState );
		return;
	}

	size_t argsi = 0;
	Kernel::Node * source = self_->kernelNodeByValue( args, argsi, true, coreLoc_, callLoc );

	++argsi;
	Kernel::Node * target = self_->kernelNodeByValue( args, argsi, true, coreLoc_, callLoc );

	++argsi;
	typedef const Lang::Boolean ArgType;
	bool loops = Helpers::down_cast_CoreArgument< ArgType >( coreLoc_, args, argsi, callLoc )->val_;

	Lang::Graph::ListRef res = self_->graph( )->findMultiEdges( self_, source, target, directed, undirected, loops );

	Kernel::ContRef cont = evalState->cont_;
	cont->takeValue( res,
									 evalState );
}


template< bool directed, bool undirected, const char * fieldID >
Lang::GraphMethod_the_edge< directed, undirected, fieldID >::GraphMethod_the_edge( RefCountPtr< const Lang::Graph > self, const Ast::FileID * fullMethodID )
	: Lang::MethodBase< Lang::Graph >( self, fullMethodID, false, true )
{
	formals_->appendEvaluatedCoreFormal( "source", Kernel::THE_SLOT_VARIABLE );
	formals_->appendEvaluatedCoreFormal( "target", Kernel::THE_SLOT_VARIABLE );
	formals_->appendEvaluatedCoreFormal( "label", Kernel::THE_VOID_VARIABLE );
}

template< bool directed, bool undirected, const char * fieldID >
Lang::GraphMethod_the_edge< directed, undirected, fieldID >::~GraphMethod_the_edge( )
{ }

template< bool directed, bool undirected, const char * fieldID >
void
Lang::GraphMethod_the_edge< directed, undirected, fieldID >::call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
{
	args.applyDefaults( callLoc );

	if( ! ( ( directed && self_->graph( )->domain( ).directed( ) ) || ( undirected && self_->graph( )->domain( ).undirected( ) ) ) ){
		if( directed && undirected )
			throw Exceptions::CoreRequirement( "The graph domain does not permit neither directed nor undirected edges.", coreLoc_, callLoc );
		if( directed )
			throw Exceptions::CoreRequirement( "The graph domain does not permit directed edges.", coreLoc_, callLoc );
		if( undirected )
			throw Exceptions::CoreRequirement( "The graph domain does not permit undirected edges.", coreLoc_, callLoc );
	}

	size_t argsi = 0;
	Kernel::Node * source = self_->kernelNodeByValue( args, argsi, true, coreLoc_, callLoc );

	++argsi;
	Kernel::Node * target = self_->kernelNodeByValue( args, argsi, true, coreLoc_, callLoc );

	++argsi;
	Kernel::ValueRef label = args.getValue( argsi );
	if( ! ( label == Lang::THE_VOID
					|| dynamic_cast< const Lang::Symbol * >( label.getPtr( ) ) != 0
					|| dynamic_cast< const Lang::Integer * >( label.getPtr( ) ) != 0 ) ){
		throw Exceptions::CoreTypeMismatch( callLoc, coreLoc_, args, argsi, Exceptions::GraphKeyTypeMismatch::expectedType );
	}

	Lang::Graph::ListRef edges = self_->graph( )->findEdges( self_, source, target, directed, undirected, true, label );
	RefCountPtr< const Lang::SingleListPair > p = edges.down_cast< const Lang::SingleListPair >( );
	if( p == NullPtr< const Lang::SingleListPair >( ) ){
		throw Exceptions::CoreRequirement( "Illegal reference to \"the edge\" since there is no matching edge.", coreLoc_, callLoc );
	}
	RefCountPtr< const Lang::Value > res = p->car_->getUntyped( );
	p = p->cdr_.down_cast< const Lang::SingleListPair >( );
	if( p != NullPtr< const Lang::SingleListPair >( ) ){
		throw Exceptions::CoreRequirement( "Illegal reference to \"the edge\" since there is more than one matching edge.", coreLoc_, callLoc );
	}

	Kernel::ContRef cont = evalState->cont_;
	cont->takeValue( res,
									 evalState );
}


template< bool directed, bool undirected, const char * fieldID >
Lang::GraphMethod_the_multiedge< directed, undirected, fieldID >::GraphMethod_the_multiedge( RefCountPtr< const Lang::Graph > self, const Ast::FileID * fullMethodID )
	: Lang::MethodBase< Lang::Graph >( self, fullMethodID, false, true )
{
	formals_->appendEvaluatedCoreFormal( "source", Kernel::THE_SLOT_VARIABLE );
	formals_->appendEvaluatedCoreFormal( "target", Kernel::THE_SLOT_VARIABLE );
}

template< bool directed, bool undirected, const char * fieldID >
Lang::GraphMethod_the_multiedge< directed, undirected, fieldID >::~GraphMethod_the_multiedge( )
{ }

template< bool directed, bool undirected, const char * fieldID >
void
Lang::GraphMethod_the_multiedge< directed, undirected, fieldID >::call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
{
	args.applyDefaults( callLoc );

	if( ! ( ( directed && self_->graph( )->domain( ).directed( ) ) || ( undirected && self_->graph( )->domain( ).undirected( ) ) ) ){
		if( directed && undirected )
			throw Exceptions::CoreRequirement( "The graph domain does not permit neither directed nor undirected edges.", coreLoc_, callLoc );
		if( directed )
			throw Exceptions::CoreRequirement( "The graph domain does not permit directed edges.", coreLoc_, callLoc );
		if( undirected )
			throw Exceptions::CoreRequirement( "The graph domain does not permit undirected edges.", coreLoc_, callLoc );
	}

	size_t argsi = 0;
	Kernel::Node * source = self_->kernelNodeByValue( args, argsi, true, coreLoc_, callLoc );

	++argsi;
	Kernel::Node * target = self_->kernelNodeByValue( args, argsi, true, coreLoc_, callLoc );

	Lang::Graph::ListRef edges = self_->graph( )->findMultiEdges( self_, source, target, directed, undirected, true );
	RefCountPtr< const Lang::SingleListPair > p = edges.down_cast< const Lang::SingleListPair >( );
	if( p == NullPtr< const Lang::SingleListPair >( ) ){
		throw Exceptions::CoreRequirement( "Illegal reference to \"the multiedge\" since there is no matching multiedge.", coreLoc_, callLoc );
	}
	RefCountPtr< const Lang::Value > res = p->car_->getUntyped( );
	p = p->cdr_.down_cast< const Lang::SingleListPair >( );
	if( p != NullPtr< const Lang::SingleListPair >( ) ){
		throw Exceptions::CoreRequirement( "Illegal reference to \"the multiedge\" since there is more than one matching multiedge.", coreLoc_, callLoc );
	}

	Kernel::ContRef cont = evalState->cont_;
	cont->takeValue( res,
									 evalState );
}


Lang::GraphMethod_rekey_with_index::GraphMethod_rekey_with_index( RefCountPtr< const Lang::Graph > self, const Ast::FileID * fullMethodID )
	: Lang::MethodBase< Lang::Graph >( self, fullMethodID, false, true )
{
	formals_->appendEvaluatedCoreFormal( "offset", Helpers::newValHandle( new Lang::Integer( 0 ) ) );
}

Lang::GraphMethod_rekey_with_index::~GraphMethod_rekey_with_index( )
{ }

void
Lang::GraphMethod_rekey_with_index::call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
{
	args.applyDefaults( callLoc );

	typedef const Lang::Integer ArgType;
	ArgType::ValueType offset = Helpers::down_cast_CoreArgument< ArgType >( coreLoc_, args, 0, callLoc )->val_;

	Kernel::ContRef cont = evalState->cont_;
	cont->takeValue( Kernel::ValueRef( new Lang::Graph( *self_, offset ) ),
									 evalState );
}

namespace Shapes
{
	namespace Kernel
	{

		class GraphMethod_with_values_cont : public Kernel::Continuation
		{
			RefCountPtr< const Lang::Graph > self_;
			Kernel::Graph::ItemType item_;
			Kernel::ContRef cont_;
			RefCountPtr< const Interaction::CoreLocation > coreLoc_;
		public:
			GraphMethod_with_values_cont( const RefCountPtr< const Lang::Graph > & self, Kernel::Graph::ItemType item, const Kernel::ContRef & cont, const RefCountPtr< const Interaction::CoreLocation > & coreLoc, const Ast::SourceLocation & callLoc );
			virtual ~GraphMethod_with_values_cont( );
			virtual void takeValue( const RefCountPtr< const Lang::Value > & val, Kernel::EvalState * evalState, bool dummy ) const;
			virtual Kernel::ContRef up( ) const;
			virtual RefCountPtr< const char > description( ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
		};

		class GraphMethod_subgraph_cont : public Kernel::Continuation
		{
			RefCountPtr< const Lang::Graph > self_;
			Kernel::Graph::SubgraphKind kind_;
			Kernel::ContRef cont_;
		public:
			GraphMethod_subgraph_cont( const RefCountPtr< const Lang::Graph > & self, Kernel::Graph::SubgraphKind kind, const Kernel::ContRef & cont, const Ast::SourceLocation & callLoc );
			virtual ~GraphMethod_subgraph_cont( );
			virtual void takeValue( const RefCountPtr< const Lang::Value > & val, Kernel::EvalState * evalState, bool dummy ) const;
			virtual Kernel::ContRef up( ) const;
			virtual RefCountPtr< const char > description( ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
		};

	}
}

template< Kernel::Graph::ItemType item, const char * fieldID >
Lang::GraphMethod_with_values< item, fieldID >::GraphMethod_with_values( RefCountPtr< const Lang::Graph > self, const Ast::FileID * fullMethodID )
	: Lang::MethodBase< Lang::Graph >( self, fullMethodID, false, true )
{
	formals_->appendEvaluatedCoreFormal( "values", Kernel::THE_SLOT_VARIABLE );
}

template< Kernel::Graph::ItemType item, const char * fieldID >
Lang::GraphMethod_with_values< item, fieldID >::~GraphMethod_with_values( )
{ }

template< Kernel::Graph::ItemType item, const char * fieldID >
void
Lang::GraphMethod_with_values< item, fieldID >::call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
{
	args.applyDefaults( callLoc );

	size_t argsi = 0;
	RefCountPtr< const Lang::Value > nodes = args.getValue( argsi );
	Kernel::ContRef cont = Kernel::ContRef( new Kernel::ForcingListContinuation
																					( Kernel::ContRef( new Kernel::GraphMethod_with_values_cont( self_, item, evalState->cont_, coreLoc_, callLoc ) ),
																						args.getLoc( argsi ),
																						false ) );
	evalState->cont_ = cont;
	cont->takeValue( nodes, evalState );
}

Kernel::GraphMethod_with_values_cont::GraphMethod_with_values_cont( const RefCountPtr< const Lang::Graph > & self, Kernel::Graph::ItemType item, const Kernel::ContRef & cont, const RefCountPtr< const Interaction::CoreLocation > & coreLoc, const Ast::SourceLocation & callLoc )
	: Kernel::Continuation( callLoc ), self_( self ), item_( item ), cont_( cont ), coreLoc_( coreLoc )
{ }

Kernel::GraphMethod_with_values_cont::~GraphMethod_with_values_cont( )
{ }

void
Kernel::GraphMethod_with_values_cont::takeValue( const RefCountPtr< const Lang::Value > & nodesUntyped, Kernel::EvalState * evalState, bool dummy ) const
{
	typedef const Lang::SingleList ArgType;
	RefCountPtr< ArgType > values = Helpers::down_cast< ArgType >( nodesUntyped, "< Internal error situation in GraphMethod_with_values_cont >" );

	RefCountPtr< Lang::Graph::ValueVector > valuesVec = RefCountPtr< Lang::Graph::ValueVector >( new Lang::Graph::ValueVector( ) );
	size_t valuesSize = 0; /* Initialize to quiet compiler warning. */
	switch( item_ )
		{
		case Kernel::Graph::NODE:
			valuesSize = self_->graph( )->nodeCount( );
			break;
		case Kernel::Graph::EDGE:
			valuesSize = self_->graph( )->edgeCount( );
			break;
		}
	valuesVec->reserve( valuesSize );

	RefCountPtr< const Lang::SingleListPair > p = values.down_cast< const Lang::SingleListPair >( );
	for( size_t i = 0; i < valuesSize; ++i ){
		if( p == NullPtr< const Lang::SingleListPair >( ) ){
			switch( item_ )
				{
				case Kernel::Graph::NODE:
					throw Exceptions::CoreRequirement( "The number of values is less than the number of nodes.", coreLoc_, traceLoc_ );
				case Kernel::Graph::EDGE:
					throw Exceptions::CoreRequirement( "The number of values is less than the number of edges.", coreLoc_, traceLoc_ );
				}
		}
		valuesVec->push_back( p->car_->getUntyped( ) );
		p = p->cdr_.down_cast< const Lang::SingleListPair >( );
	}
	if( p != NullPtr< const Lang::SingleListPair >( ) ){
		switch( item_ )
			{
			case Kernel::Graph::NODE:
				throw Exceptions::CoreRequirement( "The number of values exceeds the number of nodes.", coreLoc_, traceLoc_ );
			case Kernel::Graph::EDGE:
				throw Exceptions::CoreRequirement( "The number of values exceeds the number of edges.", coreLoc_, traceLoc_ );
			}
	}

	evalState->cont_ = cont_;
	switch( item_ )
		{
		case Kernel::Graph::NODE:
			cont_->takeValue( Kernel::ValueRef( new Lang::Graph( self_->graph( ), valuesVec, self_->edgeValues( ) ) ),
												evalState );
			break;
		case Kernel::Graph::EDGE:
			cont_->takeValue( Kernel::ValueRef( new Lang::Graph( self_->graph( ), self_->nodeValues( ), valuesVec ) ),
												evalState );
			break;
		}
}

Kernel::ContRef
Kernel::GraphMethod_with_values_cont::up( ) const
{
	return cont_;
}

RefCountPtr< const char >
Kernel::GraphMethod_with_values_cont::description( ) const
{
	switch( item_ )
		{
		case Kernel::Graph::NODE:
			return strrefdup( "graph with new node values" );
		case Kernel::Graph::EDGE:
			return strrefdup( "graph with new edge values" );
		}
	/* Not sure how to deal with compiler warnings here.
	 * Seems hard to address with the following two possible warnings at the same time:
	 *   1) Reaching end of non-void function.
	 *   2) Dead code.
	 */
	throw Exceptions::InternalError( "Kernel::GraphMethod_with_values_cont::description: Reached code that should be dead at end of non-void function." );
}

void
Kernel::GraphMethod_with_values_cont::gcMark( Kernel::GCMarkedSet & marked )
{
	const_cast< Lang::Graph * >( self_.getPtr( ) )->gcMark( marked );
	cont_->gcMark( marked );
}


template< Kernel::Graph::SubgraphKind kind, const char * fieldID >
Lang::GraphMethod_subgraph< kind, fieldID >::GraphMethod_subgraph( RefCountPtr< const Lang::Graph > self, const Ast::FileID * fullMethodID )
	: Lang::MethodBase< Lang::Graph >( self, fullMethodID, false, true )
{
	switch( kind )
		{
		case Kernel::Graph::INDUCED:
			formals_->appendEvaluatedCoreFormal( "nodes", Kernel::THE_SLOT_VARIABLE );
			break;
		case Kernel::Graph::SPANNING:
			formals_->appendEvaluatedCoreFormal( "edges", Kernel::THE_SLOT_VARIABLE );
			break;
		}
}

template< Kernel::Graph::SubgraphKind kind, const char * fieldID >
Lang::GraphMethod_subgraph< kind, fieldID >::~GraphMethod_subgraph( )
{ }

template< Kernel::Graph::SubgraphKind kind, const char * fieldID >
void
Lang::GraphMethod_subgraph< kind, fieldID >::call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
{
	args.applyDefaults( callLoc );

	size_t argsi = 0;
	RefCountPtr< const Lang::Value > nodes = args.getValue( argsi );
	Kernel::ContRef cont = Kernel::ContRef( new Kernel::ForcingListContinuation
																					( Kernel::ContRef( new Kernel::GraphMethod_subgraph_cont( self_, kind, evalState->cont_, callLoc ) ),
																						args.getLoc( argsi ),
																						false ) );
	evalState->cont_ = cont;
	cont->takeValue( nodes, evalState );
}

Kernel::GraphMethod_subgraph_cont::GraphMethod_subgraph_cont( const RefCountPtr< const Lang::Graph > & self, Kernel::Graph::SubgraphKind kind, const Kernel::ContRef & cont, const Ast::SourceLocation & callLoc )
	: Kernel::Continuation( callLoc ), self_( self ), kind_( kind ), cont_( cont )
{ }

Kernel::GraphMethod_subgraph_cont::~GraphMethod_subgraph_cont( )
{ }

void
Kernel::GraphMethod_subgraph_cont::takeValue( const RefCountPtr< const Lang::Value > & nodesUntyped, Kernel::EvalState * evalState, bool dummy ) const
{
	typedef const Lang::SingleList ArgType;
	RefCountPtr< ArgType > nodes = Helpers::down_cast< ArgType >( nodesUntyped, "< Internal error situation in GraphMethod_subgraph_cont >" );

	std::set< size_t > indices;
	RefCountPtr< const Lang::SingleListPair > p = nodes.down_cast< const Lang::SingleListPair >( );
	while( p != NullPtr< const Lang::SingleListPair >( ) ){
		size_t index;
		switch( kind_ )
			{
			case Kernel::Graph::INDUCED:
				{
					RefCountPtr< const Lang::Node > node = p->car_->getVal< const Lang::Node >( "Node to include in subgraph." );
					if( node->graph( ) != self_ )
						throw Exceptions::InvalidGraphElement( Exceptions::InvalidGraphElement::NODE, traceLoc_ );
					index = node->node( )->index( );
				}
				break;
			case Kernel::Graph::SPANNING:
				{
					RefCountPtr< const Lang::Edge > edge = p->car_->getVal< const Lang::Edge >( "Edge to include in subgraph." );
					if( edge->graph( ) != self_ )
						throw Exceptions::InvalidGraphElement( Exceptions::InvalidGraphElement::EDGE, traceLoc_ );
					index = edge->edge( )->index( );
				}
				break;
			}
		indices.insert( indices.begin( ), index );
		p = p->cdr_.down_cast< const Lang::SingleListPair >( );
	}

	evalState->cont_ = cont_;
	cont_->takeValue( Kernel::ValueRef( new Lang::Graph( *self_, indices, kind_ ) ),
										evalState );
}

Kernel::ContRef
Kernel::GraphMethod_subgraph_cont::up( ) const
{
	return cont_;
}

RefCountPtr< const char >
Kernel::GraphMethod_subgraph_cont::description( ) const
{
	switch( kind_ )
		{
		case Kernel::Graph::INDUCED:
			return strrefdup( "construct induced subgraph" );
		case Kernel::Graph::SPANNING:
			return strrefdup( "construct spanning subgraph" );
		}
	/* Not sure how to deal with compiler warnings here.
	 * Seems hard to address with the following two possible warnings at the same time:
	 *   1) Reaching end of non-void function.
	 *   2) Dead code.
	 */
	throw Exceptions::InternalError( "Kernel::GraphMethod_subgraph_cont::description: Reached code that should be dead at end of non-void function." );
}

void
Kernel::GraphMethod_subgraph_cont::gcMark( Kernel::GCMarkedSet & marked )
{
	const_cast< Lang::Graph * >( self_.getPtr( ) )->gcMark( marked );
	cont_->gcMark( marked );
}


Lang::Walk::Walk( const RefCountPtr< const Lang::Graph > & graph, const RefCountPtr< const Lang::Node > & first, const RefCountPtr< const Lang::Node > & last, const RefCountPtr< const Lang::SingleList > & edges, const Ast::SourceLocation & edgesLoc, const Ast::SourceLocation & callLoc )
	: graph_( graph ), first_( first ), last_( last ), edges_( edges )
{
	/* First check type of all edges and put in a std::list. */
	std::list< const Lang::Edge * > edgeList;
	typedef typeof edgeList EdgeListType;
	edgeCount_ = 0;
	{
		RefCountPtr< const Lang::SingleListPair > p = edges_.down_cast< const Lang::SingleListPair >( );
		while( p != NullPtr< const Lang::SingleListPair >( ) ){
			++edgeCount_;
			RefCountPtr< const Lang::Value > val = p->car_->getUntyped( );
			const Lang::Edge * e = dynamic_cast< const Lang::Edge * >( val.getPtr( ) );
			if( e == NULL ){
				throw Exceptions::TypeMismatch( edgesLoc, "Element in list of walk edges", val->getTypeName( ), Lang::Edge::staticTypeName( ) );
			}
			edgeList.push_back( e );
			p = p->cdr_.down_cast< const Lang::SingleListPair >( );
		}
	}

	/* The given <first> and <last> nodes may be null pointers, in which case they have to be determined from the
	 * other arguments.
	 */

	/* Make sure that first_ is defined. */
	if( first_ == NullPtr< const Lang::Node >( ) ){
		if( edgeList.size( ) == 0 ){
			if( last_ == NullPtr< const Lang::Node >( ) ){
				throw Exceptions::MiscellaneousRequirement( callLoc, "Either start or end node must be specified for an empty walk." );
			}else{
				first_ = last_;
			}
		}else{
			RefCountPtr< const Lang::SingleListPair > p = edges_.down_cast< const Lang::SingleListPair >( );
			RefCountPtr< const Lang::Edge > firstEdge = p->car_->getVal< const Lang::Edge >( "(internal error)" );
			bool firstEdgeDirected = firstEdge->directed( );
			EdgeListType::const_iterator e = edgeList.begin( );
			const Kernel::Edge * ke = (*e)->edge( );
			const Kernel::Node * n11 = ke->source( );
			const Kernel::Node * n12 = ke->target( );
			if( firstEdgeDirected || n11 == n12 ){
				first_ = RefCountPtr< const Lang::Node >( new Lang::Node( graph_, n11 ) );
			}else{
				if( edgeList.size( ) == 1 ){
					if( last_ == NullPtr< const Lang::Node >( ) ){
						throw Exceptions::MiscellaneousRequirement( callLoc, "Either start or end node must be specified for a walk consisting of a single undirected edge." );
					}else{
						first_ = RefCountPtr< const Lang::Node >( new Lang::Node( graph_, firstEdge->backtrace( last_->node( ), callLoc ) ) );
					}
				}else{
					/* There are at least two edges, and the first is undirected and not a loop. */
					++e;
					ke = (*e)->edge( );
					const Kernel::Node * n21 = ke->source( );
					const Kernel::Node * n22 = ke->target( );
					if( n11 == n21 || n11 == n22){
						first_ = RefCountPtr< const Lang::Node >( new Lang::Node( graph_, n12 ) );
					}else if( n12 == n21 || n12 == n22){
						first_ = RefCountPtr< const Lang::Node >( new Lang::Node( graph_, n11 ) );
					}else{
						throw Exceptions::MiscellaneousRequirement( callLoc, "The first two edges does not have a common node." );
					}
				}
			}
		}
	}

	if( first_->graph( ) != graph_ ){
		/* The caller is responsible for checking that the start and end nodes belong to the graph. */
		throw Exceptions::InternalError( "Lang::Walk::Walk: The start node does not belong to the graph." );
	}


	const Kernel::Node * n = first_->node( );

	simple_ = true; /* Initialize to true, set to false if detecting repeated node. */
	trail_ = true;  /* Initialize to true, set to false if detecting repeated edge. */

	{
		typedef std::set< const Kernel::Node * > NodeSet;
		typedef std::set< const Kernel::Edge * > EdgeSet;
		NodeSet coveredNodes;
		EdgeSet coveredEdges;

		coveredNodes.insert( n );

		for( EdgeListType::const_iterator e = edgeList.begin( ); e != edgeList.end( ); ++e ){
			{
				const Kernel::Edge * ke = (*e)->edge( );
				EdgeSet::const_iterator j = coveredEdges.find( ke );
				if( j == coveredEdges.end( ) ){
					coveredEdges.insert( coveredEdges.begin( ), ke );
				}else{
					trail_ = false;
				}
			}

			n = (*e)->trace( n, edgesLoc );

			{
				NodeSet::const_iterator j = coveredNodes.find( n );
				if( j == coveredNodes.end( ) ){
					coveredNodes.insert( coveredNodes.begin( ), n );
				}else{
					simple_ = false;
				}
			}
		}

		nodeCover_ = coveredNodes.size( ) == graph_->graph( )->nodeCount( );
		edgeCover_ = coveredEdges.size( ) == graph_->graph( )->edgeCount( );
	}

	/* Make sure that last_ is defined. */
	if( last_ == NullPtr< const Lang::Node >( ) ){
		last_ = RefCountPtr< const Lang::Node >( new Lang::Node( graph_, n ) );
	}else{
		if( last_->graph( ) != graph_ ){
			/* The caller is responsible for checking that the start and end nodes belong to the graph. */
			throw Exceptions::InternalError( "Lang::Walk::Walk: The end node does not belong to the graph." );
		}
		if( n != last_->node( ) ){
			throw Exceptions::MiscellaneousRequirement( callLoc, "The given end node is not at the end of the walk." );
		}
	}
}

Lang::Walk::~Walk( )
{ }

RefCountPtr< const Lang::Class > Lang::Walk::TypeID( new Lang::SystemFinalClass( strrefdup( "Walk" ) ) );
TYPEINFOIMPL( Walk );

Kernel::VariableHandle
Lang::Walk::getField( const char * fieldID, const RefCountPtr< const Lang::Value > & selfRef ) const
{
	if( strcmp( fieldID, "closed?" ) == 0 )
		{
			return closed_ ? Kernel::THE_TRUE_VARIABLE : Kernel::THE_FALSE_VARIABLE;
		}
	else if( strcmp( fieldID, "open?" ) == 0 )
		{
			return closed_ ? Kernel::THE_FALSE_VARIABLE : Kernel::THE_TRUE_VARIABLE;
		}
	else if( strcmp( fieldID, "simple?" ) == 0 )
		{
			return simple_ ? Kernel::THE_TRUE_VARIABLE : Kernel::THE_FALSE_VARIABLE;
		}
	else if( strcmp( fieldID, "trail?" ) == 0 )
		{
			return trail_ ? Kernel::THE_TRUE_VARIABLE : Kernel::THE_FALSE_VARIABLE;
		}
	else if( strcmp( fieldID, "node_cover?" ) == 0 )
		{
			return nodeCover_ ? Kernel::THE_TRUE_VARIABLE : Kernel::THE_FALSE_VARIABLE;
		}
	else if( strcmp( fieldID, "edge_cover?" ) == 0 )
		{
			return edgeCover_ ? Kernel::THE_TRUE_VARIABLE : Kernel::THE_FALSE_VARIABLE;
		}
	else if( strcmp( fieldID, "edge_count" ) == 0 )
		{
			return Helpers::newValHandle( new Lang::Integer( edgeCount_) );
		}
	else if( strcmp( fieldID, "start" ) == 0 )
		{
			return Kernel::VariableHandle( new Kernel::Variable( first_ ) );
		}
	else if( strcmp( fieldID, "end" ) == 0 )
		{
			return Kernel::VariableHandle( new Kernel::Variable( last_ ) );
		}
	else if( strcmp( fieldID, "edges" ) == 0 )
		{
			return Kernel::VariableHandle( new Kernel::Variable( edges_ ) );
		}
	else if( strcmp( fieldID, "graph" ) == 0 )
		{
			return Kernel::VariableHandle( new Kernel::Variable( graph_ ) );
		}
	throw Exceptions::NonExistentMember( getTypeName( ), fieldID );
}

void
Lang::Walk::show( std::ostream & os ) const
{
	os << "( graph walk of length " << edgeCount_ << " )" ;
}

void
Lang::Walk::gcMark( Kernel::GCMarkedSet & marked )
{
	/* Optimizing by only calling gcMark on graph_, since the other variables
	 * are of controlled types and are known not to link to anything which
	 * is not already covered by graph_.
	 */
	const_cast< Lang::Graph * >( graph_.getPtr( ) )->gcMark( marked );
}


RefCountPtr< const Lang::Graph >
Helpers::graphFromNodeEdgeData( const Kernel::GraphDomain & domain, const RefCountPtr< const Lang::SingleList > & partitions, const std::list< Kernel::NodeData > & nodeData, const std::list< Kernel::EdgeData > & edgeData, const Ast::SourceLocation & partitionsLoc, const Ast::SourceLocation & callLoc )
{
	std::list< Kernel::EdgeDataIndexed > uedgeDataIndexed;
	std::list< Kernel::EdgeDataIndexed > dedgeDataIndexed;
	std::list< Kernel::LoopDataIndexed > uloopDataIndexed;
	std::list< Kernel::LoopDataIndexed > dloopDataIndexed;

	std::map< int, size_t > symbolKeyIndexMap;
	std::map< int, size_t > integerKeyIndexMap;

	bool hasEdgeLabels = false;
	bool partitioned = ( partitions != NullPtr< const Lang::SingleList >( ) );

	if( partitioned && domain.loops( ) ){
		throw Exceptions::MiscellaneousRequirement( callLoc, "The domain of a partitioned graph must not allow loops." );
	}

	/* Check type and uniqueness of partition keys. */
	std::set< Lang::Symbol::KeyType > symKeys;
	std::set< Lang::Integer::ValueType > intKeys;
	if( partitioned ){
		RefCountPtr< const Lang::SingleListPair > p = partitions.down_cast< const Lang::SingleListPair >( );
		while( p != NullPtr< const Lang::SingleListPair >( ) ){
			RefCountPtr< const Lang::Value > val = p->car_->getUntyped( );

			do{

				if( const Lang::Symbol * ptr = dynamic_cast< const Lang::Symbol * >( val.getPtr( ) ) ){
					Lang::Symbol::KeyType key = ptr->getKey( );
					if( symKeys.find( key ) != symKeys.end( ) ){
						std::ostringstream oss;
						oss << "The graph partition key " ;
						val->show( oss );
						oss << " is repeated." ;
						throw Exceptions::MiscellaneousRequirement( partitionsLoc, strrefdup( oss ) );
					}
					symKeys.insert( key );
					break;
				}

				if( const Lang::Integer * ptr = dynamic_cast< const Lang::Integer * >( val.getPtr( ) ) ){
					Lang::Integer::ValueType key = ptr->val_;
					if( intKeys.find( key ) != intKeys.end( ) ){
						std::ostringstream oss;
						oss << "The graph partition key " ;
						val->show( oss );
						oss << " is repeated." ;
						throw Exceptions::MiscellaneousRequirement( partitionsLoc, strrefdup( oss ) );
					}
					intKeys.insert( key );
					break;
				}

				throw Exceptions::TypeMismatch( partitionsLoc, "Graph partition key in graph construction", val->getTypeName( ), Exceptions::GraphKeyTypeMismatch::expectedType );

			}while( false );

			p = p->cdr_.down_cast< const Lang::SingleListPair >( );
		}
	}

	{
		typedef typeof nodeData ListType;
		size_t i = 0;
		for( ListType::const_iterator n = nodeData.begin( ); n != nodeData.end( ); ++n, ++i ){
			RefCountPtr< const Lang::Value > key = n->key_;
			if( const Lang::Symbol * keySym = dynamic_cast< const Lang::Symbol * >( key.getPtr( ) ) ){
				symbolKeyIndexMap[ keySym->getKey( ) ] = i;
			} else if( const Lang::Integer * keyInt = dynamic_cast< const Lang::Integer * >( key.getPtr( ) ) ){
				integerKeyIndexMap[ keyInt->val_ ] = i;
			} else {
				throw Exceptions::TypeMismatch( callLoc, "NodeData field 'key' in graph construction", key->getTypeName( ), Exceptions::GraphKeyTypeMismatch::expectedType );
			}

			RefCountPtr< const Lang::Value > partition = n->partition_;
			if( partitioned ){
				if( partition == Lang::THE_VOID ){
					throw Exceptions::MiscellaneousRequirement( callLoc, "NodeData field 'partition' cannot be void in a graph with partitions." );
				}
				if( const Lang::Symbol * keySym = dynamic_cast< const Lang::Symbol * >( partition.getPtr( ) ) ){
					if( symKeys.find( keySym->getKey( ) ) == symKeys.end( ) ){
						throw Exceptions::InvalidGraphPartition( partition, partitions, callLoc );
					}
				} else if( const Lang::Integer * keyInt = dynamic_cast< const Lang::Integer * >( partition.getPtr( ) ) ){
					if( intKeys.find( keyInt->val_ ) == intKeys.end( ) ){
						throw Exceptions::InvalidGraphPartition( partition, partitions, callLoc );
					}
				} else {
					throw Exceptions::TypeMismatch( callLoc, "NodeData field 'partition' in graph construction", partition->getTypeName( ), Exceptions::GraphKeyTypeMismatch::expectedType );
				}
			}else{
				if( partition != Lang::THE_VOID ){
					throw Exceptions::MiscellaneousRequirement( callLoc, "NodeData field 'partition' must be void in a graph without partitions." );
				}
			}
		}
	}

	{
		std::set< std::pair< size_t, size_t > > directedSet; /* For detection of parallel edges. */
		std::set< std::pair< size_t, size_t > > undirectedSet; /* For detection of parallel edges. */

		typedef typeof edgeData ListType;
		for( ListType::const_iterator e = edgeData.begin( ); e != edgeData.end( ); ++e ){
			RefCountPtr< const Lang::Value > sourceKey = e->sourceKey_;
			RefCountPtr< const Lang::Value > targetKey = e->targetKey_;
			const Lang::Symbol * keySym;
			const Lang::Integer * keyInt;

			size_t source;
			if( ( keySym = dynamic_cast< const Lang::Symbol * >( sourceKey.getPtr( ) ) ) != 0 ){
				source = symbolKeyIndexMap[ keySym->getKey( ) ];
			} else if( ( keyInt = dynamic_cast< const Lang::Integer * >( sourceKey.getPtr( ) ) ) != 0 ){
				source = integerKeyIndexMap[ keyInt->val_ ];
			} else {
				throw Exceptions::TypeMismatch( callLoc, "EdgeData field 'source' in graph construction", sourceKey->getTypeName( ), Exceptions::GraphKeyTypeMismatch::expectedType );
			}

			size_t target;
			if( ( keySym = dynamic_cast< const Lang::Symbol * >( targetKey.getPtr( ) ) ) != 0 ){
				target = symbolKeyIndexMap[ keySym->getKey( ) ];
			}else if( ( keyInt = dynamic_cast< const Lang::Integer * >( targetKey.getPtr( ) ) ) != 0 ){
				target = integerKeyIndexMap[ keyInt->val_ ];
			}else{
				throw Exceptions::TypeMismatch( callLoc, "EdgeData field 'target' in graph construction", targetKey->getTypeName( ), Exceptions::GraphKeyTypeMismatch::expectedType );
			}

			{
				Kernel::ValueRef label = e->label_;
				if( dynamic_cast< const Lang::Void * >( label.getPtr( ) ) != 0 ){
					/* Edge has no label */
				} else if( dynamic_cast< const Lang::Symbol * >( label.getPtr( ) ) != 0 ){
					hasEdgeLabels = true;
				} else if( dynamic_cast< const Lang::Integer * >( label.getPtr( ) ) != 0 ){
					hasEdgeLabels = true;
				} else {
					throw Exceptions::TypeMismatch( callLoc, "EdgeData field 'label' in graph construction", label->getTypeName( ), Exceptions::GraphKeyTypeMismatch::expectedType );
				}
			}

			if( e->directed_ ){
				if( ! domain.directed( ) )
					throw Exceptions::GraphDomainError( Exceptions::GraphDomainError::DIRECTED, e->directed_, sourceKey, targetKey, callLoc );
				if( ! domain.parallel( ) ){
					std::pair< size_t, size_t > newEdge = std::pair< size_t, size_t >( source, target );
					if( directedSet.find( newEdge ) != directedSet.end( ) )
						throw Exceptions::GraphDomainError( Exceptions::GraphDomainError::PARALLEL, e->directed_, sourceKey, targetKey, callLoc );
					directedSet.insert( directedSet.begin( ), newEdge );
				}
				if( source == target ){
					if( ! domain.loops( ) )
						throw Exceptions::GraphDomainError( Exceptions::GraphDomainError::LOOPS, e->directed_, sourceKey, targetKey, callLoc );
					dloopDataIndexed.push_back( Kernel::LoopDataIndexed( source, e->value_, e->label_ ) );
				}else{
					dedgeDataIndexed.push_back( Kernel::EdgeDataIndexed( source, target, e->value_, e->label_ ) );
				}
			}else{
				if( source > target ){
					size_t tmpIndex = source;
					source = target;
					target = tmpIndex;
					RefCountPtr< const Lang::Value > tmpVal = sourceKey;
					sourceKey = targetKey;
					targetKey = tmpVal;
				}
				if( ! domain.undirected( ) )
					throw Exceptions::GraphDomainError( Exceptions::GraphDomainError::UNDIRECTED, e->directed_, sourceKey, targetKey, callLoc );
				if( ! domain.parallel( ) ){
					std::pair< size_t, size_t > newEdge = std::pair< size_t, size_t >( source, target );
					if( undirectedSet.find( newEdge ) != undirectedSet.end( ) )
						throw Exceptions::GraphDomainError( Exceptions::GraphDomainError::PARALLEL, e->directed_, sourceKey, targetKey, callLoc );
					undirectedSet.insert( undirectedSet.begin( ), newEdge );
				}
				if( source == target ){
					if( ! domain.loops( ) )
						throw Exceptions::GraphDomainError( Exceptions::GraphDomainError::LOOPS, e->directed_, sourceKey, targetKey, callLoc );
					uloopDataIndexed.push_back( Kernel::LoopDataIndexed( source, e->value_, e->label_ ) );
				}else{
					uedgeDataIndexed.push_back( Kernel::EdgeDataIndexed( source, target, e->value_, e->label_ ) );
				}
			}
		}
	}

	RefCountPtr< const Lang::Graph::ValueVector > nodeValues = RefCountPtr< const Lang::Graph::ValueVector >( NullPtr< const Lang::Graph::ValueVector >( ) );
	RefCountPtr< const Lang::Graph::ValueVector > edgeValues = RefCountPtr< const Lang::Graph::ValueVector >( NullPtr< const Lang::Graph::ValueVector >( ) );
	RefCountPtr< const Kernel::Graph > kernelGraph( new Kernel::Graph( domain, partitions, nodeData, hasEdgeLabels, uedgeDataIndexed, dedgeDataIndexed, uloopDataIndexed, dloopDataIndexed, & nodeValues, & edgeValues, callLoc ) );

	return RefCountPtr< const Lang::Graph >( new Lang::Graph( kernelGraph, nodeValues, edgeValues ) );
}

namespace Shapes
{
	namespace Kernel
	{

		class Mutator_Graph_addNode : public Lang::CoreMutator
		{
		public:
			Mutator_Graph_addNode( const char * name )
				: CoreMutator( name, new Kernel::EvaluatedFormals( Ast::FileID::build_internal( name ), true ) )
			{
				formals_->appendEvaluatedCoreFormal( "key", Kernel::THE_SLOT_VARIABLE );
				formals_->appendEvaluatedCoreFormal( "value", Kernel::THE_VOID_VARIABLE );
				formals_->appendEvaluatedCoreFormal( "partition", Kernel::THE_VOID_VARIABLE );
			}
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				args.applyDefaults( callLoc );

				typedef WarmGraph StateType;
				StateType * state = Helpers::mutator_cast_self< StateType >( args.getMutatorSelf( ) );

				size_t argsi = 0;
				RefCountPtr< const Lang::Value > key = args.getValue( argsi );
				do{
					if( dynamic_cast< const Lang::Symbol * >( key.getPtr( ) ) != 0 )
						break;
					if( dynamic_cast< const Lang::Integer * >( key.getPtr( ) ) != 0 )
						break;
					throw Exceptions::CoreTypeMismatch( callLoc, new Interaction::MutatorLocation( state, name_ ), args, argsi, Exceptions::GraphKeyTypeMismatch::expectedType );
				}while( false );

				++argsi;
				RefCountPtr< const Lang::Value > value = args.getValue( argsi );

				++argsi;
				RefCountPtr< const Lang::Value > partition = args.getValue( argsi );
				do{
					if( partition == Lang::THE_VOID )
						break;
					if( dynamic_cast< const Lang::Symbol * >( key.getPtr( ) ) != 0 )
						break;
					if( dynamic_cast< const Lang::Integer * >( key.getPtr( ) ) != 0 )
						break;
					throw Exceptions::CoreTypeMismatch( callLoc, new Interaction::MutatorLocation( state, name_ ), args, argsi, Exceptions::GraphKeyTypeMismatch::expectedType );
				}while( false );

				if( state->partitionKeys( ) != NullPtr< const Lang::SingleList >( ) ){
					if( partition == Lang::THE_VOID )
						throw Exceptions::MiscellaneousRequirement( callLoc, "NodeData field 'partition' cannot be void in a graph with partitions." );
					/* Although we could check that partition is a valid key here, we let Helpers::graphFromNodeEdgeData
					 * take care of that.  If it turns out to be a big problem that the errors are not reportat at the place of insertion,
					 * this can be changed.
					 */
				}else{
					if( partition != Lang::THE_VOID )
						throw Exceptions::MiscellaneousRequirement( callLoc, "NodeData field 'partition' must be void in a graph without partitions." );
				}

				state->addNode( Kernel::NodeData( key, value, partition ) );

				Kernel::ContRef cont = evalState->cont_;
				cont->takeValue( Lang::THE_VOID,
												 evalState );
			}
		};

		class Mutator_Graph_addEdge : public Lang::CoreMutator
		{
		public:
			Mutator_Graph_addEdge( const char * name )
				: CoreMutator( name, new Kernel::EvaluatedFormals( Ast::FileID::build_internal( name ), true ) )
			{
				formals_->appendEvaluatedCoreFormal( "source", Kernel::THE_SLOT_VARIABLE );
				formals_->appendEvaluatedCoreFormal( "target", Kernel::THE_SLOT_VARIABLE );
				formals_->appendEvaluatedCoreFormal( "value", Kernel::THE_VOID_VARIABLE );
				formals_->appendEvaluatedCoreFormal( "label", Kernel::THE_VOID_VARIABLE );
				formals_->appendEvaluatedCoreFormal( "directed", Kernel::THE_VOID_VARIABLE );
			}
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				args.applyDefaults( callLoc );

				typedef WarmGraph StateType;
				StateType * state = Helpers::mutator_cast_self< StateType >( args.getMutatorSelf( ) );

				size_t argsi = 0;
				RefCountPtr< const Lang::Value > sourceKey = args.getValue( argsi );
				do{
					if( dynamic_cast< const Lang::Symbol * >( sourceKey.getPtr( ) ) != 0 )
						break;
					if( dynamic_cast< const Lang::Integer * >( sourceKey.getPtr( ) ) != 0 )
						break;
					throw Exceptions::CoreTypeMismatch( callLoc, new Interaction::MutatorLocation( state, name_ ), args, argsi, Exceptions::GraphKeyTypeMismatch::expectedType );
				}while( false );

				++argsi;
				RefCountPtr< const Lang::Value > targetKey = args.getValue( argsi );
				do{
					if( dynamic_cast< const Lang::Symbol * >( targetKey.getPtr( ) ) != 0 )
						break;
					if( dynamic_cast< const Lang::Integer * >( targetKey.getPtr( ) ) != 0 )
						break;
					throw Exceptions::CoreTypeMismatch( callLoc, new Interaction::MutatorLocation( state, name_ ), args, argsi, Exceptions::GraphKeyTypeMismatch::expectedType );
				}while( false );

				++argsi;
				RefCountPtr< const Lang::Value > value = args.getValue( argsi );

				++argsi;
				RefCountPtr< const Lang::Value > label = args.getValue( argsi );
				if( ! ( label == Lang::THE_VOID
								|| dynamic_cast< const Lang::Symbol * >( label.getPtr( ) ) != 0
								|| dynamic_cast< const Lang::Integer * >( label.getPtr( ) ) != 0 ) ){
					throw Exceptions::CoreTypeMismatch( callLoc, new Interaction::MutatorLocation( state, name_ ), args, argsi, Exceptions::GraphKeyTypeMismatch::expectedType );
				}

				++argsi;
				typedef const Lang::Boolean DirectedType;
				RefCountPtr< DirectedType > directedVal = Helpers::down_cast_MutatorArgument< DirectedType >( state, name_, args, argsi, callLoc, true );
				bool directed;
				if( directedVal != NullPtr< DirectedType >( ) ){
					directed = directedVal->val_;
					if( directed ){
						if( not state->domain( ).directed( ) )
							throw Exceptions::GraphDomainError( Exceptions::GraphDomainError::DIRECTED, directed, sourceKey, targetKey, callLoc );
					}else{
						if( not state->domain( ).undirected( ) )
							throw Exceptions::GraphDomainError( Exceptions::GraphDomainError::UNDIRECTED, directed, sourceKey, targetKey, callLoc );
					}
				}else{
					if( state->domain( ).directed( ) && not state->domain( ).undirected( ) )
						directed = true;
					else if( not state->domain( ).directed( ) && state->domain( ).undirected( ) )
						directed = false;
					else
						throw Exceptions::MiscellaneousRequirement( callLoc, "The argument 'directed' must be specified since the graph domain does not determine a default interpretation." );
				}

				state->addEdge( Kernel::EdgeData( directed, sourceKey, targetKey, value, label ) );

				Kernel::ContRef cont = evalState->cont_;
				cont->takeValue( Lang::THE_VOID,
												 evalState );
			}
		};

	}
}

Kernel::WarmGraph::WarmGraph( const Kernel::GraphDomain & domain, const RefCountPtr< const Lang::SingleList > & partitionKeys, RefCountPtr< std::list< Kernel::NodeData > > & nodeDataMem, RefCountPtr< std::list< Kernel::EdgeData > > & edgeDataMem )
	: domain_( domain ), partitionKeys_( partitionKeys ), nodeDataMem_( nodeDataMem ), edgeDataMem_( edgeDataMem ), nodeData_( *nodeDataMem_ ), edgeData_( *edgeDataMem_ )
{ }

Kernel::WarmGraph::~WarmGraph( )
{ }

void
WarmGraph_register_mutators( Lang::SystemFinalClass * dstClass )
{
	dstClass->registerMutator( new Kernel::Mutator_Graph_addNode( "node" ) );
	dstClass->registerMutator( new Kernel::Mutator_Graph_addEdge( "edge" ) );
}

RefCountPtr< const Lang::Class > Kernel::WarmGraph::TypeID( new Lang::SystemFinalClass( strrefdup( "#Graph" ), WarmGraph_register_mutators ) );
TYPEINFOIMPL_STATE( WarmGraph );

void
Kernel::WarmGraph::tackOnImpl( Kernel::EvalState * evalState, const RefCountPtr< const Lang::Value > & piece, const Ast::SourceLocation & callLoc )
{
	throw Exceptions::MiscellaneousRequirement( strrefdup( "A #Graph state has no tack on operation, consider using one of the named mutators instead." ) );
}

void
Kernel::WarmGraph::freezeImpl( Kernel::EvalState * evalState, const Ast::SourceLocation & callLoc )
{
	Kernel::ContRef cont = evalState->cont_;
	/* Passing callLoc for the source location of the partition keys.  It's not right, but it doesn't matter since the
   * partition keys have already been checked.
   */
	cont->takeValue( Helpers::graphFromNodeEdgeData( domain_, partitionKeys_, nodeData_, edgeData_, callLoc, callLoc ),
									 evalState );
}

void
Kernel::WarmGraph::peekImpl( Kernel::EvalState * evalState, const Ast::SourceLocation & callLoc )
{
	Kernel::ContRef cont = evalState->cont_;
	/* Passing callLoc for the source location of the partition keys.  It's not right, but it doesn't matter since the
   * partition keys have already been checked.
   */
	cont->takeValue( Helpers::graphFromNodeEdgeData( domain_, partitionKeys_, nodeData_, edgeData_, callLoc, callLoc ),
									 evalState );
}

void
Kernel::WarmGraph::gcMark( Kernel::GCMarkedSet & marked )
{ }

void
Kernel::WarmGraph::addNode( const Kernel::NodeData & node )
{
	nodeData_.push_back( node );
}

void
Kernel::WarmGraph::addEdge( const Kernel::EdgeData & edge )
{
	edgeData_.push_back( edge );
}
