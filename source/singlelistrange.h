/* This file is part of Shapes.
 *
 * Shapes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Shapes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010, 2013 Henrik Tidefelt
 */

#include "containertypes.h"
#include "shapesexceptions.h"
#include "continuations.h"
#include "globals.h"

/* This file is divided in two parts.  The first part contains the declarations.
 */
namespace Shapes
{
	namespace Lang
	{

		template< class T >
		class SingleListRange : public Lang::SingleList
		{
			typedef typename T::ValueType elem_type;
			elem_type begin_;
			elem_type step_;
			size_t count_;
		public:
			SingleListRange( elem_type begin, elem_type step, size_t count );
			virtual ~SingleListRange( );
			virtual void show( std::ostream & os ) const;
			virtual bool isNull( ) const;
			virtual bool isForced( ) const;
			virtual void foldl( Kernel::EvalState * evalState, const RefCountPtr< const Lang::Function > & op, const Kernel::VariableHandle & nullResult, const Ast::SourceLocation & callLoc ) const;
			virtual void foldr( Kernel::EvalState * evalState, const RefCountPtr< const Lang::Function > & op, const Kernel::VariableHandle & nullResult, const Ast::SourceLocation & callLoc ) const;
			virtual void foldsl( Kernel::EvalState * evalState, const RefCountPtr< const Lang::Function > & op, const Kernel::VariableHandle & nullResult, Kernel::StateHandle state, const Ast::SourceLocation & callLoc ) const;
			virtual void foldsr( Kernel::EvalState * evalState, const RefCountPtr< const Lang::Function > & op, const Kernel::VariableHandle & nullResult, Kernel::StateHandle state, const Ast::SourceLocation & callLoc ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked ){ };
			virtual Kernel::VariableHandle getField( const char * fieldID, const RefCountPtr< const Lang::Value > & selfRef ) const;

			RefCountPtr< const Lang::SingleListPair > consify( ) const;
			void expand( std::vector< elem_type > * dst ) const;
		};

	}
}


/* This is the start of the second part of the file, containing the implementation of the templates declared in the first part.
 */
namespace Shapes
{

	template< class T >
	Lang::SingleListRange< T >::SingleListRange( elem_type begin, elem_type step, size_t count )
		: begin_( begin ), step_( step ), count_( count )
	{
		if( count_ == 0 )
			throw Exceptions::InternalError( "Attempt to construct SingleListRange with zero count." );
	}

	template< class T >
	Lang::SingleListRange< T >::~SingleListRange( )
	{ }

	template< class T >
	void
	Lang::SingleListRange< T >::show( std::ostream & os ) const
	{
		os << "< range with " << count_ << " elements >" ;
	}

	template< class T >
	bool
	Lang::SingleListRange< T >::isNull( ) const
	{
		return false;
	}

	template< class T >
	bool
	Lang::SingleListRange< T >::isForced( ) const
	{
		return true;
	}

	template< class T >
	void
	Lang::SingleListRange< T >::foldl( Kernel::EvalState * evalState, const RefCountPtr< const Lang::Function > & op, const Kernel::VariableHandle & nullResult, const Ast::SourceLocation & callLoc ) const
	{
		if( count_ > 1 ){
			evalState->cont_ = Kernel::ContRef( new Kernel::SingleFoldLCont( RefCountPtr< const Lang::SingleList >( new Lang::SingleListRange< T >( begin_ + step_, step_, count_ - 1 ) ),
																																			 op, evalState->dyn_, evalState->cont_, callLoc ) );
		}

		op->call( op, evalState, nullResult, Helpers::newValHandle( new T( begin_ ) ), callLoc );
	}

	template< class T >
	void
	Lang::SingleListRange< T >::foldr( Kernel::EvalState * evalState, const RefCountPtr< const Lang::Function > & op, const Kernel::VariableHandle & nullResult, const Ast::SourceLocation & callLoc ) const
	{
		evalState->cont_ = Kernel::ContRef( new Kernel::SingleFoldRCont( Helpers::newValHandle( new T( begin_ ) ), op, evalState->dyn_, evalState->cont_, callLoc ) );

		if( count_ > 1 ){
			Lang::SingleListRange< T >( begin_ + step_, step_, count_ - 1 ).foldr( evalState, op, nullResult, callLoc );
		}else{
			Kernel::ContRef cont = evalState->cont_;
			cont->takeHandle( nullResult,
												evalState );
		}
	}

	template< class T >
	void
	Lang::SingleListRange< T >::foldsl( Kernel::EvalState * evalState, const RefCountPtr< const Lang::Function > & op, const Kernel::VariableHandle & nullResult, Kernel::StateHandle state, const Ast::SourceLocation & callLoc ) const
	{
		if( count_ > 1 ){
			evalState->cont_ = Kernel::ContRef( new Kernel::SingleFoldSLCont( RefCountPtr< const Lang::SingleList >( new Lang::SingleListRange< T >( begin_ + step_, step_, count_ - 1 ) ),
																																				op, state, evalState->dyn_, evalState->cont_, callLoc ) );
		}

		op->call( op, evalState, nullResult, Helpers::newValHandle( new T( begin_ ) ), state, callLoc );
	}

	template< class T >
	void
	Lang::SingleListRange< T >::foldsr( Kernel::EvalState * evalState, const RefCountPtr< const Lang::Function > & op, const Kernel::VariableHandle & nullResult, Kernel::StateHandle state, const Ast::SourceLocation & callLoc ) const
	{
		evalState->cont_ = Kernel::ContRef( new Kernel::SingleFoldSRCont( Helpers::newValHandle( new T( begin_ ) ), op, state, evalState->dyn_, evalState->cont_, callLoc ) );

		if( count_ > 1 ){
			Lang::SingleListRange< T >( begin_ + step_, step_, count_ - 1 ).foldsr( evalState, op, nullResult, state, callLoc );
		}else{
			Kernel::ContRef cont = evalState->cont_;
			cont->takeHandle( nullResult,
												evalState );
		}
	}

	template< class T >
	Kernel::VariableHandle
	Lang::SingleListRange< T >::getField( const char * fieldId, const RefCountPtr< const Lang::Value > & selfRef ) const
	{
		if( strcmp( fieldId, "car" ) == 0 )
			{
				return Helpers::newValHandle( new T( begin_ ) );
			}
		if( strcmp( fieldId, "cdr" ) == 0 )
			{
				if( count_ == 1 ){
					return Kernel::VariableHandle( new Kernel::Variable( Lang::THE_CONS_NULL ) );
				}
				return Helpers::newValHandle( new SingleListRange< T >( begin_ + step_, step_, count_ - 1 ) );
			}

		return SingleList::getField( fieldId, selfRef ); /* This will throw if there is no such method. */
	}

	template< class T >
	RefCountPtr< const Lang::SingleListPair >
	Lang::SingleListRange< T >::consify( ) const
	{
		std::vector< elem_type > mem;
		mem.reserve( count_ );
		size_t c = count_;
		elem_type i = begin_;
		while( c > 0 )
			{
				mem.push_back( i );
				i += step_;
				--c;
			}

		RefCountPtr< const Lang::SingleListPair > result =
			RefCountPtr< const Lang::SingleListPair >
			( new Lang::SingleListPair( Kernel::VariableHandle( new Kernel::Variable( Kernel::ValueRef( new T( mem.back( ) ) ) ) ),
																	Lang::THE_CONS_NULL ) );
		mem.pop_back( );
		while( ! mem.empty( ) ){
			result =
				RefCountPtr< const Lang::SingleListPair >
				( new Lang::SingleListPair( Kernel::VariableHandle( new Kernel::Variable( Kernel::ValueRef( new T( mem.back( ) ) ) ) ),
																		result ) );
			mem.pop_back( );
		}

		return result;
	}

	template< class T >
	void
	Lang::SingleListRange< T >::expand( std::vector< elem_type > * dst ) const
	{
		dst->reserve( dst->size( ) + count_ );
		size_t c = count_;
		elem_type i = begin_;
		while( c > 0 )
			{
				dst->push_back( i );
				i += step_;
				--c;
			}
	}

}
