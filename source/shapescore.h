/* This file is part of Shapes.
 *
 * Shapes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Shapes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2008, 2010, 2015 Henrik Tidefelt
 */

#pragma once

#include <list>
#include <iostream>
#include <string>

#include "refcount.h"
#include "shapestypes.h"


namespace Shapes
{
  namespace Lang
  {

    class CoreFunction : public Lang::Function
    {
    protected:
      const Ast::PlacedIdentifier id_;
    public:
      CoreFunction( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name );
      CoreFunction( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name, Kernel::EvaluatedFormals * formals );
      virtual void analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst );
      virtual bool isTransforming( ) const;
      const Ast::PlacedIdentifier & id( ) const;
      virtual void show( std::ostream & os ) const;
      virtual void gcMark( Kernel::GCMarkedSet & marked ){ };
    };

    class CoreMutator : public Lang::Function
    {
    protected:
      const char * name_;
    public:
      CoreMutator( const char * name );
      CoreMutator( const char * name, Kernel::EvaluatedFormals * formals );
      virtual void analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst );
      virtual bool isTransforming( ) const;
      const char * name( ) const;
      virtual void show( std::ostream & os ) const;
      virtual void gcMark( Kernel::GCMarkedSet & marked ){ };
    };


    /* This core function is declared here so that its static member function can be used from other files than coreconstruct.cc. */
    class Core_range : public Lang::CoreFunction
    {
    public:
      Core_range( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name );
      virtual void call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const;
      static void makeRange( const RefCountPtr< const Interaction::CoreLocation > & coreLoc, Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc, bool isSpan = false, RefCountPtr< const Lang::Value > last = RefCountPtr< const Lang::Value >( NullPtr< const Lang::Value >( ) ) );
    };

  }

}
