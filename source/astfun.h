/* This file is part of Shapes.
 *
 * Shapes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Shapes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2008, 2013, 2014 Henrik Tidefelt
 */

#pragma once

#include "Shapes_Ast_decls.h"
#include "Shapes_Kernel_decls.h"

#include "ast.h"


namespace Shapes
{
	namespace Kernel
	{
		class Formals
		{
			Ast::SourceLocation loc_; /* Note: Not a const reference, as this is part of the AST. */
		public:
			bool seenDefault_;
			/* Function formals play two different roles: they allow named arguments in function application, and they define the identifiers to which
			 * arguments are bound for access from the function body.  In the first case, the formal is mapped to a simple identifier, while in the latter
			 * the formal is mapped to a placed identifier.  (Using placed identifiers for naming arguments in a function application would just be
			 * inconvenient.)  Therefore, we need two different maps that are just different ways of expressing the same order.
			 */
			Ast::IdentifierTree * argumentIdentifiers_;
			std::map< const char *, size_t, charPtrLess > * argumentOrder_;
			std::vector< Ast::Expression * > defaultExprs_; /* The sink does not have a default expression at all. */
			Ast::PlacedIdentifier * sink_; /* If null, there is no sink. */
			std::vector< bool > forcePos_;
			Ast::IdentifierTree * stateIdentifiers_;
			std::map< const char *, size_t, charPtrLess > * stateOrder_;
			Formals( );
			Formals( size_t numberOfDummyDefaultExprs );
			~Formals( );
			void setLoc( const Ast::SourceLocation & loc );
			bool appendArgumentFormal( const Ast::PlacedIdentifier & id );
			bool appendStateFormal( const Ast::PlacedIdentifier & id );

			void push_exprs( Ast::ArgListExprs * args ) const;
			Kernel::EvaluatedFormals * newEvaluatedFormals( Kernel::Arguments & args ) const;
			Kernel::EvaluatedFormals * newEvaluatedFormals( Kernel::Arguments & args, size_t * pos ) const; /* values are taken at *pos, and *pos is incremented accordingly */

			std::vector< bool > * newArgListForcePos( const Ast::ArgListExprs * argList ) const;
			std::vector< bool > * newArgListForcePos( const Ast::ArgListExprs * argList, const Kernel::Arguments & curryArgs ) const;

			bool hasSink( ) const { return sink_ != 0; }

			const Ast::SourceLocation & loc( ) const;
		};

		class CallContInfo
		{
			std::vector< bool > * forcePos_;
			bool forceAll_;										 /* if forcePos == 0, then either all or none are forced according to this variable */
		public:
			const Ast::ArgListExprs * argList_;
			Kernel::PassedEnv env_;
			Kernel::PassedDyn dyn_;
			Kernel::ContRef cont_;

			CallContInfo( const Ast::ArgListExprs * argList, const Kernel::EvalState & evalState, std::vector< bool > * forcePos );
			CallContInfo( const Ast::ArgListExprs * argList, const Kernel::EvalState & evalState, bool forceAll );
			~CallContInfo( );

			bool force( const size_t & pos ) const;
			bool isSelective( ) const;
			bool forceNone( ) const;
			bool forceAll( ) const;
			void gcMark( Kernel::GCMarkedSet & marked );
		};

	}

	namespace Ast
	{

		class ArgListExprs
		{
			bool exprOwner_;
			bool firstOrderedStateOwner_;
			Ast::Node * parent_;
		public:
			std::list< Ast::Expression * > * orderedExprs_;
			std::map< const char *, Ast::Expression *, charPtrLess > * namedExprs_;
			std::list< Ast::StateReference * > * orderedStates_;
			std::map< const char *, Ast::StateReference *, charPtrLess > * namedStates_;
			StateIDSet * freeStates_; /* Set of free states referenced by the expressions. */

			class ConstIterator
			{
			public:
				std::list< Ast::Expression * >::const_reverse_iterator i1_;
				std::map< const char *, Ast::Expression *, charPtrLess >::const_iterator i2_;
				size_t index_;

				ConstIterator( const ConstIterator & orig );
				ConstIterator( std::list< Ast::Expression * >::const_reverse_iterator i1, std::map< const char *, Ast::Expression *, charPtrLess >::const_iterator i2, const size_t & index );
			};

			ArgListExprs( bool exprOwner );
			ArgListExprs( bool exprOwner, bool firstOrderedStateOwner );
			ArgListExprs( std::list< Ast::Expression * > * orderedExprs, std::map< const char *, Ast::Expression *, charPtrLess > * namedExprs,
										std::list< Ast::StateReference * > * orderedStates, std::map< const char *, Ast::StateReference *, charPtrLess > * namedStates );
			ArgListExprs( size_t numberOfOrderedDummyExprs );
			~ArgListExprs( );
			void analyze( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst );

			ConstIterator begin( ) const;

			void evaluate( const RefCountPtr< const Kernel::CallContInfo > & info, const ArgListExprs::ConstIterator & pos, const RefCountPtr< const Lang::SingleList > & vals, Kernel::EvalState * evalState ) const;
			void evaluate_Structure( const RefCountPtr< const Kernel::CallContInfo > & info, size_t pos, const RefCountPtr< const Lang::SingleList > & values, Kernel::EvalState * evalState ) const;
			void bind( Kernel::Arguments * dst, RefCountPtr< const Lang::SingleList > vals, Kernel::PassedEnv env, Kernel::PassedDyn dyn ) const;

			Kernel::VariableHandle findNamed( RefCountPtr< const Lang::SingleList > vals, const char * name ) const;
			Kernel::VariableHandle getOrdered( RefCountPtr< const Lang::SingleList > vals, size_t pos ) const;
		};

		class FunctionFunction : public Lang::Function
		{
			Ast::SourceLocation loc_; /* Note: Not a const reference. */
			const Kernel::Formals * formals_;
			Ast::Expression * body_;
			Ast::FunctionMode functionMode_;
		public:
			FunctionFunction( const Ast::SourceLocation & loc, const Kernel::Formals * formals, Ast::Expression * body, const Ast::FunctionMode & functionMode );
			virtual ~FunctionFunction( );

			void push_exprs( Ast::ArgListExprs * args ) const;

			virtual void analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst );
			virtual void call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const;

			virtual void gcMark( Kernel::GCMarkedSet & marked ){ };
			virtual bool isTransforming( ) const { return false; }
		};

		class CallExpr : public Expression
		{
			bool curry_;
			RefCountPtr< const Lang::Function > constFun_;
			Ast::StateReference * mutatorSelf_;
		public:
			Ast::Expression * funExpr_;
			Ast::ArgListExprs * argList_;

			CallExpr( const Ast::SourceLocation & loc, Ast::Expression * funExpr, Ast::ArgListExprs * argList, bool curry = false );
			CallExpr( const Ast::SourceLocation & loc, const RefCountPtr< const Lang::Function > & constFun, Ast::ArgListExprs * argList, bool curry = false );
			virtual ~CallExpr( );
			void setMutatorSelf( Ast::StateReference * mutatorSelf );
			virtual void analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst );
			virtual void eval( Kernel::EvalState * evalState ) const;
		};

		class UnionExpr : public Expression
		{
		public:
			Ast::ArgListExprs * argList_;

			UnionExpr( const Ast::SourceLocation & loc, Ast::ArgListExprs * argList );
			virtual ~UnionExpr( );
			virtual void analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst );
			virtual void eval( Kernel::EvalState * evalState ) const;
		};

		class CallSplitExpr : public Expression
		{
			bool curry_;
			Ast::Expression * funExpr_;
			Ast::Expression * argList_;

		public:
			CallSplitExpr( const Ast::SourceLocation & loc, Ast::Expression * funExpr, Ast::Expression * argList, bool curry = false );
			virtual ~CallSplitExpr( );
			virtual void analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst );
			virtual void eval( Kernel::EvalState * evalState ) const;
		};

		class DummyExpression : public Expression
		{
		public:
			DummyExpression( );
			DummyExpression( const Ast::SourceLocation & loc );
			virtual ~DummyExpression( );
			virtual void analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst );
			virtual void eval( Kernel::EvalState * evalState ) const;
		};

	}
}
