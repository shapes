/* This file is part of Shapes.
 *
 * Shapes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Shapes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2008 Henrik Tidefelt
 */

#include "shapescore.h"
#include "globals.h"
#include "charconverters.h"
#include "autoonoff.h"
#include "ast.h"
#include "glyphlist.h"
#include "config.h"

using namespace Shapes;


namespace Shapes
{
	namespace Lang
	{
		class Core_makeglyph : public Lang::CoreFunction
		{
			Lang::Type3Glyph::Kind kind_;
		public:
			Core_makeglyph( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name, Lang::Type3Glyph::Kind kind )
				: CoreFunction( ns, name, new Kernel::EvaluatedFormals( Ast::FileID::build_internal( name ), true ) ), kind_( kind )
			{
				formals_->appendEvaluatedCoreFormal( "width", Kernel::THE_SLOT_VARIABLE );
				formals_->appendEvaluatedCoreFormal( "glyph", Kernel::THE_SLOT_VARIABLE );
				formals_->appendEvaluatedCoreFormal( "char", Kernel::THE_VOID_VARIABLE );
				formals_->appendEvaluatedCoreFormal( "name", Kernel::THE_VOID_VARIABLE );
			}
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				args.applyDefaults( callLoc );

				size_t argsi = 0;
				typedef const Lang::Length WidthType;
				RefCountPtr< WidthType > widthX = Helpers::down_cast_CoreArgument< WidthType >( id_, args, argsi, callLoc );

				++argsi;
				typedef const Lang::Drawable2D GlyphType;
				RefCountPtr< GlyphType > glyph = Helpers::down_cast_CoreArgument< GlyphType >( id_, args, argsi, callLoc );

				++argsi;
				typedef const Lang::Character CodeType;
				RefCountPtr< CodeType > codeArg = Helpers::down_cast_CoreArgument< CodeType >( id_, args, argsi, callLoc, true );

				++argsi;
				typedef const Lang::Symbol NameType;
				RefCountPtr< NameType > nameUTF8 = RefCountPtr< NameType >( NullPtr< NameType >( ) );

				RefCountPtr< const Lang::String > nameStringUTF8 = RefCountPtr< const Lang::String >( NullPtr< const Lang::String >( ) );
				if( codeArg != NullPtr< CodeType >( ) )
					{
						const size_t charBufSize = 10;
						char charBuf[ charBufSize ];
						char * dst = charBuf;
						size_t charBufAvail = charBufSize - 1;
						codeArg->val_.encode_UTF8( & dst, & charBufAvail );
						*dst = '\0';
						nameStringUTF8 = RefCountPtr< const Lang::String >( new Lang::String( strrefdup( charBuf ) ) );
					}
				{
					try
						{
							// If the argument is void, this goes through with nameUTF8 being null.	We will then refer to nameStringUTF8 as set above.
							nameUTF8 = Helpers::try_cast_CoreArgument< NameType >( args.getValue( argsi ), true );
							goto foundNameType;
						}
					catch( const NonLocalExit::NotThisType & ball )
						{
							/* Never mind, see below. */
						}

					try
						{
							// If we reach here, the value is present, but not a Symbol.	Hence we require that it be a string value for the name that replaces the default value being that of codeStringUTF8.
							nameStringUTF8 = Helpers::try_cast_CoreArgument< const Lang::String >( args.getValue( argsi ) );
							goto foundNameType;
						}
					catch( const NonLocalExit::NotThisType & ball )
						{
							/* Never mind, see below. */
						}

					throw Exceptions::CoreTypeMismatch( callLoc, id_, args, argsi, Helpers::typeSetString( Lang::Symbol::staticTypeName( ), Lang::String::staticTypeName( ) ) );
				}
			foundNameType:

				unsigned char code = 0;
				if( codeArg != NullPtr< CodeType >( ) )
					{
						code = codeArg->val_.get_MacRoman( );
					}

				RefCountPtr< const char > name = RefCountPtr< const char >( NullPtr< const char >( ) );
				if( nameUTF8 == NullPtr< NameType >( ) )
					{
						if( nameStringUTF8 == NullPtr< const Lang::String >( ) )
							{
								throw Exceptions::CoreRequirement( "At least one of <char> and <name> must be specified.", id_, callLoc );
							}
						const FontMetrics::GlyphList & glyphList = Helpers::requireGlyphList( );
						const char * dst;
						if( ! glyphList.UTF8_to_name( nameStringUTF8->val_.getPtr( ), & dst ) )
							{
								throw Exceptions::CoreOutOfRange( id_, args, 2, "When no name is given, characters without default names are not allowed.  Please refer to the glyph list." );
							}
						name = strrefdup( dst );
					}
				else
					{
						// Ensure that the glyph name is legal
						iconv_t converter = Helpers::requireUTF8ToASCIIConverter( );

						const char * inbuf = nameUTF8->name( ).getPtr( );

						size_t bufSize = strlen( inbuf );
						char * buf = new char[ bufSize + 1 ];
						name = RefCountPtr< const char >( buf );	// this will delete the buffer if it becomes unused.

						char * outbuf = buf;
						size_t inbytesleft = bufSize;
						size_t outbytesleft = bufSize;
						// The ICONV_CAST macro is defined in config.h.
						size_t count = iconv( converter,
																	ICONV_CAST( & inbuf ), & inbytesleft,
																	& outbuf, & outbytesleft );
						if( count == (size_t)(-1) )
							{
								if( errno == EILSEQ )
									{
										throw Exceptions::CoreOutOfRange( id_, args, 3, "A non-ASCII character was found in a glyph name." );
									}
								else if( errno == EINVAL )
									{
										throw Exceptions::MiscellaneousRequirement( "It is suspected that glyph's name ended with an incomplete multibyte character." );
									}
								else if( errno == E2BIG )
									{
										throw Exceptions::InternalError( "The buffer allocated for UTF-8 to ASCII conversion was too small." );
									}
								else
									{
										std::ostringstream msg;
										msg << "iconv failed with an unrecognized error code: " << errno ;
										throw Exceptions::InternalError( strrefdup( msg ) );
									}
							}
						*outbuf = '\0';
					}

				// Find boudning box:
				RefCountPtr< const Lang::ElementaryPath2D > theBBox = glyph->bbox( Lang::Drawable2D::BOUNDING );
				Concrete::Coords2D llcorner( 0, 0 );
				Concrete::Coords2D urcorner( 0, 0 );
				if( ! theBBox->boundingRectangle( & llcorner, & urcorner ) )
					{
						throw Exceptions::InternalErrorIn( id_, "The glyph has no bounding box." );
					}

				Kernel::ContRef cont = evalState->cont_;
				cont->takeValue( Kernel::ValueRef( new Lang::Type3Glyph( kind_,
																																 code,
																																 name,
																																 glyph,
																																 widthX->get( ),
																																 llcorner.x_,
																																 llcorner.y_,
																																 urcorner.x_,
																																 urcorner.y_ ) ),
												 evalState );
			}
		};

	}
}


void
Kernel::registerCore_font( Kernel::Environment * env )
{
  env->initDefineCoreFunction( new Lang::Core_makeglyph( Lang::THE_NAMESPACE_Shapes_Text, "basicglyph", Lang::Type3Glyph::BASIC ) );
  env->initDefineCoreFunction( new Lang::Core_makeglyph( Lang::THE_NAMESPACE_Shapes_Text, "coloredglyph", Lang::Type3Glyph::COLORED ) );

  env->initDefine( Lang::THE_NAMESPACE_Shapes_Text_Font, "TIMES_ROMAN", RefCountPtr< const Lang::Font >( new Lang::PDFStandardFont( BuiltInFonts::TIMES_ROMAN ) ) );
  env->initDefine( Lang::THE_NAMESPACE_Shapes_Text_Font, "TIMES_BOLD", RefCountPtr< const Lang::Font >( new Lang::PDFStandardFont( BuiltInFonts::TIMES_BOLD ) ) );
  env->initDefine( Lang::THE_NAMESPACE_Shapes_Text_Font, "TIMES_ITALIC", RefCountPtr< const Lang::Font >( new Lang::PDFStandardFont( BuiltInFonts::TIMES_ITALIC ) ) );
  env->initDefine( Lang::THE_NAMESPACE_Shapes_Text_Font, "TIMES_BOLDITALIC", RefCountPtr< const Lang::Font >( new Lang::PDFStandardFont( BuiltInFonts::TIMES_BOLDITALIC ) ) );
  env->initDefine( Lang::THE_NAMESPACE_Shapes_Text_Font, "HELVETICA", Lang::THE_FONT_HELVETICA );
  env->initDefine( Lang::THE_NAMESPACE_Shapes_Text_Font, "HELVETICA_BOLD", RefCountPtr< const Lang::Font >( new Lang::PDFStandardFont( BuiltInFonts::HELVETICA_BOLD ) ) );
  env->initDefine( Lang::THE_NAMESPACE_Shapes_Text_Font, "HELVETICA_OBLIQUE", RefCountPtr< const Lang::Font >( new Lang::PDFStandardFont( BuiltInFonts::HELVETICA_OBLIQUE ) ) );
  env->initDefine( Lang::THE_NAMESPACE_Shapes_Text_Font, "HELVETICA_BOLDOBLIQUE", RefCountPtr< const Lang::Font >( new Lang::PDFStandardFont( BuiltInFonts::HELVETICA_BOLDOBLIQUE ) ) );
  env->initDefine( Lang::THE_NAMESPACE_Shapes_Text_Font, "COURIER", RefCountPtr< const Lang::Font >( new Lang::PDFStandardFont( BuiltInFonts::COURIER ) ) );
  env->initDefine( Lang::THE_NAMESPACE_Shapes_Text_Font, "COURIER_BOLD", RefCountPtr< const Lang::Font >( new Lang::PDFStandardFont( BuiltInFonts::COURIER_BOLD ) ) );
  env->initDefine( Lang::THE_NAMESPACE_Shapes_Text_Font, "COURIER_OBLIQUE", RefCountPtr< const Lang::Font >( new Lang::PDFStandardFont( BuiltInFonts::COURIER_OBLIQUE ) ) );
  env->initDefine( Lang::THE_NAMESPACE_Shapes_Text_Font, "COURIER_BOLDOBLIQUE", RefCountPtr< const Lang::Font >( new Lang::PDFStandardFont( BuiltInFonts::COURIER_BOLDOBLIQUE ) ) );
  env->initDefine( Lang::THE_NAMESPACE_Shapes_Text_Font, "SYMBOL", RefCountPtr< const Lang::Font >( new Lang::PDFStandardFont( BuiltInFonts::SYMBOL ) ) );
  env->initDefine( Lang::THE_NAMESPACE_Shapes_Text_Font, "ZAPFDINGBATS", RefCountPtr< const Lang::Font >( new Lang::PDFStandardFont( BuiltInFonts::ZAPFDINGBATS ) ) );
}
