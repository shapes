/* This file is part of Shapes.
 *
 * Shapes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Shapes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2008, 2010, 2013 Henrik Tidefelt
 */

#include "shapestypes.h"
#include "shapesexceptions.h"
#include "astexpr.h"
#include "continuations.h"
#include "consts.h"
#include "angleselect.h"
#include "astvar.h"
#include "astclass.h"
#include "globals.h"
#include "methodbase.h"
#include "functiontypes.h" /* So that we can set up methods for VectorFunction, which is a kind of container too. */

using namespace Shapes;


namespace Shapes
{
	namespace Lang
	{

		template< class T >
		class Method_foldl : public Lang::MethodBase< T >
		{
		public:
			Method_foldl( RefCountPtr< const T > _self, const Ast::FileID * fullMethodID );
			virtual ~Method_foldl( );
			virtual void call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const;
			static const char * staticFieldID( ) { return "foldl"; }
		};

		template< class T >
		class Method_foldr : public Lang::MethodBase< T >
		{
		public:
			Method_foldr( RefCountPtr< const T > _self, const Ast::FileID * fullMethodID );
			virtual ~Method_foldr( );
			virtual void call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const;
			static const char * staticFieldID( ) { return "foldr"; }
		};

		template< class T >
		class Method_foldsl : public Lang::MethodBase< T >
		{
		public:
			Method_foldsl( RefCountPtr< const T > _self, const Ast::FileID * fullMethodID );
			virtual ~Method_foldsl( );
			virtual void call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const;
			static const char * staticFieldID( ) { return "foldsl"; }
		};

		template< class T >
		class Method_foldsr : public Lang::MethodBase< T >
		{
		public:
			Method_foldsr( RefCountPtr< const T > _self, const Ast::FileID * fullMethodID );
			virtual ~Method_foldsr( );
			virtual void call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const;
			static const char * staticFieldID( ) { return "foldsr"; }
		};

	}

	namespace Kernel
	{

		class ForceFunctionAndCall_2_args_1_state_cont : public Kernel::Continuation
		{
			Kernel::VariableHandle arg1_;
			Kernel::VariableHandle arg2_;
			Kernel::StateHandle state_;
			Kernel::PassedDyn dyn_;
			Kernel::ContRef cont_;
		public:
			ForceFunctionAndCall_2_args_1_state_cont( const Kernel::VariableHandle & arg1, const Kernel::VariableHandle & arg2, Kernel::StateHandle state, const Kernel::PassedDyn & dyn, const Kernel::ContRef & cont, const Ast::SourceLocation & callLoc );
			virtual ~ForceFunctionAndCall_2_args_1_state_cont( );
			virtual void takeValue( const RefCountPtr< const Lang::Value > & val, Kernel::EvalState * evalState, bool dummy ) const;
			virtual Kernel::ContRef up( ) const;
			virtual RefCountPtr< const char > description( ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
		};

	}
}


Lang::SingleList::SingleList( )
{ }

Lang::SingleList::~SingleList( )
{ }

void
SingleList_register_methods( Lang::SystemFinalClass * dstClass )
{
	dstClass->registerMethod( new Kernel::MethodFactory< Lang::SingleList, Lang::Method_foldl< Lang::SingleList > >( ) );
	dstClass->registerMethod( new Kernel::MethodFactory< Lang::SingleList, Lang::Method_foldr< Lang::SingleList > >( ) );
	dstClass->registerMethod( new Kernel::MethodFactory< Lang::SingleList, Lang::Method_foldsl< Lang::SingleList > >( ) );
	dstClass->registerMethod( new Kernel::MethodFactory< Lang::SingleList, Lang::Method_foldsr< Lang::SingleList > >( ) );
}

RefCountPtr< const Lang::Class > Lang::SingleList::TypeID( new Lang::SystemFinalClass( strrefdup( "SeqPair" ), SingleList_register_methods ) );
TYPEINFOIMPL( SingleList );

Kernel::VariableHandle
Lang::SingleList::getField( const char * fieldId, const RefCountPtr< const Lang::Value > & selfRef ) const
{
	return TypeID->getMethod( selfRef, fieldId ); /* This will throw if there is no such method. */
}


Kernel::SingleFoldLCont::SingleFoldLCont( const RefCountPtr< const Lang::SingleList > & cdr, const RefCountPtr< const Lang::Function > & op, const Kernel::PassedDyn & dyn, Kernel::ContRef cont, const Ast::SourceLocation & traceLoc )
	: Kernel::Continuation( traceLoc ), cdr_( cdr ), op_( op ), dyn_( dyn ), cont_( cont )
{ }

Kernel::SingleFoldLCont::~SingleFoldLCont( )
{ }

void
Kernel::SingleFoldLCont::takeHandle( Kernel::VariableHandle val, Kernel::EvalState * evalState, bool dummy ) const
{
	evalState->dyn_ = dyn_;
	evalState->cont_ = cont_;
	cdr_->foldl( evalState, op_, val, traceLoc_ );
}

Kernel::ContRef
Kernel::SingleFoldLCont::up( ) const
{
	return cont_;
}

RefCountPtr< const char >
Kernel::SingleFoldLCont::description( ) const
{
	return strrefdup( "singly linked list's foldl" );
}

void
Kernel::SingleFoldLCont::gcMark( Kernel::GCMarkedSet & marked )
{
	const_cast< Lang::SingleList * >( cdr_.getPtr( ) )->gcMark( marked );
	const_cast< Lang::Function * >( op_.getPtr( ) )->gcMark( marked );
	dyn_->gcMark( marked );
	cont_->gcMark( marked );
}


Kernel::SingleFoldRCont::SingleFoldRCont( const Kernel::VariableHandle & car, const RefCountPtr< const Lang::Function > & op, const Kernel::PassedDyn & dyn, Kernel::ContRef cont, const Ast::SourceLocation & traceLoc )
	: Kernel::Continuation( traceLoc ), car_( car ), op_( op ), dyn_( dyn ), cont_( cont )
{ }

Kernel::SingleFoldRCont::~SingleFoldRCont( )
{ }

void
Kernel::SingleFoldRCont::takeHandle( Kernel::VariableHandle val, Kernel::EvalState * evalState, bool dummy ) const
{
	evalState->dyn_ = dyn_;
	evalState->cont_ = cont_;
	op_->call( op_, evalState, car_, val, traceLoc_ );
}

Kernel::ContRef
Kernel::SingleFoldRCont::up( ) const
{
	return cont_;
}

RefCountPtr< const char >
Kernel::SingleFoldRCont::description( ) const
{
	return strrefdup( "foldr" );
}

void
Kernel::SingleFoldRCont::gcMark( Kernel::GCMarkedSet & marked )
{
	car_->gcMark( marked );
	const_cast< Lang::Function * >( op_.getPtr( ) )->gcMark( marked );
	dyn_->gcMark( marked );
	cont_->gcMark( marked );
}


Kernel::SingleFoldSLCont::SingleFoldSLCont( const RefCountPtr< const Lang::SingleList > & cdr, const RefCountPtr< const Lang::Function > & op, Kernel::StateHandle state, const Kernel::PassedDyn & dyn, Kernel::ContRef cont, const Ast::SourceLocation & traceLoc )
	: Kernel::Continuation( traceLoc ), cdr_( cdr ), op_( op ), state_( state ), dyn_( dyn ), cont_( cont )
{ }

Kernel::SingleFoldSLCont::~SingleFoldSLCont( )
{ }

void
Kernel::SingleFoldSLCont::takeHandle( Kernel::VariableHandle val, Kernel::EvalState * evalState, bool dummy ) const
{
	evalState->dyn_ = dyn_;
	evalState->cont_ = cont_;
	cdr_->foldsl( evalState, op_, val, state_, traceLoc_ );
}

Kernel::ContRef
Kernel::SingleFoldSLCont::up( ) const
{
	return cont_;
}

RefCountPtr< const char >
Kernel::SingleFoldSLCont::description( ) const
{
	return strrefdup( "singly linked list's foldsl" );
}

void
Kernel::SingleFoldSLCont::gcMark( Kernel::GCMarkedSet & marked )
{
	const_cast< Lang::SingleList * >( cdr_.getPtr( ) )->gcMark( marked );
	const_cast< Lang::Function * >( op_.getPtr( ) )->gcMark( marked );
	state_->gcMark( marked );
	dyn_->gcMark( marked );
	cont_->gcMark( marked );
}


Kernel::SingleFoldSRCont::SingleFoldSRCont( const Kernel::VariableHandle & car, const RefCountPtr< const Lang::Function > & op, Kernel::StateHandle state, const Kernel::PassedDyn & dyn, Kernel::ContRef cont, const Ast::SourceLocation & traceLoc )
	: Kernel::Continuation( traceLoc ), car_( car ), op_( op ), state_( state ),dyn_( dyn ), cont_( cont )
{ }

Kernel::SingleFoldSRCont::~SingleFoldSRCont( )
{ }

void
Kernel::SingleFoldSRCont::takeHandle( Kernel::VariableHandle val, Kernel::EvalState * evalState, bool dummy ) const
{
	evalState->dyn_ = dyn_;
	evalState->cont_ = cont_;
	op_->call( op_, evalState, car_, val, state_, traceLoc_ );
}

Kernel::ContRef
Kernel::SingleFoldSRCont::up( ) const
{
	return cont_;
}

RefCountPtr< const char >
Kernel::SingleFoldSRCont::description( ) const
{
	return strrefdup( "foldsr" );
}

void
Kernel::SingleFoldSRCont::gcMark( Kernel::GCMarkedSet & marked )
{
	car_->gcMark( marked );
	const_cast< Lang::Function * >( op_.getPtr( ) )->gcMark( marked );
	state_->gcMark( marked );
	dyn_->gcMark( marked );
	cont_->gcMark( marked );
}


namespace Shapes
{
	namespace Kernel
	{

	class ConsPairFoldLCont_cdr : public Kernel::Continuation
	{
		Kernel::VariableHandle op_;
		Kernel::VariableHandle nullResult_;
		Kernel::PassedDyn dyn_;
		Kernel::ContRef cont_;
	public:
		ConsPairFoldLCont_cdr( const Kernel::VariableHandle & op, const Kernel::VariableHandle & nullResult, const Kernel::PassedDyn & dyn, Kernel::ContRef cont, const Ast::SourceLocation & traceLoc )
			: Kernel::Continuation( traceLoc ), op_( op ), nullResult_( nullResult ), dyn_( dyn ), cont_( cont )
		{ }
		virtual ~ConsPairFoldLCont_cdr( ) { }
		virtual void takeValue( const RefCountPtr< const Lang::Value > & val, Kernel::EvalState * evalState, bool dummy ) const
		{
			evalState->dyn_ = dyn_;
			evalState->cont_ = Kernel::ContRef( new Kernel::ForceFunctionAndCall_2_args_cont( op_, nullResult_, dyn_, cont_, traceLoc_ ) );
			Kernel::VariableHandle fun = val->getField( "foldl", val );
			fun->force( fun, evalState );
		}
		virtual Kernel::ContRef up( ) const
		{
			return cont_;
		}
		virtual RefCountPtr< const char > description( ) const
		{
			return strrefdup( "cons pair's foldl (force cdr)" );
		}
		virtual void gcMark( Kernel::GCMarkedSet & marked )
		{
			op_->gcMark( marked );
			nullResult_->gcMark( marked );
			dyn_->gcMark( marked );
			cont_->gcMark( marked );
		}
	};

	class ConsPairFoldLCont_op : public Kernel::Continuation
	{
		Kernel::VariableHandle cdr_;
		Kernel::VariableHandle op_;
		Kernel::PassedDyn dyn_;
		Kernel::ContRef cont_;
	public:
		ConsPairFoldLCont_op( const Kernel::VariableHandle & cdr, const Kernel::VariableHandle & op, const Kernel::PassedDyn & dyn, Kernel::ContRef cont, const Ast::SourceLocation & traceLoc )
			: Kernel::Continuation( traceLoc ), cdr_( cdr ), op_( op ), dyn_( dyn ), cont_( cont )
		{ }
		virtual ~ConsPairFoldLCont_op( ) { }
		virtual void takeHandle( Kernel::VariableHandle val, Kernel::EvalState * evalState, bool dummy ) const
		{
			evalState->dyn_ = dyn_; /* Necessary?! */
			evalState->cont_ = Kernel::ContRef( new Kernel::ConsPairFoldLCont_cdr( op_, val, dyn_, cont_, traceLoc_ ) );
			cdr_->force( cdr_, evalState );
		}
		virtual Kernel::ContRef up( ) const
		{
			return cont_;
		}
		virtual RefCountPtr< const char > description( ) const
		{
			return strrefdup( "cons pair's foldl (op)" );
		}
		virtual void gcMark( Kernel::GCMarkedSet & marked )
		{
			cdr_->gcMark( marked );
			op_->gcMark( marked );
			dyn_->gcMark( marked );
			cont_->gcMark( marked );
		}
	};

	class ConsPairFoldRCont_cdr : public Kernel::Continuation
	{
		Kernel::VariableHandle car_;
		RefCountPtr< const Lang::Function > op_;
		Kernel::VariableHandle opHandle_;
		Kernel::VariableHandle nullResult_;
		Kernel::PassedDyn dyn_;
		Kernel::ContRef cont_;
	public:
		ConsPairFoldRCont_cdr( const Kernel::VariableHandle & car, const RefCountPtr< const Lang::Function > & op, const Kernel::VariableHandle & opHandle, const Kernel::VariableHandle & nullResult, const Kernel::PassedDyn & dyn, Kernel::ContRef cont, const Ast::SourceLocation & traceLoc )
			: Kernel::Continuation( traceLoc ), car_( car ), op_( op ), opHandle_( opHandle ), nullResult_( nullResult ), dyn_( dyn ), cont_( cont )
		{ }
		virtual ~ConsPairFoldRCont_cdr( ) { }
		virtual void takeValue( const RefCountPtr< const Lang::Value > & val, Kernel::EvalState * evalState, bool dummy ) const
		{
			evalState->dyn_ = dyn_;
			evalState->cont_ = Kernel::ContRef( new Kernel::ForceFunctionAndCall_2_args_cont( opHandle_, nullResult_, dyn_,
																																												Kernel::ContRef( new Kernel::SingleFoldRCont( car_, op_, evalState->dyn_, cont_, traceLoc_ ) ),
																																												traceLoc_ ) );
			Kernel::VariableHandle fun = val->getField( "foldr", val );
			fun->force( fun, evalState );
		}
		virtual Kernel::ContRef up( ) const
		{
			return cont_;
		}
		virtual RefCountPtr< const char > description( ) const
		{
			return strrefdup( "cons pair's foldl (force cdr)" );
		}
		virtual void gcMark( Kernel::GCMarkedSet & marked )
		{
			car_->gcMark( marked );
			opHandle_->gcMark( marked ); /* This will gcMark op_ as well! */
			nullResult_->gcMark( marked );
			dyn_->gcMark( marked );
			cont_->gcMark( marked );
		}
	};

	class ConsPairFoldSLCont_cdr : public Kernel::Continuation
	{
		Kernel::VariableHandle op_;
		Kernel::VariableHandle nullResult_;
		Kernel::StateHandle state_;
		Kernel::PassedDyn dyn_;
		Kernel::ContRef cont_;
	public:
		ConsPairFoldSLCont_cdr( const Kernel::VariableHandle & op, const Kernel::VariableHandle & nullResult, Kernel::StateHandle state, const Kernel::PassedDyn & dyn, Kernel::ContRef cont, const Ast::SourceLocation & traceLoc )
			: Kernel::Continuation( traceLoc ), op_( op ), nullResult_( nullResult ), state_( state ), dyn_( dyn ), cont_( cont )
		{ }
		virtual ~ConsPairFoldSLCont_cdr( ) { }
		virtual void takeValue( const RefCountPtr< const Lang::Value > & val, Kernel::EvalState * evalState, bool dummy ) const
		{
			evalState->dyn_ = dyn_;
			evalState->cont_ = Kernel::ContRef( new Kernel::ForceFunctionAndCall_2_args_1_state_cont( op_, nullResult_, state_, dyn_, cont_, traceLoc_ ) );
			Kernel::VariableHandle fun = val->getField( "foldsl", val );
			fun->force( fun, evalState );
		}
		virtual Kernel::ContRef up( ) const
		{
			return cont_;
		}
		virtual RefCountPtr< const char > description( ) const
		{
			return strrefdup( "cons pair's foldl (force cdr)" );
		}
		virtual void gcMark( Kernel::GCMarkedSet & marked )
		{
			op_->gcMark( marked );
			nullResult_->gcMark( marked );
			state_->gcMark( marked );
			dyn_->gcMark( marked );
			cont_->gcMark( marked );
		}
	};

	class ConsPairFoldSLCont_op : public Kernel::Continuation
	{
		Kernel::VariableHandle cdr_;
		Kernel::VariableHandle op_;
		Kernel::StateHandle state_;
		Kernel::PassedDyn dyn_;
		Kernel::ContRef cont_;
	public:
		ConsPairFoldSLCont_op( const Kernel::VariableHandle & cdr, const Kernel::VariableHandle & op, Kernel::StateHandle state, const Kernel::PassedDyn & dyn, Kernel::ContRef cont, const Ast::SourceLocation & traceLoc )
			: Kernel::Continuation( traceLoc ), cdr_( cdr ), op_( op ), state_( state ), dyn_( dyn ), cont_( cont )
		{ }
		virtual ~ConsPairFoldSLCont_op( ) { }
		virtual void takeHandle( Kernel::VariableHandle val, Kernel::EvalState * evalState, bool dummy ) const
		{
			evalState->dyn_ = dyn_; /* Necessary?! */
			evalState->cont_ = Kernel::ContRef( new Kernel::ConsPairFoldSLCont_cdr( op_, val, state_, dyn_, cont_, traceLoc_ ) );
			cdr_->force( cdr_, evalState );
		}
		virtual Kernel::ContRef up( ) const
		{
			return cont_;
		}
		virtual RefCountPtr< const char > description( ) const
		{
			return strrefdup( "cons pair's foldl (op)" );
		}
		virtual void gcMark( Kernel::GCMarkedSet & marked )
		{
			cdr_->gcMark( marked );
			op_->gcMark( marked );
			state_->gcMark( marked );
			dyn_->gcMark( marked );
			cont_->gcMark( marked );
		}
	};

	class ConsPairFoldSRCont_cdr : public Kernel::Continuation
	{
		Kernel::VariableHandle car_;
		RefCountPtr< const Lang::Function > op_;
		Kernel::VariableHandle opHandle_;
		Kernel::VariableHandle nullResult_;
		Kernel::StateHandle state_;
		Kernel::PassedDyn dyn_;
		Kernel::ContRef cont_;
	public:
		ConsPairFoldSRCont_cdr( const Kernel::VariableHandle & car, const RefCountPtr< const Lang::Function > & op, const Kernel::VariableHandle & opHandle, const Kernel::VariableHandle & nullResult, Kernel::StateHandle state, const Kernel::PassedDyn & dyn, Kernel::ContRef cont, const Ast::SourceLocation & traceLoc )
			: Kernel::Continuation( traceLoc ), car_( car ), op_( op ), opHandle_( opHandle ), nullResult_( nullResult ), state_( state ), dyn_( dyn ), cont_( cont )
		{ }
		virtual ~ConsPairFoldSRCont_cdr( ) { }
		virtual void takeValue( const RefCountPtr< const Lang::Value > & val, Kernel::EvalState * evalState, bool dummy ) const
		{
			evalState->dyn_ = dyn_;
			evalState->cont_ = Kernel::ContRef( new Kernel::ForceFunctionAndCall_2_args_1_state_cont( opHandle_, nullResult_, state_, dyn_,
																																																Kernel::ContRef( new Kernel::SingleFoldSRCont( car_, op_, state_, evalState->dyn_, cont_, traceLoc_ ) ),
																																																traceLoc_ ) );
			Kernel::VariableHandle fun = val->getField( "foldsr", val );
			fun->force( fun, evalState );
		}
		virtual Kernel::ContRef up( ) const
		{
			return cont_;
		}
		virtual RefCountPtr< const char > description( ) const
		{
			return strrefdup( "cons pair's foldl (force cdr)" );
		}
		virtual void gcMark( Kernel::GCMarkedSet & marked )
		{
			car_->gcMark( marked );
			opHandle_->gcMark( marked ); /* This will gcMark op_ as well! */
			nullResult_->gcMark( marked );
			state_->gcMark( marked );
			dyn_->gcMark( marked );
			cont_->gcMark( marked );
		}
	};

	}
}


Lang::SingleListPair::SingleListPair( const Kernel::VariableHandle & car, const RefCountPtr< const Lang::SingleList > & cdr )
	: car_( car ), cdr_( cdr ), forced_( cdr_->isForced( ) && ! car->isThunk( ) )
{ }

Lang::SingleListPair::~SingleListPair( )
{ }

void
Lang::SingleListPair::show( std::ostream & os ) const
{
	os << "< non-empty singly linked list >" ;
}

bool
Lang::SingleListPair::isNull( ) const
{
	return false;
}

bool
Lang::SingleListPair::isForced( ) const
{
	return forced_;
}

void
Lang::SingleListPair::foldl( Kernel::EvalState * evalState, const RefCountPtr< const Lang::Function > & op, const Kernel::VariableHandle & nullResult, const Ast::SourceLocation & callLoc ) const
{
	evalState->cont_ = Kernel::ContRef( new Kernel::SingleFoldLCont( cdr_, op, evalState->dyn_, evalState->cont_, callLoc ) );

	op->call( op, evalState, nullResult, car_, callLoc );
}

void
Lang::SingleListPair::foldr( Kernel::EvalState * evalState, const RefCountPtr< const Lang::Function > & op, const Kernel::VariableHandle & nullResult, const Ast::SourceLocation & callLoc ) const
{
	evalState->cont_ = Kernel::ContRef( new Kernel::SingleFoldRCont( car_, op, evalState->dyn_, evalState->cont_, callLoc ) );

	cdr_->foldr( evalState, op, nullResult, callLoc );
}

void
Lang::SingleListPair::foldsl( Kernel::EvalState * evalState, const RefCountPtr< const Lang::Function > & op, const Kernel::VariableHandle & nullResult, Kernel::StateHandle state, const Ast::SourceLocation & callLoc ) const
{
	evalState->cont_ = Kernel::ContRef( new Kernel::SingleFoldSLCont( cdr_, op, state, evalState->dyn_, evalState->cont_, callLoc ) );

	op->call( op, evalState, nullResult, car_, state, callLoc );
}

void
Lang::SingleListPair::foldsr( Kernel::EvalState * evalState, const RefCountPtr< const Lang::Function > & op, const Kernel::VariableHandle & nullResult, Kernel::StateHandle state, const Ast::SourceLocation & callLoc ) const
{
	evalState->cont_ = Kernel::ContRef( new Kernel::SingleFoldSRCont( car_, op, state, evalState->dyn_, evalState->cont_, callLoc ) );

	cdr_->foldsr( evalState, op, nullResult, state, callLoc );
}

void
Lang::SingleListPair::gcMark( Kernel::GCMarkedSet & marked )
{
	car_->gcMark( marked );
	const_cast< Lang::SingleList * >( cdr_.getPtr( ) )->gcMark( marked );
}

Kernel::VariableHandle
Lang::SingleListPair::getField( const char * fieldId, const RefCountPtr< const Lang::Value > & selfRef ) const
{
	if( strcmp( fieldId, "car" ) == 0 )
		{
			return car_;
		}
	if( strcmp( fieldId, "cdr" ) == 0 )
		{
			return Kernel::VariableHandle( new Kernel::Variable( cdr_ ) );
		}

	return SingleList::getField( fieldId, selfRef ); /* This will throw if there is no such method. */
}

Lang::SingleListNull::SingleListNull( )
{ }

Lang::SingleListNull::~SingleListNull( )
{ }

void
Lang::SingleListNull::show( std::ostream & os ) const
{
	os << "< empty singly linked list >" ;
}

bool
Lang::SingleListNull::isNull( ) const
{
	return true;
}

bool
Lang::SingleListNull::isForced( ) const
{
	return true;
}

RefCountPtr< const Lang::Class > Lang::SingleListNull::TypeID( new Lang::SystemFinalClass( strrefdup( "SeqNil" ), SingleList_register_methods ) );
TYPEINFOIMPL( SingleListNull );

Kernel::VariableHandle
Lang::SingleListNull::getField( const char * fieldId, const RefCountPtr< const Lang::Value > & selfRef ) const
{
	return TypeID->getMethod( selfRef, fieldId ); /* This will throw if there is no such method. */
}

void
Lang::SingleListNull::foldl( Kernel::EvalState * evalState, const RefCountPtr< const Lang::Function > & op, const Kernel::VariableHandle & nullResult, const Ast::SourceLocation & callLoc ) const
{
	Kernel::ContRef cont = evalState->cont_;
	cont->takeHandle( nullResult,
										evalState );
}

void
Lang::SingleListNull::foldr( Kernel::EvalState * evalState, const RefCountPtr< const Lang::Function > & op, const Kernel::VariableHandle & nullResult, const Ast::SourceLocation & callLoc ) const
{
	Kernel::ContRef cont = evalState->cont_;
	cont->takeHandle( nullResult,
										evalState );
}

void
Lang::SingleListNull::foldsl( Kernel::EvalState * evalState, const RefCountPtr< const Lang::Function > & op, const Kernel::VariableHandle & nullResult, Kernel::StateHandle state, const Ast::SourceLocation & callLoc ) const
{
	Kernel::ContRef cont = evalState->cont_;
	cont->takeHandle( nullResult,
										evalState );
}

void
Lang::SingleListNull::foldsr( Kernel::EvalState * evalState, const RefCountPtr< const Lang::Function > & op, const Kernel::VariableHandle & nullResult, Kernel::StateHandle state, const Ast::SourceLocation & callLoc ) const
{
	Kernel::ContRef cont = evalState->cont_;
	cont->takeHandle( nullResult,
										evalState );
}


Lang::ConsPair::ConsPair( const Kernel::VariableHandle & car, const Kernel::VariableHandle & cdr )
	: car_( car ), cdr_( cdr )
{ }

Lang::ConsPair::~ConsPair( )
{ }


void
ConsPair_register_methods( Lang::SystemFinalClass * dstClass )
{
	dstClass->registerMethod( new Kernel::MethodFactory< Lang::ConsPair, Lang::Method_foldl< Lang::ConsPair > >( ) );
	dstClass->registerMethod( new Kernel::MethodFactory< Lang::ConsPair, Lang::Method_foldr< Lang::ConsPair > >( ) );
	dstClass->registerMethod( new Kernel::MethodFactory< Lang::ConsPair, Lang::Method_foldsl< Lang::ConsPair > >( ) );
	dstClass->registerMethod( new Kernel::MethodFactory< Lang::ConsPair, Lang::Method_foldsr< Lang::ConsPair > >( ) );
}

RefCountPtr< const Lang::Class > Lang::ConsPair::TypeID( new Lang::SystemFinalClass( strrefdup( "ConsPair" ), ConsPair_register_methods ) );
TYPEINFOIMPL( ConsPair );

void
Lang::ConsPair::show( std::ostream & os ) const
{
	os << "< a lazy pair >" ;
}

Kernel::VariableHandle
Lang::ConsPair::getField( const char * fieldID, const RefCountPtr< const Lang::Value > & selfRef ) const
{
	if( strcmp( fieldID, "car" ) == 0 )
		{
			return car_;
		}
	if( strcmp( fieldID, "cdr" ) == 0 )
		{
			return cdr_;
		}

	return TypeID->getMethod( selfRef, fieldID ); /* This will throw if there is no such method. */
}

void
Lang::ConsPair::gcMark( Kernel::GCMarkedSet & marked )
{
	car_->gcMark( marked );
	cdr_->gcMark( marked );
}

void
Lang::ConsPair::foldl( Kernel::EvalState * evalState, const RefCountPtr< const Lang::Function > & op, const Kernel::VariableHandle & opHandle, const Kernel::VariableHandle & nullResult, const Ast::SourceLocation & callLoc ) const
{
	evalState->cont_ = Kernel::ContRef( new Kernel::ConsPairFoldLCont_op( cdr_, opHandle, evalState->dyn_, evalState->cont_, callLoc ) );

	op->call( op, evalState, nullResult, car_, callLoc );
}

void
Lang::ConsPair::foldr( Kernel::EvalState * evalState, const RefCountPtr< const Lang::Function > & op, const Kernel::VariableHandle & opHandle, const Kernel::VariableHandle & nullResult, const Ast::SourceLocation & callLoc ) const
{
	evalState->cont_ = Kernel::ContRef( new Kernel::ConsPairFoldRCont_cdr( car_, op, opHandle, nullResult, evalState->dyn_, evalState->cont_, callLoc ) );

	cdr_->force( cdr_, evalState );
}

void
Lang::ConsPair::foldsl( Kernel::EvalState * evalState, const RefCountPtr< const Lang::Function > & op, const Kernel::VariableHandle & opHandle, const Kernel::VariableHandle & nullResult, Kernel::StateHandle state, const Ast::SourceLocation & callLoc ) const
{
	evalState->cont_ = Kernel::ContRef( new Kernel::ConsPairFoldSLCont_op( cdr_, opHandle, state, evalState->dyn_, evalState->cont_, callLoc ) );

	op->call( op, evalState, nullResult, car_, state, callLoc );
}

void
Lang::ConsPair::foldsr( Kernel::EvalState * evalState, const RefCountPtr< const Lang::Function > & op, const Kernel::VariableHandle & opHandle, const Kernel::VariableHandle & nullResult, Kernel::StateHandle state, const Ast::SourceLocation & callLoc ) const
{
	evalState->cont_ = Kernel::ContRef( new Kernel::ConsPairFoldSRCont_cdr( car_, op, opHandle, nullResult, state, evalState->dyn_, evalState->cont_, callLoc ) );

	cdr_->force( cdr_, evalState );
}


template< class T >
Lang::Method_foldl< T >::Method_foldl( RefCountPtr< const T > self, const Ast::FileID * fullMethodID )
	: Lang::MethodBase< T >( self, fullMethodID, false )
{
	Lang::MethodBase< T >::formals_->appendEvaluatedCoreFormal( "op", Kernel::THE_SLOT_VARIABLE, true );
	Lang::MethodBase< T >::formals_->appendEvaluatedCoreFormal( "zero", Kernel::THE_SLOT_VARIABLE, false );
}

template< class T >
Lang::Method_foldl< T >::~Method_foldl( )
{ }

template< class T >
void
Lang::Method_foldl< T >::call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
{
	args.applyDefaults( callLoc );

	Lang::MethodBase< T >::self_->foldl( evalState,
																			 Helpers::down_cast_CoreArgument< const Lang::Function >( Lang::MethodBase< T >::coreLoc_, args, 0, callLoc ),
																			 args.getHandle( 0 ),
																			 args.getHandle( 1 ),
																			 callLoc );
}


template< class T >
Lang::Method_foldr< T >::Method_foldr( RefCountPtr< const T > self, const Ast::FileID * fullMethodID )
	: Lang::MethodBase< T >( self, fullMethodID, false )
{
	Lang::MethodBase< T >::formals_->appendEvaluatedCoreFormal( "op", Kernel::THE_SLOT_VARIABLE, true );
	Lang::MethodBase< T >::formals_->appendEvaluatedCoreFormal( "zero", Kernel::THE_SLOT_VARIABLE, false );
}

template< class T >
Lang::Method_foldr< T >::~Method_foldr( )
{ }

template< class T >
void
Lang::Method_foldr< T >::call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
{
	args.applyDefaults( callLoc );

	Lang::MethodBase< T >::self_->foldr( evalState,
																			 Helpers::down_cast_CoreArgument< const Lang::Function >( Lang::MethodBase< T >::coreLoc_, args, 0, callLoc ),
																			 args.getHandle( 0 ),
																			 args.getHandle( 1 ),
																			 callLoc );
}


template< class T >
Lang::Method_foldsl< T >::Method_foldsl( RefCountPtr< const T > self, const Ast::FileID * fullMethodID )
	: Lang::MethodBase< T >( self, fullMethodID, false )
{
	Lang::MethodBase< T >::formals_->appendEvaluatedCoreFormal( "op", Kernel::THE_SLOT_VARIABLE, true );
	Lang::MethodBase< T >::formals_->appendEvaluatedCoreFormal( "zero", Kernel::THE_SLOT_VARIABLE, false );
	Lang::MethodBase< T >::formals_->appendCoreStateFormal( "state" );
}

template< class T >
Lang::Method_foldsl< T >::~Method_foldsl( )
{ }

template< class T >
void
Lang::Method_foldsl< T >::call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
{
	args.applyDefaults( callLoc );

	Lang::MethodBase< T >::self_->foldsl( evalState,
																				Helpers::down_cast_CoreArgument< const Lang::Function >( Lang::MethodBase< T >::coreLoc_, args, 0, callLoc ),
																				args.getHandle( 0 ),
																				args.getHandle( 1 ),
																				args.getState( 0 ),
																				callLoc );
}


template< class T >
Lang::Method_foldsr< T >::Method_foldsr( RefCountPtr< const T > self, const Ast::FileID * fullMethodID )
	: Lang::MethodBase< T >( self, fullMethodID, false )
{
	Lang::MethodBase< T >::formals_->appendEvaluatedCoreFormal( "op", Kernel::THE_SLOT_VARIABLE, true );
	Lang::MethodBase< T >::formals_->appendEvaluatedCoreFormal( "zero", Kernel::THE_SLOT_VARIABLE, false );
	Lang::MethodBase< T >::formals_->appendCoreStateFormal( "state" );
}

template< class T >
Lang::Method_foldsr< T >::~Method_foldsr( )
{ }

template< class T >
void
Lang::Method_foldsr< T >::call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
{
	args.applyDefaults( callLoc );

	Lang::MethodBase< T >::self_->foldsr( evalState,
																				Helpers::down_cast_CoreArgument< const Lang::Function >( Lang::MethodBase< T >::coreLoc_, args, 0, callLoc ),
																				args.getHandle( 0 ),
																				args.getHandle( 1 ),
																				args.getState( 0 ),
																				callLoc );
}


Lang::Structure::Structure( const Ast::ArgListExprs * argList, const RefCountPtr< const Lang::SingleList > & values, bool argListOwner )
	: argListOwner_( argListOwner ), argList_( argList ), values_( values )
{ }

Lang::Structure::~Structure( )
{
	if( argListOwner_ )
		{
			delete argList_;
		}
}

RefCountPtr< const Lang::Class > Lang::Structure::TypeID( new Lang::SystemFinalClass( strrefdup( "Union" ) ) );
TYPEINFOIMPL( Structure );

Kernel::VariableHandle
Lang::Structure::getField( const char * fieldID, const RefCountPtr< const Lang::Value > & selfRef ) const
{
	return argList_->findNamed( values_, fieldID );
}

size_t
Lang::Structure::valueCount( ) const
{
	return argList_->orderedExprs_->size( ) + argList_->namedExprs_->size( );
}

Kernel::VariableHandle
Lang::Structure::getPosition( size_t pos, const RefCountPtr< const Lang::Value > & selfRef ) const
{
	return argList_->getOrdered( values_, pos );
}

RefCountPtr< const Lang::Structure >
Lang::Structure::getSink( size_t consumedArguments ) const
{
	if( argList_->orderedExprs_->size( ) <= consumedArguments )
		{
			return Lang::THE_EMPTY_STRUCT;
		}

	static std::vector< const Ast::ArgListExprs * > argLists;
	size_t resSize = argList_->orderedExprs_->size( ) - consumedArguments;
	if( resSize >= argLists.size( ) )
		{
			argLists.reserve( resSize + 1 );
			while( argLists.size( ) <= resSize )
				{
					argLists.push_back( new Ast::ArgListExprs( argLists.size( ) ) );
				}
		}

	RefCountPtr< const Lang::SingleList > resValues = values_;
	try
		{
			for( size_t i = 0; i < consumedArguments; ++i )
				{
					resValues = Helpers::try_cast_CoreArgument< const Lang::SingleListPair >( resValues )->cdr_;
				}
		}
	catch( const NonLocalExit::NotThisType & ball )
		{
			throw Exceptions::InternalError( "When constructing the sink, there was not enough arguments." );
		}
	return RefCountPtr< const Lang::Structure >( new Lang::Structure( argLists[ resSize ], resValues, false ) );
}

void
Lang::Structure::gcMark( Kernel::GCMarkedSet & marked )
{
	const_cast< Lang::SingleList * >( values_.getPtr( ) )->gcMark( marked );
}


Kernel::StructureFactory::StructureFactory( const std::list< const char * > & fields )
{
	init( fields );
}

Kernel::StructureFactory::StructureFactory( const char * field1 )
{
	std::list< const char * > fields;
	fields.push_back( field1 );
	init( fields );
}

Kernel::StructureFactory::StructureFactory( const char * field1, const char * field2 )
{
	std::list< const char * > fields;
	fields.push_back( field1 );
	fields.push_back( field2 );
	init( fields );
}

Kernel::StructureFactory::StructureFactory( const char * field1, const char * field2, const char * field3 )
{
	std::list< const char * > fields;
	fields.push_back( field1 );
	fields.push_back( field2 );
	fields.push_back( field3 );
	init( fields );
}

Kernel::StructureFactory::StructureFactory( const char * field1, const char * field2, const char * field3, const char * field4 )
{
	std::list< const char * > fields;
	fields.push_back( field1 );
	fields.push_back( field2 );
	fields.push_back( field3 );
	fields.push_back( field4 );
	init( fields );
}

void
Kernel::StructureFactory::init( const std::list< const char * > & fields )
{
	Ast::ArgListExprs * tmp = new Ast::ArgListExprs( false );
	typedef typeof fields ListType;
	for( ListType::const_iterator i = fields.begin( ); i != fields.end( ); ++i )
		{
			(*tmp->namedExprs_)[ *i ] = 0;
			typedef typeof values_ MapType;
			values_.insert( MapType::value_type( *i, Kernel::THE_VOID_VARIABLE ) );
		}
	argList_ = tmp;
}

void
Kernel::StructureFactory::clear( )
{
	typedef typeof values_ MapType;
	for( MapType::iterator i = values_.begin( ); i != values_.end( ); ++i )
		{
			i->second = Kernel::THE_VOID_VARIABLE;
		}
}

void
Kernel::StructureFactory::set( const char * field, const Kernel::VariableHandle & value )
{
	typedef typeof values_ MapType;
	MapType::iterator i = values_.find( field );
	if( i == values_.end( ) )
		{
			throw Exceptions::InternalError( "Kernel::StructureFactory::set: Field was not declared." );
		}
	i->second = value;
}

RefCountPtr< const Lang::Structure >
Kernel::StructureFactory::build( )
{
	RefCountPtr< const Lang::SingleList > tmp = Lang::THE_CONS_NULL;
	typedef typeof values_ MapType;
	for( MapType::iterator i = values_.begin( ); i != values_.end( ); ++i )
		{
			tmp = RefCountPtr< const Lang::SingleList >( new Lang::SingleListPair( i->second, tmp ) );
		}
	return RefCountPtr< const Lang::Structure >( new Lang::Structure( argList_, tmp ) );
}


Kernel::UnnamedStructureFactory::UnnamedStructureFactory( )
{ }

RefCountPtr< const Lang::Structure >
Kernel::UnnamedStructureFactory::build( const RefCountPtr< const Lang::SingleList > & values ) const
{
	/* Here we use that argLists_ is mutable, so that we can extend it with more entries as needed. */
	size_t sz = 0;
	for( RefCountPtr< const Lang::SingleList > tmp = values; ! tmp->isNull( ); tmp = dynamic_cast< const Lang::SingleListPair * >( tmp.getPtr( ) )->cdr_, ++sz )
		;
	typedef typeof argLists_ MapType;
	MapType::iterator i = argLists_.find( sz );
	const Ast::ArgListExprs * argList = 0;
	if( i == argLists_.end( ) )
		{
			Ast::ArgListExprs * tmp = new Ast::ArgListExprs( false );
			for( size_t i = 0; i < sz; ++i )
				{
					tmp->orderedExprs_->push_back( 0 );
				}
			argList = tmp;
			argLists_.insert( MapType::value_type( sz, argList ) );
		}
	else
		{
			argList = i->second;
		}
	return RefCountPtr< const Lang::Structure >( new Lang::Structure( argList, values ) );
}


Kernel::ForceFunctionAndCall_2_args_1_state_cont::ForceFunctionAndCall_2_args_1_state_cont( const Kernel::VariableHandle & arg1, const Kernel::VariableHandle & arg2, Kernel::StateHandle state, const Kernel::PassedDyn & dyn, const Kernel::ContRef & cont, const Ast::SourceLocation & callLoc )
	: Kernel::Continuation( callLoc ), arg1_( arg1 ), arg2_( arg2 ), state_( state ), dyn_( dyn ), cont_( cont )
{ }

Kernel::ForceFunctionAndCall_2_args_1_state_cont::~ForceFunctionAndCall_2_args_1_state_cont( )
{ }

void
Kernel::ForceFunctionAndCall_2_args_1_state_cont::takeValue( const RefCountPtr< const Lang::Value > & val, Kernel::EvalState * evalState, bool dummy ) const
{
	evalState->dyn_ = dyn_;
	evalState->cont_ = cont_;
	typedef const Lang::Function ArgType;
	RefCountPtr< ArgType > fun( Helpers::down_cast_ContinuationArgument< ArgType >( val, this ) );
	fun->call( fun, evalState, arg1_, arg2_, state_, traceLoc_ );
}

Kernel::ContRef
Kernel::ForceFunctionAndCall_2_args_1_state_cont::up( ) const
{
	return cont_;
}

RefCountPtr< const char >
Kernel::ForceFunctionAndCall_2_args_1_state_cont::description( ) const
{
	return strrefdup( "call function with two values and one state" );
}

void
Kernel::ForceFunctionAndCall_2_args_1_state_cont::gcMark( Kernel::GCMarkedSet & marked )
{
	arg1_->gcMark( marked );
	arg2_->gcMark( marked );
	dyn_->gcMark( marked );
	cont_->gcMark( marked );
}


void
Kernel::VectorFunction_register_methods( Lang::SystemFinalClass * dstClass )
{
	dstClass->registerMethod( new Kernel::MethodFactory< Lang::VectorFunction, Lang::Method_foldl< Lang::VectorFunction > >( ) );
	dstClass->registerMethod( new Kernel::MethodFactory< Lang::VectorFunction, Lang::Method_foldr< Lang::VectorFunction > >( ) );
	dstClass->registerMethod( new Kernel::MethodFactory< Lang::VectorFunction, Lang::Method_foldsl< Lang::VectorFunction > >( ) );
	dstClass->registerMethod( new Kernel::MethodFactory< Lang::VectorFunction, Lang::Method_foldsr< Lang::VectorFunction > >( ) );
}


RefCountPtr< const Lang::SingleList >
Helpers::SingleList_cons( Lang::Value * car, const RefCountPtr< const Lang::SingleList > & cdr )
{
	return RefCountPtr< const Lang::SingleList >( new Lang::SingleListPair( Kernel::VariableHandle( new Kernel::Variable( Kernel::ValueRef( car ) ) ),
																																					cdr ) );
}

RefCountPtr< const Lang::SingleList >
Helpers::SingleList_cons( const RefCountPtr< const Lang::Value > & car, const RefCountPtr< const Lang::SingleList > & cdr )
{
	return RefCountPtr< const Lang::SingleList >( new Lang::SingleListPair( Kernel::VariableHandle( new Kernel::Variable( car ) ),
																																					cdr ) );
}
