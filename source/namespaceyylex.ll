/* This file is part of Shapes.
 *
 * Shapes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Shapes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2015 Henrik Tidefelt
 */

%{

#include "namespacescanner.h"
#include "strrefdup.h"
#include "exitcodes.h"
#include "globals.h"
#include "shapesexceptions.h"

using namespace Shapes;
#include "yyltype.h"

#include <string.h>
#include <iostream>
#include <cstdio> // This is a workaround for a bug in Flex.

#define YY_USER_ACTION doBeforeEachAction( );
#define YY_EXIT_FAILURE Shapes::Interaction::EXIT_INTERNAL_ERROR

YYLTYPE namespacelloc;

%}

DirPrefix ^[ \t]*
UnquotedArg [^ \t\n<>]+
QuotedArg ["]([^"]|([\\][\\"]))*["]

%option c++
%option noyywrap

%option prefix="namespace"
%option yyclass="NamespaceScanner"

%x DiscardRestOfLineAndBEGIN_INITIAL
%x Prelude
%x Ignore
%x Order

%%

{DirPrefix}"encapsulated" {
  declarations_->setEncapsulated();
}
{DirPrefix}"prelude:" { BEGIN( Prelude ); }
{DirPrefix}"ignore:" { BEGIN( Ignore ); }
{DirPrefix}"order:" {
  firstSet_ = true;
  orderSet2_.clear( );
  BEGIN( Order );
}
{DirPrefix} {
  Ast::theAnalysisErrorsList.push_back( new Exceptions::NamespaceDirectoryError( namespacelloc, strrefdup( "Unrecognized command." ) ) );
  BEGIN( DiscardRestOfLineAndBEGIN_INITIAL );
}

<*>[ ]+ ;

<*>[\t] {
  Ast::theAnalysisErrorsList.push_back( new Exceptions::NamespaceDirectoryError( namespacelloc, strrefdup( "Illegal use of reserved tab character." ) ) );
}

<Prelude,Ignore,Order>[\n][ ] {
  endOfLine( );
  ++namespacelloc.lastColumn;
}
<Prelude,Ignore>[\n] {
  endOfLine( );
  BEGIN( INITIAL );
}

<Prelude>{UnquotedArg} {
  declarations_->addPrelude( yytext );
}
<Prelude>{QuotedArg} {
  declarations_->addPrelude( rinseString( yytext ) );
}
<Prelude>. {
  std::cerr << "yytext: \"" << yytext << "\"" << std::endl ;
  throw Exceptions::NamespaceDirectoryError( namespacelloc, strrefdup( "Stray character among <prelude> arguments." ) );
}

<Ignore>{UnquotedArg} {
  declarations_->addIgnore( yytext );
}
<Ignore>{QuotedArg} {
  declarations_->addIgnore( rinseString( yytext ) );
}
<Ignore>. {
  throw Exceptions::NamespaceDirectoryError( namespacelloc, strrefdup( "Stray character among <ignore> arguments." ) );
}

<Order>{UnquotedArg} {
  orderSet2_.insert( orderSet2_.begin( ), yytext );
}
<Order>{QuotedArg} {
  orderSet2_.insert( orderSet2_.begin( ), rinseString( yytext ) );
}
<Order>[<] {
  if( orderSet2_.empty( ) ){
    Ast::theAnalysisErrorsList.push_back( new Exceptions::NamespaceDirectoryError( namespacelloc, strrefdup( "Missing item." ) ) );
  }else if( firstSet_ ){
    firstSet_ = false;
  }else{
    declarations_->addPrecedes( orderSet1_, orderSet2_ );
  }
  orderSet1_ = orderSet2_;
  orderSet2_.clear( );
}
<Order>[\n] {
  endOfLine( );
  if( orderSet2_.empty( ) ){
    Ast::theAnalysisErrorsList.push_back( new Exceptions::NamespaceDirectoryError( namespacelloc, strrefdup( "Missing item." ) ) );
  } else if( firstSet_ ){
    Ast::theAnalysisErrorsList.push_back( new Exceptions::NamespaceDirectoryError( namespacelloc, strrefdup( "Expected '<'." ) ) );
  }else{
    declarations_->addPrecedes( orderSet1_, orderSet2_ );
  }
  BEGIN( INITIAL );
}
<Order>. {
  throw Exceptions::NamespaceDirectoryError( namespacelloc, strrefdup( "Stray character among <order> arguments." ) );
}

<DiscardRestOfLineAndBEGIN_INITIAL>.* {
  BEGIN( INITIAL );
}
<DiscardRestOfLineAndBEGIN_INITIAL>[\n] {
  endOfLine( );
  BEGIN( INITIAL );
}

<*>[\n] {
  endOfLine( );
}

<*>#.*[\n] {
  ++namespacelloc.lastLine;
  namespacelloc.lastColumn = 0;
}

. {
  Ast::theAnalysisErrorsList.push_back( new Exceptions::NamespaceDirectoryError( namespacelloc, strrefdup( ( std::string( "Unrecognized token: " ) + yytext ).c_str( ) ) ) );
  BEGIN( DiscardRestOfLineAndBEGIN_INITIAL );
}

%%
/* The closing %% above marks the end of the Rules section and the beginning
 * of the User Subroutines section. All text from here to the end of the
 * file is copied verbatim to the end of the generated lex.pdf.c file.
 * This section is where you put definitions of helper functions.
 */
