/* This file is part of Shapes.
 *
 * Shapes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Shapes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2008, 2009, 2013 Henrik Tidefelt
 */

#pragma once

#include "shapesexceptions.h"

namespace Shapes
{
	namespace Exceptions
	{
		class IllegalFinalReference : public RuntimeError
		{
			RefCountPtr< const char > valueType_;
			const char * fieldID_;
		public:
			IllegalFinalReference( RefCountPtr< const char > valueType, const char * fieldID );
			virtual ~IllegalFinalReference( );
			virtual void display( std::ostream & os ) const;
		};

		class ProtectedMemberPublicScope : public RuntimeError
		{
			RefCountPtr< const char > valueType_;
			const char * fieldID_;
		public:
			ProtectedMemberPublicScope( RefCountPtr< const char > valueType, const char * fieldID );
			virtual ~ProtectedMemberPublicScope( );
			virtual void display( std::ostream & os ) const;
		};

		class MemberNotAssignable : public RuntimeError
		{
			RefCountPtr< const char > valueType_;
			const char * fieldID_;
			RefCountPtr< const char > scope_;
		public:
			MemberNotAssignable( RefCountPtr< const char > valueType, const char * fieldID, RefCountPtr< const char > scope );
			virtual ~MemberNotAssignable( );
			virtual void display( std::ostream & os ) const;
		};

		class InstantiatingAbstractClass : public RuntimeError
		{
			RefCountPtr< const Lang::Class > cls_;
		public:
			InstantiatingAbstractClass( RefCountPtr< const Lang::Class > cls );
			virtual ~InstantiatingAbstractClass( );
			virtual void display( std::ostream & os ) const;
		};

		class FailedToDeclareClassAbstract : public RuntimeError
		{
			RefCountPtr< const Lang::Class > cls_;
			const Shapes::Ast::ClassFunction * classExpr_;
		public:
			FailedToDeclareClassAbstract( RefCountPtr< const Lang::Class > cls, const Shapes::Ast::ClassFunction * classExpr );
			virtual ~FailedToDeclareClassAbstract( );
			virtual void display( std::ostream & os ) const;
		};

		class RepeatedImmediateParent : public RuntimeError
		{
			RefCountPtr< const Lang::Class > cls_;
			RefCountPtr< const Lang::Class > parent_;
		public:
			RepeatedImmediateParent( RefCountPtr< const Lang::Class > cls, RefCountPtr< const Lang::Class > parent );
			virtual ~RepeatedImmediateParent( );
			virtual void display( std::ostream & os ) const;
		};

		class OverridingNonParent : public RuntimeError
		{
			RefCountPtr< const Lang::Class > cls_;
			RefCountPtr< const Lang::Class > parent_;
		public:
			OverridingNonParent( RefCountPtr< const Lang::Class > cls, RefCountPtr< const Lang::Class > parent );
			virtual ~OverridingNonParent( );
			virtual void display( std::ostream & os ) const;
		};

		class InheritingFinal : public RuntimeError
		{
			RefCountPtr< const Lang::Class > cls_;
			RefCountPtr< const Lang::Class > parent_;
		public:
			InheritingFinal( RefCountPtr< const Lang::Class > cls, RefCountPtr< const Lang::Class > parent );
			virtual ~InheritingFinal( );
			virtual void display( std::ostream & os ) const;
		};

		class OverridingUndeclaredMethod : public RuntimeError
		{
			RefCountPtr< const Lang::Class > cls_;
			RefCountPtr< const Lang::Class > parent_;
			const char * id_;
		public:
			OverridingUndeclaredMethod( RefCountPtr< const Lang::Class > cls, RefCountPtr< const Lang::Class > parent, const char * id );
			virtual ~OverridingUndeclaredMethod( );
			virtual void display( std::ostream & os ) const;
		};

		class OverridingFinalMethod : public RuntimeError
		{
			RefCountPtr< const Lang::Class > cls_;
			RefCountPtr< const Lang::Class > parent_;
			const char * id_;
		public:
			OverridingFinalMethod( RefCountPtr< const Lang::Class > cls, RefCountPtr< const Lang::Class > parent, const char * id );
			virtual ~OverridingFinalMethod( );
			virtual void display( std::ostream & os ) const;
		};

		class IllegalRepeatedBase : public RuntimeError
		{
			RefCountPtr< const Lang::Class > cls_;
			RefCountPtr< const Lang::Class > parent_;
		public:
			IllegalRepeatedBase( RefCountPtr< const Lang::Class > cls, RefCountPtr< const Lang::Class > parent );
			virtual ~IllegalRepeatedBase( );
			virtual void display( std::ostream & os ) const;
		};

		class AmbiguousInheritedMethod : public RuntimeError
		{
			RefCountPtr< const Lang::Class > cls_;
			RefCountPtr< const char > id_;
			std::set< RefCountPtr< const Lang::Class > > parents_;
		public:
			AmbiguousInheritedMethod( RefCountPtr< const Lang::Class > cls, RefCountPtr< const char > id, std::set< RefCountPtr< const Lang::Class > > parents );
			virtual ~AmbiguousInheritedMethod( );
			virtual void display( std::ostream & os ) const;
		};

		class MisplacedSuperReference : public RuntimeError
		{
		public:
			MisplacedSuperReference( const Ast::SourceLocation & loc );
			virtual ~MisplacedSuperReference( );
			virtual void display( std::ostream & os ) const;
		};

		class SuperReferenceClassNotParent : public RuntimeError
		{
			RefCountPtr< const Lang::Class > cls_;
			RefCountPtr< const Lang::Class > parent_;
		public:
			SuperReferenceClassNotParent( RefCountPtr< const Lang::Class > cls, RefCountPtr< const Lang::Class > parent );
			virtual ~SuperReferenceClassNotParent( );
			virtual void display( std::ostream & os ) const;
		};

		class NoSuchMethod : public RuntimeError
		{
			RefCountPtr< const Lang::Class > cls_;
			Kernel::MethodId method_;
		public:
			NoSuchMethod( RefCountPtr< const Lang::Class > cls, const Kernel::MethodId & method );
			virtual ~NoSuchMethod( );
			virtual void display( std::ostream & os ) const;
		};

		class NoSuchLocalMethod : public RuntimeError
		{
			RefCountPtr< const Lang::Class > cls_;
			Kernel::MethodId method_;
		public:
			NoSuchLocalMethod( RefCountPtr< const Lang::Class > cls, const Kernel::MethodId & method );
			virtual ~NoSuchLocalMethod( );
			virtual void display( std::ostream & os ) const;
		};
	}
}
