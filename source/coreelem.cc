/* This file is part of Shapes.
 *
 * Shapes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Shapes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2008, 2014, 2015 Henrik Tidefelt
 */

#include <cmath>

#include "Shapes_Helpers_decls.h"

#include "shapescore.h"
#include "ast.h"
#include "globals.h"
#include "shapesexceptions.h"
#include "consts.h"
#include "simplepdfi.h"
#include "autoonoff.h"

#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <stdio.h>
#include <stdlib.h>

#ifdef RAND_MAX
/* Use definition from stdlib.h */
#define RANDOM_MAX RAND_MAX
#define TWO_DIV_RANDOM_MAX (2. / RAND_MAX)
#else
/* Use BSD man page specification. */
#define RANDOM_MAX (((long)(1)<<31)-1)
#define TWO_DIV_RANDOM_MAX (2. / ( ((long)(1)<<31)-1. ))
#endif

using namespace Shapes;

namespace Shapes
{
	namespace Lang
	{

		class Core_nonNegativeModulo : public Lang::CoreFunction
		{
			Lang::Float::ValueType floatImpl( Lang::Float::ValueType num, Lang::Float::ValueType den ) const
			{
				Lang::Float::ValueType absDen = fabs( den );
				return num - floor( num / absDen ) * absDen;
			}
		public:
			Core_nonNegativeModulo( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name )
				: CoreFunction( ns, name, new Kernel::EvaluatedFormals( Ast::FileID::build_internal( name ), true ) )
			{
				formals_->appendEvaluatedCoreFormal( "dividend", Kernel::THE_SLOT_VARIABLE );
				formals_->appendEvaluatedCoreFormal( "divisor", Kernel::THE_SLOT_VARIABLE );
			}
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				args.applyDefaults( callLoc );

				try
					{
						typedef const Lang::Integer ArgType;
						Lang::Integer::ValueType num = Helpers::try_cast_CoreArgument< ArgType >( args.getValue( 0 ) )->val_;
						Lang::Integer::ValueType den = Helpers::down_cast_CoreArgument< ArgType >( id_, args, 1, callLoc )->val_;

						/* Division num / abs(den) with rounding towards negative infinity.
						 */
						Lang::Integer::ValueType absDen = (den >= 0) ? den : (-den);
						Lang::Integer::ValueType n = 0; /* Initialize to suppress warnings. */
						if (den != 0) {
							if (num >= 0)
								n = num / absDen;
							else
								n = -( 1 + (-num - 1) / absDen );
						} else {
							throw Exceptions::CoreOutOfRange( id_, args, 1, "The remainder is not defined when the denominator is zero." );
						}

						Kernel::ContRef cont = evalState->cont_;
						cont->takeValue( Kernel::ValueRef( new Lang::Integer( num - n * absDen ) ),
														 evalState );
						return;
					}
				catch( const NonLocalExit::NotThisType & ball )
					{
						/* Wrong type; never mind!.. but see below!
						 */
					}

				try
					{
						typedef const Lang::Float ArgType;
						RefCountPtr< ArgType > num = Helpers::try_cast_CoreArgument< ArgType >( args.getValue( 0 ) );
						RefCountPtr< ArgType > den = Helpers::down_cast_CoreArgument< ArgType >( id_, args, 1, callLoc );

						Kernel::ContRef cont = evalState->cont_;
						cont->takeValue( Kernel::ValueRef( new Lang::Float( floatImpl( num->val_, den->val_ ) ) ),
														 evalState );
						return;
					}
				catch( const NonLocalExit::NotThisType & ball )
					{
						/* Wrong type; never mind!.. but see below!
						 */
					}

				try
					{
						typedef const Lang::Length ArgType;
						RefCountPtr< ArgType > num = Helpers::try_cast_CoreArgument< ArgType >( args.getValue( 0 ) );
						RefCountPtr< ArgType > den = Helpers::down_cast_CoreArgument< ArgType >( id_, args, 1, callLoc );

						Kernel::ContRef cont = evalState->cont_;
						cont->takeValue( Kernel::ValueRef( new Lang::Length( floatImpl( num->getScalar( ), den->getScalar( ) ) ) ),
														 evalState );
						return;
					}
				catch( const NonLocalExit::NotThisType & ball )
					{
						/* Wrong type; never mind!.. but see below!
						 */
					}

				throw Exceptions::CoreTypeMismatch( callLoc, id_, args, 0, Helpers::typeSetString( Lang::Integer::staticTypeName( ), Lang::Float::staticTypeName( ), Lang::Length::staticTypeName( ) ) );
			}
		};

		class Core_denominatorSignModulo : public Lang::CoreFunction
		{
			Lang::Float::ValueType floatImpl( Lang::Float::ValueType num, Lang::Float::ValueType den ) const
			{
				return num - floor( num / den ) * den;
			}
		public:
			Core_denominatorSignModulo( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name )
				: CoreFunction( ns, name, new Kernel::EvaluatedFormals( Ast::FileID::build_internal( name ), true ) )
			{
				formals_->appendEvaluatedCoreFormal( "dividend", Kernel::THE_SLOT_VARIABLE );
				formals_->appendEvaluatedCoreFormal( "divisor", Kernel::THE_SLOT_VARIABLE );
			}
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				args.applyDefaults( callLoc );

				try
					{
						typedef const Lang::Integer ArgType;
						Lang::Integer::ValueType num = Helpers::try_cast_CoreArgument< ArgType >( args.getValue( 0 ) )->val_;
						Lang::Integer::ValueType den = Helpers::down_cast_CoreArgument< ArgType >( id_, args, 1, callLoc )->val_;

						/* Division with rounding towards negative infinity.
						 */
						Lang::Integer::ValueType n = 0; /* Initialize to suppress warnings. */
						if (den > 0) {
							if (num >= 0)
								n = num / den;
							else
								n = -( 1 + (-num - 1) / den );
						} else if (den < 0) {
							if (num > 0)
								n = -( 1 + (num - 1) / (-den) );
							else
								n = (-num) / (-den);
						} else {
							throw Exceptions::CoreOutOfRange( id_, args, 1, "The remainder is not defined when the denominator is zero." );
						}

						Kernel::ContRef cont = evalState->cont_;
						cont->takeValue( Kernel::ValueRef( new Lang::Integer( num - n * den ) ),
														 evalState );
						return;
					}
				catch( const NonLocalExit::NotThisType & ball )
					{
						/* Wrong type; never mind!.. but see below!
						 */
					}

				try
					{
						typedef const Lang::Float ArgType;
						RefCountPtr< ArgType > num = Helpers::try_cast_CoreArgument< ArgType >( args.getValue( 0 ) );
						RefCountPtr< ArgType > den = Helpers::down_cast_CoreArgument< ArgType >( id_, args, 1, callLoc );

						Kernel::ContRef cont = evalState->cont_;
						cont->takeValue( Kernel::ValueRef( new Lang::Float( floatImpl( num->val_, den->val_ ) ) ),
														 evalState );
						return;
					}
				catch( const NonLocalExit::NotThisType & ball )
					{
						/* Wrong type; never mind!.. but see below!
						 */
					}

				try
					{
						typedef const Lang::Length ArgType;
						RefCountPtr< ArgType > num = Helpers::try_cast_CoreArgument< ArgType >( args.getValue( 0 ) );
						RefCountPtr< ArgType > den = Helpers::down_cast_CoreArgument< ArgType >( id_, args, 1, callLoc );

						Kernel::ContRef cont = evalState->cont_;
						cont->takeValue( Kernel::ValueRef( new Lang::Length( floatImpl( num->getScalar( ), den->getScalar( ) ) ) ),
														 evalState );
						return;
					}
				catch( const NonLocalExit::NotThisType & ball )
					{
						/* Wrong type; never mind!.. but see below!
						 */
					}

				throw Exceptions::CoreTypeMismatch( callLoc, id_, args, 0, Helpers::typeSetString( Lang::Integer::staticTypeName( ), Lang::Float::staticTypeName( ), Lang::Length::staticTypeName( ) ) );
			}
		};

		class Core_numeratorSignModulo : public Lang::CoreFunction
		{
			Lang::Float::ValueType floatImpl( Lang::Float::ValueType num, Lang::Float::ValueType den ) const
			{
				return fmod( num, den );
			}
		public:
			Core_numeratorSignModulo( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name )
				: CoreFunction( ns, name, new Kernel::EvaluatedFormals( Ast::FileID::build_internal( name ), true ) )
			{
				formals_->appendEvaluatedCoreFormal( "dividend", Kernel::THE_SLOT_VARIABLE );
				formals_->appendEvaluatedCoreFormal( "divisor", Kernel::THE_SLOT_VARIABLE );
			}
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				args.applyDefaults( callLoc );

				try
					{
						typedef const Lang::Integer ArgType;
						Lang::Integer::ValueType num = Helpers::try_cast_CoreArgument< ArgType >( args.getValue( 0 ) )->val_;
						Lang::Integer::ValueType den = Helpers::down_cast_CoreArgument< ArgType >( id_, args, 1, callLoc )->val_;

						/* Division with rounding towards zero.
						 */
						Lang::Integer::ValueType n = 0; /* Initialize to suppress warnings. */
						if (den > 0) {
							if (num >= 0)
								n = num / den;
							else
								n = -( (-num) / den );
						} else if (den < 0) {
							if (num >= 0)
								n = -( num / (-den) );
							else
								n = (-num) / (-den);
						} else {
							throw Exceptions::CoreOutOfRange( id_, args, 1, "The remainder is not defined when the denominator is zero." );
						}

						Kernel::ContRef cont = evalState->cont_;
						cont->takeValue( Kernel::ValueRef( new Lang::Integer( num - n * den ) ),
														 evalState );
						return;
					}
				catch( const NonLocalExit::NotThisType & ball )
					{
						/* Wrong type; never mind!.. but see below!
						 */
					}

				try
					{
						typedef const Lang::Float ArgType;
						RefCountPtr< ArgType > num = Helpers::try_cast_CoreArgument< ArgType >( args.getValue( 0 ) );
						RefCountPtr< ArgType > den = Helpers::down_cast_CoreArgument< ArgType >( id_, args, 1, callLoc );

						Kernel::ContRef cont = evalState->cont_;
						cont->takeValue( Kernel::ValueRef( new Lang::Float( floatImpl( num->val_, den->val_ ) ) ),
														 evalState );
						return;
					}
				catch( const NonLocalExit::NotThisType & ball )
					{
						/* Wrong type; never mind!.. but see below!
						 */
					}

				try
					{
						typedef const Lang::Length ArgType;
						RefCountPtr< ArgType > num = Helpers::try_cast_CoreArgument< ArgType >( args.getValue( 0 ) );
						RefCountPtr< ArgType > den = Helpers::down_cast_CoreArgument< ArgType >( id_, args, 1, callLoc );

						Kernel::ContRef cont = evalState->cont_;
						cont->takeValue( Kernel::ValueRef( new Lang::Length( floatImpl( num->getScalar( ), den->getScalar( ) ) ) ),
														 evalState );
						return;
					}
				catch( const NonLocalExit::NotThisType & ball )
					{
						/* Wrong type; never mind!.. but see below!
						 */
					}

				throw Exceptions::CoreTypeMismatch( callLoc, id_, args, 0, Helpers::typeSetString( Lang::Integer::staticTypeName( ), Lang::Float::staticTypeName( ), Lang::Length::staticTypeName( ) ) );
			}
		};

		class Core_floor : public Lang::CoreFunction
		{
		public:
			Core_floor( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name ) : CoreFunction( ns, name ) { }
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				const size_t ARITY = 1;
				CHECK_ARITY( args, ARITY, id_ );

				typedef const Lang::Float ArgType;
				RefCountPtr< ArgType > arg = Helpers::down_cast_CoreArgument< ArgType >( id_, args, 0, callLoc );

				/* Warning, unhandled overflow in the static_cast from floating point to integral type.
				 */
				Kernel::ContRef cont = evalState->cont_;
				cont->takeValue( Kernel::ValueRef( new Lang::Integer( static_cast< Lang::Integer::ValueType >( floor( arg->val_ ) ) ) ),
												 evalState );
			}
		};

		class Core_ceil : public Lang::CoreFunction
		{
		public:
			Core_ceil( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name ) : CoreFunction( ns, name ) { }
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				const size_t ARITY = 1;
				CHECK_ARITY( args, ARITY, id_ );

				typedef const Lang::Float ArgType;
				RefCountPtr< ArgType > arg = Helpers::down_cast_CoreArgument< ArgType >( id_, args, 0, callLoc );

				/* Warning, unhandled overflow in the static_cast from floating point to integral type.
				 */
				Kernel::ContRef cont = evalState->cont_;
				cont->takeValue( Kernel::ValueRef( new Lang::Integer( static_cast< Lang::Integer::ValueType >( ceil( arg->val_ ) ) ) ),
												 evalState );
			}
		};

		class Core_round : public Lang::CoreFunction
		{
		public:
			Core_round( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name ) : CoreFunction( ns, name ) { }
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				const size_t ARITY = 1;
				CHECK_ARITY( args, ARITY, id_ );

				typedef const Lang::Float ArgType;
				RefCountPtr< ArgType > arg = Helpers::down_cast_CoreArgument< ArgType >( id_, args, 0, callLoc );

				/* Warning, there are two sources of unhandled overflow here.
				 * 1) The range of a double is much greater than the range of a long (the result of lround).
				 * 2) lround produces a long, while Lang::Integer::ValueType is currently int.
				 */
				Kernel::ContRef cont = evalState->cont_;
				cont->takeValue( Kernel::ValueRef( new Lang::Integer( lround( arg->val_ ) ) ),
												 evalState );
			}
		};

		class Core_cos : public Lang::CoreFunction
		{
		public:
			Core_cos( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name ) : CoreFunction( ns, name ) { }
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				const size_t ARITY = 1;
				CHECK_ARITY( args, ARITY, id_ );

				typedef const Lang::Float ArgType;
				RefCountPtr< ArgType > arg = Helpers::down_cast_CoreArgument< ArgType >( id_, args, 0, callLoc );

				Kernel::ContRef cont = evalState->cont_;
				cont->takeValue( Kernel::ValueRef( new Lang::Float( cos( arg->val_ ) ) ),
												 evalState );
			}
		};

		class Core_sin : public Lang::CoreFunction
		{
		public:
			Core_sin( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name ) : CoreFunction( ns, name ) { }
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				const size_t ARITY = 1;
				CHECK_ARITY( args, ARITY, id_ );

				typedef const Lang::Float ArgType;
				RefCountPtr< ArgType > arg = Helpers::down_cast_CoreArgument< ArgType >( id_, args, 0, callLoc );

				Kernel::ContRef cont = evalState->cont_;
				cont->takeValue( Kernel::ValueRef( new Lang::Float( sin( arg->val_ ) ) ),
												 evalState );
			}
		};

		class Core_tan : public Lang::CoreFunction
		{
		public:
			Core_tan( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name ) : CoreFunction( ns, name ) { }
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				const size_t ARITY = 1;
				CHECK_ARITY( args, ARITY, id_ );

				typedef const Lang::Float ArgType;
				RefCountPtr< ArgType > arg = Helpers::down_cast_CoreArgument< ArgType >( id_, args, 0, callLoc );

				Kernel::ContRef cont = evalState->cont_;
				cont->takeValue( Kernel::ValueRef( new Lang::Float( tan( arg->val_ ) ) ),
												 evalState );
			}
		};

		class Core_cot : public Lang::CoreFunction
		{
		public:
			Core_cot( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name ) : CoreFunction( ns, name ) { }
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				const size_t ARITY = 1;
				CHECK_ARITY( args, ARITY, id_ );

				typedef const Lang::Float ArgType;
				RefCountPtr< ArgType > arg = Helpers::down_cast_CoreArgument< ArgType >( id_, args, 0, callLoc );

				Kernel::ContRef cont = evalState->cont_;
				cont->takeValue( Kernel::ValueRef( new Lang::Float( 1 / tan( arg->val_ ) ) ),
												 evalState );
			}
		};

		class Core_arccos : public Lang::CoreFunction
		{
		public:
			Core_arccos( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name ) : CoreFunction( ns, name ) { }
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				const size_t ARITY = 1;
				CHECK_ARITY( args, ARITY, id_ );

				typedef const Lang::Float ArgType;
				RefCountPtr< ArgType > arg = Helpers::down_cast_CoreArgument< ArgType >( id_, args, 0, callLoc );

				Kernel::ContRef cont = evalState->cont_;
				cont->takeValue( Kernel::ValueRef( new Lang::Float( acos( arg->val_ ) ) ),
												 evalState );
			}
		};

		class Core_arcsin : public Lang::CoreFunction
		{
		public:
			Core_arcsin( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name ) : CoreFunction( ns, name ) { }
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				const size_t ARITY = 1;
				CHECK_ARITY( args, ARITY, id_ );

				typedef const Lang::Float ArgType;
				RefCountPtr< ArgType > arg = Helpers::down_cast_CoreArgument< ArgType >( id_, args, 0, callLoc );

				Kernel::ContRef cont = evalState->cont_;
				cont->takeValue( Kernel::ValueRef( new Lang::Float( asin( arg->val_ ) ) ),
												 evalState );
			}
		};

		class Core_arctan : public Lang::CoreFunction
		{
		public:
			Core_arctan( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name ) : CoreFunction( ns, name ) { }
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				const size_t ARITY = 1;
				CHECK_ARITY( args, ARITY, id_ );

				typedef const Lang::Float ArgType;
				RefCountPtr< ArgType > arg = Helpers::down_cast_CoreArgument< ArgType >( id_, args, 0, callLoc );

				Kernel::ContRef cont = evalState->cont_;
				cont->takeValue( Kernel::ValueRef( new Lang::Float( atan( arg->val_ ) ) ),
												 evalState );
			}
		};

		class Core_pow : public Lang::CoreFunction
		{
		public:
			Core_pow( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name ) : CoreFunction( ns, name ) { }
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				const size_t ARITY = 2;
				CHECK_ARITY( args, ARITY, id_ );

				try
					{
						typedef const Lang::Float ArgTypeBase;
						RefCountPtr< ArgTypeBase > b = Helpers::try_cast_CoreArgument< ArgTypeBase >( args.getValue( 0 ) );

						try
							{
								typedef const Lang::Float ArgTypeExp;
								RefCountPtr< ArgTypeExp > e = Helpers::try_cast_CoreArgument< ArgTypeExp >( args.getValue( 1 ) );

								Kernel::ContRef cont = evalState->cont_;
								cont->takeValue( Kernel::ValueRef( new Lang::Float( pow( b->val_, e->val_ ) ) ),
																 evalState );
								return;
							}
						catch( const NonLocalExit::NotThisType & ball )
							{
								/* Wrong type; never mind!.. but see below!
								 */
							}

						try
							{
								typedef const Lang::Integer ArgTypeExp;
								RefCountPtr< ArgTypeExp > e = Helpers::try_cast_CoreArgument< ArgTypeExp >( args.getValue( 1 ) );

								Kernel::ContRef cont = evalState->cont_;
								cont->takeValue( Kernel::ValueRef( new Lang::Float( pow( b->val_, e->val_ ) ) ),
																 evalState );
								return;
							}
						catch( const NonLocalExit::NotThisType & ball )
							{
								/* Wrong type; never mind!.. but see below!
								 */
							}

						throw Exceptions::CoreTypeMismatch( callLoc, id_, args, 1, Helpers::typeSetString( Lang::Float::staticTypeName( ), Lang::Integer::staticTypeName( ) ) );

					}
				catch( const NonLocalExit::NotThisType & ball )
					{
						/* Wrong type; never mind!.. but see below!
						 */
					}

				try
					{
						typedef const Lang::Integer ArgType;
						ArgType::ValueType b = Helpers::try_cast_CoreArgument< ArgType >( args.getValue( 0 ) )->val_;
						ArgType::ValueType e = Helpers::down_cast_CoreArgument< ArgType >( id_, args, 1, callLoc )->val_;

						if( e < 0 )
							{
								throw Exceptions::CoreOutOfRange( id_, args, 1, "Power with integer base must have non-negative exponent." );
							}

						ArgType::ValueType result = 1;
						while( e > 0 ){
							if( ( e % 2 ) != 0 ){
								result *= b;
							}
							b = b * b;
							e /= 2;
						}

						Kernel::ContRef cont = evalState->cont_;
						cont->takeValue( Kernel::ValueRef( new Lang::Integer( result ) ),
														 evalState );
						return;
					}
				catch( const NonLocalExit::NotThisType & ball )
					{
						/* Wrong type; never mind!.. but see below!
						 */
					}

				throw Exceptions::CoreTypeMismatch( callLoc, id_, args, 0, Helpers::typeSetString( Lang::Float::staticTypeName( ), Lang::Integer::staticTypeName( ) ) );
			}
		};

		class Core_exp : public Lang::CoreFunction
		{
		public:
			Core_exp( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name ) : CoreFunction( ns, name ) { }
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				const size_t ARITY = 1;
				CHECK_ARITY( args, ARITY, id_ );

				typedef const Lang::Float ArgType;
				RefCountPtr< ArgType > arg = Helpers::down_cast_CoreArgument< ArgType >( id_, args, 0, callLoc );

				Kernel::ContRef cont = evalState->cont_;
				cont->takeValue( Kernel::ValueRef( new Lang::Float( exp( arg->val_ ) ) ),
												 evalState );
			}
		};

		class Core_log : public Lang::CoreFunction
		{
		public:
			Core_log( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name ) : CoreFunction( ns, name ) { }
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				const size_t ARITY = 1;
				CHECK_ARITY( args, ARITY, id_ );

				typedef const Lang::Float ArgType;
				ArgType::ValueType arg = Helpers::down_cast_CoreArgument< ArgType >( id_, args, 0, callLoc )->val_;
				if( arg <= 0.0 )
					{
						throw Exceptions::CoreOutOfRange( id_, args, 0, "Argument must be strictly positive." );
					}

				Kernel::ContRef cont = evalState->cont_;
				cont->takeValue( Kernel::ValueRef( new Lang::Float( log( arg ) ) ),
												 evalState );
			}
		};

		class Core_log10 : public Lang::CoreFunction
		{
		public:
			Core_log10( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name ) : CoreFunction( ns, name ) { }
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				const size_t ARITY = 1;
				CHECK_ARITY( args, ARITY, id_ );

				typedef const Lang::Float ArgType;
				ArgType::ValueType arg = Helpers::down_cast_CoreArgument< ArgType >( id_, args, 0, callLoc )->val_;
				if( arg <= 0.0 )
					{
						throw Exceptions::CoreOutOfRange( id_, args, 0, "Argument must be strictly positive." );
					}

				Kernel::ContRef cont = evalState->cont_;
				cont->takeValue( Kernel::ValueRef( new Lang::Float( log10( arg ) ) ),
												 evalState );
			}
		};

		class Core_min : public Lang::CoreFunction
		{
		public:
			Core_min( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name ) : CoreFunction( ns, name ) { }
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				if( args.empty( ) )
					{
						Kernel::ContRef cont = evalState->cont_;
						cont->takeValue( Kernel::ValueRef( new Lang::Float( HUGE_VAL ) ),
														 evalState );
						return;
					}

				{
					typedef const Lang::Float ArgType;
					size_t i = 0;
					ArgType * arg = dynamic_cast< ArgType * >( args.getValue( i ).getPtr( ) );
					if( arg != 0 )
						{
							double res = arg->val_;
							++i;
							const size_t & end = args.size( );
							for( ; i != end; ++i )
								{
									res = std::min( res, Helpers::down_cast_CoreArgument< ArgType >( id_, args, i, callLoc )->val_ );
								}
							Kernel::ContRef cont = evalState->cont_;
							cont->takeValue( Kernel::ValueRef( new ArgType( res ) ),
															 evalState );
							return;
						}
				}

				{
					typedef const Lang::Length ArgType;
					size_t i = 0;
					ArgType * arg = dynamic_cast< ArgType * >( args.getValue( i ).getPtr( ) );
					if( arg != 0 )
						{
							Concrete::Length res = arg->get( );
							++i;
							const size_t & end = args.size( );
							for( ; i != end; ++i )
								{
									res = std::min( res, Helpers::down_cast_CoreArgument< ArgType >( id_, args, i, callLoc )->get( ) );
								}
							Kernel::ContRef cont = evalState->cont_;
							cont->takeValue( Kernel::ValueRef( new ArgType( res ) ),
															 evalState );
							return;
						}
				}

				{
					typedef const Lang::Integer ArgType;
					size_t i = 0;
					ArgType * arg = dynamic_cast< ArgType * >( args.getValue( i ).getPtr( ) );
					if( arg != 0 )
						{
							Lang::Integer::ValueType res = arg->val_;
							++i;
							const size_t & end = args.size( );
							for( ; i != end; ++i )
								{
									res = std::min( res, Helpers::down_cast_CoreArgument< ArgType >( id_, args, i, callLoc )->val_ );
								}
							Kernel::ContRef cont = evalState->cont_;
							cont->takeValue( Kernel::ValueRef( new ArgType( res ) ),
															 evalState );
							return;
						}
				}

				throw Exceptions::CoreTypeMismatch( callLoc, id_, args, 0, Helpers::typeSetString( Lang::Float::staticTypeName( ), Lang::Integer::staticTypeName( ), Lang::Length::staticTypeName( ) ) );
			}
		};

		class Core_max : public Lang::CoreFunction
		{
		public:
			Core_max( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name ) : CoreFunction( ns, name ) { }
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				if( args.empty( ) )
					{
						Kernel::ContRef cont = evalState->cont_;
						cont->takeValue( Kernel::ValueRef( new Lang::Float( -HUGE_VAL ) ),
														 evalState );
						return;
					}

				{
					typedef const Lang::Float ArgType;
					size_t i = 0;
					ArgType * arg = dynamic_cast< ArgType * >( args.getValue( i ).getPtr( ) );
					if( arg != 0 )
						{
							double res = arg->val_;
							++i;
							const size_t & end = args.size( );
							for( ; i != end; ++i )
								{
									res = std::max( res, Helpers::down_cast_CoreArgument< ArgType >( id_, args, i, callLoc )->val_ );
								}
							Kernel::ContRef cont = evalState->cont_;
							cont->takeValue( Kernel::ValueRef( new ArgType( res ) ),
															 evalState );
							return;
						}
				}

				{
					typedef const Lang::Length ArgType;
					size_t i = 0;
					ArgType * arg = dynamic_cast< ArgType * >( args.getValue( i ).getPtr( ) );
					if( arg != 0 )
						{
							Concrete::Length res = arg->get( );
							++i;
							const size_t & end = args.size( );
							for( ; i != end; ++i )
								{
									res = std::max( res, Helpers::down_cast_CoreArgument< ArgType >( id_, args, i, callLoc )->get( ) );
								}
							Kernel::ContRef cont = evalState->cont_;
							cont->takeValue( Kernel::ValueRef( new ArgType( res ) ),
															 evalState );
							return;
						}
				}


				{
					typedef const Lang::Integer ArgType;
					size_t i = 0;
					ArgType * arg = dynamic_cast< ArgType * >( args.getValue( i ).getPtr( ) );
					if( arg != 0 )
						{
							Lang::Integer::ValueType res = arg->val_;
							++i;
							const size_t & end = args.size( );
							for( ; i != end; ++i )
								{
									res = std::max( res, Helpers::down_cast_CoreArgument< ArgType >( id_, args, i, callLoc )->val_ );
								}
							Kernel::ContRef cont = evalState->cont_;

							cont->takeValue( Kernel::ValueRef( new ArgType( res ) ),
															 evalState );
							return;
						}
				}

				throw Exceptions::CoreTypeMismatch( callLoc, id_, args, 0, Helpers::typeSetString( Lang::Float::staticTypeName( ), Lang::Integer::staticTypeName( ), Lang::Length::staticTypeName( ) ) );
			}
		};

		class Core_sqrt : public Lang::CoreFunction
		{
		public:
			Core_sqrt( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name ) : CoreFunction( ns, name ) { }
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				const size_t ARITY = 1;
				CHECK_ARITY( args, ARITY, id_ );

				typedef const Lang::Float ArgType;
				RefCountPtr< ArgType > arg = Helpers::down_cast_CoreArgument< ArgType >( id_, args, 0, callLoc );

				if( arg->val_ < 0.0 )
					{
						throw Exceptions::CoreOutOfRange( id_, args, 0, "The square root is not defined for negative values." );
					}

				Kernel::ContRef cont = evalState->cont_;
				cont->takeValue( Kernel::ValueRef( new Lang::Float( sqrt( arg->val_ ) ) ),
												 evalState );
			}
		};

		class Core_angle : public Lang::CoreFunction
		{
		public:
			Core_angle( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name ) : CoreFunction( ns, name ) { }
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				const size_t ARITY = 1;
				CHECK_ARITY( args, ARITY, id_ );

				{
					typedef const Lang::Coords2D ArgType;
					ArgType * arg = dynamic_cast< ArgType * >( args.getValue( 0 ).getPtr( ) );
					if( arg != 0 )
						{
							if( arg->x_.get( ) == 0 && arg->y_.get( ) == 0 )
								{
									throw Exceptions::CoreOutOfRange( id_, args, 0, "Can't take the angle of something of norm 0." );
								}
							Kernel::ContRef cont = evalState->cont_;
							cont->takeValue( Kernel::ValueRef( new Lang::Float( atan2( arg->y_.getScalar( ), arg->x_.getScalar( ) ) ) ),
															 evalState );
							return;
						}
				}

				{
					typedef const Lang::FloatPair ArgType;
					ArgType * arg = dynamic_cast< ArgType * >( args.getValue( 0 ).getPtr( ) );
					if( arg != 0 )
						{
							if( arg->x_ == 0 && arg->y_ == 0 )
								{
									throw Exceptions::CoreOutOfRange( id_, args, 0, "Can't take the angle of something of norm 0." );
								}
							Kernel::ContRef cont = evalState->cont_;
							cont->takeValue( Kernel::ValueRef( new Lang::Float( atan2( arg->y_, arg->x_ ) ) ),
															 evalState );
							return;
						}
				}

				throw Exceptions::CoreTypeMismatch( callLoc, id_, args, 0, Interaction::SEVERAL_TYPES );
			}
		};

		class Core_dir : public Lang::CoreFunction
		{
		public:
			Core_dir( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name ) : CoreFunction( ns, name ) { }
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				const size_t ARITY = 1;
				CHECK_ARITY( args, ARITY, id_ );

				typedef const Lang::Float ArgType;
				RefCountPtr< ArgType > arg = Helpers::down_cast_CoreArgument< ArgType >( id_, args, 0, callLoc );

				Kernel::ContRef cont = evalState->cont_;
				cont->takeValue( Kernel::ValueRef( new Lang::FloatPair( cos( arg->val_ ), sin( arg->val_ ) ) ),
												 evalState );
			}
		};

		class Core_abs : public Lang::CoreFunction
		{
		public:
			Core_abs( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name ) : CoreFunction( ns, name ) { }
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				const size_t ARITY = 1;
				CHECK_ARITY( args, ARITY, id_ );

				const Lang::Value * untypedArg = args.getValue( 0 ).getPtr( );

				{
					typedef const Lang::Float ArgType;
					ArgType * arg = dynamic_cast< ArgType * >( untypedArg );
					if( arg != 0 )
						{
							Kernel::ContRef cont = evalState->cont_;
							cont->takeValue( Kernel::ValueRef( new Lang::Float( fabs( arg->val_ ) ) ),
															 evalState );
							return;
						}
				}

				{
					typedef const Lang::Length ArgType;
					ArgType * arg = dynamic_cast< ArgType * >( untypedArg );
					if( arg != 0 )
						{
							Kernel::ContRef cont = evalState->cont_;
							cont->takeValue( Kernel::ValueRef( new Lang::Length( arg->get( ).abs( ) ) ),
															 evalState );
							return;
						}
				}

				{
					typedef const Lang::FloatPair ArgType;
					ArgType * arg = dynamic_cast< ArgType * >( untypedArg );
					if( arg != 0 )
						{
							Kernel::ContRef cont = evalState->cont_;
							cont->takeValue( Kernel::ValueRef( new Lang::Float( hypot( arg->x_, arg->y_ ) ) ),
															 evalState );
							return;
						}
				}

				{
					typedef const Lang::Coords2D ArgType;
					ArgType * arg = dynamic_cast< ArgType * >( untypedArg );
					if( arg != 0 )
						{
							Kernel::ContRef cont = evalState->cont_;
							cont->takeValue( Kernel::ValueRef( new Lang::Length( hypotPhysical( arg->x_.get( ), arg->y_.get( ) ) ) ),
															 evalState );
							return;
						}
				}

				{
					typedef const Lang::FloatTriple ArgType;
					ArgType * arg = dynamic_cast< ArgType * >( untypedArg );
					if( arg != 0 )
						{
							Kernel::ContRef cont = evalState->cont_;
							cont->takeValue( Kernel::ValueRef( new Lang::Float( Concrete::Scalar::hypot3( arg->x_, arg->y_, arg->z_ ) ) ),
															 evalState );
							return;
						}
				}

				{
					typedef const Lang::Coords3D ArgType;
					ArgType * arg = dynamic_cast< ArgType * >( untypedArg );
					if( arg != 0 )
						{
							Kernel::ContRef cont = evalState->cont_;
							cont->takeValue( Kernel::ValueRef( new Lang::Length( hypotPhysical( arg->x_.get( ), arg->y_.get( ), arg->z_.get( ) ) ) ),
															 evalState );
							return;
						}
				}

				{
					typedef const Lang::ElementaryPath2D ArgType;
					try
						{
							RefCountPtr< ArgType > p = Helpers::elementaryPathTry2D( args.getValue( 0 ) );
							Kernel::ContRef cont = evalState->cont_;
							cont->takeValue( RefCountPtr< const Lang::Length >( new Lang::Length( p->arcLength( ) ) ),
															 evalState );
							return;
						}
					catch( NonLocalExit::NotThisType & ball )
						{
							/* Never mind */
						}
				}

				{
					typedef const Lang::ElementaryPath3D ArgType;
					try
						{
							RefCountPtr< ArgType > p = Helpers::elementaryPathTry3D( args.getValue( 0 ) );
							Kernel::ContRef cont = evalState->cont_;
							cont->takeValue( RefCountPtr< const Lang::Length >( new Lang::Length( p->arcLength( ) ) ),
															 evalState );
							return;
						}
					catch( NonLocalExit::NotThisType & ball )
						{
							/* Never mind */
						}
				}

				{
					typedef const Lang::Integer ArgType;
					ArgType * arg = dynamic_cast< ArgType * >( untypedArg );
					if( arg != 0 )
						{
							Kernel::ContRef cont = evalState->cont_;
							cont->takeValue( Kernel::ValueRef( new Lang::Integer( ( arg->val_ >= 0 ) ? (arg->val_) : (-arg->val_) ) ),
															 evalState );
							return;
						}
				}

				{
					typedef const Lang::Dash ArgType;
					ArgType * arg = dynamic_cast< ArgType * >( untypedArg );
					if( arg != 0 )
						{
							Kernel::ContRef cont = evalState->cont_;
							cont->takeValue( Kernel::ValueRef( new Lang::Length( arg->length( ) ) ),
															 evalState );
							return;
						}
				}

				throw Exceptions::CoreTypeMismatch( callLoc, id_, args, 0, Interaction::SEVERAL_TYPES );
			}
		};

		class Core_normalized : public Lang::CoreFunction
		{
		public:
			Core_normalized( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name ) : CoreFunction( ns, name ) { }
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				const size_t ARITY = 1;
				CHECK_ARITY( args, ARITY, id_ );

				{
					typedef const Lang::FloatPair ArgType;
					ArgType * arg = dynamic_cast< ArgType * >( args.getValue( 0 ).getPtr( ) );
					if( arg != 0 )
						{
							double norm = hypot( arg->x_, arg->y_ );
							if( norm == 0 )
								{
									throw Exceptions::CoreOutOfRange( id_, args, 0, "Can't normalize something of norm 0" );
								}
							Kernel::ContRef cont = evalState->cont_;
							cont->takeValue( Kernel::ValueRef( new Lang::FloatPair( arg->x_ / norm, arg->y_ / norm ) ),
															 evalState );
							return;
						}
				}

				{
					typedef const Lang::Coords2D ArgType;
					ArgType * arg = dynamic_cast< ArgType * >( args.getValue( 0 ).getPtr( ) );
					if( arg != 0 )
						{
							Concrete::Length norm = hypotPhysical( arg->x_.get( ), arg->y_.get( ) );
							if( norm == 0 )
								{
									throw Exceptions::CoreOutOfRange( id_, args, 0, "Can't normalize something of norm 0" );
								}
							Kernel::ContRef cont = evalState->cont_;
							cont->takeValue( Kernel::ValueRef( new Lang::FloatPair( arg->x_.get( ) / norm, arg->y_.get( ) / norm ) ),
															 evalState );
							return;
						}
				}

				{
					typedef const Lang::FloatTriple ArgType;
					ArgType * arg = dynamic_cast< ArgType * >( args.getValue( 0 ).getPtr( ) );
					if( arg != 0 )
						{
							double norm = Concrete::Scalar::hypot3( arg->x_, arg->y_, arg->z_ );
							if( norm == 0 )
								{
									throw Exceptions::CoreOutOfRange( id_, args, 0, "Can't normalize something of norm 0" );
								}
							Kernel::ContRef cont = evalState->cont_;
							cont->takeValue( Kernel::ValueRef( new Lang::FloatTriple( arg->x_ / norm, arg->y_ / norm, arg->z_ / norm ) ),
															 evalState );
							return;
						}
				}

				{
					typedef const Lang::Coords3D ArgType;
					ArgType * arg = dynamic_cast< ArgType * >( args.getValue( 0 ).getPtr( ) );
					if( arg != 0 )
						{
							Concrete::Length norm = hypotPhysical( arg->x_.get( ), arg->y_.get( ), arg->z_.get( ) );
							if( norm == 0 )
								{
									throw Exceptions::CoreOutOfRange( id_, args, 0, "Can't normalize something of norm 0" );
								}
							Kernel::ContRef cont = evalState->cont_;
							cont->takeValue( Kernel::ValueRef( new Lang::FloatTriple( arg->x_.get( ) / norm, arg->y_.get( ) / norm, arg->z_.get( ) / norm ) ),
															 evalState );
							return;
						}
				}

				{
					typedef const Lang::Float ArgType;
					ArgType * arg = dynamic_cast< ArgType * >( args.getValue( 0 ).getPtr( ) );
					if( arg != 0 )
						{
							if( arg->val_ > 0 )
								{
									Kernel::ContRef cont = evalState->cont_;
									cont->takeValue( Kernel::ValueRef( new Lang::Float( 1 ) ),
																	 evalState );
									return;
								}
							if( arg->val_ < 0 )
								{
									Kernel::ContRef cont = evalState->cont_;
									cont->takeValue( Kernel::ValueRef( new Lang::Float( -1 ) ),
																	 evalState );
									return;
								}
							throw Exceptions::CoreOutOfRange( id_, args, 0, "Can't normalize something of norm 0" );
						}
				}

				{
					typedef const Lang::Length ArgType;
					ArgType * arg = dynamic_cast< ArgType * >( args.getValue( 0 ).getPtr( ) );
					if( arg != 0 )
						{
							if( arg->get( ) > 0 )
								{
									Kernel::ContRef cont = evalState->cont_;
									cont->takeValue( Kernel::ValueRef( new Lang::Float( 1 ) ),
																	 evalState );
									return;
								}
							if( arg->get( ) < 0 )
								{
									Kernel::ContRef cont = evalState->cont_;
									cont->takeValue( Kernel::ValueRef( new Lang::Float( -1 ) ),
																	 evalState );
									return;
								}
							throw Exceptions::CoreOutOfRange( id_, args, 0, "Can't normalize something of norm 0" );
						}
				}

				{
					typedef const Lang::Integer ArgType;
					ArgType * arg = dynamic_cast< ArgType * >( args.getValue( 0 ).getPtr( ) );
					if( arg != 0 )
						{
							if( arg->val_ > 0 )
								{
									Kernel::ContRef cont = evalState->cont_;
									cont->takeValue( Kernel::ValueRef( new Lang::Integer( 1 ) ),
																	 evalState );
									return;
								}
							if( arg->val_ < 0 )
								{
									Kernel::ContRef cont = evalState->cont_;
									cont->takeValue( Kernel::ValueRef( new Lang::Integer( -1 ) ),
																	 evalState );
									return;
								}
							throw Exceptions::CoreOutOfRange( id_, args, 0, "Can't normalize something of norm 0" );
						}
				}


				throw Exceptions::CoreTypeMismatch( callLoc, id_, args, 0, Interaction::SEVERAL_TYPES );
			}
		};

		class Core_cross : public Lang::CoreFunction
		{
		public:
			Core_cross( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name ) : CoreFunction( ns, name ) { }
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				const size_t ARITY = 2;
				CHECK_ARITY( args, ARITY, id_ );

				double x1;
				double y1;
				double z1;
				double x2;
				double y2;
				double z2;
				bool isLength = false;

				{
					typedef const Lang::FloatTriple ArgType;
					ArgType * arg = dynamic_cast< ArgType * >( args.getValue( 0 ).getPtr( ) );
					if( arg != 0 )
						{
							x1 = arg->x_;
							y1 = arg->y_;
							z1 = arg->z_;
							goto secondArgument;
						}
				}

				{
					typedef const Lang::Coords3D ArgType;
					ArgType * arg = dynamic_cast< ArgType * >( args.getValue( 0 ).getPtr( ) );
					if( arg != 0 )
						{
							isLength = true;
							x1 = arg->x_.get( ).offtype< 1, 0 >( );
							y1 = arg->y_.get( ).offtype< 1, 0 >( );
							z1 = arg->z_.get( ).offtype< 1, 0 >( );
							goto secondArgument;
						}
				}

				throw Exceptions::CoreTypeMismatch( callLoc, id_, args, 0, Helpers::typeSetString( Lang::FloatTriple::staticTypeName( ), Lang::Coords3D::staticTypeName( ) ) );

			secondArgument:
				{
					typedef const Lang::FloatTriple ArgType;
					ArgType * arg = dynamic_cast< ArgType * >( args.getValue( 1 ).getPtr( ) );
					if( arg != 0 )
						{
							x2 = arg->x_;
							y2 = arg->y_;
							z2 = arg->z_;
							goto multiplyArguments;
						}
				}

				{
					typedef const Lang::Coords3D ArgType;
					ArgType * arg = dynamic_cast< ArgType * >( args.getValue( 1 ).getPtr( ) );
					if( arg != 0 )
						{
							if( isLength )
								{
									throw Exceptions::CoreOutOfRange( id_, args, 1, "Can't multiply two coordinate vectors.	Try normalizing one of them!" );
								}
							isLength = true;
							x2 = arg->x_.get( ).offtype< 1, 0 >( );
							y2 = arg->y_.get( ).offtype< 1, 0 >( );
							z2 = arg->z_.get( ).offtype< 1, 0 >( );
							goto multiplyArguments;
						}
				}

				throw Exceptions::CoreTypeMismatch( callLoc, id_, args, 1, Helpers::typeSetString( Lang::FloatTriple::staticTypeName( ), Lang::Coords3D::staticTypeName( ) ) );

			multiplyArguments:
				Kernel::ContRef cont = evalState->cont_;
				if( isLength )
					{
						cont->takeValue( Kernel::ValueRef( new Lang::Coords3D( Concrete::Length( y1*z2-z1*y2 ), Concrete::Length( z1*x2-x1*z2 ), Concrete::Length( x1*y2-y1*x2 ) ) ),
														 evalState );
					}
				else
					{
						cont->takeValue( Kernel::ValueRef( new Lang::FloatTriple( y1*z2-z1*y2, z1*x2-x1*z2, x1*y2-y1*x2 ) ),
														 evalState );
					}
			}
		};

		class Core_orthogonal : public Lang::CoreFunction
		{
		public:
			Core_orthogonal( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name ) : CoreFunction( ns, name ) { }
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				const size_t ARITY = 1;
				CHECK_ARITY( args, ARITY, id_ );

				typedef const Lang::FloatPair ArgType;
				RefCountPtr< ArgType > arg = Helpers::down_cast_CoreArgument< ArgType >( id_, args, 0, callLoc );

				Kernel::ContRef cont = evalState->cont_;
				cont->takeValue( Kernel::ValueRef( new Lang::FloatPair( -arg->y_, arg->x_ ) ),
												 evalState );
			}
		};

		class Core_orthogonal3D : public Lang::CoreFunction
		{
		public:
			Core_orthogonal3D( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name ) : CoreFunction( ns, name ) { }
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				const size_t ARITY = 1;
				CHECK_ARITY( args, ARITY, id_ );

				typedef const Lang::FloatTriple ArgType;
				RefCountPtr< ArgType > arg = Helpers::down_cast_CoreArgument< ArgType >( id_, args, 0, callLoc );

				double x;
				double y;
				double z;
				if( fabs( arg->x_ ) < fabs( arg->y_ ) )
					{
						// arg is more parallell to y; cross with x
						x = 0;
						y = - arg->z_;
						z = arg->y_;
					}
				else
					{
						// arg is more parallell to x; cross with y
						x = arg->z_;
						y = 0;
						z = - arg->x_;
					}
				double scale = sqrt( ( (arg->x_)*(arg->x_) + (arg->y_)*(arg->y_) + (arg->z_)*(arg->z_) ) / ( x*x + y*y + z*z ) );
				Kernel::ContRef cont = evalState->cont_;
				cont->takeValue( Kernel::ValueRef( new Lang::FloatTriple( x * scale, y * scale, z * scale ) ),
												 evalState );
			}
		};

		class Core_randomBall1D : public Lang::CoreFunction
		{
		public:
			Core_randomBall1D( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name )
				: CoreFunction( ns, name, new Kernel::EvaluatedFormals( Ast::FileID::build_internal( name ), true ) )
			{
				formals_->appendCoreStateFormal( "state" );
			}
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				args.applyDefaults( callLoc );

				typedef Kernel::WarmRandomState StateType;
				StateType * st = Helpers::down_cast_CoreState< StateType >( id_, args, 0, callLoc );

				st->setState( );

				Kernel::ContRef cont = evalState->cont_;
				cont->takeValue( Kernel::ValueRef( new Lang::Float( TWO_DIV_RANDOM_MAX * static_cast< double >( random( ) ) - 1 ) ),
												 evalState );
			}
		};

		class Core_randomBall2D : public Lang::CoreFunction
		{
		public:
			Core_randomBall2D( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name )
				: CoreFunction( ns, name, new Kernel::EvaluatedFormals( Ast::FileID::build_internal( name ), true ) )
			{
				formals_->appendCoreStateFormal( "state" );
			}
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				args.applyDefaults( callLoc );

				typedef Kernel::WarmRandomState StateType;
				StateType * st = Helpers::down_cast_CoreState< StateType >( id_, args, 0, callLoc );

				st->setState( );

				double x1 = TWO_DIV_RANDOM_MAX * static_cast< double >( random( ) ) - 1;
				double x2 = TWO_DIV_RANDOM_MAX * static_cast< double >( random( ) ) - 1;
				while( x1 * x1 + x2 * x2 > 1 )
					{
						x1 = TWO_DIV_RANDOM_MAX * static_cast< double >( random( ) ) - 1;
						x2 = TWO_DIV_RANDOM_MAX * static_cast< double >( random( ) ) - 1;
					}

				Kernel::ContRef cont = evalState->cont_;
				cont->takeValue( Kernel::ValueRef( new Lang::FloatPair( x1, x2 ) ),
												 evalState );
			}
		};

		class Core_randomBall3D : public Lang::CoreFunction
		{
		public:
			Core_randomBall3D( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name )
				: CoreFunction( ns, name, new Kernel::EvaluatedFormals( Ast::FileID::build_internal( name ), true ) )
			{
				formals_->appendCoreStateFormal( "state" );
			}
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				args.applyDefaults( callLoc );

				typedef Kernel::WarmRandomState StateType;
				StateType * st = Helpers::down_cast_CoreState< StateType >( id_, args, 0, callLoc );

				st->setState( );

				double x1 = TWO_DIV_RANDOM_MAX * static_cast< double >( random( ) ) - 1;
				double x2 = TWO_DIV_RANDOM_MAX * static_cast< double >( random( ) ) - 1;
				double x3 = TWO_DIV_RANDOM_MAX * static_cast< double >( random( ) ) - 1;
				while( x1 * x1 + x2 * x2 + x3 * x3 > 1 )
					{
						x1 = TWO_DIV_RANDOM_MAX * static_cast< double >( random( ) ) - 1;
						x2 = TWO_DIV_RANDOM_MAX * static_cast< double >( random( ) ) - 1;
						x3 = TWO_DIV_RANDOM_MAX * static_cast< double >( random( ) ) - 1;
					}

				Kernel::ContRef cont = evalState->cont_;
				cont->takeValue( Kernel::ValueRef( new Lang::FloatTriple( x1, x2, x3 ) ),
												 evalState );
			}
		};

    class Core_gensym : public Lang::CoreFunction
    {
    public:
      Core_gensym( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name )
        : CoreFunction( ns, name, new Kernel::EvaluatedFormals( Ast::FileID::build_internal( name ), true ) )
      {
        formals_->appendCoreStateFormal( "state" );
      }
      virtual void call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
      {
        args.applyDefaults( callLoc );

        typedef Kernel::WarmTime StateType;
        Helpers::down_cast_CoreState< StateType >( id_, args, 0, callLoc ); /* Ignore result, just make sure that a time state was given. */

        Kernel::ContRef cont = evalState->cont_;
        cont->takeValue( Kernel::ValueRef( new Lang::Symbol( ) ),
                         evalState );
      }
    };
  }
}


RefCountPtr< const Lang::CoreFunction > Lang::THE_FUNCTION_ABS( new Lang::Core_abs( Lang::THE_NAMESPACE_Shapes_Numeric_Math, "abs" ) );

void
Kernel::registerCore_elem( Kernel::Environment * env )
{
	env->initDefineCoreFunction( new Lang::Core_nonNegativeModulo( Lang::THE_NAMESPACE_Shapes_Numeric_Math, "modulo" ) );
	env->initDefineCoreFunction( new Lang::Core_denominatorSignModulo( Lang::THE_NAMESPACE_Shapes_Numeric_Math, "mod" ) );
	env->initDefineCoreFunction( new Lang::Core_numeratorSignModulo( Lang::THE_NAMESPACE_Shapes_Numeric_Math, "rem" ) );
	env->initDefineCoreFunction( new Lang::Core_ceil( Lang::THE_NAMESPACE_Shapes_Numeric_Math, "ceil" ) );
	env->initDefineCoreFunction( new Lang::Core_floor( Lang::THE_NAMESPACE_Shapes_Numeric_Math, "floor" ) );
	env->initDefineCoreFunction( new Lang::Core_round( Lang::THE_NAMESPACE_Shapes_Numeric_Math, "round" ) );
	env->initDefineCoreFunction( new Lang::Core_cos( Lang::THE_NAMESPACE_Shapes_Numeric_Math, "cos" ) );
	env->initDefineCoreFunction( new Lang::Core_sin( Lang::THE_NAMESPACE_Shapes_Numeric_Math, "sin" ) );
	env->initDefineCoreFunction( new Lang::Core_tan( Lang::THE_NAMESPACE_Shapes_Numeric_Math, "tan" ) );
	env->initDefineCoreFunction( new Lang::Core_cot( Lang::THE_NAMESPACE_Shapes_Numeric_Math, "cot" ) );
	env->initDefineCoreFunction( new Lang::Core_arccos( Lang::THE_NAMESPACE_Shapes_Numeric_Math, "arccos" ) );
	env->initDefineCoreFunction( new Lang::Core_arcsin( Lang::THE_NAMESPACE_Shapes_Numeric_Math, "arcsin" ) );
	env->initDefineCoreFunction( new Lang::Core_arctan( Lang::THE_NAMESPACE_Shapes_Numeric_Math, "arctan" ) );
	env->initDefineCoreFunction( new Lang::Core_pow( Lang::THE_NAMESPACE_Shapes_Numeric_Math, "pow" ) );
	env->initDefineCoreFunction( new Lang::Core_exp( Lang::THE_NAMESPACE_Shapes_Numeric_Math, "exp" ) );
	env->initDefineCoreFunction( new Lang::Core_log( Lang::THE_NAMESPACE_Shapes_Numeric_Math, "log" ) );
	env->initDefineCoreFunction( new Lang::Core_log10( Lang::THE_NAMESPACE_Shapes_Numeric_Math, "log10" ) );
	env->initDefineCoreFunction( new Lang::Core_min( Lang::THE_NAMESPACE_Shapes_Numeric_Math, "min" ) );
	env->initDefineCoreFunction( new Lang::Core_max( Lang::THE_NAMESPACE_Shapes_Numeric_Math, "max" ) );
	env->initDefineCoreFunction( new Lang::Core_sqrt( Lang::THE_NAMESPACE_Shapes_Numeric_Math, "sqrt" ) );
	env->initDefineCoreFunction( Lang::THE_FUNCTION_ABS );
	env->initDefineCoreFunction( new Lang::Core_angle( Lang::THE_NAMESPACE_Shapes_Geometry, "angle" ) );
	env->initDefineCoreFunction( new Lang::Core_dir( Lang::THE_NAMESPACE_Shapes_Geometry, "dir" ) );
	env->initDefineCoreFunction( new Lang::Core_normalized( Lang::THE_NAMESPACE_Shapes_Geometry, "normalized" ) );
	env->initDefineCoreFunction( new Lang::Core_cross( Lang::THE_NAMESPACE_Shapes_Geometry3D, "cross" ) );
	env->initDefineCoreFunction( new Lang::Core_orthogonal( Lang::THE_NAMESPACE_Shapes_Geometry, "orthogonal" ) );
	env->initDefineCoreFunction( new Lang::Core_orthogonal3D( Lang::THE_NAMESPACE_Shapes_Geometry3D, "orthogonal" ) );

	env->initDefineCoreFunction( new Lang::Core_randomBall1D( Lang::THE_NAMESPACE_Shapes_Numeric_Random, "ball1D" ) );
	env->initDefineCoreFunction( new Lang::Core_randomBall2D( Lang::THE_NAMESPACE_Shapes_Numeric_Random, "ball2D" ) );
	env->initDefineCoreFunction( new Lang::Core_randomBall3D( Lang::THE_NAMESPACE_Shapes_Numeric_Random, "ball3D" ) );

	env->initDefineCoreFunction( new Lang::Core_gensym( Lang::THE_NAMESPACE_Shapes, "gensym" ) );
}
