/* This file is part of Shapes.
 *
 * Shapes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Shapes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2008, 2013 Henrik Tidefelt
 */

#pragma once

#include <list>
#include <iostream>
#include <stack>
#include <set>

#include "ptrowner.h"
#include "refcount.h"
#include "pdfstructure.h"
#include "shapesvalue.h"
#include "environment.h"
#include "charptrless.h"

namespace Shapes
{
	namespace Lang
	{

		class SingleList : public Lang::NoOperatorOverloadValue
		{
		public:
			SingleList( );
			virtual ~SingleList( );
			virtual bool isNull( ) const = 0;
			virtual bool isForced( ) const = 0;

			/* In order to be able to use the same method template as ConsPair, we support that the <op> argument is additionally passed as a VariableHandle. */
			inline void foldl( Kernel::EvalState * evalState, const RefCountPtr< const Lang::Function > & op, const Kernel::VariableHandle & opHandle, const Kernel::VariableHandle & nullResult, const Ast::SourceLocation & callLoc ) const
			{
				this->foldl( evalState, op, nullResult, callLoc );
			}
			inline void foldr( Kernel::EvalState * evalState, const RefCountPtr< const Lang::Function > & op, const Kernel::VariableHandle & opHandle, const Kernel::VariableHandle & nullResult, const Ast::SourceLocation & callLoc ) const
			{
				this->foldr( evalState, op, nullResult, callLoc );
			}
			inline void foldsl( Kernel::EvalState * evalState, const RefCountPtr< const Lang::Function > & op, const Kernel::VariableHandle & opHandle, const Kernel::VariableHandle & nullResult, Kernel::StateHandle state, const Ast::SourceLocation & callLoc ) const
			{
				this->foldsl( evalState, op, nullResult, state, callLoc );
			}
			inline void foldsr( Kernel::EvalState * evalState, const RefCountPtr< const Lang::Function > & op, const Kernel::VariableHandle & opHandle, const Kernel::VariableHandle & nullResult, Kernel::StateHandle state, const Ast::SourceLocation & callLoc ) const
			{
				this->foldsr( evalState, op, nullResult, state, callLoc );
			}

			virtual void foldl( Kernel::EvalState * evalState, const RefCountPtr< const Lang::Function > & op, const Kernel::VariableHandle & nullResult, const Ast::SourceLocation & callLoc ) const = 0;
			virtual void foldr( Kernel::EvalState * evalState, const RefCountPtr< const Lang::Function > & op, const Kernel::VariableHandle & nullResult, const Ast::SourceLocation & callLoc ) const = 0;
			virtual void foldsl( Kernel::EvalState * evalState, const RefCountPtr< const Lang::Function > & op, const Kernel::VariableHandle & nullResult, Kernel::StateHandle state, const Ast::SourceLocation & callLoc ) const = 0;
			virtual void foldsr( Kernel::EvalState * evalState, const RefCountPtr< const Lang::Function > & op, const Kernel::VariableHandle & nullResult, Kernel::StateHandle state, const Ast::SourceLocation & callLoc ) const = 0;
			virtual Kernel::VariableHandle getField( const char * fieldID, const RefCountPtr< const Lang::Value > & selfRef ) const;
			TYPEINFODECL;
		};

		class SingleListPair : public Lang::SingleList
		{
		public:
			/* The data is provided public because it is used in function application
			 */
			Kernel::VariableHandle car_;
			RefCountPtr< const Lang::SingleList > cdr_;

		private:
			bool forced_;

		public:
			SingleListPair( const Kernel::VariableHandle & car, const RefCountPtr< const Lang::SingleList > & cdr );
			virtual ~SingleListPair( );
			virtual void show( std::ostream & os ) const;
			virtual bool isNull( ) const;
			virtual bool isForced( ) const;
			virtual void foldl( Kernel::EvalState * evalState, const RefCountPtr< const Lang::Function > & op, const Kernel::VariableHandle & nullResult, const Ast::SourceLocation & callLoc ) const;
			virtual void foldr( Kernel::EvalState * evalState, const RefCountPtr< const Lang::Function > & op, const Kernel::VariableHandle & nullResult, const Ast::SourceLocation & callLoc ) const;
			virtual void foldsl( Kernel::EvalState * evalState, const RefCountPtr< const Lang::Function > & op, const Kernel::VariableHandle & nullResult, Kernel::StateHandle state, const Ast::SourceLocation & callLoc ) const;
			virtual void foldsr( Kernel::EvalState * evalState, const RefCountPtr< const Lang::Function > & op, const Kernel::VariableHandle & nullResult, Kernel::StateHandle state, const Ast::SourceLocation & callLoc ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
			virtual Kernel::VariableHandle getField( const char * fieldID, const RefCountPtr< const Lang::Value > & selfRef ) const;
		};

		class SingleListNull : public Lang::SingleList
		{
		public:
			SingleListNull( );
			virtual ~SingleListNull( );
			virtual void show( std::ostream & os ) const;
			virtual bool isNull( ) const;
			virtual bool isForced( ) const;
			virtual void foldl( Kernel::EvalState * evalState, const RefCountPtr< const Lang::Function > & op, const Kernel::VariableHandle & nullResult, const Ast::SourceLocation & callLoc ) const;
			virtual void foldr( Kernel::EvalState * evalState, const RefCountPtr< const Lang::Function > & op, const Kernel::VariableHandle & nullResult, const Ast::SourceLocation & callLoc ) const;
			virtual void foldsl( Kernel::EvalState * evalState, const RefCountPtr< const Lang::Function > & op, const Kernel::VariableHandle & nullResult, Kernel::StateHandle state, const Ast::SourceLocation & callLoc ) const;
			virtual void foldsr( Kernel::EvalState * evalState, const RefCountPtr< const Lang::Function > & op, const Kernel::VariableHandle & nullResult, Kernel::StateHandle state, const Ast::SourceLocation & callLoc ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked ){ };
			virtual Kernel::VariableHandle getField( const char * fieldID, const RefCountPtr< const Lang::Value > & selfRef ) const;
			TYPEINFODECL;
		};

		/* Don't forget that there is also the template SingleListRange that implements Lang::SingleList, see singlelistrange.h!
		 */

		class Structure : public Lang::NoOperatorOverloadValue
		{
			bool argListOwner_;
		public:
			/* The data is provided public because it is used in function application
			 */
			const Ast::ArgListExprs * argList_;
			RefCountPtr< const Lang::SingleList > values_;
			Structure( const Ast::ArgListExprs * argList, const RefCountPtr< const Lang::SingleList > & values, bool argListOwner = false );
			virtual ~Structure( );
			virtual Kernel::VariableHandle getField( const char * fieldID, const RefCountPtr< const Lang::Value > & selfRef ) const;
			Kernel::VariableHandle getPosition( size_t pos, const RefCountPtr< const Lang::Value > & selfRef ) const;
			size_t valueCount( ) const; /* Number of elements in values_ (deduced in constant time from argList_). */
			RefCountPtr< const Lang::Structure > getSink( size_t consumedArguments ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
			TYPEINFODECL;
		};

		class ConsPair : public Lang::NoOperatorOverloadValue
		{
			Kernel::VariableHandle car_;
			Kernel::VariableHandle cdr_;
		public:
			ConsPair( const Kernel::VariableHandle & car, const Kernel::VariableHandle & cdr );
			virtual ~ConsPair( );
			const Kernel::VariableHandle & car( ) const { return car_; }
			const Kernel::VariableHandle & cdr( ) const { return cdr_; }
			virtual void show( std::ostream & os ) const;
			void foldl( Kernel::EvalState * evalState, const RefCountPtr< const Lang::Function > & op, const Kernel::VariableHandle & opHandle, const Kernel::VariableHandle & nullResult, const Ast::SourceLocation & callLoc ) const;
			void foldr( Kernel::EvalState * evalState, const RefCountPtr< const Lang::Function > & op, const Kernel::VariableHandle & opHandle, const Kernel::VariableHandle & nullResult, const Ast::SourceLocation & callLoc ) const;
			void foldsl( Kernel::EvalState * evalState, const RefCountPtr< const Lang::Function > & op, const Kernel::VariableHandle & opHandle, const Kernel::VariableHandle & nullResult, Kernel::StateHandle state, const Ast::SourceLocation & callLoc ) const;
			void foldsr( Kernel::EvalState * evalState, const RefCountPtr< const Lang::Function > & op, const Kernel::VariableHandle & opHandle, const Kernel::VariableHandle & nullResult, Kernel::StateHandle state, const Ast::SourceLocation & callLoc ) const;
			virtual Kernel::VariableHandle getField( const char * fieldID, const RefCountPtr< const Lang::Value > & selfRef ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
			TYPEINFODECL;
		};

	}

	namespace Kernel
	{
		class StructureFactory
		{
			const Ast::ArgListExprs * argList_;
			std::map< const char *, Kernel::VariableHandle, charPtrLess > values_;
		private:
			void init( const std::list< const char * > & fields );
		public:
			StructureFactory( const std::list< const char * > & fields );
			StructureFactory( const char * field1 );
			StructureFactory( const char * field1, const char * field2 );
			StructureFactory( const char * field1, const char * field2, const char * field3 );
			StructureFactory( const char * field1, const char * field2, const char * field3, const char * field4 );
			void clear( );
			void set( const char * field, const Kernel::VariableHandle & value );
			RefCountPtr< const Lang::Structure > build( );
		};

		class UnnamedStructureFactory
		{
			mutable std::map< size_t, const Ast::ArgListExprs * > argLists_;
		public:
			UnnamedStructureFactory( );
			RefCountPtr< const Lang::Structure > build( const RefCountPtr< const Lang::SingleList > & values ) const;
		};
	}

	namespace Helpers
	{
		RefCountPtr< const Lang::SingleList > SingleList_cons( Lang::Value * car, const RefCountPtr< const Lang::SingleList > & cdr );
		RefCountPtr< const Lang::SingleList > SingleList_cons( const RefCountPtr< const Lang::Value > & car, const RefCountPtr< const Lang::SingleList > & cdr );
	}
}
