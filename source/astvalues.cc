/* This file is part of Shapes.
 *
 * Shapes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Shapes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2008, 2014 Henrik Tidefelt
 */

#include "astvalues.h"
#include "shapesexceptions.h"
#include "shapescore.h"
#include "astfun.h"
#include "consts.h"
#include "globals.h"
#include "continuations.h"

using namespace Shapes;


Ast::Constant::Constant( const Ast::SourceLocation & loc, const Kernel::VariableHandle & val )
	: Ast::Expression( loc ), val_( val )
{ }

Ast::Constant::Constant( const Ast::SourceLocation & loc, RefCountPtr< const Lang::Value > val )
	: Ast::Expression( loc ), val_( Kernel::VariableHandle( new Kernel::Variable( val ) ) )
{ }

Ast::Constant::Constant( const Ast::SourceLocation & loc, const Lang::Value * val )
	: Ast::Expression( loc ), val_( Helpers::newValHandle( val ) )
{ }

Ast::Constant::~Constant( )
{ }

void
Ast::Constant::analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst )
{
	/* Nothing to do! */
}

void
Ast::Constant::eval( Kernel::EvalState * evalState ) const
{
	Kernel::ContRef cont = evalState->cont_;
	cont->takeHandle( val_,
										evalState );
}


Ast::PolarHandle2DExpr::PolarHandle2DExpr( const Ast::SourceLocation & loc, Ast::Expression * rExpr, Ast::Expression * aExpr )
	: Ast::Expression( loc ), rExpr_( rExpr ), aExpr_( aExpr )
{ }

Ast::PolarHandle2DExpr::~PolarHandle2DExpr( )
{
	delete rExpr_;
	delete aExpr_;
}

void
Ast::PolarHandle2DExpr::analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst )
{
	rExpr_->analyze( this, env, freeStatesDst );
	aExpr_->analyze( this, env, freeStatesDst );
}

void
Ast::PolarHandle2DExpr::eval( Kernel::EvalState * evalState ) const
{
	Kernel::ContRef cont = evalState->cont_;
	evalState->cont_ = Kernel::ContRef
		( new Kernel::PolarHandle2DCont( aExpr_->loc( ),
																		 RefCountPtr< Kernel::PolarHandlePromise >( new Kernel::PolarHandleTruePromise( new Kernel::Thunk( evalState->env_, evalState->dyn_, rExpr_ ) ) ),
																		 cont ) );
	evalState->expr_ = aExpr_;
}

Kernel::PolarHandle2DCont::PolarHandle2DCont( const Ast::SourceLocation & traceLoc, const RefCountPtr< Kernel::PolarHandlePromise > & rPromise, const Kernel::ContRef & cont )
	: Kernel::Continuation( traceLoc ), rPromise_( rPromise ), cont_( cont )
{ }

Kernel::PolarHandle2DCont::~PolarHandle2DCont( )
{ }

void
Kernel::PolarHandle2DCont::takeValue( const RefCountPtr< const Lang::Value > & val, Kernel::EvalState * evalState, bool dummy ) const
{
	cont_->takeValue( Kernel::ValueRef( new Lang::PolarHandle2D( rPromise_,
																															 Helpers::down_cast_ContinuationArgument< const Lang::Float >( val, this )->val_ ) ),
										evalState );
}

Kernel::ContRef
Kernel::PolarHandle2DCont::up( ) const
{
	return cont_;
}

RefCountPtr< const char >
Kernel::PolarHandle2DCont::description( ) const
{
	return strrefdup( "( <> ^ <> ) angle" );
}

void
Kernel::PolarHandle2DCont::gcMark( Kernel::GCMarkedSet & marked )
{
	cont_->gcMark( marked );
}


Ast::PolarHandle2DExprFree_a::PolarHandle2DExprFree_a( const Ast::SourceLocation & loc, Ast::Expression * rExpr )
	: Ast::Expression( loc ), rExpr_( rExpr )
{ }

Ast::PolarHandle2DExprFree_a::~PolarHandle2DExprFree_a( )
{
	delete rExpr_;
}

void
Ast::PolarHandle2DExprFree_a::analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst )
{
	rExpr_->analyze( this, env, freeStatesDst );
}

void
Ast::PolarHandle2DExprFree_a::eval( Kernel::EvalState * evalState ) const
{
	Kernel::ContRef cont = evalState->cont_;
	cont->takeValue( Kernel::ValueRef( new Lang::PolarHandle2DFree_a( RefCountPtr< Kernel::PolarHandlePromise >( new Kernel::PolarHandleTruePromise( new Kernel::Thunk( evalState->env_, evalState->dyn_, rExpr_ ) ) ) ) ),
									 evalState );
}


Ast::SpanLastExpr::SpanLastExpr( const Ast::SourceLocation & loc )
	: Ast::Expression( loc )
{ }

Ast::SpanLastExpr::~SpanLastExpr( )
{ }

void
Ast::SpanLastExpr::analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst )
{
	/* Nothing to do! */
}

void
Ast::SpanLastExpr::eval( Kernel::EvalState * evalState ) const
{
	Kernel::ContRef cont = evalState->cont_;
	cont->takeValue( evalState->dyn_->getSpanLast( ),
									 evalState );
}


Ast::EmptyExpression::EmptyExpression( const Ast::SourceLocation & loc )
	: Ast::Expression( loc )
{
	immediate_ = true;
}

Ast::EmptyExpression::~EmptyExpression( )
{ }

void
Ast::EmptyExpression::analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst )
{
	/* Nothing to do! */
}

void
Ast::EmptyExpression::eval( Kernel::EvalState * evalState ) const
{
	Kernel::ContRef cont = evalState->cont_;
	cont->takeHandle( Kernel::THE_VOID_VARIABLE,
										evalState );
}


void
Helpers::relax( const RefCountPtr< const Lang::Value > & val, Kernel::EvalState * evalState )
{
	/* Piggy-back on the mechanism for the <end> expression. */

	static Ast::SourceLocation internalLoc( Ast::FileID::build_internal( "< core internal helper 'relax' >" ) );
	static Ast::SpanLastExpr spanLast( internalLoc );

	evalState->dyn_ = Kernel::PassedDyn( new Kernel::DynamicEnvironment( evalState->dyn_, val ) );
	evalState->expr_ = & spanLast;
}
