/* This file is part of Shapes.
 *
 * Shapes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Shapes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2008 Henrik Tidefelt
 */

#include "mathematicarepresentation.h"

#include <sstream>

using namespace Shapes;


std::string
Helpers::mathematicaFormat( size_t n1, size_t n2, size_t tda, const double * data )
{
	std::ostringstream os;
	os << "{" ;
	for( const double * row = data; row != data + n1 * tda; row += tda )
		{
			if( row != data )
				{
					os << "," ;
				}
			os << "{" ;
			for( const double * src = row; src != row + n2; ++src )
				{
					if( src != row )
						{
							os << "," ;
						}
					os << *src ;
				}
			os << "}" ;
		}
	os << "}" ;
	return os.str( );
}

std::string
Helpers::mathematicaFormatTransposed( size_t n1, size_t n2, size_t tda, const double * data )
{
	std::ostringstream os;
	os << "{" ;
	for( const double * row = data; row != data + n2; ++row )
		{
			if( row != data )
				{
					os << "," ;
				}
			os << "{" ;
			for( const double * src = row; src != row + n1 * tda; src += tda )
				{
					if( src != row )
						{
							os << "," ;
						}
					os << *src ;
				}
			os << "}" ;
		}
	os << "}" ;
	return os.str( );
}

std::string
Helpers::mathematicaFormat( size_t n, size_t stride, const double ** begin, const double ** end )
{
	std::ostringstream os;
	os << "{" ;
	for( const double ** row = begin; row != end; ++row )
		{
			if( row != begin )
				{
					os << "," ;
				}
			os << "{" ;
			for( const double * src = *row; src != *row + n * stride; src += stride )
				{
					if( src != *row )
						{
							os << "," ;
						}
					os << *src ;
				}
			os << "}" ;
		}
	os << "}" ;
	return os.str( );
}

std::string
Helpers::mathematicaFormat( size_t n, size_t stride, const double * data )
{
	std::ostringstream os;
	os << "{" ;
	for( const double * src = data; src != data + n * stride; src += stride )
		{
			if( src != data )
				{
					os << "," ;
				}
			os << *src ;
		}
	os << "}" ;
	return os.str( );
}

std::string
Helpers::mathematicaFormat( const gsl_vector & x )
{
	return Helpers::mathematicaFormat( x.size, x.stride, x.data );
}

std::string
Helpers::mathematicaFormat( const gsl_matrix & x, bool transpose )
{
	if( transpose )
		{
			return Helpers::mathematicaFormatTransposed( x.size1, x.size2, x.tda, x.data );
		}
	else
		{
			return Helpers::mathematicaFormat( x.size1, x.size2, x.tda, x.data );
		}
}
