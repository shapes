/* This file is part of Shapes.
 *
 * Shapes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Shapes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2008, 2014 Henrik Tidefelt
 */

#pragma once

#include "Shapes_Lang_decls.h"
#include "Shapes_Kernel_decls.h"
#include "Shapes_Ast_decls.h"

#include "shapesvalue.h"
#include "ptrowner.h"
#include "refcount.h"
#include "pdfstructure.h"
#include "environment.h"
#include "charptrless.h"
#include "consts.h"
#include "elementarytypes.h"
#include "elementarycoords.h"

#include <list>
#include <iostream>
#include <stack>
#include <set>
#include <gsl/gsl_matrix.h>


#define CHECK_ARITY( argsVar, arity, idExpr )				\
	if( argsVar.size( ) != ARITY )\
		{\
			throw Exceptions::CoreArityMismatch( idExpr, arity, argsVar.size( ) );\
		}



namespace Shapes
{

	namespace Kernel
	{

		class Arguments
		{
			const Kernel::EvaluatedFormals * formals_;
			Environment::ValueVector variables_;
			std::vector< const Ast::Node * > locations_;
			size_t dst_;
			bool hasSink_;
			// Putting dstEnd_ after hasSink_ makes initialization more convenient.
			size_t dstEnd_; // This is one less than there are variables if there is a sink.
			bool isSink_; // This is used by functions in the core.

			Ast::ArgListExprs * sinkArgList_; // If null, there is no sink.
			RefCountPtr< const Lang::SingleList > sinkValues_;

			Environment::StateVector states_;	 // This type must match that used in Environment
			std::vector< const Ast::Node * > stateLocations_;
			size_t stateDst_;

			Kernel::StateHandle mutatorSelf_;

		public:
			Arguments( const Kernel::EvaluatedFormals * formals );
			~Arguments( );

			Kernel::Arguments clone( ) const;

			void addOrderedArgument( const Kernel::VariableHandle & arg, Ast::Expression * loc );
			void addNamedArgument( const char * id, const Kernel::VariableHandle & arg, Ast::Expression * loc );

			void addOrderedState( const Kernel::StateHandle & state, Ast::Node * loc );
			void addNamedState( const char * id, const Kernel::StateHandle & state, Ast::Node * loc );

			void applyDefaults( const Ast::SourceLocation & callLoc );

			Kernel::VariableHandle & getHandle( size_t i );
			RefCountPtr< const Lang::Value > & getValue( size_t i );
			const Ast::SourceLocation & getLoc( size_t i ) const;
			const Ast::Node * getNode( size_t i ) const;
			Kernel::Thunk * getThunk( size_t i );												//	This funciton returns a newly created copy!
			bool isSlot( size_t i ) const;

			size_t size( ) const;
			bool empty( ) const;

			Kernel::StateHandle getState( size_t i );
			const Ast::SourceLocation & getStateLoc( size_t i ) const;

			void setMutatorSelf( Kernel::StateHandle mutatorSelf );
			Kernel::StateHandle getMutatorSelf( );

			void gcMark( Kernel::GCMarkedSet & marked );

			Environment::ValueVector getVariables( ); // This function should only be called when setting up a new environment
			Environment::StateVector getStates( ); // This function should only be called when setting up a new environment
		};

		class EvaluatedFormals
		{
			bool selectiveForcing_;
			bool forceAll_;
		public:
			Kernel::Formals * formals_;										 /* it would have been const if it was not for appendEvaluatedFormal */
			std::vector< Kernel::VariableHandle > defaults_;
			std::vector< const Ast::Node * > locations_;
			bool isSink_;

			EvaluatedFormals( Kernel::Formals * _formals );
			EvaluatedFormals( const Ast::FileID * locationFileID );
			EvaluatedFormals( const Ast::FileID * locationFileID, bool _forceAll );
			~EvaluatedFormals( );

			void appendEvaluatedFormal( const char * id, const Kernel::VariableHandle & defaultVal, const Ast::Node * loc, bool force );
			void appendEvaluatedFormal( const char * id, const Kernel::VariableHandle & defaultVal, const Ast::Node * loc );
			void appendEvaluatedCoreFormal( const char * id, const Kernel::VariableHandle & defaultVal, bool force );
			void appendEvaluatedCoreFormal( const char * id, const Kernel::VariableHandle & defaultVal );
			void appendCoreStateFormal( const char * id );

			RefCountPtr< Kernel::CallContInfo > newCallContInfo( const Ast::ArgListExprs * argList, const Kernel::EvalState & evalState ) const;
			RefCountPtr< Kernel::CallContInfo > newCallContInfo( const Ast::ArgListExprs * argList, const Kernel::EvalState & evalState, const Kernel::Arguments & curryArgs ) const;

			void gcMark( Kernel::GCMarkedSet & marked );
		};

		void VectorFunction_register_methods( Lang::SystemFinalClass * dstClass ); /* Implemented in containertypes.cc for convenience.  */
	}


	namespace Helpers
	{

		template< class T >
		RefCountPtr< T >
		down_cast_CoreArgument( const Ast::PlacedIdentifier & calleeId, Kernel::Arguments & args, size_t i, const Ast::SourceLocation & callLoc, bool voidIsNull = false )
		{
			RefCountPtr< const Lang::Value > val = args.getValue( i );
			RefCountPtr< T > res = val.down_cast< T >( );
			if( res == NullPtr< T >( ) )
				{
					if( ! voidIsNull ||
							dynamic_cast< const Lang::Void * >( val.getPtr( ) ) == 0 )
						{
							throw Exceptions::CoreTypeMismatch( callLoc, calleeId, args, i, T::staticTypeName( ) );
						}
				}
			return res;
		}

		template< class T >
		RefCountPtr< T >
		down_cast_CoreArgument( const RefCountPtr< const Interaction::CoreLocation > coreLoc, Kernel::Arguments & args, size_t i, const Ast::SourceLocation & callLoc, bool voidIsNull = false )
		{
			RefCountPtr< const Lang::Value > val = args.getValue( i );
			RefCountPtr< T > res = val.down_cast< T >( );
			if( res == NullPtr< T >( ) )
				{
					if( ! voidIsNull ||
							dynamic_cast< const Lang::Void * >( val.getPtr( ) ) == 0 )
						{
							throw Exceptions::CoreTypeMismatch( callLoc, coreLoc, args, i, T::staticTypeName( ) );
						}
				}
			return res;
		}

		template< class T >
		RefCountPtr< T >
		down_cast_SyntaxArgument( const char * syntax, Kernel::Arguments & args, size_t i, const Ast::SourceLocation & callLoc, bool voidIsNull = false )
		{
			RefCountPtr< const Lang::Value > val = args.getValue( i );
			RefCountPtr< T > res = val.down_cast< T >( );
			if( res == NullPtr< T >( ) )
				{
					if( ! voidIsNull ||
							dynamic_cast< const Lang::Void * >( val.getPtr( ) ) == 0 )
						{
							throw Exceptions::CoreTypeMismatch( callLoc, new Interaction::CharPtrLocation( syntax ), args.getLoc( i ), val->getTypeName( ), T::staticTypeName( ) );
						}
				}
			return res;
		}

		template< class T >
		RefCountPtr< T >
		down_cast_MutatorArgument( const Kernel::State * state, const char * name, Kernel::Arguments & args, size_t i, const Ast::SourceLocation & callLoc, bool voidIsNull = false )
		{
			RefCountPtr< const Lang::Value > val = args.getValue( i );
			RefCountPtr< T > res = val.down_cast< T >( );
			if( res == NullPtr< T >( ) )
				{
					if( ! voidIsNull ||
							dynamic_cast< const Lang::Void * >( val.getPtr( ) ) == 0 )
						{
							throw Exceptions::CoreTypeMismatch( callLoc, new Interaction::MutatorLocation( state, name ), args, i, T::staticTypeName( ) );
						}
				}
			return res;
		}

		template< class T >
		RefCountPtr< T >
		down_cast_StructureContinuationArgument( const char * continuationName, Kernel::Arguments & args, size_t i, const Ast::SourceLocation & callLoc, bool voidIsNull = false )
		{
			RefCountPtr< const Lang::Value > val = args.getValue( i );
			RefCountPtr< T > res = val.down_cast< T >( );
			if( res == NullPtr< T >( ) )
				{
					if( ! voidIsNull ||
							dynamic_cast< const Lang::Void * >( val.getPtr( ) ) == 0 )
						{
							throw Exceptions::CoreTypeMismatch( callLoc, new Interaction::CharPtrLocation( continuationName ), args, i, T::staticTypeName( ) );
						}
				}
			return res;
		}

		template< class T >
		T *
		down_cast_CoreState( const Ast::PlacedIdentifier & calleeId, Kernel::Arguments & args, size_t i, const Ast::SourceLocation & callLoc )
		{
			Kernel::StateHandle st = args.getState( i );
			T * res = dynamic_cast< T * >( st );
			if( res == NullPtr< T >( ) )
				{
					throw Exceptions::CoreStateTypeMismatch( callLoc, calleeId, args.getStateLoc( i ), st->getTypeName( ), T::staticTypeName( ) );
				}
			return res;
		}

		template< class T >
		RefCountPtr< T >
			down_cast_CoreDynamic( const Ast::PlacedIdentifier & calleeId, const Ast::PlacedIdentifier & id, const RefCountPtr< const Lang::Value > & val, const Ast::SourceLocation & callLoc )
			{
				RefCountPtr< T > res = val.down_cast< T >( );
				if( res == NullPtr< T >( ) )
					{
						throw Exceptions::CoreDynamicTypeMismatch( callLoc, calleeId, id, val->getTypeName( ), T::staticTypeName( ) );
					}
				return res;
			}

		template< class T >
		RefCountPtr< T >
		try_cast_CoreArgument( const RefCountPtr< const Lang::Value > & val, bool voidIsNull = false )
		{
			RefCountPtr< T > res = val.down_cast< T >( );
			if( res == NullPtr< T >( ) )
				{
					if( ! voidIsNull ||
							dynamic_cast< const Lang::Void * >( val.getPtr( ) ) == 0 )
						{
							throw NonLocalExit::NotThisType( );
						}
				}
			return res;
		}

		template< class T >
		T *
		try_cast_CoreState( Kernel::StateHandle st )
		{
			T * res = dynamic_cast< T * >( st );
			if( res == NullPtr< T >( ) )
				{
					throw NonLocalExit::NotThisType( );
				}
			return res;
		}

		template< class T >
		T *
		mutator_cast_self( Kernel::StateHandle st )
		{
			T * res = dynamic_cast< T * >( st );
			if( res == NullPtr< T >( ) )
				{
					throw Exceptions::InternalError( "Type of self state does not match mutator signature." );
				}
			return res;
		}

	}


	namespace Lang
	{

		class Transform2D : public Lang::Value
		{
		public:
			double xx_;
			double yx_;
			double xy_;
			double yy_;
			Concrete::Length xt_;
			Concrete::Length yt_;
			Transform2D( double xx, double yx, double xy, double yy,
									 Concrete::Length xt, Concrete::Length yt );
			Transform2D( const Lang::Transform2D & tf2, const Lang::Transform2D & tf1 );
			virtual ~Transform2D( );
			virtual Kernel::VariableHandle getField( const char * fieldID, const RefCountPtr< const Lang::Value > & selfRef ) const;
			Transform2D * clone( ) const;
			bool isIdentity( ) const;
			bool isTranslation( ) const;
			void write_gsl_matrix( gsl_matrix * matrix_2_2 ) const;
			void write_gsl_vector( gsl_vector * vec_2 ) const;
			void shipout( std::ostream & os ) const;
			void replaceBy( const Lang::Transform2D & newtf );     // to be used by text moveto commands
			void prependShift( const Concrete::Coords2D & d );     // to be used by text newline commands
			void prependXShift( const Concrete::Length & dx );     // to be used by text painting commands
			void prependYShift( const Concrete::Length & dy );     // to be used for text rise
			void prependXScale( double a );                        // to be used for text horizontal scaling
			TYPEINFODECL;
			virtual void gcMark( Kernel::GCMarkedSet & marked ){ };
			virtual void show( std::ostream & os ) const;
			DISPATCHDECL;
		};

		class Transform3D : public Lang::Value
		{
			static const int N = 3;
			mutable gsl_matrix * planeNormalTransformData_;
		public:
			double xx_;
			double yx_;
			double zx_;
			double xy_;
			double yy_;
			double zy_;
			double xz_;
			double yz_;
			double zz_;
			Concrete::Length xt_;
			Concrete::Length yt_;
			Concrete::Length zt_;
			Transform3D( double xx, double yx, double zx, double xy, double yy, double zy, double xz, double yz, double zz,
									 Concrete::Length xt, Concrete::Length yt, Concrete::Length zt );
			Transform3D( const gsl_matrix * matrix_3_3, const gsl_vector * vec_3 );
			Transform3D( const Lang::Transform3D & tf2, const Lang::Transform3D & tf1 );
			virtual ~Transform3D( );
			virtual Kernel::VariableHandle getField( const char * fieldID, const RefCountPtr< const Lang::Value > & selfRef ) const;
			Transform3D * clone( ) const;
			bool isIdentity( ) const;
			bool isTranslation( ) const;
			Concrete::UnitFloatTriple transformPlaneUnitNormal( const Concrete::UnitFloatTriple & n ) const;
			void write_gsl_matrix( gsl_matrix * matrix_3_3 ) const;
			void write_gsl_vector( gsl_vector * vec_3 ) const;
			TYPEINFODECL;
			virtual void gcMark( Kernel::GCMarkedSet & marked ){ };
			virtual void show( std::ostream & os ) const;
			DISPATCHDECL;
		};

		class Function : public Lang::Value
		{
		protected:
			Kernel::EvaluatedFormals * formals_;  /* the reason that this is not const is only to allow convenient setup */
			virtual void analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst );
		public:
			Function( Kernel::EvaluatedFormals * formals );
			virtual ~Function( );
			virtual void call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const = 0;
			virtual Kernel::ValueRef transformed( const Lang::Transform2D & tf, Kernel::ValueRef self ) const;
			virtual bool isTransforming( ) const = 0;
			virtual RefCountPtr< Kernel::CallContInfo > newCallContInfo( const Ast::ArgListExprs * argList, const Kernel::EvalState & evalState ) const;
			virtual RefCountPtr< Kernel::CallContInfo > newCallContInfo( const Ast::ArgListExprs * argList, const Kernel::EvalState & evalState, const Kernel::Arguments & curryArgs ) const;
			virtual Kernel::Arguments newCurriedArguments( ) const;
			virtual void call( Kernel::EvalState * evalState, const Kernel::ValueRef & arg1, const Ast::SourceLocation & callLoc ) const;
			virtual void call( Kernel::EvalState * evalState, const Kernel::ValueRef & arg1, const Kernel::ValueRef & arg2, const Ast::SourceLocation & callLoc ) const;
			virtual void call( const RefCountPtr< const Lang::Function > & selfRef, Kernel::EvalState * evalState, const Kernel::VariableHandle & arg1, const Ast::SourceLocation & callLoc ) const;
			virtual void call( const RefCountPtr< const Lang::Function > & selfRef, Kernel::EvalState * evalState, const Kernel::VariableHandle & arg1, const Kernel::VariableHandle & arg2, const Ast::SourceLocation & callLoc ) const;
			virtual void call( const RefCountPtr< const Lang::Function > & selfRef, Kernel::EvalState * evalState, const Kernel::VariableHandle & arg1, const Kernel::VariableHandle & arg2, Kernel::StateHandle state, const Ast::SourceLocation & callLoc ) const;

			void analyze( Ast::Node * parent, Ast::AnalysisEnvironment * env );

			TYPEINFODECL;
			DISPATCHDECL;
		private:
			static Ast::ArgListExprs * oneExprArgList;
			static Ast::ArgListExprs * twoExprsArgList;
		};

		class CuteFunction : public Lang::Function
		{
			RefCountPtr< const Lang::Function > callee_;
			Kernel::Arguments someArgs_;
		public:
			CuteFunction( RefCountPtr< const Lang::Function > _callee, const Kernel::Arguments & _someArgs );
			virtual ~CuteFunction( );
			virtual RefCountPtr< Kernel::CallContInfo > newCallContInfo( const Ast::ArgListExprs * argList, const Kernel::EvalState & evalState ) const;
			virtual RefCountPtr< Kernel::CallContInfo > newCallContInfo( const Ast::ArgListExprs * argList, const Kernel::EvalState & evalState, const Kernel::Arguments & curryArgs ) const;
			virtual Kernel::Arguments newCurriedArguments( ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
			virtual void show( std::ostream & os ) const;
			virtual void call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const;
			virtual bool isTransforming( ) const;
		};

		class ComposedFunction : public Lang::Function
		{
			RefCountPtr< const Lang::Function > second_;
			RefCountPtr< const Lang::Function > first_;
		public:
			ComposedFunction( const RefCountPtr< const Lang::Function > & _second, const RefCountPtr< const Lang::Function > & _first );
			virtual ~ComposedFunction( );
			virtual RefCountPtr< Kernel::CallContInfo > newCallContInfo( const Ast::ArgListExprs * argList, const Kernel::EvalState & evalState ) const;
			virtual RefCountPtr< Kernel::CallContInfo > newCallContInfo( const Ast::ArgListExprs * argList, const Kernel::EvalState & evalState, const Kernel::Arguments & curryArgs ) const;
			virtual Kernel::Arguments newCurriedArguments( ) const;
			virtual void call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const;
			virtual bool isTransforming( ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
			virtual void show( std::ostream & os ) const;
		};

		class Span : public Lang::Function
		{
			RefCountPtr< const Kernel::SpanThunks > thunks_;
		public:
			Span( const Kernel::VariableHandle & begin, const Kernel::VariableHandle & end, const Kernel::VariableHandle & step, const Kernel::VariableHandle & count );
			virtual ~Span( );
			virtual void gcMark( Kernel::GCMarkedSet & marked );
			virtual void show( std::ostream & os ) const;
			virtual void call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const;
			virtual bool isTransforming( ) const;
			static const RefCountPtr< const Interaction::CoreLocation > coreLoc;
			TYPEINFODECL;
		};

		class UserFunction : public Lang::Function
		{
			Ast::Expression * body_;
			Kernel::PassedEnv env_;
			Ast::FunctionMode functionMode_;
		public:
			UserFunction( Kernel::EvaluatedFormals * _formals, Ast::Expression * _body, Kernel::PassedEnv _env, const Ast::FunctionMode & _functionMode );
			virtual ~UserFunction( );
			virtual void call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const;
			virtual bool isTransforming( ) const;
			virtual void show( std::ostream & os ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
			Ast::Expression * body( ); /* Will typically require a const_cast to be callable. */
		};

		class TransformedFunction2D : public Lang::Function
		{
			Lang::Transform2D tf_;
			RefCountPtr< const Lang::Function > fun_;
		public:
			TransformedFunction2D( const Lang::Transform2D & _tf, const RefCountPtr< const Lang::Function > & _fun );
			virtual ~TransformedFunction2D( );
			virtual void gcMark( Kernel::GCMarkedSet & marked );
			virtual void show( std::ostream & os ) const;
			virtual void call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const;
			virtual bool isTransforming( ) const;
		};

		class VectorFunction : public Lang::Function
		{
		public:
			typedef std::vector< Kernel::ValueRef > vector_type;
		private:
			RefCountPtr< const vector_type > mem_;
			mutable RefCountPtr< const std::vector< double > > memNumeric_;
			mutable Kernel::VariableHandle list_;
			static const RefCountPtr< const Interaction::CoreLocation > coreLoc;
		public:
			VectorFunction( const std::vector< Kernel::ValueRef > * mem );
			virtual ~VectorFunction( );
			virtual Kernel::VariableHandle getField( const char * fieldID, const RefCountPtr< const Lang::Value > & selfRef ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
			static void gcMark( const RefCountPtr< const std::vector< Kernel::ValueRef > > & mem, Kernel::GCMarkedSet & marked );
			virtual void show( std::ostream & os ) const;
			virtual void call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const;
			virtual bool isTransforming( ) const;
			RefCountPtr< const vector_type > mem( ) const { return mem_; }
			RefCountPtr< const std::vector< double > > getNumeric( const Ast::SourceLocation & callLoc ) const;

			static void foldl( const RefCountPtr< const std::vector< Kernel::ValueRef > > & mem, Kernel::EvalState * evalState, const RefCountPtr< const Lang::Function > & op, const Kernel::VariableHandle & nullResult, vector_type::const_iterator i, const Ast::SourceLocation & callLoc );
			static void foldr( const RefCountPtr< const std::vector< Kernel::ValueRef > > & mem, Kernel::EvalState * evalState, const RefCountPtr< const Lang::Function > & op, const Kernel::VariableHandle & nullResult, vector_type::const_reverse_iterator i, const Ast::SourceLocation & callLoc );
			static void foldsl( const RefCountPtr< const std::vector< Kernel::ValueRef > > & mem, Kernel::EvalState * evalState, const RefCountPtr< const Lang::Function > & op, const Kernel::VariableHandle & nullResult, Kernel::StateHandle state, vector_type::const_iterator i, const Ast::SourceLocation & callLoc );
			static void foldsr( const RefCountPtr< const std::vector< Kernel::ValueRef > > & mem, Kernel::EvalState * evalState, const RefCountPtr< const Lang::Function > & op, const Kernel::VariableHandle & nullResult, Kernel::StateHandle state, vector_type::const_reverse_iterator i, const Ast::SourceLocation & callLoc );

			/* In order to be able to use the same method template as ConsPair, we support that the <op> argument is additionally passed as a VariableHandle. */
			inline void foldl( Kernel::EvalState * evalState, const RefCountPtr< const Lang::Function > & op, const Kernel::VariableHandle & opHandle, const Kernel::VariableHandle & nullResult, const Ast::SourceLocation & callLoc ) const
			{
				foldl( mem_, evalState, op, nullResult, mem_->begin( ), callLoc );
			}
			inline void foldr( Kernel::EvalState * evalState, const RefCountPtr< const Lang::Function > & op, const Kernel::VariableHandle & opHandle, const Kernel::VariableHandle & nullResult, const Ast::SourceLocation & callLoc ) const
			{
				foldr( mem_, evalState, op, nullResult, mem_->rbegin( ), callLoc );
			}
			inline void foldsl( Kernel::EvalState * evalState, const RefCountPtr< const Lang::Function > & op, const Kernel::VariableHandle & opHandle, const Kernel::VariableHandle & nullResult, Kernel::StateHandle state, const Ast::SourceLocation & callLoc ) const
			{
				foldsl( mem_, evalState, op, nullResult, state, mem_->begin( ), callLoc );
			}
			inline void foldsr( Kernel::EvalState * evalState, const RefCountPtr< const Lang::Function > & op, const Kernel::VariableHandle & opHandle, const Kernel::VariableHandle & nullResult, Kernel::StateHandle state, const Ast::SourceLocation & callLoc ) const
			{
				foldsr( mem_, evalState, op, nullResult, state, mem_->rbegin( ), callLoc );
			}

			TYPEINFODECL;
		private:
			void compute_list( ) const; /* This will modify mutable data! */
		};

		class ColorInterpolator : public Lang::Function
		{
		public:
			typedef std::vector< double > KeyContainer;
			typedef std::vector< Concrete::RGB > RGBContainer;
			typedef std::vector< Concrete::Gray > GrayContainer;
			typedef std::vector< Concrete::CMYK > CMYKContainer;
			enum ColorType { UNDEFINED, RGB, GRAY, CMYK };

		private:
			RefCountPtr< const KeyContainer > key_;
			RefCountPtr< const RGBContainer > RGBcolor_;
			RefCountPtr< const GrayContainer > graycolor_;
			RefCountPtr< const CMYKContainer > CMYKcolor_;
			ColorType colorType_;
			static const char * title;
			static const RefCountPtr< const Interaction::CoreLocation > coreLoc;

		public:
			ColorInterpolator( const RefCountPtr< KeyContainer > & key,
			                   const RefCountPtr< RGBContainer > & RGBcolor,
			                   const RefCountPtr< GrayContainer > & graycolor,
			                   const RefCountPtr< CMYKContainer > & CMYKcolor,
			                   ColorType colorType );
			virtual ~ColorInterpolator( );
			virtual Kernel::VariableHandle getField( const char * fieldID, const RefCountPtr< const Lang::Value > & selfRef ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
			virtual void show( std::ostream & os ) const;
			virtual void call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const;
			virtual bool isTransforming( ) const;

		private:
			template< class COLOR_TYPE, class COLOR_CONTAINER >
			void callHelper( Kernel::EvalState * evalState, const RefCountPtr< COLOR_CONTAINER > & colorContainer,
											 double key, KeyContainer::const_iterator keyHi ) const;

		};

		class BinaryOperatorFunction : public Lang::Function
		{
			Ast::BinaryInfixExpr * opExpr_;
			RefCountPtr< const Interaction::CoreLocation > coreLoc_;
		public:
			BinaryOperatorFunction( Ast::BinaryInfixExpr * opExpr, const char * coreSyntax );
			virtual ~BinaryOperatorFunction( );
			virtual void call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const;
			virtual bool isTransforming( ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked ){ };
			virtual void show( std::ostream & os ) const;
		};

		class UnaryOperatorFunction : public Lang::Function
		{
			Ast::UnaryExpr * opExpr_;
			RefCountPtr< const Interaction::CoreLocation > coreLoc_;
		public:
			UnaryOperatorFunction( Ast::UnaryExpr * opExpr, const char * coreSyntax );
			virtual ~UnaryOperatorFunction( );
			virtual void call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const;
			virtual bool isTransforming( ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked ){ };
			virtual void show( std::ostream & os ) const;
		};

	}
}
