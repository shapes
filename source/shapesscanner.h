/* This file is part of Shapes.
 *
 * Shapes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Shapes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2008, 2010, 2014, 2015 Henrik Tidefelt
 */

#pragma once

#include "sourcelocation.h"
#include "namespacedirectory.h"
#include "identifier.h"
#include "charptrless.h"
#include "elementarylength.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <stack>
#include <map>
#include <set>
#include <list>
#include <string>
#include <stdint.h>

#ifndef FLEXINT_H								// Else *FlexLexer will be defined twice
#	undef yyFlexLexer
#	define yyFlexLexer shapesFlexLexer
#	include <FlexLexer.h>
#endif


typedef std::pair< RefCountPtr< const Shapes::Ast::NamespacePath >, const Shapes::Ast::FileID * > NeededFile;
bool operator < ( const NeededFile & n1, const NeededFile & n2 );

class ShapesScanner : public shapesFlexLexer
{
	std::stack< yy_buffer_state * > stateStack_;
	std::stack< ::Shapes::Ast::SourceLocation * > locStack_;
	std::stack< size_t > pathCountStack_;
	std::stack< size_t > loadStackSizeStack_;
	std::stack< size_t > stateStackSizeStack_;
	unsigned int quoteDepth_;
	bool moreState_;
	int lastleng_;
	void more( );
	size_t bracketDepth_;
	RefCountPtr< const Shapes::Ast::SearchContext > searchContext_;
	std::stack< RefCountPtr< const Shapes::Ast::SearchContext > > searchContextStack_;
	size_t uniqueNamespaceNumber_;
	std::map< const char *, double, charPtrLess > angleUnits_;
	std::map< const char *, double, charPtrLess > lengthUnits_;
	const char * newUnitName_;
	Shapes::Ast::SourceLocation aliasLoc_;
	Shapes::Ast::PlacedIdentifier * treeNamespaceAlias_;
	std::set< NeededFile > neededFiles_;
	std::string sourceDir_;
	std::list< std::string > needSearchPath_;
	std::stack< size_t > namespaceLimits_;
	::Shapes::Ast::NamespaceLoader loader_;
	bool inPrelude_;
	std::list< std::pair< std::istream *, const Shapes::Ast::FileID * > > yyinQueue_;
	Shapes::Ast::LoadStack loadStack_;
	bool showFiles_;
	bool randSeedSet_;
	std::list< std::pair< char *, size_t > > dataStringChunks_;
	size_t dataStringTotalLength_;
	bool interactive_;

 public:
	ShapesScanner( );
	virtual ~ShapesScanner( );
	void setInteractive( bool interactive );
	void start( );
	void queueStream( std::istream * is, const Shapes::Ast::FileID * yyinFile );
	void setSourceDir( const std::string & sourceDir );
	void push_backNeedPath( const std::string & path );
	std::string searchFile( const std::string & filename, bool runtime = false ) const;
	std::string searchFile( const std::string & filename, struct stat * dstStat, bool runtime = false ) const;
	std::string searchNamespaceIndexFile( const RefCountPtr< const Shapes::Ast::NamespacePath > & path ) const;
	std::string needpathWithSuffix( const std::string & needpath, const std::string & suffix ) const;
	void setShowFiles( bool showFiles );
	virtual int yylex( );
	void doBeforeEachAction( );
	double lookupLengthUnitFactor( const char * name ) const;
	Shapes::Concrete::Length strtoLength( const char * str ) const;
  RefCountPtr< const char > newPrivateName( );
  RefCountPtr< const char > newEncapsulationName( );

 private:
	std::string currentNeedFile;
	size_t currentNeedPushCount;
	bool currentNeedIsNeed;
	bool currentNeedHasNamespace; /* File or namespace form of ##need? */
	void endOfLine( ); /* Use each time a token ending in a newline has been consumed. */
	void push_frontNeedPath( const std::string & path );
	void pop_frontNeedPath( );
	void doInclusion( );
	void rinseString( );
	void concatenateDataString( );
	uint32_t unicodeFromGlyphname( const char * name );

	/* Note that it is important that the identifier constructors below are not used outside the scanner,
	 * since only the scanner can know for sure that the current environment has not changed since the
	 * scanning of the identifier string.
	 */
	Shapes::Ast::NamespaceReference namespace_;
	size_t namespace_yyleng_;
	void setNamespace( char * str ); /* The contents of the given string will be destroyed after the call. */
	Shapes::Ast::Identifier * prependNamespace( const char * idstr ) const;
	Shapes::Ast::Identifier * simpleIdentifier( const char * idstr ) const;
	Shapes::Ast::PlacedIdentifier * placedIdentifier( const char * idstr ) const;
	RefCountPtr< const Shapes::Ast::NamespacePath > resolvedNamespace( ) const;
};
