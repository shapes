/* This file is part of Shapes.
 *
 * Shapes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Shapes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2014 Henrik Tidefelt
 */

#include "identifier.h"
#include "sourcelocation.h"
#include "consts.h"
#include "check.h"
#include "globals.h"
#include "environment.h"
#include "shapesexceptions.h"

using namespace Shapes;


const RefCountPtr< const Ast::NamespacePath > Ast::THE_EMPTY_NAMESPACE_PATH( new Ast::NamespacePath( ) );
const RefCountPtr< const Ast::SearchContext > Ast::THE_EMPTY_SEARCH_CONTEXT( new Ast::SearchContext( Ast::THE_EMPTY_NAMESPACE_PATH, RefCountPtr< const char >( NullPtr< const char >( ) ) ) );


int
Ast::compare( const Ast::NamespacePath & path1, const Ast::NamespacePath & path2 )
{
  if( path1.size( ) < path2.size( ) )
    return -1;
  if( path1.size( ) > path2.size( ) )
    return 1;

  Ast::NamespacePath::const_iterator i1 = path1.begin( );
  Ast::NamespacePath::const_iterator i2 = path2.begin( );
  Ast::NamespacePath::const_iterator end1 = path1.end( );
  for( ; i1 != end1; ++i1, ++i2 ){
    int cmp = strcmp( *i1, *i2 );
    if( cmp < 0 )
      return -1;
    if( cmp > 0 )
      return 1;
  }
  return 0;
}


//bool
//Ast::operator < ( const Ast::NamespacePath & path1, const Ast::NamespacePath & path2 )
//{
//  return compare( path1, path2 ) < 0;
//}


void
Ast::NamespaceReference::show( std::ostream & os, bool showAbsoluteMark ) const
{
  switch( base_ ){
  case ABSOLUTE:
    if( showAbsoluteMark )
      os << Interaction::NAMESPACE_SEPARATOR ;
    break;
  case LOCAL:
    os << Interaction::ENCAPSULATION_MARK ;
    break;
  case RELATIVE:
    break;
  }

  Ast::NamespacePath::const_iterator i = path_->begin( );
  for( ; i != path_->end( ) && Ast::SearchContext::isEncapsulationName( *i ); ++i )
    ;
  if( i != path_->end( ) ){
    os << *i ;
    for( ++i; i != path_->end( ); ++i ){
      if( ! Ast::SearchContext::isEncapsulationName( *i ) )
        os << Interaction::NAMESPACE_SEPARATOR << *i ;
    }
  }
}


bool
Ast::SearchContext::insideEncapsulationNamespace( ) const
{
  if( lexicalPath_->empty( ) )
    return false;
  /* An alternative condition that could be used below:
   *  lexicalPath_.size( ) == encapsulationPath_.size( )
   * However, list<>::size is not constant time, so a name-based condition is used instead.
   */
  return isEncapsulationName( lexicalPath_->back( ) );
}


RefCountPtr< const char >
Ast::SearchContext::makePrivateName( size_t i )
{
  std::ostringstream oss;
  oss << i ;
  return strrefdup( oss );
}


RefCountPtr< const char >
Ast::SearchContext::makeEncapsulationName( size_t i )
{
  std::ostringstream oss;
  oss << "^" << i ;
  return strrefdup( oss );
}


bool
Ast::SearchContext::isEncapsulationName( const char * name )
{
  return name[0] == '^';
}


Ast::Identifier::Identifier( const RefCountPtr< const SearchContext > & searchContext, Ast::NamespaceReference::Base base, Ast::NamespacePath * path, const char * simpleId )
  : searchContext_( searchContext ), namespaceRef_( base, path ), simpleId_( simpleId )
{ }

Ast::Identifier::Identifier( const RefCountPtr< const SearchContext > & searchContext, Ast::NamespaceReference::Base base, const RefCountPtr< const Ast::NamespacePath > & path, const char * simpleId )
  : searchContext_( searchContext ), namespaceRef_( base, path ), simpleId_( simpleId )
{ }

Ast::Identifier::Identifier( const RefCountPtr< const SearchContext > & searchContext, const NamespaceReference & namespaceRef, const char * simpleId )
  : searchContext_( searchContext ), namespaceRef_( namespaceRef ), simpleId_( simpleId )
{ }

Ast::Identifier::Identifier( const Ast::Identifier & orig )
  : searchContext_( orig.searchContext_ ), namespaceRef_( orig.namespaceRef_ ), simpleId_( orig.simpleId_ )
{ }

Ast::Identifier::~Identifier( )
{
  /* Note that we intentionally allow simpleId_ to leak here, see memory management note in identifier.h.
   */
}

Ast::PlacedIdentifier *
Ast::Identifier::place( const Ast::SourceLocation & loc )
{
  if( namespaceRef_.base( ) != Ast::NamespaceReference::RELATIVE || namespaceRef_.path( ).size( ) != 0 ){
    Ast::theAnalysisErrorsList.push_back( new Exceptions::ScannerError( loc, strrefdup( "Expected simple identifier." ) ) );
  }
  Ast::PlacedIdentifier * res = new Ast::PlacedIdentifier( searchContext_->lexicalPath( ), simpleId_ );
  simpleId_ = NULL;
  return res;
}

const char *
Ast::Identifier::strip( const Ast::SourceLocation & loc )
{
  if( namespaceRef_.base( ) != Ast::NamespaceReference::RELATIVE || namespaceRef_.path( ).size( ) != 0 ){
    Ast::theAnalysisErrorsList.push_back( new Exceptions::ScannerError( loc, strrefdup( "Expected simple identifier." ) ) );
  }
  const char * res = simpleId_;
  simpleId_ = NULL;
  return res;
}

RefCountPtr< const char >
Ast::Identifier::refstrip( const Ast::SourceLocation & loc )
{
  if( namespaceRef_.base( ) != Ast::NamespaceReference::RELATIVE || namespaceRef_.path( ).size( ) != 0 ){
    Ast::theAnalysisErrorsList.push_back( new Exceptions::ScannerError( loc, strrefdup( "Expected simple identifier." ) ) );
  }
  RefCountPtr< const char > res( simpleId_ );
  simpleId_ = NULL;
  return res;
}

void
Ast::Identifier::show( std::ostream & os, Type type, bool showAbsoluteMark ) const
{
  namespaceRef_.show( os, showAbsoluteMark );
  if( ! namespaceRef_.path( ).empty( ) )
    os << Interaction::NAMESPACE_SEPARATOR ;
  os << identifierPrefix( type ) << simpleId_ ;
}

void
Ast::Identifier::debugShow( std::ostream & os ) const
{
  os << "Identifier: " ;
  if( namespaceRef_.base( ) == Ast::NamespaceReference::ABSOLUTE ){
    os << Interaction::NAMESPACE_SEPARATOR ;
  } else {
    os << "(" ;
    for( Ast::NamespacePath::const_iterator i = searchContext_->lexicalPath( )->begin( ); i != searchContext_->lexicalPath( )->end( ); ++i ){
      os << *i << Interaction::NAMESPACE_SEPARATOR ;
    }
    os << ") " ;
  }

  if( namespaceRef_.base( ) == Ast::NamespaceReference::LOCAL )
    os << Interaction::ENCAPSULATION_MARK ;

  for( Ast::NamespacePath::const_iterator i = namespaceRef_.path( ).begin( ); i != namespaceRef_.path( ).end( ); ++i ){
    os << *i << Interaction::NAMESPACE_SEPARATOR ;
  }

  os << simpleId_ ;

}

bool
Ast::Identifier::operator < ( const Ast::Identifier & other ) const
{
  if( base( ) < other.base( ) ){
    return true;
  }
  if( base( ) > other.base( ) ){
    return false;
  }

  Ast::NamespacePath::const_iterator i1 = namespaceRef_.path( ).begin( );
  Ast::NamespacePath::const_iterator end1 = namespaceRef_.path( ).end( );
  Ast::NamespacePath::const_iterator i2 = other.namespaceRef_.path( ).begin( );
  Ast::NamespacePath::const_iterator end2 = other.namespaceRef_.path( ).end( );

  while( i1 != end1 && i2 != end2 )
  {
    int cmp = strcmp( *i1, *i2 );
    if( cmp < 0 )
      return true;
    if( cmp > 0 )
      return false;
    ++i1;
    ++i2;
  }

  if( i1 != end1 )
    return false;
  if( i2 != end2 )
    return true;

  return strcmp( simpleId_, other.simpleId_ ) < 0;
}

const char *
Ast::Identifier::identifierPrefix( Type type )
{
  switch( type )
  {
    case VARIABLE:
      return "";
    case STATE:
      return Interaction::STATE_PREFIX;
    case DYNAMIC_VARIABLE:
      return Interaction::DYNAMIC_VARIABLE_PREFIX;
    case DYNAMIC_STATE:
      return Interaction::DYNAMIC_STATE_PREFIX;
    case TYPE:
      return "";
  }
}



Ast::PlacedIdentifier::PlacedIdentifier( const RefCountPtr< const NamespacePath > & absolutePath, const char * simpleId )
  : absolutePath_( absolutePath ), simpleId_( simpleId )
{ }

Ast::PlacedIdentifier::PlacedIdentifier( Ast::NamespacePath * absolutePath, const char * simpleId )
  : absolutePath_( absolutePath ), simpleId_( simpleId )
{ }

Ast::PlacedIdentifier::PlacedIdentifier( const Ast::NamespacePath & absolutePath, const char * simpleId )
  : absolutePath_( absolutePath ), simpleId_( simpleId )
{ }

Ast::PlacedIdentifier::PlacedIdentifier( const Ast::PlacedIdentifier & orig )
  : absolutePath_( orig.absolutePath_ ), simpleId_( orig.simpleId_ )
{ }

Ast::PlacedIdentifier::~PlacedIdentifier( )
{
  /* Note that we intentionally allow simpleId_ to leak here, see memory management note in identifier.h.
   */
}

Ast::PlacedIdentifier *
Ast::PlacedIdentifier::clone( ) const
{
  /* Here we use that the simpleId_ is not going to be deallocated, so we can share with the constructed object.
   */
  return new Ast::PlacedIdentifier( absolutePath_, simpleId_ );
}

Ast::Identifier *
Ast::PlacedIdentifier::newAbsolute( ) const
{
  /* Here we use that the simpleId_ is not going to be deallocated, so we can share with the constructed object.
   */
  return new Ast::Identifier( Ast::THE_EMPTY_SEARCH_CONTEXT, Ast::NamespaceReference::ABSOLUTE, absolutePath_, simpleId_ );
}

const char *
Ast::PlacedIdentifier::strip( )
{
  const char * res = simpleId_;
  simpleId_ = NULL;
  return res;
}

RefCountPtr< const char >
Ast::PlacedIdentifier::refstrip( )
{
  RefCountPtr< const char > res( simpleId_ );
  simpleId_ = NULL;
  return res;
}

void
Ast::PlacedIdentifier::show( std::ostream & os, Ast::Identifier::Type type ) const
{
  for( Ast::NamespacePath::const_iterator i = absolutePath( ).begin( ); i != absolutePath( ).end( ); ++i ){
    if( ! Ast::SearchContext::isEncapsulationName( *i ) )
      os << *i << Interaction::NAMESPACE_SEPARATOR ;
  }

  os << Ast::Identifier::identifierPrefix( type ) << simpleId_ ;
}

bool
Ast::PlacedIdentifier::operator < ( const Ast::PlacedIdentifier & other ) const
{
  Ast::NamespacePath::const_iterator i1 = absolutePath( ).begin( );
  Ast::NamespacePath::const_iterator end1 = absolutePath( ).end( );
  Ast::NamespacePath::const_iterator i2 = other.absolutePath( ).begin( );
  Ast::NamespacePath::const_iterator end2 = other.absolutePath( ).end( );

  while( i1 != end1 && i2 != end2 )
  {
    int cmp = strcmp( *i1, *i2 );
    if( cmp < 0 )
      return true;
    if( cmp > 0 )
      return false;
    ++i1;
    ++i2;
  }

  if( i1 != end1 )
    return false;
  if( i2 != end2 )
    return true;

  return strcmp( simpleId_, other.simpleId_ ) < 0;
}
