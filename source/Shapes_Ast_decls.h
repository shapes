/* This file is part of Shapes.
 *
 * Shapes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Shapes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2008, 2009, 2014 Henrik Tidefelt
 */

#pragma once

namespace Shapes
{
	namespace Ast
	{
		class AnalysisEnvironment;
		typedef int StateID;
		class ArgListExprs;
		class BinaryInfixExpr;
		class ClassFunction;
		class DummyExpression;
		class DynamicVariableDecl;
		class Expression;
		class FileID;
		class Identifier;
		class IdentifierTree;
		class MemberDeclaration;
		class Node;
		class PlacedIdentifier;
		class SearchContext;
		class SourceLocation;
		class SplitDefineVariables;
		class StateReference;
		class UnaryExpr;
		class LexiographicState;
		class DynamicBindingExpression;
		class DynamicStateBindingExpression;
		class StateIDSet;
	}
}
