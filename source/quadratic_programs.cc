/* This file is part of Shapes.
 *
 * Shapes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Shapes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2009 Henrik Tidefelt
 */

#include "quadratic_programs.h"
#include <iomanip>
#include <fstream>
#include <stdint.h>

using namespace Shapes;

const char *
Computation::QPSolverStatus::code_str( ) const
{
	switch( code )
		{
		case QP_OK:
			return "OK";
		case QP_WARNING:
			return "Warning";
		case QP_ERROR:
			return "Error";
		case QP_FAIL:
			return "Failure";
		case QP_BAD:
			return "Bad problem";
		}
	return "(QPSolverStatus::code_str: enum switch out of range)";
}

const char *
Computation::QPSolverStatus::sub_str( ) const
{
	switch( code )
		{
		case QP_OK:
			switch( termination )
				{
				case QP_OK_LM:
					return "No negative lagrange multipliers.";
				case QP_OK_GAP_ABS:
					return "Cost absolute gap less than tolerance.";
				case QP_OK_GAP_REL:
					return "Cost relative gap less than tolerance.";
				case QP_OK_UPPER:
					return "Cost sufficiently bounded from above.";
				case QP_OK_LOWER:
					return "Cost sufficiently bounded from below.";
				}
			break;
		case QP_WARNING:
			switch( warning )
				{
				case QP_WARNING_NEGATIVE_LM:
					return "Negative Lagrange multiplier.";
				case QP_WARNING_EARLY_EXIT:
					return "Early exit.";
				}
			break;
		case QP_ERROR:
			switch( error )
				{
				case QP_ERROR_UNDEFINED:
					return "Undefined error.";
				case QP_ERROR_ALLOCATE:
					return "Out of memory.";
				case QP_ERROR_DIMENSIONS_X:
					return "Wrong dimension of output x.";
				case QP_ERROR_DIMENSIONS_LM:
					return "Wrong dimension of output lm.";
				}
			break;
		case QP_FAIL:
			switch( failure )
				{
				case QP_FAIL_ITER:
					return "Maximum number of iterations exceeded.";
				}
			break;
		case QP_BAD:
			switch( bad )
				{
				case QP_BAD_INCONSISTENT_EQ:
					return "Inconsistent equality constraints.";
				case QP_BAD_INCONSISTENT_INEQ:
					return "Inconsistent inequality constraints.";
				case QP_BAD_INCONSISTENT_BOX:
					return "Inconsistent box constraints.";
				case QP_BAD_INCONSISTENT_ACTIVE:
					return "Encountered inconsistent constraints.";
				case QP_BAD_POSITIVE_DEFINITE:
					return "Objective function not positive definite.";
				case QP_BAD_LINEAR_DEPENDENT:
					return "Linearly dependent constraints.";
				case QP_BAD_INITIAL_INFEASIBLE:
					return "Infeasible initial point.";
				}
			break;
		}
	return "(QPSolverStatus::str: enum switch out of range)";

}

void
Computation::polytope_distance_generator_form_write_data( std::ostream & os,
																													const size_t dim,
																													const size_t n1, const double * g1,
																													const size_t n2, const double * g2 )
{
	os << "Polytope distance problem in generator form." << std::endl
		 << "===" << std::endl
		 << dim << " " << n1 << " " << n2 << std::endl
		 << std::fixed << std::setprecision(6) ;
	{
		const double * begin = g1;
		const double * end = begin + dim * n1;
		for( const double * src = begin; src != end; ++src )
			{
				if( src != begin )
					{
						os << " " ;
					}
				os << *src ;
			}
		os << std::endl ;
	}
	{
		const double * begin = g2;
		const double * end = begin + dim * n2;
		for( const double * src = begin; src != end; ++src )
			{
				if( src != begin )
					{
						os << " " ;
					}
				os << *src ;
			}
		os << std::endl ;
	}
}


void
Computation::polytope_distance_generator_form_write_binary_data( const char * oFilename,
																																 const size_t dim,
																																 const size_t n1, const double * g1,
																																 const size_t n2, const double * g2 )
{
	std::ofstream oFile( oFilename, std::ios::out | std::ios::binary );
	uint32_t tmp;
	tmp = dim;
	oFile.write( reinterpret_cast< const char * >( & tmp ), sizeof( tmp ) );
	tmp = n1;
	oFile.write( reinterpret_cast< const char * >( & tmp ), sizeof( tmp ) );
	tmp = n2;
	oFile.write( reinterpret_cast< const char * >( & tmp ), sizeof( tmp ) );
	{
		const double * begin = g1;
		const double * end = begin + dim * n1;
		for( const double * src = begin; src != end; ++src )
			{
				oFile.write( reinterpret_cast< const char * >( src ), sizeof( *src ) );
			}
	}
	{
		const double * begin = g2;
		const double * end = begin + dim * n2;
		for( const double * src = begin; src != end; ++src )
			{
				oFile.write( reinterpret_cast< const char * >( src ), sizeof( *src ) );
			}
	}
}
