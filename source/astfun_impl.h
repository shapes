/* This file is part of Shapes.
 *
 * Shapes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Shapes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2013, 2014 Henrik Tidefelt
 */

#pragma once

namespace Shapes
{
	namespace Kernel
	{

		class CallCont_1 : public Kernel::Continuation
		{
			const Ast::ArgListExprs * argList_;
			bool curry_;
			Kernel::StateHandle mutatorSelf_;
			Kernel::PassedEnv env_;
			Kernel::PassedDyn dyn_;
			Kernel::ContRef cont_;
			const Ast::SourceLocation & callLoc_;
		public:
			CallCont_1( const Ast::SourceLocation & traceLoc, const Ast::ArgListExprs * argList, bool curry, Kernel::StateHandle mutatorSelf, const Kernel::EvalState & evalState, const Ast::SourceLocation & callLoc );
			virtual ~CallCont_1( );
			virtual void takeValue( const RefCountPtr< const Lang::Value > & val, Kernel::EvalState * evalState, bool dummy ) const;
			virtual Kernel::ContRef up( ) const;
			virtual RefCountPtr< const char > description( ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
		};

		class CallCont_last : public Kernel::Continuation
		{
			RefCountPtr< const Lang::Function > fun_;
			const Ast::ArgListExprs * argList_;
			bool curry_;
			Kernel::StateHandle mutatorSelf_;
			Kernel::PassedEnv env_;
			Kernel::PassedDyn dyn_;
			Kernel::ContRef cont_;
		public:
			CallCont_last( const RefCountPtr< const Lang::Function > & fun, const Ast::ArgListExprs * argList, bool curry, Kernel::StateHandle mutatorSelf, const Kernel::PassedEnv & env, const Kernel::PassedDyn & dyn, const Kernel::ContRef & cont, const Ast::SourceLocation & callLoc );
			virtual ~CallCont_last( );
			virtual void takeValue( const RefCountPtr< const Lang::Value > & valsUntyped, Kernel::EvalState * evalState, bool dummy ) const;
			virtual Kernel::ContRef up( ) const;
			virtual RefCountPtr< const char > description( ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
		};

		class CallCont_n : public Kernel::Continuation
		{
			RefCountPtr< const Kernel::CallContInfo > info_;
			Ast::ArgListExprs::ConstIterator pos_;
			RefCountPtr< const Lang::SingleList > vals_;
			bool force_; /* If false, the argument is immediate but not forced. */
		public:
			CallCont_n( const Ast::SourceLocation & traceLoc, const RefCountPtr< const Kernel::CallContInfo > & info, const Ast::ArgListExprs::ConstIterator & pos, RefCountPtr< const Lang::SingleList > vals, bool force );
			virtual ~CallCont_n( );
			virtual void takeHandle( Kernel::VariableHandle val, Kernel::EvalState * evalState, bool dummy ) const;
			virtual Kernel::ContRef up( ) const;
			virtual RefCountPtr< const char > description( ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
		};

		class CallCont_Structure_last : public Kernel::Continuation
		{
			RefCountPtr< const Lang::Function > fun_;
			const Ast::ArgListExprs * argList_;
			RefCountPtr< const Lang::SingleList > values_;
			bool curry_;
			Kernel::PassedEnv env_;
			Kernel::PassedDyn dyn_;
			Kernel::ContRef cont_;
		public:
			CallCont_Structure_last( const RefCountPtr< const Lang::Function > & fun, const Ast::ArgListExprs * argList, const RefCountPtr< const Lang::SingleList > & values, bool curry, const Kernel::PassedEnv & env, const Kernel::PassedDyn & dyn, const Kernel::ContRef & cont, const Ast::SourceLocation & callLoc );
			virtual ~CallCont_Structure_last( );
			virtual void takeValue( const RefCountPtr< const Lang::Value > & valDummy, Kernel::EvalState * evalState, bool dummy ) const;
			virtual Kernel::ContRef up( ) const;
			virtual RefCountPtr< const char > description( ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
		};

		class CallCont_Structure_n : public Kernel::Continuation
		{
			RefCountPtr< const Kernel::CallContInfo > info_;
			size_t pos_; /* Reverse index of first element in rest_.  For example, when rest_ contains a single element, pos_ shall be 0. */
			RefCountPtr< const Lang::SingleList > rest_;
		public:
			CallCont_Structure_n( const Ast::SourceLocation & traceLoc, const RefCountPtr< const Kernel::CallContInfo > & info, size_t pos, RefCountPtr< const Lang::SingleList > rest );
			virtual ~CallCont_Structure_n( );
			virtual void takeValue( const RefCountPtr< const Lang::Value > & forcedValue, Kernel::EvalState * evalState, bool dummy ) const;
			virtual Kernel::ContRef up( ) const;
			virtual RefCountPtr< const char > description( ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
		};

		class UnionCont_last : public Kernel::Continuation
		{
			const Ast::ArgListExprs * argList_;
			Kernel::ContRef cont_;
		public:
			UnionCont_last( const Ast::ArgListExprs * argList, const Kernel::ContRef & cont, const Ast::SourceLocation & callLoc );
			virtual ~UnionCont_last( );
			virtual void takeValue( const RefCountPtr< const Lang::Value > & valsUntyped, Kernel::EvalState * evalState, bool dummy ) const;
			virtual Kernel::ContRef up( ) const;
			virtual RefCountPtr< const char > description( ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
		};

		class SplitCont_1 : public Kernel::Continuation
		{
			Ast::Expression * argList_;
			bool curry_;
			Kernel::PassedEnv env_;
			Kernel::PassedDyn dyn_;
			Kernel::ContRef cont_;
			const Ast::SourceLocation & callLoc_;
		public:
			SplitCont_1( const Ast::SourceLocation & traceLoc, Ast::Expression * argList, bool curry, const Kernel::EvalState & evalState, const Ast::SourceLocation & callLoc );
			virtual ~SplitCont_1( );
			virtual void takeValue( const RefCountPtr< const Lang::Value > & val, Kernel::EvalState * evalState, bool dummy ) const;
			virtual Kernel::ContRef up( ) const;
			virtual RefCountPtr< const char > description( ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
		};

		class SplitCont_2 : public Kernel::Continuation
		{
			RefCountPtr< const Lang::Function > fun_;
			bool curry_;
			Kernel::PassedEnv env_;
			Kernel::PassedDyn dyn_;
			Kernel::ContRef cont_;
		public:
			SplitCont_2( const RefCountPtr< const Lang::Function > & fun, bool curry, const Kernel::PassedEnv & env, const Kernel::PassedDyn & dyn, const Kernel::ContRef & cont, const Ast::SourceLocation & callLoc );
			virtual ~SplitCont_2( );
			virtual void takeValue( const RefCountPtr< const Lang::Value > & valsUntyped, Kernel::EvalState * evalState, bool dummy ) const;
			virtual Kernel::ContRef up( ) const;
			virtual RefCountPtr< const char > description( ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
		};

	}
}
