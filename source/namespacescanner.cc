/* This file is part of Shapes.
 *
 * Shapes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Shapes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2015 Henrik Tidefelt
 */

#include "namespacescanner.h"

using namespace Shapes;


NamespaceScanner::NamespaceScanner( Shapes::Ast::NamespaceDeclarations * declarations, std::istream * yyin, const Shapes::Ast::FileID * fileID )
: yyFlexLexer( yyin, 0 ), moreState_( false ), lastleng_( 0 ),
  declarations_( declarations )
{
  namespacelloc = Ast::SourceLocation( fileID );
}


void
NamespaceScanner::more( )
{
  moreState_ = true; /* This one is for ourselves to use in doBeforeEachAction. */
  yy_more_flag = 1; /* This one is for flex, and will be reset before we reach doBeforeEachAction. */
  lastleng_ = yyleng;
}


void
NamespaceScanner::doBeforeEachAction( )
{
  if( moreState_ ){
    namespacelloc.lastColumn += yyleng - lastleng_;
  }else{
    namespacelloc.firstLine = namespacelloc.lastLine;
    namespacelloc.firstColumn = namespacelloc.lastColumn;
    namespacelloc.lastColumn = namespacelloc.firstColumn + yyleng;
  }
  moreState_ = false;
}


void
NamespaceScanner::endOfLine( )
{
  namespacelloc.firstLine = namespacelloc.lastLine + 1;
  namespacelloc.lastLine = namespacelloc.firstLine;
  namespacelloc.lastColumn = 0;
}


std::string
NamespaceScanner::rinseString( const char * str )
{
  /* A string is always terminated by the one byte sequence in both ends: " */
  size_t bytecount = std::strlen(str) - 2;
  char * mem = new char[ bytecount + 1 ];
  memcpy( mem, str, bytecount );
  mem[ bytecount ] = '\0';
  std::string res(mem);
  delete mem;
  return res;
}
