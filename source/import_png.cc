/* This file is part of Shapes.
 *
 * Shapes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Shapes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2009, 2010 Henrik Tidefelt
 */

#include "config.h"

#ifdef HAVE_LIBPNG
/* Placing this inclusion first could be a dangerous way of avoiding a relevant warning
 * about possibly conflicting uses of setjmp.h.  See libpng/pngconf.h for for information.
 */
#include <png.h>
#endif

#include "drawabletypes.h"
#include "rasterloaders.h"
#include "statetypes.h"
#include "autoonoff.h"
#include "globals.h"
#include "consts.h"
#include "warn.h"

#include <fstream>

using namespace Shapes;


#ifdef HAVE_LIBPNG

namespace Shapes
{
	namespace Kernel
	{

		void
		Shapes_pnglib_error_fn( png_structp png_ptr,
														png_const_charp error_msg )
		{
			throw Exceptions::ExternalError( strrefdup( error_msg ) );
		}

		void
		Shapes_pnglib_warning_fn( png_structp png_ptr,
															png_const_charp warn_msg )
		{
			std::ostringstream msg;
			msg << "libpng says " << warn_msg ;
			WARN_OR_THROW( Exceptions::ExternalError( strrefdup( msg ), true ) );
		}

		void
		Shapes_pnglib_read_data( png_structp png_ptr,
														 png_bytep data,
														 png_size_t length )
		{
			std::istream * iFile = static_cast< std::istream * >( png_get_io_ptr( png_ptr ) );
			iFile->read( reinterpret_cast< char * >( data ), length );
		}

	}

	namespace Helpers
	{

		void separateAlpha_bits( const unsigned char * row, size_t rowBytes, size_t bitsPerUnit, std::ostream & colorDst, size_t colorUnits, std::ostream & alphaDst, size_t alphaUnits, const png_color_16 * transparentColor, char * buf1, char * buf2 );
		template< class C >
		void separateAlpha( const unsigned char * row, size_t rowBytes, std::ostream & colorDst, size_t colorUnts, std::ostream & alphaDst, size_t alphaUnits, const png_color_16 * transparentColor, char * buf1, char * buf2 );
		void applyPalette( const unsigned char * row, size_t rowBytes, size_t index_depth, const png_colorp palette, const png_bytep trans, int num_palette, std::ostream & colorDst, std::ostream & alphaDst );

	}
}

#endif


RefCountPtr< const Lang::RasterImage >
Shapes::Helpers::RasterLoader_PNG::load( RefCountPtr< std::ifstream > & filePtr, const std::string & full_filename, Concrete::Length resolution, bool override, const Ast::PlacedIdentifier & coreId, const Ast::SourceLocation & callLoc ) const
{
#ifndef HAVE_LIBPNG
	throw Exceptions::BuildRequirement( Interaction::BUILD_REQ_LIBPNG, coreId, callLoc );
#else

	std::ifstream & iFile = *filePtr;

	const size_t SIGNATURE_SIZE = 8;
	{
		char header[ SIGNATURE_SIZE + 1 ];
    iFile.read( header, SIGNATURE_SIZE );
    bool is_png = png_sig_cmp( reinterpret_cast< png_bytep >( header ), 0, SIGNATURE_SIZE ) == 0;
    if( ! is_png )
    {
			throw Exceptions::CoreRequirement( "The imported file does not have the magic signature of a PNG file.", coreId, callLoc );
    }
	}

	png_structp png_ptr = png_create_read_struct( PNG_LIBPNG_VER_STRING,
																								static_cast< png_voidp >( 0 ), /* Pointer to additional information passed to error and warning handlers? */
																								Kernel::Shapes_pnglib_error_fn,
																								Kernel::Shapes_pnglib_warning_fn );

	/* We use two error handling mechanisms in parallel, since I don't understand exactly how to use the longjmp function in the uer-defined
	 * error handler Shapes_pnglib_error_fn.  Hence, we both use setjmp in case someone uses longjmp, and use try/catch to handle the errors
	 * thrown by Shapes_pnglib_error_fn.
	 */

	png_set_error_fn( png_ptr,
									  static_cast< png_voidp >( 0 ), /* Pointer to additional information passed to error and warning handlers? */
										Kernel::Shapes_pnglib_error_fn,
										Kernel::Shapes_pnglib_warning_fn);

	if( png_ptr == 0)
		{
			throw Exceptions::ExternalError( "Failed to allocate read structure for reading a PNG file." );
		}

	png_infop info_ptr = png_create_info_struct( png_ptr );
	if( info_ptr == 0)
    {
			png_destroy_read_struct( & png_ptr,
															 static_cast< png_infopp >( 0 ), static_cast< png_infopp >( 0 ) );
			throw Exceptions::ExternalError( "Failed to allocate info structure for reading a PNG file." );
    }

	png_infop end_info = png_create_info_struct( png_ptr );
	if( end_info == 0 )
		{
			png_destroy_read_struct( & png_ptr, & info_ptr,
															 static_cast< png_infopp >( 0 ) );
			throw Exceptions::ExternalError( "Failed to allocate end-info structure for reading a PNG file." );
		}

	if( setjmp( png_jmpbuf( png_ptr ) ) )
		{
			png_destroy_read_struct( & png_ptr, & info_ptr, & end_info );
			throw Exceptions::ExternalError( "Caught a longjmp from libpng while reading a PNG file." );
		}

	try
		{
			png_set_sig_bytes( png_ptr, SIGNATURE_SIZE );

			png_set_read_fn( png_ptr,
											 static_cast< png_voidp >( filePtr.getPtr( ) ),
											 Kernel::Shapes_pnglib_read_data );


			png_read_png( png_ptr, info_ptr, PNG_TRANSFORM_IDENTITY, 0 );

			png_uint_32 size_x;
			png_uint_32 size_y;
			int size_depth;
			int color_type;
			int interlace_method;
			int compression_method;
			int filter_method;

			png_get_IHDR( png_ptr, info_ptr,
										& size_x, & size_y, & size_depth,
										& color_type, & interlace_method, & compression_method, & filter_method );

			RefCountPtr< const Lang::ColorSpace > colorSpace = Lang::THE_COLOR_SPACE_DEVICE_GRAY; /* Initialize with anything. */
			bool hasAlpha = false;
			bool hasTransparentColor = false;
			bool hasPalette = false;
			png_colorp palette;
			int num_palette;
			switch( color_type )
				{
				case PNG_COLOR_TYPE_GRAY:
					/* Use the default value set above! */
					break;
				case PNG_COLOR_TYPE_RGB:
					colorSpace = Lang::THE_COLOR_SPACE_DEVICE_RGB;
					break;
				case PNG_COLOR_TYPE_GRAY_ALPHA:
					colorSpace = Lang::THE_COLOR_SPACE_DEVICE_GRAY;
					hasAlpha = true;
					break;
				case PNG_COLOR_TYPE_RGB_ALPHA:
					colorSpace = Lang::THE_COLOR_SPACE_DEVICE_RGB;
					hasAlpha = true;
					break;
				case PNG_COLOR_TYPE_PALETTE:
					/* The palette colors use 8 bit in each of the three channels. */
					colorSpace = Lang::THE_COLOR_SPACE_DEVICE_RGB;
					png_get_PLTE( png_ptr, info_ptr, & palette, & num_palette );
					hasPalette = true;
					break;
				default:
					throw Exceptions::CoreRequirement( "The PNG file was using an unknown color type value.", coreId, callLoc );
				}

			png_bytep tRNS_trans;
			png_color_16p tRNS_trans_values;
			if( png_get_valid( png_ptr, info_ptr, PNG_INFO_tRNS ) != 0 )
				{
					hasAlpha = true;
					int tRNS_num_trans;
					png_get_tRNS( png_ptr, info_ptr, & tRNS_trans, & tRNS_num_trans, & tRNS_trans_values );
					if( hasPalette )
						{
							if( tRNS_num_trans != num_palette )
								{
									throw Exceptions::CoreRequirement( "The PNG file has a transparency pallete of different size compared to the color palette.", coreId, callLoc );
								}
						}
					else
						{
							hasTransparentColor = true;
						}
				}

			size_t components = colorSpace->numberOfComponents( );
			size_t png_channels = png_get_channels( png_ptr, info_ptr );

			Concrete::Length resolution_x = resolution;
			Concrete::Length resolution_y = resolution;
			if( ! override && png_get_valid( png_ptr, info_ptr, PNG_INFO_pHYs ) != 0 )
				{
					png_uint_32 x_pixels_per_unit;
					png_uint_32 y_pixels_per_unit;
					int phys_unit_type;
					png_get_pHYs( png_ptr, info_ptr, & x_pixels_per_unit, & y_pixels_per_unit, & phys_unit_type );
					switch( phys_unit_type )
						{
						case PNG_RESOLUTION_UNKNOWN:/* Only aspect ratio. */
							{
								Concrete::Length tmpRes = sqrt( static_cast< double >( x_pixels_per_unit ) * static_cast< double >( y_pixels_per_unit ) ) * resolution;
								resolution_x = ( tmpRes / x_pixels_per_unit );
								resolution_y = ( tmpRes / y_pixels_per_unit );
							}
							break;
						case PNG_RESOLUTION_METER:/* Dots per meter. */
							{
								resolution_x = ( Concrete::Length( 100/(2.54/72) ) / x_pixels_per_unit );
								resolution_y = ( Concrete::Length( 100/(2.54/72) ) / y_pixels_per_unit );
							}
							break;
						default:
							throw Exceptions::CoreRequirement( "The PNG file was using an unknown density unit value.", coreId, callLoc );
						}
				}

			Lang::RasterImage * imagePtr = Lang::RasterImage::newInstance( size_x, size_y, hasPalette ? 8 : size_depth, resolution_x, resolution_y, colorSpace );
			RefCountPtr< const Lang::RasterImage > image = RefCountPtr< const Lang::RasterImage >( RefCountPtr< const Lang::RasterImage >( imagePtr ) );

			RefCountPtr< SimplePDF::PDF_Stream_out > maskStream = RefCountPtr< SimplePDF::PDF_Stream_out >( NullPtr< SimplePDF::PDF_Stream_out >( ) );

			SimplePDF::PDF_Stream_out & stream = *(imagePtr->stream( ));
			stream[ "Filter" ] = SimplePDF::newName( "FlateDecode" );
			if( hasAlpha )
				{
					Lang::RasterImage * maskPtr = Lang::RasterImage::newInstance( size_x, size_y, hasPalette ? 8 : size_depth, resolution_x, resolution_y, Lang::THE_COLOR_SPACE_DEVICE_GRAY );
					imagePtr->setSoftMask( RefCountPtr< const Lang::RasterImage >( RefCountPtr< const Lang::RasterImage >( maskPtr ) ) );
					maskStream = maskPtr->stream( );
					(*maskStream)[ "Filter" ] = SimplePDF::newName( "FlateDecode" );

					const SimplePDF::PDF_Version::Version SOFTMASK_VERSION = SimplePDF::PDF_Version::PDF_1_4;
					if( Kernel::the_PDF_version.greaterOrEqual( SOFTMASK_VERSION ) )
						{
							stream[ "SMask" ] = maskPtr->getResource( );
						}
					else
						{
							Kernel::the_PDF_version.message( SOFTMASK_VERSION, "A PNG alpha channel (soft mask) was ignored." );
						}
				}

			size_t rowbytes = png_get_rowbytes( png_ptr, info_ptr );
			char * buf1 = 0;
			char * buf2 = 0;
			PossiblyDeleteOnExit< char > buf1Deleter;
			PossiblyDeleteOnExit< char > buf2Deleter;
			if( hasAlpha )
				{
					buf1 = new char[ rowbytes ];
					buf1Deleter.activate( buf1 );
					buf2 = new char[ rowbytes ];
					buf2Deleter.activate( buf2 );
				}
			png_bytep * rowBegin = png_get_rows( png_ptr, info_ptr );
			for( png_bytep * srcRow = rowBegin; srcRow < rowBegin + size_y; ++srcRow )
				{
					/* Note that image data in PDF starts each row on a fresh byte boundary.
					 */
					if( hasPalette )
						{
							if( hasAlpha )
								{
									Helpers::applyPalette( reinterpret_cast< unsigned char * >( *srcRow ), rowbytes, size_depth, palette, tRNS_trans, num_palette, stream.data, maskStream->data );
								}
							else
								{
									std::ostringstream ignore;
									Helpers::applyPalette( reinterpret_cast< unsigned char * >( *srcRow ), rowbytes, size_depth, palette, 0, num_palette, stream.data, ignore );
								}
						}
					else if( hasAlpha )
						{
							if( hasTransparentColor )
								{
									/* There may be some kind of filler (byte or bits) in the data, that we need to discard. */
									switch( size_depth )
										{
										case 1:
										case 2:
										case 4:
											Helpers::separateAlpha_bits( reinterpret_cast< unsigned char * >( *srcRow ), rowbytes, size_depth, stream.data, components, maskStream->data, png_channels - components, tRNS_trans_values, buf1, buf2 );
											break;
										case 8:
											Helpers::separateAlpha< png_byte >( reinterpret_cast< unsigned char * >( *srcRow ), rowbytes, stream.data, components, maskStream->data, png_channels - components, tRNS_trans_values, buf1, buf2 );
											break;
										case 16:
											Helpers::separateAlpha< png_uint_16 >( reinterpret_cast< unsigned char * >( *srcRow ), rowbytes, stream.data, components, maskStream->data, png_channels - components, tRNS_trans_values, buf1, buf2 );
											break;
										default:
											throw Exceptions::InternalError( "Unexpected color depth in PNG image." );
										}
								}
							else
								{
									switch( size_depth )
										{
										case 1:
										case 2:
										case 4:
											Helpers::separateAlpha_bits( reinterpret_cast< unsigned char * >( *srcRow ), rowbytes, size_depth, stream.data, components, maskStream->data, 1, 0, buf1, buf2 );
											break;
										case 8:
											Helpers::separateAlpha< png_byte >( reinterpret_cast< unsigned char * >( *srcRow ), rowbytes, stream.data, components, maskStream->data, 1, 0, buf1, buf2 );
											break;
										case 16:
											Helpers::separateAlpha< png_uint_16 >( reinterpret_cast< unsigned char * >( *srcRow ), rowbytes, stream.data, components, maskStream->data, 1, 0, buf1, buf2 );
											break;
										default:
											throw Exceptions::InternalError( "Unexpected color depth in PNG image." );
										}
								}
						}
					else if( png_channels > components )
						{
							/* There is some kind of filler (byte or bits) in the data, that we need to discard. */
							std::ostringstream ignore;
							switch( size_depth )
								{
								case 1:
								case 2:
								case 4:
									Helpers::separateAlpha_bits( reinterpret_cast< unsigned char * >( *srcRow ), rowbytes, size_depth, stream.data, components, ignore, png_channels - components, 0, buf1, buf2 );
									break;
								case 8:
									Helpers::separateAlpha< png_byte >( reinterpret_cast< unsigned char * >( *srcRow ), rowbytes, stream.data, components, ignore, png_channels - components, 0, buf1, buf2 );
									break;
								case 16:
									Helpers::separateAlpha< png_uint_16 >( reinterpret_cast< unsigned char * >( *srcRow ), rowbytes, stream.data, components, ignore, png_channels - components, 0, buf1, buf2 );
									break;
								default:
									throw Exceptions::InternalError( "Unexpected color depth in PNG image." );
								}
						}
					else
						{
							stream.data.write( reinterpret_cast< char * >( *srcRow ), rowbytes );
						}
				}

			/* If everything was OK, we just free allocated resources and return.
			 */
			png_destroy_read_struct( & png_ptr, & info_ptr, & end_info );

			return image;
		}
	catch( const Exceptions::Exception & ball )
		{
			png_destroy_read_struct( & png_ptr, & info_ptr, & end_info );
			throw;
		}
#endif
}


#ifdef HAVE_LIBPNG

void
Helpers::separateAlpha_bits( const unsigned char * row, size_t rowBytes, size_t bitsPerUnit, std::ostream & colorDst, size_t colorUnits, std::ostream & alphaDst, size_t alphaUnits, const png_color_16 * transparentColor, char * bufCo, char * bufAl )
{
	char * dstCo = bufCo;
	char * dstAl = bufAl;

	png_uint_16 transparentColorArray[3];
	if( transparentColor != 0 )
		{
			switch( colorUnits )
				{
				case 1:
					transparentColorArray[0] = transparentColor->gray;
					break;
				case 3:
					transparentColorArray[0] = transparentColor->red;
					transparentColorArray[1] = transparentColor->green;
					transparentColorArray[2] = transparentColor->blue;
					break;
				default:
					throw Exceptions::InternalError( "Unexpected number of color channels in PNG image alpha bit separation." );
				}
		}
	const unsigned char * src = row;
	const unsigned char * srcEnd = row + rowBytes;
	unsigned char srcByte = *src;
	++src;
	unsigned char dstByteCo = 0; /* Initialize to inhibit compiler warnings. */
	unsigned char dstByteAl = 0; /* Initialize to inhibit compiler warnings. */
	unsigned char srcShift = 8 - bitsPerUnit;
	unsigned char dstAvailCo = 8;
	unsigned char dstAvailAl = 8;
	unsigned char mask;
	switch( bitsPerUnit )
		{
		case 1:
			mask = 0x01;
			break;
		case 2:
			mask = 0x03;
			break;
		case 4:
			mask = 0x07;
			break;
		default:
			throw Exceptions::InternalError( "Unexpected color depth in PNG image alpha bit separation." );
		}
	while( true )
		{
			bool trans = true;
			const png_uint_16 * transparentColorSrc = transparentColorArray;
			for( size_t i = 0; i < colorUnits; ++i, ++transparentColorSrc )
				{
					dstByteCo = ( dstByteCo << bitsPerUnit ) | ( ( srcByte >> srcShift ) & mask );
					if( transparentColor != 0 && ( ( srcByte >> srcShift ) & mask ) != *transparentColorSrc )
						{
							trans = false;
						}
					dstAvailCo -= bitsPerUnit;
					if( dstAvailCo == 0 )
						{
							*reinterpret_cast< unsigned char * >( dstCo ) = dstByteCo;
							++dstCo;
							dstAvailCo = 8;
						}
					if( srcShift == 0 )
						{
							if( src == srcEnd )
								{
									goto srcComplete;
								}
							srcByte = *src;
							++src;
							srcShift = 8;
						}
					srcShift -= bitsPerUnit;
				}
			if( transparentColor == 0 )
				{
					for( size_t i = 0; i < alphaUnits; ++i  )
						{
							dstByteAl = ( dstByteAl << bitsPerUnit ) | ( ( srcByte >> srcShift ) & mask );
							dstAvailAl -= bitsPerUnit;
							if( dstAvailAl == 0 )
								{
									*reinterpret_cast< unsigned char * >( dstAl ) = dstByteAl;
									++dstAl;
									dstAvailAl = 8;
								}
							if( srcShift == 0 )
								{
									if( src == srcEnd )
										{
											goto srcComplete;
										}
									srcByte = *src;
									++src;
									srcShift = 8;
								}
							srcShift -= bitsPerUnit;
						}
				}
			else
				{
					dstByteAl = ( dstByteAl << bitsPerUnit ) | ( trans ? mask : 0 );
					dstAvailAl -= bitsPerUnit;
					if( dstAvailAl == 0 )
						{
							*reinterpret_cast< unsigned char * >( dstAl ) = dstByteAl;
							++dstAl;
							dstAvailAl = 8;
						}

					/* Just ignore filler data. */
					for( size_t i = 0; i < alphaUnits; ++i  )
						{
							if( srcShift == 0 )
								{
									if( src == srcEnd )
										{
											goto srcComplete;
										}
									srcByte = *src;
									++src;
									srcShift = 8;
								}
							srcShift -= bitsPerUnit;
						}
				}
		}
 srcComplete:

	if( dstAvailCo < 8 )
		{
			*reinterpret_cast< unsigned char * >( dstCo ) = ( dstByteCo << dstAvailCo );
			++dstCo;
		}
	if( dstAvailAl < 8 )
		{
			*reinterpret_cast< unsigned char * >( dstAl ) = ( dstByteAl << dstAvailAl );
			++dstAl;
		}

	colorDst.write( bufCo, dstCo - bufCo );
	alphaDst.write( bufAl, dstAl - bufAl );
}

template< class C >
void
Helpers::separateAlpha( const unsigned char * row, size_t rowBytes, std::ostream & colorDst, size_t colorUnits, std::ostream & alphaDst, size_t alphaUnits, const png_color_16 * transparentColor, char * bufCo, char * bufAl )
{
	C * dstCo = reinterpret_cast< C * >( bufCo );
	C * dstAl = reinterpret_cast< C * >( bufAl );

	C OPAQUE = 0;

	png_uint_16 transparentColorArray[3];
	if( transparentColor != 0 )
		{
			switch( colorUnits )
				{
				case 1:
					transparentColorArray[0] = transparentColor->gray;
					break;
				case 3:
					transparentColorArray[0] = transparentColor->red;
					transparentColorArray[1] = transparentColor->green;
					transparentColorArray[2] = transparentColor->blue;
					break;
				default:
					throw Exceptions::InternalError( "Unexpected number of color channels in PNG image alpha bit separation." );
				}
		}

	const C * src = reinterpret_cast< const C * >( row );
	const C * srcEnd = reinterpret_cast< const C * >( row + rowBytes );
	const C * tmpEnd = reinterpret_cast< const C * >( row );
	for( ; src < srcEnd; )
		{
			bool trans = true;
			const png_uint_16 * transparentColorSrc = transparentColorArray;

			tmpEnd += colorUnits;
			for( ; src != tmpEnd; ++src, ++dstCo, ++transparentColorSrc )
				{
					*reinterpret_cast< unsigned char * >( dstCo ) = *src;
					if( transparentColor != 0 && *src != *transparentColorSrc )
						{
							trans = false;
						}
				}
			tmpEnd += alphaUnits;
			if( transparentColor == 0 )
				{
					for( ; src != tmpEnd; ++src, ++dstAl )
						{
							*reinterpret_cast< unsigned char * >( dstAl ) = *src;
						}
				}
			else
				{
					*dstAl = trans ? (~OPAQUE) : OPAQUE;
					++dstAl;

					/* Just ignore fill bytes. */
					src = tmpEnd;
				}
		}
	colorDst.write( bufCo, reinterpret_cast< char * >( dstCo ) - bufCo );
	alphaDst.write( bufAl, reinterpret_cast< char * >( dstAl ) - bufAl );
}

void
Helpers::applyPalette( const unsigned char * row, size_t rowBytes, size_t index_depth, const png_colorp palette, const png_bytep trans, int num_palette, std::ostream & colorDst, std::ostream & alphaDst )
{
	switch( index_depth )
		{
		case 1:
		case 2:
		case 4:
			{
				const unsigned char * src = row;
				const unsigned char * srcEnd = row + rowBytes;
				unsigned char srcByte = *src;
				++src;
				unsigned char srcShift = 8 - index_depth;
				unsigned char mask;
				switch( index_depth )
					{
					case 1:
						mask = 0x01;
						break;
					case 2:
						mask = 0x03;
						break;
					case 4:
						mask = 0x07;
						break;
					default:
						throw Exceptions::InternalError( "Switch in PNG palette application was out of range." );
					}
				while( true )
					{
						unsigned char index = ( srcByte >> srcShift ) & mask;
						if( index >= num_palette )
							{
								throw Exceptions::ExternalError( "PNG palette index is out of bounds." );
							}
						unsigned char buf[3];
						buf[0] = palette[ index ].red;
						buf[1] = palette[ index ].green;
						buf[2] = palette[ index ].blue;
						colorDst.write( reinterpret_cast< char * >( buf ), 3 );
						if( trans != 0 )
							{
								alphaDst.write( reinterpret_cast< char * >( & ( trans[ index ] ) ), 1 );
							}
						if( srcShift == 0 )
							{
								if( src == srcEnd )
									{
										break;
									}
								srcByte = *src;
								++src;
								srcShift = 8;
							}
						srcShift -= index_depth;
					}
			}
			break;
		case 8:
			{
				const unsigned char * srcEnd = row + rowBytes;
				for( const unsigned char * src = row; src != srcEnd; ++src )
					{
						if( *src >= num_palette )
							{
								throw Exceptions::ExternalError( "PNG palette index is out of bounds." );
							}
						unsigned char buf[3];
						buf[0] = palette[ *src ].red;
						buf[1] = palette[ *src ].green;
						buf[2] = palette[ *src ].blue;
						colorDst.write( reinterpret_cast< char * >( buf ), 3 );
						if( trans != 0 )
							{
								alphaDst.write( reinterpret_cast< char * >( & ( trans[ *src ] ) ), 1 );
							}
					}
			}
			break;
		default:
			throw Exceptions::InternalError( "Unexpected depth in PNG palette image (supported values are { 1, 2, 4, 8 })." );
		}
}

#endif
