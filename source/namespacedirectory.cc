/* This file is part of Shapes.
 *
 * Shapes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Shapes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2015 Henrik Tidefelt
 */

#include "namespacedirectory.h"
#include "shapesexceptions.h"
#include "namespacescanner.h"
#include "shapesscanner.h"
#include "ptrowner.h"
#include "globals.h"

#include <fstream>
#include <queue>
#include <dirent.h>
#include <libgen.h>

using namespace Shapes;


Ast::NamespaceDeclarations::NamespaceDeclarations( )
: encapsulated_( false )
{ }

void
Ast::NamespaceDeclarations::addPrelude( const std::string & entry )
{
  preludeSet_.insert( preludeSet_.begin( ), entry );
}

void
Ast::NamespaceDeclarations::addIgnore( const std::string & entry )
{
  ignoreSet_.insert( ignoreSet_.begin( ), entry );
}

void
Ast::NamespaceDeclarations::addPrecedes( const std::set< std::string > & x, const std::set< std::string > & y )
{
  for( std::set< std::string >::const_iterator i = x.begin(); i != x.end(); ++i ){
    std::set< std::string > & s = orderGraph_[ *i ];
    s.insert( y.begin( ), y.end( ) );
  }
}

class OrderGraphNode
{
  std::list< OrderGraphNode * > predecessors_;
  Ast::NamespaceDirectoryEntry * entry_;
  bool visited_;
public:
  OrderGraphNode( Ast::NamespaceDirectoryEntry * entry )
  : entry_( entry ), visited_( false )
  { }
  void addPredecessor( OrderGraphNode * pre )
  {
    predecessors_.push_back( pre );
  }
  bool visited( ) const { return visited_; }
  std::string name( ) const { return entry_->name( ); }
  /* Returns NULL if no cycle was detected.
   * Otherwise, a pointer to a node on a cycle is returned.
   */
  OrderGraphNode * depthFirstVisit( std::stack< Ast::NamespaceDirectoryEntry * > * loadOrderStack )
  {
    if( visited_ ){
      return this;
    }
    visited_ = true;

    typedef typeof predecessors_ ListType;
    for( ListType::iterator i = predecessors_.begin( ); i != predecessors_.end( ); ++i ){
      OrderGraphNode * cycleNode = (*i)->depthFirstVisit( loadOrderStack );
      if( cycleNode != NULL )
        return cycleNode;
    }

    loadOrderStack->push( entry_ );

    return NULL;
  }
};

void
Ast::NamespaceDeclarations::invalidOrderGraphEntries( const std::map< std::string, NamespaceDirectoryEntry * > & entries, std::set< std::string > * invalidEntries ) const
{
  typedef typeof orderGraph_ OrderMapType;
  for( OrderMapType::const_iterator i = orderGraph_.begin( ); i != orderGraph_.end( ); ++i ){
    invalidEntries->insert( invalidEntries->begin( ), i->first );
    invalidEntries->insert( i->second.begin( ), i->second.end( ) );
  }
  typedef typeof entries EntriesMapType;
  for( EntriesMapType::const_iterator i = entries.begin( ); i != entries.end( ); ++i ){
    invalidEntries->erase( i->first );
  }
}


std::string
Ast::NamespaceDeclarations::orderEntries( const std::map< std::string, NamespaceDirectoryEntry * > & entries, std::vector< NamespaceDirectoryEntry * > * loadOrder ) const
{
  PtrOwner_back_Access< std::list< OrderGraphNode * > > nodeMemory;
  typedef std::map< std::string, OrderGraphNode * > NodeMap;
  NodeMap nodes;
  typedef typeof entries EntriesMapType;
  for( EntriesMapType::const_iterator i = entries.begin( ); i != entries.end( ); ++i ){
    OrderGraphNode * node = new OrderGraphNode( i->second );
    nodeMemory.push_back( node );
    nodes[ i->first ] = node;
  }

  for( NodeMap::const_iterator i = nodes.begin( ); i != nodes.end( ); ++i ){
    typedef typeof orderGraph_ OrderMapType;
    OrderMapType::const_iterator j = orderGraph_.find( i->first );
    if( j == orderGraph_.end( ) )
      continue;
    typedef typeof j->second SetType;
    for( SetType::const_iterator k = j->second.begin( ); k != j->second.end( ); ++j ){
      NodeMap::iterator l = nodes.find( *k );
      if( l == nodes.end( ) ){
        throw Exceptions::InternalError( "Ast::NamespaceDeclarations::orderEntries: Invalid entries should have been detected earlier." );
      }
      i->second->addPredecessor( l->second );
    }
  }

  std::stack< Ast::NamespaceDirectoryEntry * > loadOrderStack;
  for( NodeMap::iterator i = nodes.begin( ); i != nodes.end( ); ++i ){
    OrderGraphNode * node = i->second;
    if( node->visited( ) )
      continue;
    OrderGraphNode * cycleNode = node->depthFirstVisit( & loadOrderStack );
    if( cycleNode != NULL )
      return cycleNode->name( );
  }

  loadOrder->clear( );
  loadOrder->reserve( entries.size( ) );
  while( ! loadOrderStack.empty( ) ){
    loadOrder->push_back( loadOrderStack.top( ) );
    loadOrderStack.pop( );
  }

  return std::string( );
}


Ast::NamespaceDirectoryEntry::NamespaceDirectoryEntry( const std::string & name, const Ast::NamespaceDirectory * parent )
: name_( name ), parent_( parent ), prelude_( false )
{ }

Ast::NamespaceDirectoryEntry::~NamespaceDirectoryEntry( )
{ }

void
Ast::NamespaceDirectoryEntry::schedulePrelude( Ast::LoadStack * loadStack )
{
  this->schedule_impl( loadStack, true );
}

void
Ast::NamespaceDirectoryEntry::schedule( Ast::LoadStack * loadStack )
{
  this->schedule_impl( loadStack, false );
}


Ast::NamespaceDirectory::NamespaceDirectory( const RefCountPtr< const Ast::NamespacePath > & namespacePath, const std::string & filesystemPath, const std::string & name, const Ast::NamespaceDirectory * parent )
  : Ast::NamespaceDirectoryEntry( name, parent ), initialized_( false ), namespacePath_( namespacePath ), filesystemPath_( filesystemPath ), encapsulated_( false )
{ }

Ast::NamespaceDirectory::~NamespaceDirectory( )
{ }

void
Ast::NamespaceDirectory::schedule_impl( Ast::LoadStack * loadStack, bool preludeOnly )
{
  initialize( );

  typedef typeof loadOrder_ VecType;
  for( VecType::iterator i = loadOrder_.begin( ); i != loadOrder_.end( ); ++i ){
    if( preludeOnly && ! (*i)->prelude( ) )
      continue;
    (*i)->schedule_impl( loadStack, preludeOnly );
  }
}

void
Ast::NamespaceDirectory::scheduleNamespace( const Ast::NamespacePath & namespacePath, const Ast::NamespacePath::const_iterator & nsBegin, Ast::LoadStack * loadStack )
{
  initialize( );

  if( nsBegin == namespacePath.end( ) ){
    schedule( loadStack );
    return;
  }

  Ast::NamespacePath::const_iterator next( nsBegin );
  ++next;

  typedef typeof entries_ MapType;
  MapType::iterator i = entries_.find( *nsBegin );
  if( i == entries_.end( ) ){
    throw Exceptions::NamespaceDirectoryLookupError( Exceptions::NamespaceDirectoryLookupError::NOT_FOUND, namespacePath.begin( ), next );
  }

  NamespaceDirectory * dir = dynamic_cast< NamespaceDirectory * >( i->second );
  if( dir == NULL ){
    throw Exceptions::NamespaceDirectoryLookupError( Exceptions::NamespaceDirectoryLookupError::WRONG_TYPE, namespacePath.begin( ), next );
  }

  dir->scheduleNamespace( namespacePath, next, loadStack );
}

const Ast::FileID *
Ast::NamespaceDirectory::resolveSingleFile( const Ast::NamespacePath & namespacePath, const Ast::NamespacePath::const_iterator & nsBegin, const std::string & filename, const Ast::SourceLocation & loc )
{
  initialize( );

  if( nsBegin == namespacePath.end( ) ){

    typedef typeof entries_ MapType;
    MapType::iterator i = entries_.find( filename );
    if( i == entries_.end( ) ){
      throw Exceptions::NamespaceDirectoryFileError( loc, Exceptions::NamespaceDirectoryFileError::NOT_FOUND, namespacePath, filename );
    }

    NamespaceFile * fileEntry = dynamic_cast< NamespaceFile * >( i->second );
    if( fileEntry == NULL ){
      throw Exceptions::NamespaceDirectoryFileError( loc, Exceptions::NamespaceDirectoryFileError::WRONG_TYPE, namespacePath, filename );
    }

    return fileEntry->fileID();

  }

  Ast::NamespacePath::const_iterator next( nsBegin );
  ++next;

  typedef typeof entries_ MapType;
  MapType::iterator i = entries_.find( *nsBegin );
  if( i == entries_.end( ) ){
    throw Exceptions::NamespaceDirectoryLookupError( loc, Exceptions::NamespaceDirectoryLookupError::NOT_FOUND, namespacePath.begin( ), next );
  }

  NamespaceDirectory * dir = dynamic_cast< NamespaceDirectory * >( i->second );
  if( dir == NULL ){
    throw Exceptions::NamespaceDirectoryLookupError( loc, Exceptions::NamespaceDirectoryLookupError::WRONG_TYPE, namespacePath.begin( ), next );
  }

  return dir->resolveSingleFile( namespacePath, next, filename, loc );
}

std::string
Ast::NamespaceDirectory::name( ) const
{
  return filesystemPath_;
}

void
Ast::NamespaceDirectory::initialize( )
{
  if( initialized_ )
    return;
  initialized_ = true;

  std::string filename = Ast::theShapesScanner.needpathWithSuffix( filesystemPath_ + "/", "Shapes-Namespace.txt" );
  std::ifstream iFile( filename );
  struct stat iFileStat;
  if( stat( filename.c_str( ), & iFileStat ) != 0 || ! iFile.is_open( ) ){
    throw Exceptions::FileReadOpenError( Ast::THE_UNKNOWN_LOCATION, strrefdup( filename ), NULL, NULL );
  }
  const Ast::FileID * iFileID = Ast::FileID::build_stat( iFileStat, filename );

  NamespaceDeclarations declarations;
  NamespaceScanner scanner( & declarations, & iFile, iFileID );
  int status = scanner.yylex( );
  if( status != 0 )
    {
      std::ostringstream oss;
      oss << "Namespace scanner returned with non-zero status: " << status ;
      throw Exceptions::InternalError( strrefdup( oss ) );
    }

  encapsulated_ = declarations.encapsulated( );

  DIR* deDir = opendir( filesystemPath_.c_str( ) );
  if( deDir == NULL ){
    throw Exceptions::FileReadOpenError( Ast::THE_UNKNOWN_LOCATION, strrefdup( filesystemPath_ ), NULL, NULL );
  }

  for( struct dirent* de = readdir(deDir); de != NULL; de = readdir(deDir) ) {
    if (strcmp(de->d_name, ".") == 0 || strcmp(de->d_name, "..") == 0)
      continue;
    std::string name( de->d_name );
    if( declarations.inIgnoreSet( name ) )
      continue;
    std::string fullname = filesystemPath_ + "/" + name;
    struct stat theStat;
    if( stat( fullname.c_str( ), & theStat ) != 0 )
      continue; /* Strange directory entry, ignore it. */
    if( ( theStat.st_mode & S_IFDIR ) != 0 ){
      addDirectory( name, fullname, declarations.inPreludeSet( name ) );
    }else if( ( theStat.st_mode & S_IFREG ) != 0 ){
      if( name.compare( name.size( ) - 6, std::string::npos, ".shext" ) == 0 ){
        const Ast::FileID * fileID = Ast::FileID::build_stat( theStat, fullname );
        NamespaceFile * entry = new NamespaceFile( fileID, name, this );
        if( declarations.inPreludeSet( name ) )
          entry->setPrelude( );
        entries_[ name ] = entry;
      }
    }else{
      /* Other type of file, ignore it. */
    }
  }
  closedir( deDir );

  std::set< std::string > invalidEntries;
  declarations.invalidOrderGraphEntries( entries_, & invalidEntries );
  if( ! invalidEntries.empty( ) ){
    throw Exceptions::NamespaceDirectoryInvalidEntries( namespacePath_, invalidEntries );
  }

  std::string cycleEntry = declarations.orderEntries( entries_, & loadOrder_ );
  if( ! cycleEntry.empty( ) ){
    throw Exceptions::NamespaceDirectoryCircularOrdering( namespacePath_, cycleEntry );
  }
}

bool
Ast::NamespaceDirectory::addDirectory( const std::string & nsName, const std::string & dir, bool prelude )
{
  std::string filename = Ast::theShapesScanner.needpathWithSuffix( dir + "/", "Shapes-Namespace.txt" );
  std::ifstream iFile( filename );
  if( ! iFile.is_open( ) )
    return false;

  typedef typeof entries_ MapType;
  MapType::iterator i = entries_.find( nsName );
  if( i != entries_.end( ) ){
    throw Exceptions::AmbiguousNamespaceDirectory( i->second->name( ), dir );
  }

  Ast::NamespacePath * pathMem = new Ast::NamespacePath( *namespacePath_ );
  RefCountPtr< const Ast::NamespacePath > path( pathMem );
  /* Leaking memory with each strdup below, see 'Memory management notice' in identifier.h. */
  if( encapsulated_ )
    pathMem->push_back( strdup( Ast::theShapesScanner.newEncapsulationName( ).getPtr( ) ) );
  pathMem->push_back( strdup( nsName.c_str( ) ) );
  NamespaceDirectory * entry = new NamespaceDirectory( path, dir, nsName, this );
  if( prelude )
    entry->setPrelude( );
  entries_[ nsName ] = entry;

  return true;
}


Ast::NamespaceFile::NamespaceFile( const FileID * fileID, const std::string & name, const Ast::NamespaceDirectory * parent )
  : Ast::NamespaceDirectoryEntry( name, parent), fileID_( fileID )
{ }

Ast::NamespaceFile::~NamespaceFile( )
{ }

void
Ast::NamespaceFile::schedule_impl( Ast::LoadStack * loadStack, bool preludeOnly )
{
  if( ! prelude_ && preludeOnly ){
    throw Exceptions::InternalError( "Ast::NamespaceFile::schedule was called on a non-prelude file while loading the prelude." );
  }
  loadStack->push( fileID_, parent_->namespacePath( ) );
}

std::string
Ast::NamespaceFile::name( ) const
{
  return fileID_->name( );
}


Ast::LoadStack::LoadStack( )
{ }

void
Ast::LoadStack::push( const FileID * fileID, const RefCountPtr< const Ast::NamespacePath > & namespacePath )
{
  fileIDs_.push( fileID );
  namespacePaths_.push( namespacePath );
}

void
Ast::LoadStack::pop( )
{
  fileIDs_.pop( );
  namespacePaths_.pop( );
}




Ast::NamespaceLoader::NamespaceLoader( )
{ }

Ast::NamespaceLoader::~NamespaceLoader( )
{
  typedef typeof topEntries_ MapType;
  for( MapType::iterator i = topEntries_.begin( ); i != topEntries_.end( ); ++i ){
    delete i->second;
  }
}

void
Ast::NamespaceLoader::registerNeedDirectory( const std::string & dir )
{
  struct stat theStat;
  if( stat( dir.c_str( ), & theStat ) != 0 )
    return;
  if( ( theStat.st_mode & S_IFDIR ) == 0 ){
    /* Ignore non-directory */
    return;
  }

  /* See if the directory itself is a namespace directory. */
  {
    char* dirstr = strdup( dir.c_str( ) );
    std::string nsName( basename( dirstr ) );
    free(dirstr);
    if( addDirectory( nsName, dir ) )
      return;
  }

  /* See if any of the subdirectories are namespace directories. */
  DIR* deDir = opendir( dir.c_str( ) );
  while (deDir != NULL) {
    struct dirent* de = readdir(deDir);
    if (de == NULL) break;
    if (strcmp(de->d_name, ".") == 0 || strcmp(de->d_name, "..") == 0)
      continue;
    std::string nsName( de->d_name );
    std::string subdir = dir + "/" + nsName;
    if( stat( subdir.c_str( ), & theStat ) != 0 )
      continue; /* Strange directory entry, ignore it. */
    if( ( theStat.st_mode & S_IFDIR ) == 0 )
      continue;
    addDirectory( nsName, subdir );
  }
  closedir( deDir );
}

bool
Ast::NamespaceLoader::addDirectory( const std::string & nsName, const std::string & dir )
{
  std::string filename = Ast::theShapesScanner.needpathWithSuffix( dir + "/", "Shapes-Namespace.txt" );
  std::ifstream iFile( filename );
  if( ! iFile.is_open( ) )
    return false;

  typedef typeof topEntries_ MapType;
  MapType::iterator i = topEntries_.find( nsName );
  if( i != topEntries_.end( ) ){
    throw Exceptions::AmbiguousNamespaceDirectory( i->second->name( ), dir );
  }

  Ast::NamespacePath * pathMem = new Ast::NamespacePath( );
  RefCountPtr< const Ast::NamespacePath > path( pathMem );
  pathMem->push_back( strdup( nsName.c_str( ) ) ); /* Leaking memory, see 'Memory management notice' in identifier.h. */
  topEntries_[ nsName ] = new NamespaceDirectory( path, dir, nsName, NULL );

  return true;
}

void
Ast::NamespaceLoader::pushPreludeFiles( LoadStack * loadStack )
{
  typedef typeof topEntries_ MapType;
  for( MapType::iterator i = topEntries_.begin( ); i != topEntries_.end( ); ++i ){
    i->second->schedulePrelude( loadStack ); /* true means to only schedule the prelude. */
  }
}

void
Ast::NamespaceLoader::pushNamespaceFiles( const Ast::NamespacePath & namespacePath, LoadStack * loadStack )
{
  if( namespacePath.empty() ){
    throw Exceptions::NamespaceDirectoryLookupError( Exceptions::NamespaceDirectoryLookupError::GLOBAL, namespacePath.begin( ), namespacePath.end( ) );
  }

  Ast::NamespacePath::const_iterator begin = namespacePath.begin( );
  Ast::NamespacePath::const_iterator next = begin;
  ++next;

  typedef typeof topEntries_ MapType;
  MapType::iterator i = topEntries_.find( *begin );
  if( i == topEntries_.end( ) ){
    throw Exceptions::NamespaceDirectoryLookupError( Exceptions::NamespaceDirectoryLookupError::NOT_FOUND, namespacePath.begin( ), next );
  }

  i->second->scheduleNamespace( namespacePath, next, loadStack );
}

const Ast::FileID *
Ast::NamespaceLoader::resolveSingleFile( const Ast::NamespacePath & namespacePath, const std::string & filename, const Ast::SourceLocation & loc )
{
  if( namespacePath.empty() ){
    throw Exceptions::NamespaceDirectoryLookupError( loc, Exceptions::NamespaceDirectoryLookupError::GLOBAL, namespacePath.begin( ), namespacePath.end( ) );
  }

  Ast::NamespacePath::const_iterator begin = namespacePath.begin( );
  Ast::NamespacePath::const_iterator next = begin;
  ++next;

  typedef typeof topEntries_ MapType;
  MapType::iterator i = topEntries_.find( *begin );
  if( i == topEntries_.end( ) ){
    throw Exceptions::NamespaceDirectoryLookupError( loc, Exceptions::NamespaceDirectoryLookupError::NOT_FOUND, namespacePath.begin( ), next );
  }

  return i->second->resolveSingleFile( namespacePath, next, filename, loc );
}
