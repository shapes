/* This file is part of Shapes.
 *
 * Shapes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Shapes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2008, 2013, 2014 Henrik Tidefelt
 */

#include "Shapes_Helpers_decls.h"

#include "continuations.h"
#include "hottypes.h"
#include "globals.h"
#include "shapescore.h"
#include "functiontypes.h"
#include "astfun.h"
#include "astvalues.h"
#include "singlelistrange.h"

using namespace Shapes;


Kernel::IfContinuation::IfContinuation( const Kernel::VariableHandle & consequence, const Kernel::VariableHandle & alternative, const Kernel::ContRef & cont, const Ast::SourceLocation & traceLoc )
	: Kernel::Continuation( traceLoc ), consequence_( consequence ), alternative_( alternative ), cont_( cont )
{ }

Kernel::IfContinuation::~IfContinuation( )
{ }

void
Kernel::IfContinuation::takeValue( const RefCountPtr< const Lang::Value > & val, Kernel::EvalState * evalState, bool dummy ) const
{
	typedef const Lang::Boolean ArgType;
	RefCountPtr< ArgType > arg = Helpers::down_cast_ContinuationArgument< ArgType >( val, this );

	if( arg->val_ )
		{
			evalState->cont_ = cont_;
			cont_->takeHandle( consequence_,
												evalState );
		}
	else
		{
			evalState->cont_ = cont_;
			cont_->takeHandle( alternative_,
												evalState );
		}
}

Kernel::ContRef
Kernel::IfContinuation::up( ) const
{
	return cont_;
}

RefCountPtr< const char >
Kernel::IfContinuation::description( ) const
{
	return strrefdup( "if" );
}

void
Kernel::IfContinuation::gcMark( Kernel::GCMarkedSet & marked )
{
	consequence_->gcMark( marked );
	alternative_->gcMark( marked );
	cont_->gcMark( marked );
}


Kernel::IgnoreContinuation::IgnoreContinuation( const Kernel::ContRef & cont, const Ast::SourceLocation & traceLoc )
	: Kernel::Continuation( traceLoc ), cont_( cont )
{ }

Kernel::IgnoreContinuation::~IgnoreContinuation( )
{ }

void
Kernel::IgnoreContinuation::takeHandle( Kernel::VariableHandle, Kernel::EvalState * evalState, bool dummy ) const
{
	evalState->cont_ = cont_;
	cont_->takeHandle( Kernel::THE_VOID_VARIABLE, evalState );
}

Kernel::ContRef
Kernel::IgnoreContinuation::up( ) const
{
	return cont_;
}

RefCountPtr< const char >
Kernel::IgnoreContinuation::description( ) const
{
	return strrefdup( "ignore" );
}

void
Kernel::IgnoreContinuation::gcMark( Kernel::GCMarkedSet & marked )
{
	cont_->gcMark( marked );
}


Kernel::IntroduceStateContinuation::IntroduceStateContinuation( const Kernel::PassedEnv & env, size_t * pos, const Kernel::ContRef & cont, const Ast::SourceLocation & traceLoc )
	: Kernel::Continuation( traceLoc ), env_( env ), pos_( pos ), cont_( cont )
{ }

Kernel::IntroduceStateContinuation::~IntroduceStateContinuation( )
{ }

void
Kernel::IntroduceStateContinuation::takeValue( const RefCountPtr< const Lang::Value > & val, Kernel::EvalState * evalState, bool dummy ) const
{
	typedef const Lang::Hot ArgType;
	RefCountPtr< ArgType > hot( Helpers::down_cast_ContinuationArgument< ArgType >( val, this ) );
	env_->introduceState( *pos_, hot->newState( ) );
	evalState->cont_ = cont_;
	cont_->takeHandle( Kernel::THE_VOID_VARIABLE, evalState );
}

Kernel::ContRef
Kernel::IntroduceStateContinuation::up( ) const
{
	return cont_;
}

RefCountPtr< const char >
Kernel::IntroduceStateContinuation::description( ) const
{
	return strrefdup( "introduce state" );
}

void
Kernel::IntroduceStateContinuation::gcMark( Kernel::GCMarkedSet & marked )
{
	env_->gcMark( marked );
	cont_->gcMark( marked );
}


Kernel::StoreValueContinuation::StoreValueContinuation( Kernel::ValueRef * res, const Kernel::ContRef & cont, const Ast::SourceLocation & traceLoc )
	: Kernel::Continuation( traceLoc ), res_( res ), cont_( cont )
{ }

Kernel::StoreValueContinuation::~StoreValueContinuation( )
{ }

void
Kernel::StoreValueContinuation::takeHandle( Kernel::VariableHandle val, Kernel::EvalState * evalState, bool dummy ) const
{
	if( val->isThunk( ) )
		{
			val->force( val, evalState );
		}
	else
		{
			*res_ = val->getUntyped( );
			evalState->cont_ = cont_;
			cont_->takeHandle( val, evalState );
		}
}

Kernel::ContRef
Kernel::StoreValueContinuation::up( ) const
{
	return cont_;
}

RefCountPtr< const char >
Kernel::StoreValueContinuation::description( ) const
{
	return strrefdup( "store and return" );
}

void
Kernel::StoreValueContinuation::gcMark( Kernel::GCMarkedSet & marked )
{
	cont_->gcMark( marked );
}


Kernel::StoreVariableContinuation::StoreVariableContinuation( const Kernel::VariableHandle & dst, const Kernel::ContRef & cont, const Ast::SourceLocation & traceLoc )
	: Kernel::Continuation( traceLoc ), dst_( dst ), cont_( cont )
{ }

Kernel::StoreVariableContinuation::~StoreVariableContinuation( )
{ }

void
Kernel::StoreVariableContinuation::takeHandle( Kernel::VariableHandle val, Kernel::EvalState * evalState, bool dummy ) const
{
	if( val->isThunk( ) )
		{
			val->force( val, evalState );
		}
	else
		{
			try{
				/* This class is friends with Variable, hence setValue is accessible. */
				dst_->setValue( val->getUntyped( ) );
			}catch( const Exceptions::BadSetValueState & ball ){
				throw Exceptions::InternalError( traceLoc_, "Multiple uses of continuation that stores value in variable." );
			}
			evalState->cont_ = cont_;
			cont_->takeHandle( val, evalState );
		}
}

Kernel::ContRef
Kernel::StoreVariableContinuation::up( ) const
{
	return cont_;
}

RefCountPtr< const char >
Kernel::StoreVariableContinuation::description( ) const
{
	return strrefdup( "store and return" );
}

void
Kernel::StoreVariableContinuation::gcMark( Kernel::GCMarkedSet & marked )
{
	dst_->gcMark( marked );
	cont_->gcMark( marked );
}


Kernel::InsertionContinuation::InsertionContinuation( const Kernel::StateHandle & dst, const Kernel::ContRef & cont, const Kernel::PassedDyn & dyn, const Ast::SourceLocation & traceLoc )
	: Kernel::Continuation( traceLoc ), dst_( dst ), dyn_( dyn ), cont_( cont )
{ }

Kernel::InsertionContinuation::~InsertionContinuation( )
{ }

void
Kernel::InsertionContinuation::takeValue( const RefCountPtr< const Lang::Value > & val, Kernel::EvalState * evalState, bool dummy ) const
{
	evalState->cont_ = cont_;
	evalState->dyn_ = dyn_;  /* This is how we pass dyn_ to dst_->tackOn .  Additionally passing it as a separate argument is (used to be) very confusing. */
	dst_->tackOn( evalState, val, traceLoc_ );
}

Kernel::ContRef
Kernel::InsertionContinuation::up( ) const
{
	return cont_;
}

RefCountPtr< const char >
Kernel::InsertionContinuation::description( ) const
{
	return strrefdup( "insertion" );
}

void
Kernel::InsertionContinuation::gcMark( Kernel::GCMarkedSet & marked )
{
	cont_->gcMark( marked );
	dyn_->gcMark( marked );
}


Kernel::StmtStoreValueContinuation::StmtStoreValueContinuation( Kernel::ValueRef * res, const Kernel::ContRef & cont, const Ast::SourceLocation & traceLoc )
	: Kernel::Continuation( traceLoc ), res_( res ), cont_( cont )
{ }

Kernel::StmtStoreValueContinuation::~StmtStoreValueContinuation( )
{ }

void
Kernel::StmtStoreValueContinuation::takeValue( const RefCountPtr< const Lang::Value > & val, Kernel::EvalState * evalState, bool dummy ) const
{
	*res_ = val;
	evalState->cont_ = cont_;
	cont_->takeHandle( Kernel::THE_VOID_VARIABLE, evalState );
}

Kernel::ContRef
Kernel::StmtStoreValueContinuation::up( ) const
{
	return cont_;
}

RefCountPtr< const char >
Kernel::StmtStoreValueContinuation::description( ) const
{
	return strrefdup( "statement store (value)" );
}

void
Kernel::StmtStoreValueContinuation::gcMark( Kernel::GCMarkedSet & marked )
{
	cont_->gcMark( marked );
}


Kernel::StmtStoreVariableContinuation::StmtStoreVariableContinuation( const Kernel::VariableHandle & dst, const Kernel::ContRef & cont, const Ast::SourceLocation & traceLoc )
	: Kernel::Continuation( traceLoc ), dst_( dst ), cont_( cont )
{ }

Kernel::StmtStoreVariableContinuation::~StmtStoreVariableContinuation( )
{ }

void
Kernel::StmtStoreVariableContinuation::takeValue( const RefCountPtr< const Lang::Value > & val, Kernel::EvalState * evalState, bool dummy ) const
{
	/* This class is friends with Variable, hence setValue is accessible. */
	dst_->setValue( val );
	evalState->cont_ = cont_;
	cont_->takeHandle( Kernel::THE_VOID_VARIABLE, evalState );
}

Kernel::ContRef
Kernel::StmtStoreVariableContinuation::up( ) const
{
	return cont_;
}

RefCountPtr< const char >
Kernel::StmtStoreVariableContinuation::description( ) const
{
	return strrefdup( "statement store (variable)" );
}

void
Kernel::StmtStoreVariableContinuation::gcMark( Kernel::GCMarkedSet & marked )
{
	dst_->gcMark( marked );
	cont_->gcMark( marked );
}



Kernel::ForcingContinuation::ForcingContinuation( const Kernel::ContRef & cont, const Ast::SourceLocation & traceLoc )
	: Kernel::Continuation( traceLoc ), cont_( cont )
{ }

Kernel::ForcingContinuation::~ForcingContinuation( )
{ }

void
Kernel::ForcingContinuation::takeValue( const RefCountPtr< const Lang::Value > & val, Kernel::EvalState * evalState, bool dummy ) const
{
	/* The reason for using this is simply that it only takes values.  Then, the value
	 * is passed to whatever surrounding continuation.
	 */
	evalState->cont_ = cont_;
	cont_->takeValue( val, evalState );
}

Kernel::ContRef
Kernel::ForcingContinuation::up( ) const
{
	return cont_;
}

RefCountPtr< const char >
Kernel::ForcingContinuation::description( ) const
{
	return strrefdup( "force evaluation" );
}

void
Kernel::ForcingContinuation::gcMark( Kernel::GCMarkedSet & marked )
{
	cont_->gcMark( marked );
}


Kernel::ForcingListContinuation::ForcingListContinuation( const Kernel::ContRef & cont, const Ast::SourceLocation & traceLoc, bool forceStructures, bool consify )
	: Kernel::Continuation( traceLoc ), cont_( cont ), forceStructures_( forceStructures ), consify_( consify ), index_( 0 ), pile_( Lang::THE_CONS_NULL )
{ }

Kernel::ForcingListContinuation::ForcingListContinuation( bool forceStructures, bool consify, size_t index, const RefCountPtr< const Lang::SingleList > & pile, const Kernel::ContRef & cont, const Ast::SourceLocation & traceLoc )
	: Kernel::Continuation( traceLoc ), cont_( cont ), forceStructures_( forceStructures ), consify_( consify ), index_( index ), pile_( pile )
{ }

Kernel::ForcingListContinuation::~ForcingListContinuation( )
{ }

void
Kernel::ForcingListContinuation::takeValue( const RefCountPtr< const Lang::Value > & val, Kernel::EvalState * evalState, bool dummy ) const
{
	{
		RefCountPtr< const Lang::SingleList > rest = val.down_cast< const Lang::SingleList >( );
		if( rest != NullPtr< const Lang::SingleList >( ) ){
			RefCountPtr< const Lang::SingleList > pile = pile_;
			RefCountPtr< const Lang::SingleList > result = Lang::THE_CONS_NULL;

			typedef const Lang::SingleListPair PairType;
			PairType * p = dynamic_cast< PairType * >( rest.getPtr( ) );
			if( p != 0 )
				{
					if( index_ == 0 && p->isForced( ) ){
						/* Efficient code should end up here most of the time. */
						if( ! forceStructures_ ){
							evalState->cont_ = cont_;
							cont_->takeValue( val, evalState );
							return;
						}
					}
					size_t index = index_;
					while( p != 0 ){
						if( p->car_->isThunk( ) ){
							evalState->cont_ = Kernel::ContRef( new Kernel::ForcingListCarContinuation( forceStructures_, consify_, index, p->cdr_, pile, cont_, traceLoc_ ) );
							Kernel::VariableHandle carCopy = p->car_.unconst_cast< Kernel::Variable >( );
							carCopy->force( carCopy, evalState );
							return;
						}
						RefCountPtr< const Lang::Value > carVal = p->car_->getUntyped( );
						if( forceStructures_ && carVal.down_cast< const Lang::Structure >( ) != NullPtr< const Lang::Structure >( ) ){
							evalState->cont_ = Kernel::ContRef( new Kernel::ForcingListCarStructureContinuation( consify_, index, p->cdr_, pile, cont_, traceLoc_ ) );
							Kernel::ContRef cont = evalState->cont_;
							cont->takeValue( carVal, evalState );
							return;
						}
						pile = Helpers::SingleList_cons( carVal, pile );
						rest = p->cdr_;
						p = dynamic_cast< PairType * >( rest.getPtr( ) );
						++index;
					}
				}

			if( rest->isNull( ) ){
				goto reverse;
			}

			{
				typedef const Lang::SingleListRange< const Lang::Integer > SpanType;
				RefCountPtr< SpanType > span = rest.down_cast< SpanType >( );
				if( span != NullPtr< SpanType >( ) )
					{
						if( index_ == 0 && ! consify_ ){
							evalState->cont_ = cont_;
							cont_->takeValue( val, evalState ); /* val and rest are the same since index is 0. */
							return;
						}
						if( consify_ ){
							result = span->consify( );
						}else{
							result = span;
						}
						goto reverse;
					}
			}
			{
				typedef const Lang::SingleListRange< const Lang::Float > SpanType;
				RefCountPtr< SpanType > span = rest.down_cast< SpanType >( );
				if( span != NullPtr< SpanType >( ) )
					{
						if( index_ == 0 && ! consify_ ){
							evalState->cont_ = cont_;
							cont_->takeValue( val, evalState ); /* val and rest are the same since index is 0. */
							return;
						}
						if( consify_ ){
							result = span->consify( );
						}else{
							result = span;
						}
						goto reverse;
					}
			}
			{
				typedef const Lang::SingleListRange< const Lang::Length > SpanType;
				RefCountPtr< SpanType > span = rest.down_cast< SpanType >( );
				if( span != NullPtr< SpanType >( ) )
					{
						if( index_ == 0 && ! consify_ ){
							evalState->cont_ = cont_;
							cont_->takeValue( val, evalState ); /* val and rest are the same since index is 0. */
							return;
						}
						if( consify_ ){
							result = span->consify( );
						}else{
							result = span;
						}
						goto reverse;
					}
			}

			throw Exceptions::InternalError( "Kernel::ForcingListContinuation::takeValue: Uncovered subtype of SingleList." );

		reverse:
			/* We're done forcing.  Reverse the accumulated result. */
			{
				typedef const Lang::SingleListPair PairType;
				PairType * pTyped = dynamic_cast< PairType * >( pile.getPtr( ) );
				while( pTyped != 0 ){
					result = RefCountPtr< const Lang::SingleListPair >( new Lang::SingleListPair( pTyped->car_, result ) );
					pTyped = dynamic_cast< PairType * >( pTyped->cdr_.getPtr( ) );
				}
			}
			evalState->cont_ = cont_;
			cont_->takeValue( result, evalState );
			return;
		}
	}

	{
		typedef const Lang::ConsPair PairType;
		PairType * p = dynamic_cast< PairType * >( val.getPtr( ) );
		if( p != 0 )
			{
				evalState->cont_ = Kernel::ContRef( new Kernel::ForcingConsCarContinuation( forceStructures_, consify_, index_, p->cdr( ), pile_, cont_, traceLoc_ ) );
				Kernel::VariableHandle carCopy = p->car( ).unconst_cast< Kernel::Variable >( );
				carCopy->force( carCopy, evalState );
				return;
			}
	}

	throw Exceptions::ExpectedList( traceLoc_, index_, val->getTypeName( ) );
}

Kernel::ContRef
Kernel::ForcingListContinuation::up( ) const
{
	return cont_;
}

RefCountPtr< const char >
Kernel::ForcingListContinuation::description( ) const
{
	return strrefdup( "force all elements in list" );
}

void
Kernel::ForcingListContinuation::gcMark( Kernel::GCMarkedSet & marked )
{
	cont_->gcMark( marked );
	const_cast< Lang::SingleList * >( pile_.getPtr( ) )->gcMark( marked );
}


Kernel::ForcingListCarContinuation::ForcingListCarContinuation( bool forceStructures, bool consify, size_t index, const RefCountPtr< const Lang::SingleList > & cdr, const RefCountPtr< const Lang::SingleList > & pile, const Kernel::ContRef & cont, const Ast::SourceLocation & traceLoc )
	: Kernel::Continuation( traceLoc ), cont_( cont ), forceStructures_( forceStructures ), consify_( consify ), index_( index ), cdr_( cdr ), pile_( pile )
{ }

Kernel::ForcingListCarContinuation::~ForcingListCarContinuation( )
{ }

void
Kernel::ForcingListCarContinuation::takeValue( const RefCountPtr< const Lang::Value > & val, Kernel::EvalState * evalState, bool dummy ) const
{
	if( forceStructures_ && val.down_cast< const Lang::Structure >( ) != NullPtr< const Lang::Structure >( ) ){
		Kernel::ContRef cont = Kernel::ContRef( new Kernel::ForcingListCarStructureContinuation( consify_, index_, cdr_, pile_, cont_, traceLoc_ ) );
		evalState->cont_ = cont;
		cont->takeValue( val, evalState );
		return;
	}

	evalState->cont_ = Kernel::ContRef( new Kernel::ForcingListContinuation( forceStructures_, consify_, index_ + 1, Helpers::SingleList_cons( val, pile_ ), cont_, traceLoc_ ) );
	Kernel::ContRef cont = evalState->cont_;
	cont->takeValue( cdr_, evalState );
}

Kernel::ContRef
Kernel::ForcingListCarContinuation::up( ) const
{
	return cont_;
}

RefCountPtr< const char >
Kernel::ForcingListCarContinuation::description( ) const
{
	return strrefdup( "force element in list (singly linked)" );
}

void
Kernel::ForcingListCarContinuation::gcMark( Kernel::GCMarkedSet & marked )
{
	const_cast< Lang::SingleList * >( cdr_.getPtr( ) )->gcMark( marked );
	const_cast< Lang::SingleList * >( pile_.getPtr( ) )->gcMark( marked );
	cont_->gcMark( marked );
}

Kernel::ForcingListCarStructureContinuation::ForcingListCarStructureContinuation( bool consify, size_t index, const RefCountPtr< const Lang::SingleList > & cdr, const RefCountPtr< const Lang::SingleList > & pile, const Kernel::ContRef & cont, const Ast::SourceLocation & traceLoc )
	: Kernel::ForcedStructureContinuation( "Forcing structures in list (singly linked)", traceLoc ), cont_( cont ), consify_( consify ), index_( index ), cdr_( cdr ), pile_( pile )
{ }

Kernel::ForcingListCarStructureContinuation::~ForcingListCarStructureContinuation( )
{ }

void
Kernel::ForcingListCarStructureContinuation::takeStructure( const RefCountPtr< const Lang::Structure > & structure, Kernel::EvalState * evalState ) const
{
	evalState->cont_ = Kernel::ContRef( new Kernel::ForcingListContinuation( true, consify_, index_ + 1, Helpers::SingleList_cons( structure, pile_ ), cont_, traceLoc_ ) );
	Kernel::ContRef cont = evalState->cont_;
	cont->takeValue( cdr_, evalState );
}

Kernel::ContRef
Kernel::ForcingListCarStructureContinuation::up( ) const
{
	return cont_;
}

RefCountPtr< const char >
Kernel::ForcingListCarStructureContinuation::description( ) const
{
	return strrefdup( "force structure element in list (singly linked)" );
}

void
Kernel::ForcingListCarStructureContinuation::gcMark( Kernel::GCMarkedSet & marked )
{
	const_cast< Lang::SingleList * >( cdr_.getPtr( ) )->gcMark( marked );
	const_cast< Lang::SingleList * >( pile_.getPtr( ) )->gcMark( marked );
	cont_->gcMark( marked );
}

Kernel::ForcingConsCarContinuation::ForcingConsCarContinuation( bool forceStructures, bool consify, size_t index, const Kernel::VariableHandle & cdr, const RefCountPtr< const Lang::SingleList > & pile, const Kernel::ContRef & cont, const Ast::SourceLocation & traceLoc )
	: Kernel::Continuation( traceLoc ), cont_( cont ), forceStructures_( forceStructures ), consify_( consify ), index_( index ), cdr_( cdr ), pile_( pile )
{ }

Kernel::ForcingConsCarContinuation::~ForcingConsCarContinuation( )
{ }

void
Kernel::ForcingConsCarContinuation::takeValue( const RefCountPtr< const Lang::Value > & val, Kernel::EvalState * evalState, bool dummy ) const
{
	if( forceStructures_ && val.down_cast< const Lang::Structure >( ) != NullPtr< const Lang::Structure >( ) ){
		Kernel::ContRef cont = Kernel::ContRef( new Kernel::ForcingConsCarStructureContinuation( consify_, index_, cdr_, pile_, cont_, traceLoc_ ) );
		evalState->cont_ = cont;
		cont->takeValue( val, evalState );
		return;
	}

	evalState->cont_ = Kernel::ContRef( new Kernel::ForcingListContinuation( forceStructures_, consify_, index_ + 1, Helpers::SingleList_cons( val, pile_ ), cont_, traceLoc_ ) );
	Kernel::VariableHandle cdrCopy = cdr_.unconst_cast< Kernel::Variable >( );
	cdrCopy->force( cdrCopy, evalState );
}

Kernel::ContRef
Kernel::ForcingConsCarContinuation::up( ) const
{
	return cont_;
}

RefCountPtr< const char >
Kernel::ForcingConsCarContinuation::description( ) const
{
	return strrefdup( "force element in list (cons)" );
}

void
Kernel::ForcingConsCarContinuation::gcMark( Kernel::GCMarkedSet & marked )
{
	cdr_.getPtr( )->gcMark( marked );
	const_cast< Lang::SingleList * >( pile_.getPtr( ) )->gcMark( marked );
	cont_->gcMark( marked );
}

Kernel::ForcingConsCarStructureContinuation::ForcingConsCarStructureContinuation( bool consify, size_t index, const Kernel::VariableHandle & cdr, const RefCountPtr< const Lang::SingleList > & pile, const Kernel::ContRef & cont, const Ast::SourceLocation & traceLoc )
	: Kernel::ForcedStructureContinuation( "Forcing structures in list (cons list)", traceLoc ), cont_( cont ), consify_( consify ), index_( index ), cdr_( cdr ), pile_( pile )
{ }

Kernel::ForcingConsCarStructureContinuation::~ForcingConsCarStructureContinuation( )
{ }

void
Kernel::ForcingConsCarStructureContinuation::takeStructure( const RefCountPtr< const Lang::Structure > & structure, Kernel::EvalState * evalState ) const
{
	evalState->cont_ = Kernel::ContRef( new Kernel::ForcingListContinuation( true, consify_, index_ + 1, Helpers::SingleList_cons( structure, pile_ ), cont_, traceLoc_ ) );
	Kernel::VariableHandle cdrCopy = cdr_.unconst_cast< Kernel::Variable >( );
	cdrCopy->force( cdrCopy, evalState );
}

Kernel::ContRef
Kernel::ForcingConsCarStructureContinuation::up( ) const
{
	return cont_;
}

RefCountPtr< const char >
Kernel::ForcingConsCarStructureContinuation::description( ) const
{
	return strrefdup( "force structure element in list (cons)" );
}

void
Kernel::ForcingConsCarStructureContinuation::gcMark( Kernel::GCMarkedSet & marked )
{
	cdr_.getPtr( )->gcMark( marked );
	const_cast< Lang::SingleList * >( pile_.getPtr( ) )->gcMark( marked );
	cont_->gcMark( marked );
}


Kernel::ExitContinuation::ExitContinuation( bool * done, const Ast::SourceLocation & traceLoc )
	: Kernel::Continuation( traceLoc ), done_( done )
{ }

Kernel::ExitContinuation::~ExitContinuation( )
{ }

void
Kernel::ExitContinuation::takeHandle( Kernel::VariableHandle val, Kernel::EvalState * evalState, bool dummy ) const
{
	*done_ = true;
}

Kernel::ContRef
Kernel::ExitContinuation::up( ) const
{
	return Kernel::ContRef( NullPtr< Kernel::Continuation >( ) );
}

RefCountPtr< const char >
Kernel::ExitContinuation::description( ) const
{
	return strrefdup( "exit (non-forcing)" );
}

void
Kernel::ExitContinuation::gcMark( Kernel::GCMarkedSet & marked )
{ }


Kernel::DefaultErrorContinuation::DefaultErrorContinuation( const Ast::SourceLocation & traceLoc )
	: Kernel::Continuation( traceLoc )
{ }

Kernel::DefaultErrorContinuation::~DefaultErrorContinuation( )
{ }

void
Kernel::DefaultErrorContinuation::takeValue( const RefCountPtr< const Lang::Value > & val, Kernel::EvalState * evalState, bool dummy ) const
{
	throw Exceptions::UncaughtError( Helpers::down_cast_ContinuationArgument< const Lang::Exception >( val, this ) );
}

Kernel::ContRef
Kernel::DefaultErrorContinuation::up( ) const
{
	return Kernel::ContRef( NullPtr< Kernel::Continuation >( ) );
}

RefCountPtr< const char >
Kernel::DefaultErrorContinuation::description( ) const
{
	return strrefdup( "default error continuation" );
}

void
Kernel::DefaultErrorContinuation::gcMark( Kernel::GCMarkedSet & marked )
{ }


Kernel::Transform2DCont::Transform2DCont( Lang::Transform2D tf, const Kernel::ContRef & cont, const Ast::SourceLocation & traceLoc )
	: Kernel::Continuation( traceLoc ), tf_( tf ), cont_( cont )
{ }

Kernel::Transform2DCont::~Transform2DCont( )
{ }

void
Kernel::Transform2DCont::takeValue( const RefCountPtr< const Lang::Value > & val, Kernel::EvalState * evalState, bool dummy ) const
{
	try
		{
			typedef const Lang::Geometric2D ArgType;
			RefCountPtr< ArgType > arg = Helpers::try_cast_CoreArgument< ArgType >( val );
			evalState->cont_ = cont_;
			cont_->takeValue( arg->transformed( tf_, arg ),
												evalState );
			return;
		}
	catch( const NonLocalExit::NotThisType & ball )
		{
			/* Wrong type; never mind!.. but see below!
			 */
		}

	try
		{
			typedef const Lang::FloatPair ArgType;
			RefCountPtr< ArgType > arg = Helpers::try_cast_CoreArgument< ArgType >( val );
			evalState->cont_ = cont_;
			cont_->takeValue( arg->transformed( tf_ ),
												evalState );
			return;
		}
	catch( const NonLocalExit::NotThisType & ball )
		{
			/* Wrong type; never mind!.. but see below!
			 */
		}

	throw Exceptions::ContinuationTypeMismatch( this, val->getTypeName( ), Helpers::typeSetString( Lang::Geometric2D::staticTypeName( ), Lang::FloatPair::staticTypeName( ) ) );
}

Kernel::ContRef
Kernel::Transform2DCont::up( ) const
{
	return cont_;
}

RefCountPtr< const char >
Kernel::Transform2DCont::description( ) const
{
	return strrefdup( "2D transform application" );
}

void
Kernel::Transform2DCont::gcMark( Kernel::GCMarkedSet & marked )
{
	cont_->gcMark( marked );
}


Kernel::Transform3DCont::Transform3DCont( Lang::Transform3D tf, const Kernel::ContRef & cont, const Ast::SourceLocation & traceLoc )
	: Kernel::Continuation( traceLoc ), tf_( tf ), cont_( cont )
{ }

Kernel::Transform3DCont::~Transform3DCont( )
{ }

void
Kernel::Transform3DCont::takeValue( const RefCountPtr< const Lang::Value > & val, Kernel::EvalState * evalState, bool dummy ) const
{
	try
		{
			typedef const Lang::Geometric3D ArgType;
			RefCountPtr< ArgType > arg = Helpers::try_cast_CoreArgument< ArgType >( val );
			evalState->cont_ = cont_;
			cont_->takeValue( arg->transformed( tf_, arg ),
												evalState );
			return;
		}
	catch( const NonLocalExit::NotThisType & ball )
		{
			/* Wrong type; never mind!.. but see below!
			 */
		}

	try
		{
			typedef const Lang::FloatTriple ArgType;
			RefCountPtr< ArgType > arg = Helpers::try_cast_CoreArgument< ArgType >( val );
			evalState->cont_ = cont_;
			cont_->takeValue( arg->transformed( tf_ ),
												evalState );
			return;
		}
	catch( const NonLocalExit::NotThisType & ball )
		{
			/* Wrong type; never mind!.. but see below!
			 */
		}

	throw Exceptions::ContinuationTypeMismatch( this, val->getTypeName( ), Helpers::typeSetString( Lang::Geometric3D::staticTypeName( ), Lang::FloatTriple::staticTypeName( ) ) );
}

Kernel::ContRef
Kernel::Transform3DCont::up( ) const
{
	return cont_;
}

RefCountPtr< const char >
Kernel::Transform3DCont::description( ) const
{
	return strrefdup( "3D transform application" );
}

void
Kernel::Transform3DCont::gcMark( Kernel::GCMarkedSet & marked )
{
	cont_->gcMark( marked );
}


Kernel::PathApplication2DCont::PathApplication2DCont( RefCountPtr< const Lang::ElementaryPath2D > path, const Kernel::ContRef & cont, const Ast::SourceLocation & traceLoc )
	: Kernel::Continuation( traceLoc ), path_( path ), cont_( cont )
{ }

Kernel::PathApplication2DCont::~PathApplication2DCont( )
{ }

void
Kernel::PathApplication2DCont::takeValue( const RefCountPtr< const Lang::Value > & val, Kernel::EvalState * evalState, bool dummy ) const
{
	Concrete::SplineTime t = Helpers::pathTimeCast( path_.getPtr( ), val.getPtr( ), this );

	evalState->cont_ = cont_;
	cont_->takeValue( Kernel::ValueRef( new Lang::PathSlider2D( path_, t.t( ) ) ),
									 evalState );
	return;
}

Kernel::ContRef
Kernel::PathApplication2DCont::up( ) const
{
	return cont_;
}

RefCountPtr< const char >
Kernel::PathApplication2DCont::description( ) const
{
	return strrefdup( "2D path point selection" );
}

void
Kernel::PathApplication2DCont::gcMark( Kernel::GCMarkedSet & marked )
{
	const_cast< Lang::ElementaryPath2D * >( path_.getPtr( ) )->gcMark( marked );
	cont_->gcMark( marked );
}


Kernel::PathApplication3DCont::PathApplication3DCont( RefCountPtr< const Lang::ElementaryPath3D > path, const Kernel::ContRef & cont, const Ast::SourceLocation & traceLoc )
	: Kernel::Continuation( traceLoc ), path_( path ), cont_( cont )
{ }

Kernel::PathApplication3DCont::~PathApplication3DCont( )
{ }

void
Kernel::PathApplication3DCont::takeValue( const RefCountPtr< const Lang::Value > & val, Kernel::EvalState * evalState, bool dummy ) const
{
	Concrete::SplineTime t = Helpers::pathTimeCast( path_.getPtr( ), val.getPtr( ), this );

	evalState->cont_ = cont_;
	cont_->takeValue( Kernel::ValueRef( new Lang::PathSlider3D( path_, t.t( ) ) ),
										evalState );
	return;
}

Kernel::ContRef
Kernel::PathApplication3DCont::up( ) const
{
	return cont_;
}

RefCountPtr< const char >
Kernel::PathApplication3DCont::description( ) const
{
	return strrefdup( "3D path point selection" );
}

void
Kernel::PathApplication3DCont::gcMark( Kernel::GCMarkedSet & marked )
{
	const_cast< Lang::ElementaryPath3D * >( path_.getPtr( ) )->gcMark( marked );
	cont_->gcMark( marked );
}


Kernel::ComposedFunctionCall_cont::ComposedFunctionCall_cont( const RefCountPtr< const Lang::Function > & second, const Kernel::PassedDyn & dyn, const Kernel::ContRef & cont, const Ast::SourceLocation & callLoc )
	: Kernel::Continuation( callLoc ), second_( second ), dyn_( dyn ), cont_( cont )
{ }

Kernel::ComposedFunctionCall_cont::~ComposedFunctionCall_cont( )
{ }

void
Kernel::ComposedFunctionCall_cont::takeHandle( Kernel::VariableHandle val, Kernel::EvalState * evalState, bool dummy ) const
{
	evalState->dyn_ = dyn_;
	evalState->cont_ = cont_;
	second_->call( second_, evalState, val, traceLoc_ );
}

Kernel::ContRef
Kernel::ComposedFunctionCall_cont::up( ) const
{
	return cont_;
}

RefCountPtr< const char >
Kernel::ComposedFunctionCall_cont::description( ) const
{
	return strrefdup( "composed function's second application" );
}

void
Kernel::ComposedFunctionCall_cont::gcMark( Kernel::GCMarkedSet & marked )
{
	const_cast< Lang::Function * >( second_.getPtr( ) )->gcMark( marked );
	dyn_->gcMark( marked );
	cont_->gcMark( marked );
}


Kernel::ForceFunctionAndCall_2_args_cont::ForceFunctionAndCall_2_args_cont( const Kernel::VariableHandle & arg1, const Kernel::VariableHandle & arg2, const Kernel::PassedDyn & dyn, const Kernel::ContRef & cont, const Ast::SourceLocation & callLoc )
	: Kernel::Continuation( callLoc ), arg1_( arg1 ), arg2_( arg2 ), dyn_( dyn ), cont_( cont )
{ }

Kernel::ForceFunctionAndCall_2_args_cont::~ForceFunctionAndCall_2_args_cont( )
{ }

void
Kernel::ForceFunctionAndCall_2_args_cont::takeValue( const RefCountPtr< const Lang::Value > & val, Kernel::EvalState * evalState, bool dummy ) const
{
	evalState->dyn_ = dyn_;
	evalState->cont_ = cont_;
	typedef const Lang::Function ArgType;
	RefCountPtr< ArgType > fun( Helpers::down_cast_ContinuationArgument< ArgType >( val, this ) );
	fun->call( fun, evalState, arg1_, arg2_, traceLoc_ );
}

Kernel::ContRef
Kernel::ForceFunctionAndCall_2_args_cont::up( ) const
{
	return cont_;
}

RefCountPtr< const char >
Kernel::ForceFunctionAndCall_2_args_cont::description( ) const
{
	return strrefdup( "call function with two values" );
}

void
Kernel::ForceFunctionAndCall_2_args_cont::gcMark( Kernel::GCMarkedSet & marked )
{
	arg1_->gcMark( marked );
	arg2_->gcMark( marked );
	dyn_->gcMark( marked );
	cont_->gcMark( marked );
}


Kernel::RelaxContinuation::RelaxContinuation( Kernel::ContRef & cont )
	: Kernel::Continuation( cont->traceLoc( ) ), cont_( cont )
{ }

Kernel::RelaxContinuation::~RelaxContinuation( )
{ }

void
Kernel::RelaxContinuation::takeValue( const RefCountPtr< const Lang::Value > & val, Kernel::EvalState * evalState, bool dummy ) const
{
	evalState->cont_ = cont_;
	Helpers::relax( val, evalState );
}

Kernel::ContRef
Kernel::RelaxContinuation::up( ) const
{
	return cont_;
}

RefCountPtr< const char >
Kernel::RelaxContinuation::description( ) const
{
	return strrefdup( "relax" );
}

void
Kernel::RelaxContinuation::gcMark( Kernel::GCMarkedSet & marked )
{
	cont_->gcMark( marked );
}
