/* This file is part of Shapes.
 *
 * Shapes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Shapes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2013, 2014 Henrik Tidefelt
 */

#pragma once

#include "ast.h"
#include "graphtypes.h"
#include "globals.h"


namespace Shapes
{
	namespace Kernel
	{

		class Core_graph_cont_partitions : public Kernel::Continuation
		{
			RefCountPtr< const Lang::Value > nodes_;
			const Ast::SourceLocation & nodesLoc_;
			RefCountPtr< const Lang::Value > edges_;
			const Ast::SourceLocation & edgesLoc_;
			RefCountPtr< const Kernel::GraphDomain > domain_;
			Kernel::ContRef cont_;
		public:
			Core_graph_cont_partitions( RefCountPtr< const Lang::Value > nodes, const Ast::SourceLocation & nodesLoc, RefCountPtr< const Lang::Value > edges, const Ast::SourceLocation & edgesLoc, RefCountPtr< const Kernel::GraphDomain > domain, const Kernel::ContRef & cont, const Ast::SourceLocation & partitionsLoc );
			virtual ~Core_graph_cont_partitions( );
			virtual void takeValue( const RefCountPtr< const Lang::Value > & val, Kernel::EvalState * evalState, bool dummy ) const;
			virtual Kernel::ContRef up( ) const;
			virtual RefCountPtr< const char > description( ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
		};

		class Core_graph_cont_nodes : public Kernel::Continuation
		{
			RefCountPtr< const Lang::Value > edges_;
			const Ast::SourceLocation & edgesLoc_;
			RefCountPtr< const Kernel::GraphDomain > domain_;
			RefCountPtr< const Lang::SingleList > partitions_;
			const Ast::SourceLocation & partitionsLoc_;
			Kernel::ContRef cont_;
		public:
			Core_graph_cont_nodes( RefCountPtr< const Lang::Value > edges, const Ast::SourceLocation & edgesLoc, RefCountPtr< const Kernel::GraphDomain > domain, const RefCountPtr< const Lang::SingleList > & partitions, const Kernel::ContRef & cont, const Ast::SourceLocation & partitionsLoc, const Ast::SourceLocation & nodesLoc );
			virtual ~Core_graph_cont_nodes( );
			virtual void takeValue( const RefCountPtr< const Lang::Value > & val, Kernel::EvalState * evalState, bool dummy ) const;
			virtual Kernel::ContRef up( ) const;
			virtual RefCountPtr< const char > description( ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
		};

		class Core_graph_cont_edges : public Kernel::Continuation
		{
			RefCountPtr< const Lang::SingleList > nodes_;
			RefCountPtr< const Kernel::GraphDomain > domain_;
			RefCountPtr< const Lang::SingleList > partitions_;
			const Ast::SourceLocation & nodesLoc_;
			const Ast::SourceLocation & partitionsLoc_;
			/* The edgesLoc passed to the contructor will be stored as traceLoc_. */
			Kernel::ContRef cont_;
		public:
			Core_graph_cont_edges( RefCountPtr< const Lang::SingleList > nodes, RefCountPtr< const Kernel::GraphDomain > domain, const RefCountPtr< const Lang::SingleList > & partitions, const Kernel::ContRef & cont, const Ast::SourceLocation & nodesLoc, const Ast::SourceLocation & partitionsLoc, const Ast::SourceLocation & edgesLoc );
			virtual ~Core_graph_cont_edges( );
			virtual void takeValue( const RefCountPtr< const Lang::Value > & val, Kernel::EvalState * evalState, bool dummy ) const;
			virtual Kernel::ContRef up( ) const;
			virtual RefCountPtr< const char > description( ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
		private:
			RefCountPtr< const Lang::Graph > constructGraph( const RefCountPtr< const Lang::SingleList > & edges, Kernel::PassedEnv env, Kernel::PassedDyn dyn ) const;
		};

		class Core_walk_cont_edges : public Kernel::Continuation
		{
			RefCountPtr< const Lang::Graph > graph_;
			RefCountPtr< const Lang::Node > first_;
			RefCountPtr< const Lang::Node > last_;
			const Ast::SourceLocation & callLoc_;
			/* The edgesLoc passed to the contructor will be stored as traceLoc_. */
			Kernel::ContRef cont_;
		public:
			Core_walk_cont_edges( const RefCountPtr< const Lang::Graph > & graph, const RefCountPtr< const Lang::Node > & first, const RefCountPtr< const Lang::Node > & last, const Ast::SourceLocation & edgesLoc, const Kernel::ContRef & cont, const Ast::SourceLocation & callLoc );
			virtual ~Core_walk_cont_edges( );
			virtual void takeValue( const RefCountPtr< const Lang::Value > & val, Kernel::EvalState * evalState, bool dummy ) const;
			virtual Kernel::ContRef up( ) const;
			virtual RefCountPtr< const char > description( ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
		};

		class NodeDataReceiverFormals : public Kernel::EvaluatedFormals
		{
		public:
			NodeDataReceiverFormals( )
				: Kernel::EvaluatedFormals( Ast::FileID::build_internal( "< NodeData receiver >" ), true )
			{
				appendEvaluatedCoreFormal( "key", Kernel::THE_SLOT_VARIABLE );
				appendEvaluatedCoreFormal( "value", Kernel::THE_VOID_VARIABLE );
				appendEvaluatedCoreFormal( "partition", Kernel::THE_VOID_VARIABLE );
			}
		};

		class EdgeDataReceiverFormals : public Kernel::EvaluatedFormals
		{
		public:
			EdgeDataReceiverFormals( )
				: Kernel::EvaluatedFormals( Ast::FileID::build_internal( "< EdgeData receiver >" ), true )
			{
				appendEvaluatedCoreFormal( "source", Kernel::THE_SLOT_VARIABLE );
				appendEvaluatedCoreFormal( "target", Kernel::THE_SLOT_VARIABLE );
				appendEvaluatedCoreFormal( "value", Kernel::THE_VOID_VARIABLE );
				appendEvaluatedCoreFormal( "label", Kernel::THE_VOID_VARIABLE );
				appendEvaluatedCoreFormal( "directed", Kernel::THE_VOID_VARIABLE );
			}
		};

	}
}
