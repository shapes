/* This file is part of Shapes.
 *
 * Shapes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Shapes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2008, 2009, 2014 Henrik Tidefelt
 */

#pragma once

#include "Shapes_Ast_decls.h"
#include "Shapes_Kernel_decls.h"

#include "refcount.h"
#include "shapestypes.h"
#include "dynamicenvironment.h"
#include "sourcelocation.h"

#include <list>
#include <iostream>
#include <string>

namespace Shapes
{

	namespace Kernel
	{

		class EvalState
		{
		public:
			Ast::Expression * expr_;
			Kernel::PassedEnv env_;
			Kernel::PassedDyn dyn_;
			Kernel::ContRef cont_;

			EvalState( Ast::Expression * expr, const Kernel::PassedEnv & env, const Kernel::PassedDyn & dyn, const Kernel::ContRef & cont )
				: expr_( expr ), env_( env ), dyn_( dyn ), cont_( cont )
			{ }
		};

		class Continuation
		{
		protected:
			const Ast::SourceLocation & traceLoc_;
		public:
			Continuation( const Ast::SourceLocation & traceLoc )
				: traceLoc_( traceLoc )
			{ }
			virtual ~Continuation( )
			{ }
			virtual void takeHandle( Kernel::VariableHandle val, Kernel::EvalState * evalState, bool callingMyself = false ) const;
			virtual void takeValue( const RefCountPtr< const Lang::Value > & val, Kernel::EvalState * evalState, bool callingMyself = false ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked ) = 0;
			virtual Kernel::ContRef up( ) const = 0; // Returns a NullPtr when there is nothing to return.
			virtual RefCountPtr< const char > description( ) const = 0; // outer continuation before ourselves in the list, that is first push_front ourselves, then recurse.
			const Ast::SourceLocation & traceLoc( ) const;
			void backTrace( std::ostream & os ) const;
		};

		class ForcedStructureContinuation : public Continuation
		{
		protected:
			const char * continuationName_;
		public:
			ForcedStructureContinuation( const char * continuationName, const Ast::SourceLocation & traceLoc );
			virtual ~ForcedStructureContinuation( );
			virtual void takeValue( const RefCountPtr< const Lang::Value > & val, Kernel::EvalState * evalState, bool dummy ) const;
			virtual void takeStructure( const RefCountPtr< const Lang::Structure > & structure, Kernel::EvalState * evalState ) const = 0;

			static RefCountPtr< const Lang::SingleList > findUnforced( RefCountPtr< const Lang::SingleList > lst );
		};

	}

	namespace Ast
	{

		class Node
		{
		public:
			typedef std::vector< Ast::Node * > ChildrenType;
		protected:
			Node * parent_;
			Ast::AnalysisEnvironment * analysisEnv_;
			const Ast::SourceLocation loc_; /* Note: Not a const reference. */
			ChildrenType children_;
		public:
			Node( const Ast::SourceLocation & loc );
			Node( const Ast::SourceLocation & firstLoc, const Ast::SourceLocation & lastLoc ); /* Call SourceLocation two argument constructor on the fly. */
			virtual ~Node( );
			void analyze( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst );
			/* The setParent and changeParent methods shall only be used when manipulating already analyzed expression,
			 * for example, when wrapping an already analyzed expression in Ast::Force.
			 */
			void setParent( Ast::Node * parent, Ast::AnalysisEnvironment * env );
			void changeParent( Ast::Node * parent );
			const Ast::SourceLocation & loc( ) const;
			Ast::Node * parent( );
			ChildrenType & children( );
			Ast::AnalysisEnvironment * getEnv( );
			Ast::Expression * findExpressionSameFile( const Ast::SourceLocation & loc );
		protected:
			virtual void analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst ) = 0;
		};

		class Expression : public Node
		{
		public:
		 /* The immediate_ flag means that the expression should not be delayed.  This is not the same as forcing
		  * the expression (compare Ast::Force), which keeps forcing the result of the expression until a value
		  * is obtained.
			 */
			bool immediate_;
			Kernel::BreakpointInfo * breakpoint_; /* Pointer is null if there is no breakpoint. */

			Expression( const Ast::SourceLocation & loc );
			Expression( const Ast::SourceLocation & firstLoc, const Ast::SourceLocation & lastLoc ); /* Call SourceLocation two argument constructor on the fly. */
			virtual ~Expression( );
			virtual void eval( Kernel::EvalState * evalState ) const = 0;
		};

		class BindNode : public Node
		{
		protected:
			const Ast::SourceLocation idLoc_; /* Note: Not a const reference. */
			const Ast::PlacedIdentifier * id_;
		public:
			BindNode( const Ast::SourceLocation & loc, const Ast::SourceLocation & idLoc, const Ast::PlacedIdentifier * id );
			BindNode( const Ast::SourceLocation & firstLoc, const Ast::SourceLocation & lastLoc, const Ast::SourceLocation & idLoc, const Ast::PlacedIdentifier * id ); /* Call SourceLocation two argument constructor on the fly. */
			virtual ~BindNode( );
			const Ast::PlacedIdentifier * id( ) const;
			const Ast::SourceLocation & idLoc( ) const;
			virtual void initializeEnvironment( Kernel::PassedEnv env, Kernel::PassedDyn dyn ) const = 0;
			virtual void evalHelper( Kernel::EvalState * evalState ) const = 0; /* Avoid the name <eval>, to make it easy to keep track of all places in the source where Expression::eval is invoked. */
		};

		class Assertion : public Expression
		{
		public:
			Assertion( const Ast::SourceLocation & loc );
			virtual ~Assertion( );
		};

		class ErrorExpression : public Expression
		{
		public:
			ErrorExpression( const Ast::SourceLocation & loc );
			virtual ~ErrorExpression( );
			virtual void analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst );
			virtual void eval( Kernel::EvalState * evalState ) const;
		};

	}

}
