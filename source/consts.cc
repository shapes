/* This file is part of Shapes.
 *
 * Shapes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Shapes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2008, 2014, 2015 Henrik Tidefelt
 */

#include "Shapes_Kernel_decls.h"

#include "consts.h"
#include "shapesexceptions.h"
#include "classtypes.h"
#include "statetypes.h"
#include "fonttypes.h"

using namespace Shapes;

/* Helper function just for initializing namespace constants.
 */
static
RefCountPtr< Ast::NamespacePath >
makeCoreNamespacePath( const Ast::NamespacePath & prefix, const char * name )
{
  RefCountPtr< Ast::NamespacePath > path( new Ast::NamespacePath( prefix ) );
  path->push_back( name );
  return path;
}
static Ast::NamespacePath globalNamespace; /* Just like Ast::THE_EMPTY_NAMESPACE_PATH, but not introducing an unnecessary global initialization order constraint. */

/* Make sure these are initialized before used to initialize other constants!
 */
const RefCountPtr< const Ast::NamespacePath > Lang::THE_NAMESPACE_Shapes = makeCoreNamespacePath( globalNamespace, "Shapes" );
const RefCountPtr< const Ast::NamespacePath > Lang::THE_NAMESPACE_Shapes_Data = makeCoreNamespacePath( *Lang::THE_NAMESPACE_Shapes, "Data" );
const RefCountPtr< const Ast::NamespacePath > Lang::THE_NAMESPACE_Shapes_Data_Type = makeCoreNamespacePath( *THE_NAMESPACE_Shapes_Data, "Type" );
const RefCountPtr< const Ast::NamespacePath > Lang::THE_NAMESPACE_Shapes_Debug = makeCoreNamespacePath( *Lang::THE_NAMESPACE_Shapes, "Debug" );
const RefCountPtr< const Ast::NamespacePath > Lang::THE_NAMESPACE_Shapes_Geometry = makeCoreNamespacePath( *Lang::THE_NAMESPACE_Shapes, "Geometry" );
const RefCountPtr< const Ast::NamespacePath > Lang::THE_NAMESPACE_Shapes_Geometry3D = makeCoreNamespacePath( *Lang::THE_NAMESPACE_Shapes, "Geometry3D" );
const RefCountPtr< const Ast::NamespacePath > Lang::THE_NAMESPACE_Shapes_Graphics = makeCoreNamespacePath( *Lang::THE_NAMESPACE_Shapes, "Graphics" );
const RefCountPtr< const Ast::NamespacePath > Lang::THE_NAMESPACE_Shapes_Graphics_PDF = makeCoreNamespacePath( *THE_NAMESPACE_Shapes_Graphics, "PDF" );
const RefCountPtr< const Ast::NamespacePath > Lang::THE_NAMESPACE_Shapes_Graphics_Tag = makeCoreNamespacePath( *THE_NAMESPACE_Shapes_Graphics, "Tag" );
const RefCountPtr< const Ast::NamespacePath > Lang::THE_NAMESPACE_Shapes_Graphics3D = makeCoreNamespacePath( *Lang::THE_NAMESPACE_Shapes, "Graphics3D" );
const RefCountPtr< const Ast::NamespacePath > Lang::THE_NAMESPACE_Shapes_Graphics3D_Tag = makeCoreNamespacePath( *THE_NAMESPACE_Shapes_Graphics3D, "Tag" );
const RefCountPtr< const Ast::NamespacePath > Lang::THE_NAMESPACE_Shapes_IO = makeCoreNamespacePath( *Lang::THE_NAMESPACE_Shapes, "IO" );
const RefCountPtr< const Ast::NamespacePath > Lang::THE_NAMESPACE_Shapes_Layout = makeCoreNamespacePath( *Lang::THE_NAMESPACE_Shapes, "Layout" );
const RefCountPtr< const Ast::NamespacePath > Lang::THE_NAMESPACE_Shapes_Numeric = makeCoreNamespacePath( *Lang::THE_NAMESPACE_Shapes, "Numeric" );
const RefCountPtr< const Ast::NamespacePath > Lang::THE_NAMESPACE_Shapes_Numeric_Constant = makeCoreNamespacePath( *THE_NAMESPACE_Shapes_Numeric, "Constant" );
const RefCountPtr< const Ast::NamespacePath > Lang::THE_NAMESPACE_Shapes_Numeric_Math = makeCoreNamespacePath( *THE_NAMESPACE_Shapes_Numeric, "Math" );
const RefCountPtr< const Ast::NamespacePath > Lang::THE_NAMESPACE_Shapes_Numeric_Random = makeCoreNamespacePath( *THE_NAMESPACE_Shapes_Numeric, "Random" );
const RefCountPtr< const Ast::NamespacePath > Lang::THE_NAMESPACE_Shapes_String = makeCoreNamespacePath( *Lang::THE_NAMESPACE_Shapes, "String" );
const RefCountPtr< const Ast::NamespacePath > Lang::THE_NAMESPACE_Shapes_Text = makeCoreNamespacePath( *Lang::THE_NAMESPACE_Shapes, "Text" );
const RefCountPtr< const Ast::NamespacePath > Lang::THE_NAMESPACE_Shapes_Text_Font = makeCoreNamespacePath( *THE_NAMESPACE_Shapes_Text, "Font" );
const RefCountPtr< const Ast::NamespacePath > Lang::THE_NAMESPACE_Shapes_Traits = makeCoreNamespacePath( *Lang::THE_NAMESPACE_Shapes, "Traits" );
const RefCountPtr< const Ast::NamespacePath > Lang::THE_NAMESPACE_Shapes_Traits_BW = makeCoreNamespacePath( *THE_NAMESPACE_Shapes_Traits, "BW" );
const RefCountPtr< const Ast::NamespacePath > Lang::THE_NAMESPACE_Shapes_Traits_Blend = makeCoreNamespacePath( *THE_NAMESPACE_Shapes_Traits, "Blend" );
const RefCountPtr< const Ast::NamespacePath > Lang::THE_NAMESPACE_Shapes_Traits_Cap = makeCoreNamespacePath( *THE_NAMESPACE_Shapes_Traits, "Cap" );
const RefCountPtr< const Ast::NamespacePath > Lang::THE_NAMESPACE_Shapes_Traits_Device = makeCoreNamespacePath( *THE_NAMESPACE_Shapes_Traits, "Device" );
const RefCountPtr< const Ast::NamespacePath > Lang::THE_NAMESPACE_Shapes_Traits_Join = makeCoreNamespacePath( *THE_NAMESPACE_Shapes_Traits, "Join" );
const RefCountPtr< const Ast::NamespacePath > Lang::THE_NAMESPACE_Shapes_Traits_Light = makeCoreNamespacePath( *THE_NAMESPACE_Shapes_Traits, "Light" );
const RefCountPtr< const Ast::NamespacePath > Lang::THE_NAMESPACE_Shapes_Traits_RGB = makeCoreNamespacePath( *THE_NAMESPACE_Shapes_Traits, "RGB" );


RefCountPtr< const char > Interaction::SEVERAL_TYPES = strrefdup( "(several types)" );
RefCountPtr< const char > Interaction::PUBLIC_SCOPE_NAME = strrefdup( "public" );
RefCountPtr< const char > Interaction::PROTECTED_SCOPE_NAME = strrefdup( "protected" );

const char Interaction::NAMESPACE_SEPARATOR[] = "..";
const char Interaction::ENCAPSULATION_MARK[] = "^^";
const char Interaction::DYNAMIC_VARIABLE_PREFIX[] = "@";
const char Interaction::STATE_PREFIX[] = "•";
const char Interaction::DYNAMIC_STATE_PREFIX[] = "@•";

const char Interaction::BUILD_REQ_LIBJPEG[] = "JPEG";
const char Interaction::BUILD_REQ_LIBPNG[] = "PNG";
const char Interaction::BUILD_REQ_FREETYPE[] = "FreeType";
const char Interaction::BUILD_REQ_FONTCONFIG[] = "FontConfig";
const char Interaction::BUILD_REQ_OPENSSL[] = "OpenSSL";

/* The initialization of Lang::TEX_SYNTAX_ID is placed in coreast.cc to ensure it gets initialized before we need it there.
 */

const Ast::Identifier Lang::SELF_ID( Ast::THE_EMPTY_SEARCH_CONTEXT, Ast::NamespaceReference::ABSOLUTE, Lang::THE_NAMESPACE_Shapes, "self" ); /* Warning! Has Ast::THE_EMPTY_SEARCH_CONTEXT been initialized before being used here? */
const char Kernel::SPLIT_VAR_PREFIX[] = ".splvar"; /* Note that the leading dot puts this variable aside all user-variables. */
const char Kernel::HTML_DIR[] = HTML_DIRDEF;
const char Kernel::RESOURCES_DIR[] = RESOURCES_DIRDEF;
const Ast::PlacedIdentifier Kernel::SEQUENTIAL_EXPR_VAR_ID( Lang::THE_NAMESPACE_Shapes, ".seqvar" ); /* Note that the leading dot puts this variable aside all user-variables. */
const Ast::PlacedIdentifier Kernel::MUTATOR_CURRY_VAR_ID( Lang::THE_NAMESPACE_Shapes, ".callvar" ); /* Note that the leading dot puts this variable aside all user-variables. */

const char Lang::CONTINUATION_ID_ERROR[] = "error";

const Ast::PlacedIdentifier Lang::HANDLER_NO_INTERSECTION( Lang::THE_NAMESPACE_Shapes_Geometry, "handler_NoIntersection" );
const Ast::PlacedIdentifier Lang::CANVAS_ID( Lang::THE_NAMESPACE_Shapes_IO, "page" );
const Ast::PlacedIdentifier Lang::CATALOG_ID( Lang::THE_NAMESPACE_Shapes_IO, "catalog" );

const Ast::PlacedIdentifier Lang::DYNAMIC_VARIABLE_ID_EYEZ( Lang::THE_NAMESPACE_Shapes_Geometry3D, "eyez" );
const Ast::PlacedIdentifier Lang::DYNAMIC_VARIABLE_ID_TEX_BLEED( Lang::THE_NAMESPACE_Shapes_Layout, "TeX_bleed" );
const Ast::PlacedIdentifier Lang::DYNAMIC_VARIABLE_ID_DEFAULT_UNIT( Lang::THE_NAMESPACE_Shapes_Geometry, "defaultunit" );
const Ast::PlacedIdentifier Lang::DYNAMIC_VARIABLE_ID_BLEND_SPACE( Lang::THE_NAMESPACE_Shapes_Traits, "blendspace" );
const Ast::PlacedIdentifier Lang::DYNAMIC_VARIABLE_ID_STROKING( Lang::THE_NAMESPACE_Shapes_Traits, "stroking" );
const Ast::PlacedIdentifier Lang::DYNAMIC_VARIABLE_ID_WIDTH( Lang::THE_NAMESPACE_Shapes_Traits, "width" );
const Ast::PlacedIdentifier Lang::DYNAMIC_VARIABLE_ID_NONSTROKING( Lang::THE_NAMESPACE_Shapes_Traits, "nonstroking" );
const Ast::PlacedIdentifier Lang::DYNAMIC_VARIABLE_ID_AUTOINTENSITY( Lang::THE_NAMESPACE_Shapes_Traits_Light, "autointensity" );

const Ast::PlacedIdentifier Lang::DYNAMIC_VARIABLE_ID_TEXT_SIZE( Lang::THE_NAMESPACE_Shapes_Text, "size" );
const Ast::PlacedIdentifier Lang::DYNAMIC_VARIABLE_ID_TEXT_FONT( Lang::THE_NAMESPACE_Shapes_Text, "font" );

const Ast::MemberMode Ast::MEMBER_ACCESS_BITS = 0x000F;
const Ast::MemberMode Ast::MEMBER_ACCESS_PRIVATE = 0x0000;
const Ast::MemberMode Ast::MEMBER_ACCESS_PUBLIC_GET = 0x0001;
const Ast::MemberMode Ast::MEMBER_ACCESS_PUBLIC_INSERT = 0x0002;
const Ast::MemberMode Ast::MEMBER_ACCESS_PROTECTED_GET = 0x0004;
const Ast::MemberMode Ast::MEMBER_ACCESS_PROTECTED_INSERT = 0x0008;
const Ast::MemberMode Ast::MEMBER_CONST = 0x0010;
const Ast::MemberMode Ast::MEMBER_METHOD = 0x0020;
const Ast::MemberMode Ast::MEMBER_ABSTRACT = 0x0040;
const Ast::MemberMode Ast::MEMBER_FINAL = 0x0080;
const Ast::MemberMode Ast::MEMBER_TRANSFORMING = 0x0100;

const Ast::ClassMode Ast::CLASS_MODE_ABSTRACT = 0x0001;
const Ast::ClassMode Ast::CLASS_MODE_FINAL = 0x0002;

const Ast::FunctionMode Ast::FUNCTION_TRANSFORMING = 0x0001;

const char Lang::MESSAGE_DRAWABLE_DRAW_ID[] = "draw";

const double Computation::SINGULAR_TRANSFORM_LIMIT = 1e-8;


RefCountPtr< const char > BuiltInFonts::TIMES_ROMAN = strrefdup( "Times-Roman" );
RefCountPtr< const char > BuiltInFonts::TIMES_BOLD = strrefdup( "Times-Bold" );
RefCountPtr< const char > BuiltInFonts::TIMES_ITALIC = strrefdup( "Times-Italic" );
RefCountPtr< const char > BuiltInFonts::TIMES_BOLDITALIC = strrefdup( "Times-BoldItalic" );
// HELVETICA is initialized in globals.cc to ensure correct order of initialization.
RefCountPtr< const char > BuiltInFonts::HELVETICA_BOLD = strrefdup( "Helvetica-Bold" );
RefCountPtr< const char > BuiltInFonts::HELVETICA_OBLIQUE = strrefdup( "Helvetica-Oblique" );
RefCountPtr< const char > BuiltInFonts::HELVETICA_BOLDOBLIQUE = strrefdup( "Helvetica-BoldOblique" );
RefCountPtr< const char > BuiltInFonts::COURIER = strrefdup( "Courier" );
RefCountPtr< const char > BuiltInFonts::COURIER_BOLD = strrefdup( "Courier-Bold" );
RefCountPtr< const char > BuiltInFonts::COURIER_OBLIQUE = strrefdup( "Courier-Oblique" );
RefCountPtr< const char > BuiltInFonts::COURIER_BOLDOBLIQUE = strrefdup( "Courier-BoldOblique" );
RefCountPtr< const char > BuiltInFonts::SYMBOL = strrefdup( "Symbol" );
RefCountPtr< const char > BuiltInFonts::ZAPFDINGBATS = strrefdup( "ZapfDingbats" );


const size_t Computation::RREL_SIZE = 33;
const double Computation::RREL_TH_STEP = 0.1;
const double Computation::RREL_TABLE[RREL_SIZE] =
	{ 1/3,0.3341686344960354,0.33668841107954134,
	 0.3409422096583605,0.34701519474761894,
	 0.35502910405887494,0.36514854168289823,
	 0.3775911942845651,0.3926341399960872,
	 0.41062983298282885,0.43202211298939736,
	 0.45737337400783484,0.4873946524706331,
	 0.522992662410542,0.5653317708668038,
	 0.6159221561762755,0.6767424374123584,
	 0.7504184408114359,0.8404852622717847,
	 0.9517742868557572,1.0910034113732654,
	 1.267683405767762,1.4955440295942208,
	 1.7947857010553352,2.1956618428196286,
	 2.7440092614738933,3.5090279091817442,
	 4.590771723726686,6.112526284299868,8.144027798714662,
	 10.441038625842998,12.076454051457787,13};
