/* This file is part of Shapes.
 *
 * Shapes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Shapes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2009 Henrik Tidefelt
 */

#include "drawabletypes.h"
#include "rasterloaders.h"
#include "statetypes.h"
#include "globals.h"
#include "consts.h"
#include "config.h"

#include <fstream>
#include <cstdio>

using namespace Shapes;


#ifdef HAVE_LIBJPEG

/* Since jpeglib.h includes jconfig.h, which contains #defines that are in conflict with our own #defines.
 * Note that it is necessary to include config.h before jpeglib.h, becase it is only after config.h has been
 * included that we can tell whether jpeglib.h should be included at all.
 */
#undef HAVE_STDDEF_H
#undef HAVE_STDLIB_H
#include <jpeglib.h>

namespace Shapes
{
	namespace Kernel
	{

		struct Shapes_jpeglib_error_mgr : public jpeg_error_mgr
		{
			const char * filename_;
		};

		void
		Shapes_jpeglib_exit( j_common_ptr cinfo )
		{
			char buf[ JMSG_LENGTH_MAX ];
			(*cinfo->err->format_message)( cinfo, buf );
			std::ostringstream msg;
			msg << "There was a problem importing the JPEG file " << reinterpret_cast< Shapes_jpeglib_error_mgr * >( cinfo->err )->filename_ << ": " << buf ;
			throw Exceptions::ExternalError( strrefdup( msg ) );
		}

	}
}
#endif


RefCountPtr< const Lang::RasterImage >
Shapes::Helpers::RasterLoader_JPEG::load( RefCountPtr< std::ifstream > & filePtr, const std::string & full_filename, Concrete::Length resolution, bool override, const Ast::PlacedIdentifier & coreId, const Ast::SourceLocation & callLoc ) const
{
#ifndef HAVE_LIBJPEG
	throw Exceptions::BuildRequirement( Interaction::BUILD_REQ_LIBJPEG, coreId, callLoc );
#else

	/* The library seems incompatible with c++ istream sources, so we close the stream and do it the old way instead.
	 */
	filePtr->close( );
	FILE * iFile = fopen( full_filename.c_str( ), "rb" );
	if( iFile == 0 )
		{
			std::ostringstream msg;
			msg << "The file " << full_filename << " was found, but could not be opened for read." ;
			throw Exceptions::ExternalError( strrefdup( msg ) );
		}

  struct jpeg_decompress_struct cinfo;
	Kernel::Shapes_jpeglib_error_mgr jerr;

  /* Step 1: allocate and initialize JPEG decompression object */

  /* We set up the normal JPEG error routines, then override error_exit. */
  cinfo.err = jpeg_std_error( & jerr );
  jerr.error_exit = Kernel::Shapes_jpeglib_exit;
	jerr.filename_ = full_filename.c_str( );
  /* Establish the setjmp return context for my_error_exit to use. */
	try
		{
			/* Now we can initialize the JPEG decompression object. */
			jpeg_create_decompress( & cinfo );
			jpeg_stdio_src( & cinfo, iFile );
			(void) jpeg_read_header( & cinfo, TRUE );
			(void) jpeg_start_decompress( & cinfo );

			Concrete::Length resolution_x = resolution;
			Concrete::Length resolution_y = resolution;
			if( ! override )
				{
					switch( cinfo.density_unit )
						{
						case 0:/* Only aspect ratio. */
							{
								Concrete::Length tmpRes = sqrt( static_cast< double >( cinfo.X_density ) * static_cast< double >( cinfo.Y_density ) ) * resolution;
								resolution_x = ( tmpRes / cinfo.X_density );
								resolution_y = ( tmpRes / cinfo.Y_density );
							}
							break;
						case 1:/* Dots per inch. */
							{
								resolution_x = ( Concrete::Length( 72. ) / cinfo.X_density );
								resolution_y = ( Concrete::Length( 72. ) / cinfo.Y_density );
							}
							break;
						case 2:/* Dots per centimeter. */
							{
								resolution_x = ( Concrete::Length( 72. / 2.54 ) / cinfo.X_density );
								resolution_y = ( Concrete::Length( 72. / 2.54 ) / cinfo.Y_density );
							}
							break;
						default:
							throw Exceptions::CoreRequirement( "The JPEG file was using an unknown density unit value.", coreId, callLoc );
						}
				}
			size_t size_x = cinfo.image_width;
			size_t size_y = cinfo.image_height;
			size_t size_depth = 8; /* Always 8 for JPEG, according to the "PDF Reference, fifth edition", section 3.3.7 ("DCTDeccode Filter"). */

			RefCountPtr< const Lang::ColorSpace > colorSpace = Lang::THE_COLOR_SPACE_DEVICE_RGB;
			int colorConversion = 0; /* This flag allows some unusual colorspaces of JPEG to be treated as ordinary RGB or CMYK. */
			switch( cinfo.jpeg_color_space )
				{
				case JCS_UNKNOWN:		/* error/unspecified */
					throw Exceptions::CoreRequirement( "The color space was unspecified in the JPEG file.", coreId, callLoc );
				case JCS_GRAYSCALE:		/* monochrome */
					colorSpace = Lang::THE_COLOR_SPACE_DEVICE_GRAY;
					colorConversion = 0;
					break;
				case JCS_RGB:		/* red/green/blue */
					/* Use the default value set above! */
					break;
				case JCS_YCbCr:		/* Y/Cb/Cr (also known as YUV) */
					colorSpace = Lang::THE_COLOR_SPACE_DEVICE_RGB;
					colorConversion = 1;
					break;
				case JCS_CMYK:		/* C/M/Y/K */
					colorSpace = Lang::THE_COLOR_SPACE_DEVICE_CMYK;
					colorConversion = 0;
					break;
				case JCS_YCCK:		/* Y/Cb/Cr/K */
					colorSpace = Lang::THE_COLOR_SPACE_DEVICE_CMYK;
					colorConversion = 1;
					break;
				default:
					throw Exceptions::CoreRequirement( "The JPEG file was using an unknown color type value.", coreId, callLoc );
				}

			Lang::RasterImage * imagePtr = Lang::RasterImage::newInstance( size_x, size_y, size_depth, resolution_x, resolution_y, colorSpace );
			RefCountPtr< const Lang::RasterImage > image = RefCountPtr< const Lang::RasterImage >( RefCountPtr< const Lang::RasterImage >( imagePtr ) );

			SimplePDF::PDF_Stream_out & stream = *(imagePtr->stream( ));
			stream[ "Filter" ] = SimplePDF::newName( "DCTDecode" );
			/* The default value for the ColorTransform depends on the number of components in the color space. */
			if( ( colorConversion != 1 && colorSpace->numberOfComponents( ) == 3 ) ||
					( colorConversion != 0 && colorSpace->numberOfComponents( ) != 3 ) )
				{
					stream[ "ColorTransform" ] = SimplePDF::newInt( colorConversion );
				}
			stream.invertFilterOnWrite = false;

			std::fseek( iFile, 0, SEEK_END );
			const size_t filesize = std::ftell( iFile );
			std::rewind( iFile );
			RefCountPtr< char > buf = RefCountPtr< char >( new char[ filesize ] );
			if( fread( buf.getPtr( ), 1, filesize, iFile ) != filesize )
				{
					throw Exceptions::ExternalError( "Failed to read JPEG file content into memory." );
				}
			stream.data.write( buf.getPtr( ), filesize );

			/* If everything was OK, we just free allocated resources and return.
			 */
			jpeg_destroy_decompress( & cinfo );
			fclose( iFile );

			return image;

		}
	catch( const Exceptions::Exception & ball )
		{
			jpeg_destroy_decompress( & cinfo );
			fclose( iFile );
			throw;
		}
#endif
}
