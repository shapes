/* This file is part of Shapes.
 *
 * Shapes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Shapes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2008, 2014 Henrik Tidefelt
 */

#pragma once

#include <cstring>
//#include <climits>
#include <limits>
#include <string>

#include "Shapes_Kernel_decls.h"
#include "Shapes_Lang_decls.h"
#include "Shapes_Ast_decls.h"

#include "refcount.h"
#include "charptrless.h"
#include "ptrowner.h"
#include "sourcelocation.h"
#include "shapesexceptions.h"
#include "shapesvalue.h"
//#include "dynamicenvironment.h"

#include <map>
#include <set>
#include <vector>


namespace Shapes
{

  namespace Computation
  {

    enum SpecialUnit { SPECIALU_DIST = 0x0001, SPECIALU_CIRC = 0x0002, SPECIALU_CORR = 0x0004, SPECIALU_NOINFLEX = 0x0008 };

  }

  namespace Ast
  {

    class IdentifierTree
    {
    public:
      static const size_t NOT_FOUND;

    private:
      size_t size_;
      IdentifierTree * root_;
      IdentifierTree * encap_;
      Ast::IdentifierTree * encapParent_; /* If non-null, this is an encapsulation namespace, and encapParent_ points to the parent. */
      std::map< const char *, size_t, charPtrLess > simpleIdentifiers_;
      /* For the namespaces, we need to know which names are aliases and which are the real namespace names.
       * It's a pity we need it, because it is only used for debugging purposes when we need to map a variable entry
       * to its corresponding global identifier.
       */
      class NamespaceLink
      {
      public:
        /* Kinds of namespace links:
         *   PRIMARY - Main entry, used in all user communication etc.
         *   SECONDARY - Alternative to PRIMARY, but not used for user communication etc.
         *   RESTRICTED - Alias that should only be matched against the first namespace identifier of a relative reference.
         */
        enum Kind { PRIMARY, SECONDARY, RESTRICTED };
      private:
        Kind kind_;
        IdentifierTree * node_;
      public:
        NamespaceLink( Kind kind, IdentifierTree * node )
        : kind_( kind ), node_( node )
        { }
        Kind kind( ) const { return kind_; }
        IdentifierTree * node( ) const { return node_; }
      };
      std::map< const char *, NamespaceLink, charPtrLess > namespaces_;

    public:
      IdentifierTree( IdentifierTree * parent, bool encapsulation );
      ~IdentifierTree( );

      size_t size( ) const { return size_; }
      IdentifierTree * root( ) { return root_; }
      const IdentifierTree * root( ) const { return root_; }
      IdentifierTree * encap( ) { return encap_; }
      const IdentifierTree * encap( ) const { return encap_; }
      IdentifierTree * base( Ast::NamespaceReference::Base b );
      const IdentifierTree * base( Ast::NamespaceReference::Base b ) const;

      /* Returns true if the identifier could be inserted.  The source location is only used for reporting namespace collisions. */
      bool insert( const Ast::PlacedIdentifier & id, const Ast::SourceLocation & loc );

      /* Returns true if the expansion could be resolved.
       */
      bool insertAlias( const Ast::PlacedIdentifier & id, const Ast::NamespaceReference & expansion, const Ast::SourceLocation & idLoc );

      /* Returns NOT_FOUND in case of miss.
       */
      size_t lookup( const Ast::Identifier & id ) const;
      size_t lookup( const Ast::PlacedIdentifier & id ) const;
      size_t lookupPrivateAlias( const Ast::PlacedIdentifier & id, const char * privateName ) const;

      Ast::PlacedIdentifier reverseMap( size_t pos ) const;
      void dumpMap( std::map< PlacedIdentifier, size_t > * dst ) const;
      void showPath( std::ostream & os ) const;

    private:
      Ast::IdentifierTree * newChild( const char * key, const Ast::SourceLocation & loc );
      bool insert( Ast::NamespacePath::const_iterator begin, const Ast::NamespacePath::const_iterator & end, const char * simpleId, size_t pos, const Ast::SourceLocation & loc );
      bool insertAlias( Ast::NamespacePath::const_iterator begin, const Ast::NamespacePath::const_iterator & end, const char * simpleId, const Ast::NamespaceReference & expansion, const Ast::SourceLocation & idLoc );
      void insertEncapsulationLink( const char * key, size_t pos, const Ast::SourceLocation & loc );
      void insertEncapsulationLink( const char * key, Ast::IdentifierTree * tree, const Ast::SourceLocation & loc );
      size_t lookupPrefix( const Ast::NamespacePath::const_iterator & begin, const Ast::NamespacePath::const_iterator & end, const char * privateName, const Ast::Identifier & id ) const;
      size_t lookupInside( const Ast::NamespacePath::const_iterator & begin, const Ast::NamespacePath::const_iterator & end, const Ast::Identifier & id ) const;
      size_t lookupSuffix( bool followRestricted, const Ast::NamespacePath::const_iterator & begin, const Ast::NamespacePath::const_iterator & end, const char * simpleId ) const;
      Ast::IdentifierTree * resolveAlias( const Ast::NamespaceReference & expansion, bool * found, const Ast::SourceLocation & loc );
      Ast::IdentifierTree * resolveAlias( const Ast::NamespacePath::const_iterator & begin, const Ast::NamespacePath::const_iterator & end, bool * found, const Ast::SourceLocation & loc );
      Ast::NamespacePath * reverseMapHelper( size_t pos, const char ** simpleIdDst ) const;
      bool pushPath( const Ast::IdentifierTree * tree, std::list< const char * > * dst ) const;
      void dumpMap( std::map< PlacedIdentifier, size_t > * dst, Ast::NamespacePath * prefix ) const;
    };

  }

  namespace Kernel
  {

    class Thunk
    {
      Kernel::PassedEnv env_;
      Kernel::PassedDyn dyn_;
      Ast::Expression * expr_;
      mutable bool forced_;
    public:
      Thunk( Kernel::PassedEnv env, Kernel::PassedDyn dyn, Ast::Expression * expr );
      void force( Kernel::EvalState * evalState, bool onlyOnce = true ) const;
      Thunk * deepCopy( );
      void gcMark( Kernel::GCMarkedSet & marked );
      Ast::Expression * getExpr( );
      void printEnv( std::ostream & os ) const;
    };

    /* Although there are several kinds of variables, I don't use c++ polymorphism for efficiency reasons.
     */
    class Variable
    {
      static const unsigned char THUNK =    0x03;
      static const unsigned char FORCING =  0x02;
      static const unsigned char COLD =     0x00;
      mutable Kernel::Thunk * thunk_;                     /* thunk != 0 means that the thunk waits to be forced */
      mutable RefCountPtr< const Lang::Value > val_;
      mutable unsigned char state_;
    private:
      Variable( const Kernel::Variable & orig );
    public:
      Variable( const RefCountPtr< const Lang::Value > & val );
      Variable( Kernel::Thunk * thunk );
      ~Variable( );
      void force( const Kernel::VariableHandle & selfRef, Kernel::EvalState * evalState ) const;
      RefCountPtr< const Lang::Value > & getUntyped( ) const;
      template< class T >
        RefCountPtr< T > getVal( ) const
        {
          if( val_ == NullPtr< const Lang::Value >( ) )
            {
              throw Exceptions::InternalError( strrefdup( "The value is not ready to be get without continuation." ) );
            }
          RefCountPtr< T > res = val_.down_cast< T >( );
          if( res == NullPtr< T >( ) )
            {
              throw Exceptions::TypeMismatch( val_->getTypeName( ), T::staticTypeName( ) );
            }
          return res;
        }
      template< class T >
        RefCountPtr< T > getVal( const char * hint ) const
        {
          if( val_ == NullPtr< const Lang::Value >( ) )
            {
              throw Exceptions::InternalError( strrefdup( "The value is not ready to be get without continuation." ) );
            }
          RefCountPtr< T > res = val_.down_cast< T >( );
          if( res == NullPtr< T >( ) )
            {
              throw Exceptions::TypeMismatch( Ast::THE_UNKNOWN_LOCATION, hint, val_->getTypeName( ), T::staticTypeName( ) );
            }
          return res;
        }
      template< class T >
        RefCountPtr< T > getVal( const Ast::SourceLocation & loc ) const
        {
          if( val_ == NullPtr< const Lang::Value >( ) )
            {
              throw Exceptions::InternalError( strrefdup( "The value is not ready to be get without continuation." ) );
            }
          RefCountPtr< T > res = val_.down_cast< T >( );
          if( res == NullPtr< T >( ) )
            {
              throw Exceptions::TypeMismatch( loc, val_->getTypeName( ), T::staticTypeName( ) );
            }
          return res;
        }
      template< class T >
        RefCountPtr< T > tryVal( ) const
        {
          if( val_ == NullPtr< const Lang::Value >( ) )
            {
              throw Exceptions::InternalError( strrefdup( "The value is not ready to be get without continuation." ) );
            }
          RefCountPtr< T > res = val_.down_cast< T >( );
          if( res == NullPtr< T >( ) )
            {
              throw NonLocalExit::NotThisType( );
            }
          return res;
        }
      bool isThunk( ) const;
      bool isThunk( Ast::Expression * expr ) const; /* Test for thunk with given expression. */
      Kernel::Thunk * copyThunk( ) const;
      void gcMark( Kernel::GCMarkedSet & marked );
    private:
      void setValue( const RefCountPtr< const Lang::Value > & val ) const;
      friend class Kernel::StoreVariableContinuation;
      friend class Kernel::StmtStoreVariableContinuation;
    };

    class State
    {
      bool alive_;

    private:
      State( const Kernel::State & orig );
    public:
      State( );
      virtual ~State( );
      void tackOn( Kernel::EvalState * evalState, const RefCountPtr< const Lang::Value > & piece, const Ast::SourceLocation & callLoc );
      void peek( Kernel::EvalState * evalState, const Ast::SourceLocation & callLoc );
      void freeze( Kernel::EvalState * evalState, const Ast::SourceLocation & callLoc );

      RefCountPtr< const Lang::Function > getMutator( const char * mutatorID );

      virtual const RefCountPtr< const ::Shapes::Lang::Class > & getClass( ) const = 0;
      RefCountPtr< const char > getTypeName( ) const;
      bool isAlive( ) const;
      virtual void gcMark( Kernel::GCMarkedSet & marked ) = 0;

    protected:
      virtual void tackOnImpl( Kernel::EvalState * evalState, const RefCountPtr< const Lang::Value > & piece, const Ast::SourceLocation & callLoc ) = 0;
      virtual void peekImpl( Kernel::EvalState * evalState, const Ast::SourceLocation & callLoc ) = 0;
      virtual void freezeImpl( Kernel::EvalState * evalState, const Ast::SourceLocation & callLoc ) = 0;
    };

    class DynamicVariableProperties
    {
    protected:
      const Ast::PlacedIdentifier * id_;
    public:
    DynamicVariableProperties( const Ast::PlacedIdentifier * id )
      : id_( id )
      { }
      virtual ~DynamicVariableProperties( );
      const Ast::PlacedIdentifier & id( ) const { return *id_; }
      virtual Kernel::VariableHandle fetch( const Kernel::PassedDyn & dyn ) const = 0;
      virtual bool forceValue( ) const { return true; };
      virtual void makeBinding( Kernel::VariableHandle val, const Ast::DynamicBindingExpression * bindingExpr, Kernel::EvalState * evalState ) const = 0;
    };

    class DynamicStateProperties
    {
    protected:
      const Ast::PlacedIdentifier * id_;
    public:
    DynamicStateProperties( const Ast::PlacedIdentifier * id )
      : id_( id )
      { }
      virtual ~DynamicStateProperties( );
      const Ast::PlacedIdentifier & id( ) const { return *id_; }
      virtual Kernel::StateHandle fetch( const Kernel::PassedDyn & dyn ) const = 0;
      virtual void makeBinding( Kernel::StateHandle val, const Ast::DynamicStateBindingExpression * bindingExpr, Kernel::EvalState * evalState ) const = 0;
    };

    class Environment
    {
    public:
      class LexicalKey
      {
      public:
        size_t up_;
        size_t pos_;
        LexicalKey( size_t up, size_t pos )
          : up_( up ), pos_( pos )
        { };
        LexicalKey( const LexicalKey & orig )
          : up_( orig.up_ ), pos_( orig.pos_ )
        { };
        LexicalKey & oneAbove( )
          {
            ++up_;
            return *this;
          }
        bool isMissing( ) const { return pos_ == std::numeric_limits< size_t >::max( ); }
      };
      static LexicalKey theMissingKey;
      typedef RefCountPtr< std::vector< VariableHandle > > ValueVector;
      typedef RefCountPtr< std::vector< StateHandle > > StateVector;
      static const Ast::SourceLocation THE_INITIALIZATION_LOCATION;
    private:
      Environment * parent_;

      Ast::IdentifierTree * bindings_;
      ValueVector values_;
      Ast::IdentifierTree * dynamicKeyBindings_;
      std::vector< DynamicVariableProperties * > * dynamicKeyValues_;

      Ast::IdentifierTree * stateBindings_;
      StateVector states_;
      Ast::IdentifierTree * dynamicStateKeyBindings_;
      std::vector< DynamicStateProperties * > * dynamicStateKeyValues_;

      PtrOwner_back_Access< std::list< const char * > > charPtrDeletionList_;

      bool gcMarked_;

      const char * debugName_;

    public:
      Environment( std::list< Kernel::Environment * > & garbageArea, const char * debugName );
      Environment( std::list< Kernel::Environment * > & garbageArea, Environment * parent, Ast::IdentifierTree * bindings, const ValueVector & values, Ast::IdentifierTree * stateBindings, const StateVector & states, const char * debugName );
      ~Environment( );
      void setParent( Environment * parent );
      void setupDynamicKeyVariables( Ast::IdentifierTree * dynamicKeyBindings );
      void setupDynamicStateKeyVariables( Ast::IdentifierTree * dynamicStateKeyBindings );
      void extendVectors( );
      Kernel::Environment * getParent( );
      const Kernel::Environment * getParent( ) const;
      void clear( );
      Ast::AnalysisEnvironment * newAnalysisEnvironment( Ast::AnalysisEnvironment * parent = 0 ) const;

      void clear_gcMarked( ) { gcMarked_ = false; }
      bool gcMarked( ) const { return gcMarked_; }
      void gcMark( Kernel::GCMarkedSet & marked );

      static void collect( std::list< Kernel::Environment * > & garbageArea );

      void define( size_t pos, const Kernel::VariableHandle & val );
      void lookup( const LexicalKey & lexKey, Kernel::EvalState * evalState ) const;
      void lookup( size_t pos, Kernel::EvalState * evalState ) const;
      VariableHandle getVarHandle( const LexicalKey & lexKey );
      VariableHandle getVarHandle( size_t pos );

      void introduceState( size_t pos, Kernel::State * state );
      void freeze( size_t pos, Kernel::EvalState * evalState, const Ast::SourceLocation & loc );
      void peek( const LexicalKey & lexKey, Kernel::EvalState * evalState, const Ast::SourceLocation & loc );
      void tackOn( const LexicalKey & lexKey, Kernel::EvalState * evalState, const RefCountPtr< const Lang::Value > & piece, const Ast::SourceLocation & callLoc );
      StateHandle getStateHandle( const LexicalKey & lexKey );
      StateHandle getStateHandle( size_t pos );

      void defineDynamic( const Ast::PlacedIdentifier * debugId, size_t pos, const RefCountPtr< const Lang::Function > & filter, const Kernel::VariableHandle & defaultVal );

      const DynamicVariableProperties & lookupDynamicVariable( const LexicalKey & lexKey ) const;
      const DynamicVariableProperties & lookupDynamicVariable( size_t pos ) const;

      void defineDynamicState( const Ast::PlacedIdentifier * debugId, size_t pos, Kernel::PassedEnv env, Kernel::PassedDyn dyn, Ast::StateReference * defaultState );

      const DynamicStateProperties & lookupDynamicState( const LexicalKey & lexKey ) const;
      const DynamicStateProperties & lookupDynamicState( size_t pos ) const;


      size_t size( ) const;

      static size_t createdCount;
      static size_t liveCount;

      void print( std::ostream & os ) const;
      size_t recursivePrint( std::ostream & os, std::set< Ast::PlacedIdentifier > * shadowed, std::set< Ast::PlacedIdentifier > * shadowedStates ) const;

      bool isBaseEnvironment( ) const { return parent_ == 0; };

      /* The following public methods shall only be unsed on the base environment immediately after it has been created.
       */
      void initDefineHandle( const Ast::PlacedIdentifier & id, const Kernel::VariableHandle & val );
      void initDefineCoreFunction( Lang::CoreFunction * fun );
      void initDefineCoreFunction( RefCountPtr< const Lang::CoreFunction > fun );
      void initDefine( const Ast::PlacedIdentifier & id, const RefCountPtr< const Lang::Value > & val );
      void initDefine( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name, const RefCountPtr< const Lang::Value > & val );
      void initDefine( const Ast::PlacedIdentifier & id, Kernel::StateHandle state );
      void initDefine( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name, Kernel::StateHandle state );
      void initDefineClass( const RefCountPtr< const Lang::Class > & cls );
      void initDefineDynamic( DynamicVariableProperties * dynProps );
      void initDefineDynamic( const Ast::PlacedIdentifier & id, const RefCountPtr< const Lang::Function > & filter, const Kernel::VariableHandle & defaultVal );
      void initDefineDynamic( const Ast::PlacedIdentifier * id, const RefCountPtr< const Lang::Function > & filter, const Kernel::VariableHandle & defaultVal ); /* Transfers ownership. */
      void initDefineDynamicHandler( const Ast::PlacedIdentifier & id, const char * msg );

      /* Memory management for identifiers created during initialization.
       */
      static const Ast::PlacedIdentifier * newPlacedIdentifier( const RefCountPtr< const Ast::NamespacePath > & path, const char * simpleId );
      static const Ast::PlacedIdentifier * newPlacedIdentifier( const Ast::PlacedIdentifier & id );
     private:
      static PtrOwner_back_Access< std::list< const Ast::PlacedIdentifier * > > coreIdentifiers;
    };

  }

  namespace Ast
  {

    class AnalysisEnvironment
    {
    public:
      typedef Kernel::Environment::LexicalKey LexicalKey;
    private:
      AnalysisEnvironment * parent_;
      size_t level_; /* 0 for the kernel binding environment, otherwise one more than the parent's level. */
      typedef std::vector< StateID > StateVector;

      const Ast::IdentifierTree * bindings_;
      const Ast::IdentifierTree * dynamicKeyBindings_;
      const Ast::IdentifierTree * stateBindings_;
      static StateID nextStateID;
      StateVector states_;
      const Ast::IdentifierTree * dynamicStateKeyBindings_;
      std::vector< const Ast::Node * > statesFirstUse_;

      bool functionBoundary_;

    public:
      AnalysisEnvironment( PtrOwner_back_Access< std::list< Ast::AnalysisEnvironment * > > & deleter, Ast::AnalysisEnvironment * parent, const Ast::IdentifierTree * bindings, const Ast::IdentifierTree * stateBindings );
      ~AnalysisEnvironment( );
      void updateBindings( ); /* Call this method when additional bindings have been introduced after the AnalysisEnvironment was initialized. */
      Ast::AnalysisEnvironment * getParent( ) const;
      void setupDynamicKeyVariables( const Ast::IdentifierTree * dynamicKeyBindings );
      void setupDynamicStateKeyVariables( const Ast::IdentifierTree * dynamicStateKeyBindings );
      void activateFunctionBoundary( );
      size_t level( ) const { return level_; }; /* 0 means that this is the kernel binding environement, 1 means prelude environment, and so forth. */

      size_t findLocalVariablePosition( const Ast::SourceLocation & loc, const Ast::PlacedIdentifier & id ) const;
      LexicalKey findLexicalVariableKey( const Ast::SourceLocation & loc, const Ast::Identifier & id ) const;
      LexicalKey findPrivateAliasVariableKey( const Ast::SourceLocation & loc, const Ast::PlacedIdentifier & id, const char * privateName ) const;
      Ast::PlacedIdentifier reverseMapLexicalVariable( const LexicalKey & lexKey ) const;

      /* These only differ from those above by the error messages they may generate. */
      LexicalKey findLexicalTypeKey( const Ast::SourceLocation & loc, const Ast::Identifier & id ) const;

      size_t findLocalStatePosition( const Ast::SourceLocation & loc, const Ast::PlacedIdentifier & id ) const;
      LexicalKey findLexicalStateKey( const Ast::SourceLocation & loc, const Ast::Identifier & id, const Ast::Node * node );
      StateID getStateID( const LexicalKey & lexKey ) const;
      StateID getStateID( size_t pos ) const;
      static StateID getTheAnyStateID( );
      const Ast::Node * getStateFirstUse( size_t pos ) const;

      size_t findLocalDynamicPosition( const Ast::SourceLocation & loc, const Ast::PlacedIdentifier & id ) const;
      LexicalKey findLexicalDynamicKey( const Ast::SourceLocation & loc, const Ast::Identifier & id ) const;

      size_t findLocalDynamicStatePosition( const Ast::SourceLocation & loc, const Ast::PlacedIdentifier & id ) const;
      LexicalKey findLexicalDynamicStateKey( const Ast::SourceLocation & loc, const Ast::Identifier & id ) const;

      bool isBaseEnvironment( ) const { return parent_ == 0; };
    };

    class StateIDSet : public std::set< Ast::StateID >
    { };

  }

}
