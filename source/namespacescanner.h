/* This file is part of Shapes.
 *
 * Shapes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Shapes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2015 Henrik Tidefelt
 */

#pragma once

#include "yyltype.h"
#include "sourcelocation.h"
#include "namespacedirectory.h"

#include <string>
#include <set>
#include <map>

#ifndef FLEXINT_H /* Else *FlexLexer will be defined twice */
# undef yyFlexLexer
# define yyFlexLexer namespaceFlexLexer
# include <FlexLexer.h>
#endif


extern Shapes::YYLTYPE namespacelloc;

class NamespaceScanner : public namespaceFlexLexer
{
  bool moreState_;
  int lastleng_;
  void more( );
  void doBeforeEachAction( );
  void endOfLine( ); /* Use each time a token ending in a newline has been consumed. */
  Shapes::Ast::NamespaceDeclarations * declarations_;
  bool firstSet_;
  std::set< std::string > orderSet1_;
  std::set< std::string > orderSet2_;

 public:
  typedef int UnionType;
  UnionType yylval;
  NamespaceScanner( Shapes::Ast::NamespaceDeclarations * declarations, std::istream * yyin, const Shapes::Ast::FileID * fileID );
  virtual int yylex( );

 private:
  static std::string rinseString( const char * str );
};
