/* This file is part of Shapes.
 *
 * Shapes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Shapes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2008, 2009, 2014 Henrik Tidefelt
 */

#include <cmath>

#include "Shapes_Helpers_decls.h"

#include "shapescore.h"
#include "ast.h"
#include "globals.h"
#include "shapesexceptions.h"
#include "consts.h"
#include "simplepdfi.h"

#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include <stdio.h>

using namespace Shapes;


namespace Shapes
{
	namespace Lang
	{
		class Core_duration : public Lang::CoreFunction
		{
		public:
			Core_duration( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name ) : CoreFunction( ns, name ) { }
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				const size_t ARITY = 1;
				CHECK_ARITY( args, ARITY, id_ );

				try
					{
						typedef const Lang::ElementaryPath2D ArgType;
						RefCountPtr< ArgType > arg = Helpers::elementaryPathTry2D( args.getValue( 0 ) );
						Kernel::ContRef cont = evalState->cont_;
						cont->takeValue( Kernel::ValueRef( new Lang::Integer( arg->duration( ) ) ),
														 evalState );
						return;
					}
				catch( const NonLocalExit::NotThisType & ball )
					{
						/* Wrong type; never mind!.. but see below!
						 */
					}

				try
					{
						typedef const Lang::ElementaryPath3D ArgType;
						RefCountPtr< ArgType > arg = Helpers::elementaryPathTry3D( args.getValue( 0 ) );
						Kernel::ContRef cont = evalState->cont_;
						cont->takeValue( Kernel::ValueRef( new Lang::Integer( arg->duration( ) ) ),
														 evalState );
						return;
					}
				catch( const NonLocalExit::NotThisType & ball )
					{
						/* Wrong type; never mind!.. but see below!
						 */
					}

				throw Exceptions::CoreTypeMismatch( callLoc, id_, args, 0, Helpers::typeSetString( Lang::ElementaryPath2D::staticTypeName( ), Lang::ElementaryPath3D::staticTypeName( ) ) );
			}
		};

		class Core_controlling_maximizer : public Lang::CoreFunction
		{
		public:
			Core_controlling_maximizer( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name ) : CoreFunction( ns, name ) { }
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				const size_t ARITY = 2;
				CHECK_ARITY( args, ARITY, id_ );

				{
					typedef const Lang::ElementaryPath2D ArgType0;
					RefCountPtr< ArgType0 > p = NullPtr< ArgType0 >( );
					size_t argsi = 0;
					try
						{
							p = Helpers::elementaryPathTry2D( args.getValue( argsi ) );
						}
					catch( const NonLocalExit::NotThisType & ball )
						{
							goto nextType1;
						}

					++argsi;

					typedef const Lang::FloatPair ArgType1;
					RefCountPtr< ArgType1 > d = Helpers::down_cast_CoreArgument< ArgType1 >( id_, args, argsi, callLoc );

					Kernel::ContRef cont = evalState->cont_;
					cont->takeValue( p->controllingMaximizer( *d ),
													 evalState );
					return;
				}

			nextType1:

				{
					typedef const Lang::ElementaryPath3D ArgType0;
					RefCountPtr< ArgType0 > p = NullPtr< ArgType0 >( );
					size_t argsi = 0;
					try
						{
							p = Helpers::elementaryPathTry3D( args.getValue( argsi ) );
						}
					catch( const NonLocalExit::NotThisType & ball )
						{
							goto nextType2;
						}

					++argsi;

					typedef const Lang::FloatTriple ArgType1;
					RefCountPtr< ArgType1 > d = Helpers::down_cast_CoreArgument< ArgType1 >( id_, args, argsi, callLoc );

					Kernel::ContRef cont = evalState->cont_;
					cont->takeValue( p->controllingMaximizer( *d ),
													 evalState );
					return;
				}

			nextType2:
				throw Exceptions::CoreTypeMismatch( callLoc, id_, args, 0, Helpers::typeSetString( Lang::ElementaryPath2D::staticTypeName( ), Lang::ElementaryPath3D::staticTypeName( ) ) );
			}
		};

		class Core_discrete_mean : public Lang::CoreFunction
		{
		public:
			Core_discrete_mean( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name ) : CoreFunction( ns, name ) { }
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				const size_t ARITY = 1;
				CHECK_ARITY( args, ARITY, id_ );

				size_t argsi = 0;

				try
					{
						typedef const Lang::ElementaryPath2D ArgType;
						RefCountPtr< ArgType > p = Helpers::elementaryPathTry2D( args.getValue( argsi ) );
						Kernel::ContRef cont = evalState->cont_;
						cont->takeValue( p->discreteMean( ),
														 evalState );
						return;
					}
				catch( const NonLocalExit::NotThisType & ball )
					{
						/* Wrong type; never mind!.. but see below!
						 */
					}

				try
					{
						typedef const Lang::ElementaryPath3D ArgType;
						RefCountPtr< ArgType > p = Helpers::elementaryPathTry3D( args.getValue( argsi ) );
						Kernel::ContRef cont = evalState->cont_;
						cont->takeValue( p->discreteMean( ),
														 evalState );
						return;
					}
				catch( const NonLocalExit::NotThisType & ball )
					{
						/* Wrong type; never mind!.. but see below!
						 */
					}

				throw Exceptions::CoreTypeMismatch( callLoc, id_, args, 0, Helpers::typeSetString( Lang::ElementaryPath2D::staticTypeName( ), Lang::ElementaryPath3D::staticTypeName( ) ) );
			}
		};

		class Core_discrete_maximizer : public Lang::CoreFunction
		{
		public:
			Core_discrete_maximizer( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name ) : CoreFunction( ns, name ) { }
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				const size_t ARITY = 2;
				CHECK_ARITY( args, ARITY, id_ );

				{
					typedef const Lang::ElementaryPath2D ArgType0;
					RefCountPtr< ArgType0 > p = NullPtr< ArgType0 >( );
					size_t argsi = 0;
					try
						{
							p = Helpers::elementaryPathTry2D( args.getValue( argsi ) );
						}
					catch( const NonLocalExit::NotThisType & ball )
						{
							goto nextType1;
						}

					++argsi;

					typedef const Lang::FloatPair ArgType1;
					RefCountPtr< ArgType1 > d = Helpers::down_cast_CoreArgument< ArgType1 >( id_, args, argsi, callLoc );

					Kernel::ContRef cont = evalState->cont_;
					typedef const Lang::PathSlider2D ResultType;
					cont->takeValue( RefCountPtr< ResultType >( new ResultType( p, p->discreteMaximizer( *d ) ) ),
													 evalState );
					return;
				}

			nextType1:

				{
					typedef const Lang::ElementaryPath3D ArgType0;
					RefCountPtr< ArgType0 > p = NullPtr< ArgType0 >( );
					size_t argsi = 0;
					try
						{
							p = Helpers::elementaryPathTry3D( args.getValue( argsi ) );
						}
					catch( const NonLocalExit::NotThisType & ball )
						{
							goto nextType2;
						}

					++argsi;

					typedef const Lang::FloatTriple ArgType1;
					RefCountPtr< ArgType1 > d = Helpers::down_cast_CoreArgument< ArgType1 >( id_, args, argsi, callLoc );

					Kernel::ContRef cont = evalState->cont_;
					typedef const Lang::PathSlider3D ResultType;
					cont->takeValue( RefCountPtr< ResultType >( new ResultType( p, p->discreteMaximizer( *d ) ) ),
													 evalState );
					return;
				}

			nextType2:
				throw Exceptions::CoreTypeMismatch( callLoc, id_, args, 0, Helpers::typeSetString( Lang::ElementaryPath2D::staticTypeName( ), Lang::ElementaryPath3D::staticTypeName( ) ) );
			}
		};

		class Core_discrete_approximator : public Lang::CoreFunction
		{
		public:
			Core_discrete_approximator( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name ) : CoreFunction( ns, name ) { }
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				const size_t ARITY = 2;
				CHECK_ARITY( args, ARITY, id_ );

				{
					typedef const Lang::ElementaryPath2D ArgType0;
					RefCountPtr< ArgType0 > p = NullPtr< ArgType0 >( );
					size_t argsi = 0;
					try
						{
							p = Helpers::elementaryPathTry2D( args.getValue( argsi ) );
						}
					catch( const NonLocalExit::NotThisType & ball )
						{
							goto nextType1;
						}

					++argsi;

					typedef const Lang::Coords2D ArgType1;
					RefCountPtr< ArgType1 > d = Helpers::down_cast_CoreArgument< ArgType1 >( id_, args, argsi, callLoc );

					Kernel::ContRef cont = evalState->cont_;
					typedef const Lang::PathSlider2D ResultType;
					cont->takeValue( RefCountPtr< ResultType >( new ResultType( p, p->discreteApproximator( *d ) ) ),
													 evalState );
					return;
				}

			nextType1:

				{
					typedef const Lang::ElementaryPath3D ArgType0;
					RefCountPtr< ArgType0 > p = NullPtr< ArgType0 >( );
					size_t argsi = 0;
					try
						{
							p = Helpers::elementaryPathTry3D( args.getValue( argsi ) );
						}
					catch( const NonLocalExit::NotThisType & ball )
						{
							goto nextType2;
						}

					++argsi;

					typedef const Lang::Coords3D ArgType1;
					RefCountPtr< ArgType1 > d = Helpers::down_cast_CoreArgument< ArgType1 >( id_, args, argsi, callLoc );

					Kernel::ContRef cont = evalState->cont_;
					typedef const Lang::PathSlider3D ResultType;
					cont->takeValue( RefCountPtr< ResultType >( new ResultType( p, p->discreteApproximator( *d ) ) ),
													 evalState );
					return;
				}

			nextType2:
				throw Exceptions::CoreTypeMismatch( callLoc, id_, args, 0, Helpers::typeSetString( Lang::ElementaryPath2D::staticTypeName( ), Lang::ElementaryPath3D::staticTypeName( ) ) );
			}
		};

		class Core_continuous_mean : public Lang::CoreFunction
		{
		public:
			Core_continuous_mean( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name ) : CoreFunction( ns, name ) { }
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				const size_t ARITY = 1;
				CHECK_ARITY( args, ARITY, id_ );

				size_t argsi = 0;

				try
					{
						typedef const Lang::ElementaryPath2D ArgType;
						RefCountPtr< ArgType > p = Helpers::elementaryPathTry2D( args.getValue( argsi ) );
						Kernel::ContRef cont = evalState->cont_;
						cont->takeValue( p->continuousMean( ),
														 evalState );
						return;
					}
				catch( const NonLocalExit::NotThisType & ball )
					{
						/* Wrong type; never mind!.. but see below!
						 */
					}

				try
					{
						typedef const Lang::ElementaryPath3D ArgType;
						RefCountPtr< ArgType > p = Helpers::elementaryPathTry3D( args.getValue( argsi ) );
						Kernel::ContRef cont = evalState->cont_;
						cont->takeValue( p->continuousMean( ),
														 evalState );
						return;
					}
				catch( const NonLocalExit::NotThisType & ball )
					{
						/* Wrong type; never mind!.. but see below!
						 */
					}

				throw Exceptions::CoreTypeMismatch( callLoc, id_, args, 0, Helpers::typeSetString( Lang::ElementaryPath2D::staticTypeName( ), Lang::ElementaryPath3D::staticTypeName( ) ) );
			}
		};

		class Core_continuous_maximizer : public Lang::CoreFunction
		{
		public:
			Core_continuous_maximizer( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name ) : CoreFunction( ns, name ) { }
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				const size_t ARITY = 2;
				CHECK_ARITY( args, ARITY, id_ );

				{
					typedef const Lang::ElementaryPath2D ArgType0;
					RefCountPtr< ArgType0 > p = NullPtr< ArgType0 >( );
					size_t argsi = 0;
					try
						{
							p = Helpers::elementaryPathTry2D( args.getValue( argsi ) );
						}
					catch( const NonLocalExit::NotThisType & ball )
						{
							goto nextType1;
						}

					++argsi;

					typedef const Lang::FloatPair ArgType1;
					RefCountPtr< ArgType1 > d = Helpers::down_cast_CoreArgument< ArgType1 >( id_, args, argsi, callLoc );

					Kernel::ContRef cont = evalState->cont_;
					typedef const Lang::PathSlider2D ResultType;
					cont->takeValue( RefCountPtr< ResultType >( new ResultType( p, p->continuousMaximizer( *d ) ) ),
													 evalState );
					return;
				}

			nextType1:

				{
					typedef const Lang::ElementaryPath3D ArgType0;
					RefCountPtr< ArgType0 > p = NullPtr< ArgType0 >( );
					size_t argsi = 0;
					try
						{
							p = Helpers::elementaryPathTry3D( args.getValue( argsi ) );
						}
					catch( const NonLocalExit::NotThisType & ball )
						{
							goto nextType2;
						}

					++argsi;

					typedef const Lang::FloatTriple ArgType1;
					RefCountPtr< ArgType1 > d = Helpers::down_cast_CoreArgument< ArgType1 >( id_, args, argsi, callLoc );

					Kernel::ContRef cont = evalState->cont_;
					typedef const Lang::PathSlider3D ResultType;
					cont->takeValue( RefCountPtr< ResultType >( new ResultType( p, p->continuousMaximizer( *d ) ) ),
													 evalState );
					return;
				}

			nextType2:
				throw Exceptions::CoreTypeMismatch( callLoc, id_, args, 0, Helpers::typeSetString( Lang::ElementaryPath2D::staticTypeName( ), Lang::ElementaryPath3D::staticTypeName( ) ) );
			}
		};

		class Core_continuous_approximator : public Lang::CoreFunction
		{
		public:
			Core_continuous_approximator( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name ) : CoreFunction( ns, name ) { }
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				const size_t ARITY = 2;
				CHECK_ARITY( args, ARITY, id_ );

				{
					typedef const Lang::ElementaryPath2D ArgType0;
					RefCountPtr< ArgType0 > p = NullPtr< ArgType0 >( );
					size_t argsi = 0;
					try
						{
							p = Helpers::elementaryPathTry2D( args.getValue( argsi ) );
						}
					catch( const NonLocalExit::NotThisType & ball )
						{
							goto nextType1;
						}

					++argsi;

					{
						typedef const Lang::Coords2D ArgType1;
						try
							{
								RefCountPtr< ArgType1 > d = Helpers::try_cast_CoreArgument< ArgType1 >( args.getValue( argsi ) );

								Kernel::ContRef cont = evalState->cont_;
								typedef const Lang::PathSlider2D ResultType;
								cont->takeValue( RefCountPtr< ResultType >( new ResultType( p, p->continuousApproximator( *d ) ) ),
																 evalState );
								return;
							}
						catch( const NonLocalExit::NotThisType & ball )
							{
								goto nextType11;
							}
					}

				nextType11:
					{
						typedef const Lang::ElementaryPath2D ArgType1;
						RefCountPtr< ArgType1 > p2 = NullPtr< ArgType1 >( );
						try
							{
								p2 = Helpers::elementaryPathTry2D( args.getValue( argsi ) );
								RefCountPtr< const Lang::Structure > info = NullPtr< typeof *info >( );
								Concrete::SplineTime t = p->continuousApproximator( p2, & info );
								Kernel::ContRef cont = evalState->cont_;
								typedef const Lang::PathSlider2D ResultType;
								cont->takeValue( RefCountPtr< ResultType >( new ResultType( p, t, Kernel::VariableHandle( new Kernel::Variable( info ) ) ) ),
																 evalState );
								return;
							}
						catch( const NonLocalExit::NotThisType & ball )
							{
								goto nextType12;
							}
					}

				nextType12:
					throw Exceptions::CoreTypeMismatch( callLoc, id_, args, 1, Helpers::typeSetString( Lang::Coords2D::staticTypeName( ), Lang::ElementaryPath2D::staticTypeName( ) ) );
				}

			nextType1:

				{
					typedef const Lang::ElementaryPath3D ArgType0;
					RefCountPtr< ArgType0 > p = NullPtr< ArgType0 >( );
					size_t argsi = 0;
					try
						{
							p = Helpers::elementaryPathTry3D( args.getValue( argsi ) );
						}
					catch( const NonLocalExit::NotThisType & ball )
						{
							goto nextType2;
						}

					++argsi;

					{
						typedef const Lang::Coords3D ArgType1;
						try
							{
								RefCountPtr< ArgType1 > d = Helpers::try_cast_CoreArgument< ArgType1 >( args.getValue( argsi ) );

								Kernel::ContRef cont = evalState->cont_;
								typedef const Lang::PathSlider3D ResultType;
								cont->takeValue( RefCountPtr< ResultType >( new ResultType( p, p->continuousApproximator( *d ) ) ),
																 evalState );
								return;
							}
						catch( const NonLocalExit::NotThisType & ball )
							{
								goto nextType21;
							}
					}

				nextType21:
					{
						typedef const Lang::ElementaryPath3D ArgType1;
						RefCountPtr< ArgType1 > p2 = NullPtr< ArgType1 >( );
						try
							{
								p2 = Helpers::elementaryPathTry3D( args.getValue( argsi ) );
								RefCountPtr< const Lang::Structure > info = NullPtr< typeof *info >( );
								Concrete::SplineTime t = p->continuousApproximator( p2, & info );
								Kernel::ContRef cont = evalState->cont_;
								typedef const Lang::PathSlider3D ResultType;
								cont->takeValue( RefCountPtr< ResultType >( new ResultType( p, t, Kernel::VariableHandle( new Kernel::Variable( info ) ) ) ),
																 evalState );
								return;
							}
						catch( const NonLocalExit::NotThisType & ball )
							{
								goto nextType22;
							}
					}

				nextType22:
					throw Exceptions::CoreTypeMismatch( callLoc, id_, args, 1, Helpers::typeSetString( Lang::Coords3D::staticTypeName( ), Lang::ElementaryPath3D::staticTypeName( ) ) );
				}

			nextType2:
				throw Exceptions::CoreTypeMismatch( callLoc, id_, args, 0, Helpers::typeSetString( Lang::ElementaryPath2D::staticTypeName( ), Lang::ElementaryPath3D::staticTypeName( ) ) );
			}
		};


		class Core_intersection : public Lang::CoreFunction
		{
			mutable Kernel::Environment::LexicalKey * idKey;
		public:
			Core_intersection( const RefCountPtr< const Ast::NamespacePath > & ns, const char * name ) : CoreFunction( ns, name ), idKey( 0 ) { }
			virtual ~Core_intersection( ) { if( idKey != 0 ){ delete idKey; } }
			virtual void
			call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
			{
				const size_t ARITY = 2;
				CHECK_ARITY( args, ARITY, id_ );

				typedef const Lang::ElementaryPath2D ArgType1;
				size_t argsi = 0;
				RefCountPtr< ArgType1 > p = Helpers::elementaryPathCast2D( id_, args, argsi, callLoc );

				++argsi;

				typedef const Lang::ElementaryPath2D ArgType2;
				RefCountPtr< ArgType2 > p2 = Helpers::elementaryPathCast2D( id_, args, argsi, callLoc );

				Kernel::ContRef cont = evalState->cont_;
				try
					{
						RefCountPtr< const Lang::Structure > info = NullPtr< typeof *info >( );
						Concrete::SplineTime t = p->intersection( p2, & info );
						typedef const Lang::PathSlider2D ResultType;
						cont->takeValue( RefCountPtr< ResultType >( new ResultType( p, t, Kernel::VariableHandle( new Kernel::Variable( info ) ) ) ),
														 evalState );
					}
				catch( const Exceptions::OutOfRange & ball )
					{
						if( idKey == 0 )
							{
								Ast::Identifier * handlerId = Lang::HANDLER_NO_INTERSECTION.newAbsolute( );
								idKey = new Kernel::Environment::LexicalKey( Ast::theGlobalAnalysisEnvironment->findLexicalDynamicKey( callLoc, *handlerId ) );
								delete handlerId;
							}

						const Kernel::DynamicVariableProperties & dynProps = Kernel::theGlobalEnvironment->lookupDynamicVariable( *idKey );

						Kernel::VariableHandle handler = dynProps.fetch( evalState->dyn_ );

						typedef const Lang::Function HandlerType;

						RefCountPtr< HandlerType > fun( handler->getVal< HandlerType >( callLoc ) );
						fun->call( evalState, args.getValue( 0 ), args.getValue( 1 ), callLoc );
					}
			}
		};
	}
}


void
Kernel::registerCore_point( Kernel::Environment * env )
{
	env->initDefineCoreFunction( new Lang::Core_duration( Lang::THE_NAMESPACE_Shapes_Geometry, "duration" ) );
	env->initDefineCoreFunction( new Lang::Core_controlling_maximizer( Lang::THE_NAMESPACE_Shapes_Geometry, "controlling_maximizer" ) );
	env->initDefineCoreFunction( new Lang::Core_discrete_mean( Lang::THE_NAMESPACE_Shapes_Geometry, "pathpoint_mean" ) );
	env->initDefineCoreFunction( new Lang::Core_discrete_maximizer( Lang::THE_NAMESPACE_Shapes_Geometry, "pathpoint_maximizer" ) );
	env->initDefineCoreFunction( new Lang::Core_discrete_approximator( Lang::THE_NAMESPACE_Shapes_Geometry, "pathpoint_approximator" ) );
	env->initDefineCoreFunction( new Lang::Core_continuous_mean( Lang::THE_NAMESPACE_Shapes_Geometry, "mean" ) );
	env->initDefineCoreFunction( new Lang::Core_continuous_maximizer( Lang::THE_NAMESPACE_Shapes_Geometry, "maximizer" ) );
	env->initDefineCoreFunction( new Lang::Core_continuous_approximator( Lang::THE_NAMESPACE_Shapes_Geometry, "approximator" ) );
	env->initDefineCoreFunction( new Lang::Core_intersection( Lang::THE_NAMESPACE_Shapes_Geometry, "intersection" ) );
}
