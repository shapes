/* This file is part of Shapes.
 *
 * Shapes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Shapes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2008, 2009 Henrik Tidefelt
 */

#include "main.h"

#include <typeinfo>

using namespace Shapes;
using namespace SimplePDF;

void
nonInteractiveEvaluation( Kernel::WarmCatalog::ShipoutList & documents, SplitMode splitMode,
													bool evalTrace, bool evalBackTrace, bool cleanupMemory,
													RefCountPtr< std::ifstream > & labelDBFile, const std::string & labelDBName )
{
	try
		{
			shapesparse( );
			initCore( labelDBFile, labelDBName );

			RefCountPtr< const Kernel::GraphicsState > graphicsState( new Kernel::GraphicsState( true ) );
			Kernel::PassedDyn baseDyn( new Kernel::DynamicEnvironment( graphicsState ) );

			bool done = false;
			Kernel::ValueRef resultUntyped = NullPtr< const Lang::Value >( );
			Kernel::EvalState evalState( Ast::theProgram,
																	 Kernel::theGlobalEnvironment,
																	 baseDyn,
																	 Kernel::ContRef( new Kernel::StoreValueContinuation( & resultUntyped,
																																												Kernel::ContRef( new Kernel::ExitContinuation( & done, Ast::theProgram->loc( ) ) ),
																																												Ast::theProgram->loc( ) ) ) );

		restart:
			try
				{
					while( ! done )
						{
							if( evalTrace )
								{
									if( evalBackTrace )
										{
											evalState.cont_->backTrace( std::cerr );
											std::cerr << "--- Bottom of trace ---" << std::endl ;
										}
									else
										{
											std::cerr << "Eval trace: Cont: " << evalState.cont_->traceLoc( ) << "  Expr: " << evalState.expr_->loc( ) << std::endl ;
										}
								}
							Ast::Expression * expr = evalState.expr_;
							expr->eval( & evalState );
						}
				}
			catch( const Exceptions::StaticInconsistency & ball )
				{
					std::cout.flush( );
					std::cerr << ball.loc( ) << ": " ;
					ball.display( std::cerr );
					abortProcedure( ball.exitCode( ) );
				}
			catch( const Exceptions::CatchableError & ball )
				{
					Kernel::ContRef cont = evalState.dyn_->getEscapeContinuation( Lang::CONTINUATION_ID_ERROR, ball.getLoc( ) );
					cont->takeValue( ball.clone( evalState.cont_ ),
													 & evalState );
					goto restart;
				}
			catch( const Exceptions::UncaughtError & uncaught )
				{
					const Lang::Exception & ball = *uncaught.ball( );
					std::cout.flush( );
					if( Interaction::debugBacktrace )
						{
							ball.cont( )->backTrace( std::cerr );
						}

					if( ! ball.loc( ).isUnknown( ) )
						{
							std::cerr << ball.loc( ) << Exceptions::Exception::locsep ;
						}
					else
						{
							std::cerr << ball.cont( )->traceLoc( ) << Exceptions::Exception::locsep ;
						}
					ball.show( std::cerr );
					abortProcedure( ball.exitCode( ) );
				}
			catch( const Exceptions::Exception & ball )
				{
					std::cout.flush( );
					if( Interaction::debugBacktrace )
						{
							evalState.cont_->backTrace( std::cerr );
						}

					try
						{
							const Exceptions::RuntimeError & rerr = dynamic_cast< const Exceptions::RuntimeError & >( ball );
							if( rerr.getLoc( ).isUnknown( ) )
								{
									throw std::bad_cast( ); /* It's not a bad cast, but the cast value is just as little useful as a bad cast. */
								}
							std::cerr << rerr.getLoc( ) << Exceptions::Exception::locsep ;
						}
					catch( std::bad_cast & bad_cast_ball )
						{
							std::cerr << evalState.cont_->traceLoc( ) << Exceptions::Exception::locsep ;
						}
					ball.display( std::cerr );
					abortProcedure( ball.exitCode( ) );
				}

			if( ! Interaction::blankMode )
				shipout( resultUntyped, baseDyn, & documents, splitMode );

			if( cleanupMemory )
				{
					Kernel::theGlobalEnvironment->clear( );
				}

			delete Kernel::theGlobalEnvironment;
			delete Ast::theProgram;
		}
	catch( const Exceptions::StaticInconsistency & ball )
		{
			std::cout.flush( );
			std::cerr << ball.loc( ) << ": " ;
			ball.display( std::cerr );
			abortProcedure( ball.exitCode( ) );
		}
	catch( const Exceptions::UncaughtError & uncaught )
		{
			const Lang::Exception & ball = *uncaught.ball( );
			std::cout.flush( );
			if( Interaction::debugBacktrace )
				{
					ball.cont( )->backTrace( std::cerr );
				}

			if( ! ball.loc( ).isUnknown( ) )
				{
					std::cerr << ball.loc( ) << Exceptions::Exception::locsep ;
				}
			else
				{
					std::cerr << ball.cont( )->traceLoc( ) << Exceptions::Exception::locsep ;
				}
			ball.show( std::cerr );
			abortProcedure( ball.exitCode( ) );
		}
	catch( const Exceptions::Exception & ball )
		{
			std::cout.flush( );
			ball.display( std::cerr );
			abortProcedure( ball.exitCode( ) );
		}
}


void
shipout( Kernel::ValueRef resultUntyped, Kernel::PassedDyn baseDyn, Kernel::WarmCatalog::ShipoutList * documents, SplitMode splitMode )
{
	Kernel::WarmCatalog * catalog = dynamic_cast< Kernel::WarmCatalog * >( Kernel::theGlobalEnvironment->getStateHandle( Ast::theGlobalAnalysisEnvironment->findLocalStatePosition( Ast::THE_UNKNOWN_LOCATION, Lang::CATALOG_ID ) ) );
	RefCountPtr< const Lang::Group2D > finalPicture = dynamic_cast< Kernel::WarmGroup2D * >( Kernel::theGlobalEnvironment->getStateHandle( Ast::theGlobalAnalysisEnvironment->findLocalStatePosition( Ast::THE_UNKNOWN_LOCATION, Lang::CANVAS_ID ) ) )->getPile( );
	RefCountPtr< const Lang::Drawable2D > result = Helpers::down_cast< const Lang::Drawable2D >( resultUntyped, "", true ); /* True means "void is null". */
	if( result != NullPtr< const Lang::Drawable2D >( ) && result == Lang::THE_NULL2D )
		{
			throw Exceptions::MiscellaneousRequirement( "Program evaluated to a null-value." );
		}
	if( result == NullPtr< const Lang::Drawable2D >( ) && catalog->isEmpty( ) && finalPicture->isNull( ) )
		{
			throw Exceptions::EmptyFinalPicture( );
		}
	if( catalog->isEmpty( ) )
		{
			if( result != NullPtr< const Lang::Drawable2D >( ) )
				{
					catalog->tackOnPage( baseDyn, result, Ast::THE_UNKNOWN_LOCATION );
				}
			else
				{
					catalog->tackOnPage( baseDyn, finalPicture, Ast::THE_UNKNOWN_LOCATION );
				}
		}
	else
		{
			if( result != NullPtr< const Lang::Drawable2D >( ) )
				{
					throw Exceptions::MiscellaneousRequirement( "Ambiguous program output; #catalog was non-empty, and program returned with a value." );
				}
		}

	catalog->shipout( splitMode != SPLIT_NO, documents );
}


void
initCore( RefCountPtr< std::ifstream > & labelDBFile, const std::string & labelDBName )
{
	Kernel::theGlobalEnvironment = new Kernel::Environment( Kernel::theEnvironmentList, "Global" );
	Kernel::registerGlobals( Kernel::theGlobalEnvironment );
	Kernel::registerDynamic( Kernel::theGlobalEnvironment );
	Kernel::registerHot( Kernel::theGlobalEnvironment );
	Kernel::registerClasses( Kernel::theGlobalEnvironment );
	Kernel::registerCore_elem( Kernel::theGlobalEnvironment );
	Kernel::registerCore_point( Kernel::theGlobalEnvironment );
	Kernel::registerCore_path( Kernel::theGlobalEnvironment );
	Kernel::registerCore_draw( Kernel::theGlobalEnvironment );
	Kernel::registerCore_construct( Kernel::theGlobalEnvironment );
	Kernel::registerCore_font( Kernel::theGlobalEnvironment );
	Kernel::registerCore_misc( Kernel::theGlobalEnvironment );
	Kernel::registerCore_sort( Kernel::theGlobalEnvironment );
	Kernel::registerCore_state( Kernel::theGlobalEnvironment );
	Kernel::registerCore_annotation( Kernel::theGlobalEnvironment );
	Kernel::registerCore_decomp( Kernel::theGlobalEnvironment );
	Ast::theGlobalAnalysisEnvironment = Kernel::theGlobalEnvironment->newAnalysisEnvironment( );
	{
		Ast::StateIDSet freeStates;
		Ast::theProgram->analyze( 0, Ast::theGlobalAnalysisEnvironment, & freeStates );
	}
	if( ! Ast::theAnalysisErrorsList.empty( ) )
		{
			std::cout.flush( );
			typedef typeof Ast::theAnalysisErrorsList ListType;
			for( ListType::const_iterator i = Ast::theAnalysisErrorsList.begin( ); i != Ast::theAnalysisErrorsList.end( ); ++i )
				{
					{
						typedef const Exceptions::StaticInconsistency ErrorType;
						ErrorType * err = dynamic_cast< ErrorType * >( *i );
						if( err != 0 )
							{
								std::cerr << err->loc( ) << ": " ;
								err->display( std::cerr );
								continue;
							}
					}
					std::cerr << "(Bad exception type)" << ": " ;
					(*i)->display( std::cerr );
				}
			abortProcedure( Interaction::EXIT_USER_ERROR );
		}

	// The display unit is looked up after the input is scanned, so the user may use her own units
	Interaction::displayUnitFactor = Ast::theShapesScanner.lookupLengthUnitFactor( Interaction::displayUnitName );
	if( Interaction::displayUnitFactor <= 0 )
		{
			std::cerr << "Invalid display unit: " << Interaction::displayUnitName << std::endl ;
			abortProcedure( Interaction::EXIT_INVOCATION_ERROR );
		}
	Interaction::displayUnit = Concrete::Length( 1 / Interaction::displayUnitFactor );
	labelDBFile = performIterativeStartup( labelDBName );
}
