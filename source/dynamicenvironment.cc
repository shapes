/* This file is part of Shapes.
 *
 * Shapes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Shapes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2008, 2010, 2014 Henrik Tidefelt
 */

#include <cmath>

#include "Shapes_Helpers_decls.h"

#include "dynamicenvironment.h"
#include "hottypes.h"
#include "globals.h"
#include "shapescore.h"
#include "ast.h"
#include "astvar.h"
#include "isnan.h"
#include "continuations.h"
#include "check.h"

#include <limits>
#include <iomanip>

using namespace Shapes;


void
Kernel::SpecialUnitVariables::specialUnitService( Concrete::Length * d, double * a0, double * a1 )
{
	if( p0_ == 0 || p1_ == 0 )
		{
			throw Exceptions::InternalError( "The path points were not setup before calling specialUnitService" );
		}
	Concrete::Length x0;
	Concrete::Length y0;
	Concrete::Length x3;
	Concrete::Length y3;
	double ag0; /* global angles that will later be compared with aRef */
	double ag1;

	if( reverseDirection_ )
		{
			x0 = p1_->mid_->x_;
			y0 = p1_->mid_->y_;
			x3 = p0_->mid_->x_;
			y3 = p0_->mid_->y_;
			ag0 = p1_->rearAngle_;
			ag1 = p0_->frontAngle_;
		}
	else
		{
			x0 = p0_->mid_->x_;
			y0 = p0_->mid_->y_;
			x3 = p1_->mid_->x_;
			y3 = p1_->mid_->y_;
			ag0 = p0_->frontAngle_;
			ag1 = p1_->rearAngle_;
		}

	Concrete::Length dx = x3 - x0;
	Concrete::Length dy = y3 - y0;

	*d = hypotPhysical( dx, dy );

	double aRef = atan2( dy.offtype< 1, 0 >( ), dx.offtype< 1, 0 >( ) );	/* Angles will be relative to the angle pointing to the other mid point */

	if( p0_->frontState_ & Concrete::PathPoint2D::FREE_ANGLE )
		{
			throw Exceptions::InternalError( "Found a free angle in specialUnitService." );
		}

	if( p1_->rearState_ & Concrete::PathPoint2D::FREE_ANGLE )
		{
			throw Exceptions::InternalError( "Found a free angle in specialUnitService." );
		}

	CHECK(
				if( IS_NAN( ag0 ) )
					{
						std::ostringstream msg;
						msg << "Using uninitialized adjacent angle in "
								<< "(" << Concrete::Length::offtype( x0 ) << "bp," << Concrete::Length::offtype( y0 ) << "bp)"
								<< "--"
								<< "(" << Concrete::Length::offtype( x3 ) << "bp," << Concrete::Length::offtype( y3 ) << "bp)" ;
						throw Exceptions::InternalError( strrefdup( msg ) );
					}
				);

	double ar0 = aRef - ag0;
	if( ar0 < - M_PI )
		{
			do
				{
					ar0 += 2 * M_PI;
				}
			while( ar0 < - M_PI );
		}
	else
		{
			while( ar0 > M_PI )
				{
					ar0 -= 2 * M_PI;
				}
		}

	double ar1 = ag1 - aRef - M_PI;
	if( IS_NAN( ag1 ) )
		{
			/* The angle is NaN is there is no opposite angle.  The implicit direction is then towards the neighboring point.
			 */
			ar1 = 0;
		}
	else
		{
			if( ar1 < - M_PI )
				{
					do
						{
							ar1 += 2 * M_PI;
						}
					while( ar1 < - M_PI );
				}
			else
				{
					while( ar1 > M_PI )
						{
							ar1 -= 2 * M_PI;
						}
				}
		}

	if( ar0 < 0 )
		{
			ar0 = - ar0;
			ar1 = - ar1;
		}

	ar1 -= ar0;

	if( ar1 < - M_PI )
		{
			ar1 += 2 * M_PI;
		}
	else if( ar1 > M_PI )
		{
			ar1 -= 2 * M_PI;
		}

	*a0 = ar0;
	*a1 = ar1;
}

Kernel::SystemDynamicVariables::SystemDynamicVariables( )
	: graphicsState_( NullPtr< const Kernel::GraphicsState >( ) ),
		facetState_( NullPtr< const Kernel::FacetState >( ) ),
		textState_( NullPtr< const Kernel::TextState >( ) ),
		eyez_( std::numeric_limits< double >::signaling_NaN( ) ),
		TeX_bleed_( std::numeric_limits< double >::signaling_NaN( ) ),
		defaultUnit_( NullPtr< const Kernel::PolarHandlePromise >( ) ),
		blendSpace_( NullPtr< const Lang::ColorSpace >( ) )
{ }

Kernel::SystemDynamicVariables::SystemDynamicVariables( const RefCountPtr< const Kernel::GraphicsState > & graphicsState )
	: graphicsState_( graphicsState ),
		facetState_( new Kernel::FacetState( true ) ),
		textState_( new Kernel::TextState( true ) ),
		eyez_( 50 * 72 / 2.54 ), /* 50 cm */
		TeX_bleed_( 0.5 ), /* 0.5 bp */
		defaultUnit_( new Kernel::PolarHandleEmptyPromise( ) ),
		blendSpace_( Lang::THE_INHERITED_COLOR_SPACE )
{ }

void
Kernel::SystemDynamicVariables::addFrom( const SystemDynamicVariables & other )
{
	if( graphicsState_ == NullPtr< const Kernel::GraphicsState >( ) )
		{
			graphicsState_ = other.graphicsState_;
		}
	else if( other.graphicsState_ != NullPtr< const Kernel::GraphicsState >( ) )
		{
			graphicsState_ = RefCountPtr< const Kernel::GraphicsState >( new Kernel::GraphicsState( *graphicsState_, *other.graphicsState_ ) );
		}
	/* In the remaining situation, there is nothing to merge, so we're done. */

	if( facetState_ == NullPtr< const Kernel::FacetState >( ) )
		{
			facetState_ = other.facetState_;
		}
	else if( other.facetState_ != NullPtr< const Kernel::FacetState >( ) )
		{
			facetState_ = RefCountPtr< const Kernel::FacetState >( new Kernel::FacetState( *facetState_, *other.facetState_ ) );
		}
	/* In the remaining situation, there is nothing to merge, so we're done. */

	if( textState_ == NullPtr< const Kernel::TextState >( ) )
		{
			textState_ = other.textState_;
		}
	else if( other.textState_ != NullPtr< const Kernel::TextState >( ) )
		{
			textState_ = RefCountPtr< const Kernel::TextState >( new Kernel::TextState( *textState_, *other.textState_ ) );
		}
	/* In the remaining situation, there is nothing to merge, so we're done. */

	if( IS_NAN( eyez_ ) )
		{
			eyez_ = other.eyez_;
		}

	if( IS_NAN( TeX_bleed_ ) )
		{
			TeX_bleed_ = other.TeX_bleed_;
		}

	if( defaultUnit_ == NullPtr< const Kernel::PolarHandlePromise >( ) )
		{
			defaultUnit_ = other.defaultUnit_;
		}

	if( blendSpace_ == NullPtr< const Lang::ColorSpace >( ) )
		{
			blendSpace_ = other.blendSpace_;
		}
}

void
Kernel::SystemDynamicVariables::print( std::ostream & os, const std::string & indentation ) const
{
	const char * moreIndentation = "  ";

	if( graphicsState_ != NullPtr< const Kernel::GraphicsState >( ) )
		{
			os << indentation << "Graphics state bindings:" << std::endl ;
			graphicsState_->print( os, indentation + moreIndentation );
		}

	if( textState_ != NullPtr< const Kernel::TextState >( ) )
		{
			os << indentation << "Text state bindings:" << std::endl ;
			textState_->print( os, indentation + moreIndentation );
		}

	if( facetState_ != NullPtr< const Kernel::FacetState >( ) )
		{
			os << indentation << "Facet state bindings:" << std::endl ;
			facetState_->print( os, indentation + moreIndentation );
		}

	if( ! IS_NAN( eyez_ ) )
		{
			os << indentation ;
			Lang::DYNAMIC_VARIABLE_ID_EYEZ.show( os, Ast::Identifier::DYNAMIC_VARIABLE );
			os << ": " << eyez_ / Interaction::displayUnit << Interaction::displayUnitName << std::endl ;
		}

	if( ! IS_NAN( TeX_bleed_ ) )
		{
			os << indentation ;
			Lang::DYNAMIC_VARIABLE_ID_TEX_BLEED.show( os, Ast::Identifier::DYNAMIC_VARIABLE );
			os << ": " << TeX_bleed_ / Interaction::displayUnit << Interaction::displayUnitName << std::endl ;
		}

	if( defaultUnit_ != NullPtr< const Kernel::PolarHandlePromise >( ) )
		{
			os << indentation ;
			Lang::DYNAMIC_VARIABLE_ID_DEFAULT_UNIT.show( os, Ast::Identifier::DYNAMIC_VARIABLE );
			os << ": " ;
			defaultUnit_->show( os );
			os << std::endl ;
		}

	if( blendSpace_ != NullPtr< const Lang::ColorSpace >( ) )
		{
			os << indentation ;
			Lang::DYNAMIC_VARIABLE_ID_BLEND_SPACE.show( os, Ast::Identifier::DYNAMIC_VARIABLE );
			os << ": " ;
			if( blendSpace_->isInherent( ) )
				{
					os << "< inherent >" << std::endl ;
				}
			else
				{
					os << blendSpace_->name( ) << std::endl ;
				}
		}

}


Kernel::DynamicEnvironment::DynamicEnvironment( const RefCountPtr< const Kernel::GraphicsState > & graphicsState )
	: parent_( NullPtr< Kernel::DynamicEnvironment >( ) ), sysBindings_( new Kernel::SystemDynamicVariables( graphicsState ) ),
		specialBindings_( 0 ), spanLast_( 0 ),
		contId_( Lang::CONTINUATION_ID_ERROR ), contVal_( Kernel::ContRef( new Kernel::DefaultErrorContinuation( Ast::theProgram->loc( ) ) ) )
{ }

Kernel::DynamicEnvironment::DynamicEnvironment( RefCountPtr< Kernel::DynamicEnvironment > parent, const Lang::DynamicBindings & bindings )
	: parent_( NullPtr< Kernel::DynamicEnvironment >( ) ), sysBindings_( 0 ), specialBindings_( 0 ), spanLast_( 0 ),
		contId_( 0 ), contVal_( NullPtr< Kernel::Continuation >( ) )
{
	bindings.bind( bindings_, & sysBindings_ );
	if( sysBindings_ != 0 &&
			sysBindings_->graphicsState_ != NullPtr< const Kernel::GraphicsState >( ) )
		{
			sysBindings_->graphicsState_ =
				RefCountPtr< const Kernel::GraphicsState >( new Kernel::GraphicsState( *(sysBindings_->graphicsState_),
																																							 *(parent->getGraphicsState( )) ) );
		}
	if( sysBindings_ != 0 &&
			sysBindings_->facetState_ != NullPtr< const Kernel::FacetState >( ) )
		{
			sysBindings_->facetState_ =
				RefCountPtr< const Kernel::FacetState >( new Kernel::FacetState( *(sysBindings_->facetState_),
																																				 *(parent->getFacetState( )) ) );
		}
	if( sysBindings_ != 0 &&
			sysBindings_->textState_ != NullPtr< const Kernel::TextState >( ) )
		{
			sysBindings_->textState_ =
				RefCountPtr< const Kernel::TextState >( new Kernel::TextState( *(sysBindings_->textState_),
																																			 *(parent->getTextState( )) ) );
		}
	parent_ = parent->selectParent( parent, bindings_ );
}

Kernel::DynamicEnvironment::DynamicEnvironment( RefCountPtr< Kernel::DynamicEnvironment > parent, const RefCountPtr< const Kernel::GraphicsState > & graphicsState )
	: parent_( parent ), sysBindings_( new Kernel::SystemDynamicVariables( ) ), specialBindings_( 0 ), spanLast_( 0 ),
		contId_( 0 ), contVal_( NullPtr< Kernel::Continuation >( ) )
{
	sysBindings_->graphicsState_ = graphicsState;
}

Kernel::DynamicEnvironment::DynamicEnvironment( RefCountPtr< Kernel::DynamicEnvironment > parent, Kernel::SystemDynamicVariables * sysBindings )
	: parent_( parent ), sysBindings_( sysBindings ), specialBindings_( 0 ), spanLast_( 0 ),
		contId_( 0 ), contVal_( NullPtr< Kernel::Continuation >( ) )
{ }

Kernel::DynamicEnvironment::DynamicEnvironment( RefCountPtr< Kernel::DynamicEnvironment > parent, Kernel::SpecialUnitVariables * specialBindings )
	: parent_( parent ), sysBindings_( 0 ), specialBindings_( specialBindings ), spanLast_( 0 ),
		contId_( 0 ), contVal_( NullPtr< Kernel::Continuation >( ) )
{ }

Kernel::DynamicEnvironment::DynamicEnvironment( RefCountPtr< Kernel::DynamicEnvironment > parent, const RefCountPtr< const Lang::Value > & spanLast )
	: parent_( parent ), sysBindings_( 0 ), specialBindings_( 0 ), spanLast_( new Kernel::SpanLastValue( spanLast ) ),
		contId_( 0 ), contVal_( NullPtr< Kernel::Continuation >( ) )
{ }


Kernel::DynamicEnvironment::DynamicEnvironment( RefCountPtr< Kernel::DynamicEnvironment > parent, const char * contId, const Kernel::ContRef & contVal )
	: parent_( parent ), sysBindings_( 0 ), specialBindings_( 0 ), spanLast_( 0 ),
		contId_( contId ), contVal_( contVal )
{ }

Kernel::DynamicEnvironment::~DynamicEnvironment( )
{
	if( sysBindings_ != 0 )
		{
			delete sysBindings_;
		}
	if( specialBindings_ != 0 )
		{
			delete specialBindings_;
		}
	if( spanLast_ != 0 )
		{
			delete spanLast_;
		}
}

void
Kernel::DynamicEnvironment::tackOn( const KeyType & key, Kernel::EvalState * evalState, const RefCountPtr< const Lang::Value > & piece, const Ast::SourceLocation & callLoc )
{
	throw Exceptions::NotImplemented( "DynamicEnvironment::tackOn" );
//	 MapType::iterator i = bindings_.find( key );
//	 if( i == bindings_.end( ) )
//		 {
//			 if( isBaseEnvironment( ) )
//				 {
//					 throw Exceptions::InternalError( "Key of dynamic variable was not found in dynamic environment." );
//				 }
//			 return parent_->tackOn( key, evalState, piece, callLoc );
//		 }
//	 return i->second.first->tackOn( evalState, piece, callLoc );
}

void
Kernel::DynamicEnvironment::lookup( const KeyType & key, Kernel::EvalState * evalState ) const
{
	MapType::const_iterator i = bindings_.find( key );
	if( i == bindings_.end( ) )
		{
			if( isBaseEnvironment( ) )
				{
					throw NonLocalExit::DynamicBindingNotFound( );
				}
			return parent_->lookup( key, evalState );
		}

	Kernel::ContRef cont = evalState->cont_;
	cont->takeHandle( i->second.first,
										evalState );
}

Kernel::VariableHandle
Kernel::DynamicEnvironment::getVarHandle( const KeyType & key ) const
{
	MapType::const_iterator i = bindings_.find( key );
	if( i == bindings_.end( ) )
		{
			if( isBaseEnvironment( ) )
				{
					throw NonLocalExit::DynamicBindingNotFound( );
				}
			return parent_->getVarHandle( key );
		}
	return i->second.first;
}

RefCountPtr< Kernel::DynamicEnvironment >
Kernel::DynamicEnvironment::selectParent( RefCountPtr< Kernel::DynamicEnvironment > & self, const MapType & newBindings )
{
	if( parent_ == NullPtr< Kernel::DynamicEnvironment >( ) ||
			sysBindings_ != 0 ||
			specialBindings_ != 0 ||
			spanLast_ != 0 ||
			contId_ != 0 )
		{
			return self;
		}

	MapType::const_iterator hint = newBindings.begin( );
	for( MapType::const_iterator i = bindings_.begin( ); i != bindings_.end( ); ++i )
		{
			hint = newBindings.find( i->first );
			if( hint == newBindings.end( ) )
				{
					return self;
				}
		}

	return parent_->selectParent( parent_, newBindings );
}

void
Kernel::DynamicEnvironment::gcMark( Kernel::GCMarkedSet & marked )
{
	for( MapType::iterator i = bindings_.begin( ); i != bindings_.end( ); ++i )
		{
			i->second.first->gcMark( marked );
		}
}


RefCountPtr< const Kernel::GraphicsState >
Kernel::DynamicEnvironment::getGraphicsState( ) const
{
	if( sysBindings_ == 0 ||
			sysBindings_->graphicsState_ == NullPtr< const Kernel::GraphicsState >( ) )
		{
			if( parent_ == NullPtr< Kernel::DynamicEnvironment >( ) )
				{
					throw Exceptions::InternalError( "The graphics state was needed but not defined." );
				}
			return parent_->getGraphicsState( );
		}

	return sysBindings_->graphicsState_;
}

RefCountPtr< const Kernel::FacetState >
Kernel::DynamicEnvironment::getFacetState( ) const
{
	if( sysBindings_ == 0 ||
			sysBindings_->facetState_ == NullPtr< const Kernel::FacetState >( ) )
		{
			if( parent_ == NullPtr< Kernel::DynamicEnvironment >( ) )
				{
					throw Exceptions::InternalError( "The facet state was needed but not defined." );
				}
			return parent_->getFacetState( );
		}

	return sysBindings_->facetState_;
}

RefCountPtr< const Kernel::TextState >
Kernel::DynamicEnvironment::getTextState( ) const
{
	if( sysBindings_ == 0 ||
			sysBindings_->textState_ == NullPtr< const Kernel::TextState >( ) )
		{
			if( parent_ == NullPtr< Kernel::DynamicEnvironment >( ) )
				{
					throw Exceptions::InternalError( "The text state was needed but not defined." );
				}
			return parent_->getTextState( );
		}

	return sysBindings_->textState_;
}

Concrete::Length
Kernel::DynamicEnvironment::getEyeZ( ) const
{
	if( sysBindings_ == 0 ||
			IS_NAN( sysBindings_->eyez_ ) )
		{
			if( parent_ == NullPtr< Kernel::DynamicEnvironment >( ) )
				{
					throw Exceptions::InternalError( "@eyez should allways be bound." );
				}
			return parent_->getEyeZ( );
		}

	return sysBindings_->eyez_;
}

Concrete::Length
Kernel::DynamicEnvironment::getTeXBleed( ) const
{
	if( sysBindings_ == 0 ||
			IS_NAN( sysBindings_->TeX_bleed_ ) )
		{
			if( parent_ == NullPtr< Kernel::DynamicEnvironment >( ) )
				{
					throw Exceptions::InternalError( "@TeX_bleed should allways be bound." );
				}
			return parent_->getTeXBleed( );
		}

	return sysBindings_->TeX_bleed_;
}

RefCountPtr< const Kernel::PolarHandlePromise >
Kernel::DynamicEnvironment::getDefaultUnit( ) const
{
	if( sysBindings_ == 0 ||
			sysBindings_->defaultUnit_ == NullPtr< const Kernel::PolarHandlePromise >( ) )
		{
			if( parent_ == NullPtr< Kernel::DynamicEnvironment >( ) )
				{
					throw Exceptions::InternalError( "The default unit should allways be defined." );
				}
			return parent_->getDefaultUnit( );
		}

	return sysBindings_->defaultUnit_;
}

RefCountPtr< const Lang::Value >
Kernel::DynamicEnvironment::getSpanLast( ) const
{
	if( spanLast_ == 0 )
		{
			if( parent_ == NullPtr< Kernel::DynamicEnvironment >( ) )
				{
					throw Exceptions::InternalError( "The value of %last is not determined by the current dynamic context." );
				}
			return parent_->getSpanLast( );
		}

	return spanLast_->last_;
}

Kernel::ContRef
Kernel::DynamicEnvironment::getEscapeContinuation( const char * id, const Ast::SourceLocation & loc ) const
{
	if( contId_ == 0 ||
			strcmp( contId_, id ) != 0 )
		{
			if( parent_ == NullPtr< Kernel::DynamicEnvironment >( ) )
				{
					throw Exceptions::UndefinedEscapeContinuation( id, loc );
				}
			return parent_->getEscapeContinuation( id, loc );
		}

	return contVal_;
}

RefCountPtr< const Lang::ColorSpace >
Kernel::DynamicEnvironment::getBlendSpace( ) const
{
	if( sysBindings_ == 0 ||
			sysBindings_->blendSpace_ == NullPtr< const Lang::ColorSpace >( ) )
		{
			if( parent_ == NullPtr< Kernel::DynamicEnvironment >( ) )
				{
					throw Exceptions::InternalError( "The blend space should allways be defined." );
				}
			return parent_->getBlendSpace( );
		}

	return sysBindings_->blendSpace_;
}

void
Kernel::DynamicEnvironment::specialUnitService( Concrete::Length * d, double * a0, double * a1 )
{
	if( specialBindings_ != 0 )
		{
			specialBindings_->specialUnitService( d, a0, a1 );
		}
	else
		{
			if( parent_ == NullPtr< Kernel::DynamicEnvironment >( ) )
				{
					throw Exceptions::InternalError( "The special unit dynamic context was needed but not defined." );
				}
			parent_->specialUnitService( d, a0, a1 );
		}
}

bool
Kernel::DynamicEnvironment::isBaseEnvironment( ) const
{
	return parent_ == NullPtr< Kernel::DynamicEnvironment >( );
}

void
Kernel::DynamicEnvironment::print( std::ostream & os ) const
{
	std::set< MapType::key_type > shadowed;
	recursivePrint( os, & shadowed );
}

size_t
Kernel::DynamicEnvironment::recursivePrint( std::ostream & os, std::set< MapType::key_type > * shadowed ) const
{
	std::set< MapType::key_type > shadowedBefore( *shadowed );

	size_t depth = 0;
	if( ! isBaseEnvironment( ) )
		{
			for( MapType::const_iterator i = bindings_.begin( ); i != bindings_.end( ); ++i )
				{
					shadowed->insert( shadowed->begin( ), i->first );
				}
			depth = parent_->recursivePrint( os, shadowed ) + 1;
		}

	std::string indentation = std::string( depth, ' ' );

	os << indentation << "@@ -- -- -- -- -- -- -- -- @@" << std::endl ;

	if( contId_ != 0 )
		{
			os << indentation << " (esc) " << "< escape continuation > " << contId_ << ": " << contVal_->traceLoc( ) << " (" << contVal_->description( ) << ")" << std::endl ;
		}

	if( sysBindings_ != 0 )
		{
			sysBindings_->print( os, indentation + " (sys) " ); /* The extra indentation matches " (num) "  */
		}

	if( specialBindings_ != 0 )
		{
			os << indentation << " (-- ) " << "< special unit points >: " << *(specialBindings_->p0_->mid_) << ( (specialBindings_->reverseDirection_) ? " <-- " : " --> " ) << *(specialBindings_->p1_->mid_) << std::endl ;
		}

	if( spanLast_ != 0 )
		{
			os << indentation << " (spn) " << "< %last >: " ;
			spanLast_->last_->show( os );
			os << std::endl ;
		}

	for( MapType::const_iterator i = bindings_.begin( ); i != bindings_.end( ); ++i )
		{
			os << indentation ;
			if( shadowedBefore.find( i->first ) != shadowedBefore.end( ) )
				{
					os << "#" ;
				}
			else
				{
					os << " " ;
				}
			os << "(" << std::setw(3) << i->first << ") " << Interaction::DYNAMIC_VARIABLE_PREFIX << i->second.second->id( ) << ": " ;
			if( i->second.first == NullPtr< Kernel::Variable >( ) )
				{
					os << "< Uninitialized >" ;
				}
			else if( i->second.first->isThunk( ) )
				{
					os << "< thunk >" ;
				}
			else if( dynamic_cast< const Lang::Instance * >( i->second.first->getUntyped( ).getPtr( ) ) == 0 )
				{
					i->second.first->getUntyped( )->show( os );
				}
			else
				{
					os << "..." ;
				}
			os << " (" << i->second.second->idLoc( ) << ")" << std::endl ;
		}

	//	os << indentation << "-- -- -- -- -- -- -- -- -- --" << std::endl ;

	return depth;
}

Lang::EyeZBinding::EyeZBinding( const Ast::PlacedIdentifier * id, const Ast::DynamicBindingExpression * bindingExpr, Concrete::Length val )
	: bindingExpr_( bindingExpr ), val_( val ), id_( id )
{ }

Lang::EyeZBinding::~EyeZBinding( )
{ }

void
Lang::EyeZBinding::bind( MapType & bindings, Kernel::SystemDynamicVariables ** sysBindings ) const
{
	if( *sysBindings == 0 )
		{
			*sysBindings = new Kernel::SystemDynamicVariables( );
			(*sysBindings)->eyez_ = val_;
			return;
		}

	if( ! IS_NAN( (*sysBindings)->eyez_ ) )
		{
			throw Exceptions::MultipleDynamicBind( id_, bindingExpr_->idLoc( ), Ast::THE_UNKNOWN_LOCATION );
		}

	(*sysBindings)->eyez_ = val_;
}

void
Lang::EyeZBinding::show( std::ostream & os ) const
{
	os << Interaction::DYNAMIC_VARIABLE_PREFIX << id_ << ":"
		 << val_ / Interaction::displayUnit << Interaction::displayUnitName ;
}

void
Lang::EyeZBinding::gcMark( Kernel::GCMarkedSet & marked )
{ }



Kernel::EyeZDynamicVariableProperties::EyeZDynamicVariableProperties( const Ast::PlacedIdentifier * id )
	: Kernel::DynamicVariableProperties( id )
{ }

Kernel::EyeZDynamicVariableProperties::~EyeZDynamicVariableProperties( )
{ }

Kernel::VariableHandle
Kernel::EyeZDynamicVariableProperties::fetch( const Kernel::PassedDyn & dyn ) const
{
	return Helpers::newValHandle( new Lang::Length( dyn->getEyeZ( ) ) );
}

void
Kernel::EyeZDynamicVariableProperties::makeBinding( Kernel::VariableHandle val, const Ast::DynamicBindingExpression * bindingExpr, Kernel::EvalState * evalState ) const
{
	try
		{
			RefCountPtr< const Lang::Length > len = val->tryVal< const Lang::Length >( );
			Kernel::ContRef cont = evalState->cont_;
			cont->takeValue( Kernel::ValueRef( new Lang::EyeZBinding( id_, bindingExpr, len->get( ) ) ),
											 evalState );
			return;
		}
	catch( const NonLocalExit::NotThisType & ball )
		{
			/* never mind */
		}

	try
		{
			RefCountPtr< const Lang::Float > maybeInfinity = val->tryVal< const Lang::Float >( );
			if( maybeInfinity->val_ < HUGE_VAL )
				{
					throw Exceptions::OutOfRange( bindingExpr->exprLoc( ), strrefdup( "The only float value allowed here is infinity." ) );
				}
			Kernel::ContRef cont = evalState->cont_;
			cont->takeValue( Kernel::ValueRef( new Lang::EyeZBinding( id_, bindingExpr, Concrete::HUGE_LENGTH ) ),
											 evalState );
			return;
		}
	catch( const NonLocalExit::NotThisType & ball )
		{
			/* never mind */
		}

	throw Exceptions::TypeMismatch( bindingExpr->exprLoc( ), val->getUntyped( )->getTypeName( ), Helpers::typeSetString( Lang::Length::staticTypeName( ), Lang::Float::staticTypeName( ) ) );
}


Lang::DefaultUnitBinding::DefaultUnitBinding( const Ast::PlacedIdentifier * id, const Ast::DynamicBindingExpression * bindingExpr, const RefCountPtr< const Kernel::PolarHandlePromise > & val )
	: bindingExpr_( bindingExpr ), val_( val ), id_( id )
{ }

Lang::DefaultUnitBinding::~DefaultUnitBinding( )
{ }

void
Lang::DefaultUnitBinding::bind( MapType & bindings, Kernel::SystemDynamicVariables ** sysBindings ) const
{
	if( *sysBindings == 0 )
		{
			*sysBindings = new Kernel::SystemDynamicVariables( );
			(*sysBindings)->defaultUnit_ = val_;
			return;
		}

	if( (*sysBindings)->defaultUnit_ != NullPtr< const Kernel::PolarHandlePromise >( ) )
		{
			throw Exceptions::MultipleDynamicBind( id_, bindingExpr_->idLoc( ), Ast::THE_UNKNOWN_LOCATION );
		}

	(*sysBindings)->defaultUnit_ = val_;
}

void
Lang::DefaultUnitBinding::show( std::ostream & os ) const
{
	os << Interaction::DYNAMIC_VARIABLE_PREFIX << id_ << ":<promise>" ;
}

void
Lang::DefaultUnitBinding::gcMark( Kernel::GCMarkedSet & marked )
{
	val_->gcMark( marked );
}


Kernel::DefaultUnitDynamicVariableProperties::DefaultUnitDynamicVariableProperties( const Ast::PlacedIdentifier * id )
	: Kernel::DynamicVariableProperties( id )
{ }

Kernel::DefaultUnitDynamicVariableProperties::~DefaultUnitDynamicVariableProperties( )
{ }

Kernel::VariableHandle
Kernel::DefaultUnitDynamicVariableProperties::fetch( const Kernel::PassedDyn & dyn ) const
{
	throw Exceptions::MiscellaneousRequirement( "The default unit cannot be evaluated as a variable." );
}

void
Kernel::DefaultUnitDynamicVariableProperties::makeBinding( Kernel::VariableHandle val, const Ast::DynamicBindingExpression * bindingExpr, Kernel::EvalState * evalState ) const
{
	if( ! val->isThunk( ) )
		{
			throw Exceptions::InternalError( "The default unit handle was not a thunk." );
		}

	Kernel::ContRef cont = evalState->cont_;
	cont->takeValue( Kernel::ValueRef( new Lang::DefaultUnitBinding( id_, bindingExpr, RefCountPtr< const Kernel::PolarHandlePromise >( new Kernel::PolarHandleTruePromise( val->copyThunk( ) ) ) ) ),
									 evalState );
	return;
}


Lang::TeXBleedBinding::TeXBleedBinding( const Ast::PlacedIdentifier * id, const Ast::DynamicBindingExpression * bindingExpr, Concrete::Length val )
	: bindingExpr_( bindingExpr ), val_( val ), id_( id )
{ }

Lang::TeXBleedBinding::~TeXBleedBinding( )
{ }

void
Lang::TeXBleedBinding::bind( MapType & bindings, Kernel::SystemDynamicVariables ** sysBindings ) const
{
	if( *sysBindings == 0 )
		{
			*sysBindings = new Kernel::SystemDynamicVariables( );
			(*sysBindings)->TeX_bleed_ = val_;
			return;
		}

	if( ! IS_NAN( (*sysBindings)->TeX_bleed_ ) )
		{
			throw Exceptions::MultipleDynamicBind( id_, bindingExpr_->idLoc( ), Ast::THE_UNKNOWN_LOCATION );
		}

	(*sysBindings)->TeX_bleed_ = val_;
}

void
Lang::TeXBleedBinding::show( std::ostream & os ) const
{
	os << Interaction::DYNAMIC_VARIABLE_PREFIX << id_ << ":"
		 << val_ / Interaction::displayUnit << Interaction::displayUnitName ;
}

void
Lang::TeXBleedBinding::gcMark( Kernel::GCMarkedSet & marked )
{ }



Kernel::TeXBleedDynamicVariableProperties::TeXBleedDynamicVariableProperties( const Ast::PlacedIdentifier * id )
	: Kernel::DynamicVariableProperties( id )
{ }

Kernel::TeXBleedDynamicVariableProperties::~TeXBleedDynamicVariableProperties( )
{ }

Kernel::VariableHandle
Kernel::TeXBleedDynamicVariableProperties::fetch( const Kernel::PassedDyn & dyn ) const
{
	return Helpers::newValHandle( new Lang::Length( dyn->getTeXBleed( ) ) );
}

void
Kernel::TeXBleedDynamicVariableProperties::makeBinding( Kernel::VariableHandle val, const Ast::DynamicBindingExpression * bindingExpr, Kernel::EvalState * evalState ) const
{
	RefCountPtr< const Lang::Length > len = val->getVal< const Lang::Length >( bindingExpr->exprLoc( ) );
	Kernel::ContRef cont = evalState->cont_;
	cont->takeValue( Kernel::ValueRef( new Lang::TeXBleedBinding( id_, bindingExpr, len->get( ) ) ),
									 evalState );
}


Lang::BlendSpaceBinding::BlendSpaceBinding( const Ast::PlacedIdentifier * id, const Ast::DynamicBindingExpression * bindingExpr, const RefCountPtr< const Lang::ColorSpace > & space )
	: bindingExpr_( bindingExpr ), space_( space ), id_( id )
{ }

Lang::BlendSpaceBinding::~BlendSpaceBinding( )
{ }

void
Lang::BlendSpaceBinding::bind( MapType & bindings, Kernel::SystemDynamicVariables ** sysBindings ) const
{
	if( *sysBindings == 0 )
		{
			*sysBindings = new Kernel::SystemDynamicVariables( );
			(*sysBindings)->blendSpace_ = space_;
			return;
		}

	if( (*sysBindings)->blendSpace_ != NullPtr< const Lang::ColorSpace >( ) )
		{
			throw Exceptions::MultipleDynamicBind( id_, bindingExpr_->idLoc( ), Ast::THE_UNKNOWN_LOCATION );
		}

	(*sysBindings)->blendSpace_ = space_;
}

void
Lang::BlendSpaceBinding::show( std::ostream & os ) const
{
	os << Interaction::DYNAMIC_VARIABLE_PREFIX << id_ << "" ;
	space_->show( os );
}

void
Lang::BlendSpaceBinding::gcMark( Kernel::GCMarkedSet & marked )
{ }



Kernel::BlendSpaceDynamicVariableProperties::BlendSpaceDynamicVariableProperties( const Ast::PlacedIdentifier * id )
	: Kernel::DynamicVariableProperties( id )
{ }

Kernel::BlendSpaceDynamicVariableProperties::~BlendSpaceDynamicVariableProperties( )
{ }

Kernel::VariableHandle
Kernel::BlendSpaceDynamicVariableProperties::fetch( const Kernel::PassedDyn & dyn ) const
{
	return Kernel::VariableHandle( new Kernel::Variable( dyn->getBlendSpace( ) ) );
}

void
Kernel::BlendSpaceDynamicVariableProperties::makeBinding( Kernel::VariableHandle val, const Ast::DynamicBindingExpression * bindingExpr, Kernel::EvalState * evalState ) const
{
	RefCountPtr< const Lang::ColorSpace > space = val->getVal< const Lang::ColorSpace >( bindingExpr->exprLoc( ) );
	if( ! space->isBlendable( ) )
		{
			throw Exceptions::OutOfRange( bindingExpr->exprLoc( ), strrefdup( "This color space cannot be used in blending." ) );
		}

	Kernel::ContRef cont = evalState->cont_;
	cont->takeValue( Kernel::ValueRef( new Lang::BlendSpaceBinding( id_, bindingExpr, space ) ),
									 evalState );
}


Kernel::SpotDynamicVariableProperties::SpotDynamicVariableProperties( const Ast::PlacedIdentifier * id )
	: Kernel::DynamicVariableProperties( id )
{ }

Kernel::SpotDynamicVariableProperties::~SpotDynamicVariableProperties( )
{ }

Kernel::VariableHandle
Kernel::SpotDynamicVariableProperties::fetch( const Kernel::PassedDyn & dyn ) const
{
	/* Note that this code should be very similar to Core_spot::call.
	 */

	static RefCountPtr< const Lang::ElementaryPath2D > pth = RefCountPtr< const Lang::ElementaryPath2D >( NullPtr< const Lang::ElementaryPath2D >( ) );
	if( pth == NullPtr< const Lang::ElementaryPath2D >( ) )
		{
			Lang::ElementaryPath2D * mem = new Lang::ElementaryPath2D;
			mem->push_back( new Concrete::PathPoint2D( Concrete::ZERO_LENGTH, Concrete::ZERO_LENGTH ) );
			if( Kernel::allowSingletonPaths )
				{
					mem->close( );
				}
			else
				{
					mem->push_back( new Concrete::PathPoint2D( Concrete::ZERO_LENGTH, Concrete::ZERO_LENGTH ) );
				}
			pth = RefCountPtr< const Lang::ElementaryPath2D >( mem );
		}

	Kernel::GraphicsState * capState( new Kernel::GraphicsState( *dyn->getGraphicsState( ) ) );
	capState->cap_ = Lang::CapStyle::CAP_ROUND;

	return Helpers::newValHandle( new Lang::PaintedPath2D
																( RefCountPtr< const Kernel::GraphicsState >( capState ),
																	pth,
																	"S" ) );
}

void
Kernel::SpotDynamicVariableProperties::makeBinding( Kernel::VariableHandle val, const Ast::DynamicBindingExpression * bindingExpr, Kernel::EvalState * evalState ) const
{
	std::ostringstream msg;
	msg << "The dynamic variable " ;
	id_->show( msg, Ast::Identifier::DYNAMIC_VARIABLE );
	msg << " cannot be changed from its default." ;
	throw Exceptions::MiscellaneousRequirement( strrefdup( msg ) );
}


Kernel::DynamicEnvironment::KeyType Kernel::DynamicEnvironment::nextKey( 0 );

Kernel::DynamicEnvironment::KeyType
Kernel::DynamicEnvironment::getFreshKey( )
{
	++nextKey;
	return nextKey;
}



Lang::DynamicExpression::DynamicExpression( Kernel::PassedEnv env, Ast::Expression * expr )
	: env_( env ), expr_( expr )
{ }

Lang::DynamicExpression::~DynamicExpression( )
{ }

RefCountPtr< const Lang::Class > Lang::DynamicExpression::TypeID( new Lang::SystemFinalClass( strrefdup( "DynamicExpression" ) ) );
TYPEINFOIMPL( DynamicExpression );

void
Lang::DynamicExpression::evalHelper( Kernel::EvalState * evalState ) const
{
	evalState->env_ = env_;
	evalState->expr_ = expr_;
}

void
Lang::DynamicExpression::gcMark( Kernel::GCMarkedSet & marked )
{
	env_->gcMark( marked );
}


void
Kernel::registerDynamic( Kernel::Environment * env )
{
	env->initDefineDynamic( new Kernel::WidthDynamicVariableProperties( & Lang::DYNAMIC_VARIABLE_ID_WIDTH ) );
	env->initDefineDynamic( new Kernel::CapStyleDynamicVariableProperties( Kernel::Environment::newPlacedIdentifier( Lang::THE_NAMESPACE_Shapes_Traits, "cap" ) ) );
	env->initDefineDynamic( new Kernel::JoinStyleDynamicVariableProperties( Kernel::Environment::newPlacedIdentifier( Lang::THE_NAMESPACE_Shapes_Traits, "join" ) ) );
	env->initDefineDynamic( new Kernel::MiterLimitDynamicVariableProperties( Kernel::Environment::newPlacedIdentifier( Lang::THE_NAMESPACE_Shapes_Traits, "miterlimit" ) ) );
	env->initDefineDynamic( new Kernel::StrokingDynamicVariableProperties( & Lang::DYNAMIC_VARIABLE_ID_STROKING ) );
	env->initDefineDynamic( new Kernel::NonStrokingDynamicVariableProperties( & Lang::DYNAMIC_VARIABLE_ID_NONSTROKING ) );
	env->initDefineDynamic( new Kernel::DashDynamicVariableProperties( Kernel::Environment::newPlacedIdentifier( Lang::THE_NAMESPACE_Shapes_Traits, "dash" ) ) );
	env->initDefineDynamic( new Kernel::BlendModeDynamicVariableProperties( Kernel::Environment::newPlacedIdentifier( Lang::THE_NAMESPACE_Shapes_Traits, "blend" ) ) );
	env->initDefineDynamic( new Kernel::AlphaDynamicVariableProperties( Kernel::Environment::newPlacedIdentifier( Lang::THE_NAMESPACE_Shapes_Traits, "nonstrokingalpha" ), false ) );
	env->initDefineDynamic( new Kernel::AlphaDynamicVariableProperties( Kernel::Environment::newPlacedIdentifier( Lang::THE_NAMESPACE_Shapes_Traits, "strokingalpha" ), true ) );

	env->initDefineDynamic( new Kernel::ReflectionsDynamicVariableProperties( Kernel::Environment::newPlacedIdentifier( Lang::THE_NAMESPACE_Shapes_Traits_Light, "reflections" ) ) );
	env->initDefineDynamic( new Kernel::AutoIntensityDynamicVariableProperties( & Lang::DYNAMIC_VARIABLE_ID_AUTOINTENSITY ) );
	env->initDefineDynamic( new Kernel::AutoScatteringDynamicVariableProperties( Kernel::Environment::newPlacedIdentifier( Lang::THE_NAMESPACE_Shapes_Traits_Light, "autoscattering" ) ) );
	env->initDefineDynamic( new Kernel::ViewResolutionDynamicVariableProperties( Kernel::Environment::newPlacedIdentifier( Lang::THE_NAMESPACE_Shapes_Traits_Light, "facetresolution" ) ) );
	env->initDefineDynamic( new Kernel::ShadeOrderDynamicVariableProperties( Kernel::Environment::newPlacedIdentifier( Lang::THE_NAMESPACE_Shapes_Traits_Light, "shadeorder" ) ) );

	env->initDefineDynamic( new Kernel::CharacterSpacingDynamicVariableProperties( Kernel::Environment::newPlacedIdentifier( Lang::THE_NAMESPACE_Shapes_Text, "characterspacing" ) ) );
	env->initDefineDynamic( new Kernel::WordSpacingDynamicVariableProperties( Kernel::Environment::newPlacedIdentifier( Lang::THE_NAMESPACE_Shapes_Text, "wordspacing" ) ) );
	env->initDefineDynamic( new Kernel::HorizontalScalingDynamicVariableProperties( Kernel::Environment::newPlacedIdentifier( Lang::THE_NAMESPACE_Shapes_Text, "horizontalscaling" ) ) );
	env->initDefineDynamic( new Kernel::LeadingDynamicVariableProperties( Kernel::Environment::newPlacedIdentifier( Lang::THE_NAMESPACE_Shapes_Text, "leading" ) ) );
	env->initDefineDynamic( new Kernel::FontDynamicVariableProperties( & Lang::DYNAMIC_VARIABLE_ID_TEXT_FONT ) );
	env->initDefineDynamic( new Kernel::TextSizeDynamicVariableProperties( & Lang::DYNAMIC_VARIABLE_ID_TEXT_SIZE ) );
	env->initDefineDynamic( new Kernel::TextRenderingModeDynamicVariableProperties( Kernel::Environment::newPlacedIdentifier( Lang::THE_NAMESPACE_Shapes_Text, "rendering" ) ) );
	env->initDefineDynamic( new Kernel::TextRiseDynamicVariableProperties( Kernel::Environment::newPlacedIdentifier( Lang::THE_NAMESPACE_Shapes_Text, "rise" ) ) );
	env->initDefineDynamic( new Kernel::TextKnockoutDynamicVariableProperties( Kernel::Environment::newPlacedIdentifier( Lang::THE_NAMESPACE_Shapes_Text, "knockout" ) ) );

	env->initDefineDynamic( new Kernel::TeXBleedDynamicVariableProperties( & Lang::DYNAMIC_VARIABLE_ID_TEX_BLEED ) );

	env->initDefineDynamic( new Kernel::EyeZDynamicVariableProperties( & Lang::DYNAMIC_VARIABLE_ID_EYEZ ) );
	env->initDefineDynamic( new Kernel::DefaultUnitDynamicVariableProperties( & Lang::DYNAMIC_VARIABLE_ID_DEFAULT_UNIT ) );
	env->initDefineDynamic( new Kernel::BlendSpaceDynamicVariableProperties( & Lang::DYNAMIC_VARIABLE_ID_BLEND_SPACE ) );

	env->initDefineDynamic( new Kernel::SpotDynamicVariableProperties( Kernel::Environment::newPlacedIdentifier( Lang::THE_NAMESPACE_Shapes_Graphics, "spot" ) ) );
}

