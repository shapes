/* This file is part of Shapes.
 *
 * Shapes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Shapes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2008, 2013, 2014 Henrik Tidefelt
 */

#include "Shapes_Helpers_decls.h"

#include "astfun.h"
#include "astfun_impl.h"
#include "shapesexceptions.h"
#include "shapescore.h"
#include "consts.h"
#include "globals.h"
#include "continuations.h"
#include "astvar.h"
#include "astvalues.h"
#include "texlabelmanager.h"

#include <sstream>

using namespace Shapes;
using namespace std;


Kernel::Formals::Formals( )
	: loc_( Ast::THE_UNKNOWN_LOCATION, bool( ) ),
		seenDefault_( false ),
		argumentIdentifiers_( new Ast::IdentifierTree( NULL, false ) ),
		argumentOrder_( new std::map< const char *, size_t, charPtrLess > ),
		sink_( NULL ),
		stateIdentifiers_( new Ast::IdentifierTree( NULL, false ) ),
		stateOrder_( new std::map< const char *, size_t, charPtrLess > )
{ }

Kernel::Formals::Formals( size_t numberOfDummyDefaultExprs )
	: loc_( Ast::THE_UNKNOWN_LOCATION, bool( ) ),
		seenDefault_( false ),
		argumentIdentifiers_( new Ast::IdentifierTree( NULL, false ) ),
		argumentOrder_( new std::map< const char *, size_t, charPtrLess > ),
		sink_( NULL ),
		stateIdentifiers_( new Ast::IdentifierTree( NULL, false ) ),
		stateOrder_( new std::map< const char *, size_t, charPtrLess > )
{
	// Pushing null expressions is just a way of specifying how many non-sink arguments there are.
	for( size_t i = 0; i < numberOfDummyDefaultExprs; ++i )
		{
			defaultExprs_.push_back( NULL );
		}
}

Kernel::Formals::~Formals( )
{
	delete argumentIdentifiers_;
	delete argumentOrder_;
	delete stateIdentifiers_;
	delete stateOrder_;
	if( sink_ != NULL )
		{
			delete sink_;
		}
}

void
Kernel::Formals::setLoc( const Ast::SourceLocation & loc )
{
	loc_ = loc;
}

bool
Kernel::Formals::appendArgumentFormal( const Ast::PlacedIdentifier & id )
{
	size_t pos = argumentIdentifiers_->size( );
	if( ! argumentIdentifiers_->insert( id, loc_ ) ) /* The source location given here is more or less a dummy, it should never be needed. */
		return false;
	argumentOrder_->insert( std::pair< const char *, size_t >( id.simpleId( ), pos ) );
	return true;
}

bool
Kernel::Formals::appendStateFormal( const Ast::PlacedIdentifier & id )
{
	size_t pos = stateIdentifiers_->size( );
	if( ! stateIdentifiers_->insert( id, loc_ ) ) /* The source location given here is more or less a dummy, it should never be needed. */
		return false;
	stateOrder_->insert( std::pair< const char *, size_t >( id.simpleId( ), pos ) );
	return true;
}

void
Kernel::Formals::push_exprs( Ast::ArgListExprs * args ) const
{
	typedef typeof defaultExprs_ ListType;
	for( ListType::const_iterator i = defaultExprs_.begin( ); i != defaultExprs_.end( ); ++i )
		{
			if( *i != 0 )
				{
					args->orderedExprs_->push_back( *i );
				}
		}
}

Kernel::EvaluatedFormals *
Kernel::Formals::newEvaluatedFormals( Kernel::Arguments & args ) const
{
	size_t pos = 0;
	return newEvaluatedFormals( args, & pos );
}

Kernel::EvaluatedFormals *
Kernel::Formals::newEvaluatedFormals( Kernel::Arguments & args, size_t * pos ) const
{
	Kernel::EvaluatedFormals * res = new Kernel::EvaluatedFormals( const_cast< Kernel::Formals * >( this ) );
	res->isSink_ = false; // The formals created here belong to user functions, which are exactly those which are not sinks.

	res->defaults_.reserve( defaultExprs_.size( ) );
	res->locations_.reserve( defaultExprs_.size( ) );

	typedef typeof defaultExprs_ ListType;
	for( ListType::const_iterator i = defaultExprs_.begin( ); i != defaultExprs_.end( ); ++i )
		{
			if( *i != 0 )
				{
					res->defaults_.push_back( args.getHandle( *pos ) );
					res->locations_.push_back( args.getNode( *pos ) );
					++(*pos);
				}
			else
				{
					res->defaults_.push_back( Kernel::THE_SLOT_VARIABLE );
					res->locations_.push_back( 0 );															 // I really hope this is never dereferenced!
				}
		}

	return res;
}

std::vector< bool > *
Kernel::Formals::newArgListForcePos( const Ast::ArgListExprs * argList ) const
{
	/* Here, we use the knowledge that ordered arguments are evaluated backwards, and named arguments
	 * in the natural order (the lexiographic order of std::map).
	 */

	std::vector< bool > * res = new std::vector< bool >;
	res->resize( argList->orderedExprs_->size( ) );
	res->reserve( argList->orderedExprs_->size( ) + argList->namedExprs_->size( ) );

		if( ! argList->orderedExprs_->empty( ) )
			{
				typedef typeof forcePos_ SrcType;
				SrcType::const_iterator src = forcePos_.begin( );
				for( size_t arg = argList->orderedExprs_->size( ) - 1; ; --arg, ++src )
					{
						(*res)[ arg ] = *src;
						if( arg == 0 )
							{
								break;
							}
					}
			}

	{
		typedef typeof *argList->namedExprs_ MapType;
		MapType::const_iterator end = argList->namedExprs_->end( );
		for( MapType::const_iterator arg = argList->namedExprs_->begin( ); arg != end; ++arg )
			{
				res->push_back( forcePos_[ (*argumentOrder_)[ arg->first ] ] );
			}
	}

	return res;
}

std::vector< bool > *
Kernel::Formals::newArgListForcePos( const Ast::ArgListExprs * argList, const Kernel::Arguments & curryArgs ) const
{
	/* Compare with the non-curry version!
	 */

	std::vector< bool > * res = new std::vector< bool >;
	res->resize( argList->orderedExprs_->size( ) );
	res->reserve( argList->orderedExprs_->size( ) + argList->namedExprs_->size( ) );

	if( ! argList->orderedExprs_->empty( ) )
		{
			typedef typeof forcePos_ SrcType;
			SrcType::const_iterator src = forcePos_.begin( );
			size_t curryPos = 0;
			while( curryPos < curryArgs.size( ) &&
						 ! curryArgs.isSlot( curryPos ) )
				{
					++src;
					++curryPos;
				}
			for( size_t arg = argList->orderedExprs_->size( ) - 1; ; --arg )
				{
					(*res)[ arg ] = *src;
					if( arg == 0 )
						{
							break;
						}
					++src;
					++curryPos;
					while( curryPos < curryArgs.size( ) &&
								 ! curryArgs.isSlot( curryPos ) )
						{
							++src;
							++curryPos;
						}
				}
		}

	{
		typedef typeof *argList->namedExprs_ MapType;
		MapType::const_iterator end = argList->namedExprs_->end( );
		for( MapType::const_iterator arg = argList->namedExprs_->begin( ); arg != end; ++arg )
			{
				res->push_back( forcePos_[ (*argumentOrder_)[ arg->first ] ] );
			}
	}

	return res;
}

const Ast::SourceLocation &
Kernel::Formals::loc( ) const
{
	return loc_;
}



Ast::ArgListExprs::ConstIterator::ConstIterator( std::list< Ast::Expression * >::const_reverse_iterator i1, std::map< const char *, Ast::Expression *, charPtrLess >::const_iterator i2, const size_t & index )
	: i1_( i1 ), i2_( i2 ), index_( index )
{ }

Ast::ArgListExprs::ConstIterator::ConstIterator( const Ast::ArgListExprs::ConstIterator & orig )
	: i1_( orig.i1_ ), i2_( orig.i2_ ), index_( orig.index_ )
{ }

Ast::ArgListExprs::ArgListExprs( bool exprOwner )
	: exprOwner_( exprOwner ), firstOrderedStateOwner_( exprOwner ), orderedExprs_( new std::list< Ast::Expression * > ), namedExprs_( new std::map< const char *, Ast::Expression *, charPtrLess > ),
		orderedStates_( new std::list< Ast::StateReference * > ), namedStates_( new std::map< const char *, Ast::StateReference *, charPtrLess > )
{ }

Ast::ArgListExprs::ArgListExprs( bool exprOwner, bool firstOrderedStateOwner )
	: exprOwner_( exprOwner ), firstOrderedStateOwner_( firstOrderedStateOwner ), orderedExprs_( new std::list< Ast::Expression * > ), namedExprs_( new std::map< const char *, Ast::Expression *, charPtrLess > ),
		orderedStates_( new std::list< Ast::StateReference * > ), namedStates_( new std::map< const char *, Ast::StateReference *, charPtrLess > )
{ }


Ast::ArgListExprs::ArgListExprs( std::list< Ast::Expression * > * orderedExprs, std::map< const char *, Ast::Expression *, charPtrLess > * namedExprs, std::list< Ast::StateReference * > * orderedStates, std::map< const char *, Ast::StateReference *, charPtrLess > * namedStates )
	: exprOwner_( true ), firstOrderedStateOwner_( true ), orderedExprs_( orderedExprs ), namedExprs_( namedExprs ), orderedStates_( orderedStates ), namedStates_( namedStates )
{ }

Ast::ArgListExprs::~ArgListExprs( )
{
	{
		if( exprOwner_ )
			{
				typedef list< Ast::Expression * >::iterator I;
				for( I i = orderedExprs_->begin( ); i != orderedExprs_->end( ); ++i )
					{
						delete *i;
					}
			}
		delete orderedExprs_;
	}

	{
		if( exprOwner_ )
			{
				typedef std::map< const char *, Ast::Expression *, charPtrLess >::const_iterator I;
				for( I i = namedExprs_->begin( ); i != namedExprs_->end( ); ++i )
					{
						delete i->first;
						delete i->second;
					}
			}
		delete namedExprs_;
	}

	{
		if( exprOwner_ )
			{
				typedef std::list< Ast::StateReference * >::iterator I;
				I i = orderedStates_->begin( );
				if( i != orderedStates_->end( ) ){
					if( firstOrderedStateOwner_ ){
						delete *i;
					}
					for( ++i; i != orderedStates_->end( ); ++i )
						{
							delete *i;
						}
				}
			}
		delete orderedStates_;
	}

	{
		if( exprOwner_ )
			{
				typedef std::map< const char *, Ast::StateReference *, charPtrLess >::const_iterator I;
				for( I i = namedStates_->begin( ); i != namedStates_->end( ); ++i )
					{
						delete i->first;
						delete i->second;
					}
			}
		delete namedStates_;
	}
}

Ast::ArgListExprs::ArgListExprs( size_t numberOfOrderedDummyExprs )
	: exprOwner_( true ), firstOrderedStateOwner_( true ), orderedExprs_( new std::list< Ast::Expression * > ), namedExprs_( new std::map< const char *, Ast::Expression *, charPtrLess > ), orderedStates_( new typeof *orderedStates_ ), namedStates_( new typeof *namedStates_ )
{
	for( size_t i = 0; i < numberOfOrderedDummyExprs; ++i )
		{
			orderedExprs_->push_back( new Ast::DummyExpression );
		}
}

void
Ast::ArgListExprs::evaluate( const RefCountPtr< const Kernel::CallContInfo > & info, const Ast::ArgListExprs::ConstIterator & pos, const RefCountPtr< const Lang::SingleList > & vals, Kernel::EvalState * evalState ) const
{
	std::list< Ast::Expression * >::const_reverse_iterator i1end = orderedExprs_->rend( );

	if( pos.i1_ == i1end &&
			pos.i2_ == namedExprs_->end( ) )
		{
			evalState->cont_ = info->cont_;
			info->cont_->takeValue( vals, evalState );
			return;
		}

	if( pos.i1_ != i1end )
		{
			/* Note that it is necessary that the evaluation of expressions with free states is not delayed.
			 * If it would, it would be very strange for the calling function if a passed state would change
			 * value in response to the accessing of a formal parameter.
			 */
			bool force = info->force( pos.index_ );
			if( force || (*(pos.i1_))->immediate_ )
				{
					typedef typeof const_cast< Ast::ArgListExprs::ConstIterator & >( pos ).i1_ Iterator;
					Iterator next = pos.i1_;
					++next;
					evalState->expr_ = *(pos.i1_);
					evalState->env_ = info->env_;
					evalState->dyn_ = info->dyn_;
					evalState->cont_ = Kernel::ContRef( new Kernel::CallCont_n( evalState->expr_->loc( ),
																																			info,
																																			Ast::ArgListExprs::ConstIterator( next, pos.i2_, pos.index_ + 1 ),
																																			vals,
																																			force ) );
					return;
				}
			else
				{
					/* Delay evaluation of this argument by just putting a thunk in the list.
					 */
					typedef typeof const_cast< Ast::ArgListExprs::ConstIterator & >( pos ).i1_ Iterator;
					Iterator next = pos.i1_;
					++next;
					evaluate( info,
										Ast::ArgListExprs::ConstIterator( next, pos.i2_, pos.index_ + 1 ),
										RefCountPtr< const Lang::SingleListPair >( new Lang::SingleListPair( Kernel::VariableHandle( new Kernel::Variable( new Kernel::Thunk( info->env_, info->dyn_, *pos.i1_ ) ) ),
																																												 vals ) ),
										evalState );
					return; /* It is not really important that the above compiles to a tail call.  Hopefully, it does, but otherwise it is easy to see that the chain of recursive calls to this function
										* will always completes without evaluation of expressions. */
				}
		}
	else
		{
			bool force = info->force( pos.index_ );
			if( force || pos.i2_->second->immediate_ )
				{
					typedef typeof const_cast< Ast::ArgListExprs::ConstIterator & >( pos ).i2_ Iterator;
					Iterator next = pos.i2_;
					++next;
					evalState->expr_ = pos.i2_->second;
					evalState->env_ = info->env_;
					evalState->dyn_ = info->dyn_;
					evalState->cont_ = Kernel::ContRef( new Kernel::CallCont_n( evalState->expr_->loc( ),
																																			info,
																																			Ast::ArgListExprs::ConstIterator( pos.i1_, next, pos.index_ + 1 ),
																																			vals,
																																			force ) );
					return;
				}
			else
				{
					/* Delay evaluation of this argument by just putting a thunk in the list.
					 */
					typedef typeof const_cast< Ast::ArgListExprs::ConstIterator & >( pos ).i2_ Iterator;
					Iterator next = pos.i2_;
					++next;
					evaluate( info,
										Ast::ArgListExprs::ConstIterator( pos.i1_, next, pos.index_ + 1 ),
										RefCountPtr< const Lang::SingleListPair >( new Lang::SingleListPair( Kernel::VariableHandle( new Kernel::Variable( new Kernel::Thunk( info->env_, info->dyn_, pos.i2_->second ) ) ),
																																												 vals ) ),
										evalState );
					return; /* It is not really important that the above compiles to a tail call.  Hopefully, it does, but otherwise it is easy to see that the chain of recursive calls to this function
										* will always complete without evaluation of expressions. */
				}
		}
}

void
Ast::ArgListExprs::evaluate_Structure( const RefCountPtr< const Kernel::CallContInfo > & info, size_t pos, const RefCountPtr< const Lang::SingleList > & values, Kernel::EvalState * evalState ) const
{
	const Lang::SingleListPair * valuesPtr = dynamic_cast< const Lang::SingleListPair * >( values.getPtr( ) );
	if( valuesPtr == NULL ){
		/* Finished, let the waiting CallCont_Structure_last do the rest. */
		evalState->cont_ = info->cont_;
		info->cont_->takeValue( Lang::THE_VOID, /* The continuation already has all values; Lang::THE_VOID is just a dummy. */
														evalState );
		return;
	}

	Kernel::VariableHandle first = valuesPtr->car_;

	if( info->force( pos ) && first->isThunk( ) )
		{
			evalState->env_ = info->env_;
			evalState->dyn_ = info->dyn_;
			evalState->cont_ = Kernel::ContRef( new Kernel::CallCont_Structure_n
				( evalState->expr_->loc( ),
					info,
					pos - 1,
					valuesPtr->cdr_ ) );
			first->force( first, evalState );
			return;
		}
	else
		{
			evaluate_Structure( info, pos - 1, valuesPtr->cdr_, evalState );
			return;
		}
}

void
Ast::ArgListExprs::bind( Kernel::Arguments * dst, RefCountPtr< const Lang::SingleList > vals, Kernel::PassedEnv env, Kernel::PassedDyn dyn ) const
{
	typedef const Lang::SingleListPair ConsType;

	/* Note that the arguments are bound in backwards-order, since that is how the values are accessed.
	 */

	{
		typedef std::map< const char *, Ast::Expression *, charPtrLess >::const_reverse_iterator I;
		I i = namedExprs_->rbegin( );
		I end = namedExprs_->rend( );
		for( ; i != end; ++i )
			{
				RefCountPtr< ConsType > lst = vals.down_cast< ConsType >( );
				if( lst == NullPtr< ConsType >( ) )
					{
						throw Exceptions::InternalError( strrefdup( "Out of argument values when binding application." ) );
					}
				dst->addNamedArgument( i->first, lst->car_, i->second );
				vals = lst->cdr_;
			}
	}

	{
		typedef list< Ast::Expression * >::const_iterator I;
		I i = orderedExprs_->begin( );
		I end = orderedExprs_->end( );
		for( ; i != end; ++i )
			{
				RefCountPtr< ConsType > lst = vals.down_cast< ConsType >( );
				if( lst == NullPtr< ConsType >( ) )
					{
						throw Exceptions::InternalError( strrefdup( "Out of argument values when binding application." ) );
					}
				dst->addOrderedArgument( lst->car_, *i );
				vals = lst->cdr_;
			}
	}

	/* Here, it could/should be verified that vals is null.  However, it only isn't in case of an internal error...
	 */

	/* Next, we turn to the states.  The states need no evaluation, as they are always passed by reference.
	 */

	{
		typedef std::map< const char *, Ast::StateReference *, charPtrLess >::const_iterator I;
		I i = namedStates_->begin( );
		I end = namedStates_->end( );
		for( ; i != end; ++i )
			{
				dst->addNamedState( i->first, i->second->getHandle( env, dyn ) , i->second );
			}
	}

	{
		typedef list< Ast::StateReference * >::const_iterator I;
		I i = orderedStates_->begin( );
		I end = orderedStates_->end( );
		for( ; i != end; ++i )
			{
				dst->addOrderedState( (*i)->getHandle( env, dyn ), *i );
			}
	}

}


Kernel::VariableHandle
Ast::ArgListExprs::findNamed( RefCountPtr< const Lang::SingleList > vals, const char * name ) const
{
	/* This function is called when a Lang::Structure is asked for a field by name.
	 * This is reflected in the generated error in case the name is not found.
	 */

	typedef const Lang::SingleListPair ConsType;

	/* Note that the arguments are bound in backwards-order, since that is how the values are accessed.
	 */

	typedef std::map< const char *, Ast::Expression *, charPtrLess >::const_reverse_iterator I;
	I i = namedExprs_->rbegin( );
	I end = namedExprs_->rend( );
	for( ; i != end; ++i )
		{
			RefCountPtr< ConsType > lst = vals.down_cast< ConsType >( );
			if( lst == NullPtr< ConsType >( ) )
				{
					throw Exceptions::InternalError( strrefdup( "Out of argument values when searching fields." ) );
				}
			if( strcmp( i->first, name ) == 0 )
				{
					return lst->car_;
				}
			vals = lst->cdr_;
		}

	throw Exceptions::NonExistentMember( strrefdup( "< user union >" ), name );
}

Kernel::VariableHandle
Ast::ArgListExprs::getOrdered( RefCountPtr< const Lang::SingleList > vals, size_t pos ) const
{
	/* This function is called when a Lang::Structure is asked for a field by position.
	 * This is reflected in the generated error in case the position is not found.
	 */

	typedef const Lang::SingleListPair ConsType;

	/* Note that the arguments are bound in backwards-order, since that is how the values are accessed.
	 */

	size_t i = 0;
	while( true )
		{
			RefCountPtr< ConsType > lst = vals.down_cast< ConsType >( );
			if( lst == NullPtr< ConsType >( ) )
				{
					break;
				}
			if( i == pos )
				{
					return lst->car_;
				}
			vals = lst->cdr_;
			++i;
		}

	throw Exceptions::NonExistentPosition( pos, i - 1 );
}

void
Ast::ArgListExprs::analyze( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst )
{
	parent_ = parent;

	RefCountPtr< Ast::StateIDSet > passedStates( new Ast::StateIDSet );

	/* First traverse state arguments, so that we know which states that must not appear in the value arguments.
	 */
	{
		typedef typeof *orderedStates_ ListType;
		for( ListType::iterator i = orderedStates_->begin( ); i != orderedStates_->end( ); ++i )
			{
				(*i)->analyze( parent_, env, passedStates.getPtr( ) );
			}
	}
	{
		typedef typeof *namedStates_ ListType;
		for( ListType::iterator i = namedStates_->begin( ); i != namedStates_->end( ); ++i )
			{
				i->second->analyze( parent_, env, passedStates.getPtr( ) );
			}
	}
	if( passedStates->size( ) < orderedStates_->size( ) + namedStates_->size( ) )
		{
			Ast::theAnalysisErrorsList.push_back( new Exceptions::RepeatedStateArgument( parent_->loc( ), passedStates ) );
		}

	/* Traverse children
	 */
	{
		typedef typeof *orderedExprs_ ListType;
		for( ListType::iterator i = orderedExprs_->begin( ); i != orderedExprs_->end( ); ++i )
			{
				if( (*i)->immediate_ )
					{
						(*i)->analyze( parent_, env, freeStatesDst );
					}
				else
					{
						Ast::StateIDSet freeStates;
						(*i)->analyze( parent_, env, & freeStates );
						for( Ast::StateIDSet::const_iterator j = freeStates.begin( ); j != freeStates.end( ); ++j )
							{
								if( passedStates->find( *j ) != passedStates->end( ) )
									{
										Ast::theAnalysisErrorsList.push_back( new Exceptions::FreeStateIsAlsoPassed( (*i)->loc( ), *j ) );
									}
								freeStatesDst->insert( *j );
							}
					}
			}
	}
	{
		typedef typeof *namedExprs_ ListType;
		for( ListType::iterator i = namedExprs_->begin( ); i != namedExprs_->end( ); ++i )
			{
				if( i->second->immediate_ )
					{
						i->second->analyze( parent_, env, freeStatesDst );
					}
				else
					{
						Ast::StateIDSet freeStates;
						i->second->analyze( parent_, env, & freeStates );
						for( Ast::StateIDSet::const_iterator j = freeStates.begin( ); j != freeStates.end( ); ++j )
							{
								if( passedStates->find( *j ) != passedStates->end( ) )
									{
										Ast::theAnalysisErrorsList.push_back( new Exceptions::FreeStateIsAlsoPassed( i->second->loc( ), *j ) );
									}
								freeStatesDst->insert( *j );
							}
					}
			}
	}

	{
		for( Ast::StateIDSet::const_iterator i = passedStates->begin( ); i != passedStates->end( ); ++i )
			{
				freeStatesDst->insert( *i );
			}
	}
}

Ast::ArgListExprs::ConstIterator
Ast::ArgListExprs::begin( ) const
{
	return Ast::ArgListExprs::ConstIterator( orderedExprs_->rbegin( ), namedExprs_->begin( ), 0 );
}


Ast::FunctionFunction::FunctionFunction( const Ast::SourceLocation & loc, const Kernel::Formals * formals, Ast::Expression * body, const Ast::FunctionMode & functionMode )
	: Lang::Function( new Kernel::EvaluatedFormals( Ast::FileID::build_internal( "< function construction >" ), false ) ), loc_( loc, bool( ) ), formals_( formals ), body_( body ), functionMode_( functionMode )
{ }

Ast::FunctionFunction::~FunctionFunction( )
{
	delete formals_;
	delete body_;
}

void
Ast::FunctionFunction::push_exprs( Ast::ArgListExprs * args ) const
{
	formals_->push_exprs( args );
}

void
Ast::FunctionFunction::analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * parentEnv, Ast::StateIDSet * freeStatesDst )
{
	Ast::AnalysisEnvironment * env( new Ast::AnalysisEnvironment( Ast::theAnalysisEnvironmentList, parentEnv, formals_->argumentIdentifiers_, formals_->stateIdentifiers_ ) );
	env->activateFunctionBoundary( );

	/* We know that the body does not access states outside the function's scope, so we can simply
	 * leave freeStatesDst without change.
	 */
	Ast::StateIDSet freeStates;
	body_->analyze( parent, env, & freeStates );
}

void
Ast::FunctionFunction::call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
{
	Kernel::ContRef cont = evalState->cont_;
	cont->takeValue( Kernel::ValueRef( new Lang::UserFunction( formals_->newEvaluatedFormals( args ),
																														 body_, evalState->env_, functionMode_ ) ),
									 evalState );
}


Kernel::CallCont_last::CallCont_last( const RefCountPtr< const Lang::Function > & fun, const Ast::ArgListExprs * argList, bool curry, Kernel::StateHandle mutatorSelf, const Kernel::PassedEnv & env, const Kernel::PassedDyn & dyn, const Kernel::ContRef & cont, const Ast::SourceLocation & callLoc )
	: Kernel::Continuation( callLoc ), fun_( fun ), argList_( argList ), curry_( curry ), mutatorSelf_( mutatorSelf ), env_( env ), dyn_( dyn ), cont_( cont )
{ }

Kernel::CallCont_last::~CallCont_last( )
{ }

void
Kernel::CallCont_last::takeValue( const RefCountPtr< const Lang::Value > & valsUntyped, Kernel::EvalState * evalState, bool dummy ) const
{
	typedef const Lang::SingleList ArgType;
	RefCountPtr< ArgType > vals = Helpers::down_cast< ArgType >( valsUntyped, "< Internal error situation in CallCont_last >" );

	Kernel::Arguments args = fun_->newCurriedArguments( );
	argList_->bind( & args, vals, env_, dyn_ );
	if( mutatorSelf_ != 0 )
		{
			args.setMutatorSelf( mutatorSelf_ );
		}

	if( curry_ )
		{
			evalState->cont_ = cont_;
			cont_->takeValue( Kernel::ValueRef( new Lang::CuteFunction( fun_, args ) ), evalState );
		}
	else
		{
			evalState->env_ = env_; /* This matters only when the function being called is FunctionFunction! */
			evalState->dyn_ = dyn_;
			evalState->cont_ = cont_;
			fun_->call( evalState, args, traceLoc_ );
		}
}

Kernel::ContRef
Kernel::CallCont_last::up( ) const
{
	return cont_;
}

RefCountPtr< const char >
Kernel::CallCont_last::description( ) const
{
	return strrefdup( "function call's application" );
}

void
Kernel::CallCont_last::gcMark( Kernel::GCMarkedSet & marked )
{
	const_cast< Lang::Function * >( fun_.getPtr( ) )->gcMark( marked );
	dyn_->gcMark( marked );
	cont_->gcMark( marked );
}


Kernel::CallCont_Structure_last::CallCont_Structure_last( const RefCountPtr< const Lang::Function > & fun, const Ast::ArgListExprs * argList, const RefCountPtr< const Lang::SingleList > & values, bool curry, const Kernel::PassedEnv & env, const Kernel::PassedDyn & dyn, const Kernel::ContRef & cont, const Ast::SourceLocation & callLoc )
	: Kernel::Continuation( callLoc ), fun_( fun ), argList_( argList ), values_( values ), curry_( curry ), env_( env ), dyn_( dyn ), cont_( cont )
{ }

Kernel::CallCont_Structure_last::~CallCont_Structure_last( )
{ }

void
Kernel::CallCont_Structure_last::takeValue( const RefCountPtr< const Lang::Value > & valDummy, Kernel::EvalState * evalState, bool dummy ) const
{
	Kernel::Arguments args = fun_->newCurriedArguments( );
	argList_->bind( & args, values_, env_, dyn_ );

	if( curry_ )
		{
			evalState->cont_ = cont_;
			cont_->takeValue( Kernel::ValueRef( new Lang::CuteFunction( fun_, args ) ), evalState );
		}
	else
		{
			evalState->env_ = env_; /* This matters only when the function being called is FunctionFunction! */
			evalState->dyn_ = dyn_;
			evalState->cont_ = cont_;
			fun_->call( evalState, args, traceLoc_ );
		}
}

Kernel::ContRef
Kernel::CallCont_Structure_last::up( ) const
{
	return cont_;
}

RefCountPtr< const char >
Kernel::CallCont_Structure_last::description( ) const
{
	return strrefdup( "split structure application" );
}

void
Kernel::CallCont_Structure_last::gcMark( Kernel::GCMarkedSet & marked )
{
	const_cast< Lang::Function * >( fun_.getPtr( ) )->gcMark( marked );
	const_cast< Lang::SingleList * >( values_.getPtr( ) )->gcMark( marked );
	dyn_->gcMark( marked );
	cont_->gcMark( marked );
}


Kernel::CallContInfo::CallContInfo( const Ast::ArgListExprs * argList, const Kernel::EvalState & evalState, std::vector< bool > * forcePos )
	: forcePos_( forcePos ), forceAll_( false ), argList_( argList ), env_( evalState.env_ ), dyn_( evalState.dyn_ ), cont_( evalState.cont_ )
{ }

Kernel::CallContInfo::CallContInfo( const Ast::ArgListExprs * argList, const Kernel::EvalState & evalState, bool forceAll )
	: forcePos_( 0 ), forceAll_( forceAll ), argList_( argList ), env_( evalState.env_ ), dyn_( evalState.dyn_ ), cont_( evalState.cont_ )
{ }

Kernel::CallContInfo::~CallContInfo( )
{
	if( forcePos_ != 0 )
		{
			delete forcePos_;
		}
}

bool
Kernel::CallContInfo::force( const size_t & pos ) const
{
	if( forcePos_ != 0 )
		{
			return (*forcePos_)[ pos ];
		}
	return forceAll_;
}

bool
Kernel::CallContInfo::isSelective( ) const
{
	return forcePos_ != 0;
}

bool
Kernel::CallContInfo::forceNone( ) const
{
	if( isSelective( ) )
		return false;
	return ! forceAll_;
}

bool
Kernel::CallContInfo::forceAll( ) const
{
	if( isSelective( ) )
		return false;
	return forceAll_;
}

void
Kernel::CallContInfo::gcMark( Kernel::GCMarkedSet & marked )
{
	env_->gcMark( marked );
	dyn_->gcMark( marked );
	cont_->gcMark( marked );
}


Kernel::CallCont_n::CallCont_n( const Ast::SourceLocation & traceLoc, const RefCountPtr< const Kernel::CallContInfo > & info, const Ast::ArgListExprs::ConstIterator & pos, RefCountPtr< const Lang::SingleList > vals, bool force )
	: Kernel::Continuation( traceLoc ), info_( info ), pos_( pos ), vals_( vals ), force_( force )
{ }

Kernel::CallCont_n::~CallCont_n( )
{ }

void
Kernel::CallCont_n::takeHandle( Kernel::VariableHandle val, Kernel::EvalState * evalState, bool dummy ) const
{
	/* Even if force_ is set, this continuation takes handles since handles is what goes into the argument list.
	 */

	if( force_ && val->isThunk( ) )
		{
			val->force( val, evalState );
			return;
		}
	if( val == Kernel::THE_SLOT_VARIABLE )
		{
			/* If we don't detect this, the error messages that will be printed later would be very confusing,
			 * saying that the argument is missing, when the problem is likely to be that some internal
			 * computation should result in THE_VOID_VARIABLE rather than THE_SLOT_VARIABLE.
			 */
			throw Exceptions::InternalError( "An argument expression evaluated to THE_SLOT_VARIABLE." );
		}
	info_->argList_->evaluate( info_,
														 pos_,
														 RefCountPtr< const Lang::SingleListPair >( new Lang::SingleListPair( val, vals_ ) ),
														 evalState );
}

Kernel::ContRef
Kernel::CallCont_n::up( ) const
{
	return info_->cont_;
}

RefCountPtr< const char >
Kernel::CallCont_n::description( ) const
{
	return strrefdup( force_ ? "function call's forced argument" : "function call's immediate argument" );
}

void
Kernel::CallCont_n::gcMark( Kernel::GCMarkedSet & marked )
{
	const_cast< Kernel::CallContInfo * >( info_.getPtr( ) )->gcMark( marked );
	const_cast< Lang::SingleList * >( vals_.getPtr( ) )->gcMark( marked );
}


Kernel::CallCont_Structure_n::CallCont_Structure_n( const Ast::SourceLocation & traceLoc, const RefCountPtr< const Kernel::CallContInfo > & info, size_t pos, RefCountPtr< const Lang::SingleList > rest )
	: Kernel::Continuation( traceLoc ), info_( info ), pos_( pos ), rest_( rest )
{ }

Kernel::CallCont_Structure_n::~CallCont_Structure_n( )
{ }

void
Kernel::CallCont_Structure_n::takeValue( const RefCountPtr< const Lang::Value > & forcedValue, Kernel::EvalState * evalState, bool dummy ) const
{
	info_->argList_->evaluate_Structure( info_, pos_, rest_, evalState );
}

Kernel::ContRef
Kernel::CallCont_Structure_n::up( ) const
{
	return info_->cont_;
}

RefCountPtr< const char >
Kernel::CallCont_Structure_n::description( ) const
{
	return strrefdup( "split structure application forced argument" );
}

void
Kernel::CallCont_Structure_n::gcMark( Kernel::GCMarkedSet & marked )
{
	const_cast< Kernel::CallContInfo * >( info_.getPtr( ) )->gcMark( marked );
	const_cast< Lang::SingleList * >( rest_.getPtr( ) )->gcMark( marked );
}


Kernel::CallCont_1::CallCont_1( const Ast::SourceLocation & traceLoc, const Ast::ArgListExprs * argList, bool curry, Kernel::StateHandle mutatorSelf, const Kernel::EvalState & evalState, const Ast::SourceLocation & callLoc )
	: Kernel::Continuation( traceLoc ), argList_( argList ), curry_( curry ), mutatorSelf_( mutatorSelf ), env_( evalState.env_ ), dyn_( evalState.dyn_ ), cont_( evalState.cont_ ), callLoc_( callLoc )
{ }

Kernel::CallCont_1::~CallCont_1( )
{ }

void
Kernel::CallCont_1::takeValue( const RefCountPtr< const Lang::Value > & funUntyped, Kernel::EvalState * evalState, bool dummy ) const
{
	{
		typedef const Lang::Function ArgType;
		RefCountPtr< ArgType > fun = funUntyped.down_cast< const Lang::Function >( );
		if( fun != NullPtr< ArgType >( ) )
			{
				evalState->env_ = env_;
				evalState->dyn_ = dyn_;
				evalState->cont_ = Kernel::ContRef( new Kernel::CallCont_last( fun, argList_, curry_, mutatorSelf_, env_, dyn_, cont_, callLoc_ ) );
				argList_->evaluate( fun->newCallContInfo( argList_, *evalState ),
														argList_->begin( ), Lang::THE_CONS_NULL,
														evalState );
				return;
			}
	}
	{
		typedef const Lang::Transform2D ArgType;
		ArgType * transformVal = dynamic_cast< ArgType * >( funUntyped.getPtr( ) );
		if( transformVal != 0 )
			{
				if( curry_ )
					{
						throw Exceptions::MiscellaneousRequirement( strrefdup( "Don't Curry transform applications.  It's useless anyway!" ) );
					}
				if( argList_->orderedExprs_->size( ) != 1 )
					{
						throw Exceptions::CoreArityMismatch( "<transform application>", 1, argList_->orderedExprs_->size( ) );
					}
				if( ! argList_->namedExprs_->empty( ) )
					{
						throw Exceptions::CoreNoNamedFormals( "<transform application>" );
					}
				evalState->expr_ = argList_->orderedExprs_->front( );
				evalState->env_ = env_;
				evalState->dyn_ = dyn_;
				evalState->cont_ = Kernel::ContRef( new Kernel::Transform2DCont( *transformVal, cont_, callLoc_ ) );
				return;
			}
	}
	{
		typedef const Lang::Transform3D ArgType;
		ArgType * transformVal = dynamic_cast< ArgType * >( funUntyped.getPtr( ) );
		if( transformVal != 0 )
			{
				if( curry_ )
					{
						throw Exceptions::MiscellaneousRequirement( strrefdup( "Don't Curry transform applications.  It's useless anyway!" ) );
					}
				if( argList_->orderedExprs_->size( ) != 1 )
					{
						throw Exceptions::CoreArityMismatch( "<transform application>", 1, argList_->orderedExprs_->size( ) );
					}
				if( ! argList_->namedExprs_->empty( ) )
					{
						throw Exceptions::CoreNoNamedFormals( "<transform application>" );
					}
				evalState->expr_ = argList_->orderedExprs_->front( );
				evalState->env_ = env_;
				evalState->dyn_ = dyn_;
				evalState->cont_ = Kernel::ContRef( new Kernel::Transform3DCont( *transformVal, cont_, callLoc_ ) );
				return;
			}
	}

	{
		typedef const Lang::ElementaryPath2D ArgType;
		RefCountPtr< ArgType > path = NullPtr< ArgType >( );
		try
			{
				path = Helpers::elementaryPathCast2D( funUntyped, this );
			}
		catch( const Exceptions::ContinuationTypeMismatch & ball )
			{
				goto nextType1;
			}

		if( curry_ )
			{
				throw Exceptions::MiscellaneousRequirement( strrefdup( "Don't Curry path point selection.  It's useless anyway!" ) );
			}
		if( argList_->orderedExprs_->size( ) != 1 )
			{
				throw Exceptions::CoreArityMismatch( "<path point selection>", 1, argList_->orderedExprs_->size( ) );
			}
		if( ! argList_->namedExprs_->empty( ) )
			{
				throw Exceptions::CoreNoNamedFormals( "<path point selection>" );
			}

		evalState->expr_ = argList_->orderedExprs_->front( );
		evalState->env_ = env_;
		evalState->dyn_ = dyn_;
		evalState->cont_ = Kernel::ContRef( new Kernel::PathApplication2DCont( path,
																																					 cont_,
																																					 callLoc_ ) );
		return;
	}
 nextType1:

	{
		typedef const Lang::ElementaryPath3D ArgType;
		RefCountPtr< ArgType > path = NullPtr< ArgType >( );
		try
			{
				path = Helpers::elementaryPathCast3D( funUntyped, this );
			}
		catch( const Exceptions::ContinuationTypeMismatch & ball )
			{
				goto nextType2;
			}

		if( curry_ )
			{
				throw Exceptions::MiscellaneousRequirement( strrefdup( "Don't Curry path point selection.  It's useless anyway!" ) );
			}
		if( argList_->orderedExprs_->size( ) != 1 )
			{
				throw Exceptions::CoreArityMismatch( "<path point selection>", 1, argList_->orderedExprs_->size( ) );
			}
		if( ! argList_->namedExprs_->empty( ) )
			{
				throw Exceptions::CoreNoNamedFormals( "<path point selection>" );
			}

		evalState->expr_ = argList_->orderedExprs_->front( );
		evalState->env_ = env_;
		evalState->dyn_ = dyn_;
		evalState->cont_ = Kernel::ContRef( new Kernel::PathApplication3DCont( path,
																																					 cont_,
																																					 callLoc_ ) );
		return;
	}
 nextType2:

	throw Exceptions::TypeMismatch( traceLoc_,
																	funUntyped->getTypeName( ),
																	Helpers::typeSetString( Lang::Function::staticTypeName( ),
																													Lang::Transform2D::staticTypeName( ),
																													Lang::Transform3D::staticTypeName( ),
																													Lang::ElementaryPath2D::staticTypeName( ),
																													Lang::ElementaryPath3D::staticTypeName( ) ) );
}

Kernel::ContRef
Kernel::CallCont_1::up( ) const
{
	return cont_;
}

RefCountPtr< const char >
Kernel::CallCont_1::description( ) const
{
	return strrefdup( "function call's function" );
}

void
Kernel::CallCont_1::gcMark( Kernel::GCMarkedSet & marked )
{
	dyn_->gcMark( marked );
	cont_->gcMark( marked );
}



Ast::CallExpr::CallExpr( const Ast::SourceLocation & loc, Ast::Expression * funExpr, Ast::ArgListExprs * argList, bool curry )
	: Ast::Expression( loc ), curry_( curry ), constFun_( Kernel::THE_NO_FUNCTION ), mutatorSelf_( 0 ), funExpr_( funExpr ), argList_( argList )
{ }

Ast::CallExpr::CallExpr( const Ast::SourceLocation & loc, const RefCountPtr< const Lang::Function > & constFun, Ast::ArgListExprs * argList, bool curry )
	: Ast::Expression( loc ), curry_( curry ), constFun_( constFun ), mutatorSelf_( 0 ), funExpr_( 0 ), argList_( argList )
{ }

Ast::CallExpr::~CallExpr( )
{
	if( funExpr_ != 0 )
		{
			delete funExpr_;
		}
	if( mutatorSelf_ != 0 )
		{
			delete mutatorSelf_;
		}
	delete argList_;
}

void
Ast::CallExpr::setMutatorSelf( Ast::StateReference * mutatorSelf )
{
	mutatorSelf_ = mutatorSelf;
}

void
Ast::CallExpr::analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst )
{
	if( mutatorSelf_ != 0 )
		{
			mutatorSelf_->analyze( this, env, freeStatesDst );
		}
	if( funExpr_ != 0 )
		{
			funExpr_->analyze( this, env, freeStatesDst );
		}
	else
		{
			const_cast< Lang::Function * >( constFun_.getPtr( ) )->analyze( this, env );
		}
	argList_->analyze( this, env, freeStatesDst );

	if( funExpr_ != 0 )
		{
			Ast::LexiographicVariable * var = dynamic_cast< Ast::LexiographicVariable * >( funExpr_ );
			if( var != 0 &&
					var->scope_level( ) == 0 )
				{
					/* The callee is bound in the base environment. */
					if( strcmp( var->id( )->simpleId( ), Lang::TEX_SYNTAX_ID ) == 0 ) /* Assume there is only one namespace with a matching identifier to simplify the comparison. */
						{
							/* Someone is calling TeX.  If the argument is a string literal, it should be announced. */
							if( argList_->orderedExprs_ != 0 && ! argList_->orderedExprs_->empty( ) )
								{
									/* If there are named arguments or more than one ordered argument, they are ignored; only the first ordered argument is checked. */
									Ast::Constant * carg = dynamic_cast< Ast::Constant * >( argList_->orderedExprs_->front( ) );
									if( carg != 0 )
										{
											try
												{
													typedef const Lang::String ArgType;
													RefCountPtr< ArgType > strArg = carg->val( )->tryVal< ArgType >( );
													Kernel::theTeXLabelManager.announce( std::string( strArg->val_.getPtr( ) ), carg->loc( ) );
												}
											catch( const NonLocalExit::NotThisType & ball )
												{
													/* This is probably a type mismatch error, but we let the callee make the decision. */
												}
										}
								}
						}
				}
		}
}

void
Ast::CallExpr::eval( Kernel::EvalState * evalState ) const
{
	if( funExpr_ != 0 )
		{
			evalState->expr_ = funExpr_;
			evalState->cont_ = Kernel::ContRef( new Kernel::CallCont_1( evalState->expr_->loc( ), argList_, curry_, mutatorSelf_ ? (mutatorSelf_->getHandle( evalState->env_, evalState->dyn_ )) : 0, *evalState, loc_ ) );
		}
	else
		{
			evalState->cont_ = Kernel::ContRef( new Kernel::CallCont_last( constFun_, argList_, curry_, mutatorSelf_ ? (mutatorSelf_->getHandle( evalState->env_, evalState->dyn_ )) : 0, evalState->env_, evalState->dyn_, evalState->cont_, loc_ ) );
			argList_->evaluate( constFun_->newCallContInfo( argList_, *evalState ),
													argList_->begin( ), Lang::THE_CONS_NULL,
													evalState );
		}
}

Kernel::UnionCont_last::UnionCont_last( const Ast::ArgListExprs * argList, const Kernel::ContRef & cont, const Ast::SourceLocation & callLoc )
	: Kernel::Continuation( callLoc ), argList_( argList ), cont_( cont )
{ }

Kernel::UnionCont_last::~UnionCont_last( )
{ }

void
Kernel::UnionCont_last::takeValue( const RefCountPtr< const Lang::Value > & valsUntyped, Kernel::EvalState * evalState, bool dummy ) const
{
	typedef const Lang::SingleList ArgType;
	RefCountPtr< ArgType > vals = Helpers::down_cast< ArgType >( valsUntyped, "< Internal error situation in UnionCont_last >" );

	evalState->cont_ = cont_;
	cont_->takeValue( Kernel::ValueRef( new Lang::Structure( argList_, vals ) ),
										evalState );
}

Kernel::ContRef
Kernel::UnionCont_last::up( ) const
{
	return cont_;
}

RefCountPtr< const char >
Kernel::UnionCont_last::description( ) const
{
	return strrefdup( "union" );
}

void
Kernel::UnionCont_last::gcMark( Kernel::GCMarkedSet & marked )
{
	cont_->gcMark( marked );
}


Ast::UnionExpr::UnionExpr( const Ast::SourceLocation & loc, Ast::ArgListExprs * argList )
	: Ast::Expression( loc ), argList_( argList )
{ }

Ast::UnionExpr::~UnionExpr( )
{
	delete argList_;
}


void
Ast::UnionExpr::analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst )
{
	argList_->analyze( this, env, freeStatesDst );
}

void
Ast::UnionExpr::eval( Kernel::EvalState * evalState ) const
{
	evalState->cont_ = Kernel::ContRef( new Kernel::UnionCont_last( argList_, evalState->cont_, loc_ ) );

	argList_->evaluate( RefCountPtr< Kernel::CallContInfo >( new Kernel::CallContInfo( argList_, *evalState, false ) ),
											argList_->begin( ), Lang::THE_CONS_NULL,
											evalState );
}


Kernel::SplitCont_1::SplitCont_1( const Ast::SourceLocation & traceLoc, Ast::Expression * argList, bool curry, const Kernel::EvalState & evalState, const Ast::SourceLocation & callLoc )
	: Kernel::Continuation( traceLoc ), argList_( argList ), curry_( curry ), env_( evalState.env_ ), dyn_( evalState.dyn_ ), cont_( evalState.cont_ ), callLoc_( callLoc )
{ }

Kernel::SplitCont_1::~SplitCont_1( )
{ }

void
Kernel::SplitCont_1::takeValue( const RefCountPtr< const Lang::Value > & funUntyped, Kernel::EvalState * evalState, bool dummy ) const
{
	typedef const Lang::Function ArgType;
	RefCountPtr< ArgType > fun = Helpers::down_cast< ArgType >( funUntyped, "Split's function" );

	evalState->env_ = env_;
	evalState->dyn_ = dyn_;
	evalState->cont_ = Kernel::ContRef( new Kernel::SplitCont_2( fun, curry_, env_, dyn_, cont_, callLoc_ ) );
	evalState->expr_ = argList_;
}

Kernel::ContRef
Kernel::SplitCont_1::up( ) const
{
	return cont_;
}

RefCountPtr< const char >
Kernel::SplitCont_1::description() const
{
	return strrefdup( "split call's function" );
}

void
Kernel::SplitCont_1::gcMark( Kernel::GCMarkedSet & marked )
{
	dyn_->gcMark( marked );
	cont_->gcMark( marked );
}


Kernel::SplitCont_2::SplitCont_2( const RefCountPtr< const Lang::Function > & fun, bool curry, const Kernel::PassedEnv & env, const Kernel::PassedDyn & dyn, const Kernel::ContRef & cont, const Ast::SourceLocation & callLoc )
	: Kernel::Continuation( callLoc ), fun_( fun ), curry_( curry ), env_( env ), dyn_( dyn ), cont_( cont )
{ }

Kernel::SplitCont_2::~SplitCont_2( )
{ }

void
Kernel::SplitCont_2::takeValue( const RefCountPtr< const Lang::Value > & valsUntyped, Kernel::EvalState * evalState, bool dummy ) const
{
	/* This continuation shall mimic the behavior Kernel::CallCont_1.
	 */

	typedef const Lang::Structure ArgType;
	RefCountPtr< ArgType > structure = Helpers::down_cast< ArgType >( valsUntyped, "Split" );

	const Ast::ArgListExprs * argList = structure->argList_;

	evalState->env_ = env_;
	evalState->dyn_ = dyn_;
	evalState->cont_ = Kernel::ContRef( new Kernel::CallCont_Structure_last( fun_, argList, structure->values_, curry_, env_, dyn_, cont_, traceLoc_ ) );
	RefCountPtr< Kernel::CallContInfo > callContInfo = fun_->newCallContInfo( argList, *evalState );

	if( callContInfo->forceNone( ) ){

		/* Bypass forcing.
		 */
		Kernel::ContRef cont = evalState->cont_;
		cont->takeValue( structure->values_, evalState );

	}else{

		argList->evaluate_Structure( callContInfo,
											 					 structure->valueCount( ) - 1, /* Reverse index of first element in structure->values_. */
											 					 structure->values_,
											 					 evalState );

	}
}

Kernel::ContRef
Kernel::SplitCont_2::up( ) const
{
	return cont_;
}

RefCountPtr< const char >
Kernel::SplitCont_2::description( ) const
{
	return strrefdup( "Split/splice" );
}

void
Kernel::SplitCont_2::gcMark( Kernel::GCMarkedSet & marked )
{
	cont_->gcMark( marked );
}


Ast::CallSplitExpr::CallSplitExpr( const Ast::SourceLocation & loc, Ast::Expression * funExpr, Ast::Expression * argList, bool curry )
	: Ast::Expression( loc ), curry_( curry ), funExpr_( funExpr ), argList_( argList )
{ }

Ast::CallSplitExpr::~CallSplitExpr( )
{
	delete funExpr_;
	delete argList_;
}


void
Ast::CallSplitExpr::analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst )
{
	funExpr_->analyze( this, env, freeStatesDst );
	argList_->analyze( this, env, freeStatesDst );
}

void
Ast::CallSplitExpr::eval( Kernel::EvalState * evalState ) const
{
	evalState->expr_ = funExpr_;
	evalState->cont_ = Kernel::ContRef( new Kernel::SplitCont_1( evalState->expr_->loc( ), argList_, curry_, *evalState, loc_ ) );
}


Ast::DummyExpression::DummyExpression( )
	: Ast::Expression( Ast::THE_UNKNOWN_LOCATION )
{ }

Ast::DummyExpression::DummyExpression( const Ast::SourceLocation & loc )
	: Ast::Expression( loc )
{ }

Ast::DummyExpression::~DummyExpression( )
{ }

void
Ast::DummyExpression::analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst )
{
	/* Nothing to do! */
}

void
Ast::DummyExpression::eval( Kernel::EvalState * evalState ) const
{
	throw Exceptions::InternalError( strrefdup( "A DummyExpression must never be evaluated." ) );
}

