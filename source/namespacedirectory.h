/* This file is part of Shapes.
 *
 * Shapes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Shapes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2015 Henrik Tidefelt
 */

#pragma once

#include "refcount.h"
#include "charptrless.h"
#include "sourcelocation.h"
#include "identifier.h"
#include "Shapes_Ast_decls.h"

#include <list>
#include <stack>
#include <map>
#include <set>
#include <string>
#include <iostream>

namespace Shapes
{
  namespace Ast /* Not necessarily the ideal home for these classes. */
  {

    class NamespaceDirectoryEntry;

    /* Model of a Shapes-Namespace.txt file.
     * Used during initialization of NamespaceDirectory.
     */
    class NamespaceDeclarations
    {
      bool encapsulated_;
      std::map< std::string, std::set< std::string > > orderGraph_; /* All entries in orderGraph_[x] precede x. */
      std::set< std::string > preludeSet_;
      std::set< std::string > ignoreSet_;

    public:
      NamespaceDeclarations( );

      void setEncapsulated( ) { encapsulated_ = true; }
      void addPrelude( const std::string & entry );
      void addIgnore( const std::string & entry );
      void addPrecedes( const std::set< std::string > & x, const std::set< std::string > & y );

      bool encapsulated( ) const { return encapsulated_; }
      bool inIgnoreSet( const std::string & name ) const { return ignoreSet_.find( name ) != ignoreSet_.end( ); }
      bool inPreludeSet( const std::string & name ) const { return preludeSet_.find( name ) != preludeSet_.end( ); }
      void invalidOrderGraphEntries( const std::map< std::string, NamespaceDirectoryEntry * > & entries, std::set< std::string > * invalidEntries ) const;
      std::string orderEntries( const std::map< std::string, NamespaceDirectoryEntry * > & entries, std::vector< NamespaceDirectoryEntry * > * loadOrder ) const; /* Returns the name of an entry on a cycle, or the empty string if no cycle exists. */
    };

    /* The load stack contains files waiting to be loaded.
     */
    class LoadStack
    {
      std::stack< const FileID * > fileIDs_;
      std::stack< RefCountPtr< const Ast::NamespacePath > > namespacePaths_;

    public:
      LoadStack( );
      size_t size( ) const { return fileIDs_.size( ); }
      bool empty( ) const { return fileIDs_.empty( ); }

      const FileID * topFileID( ) const { return fileIDs_.top( ); }
      RefCountPtr< const Ast::NamespacePath > topNamespace( ) const { return namespacePaths_.top( ); }

      void push( const FileID * fileID, const RefCountPtr< const Ast::NamespacePath > & namespacePath );
      void pop( );
    };

    class NamespaceDirectory;
    class NamespaceFile;

    class NamespaceDirectoryEntry
    {
    protected:
      std::string name_;
      const Ast::NamespaceDirectory * parent_;
      bool prelude_;

    public:
      NamespaceDirectoryEntry( const std::string & name, const Ast::NamespaceDirectory * parent );
      virtual ~NamespaceDirectoryEntry( );
      void setPrelude( ) { prelude_ = true; }
      bool prelude( ) const { return prelude_; }
      virtual std::string name( ) const = 0;
      void schedulePrelude( LoadStack * loadStack );
      void schedule( LoadStack * loadStack );
      virtual void schedule_impl( LoadStack * loadStack, bool preludeOnly = false ) = 0;
    };

    /* Model of a filesystem directory corresponding to a namespace.
     */
    class NamespaceDirectory : public NamespaceDirectoryEntry
    {
      bool initialized_;
      RefCountPtr< const Ast::NamespacePath > namespacePath_;
      std::string filesystemPath_;
      bool encapsulated_;
      std::map< std::string, NamespaceDirectoryEntry * > entries_;
      std::vector< NamespaceDirectoryEntry * > loadOrder_;

    public:
      NamespaceDirectory( const RefCountPtr< const Ast::NamespacePath > & namespacePath, const std::string & filesystemPath, const std::string & name, const Ast::NamespaceDirectory * parent );
      virtual ~NamespaceDirectory( );
      RefCountPtr< const Ast::NamespacePath > namespacePath( ) const { return namespacePath_; }
      void scheduleNamespace( const Ast::NamespacePath & namespacePath, const Ast::NamespacePath::const_iterator & nsBegin, LoadStack * loadStack );
      const FileID * resolveSingleFile( const Ast::NamespacePath & namespacePath, const Ast::NamespacePath::const_iterator & nsBegin, const std::string & filename, const Ast::SourceLocation & loc );
      virtual std::string name( ) const;
      virtual void schedule_impl( LoadStack * loadStack, bool preludeOnly );

    private:
      void initialize( );
      bool addDirectory( const std::string & nsName, const std::string & dir, bool prelude ); /* Returns true if dir is a namespace directory. */
    };

    class NamespaceFile : public NamespaceDirectoryEntry
    {
      const FileID * fileID_;

    public:
      NamespaceFile( const FileID * fileID, const std::string & name, const Ast::NamespaceDirectory * parent );
      virtual ~NamespaceFile( );
      const FileID * fileID() const { return fileID_; }
      virtual std::string name( ) const;
      virtual void schedule_impl( LoadStack * loadStack, bool preludeOnly );
    };

    /* The namespace loader keeps track of all namespace directories on the need path,
     * and is responsible for listing the files to be loaded as part of the prelude
     * or that correspond to a namespace.
     */
    class NamespaceLoader
    {
      std::map< std::string, NamespaceDirectory * > topEntries_;

    public:
      NamespaceLoader( );
      ~NamespaceLoader( );
      void registerNeedDirectory( const std::string & dir );

      void pushPreludeFiles( LoadStack * loadStack );
      void pushNamespaceFiles( const Ast::NamespacePath & namespacePath, LoadStack * loadStack );
      const Ast::FileID * resolveSingleFile( const Ast::NamespacePath & namespacePath, const std::string & filename, const Ast::SourceLocation & loc );

    private:
      bool addDirectory( const std::string & nsName, const std::string & dir ); /* Returns true if dir is a namespace directory. */
    };

  }
}
