 /* This file is part of Shapes.
 *
 * Shapes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Shapes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2008, 2009, 2013, 2014 Henrik Tidefelt
 */

#include "sourcelocation.h"
#include "charconverters.h"
#include "shapesexceptions.h"
#include "ast.h"
#include "globals.h"
#include "utf8tools.h"
#include "config.h"

#include <cerrno>
#include <fstream>
#include <sstream>
#include <limits>
#include <iomanip>
#include <cstring>

using namespace Shapes;

namespace Shapes
{
	namespace Ast
	{
		std::map< const Ast::FileID *, Ast::FileInfo *, Ast::FileIDPtrLess > theSourceFiles;
	}

	namespace Interaction
	{
		/* Make sure this is initialized before any SPECIAL file is initialized!
		 */
		std::vector< const char * > theInMemorySources;
	}

	namespace Exceptions
	{
		class FileID_OpenError : public Exception
		{
		private:
			const Ast::FileID * fileID_;
			const char * msg_;
		public:
			FileID_OpenError( const Ast::FileID * fileID, const char * msg )
				: fileID_( fileID ), msg_( msg )
			{ }
			virtual ~FileID_OpenError( )
			{ }
			virtual void display( std::ostream & os ) const
			{
				os << Ast::THE_UNKNOWN_LOCATION << locsep << "Low-level source location FileID problem: " << msg_ << std::endl ;
			}
			virtual Interaction::ExitCode exitCode( ) const { return Interaction::EXIT_INTERNAL_ERROR; }
		};
	}
}

const char * Ast::FileID::INPUTLIST_PREFIX = "#";
const size_t Ast::FileID::INPUTLIST_PREFIX_LENGTH = Ast::FileID::inputlistPrefixLength( ); /* Don't do before Ast::SourceLocation::INPUTLIST_PREFIX is initialized! */


Ast::FileInfo::FileInfo( const std::string & name )
	: name_( name )
{ }


Ast::FileID::FileID( Kind kind, dev_t st_dev, ino_t st_ino, const char * strData )
	: kind_( kind ), st_dev_( st_dev ), st_ino_( st_ino ), strData_( strData )
{ }

const Ast::FileID *
Ast::FileID::build_stat( const struct stat & stat, const std::string & filename )
{
	const FileID * tmp = new FileID( NORMAL_FILE, stat.st_dev, stat.st_ino, 0 );
	typedef typeof Ast::theSourceFiles MapType;
	MapType::iterator i = Ast::theSourceFiles.find( tmp );
	if( i != Ast::theSourceFiles.end( ) )
		{
			delete tmp;
			return i->first;
		}
	const_cast< FileID * >( tmp )->strData_ = strdup( filename.c_str( ) );
	Ast::theSourceFiles.insert( MapType::value_type( tmp, new Ast::FileInfo( filename ) ) );
	return tmp;
}

const Ast::FileID *
Ast::FileID::build_inMemory( ino_t index, const std::string & sourceName )
{
	const FileID * tmp = new FileID( IN_MEMORY, 0, index, 0 );
	typedef typeof Ast::theSourceFiles MapType;
	MapType::iterator i = Ast::theSourceFiles.find( tmp );
	if( i != Ast::theSourceFiles.end( ) )
		{
			delete tmp;
			return i->first;
		}
	Ast::theSourceFiles.insert( MapType::value_type( tmp, new Ast::FileInfo( sourceName ) ) );
	return tmp;
}

const Ast::FileID *
Ast::FileID::build_special( const char * specialName )
{
	const FileID * tmp = new FileID( SPECIAL, 0, 0, specialName );
	typedef typeof Ast::theSourceFiles MapType;
	MapType::iterator i = Ast::theSourceFiles.find( tmp );
	if( i != Ast::theSourceFiles.end( ) )
		{
			delete tmp;
			return i->first;
		}
	Ast::theSourceFiles.insert( MapType::value_type( tmp, 0 ) );
	tmp->initInfo( specialName );
	return tmp;
}

const Ast::FileID *
Ast::FileID::build_internal( const char * locationName )
{
	/* A SPECIAL FileID has no associated FileInfo object in Ast::theSourceFiles.  Instead,
	 * the name of the source location (which does not have any line or column information by definition)
	 * is stored directly in the strData_ field.
	 * For efficiency, we must not make a deep copy of locationName, so the caller must take care of memory
	 * management.  In most cases locatioName is a string litteral, so then there's no problem.
	 */
	return new FileID( INTERNAL, 0, 0, locationName );
}

bool
Ast::FileID::is_inMemory( const std::string & filename, const FileID ** fileDst )
{
	if( strncmp( filename.c_str( ), INPUTLIST_PREFIX, INPUTLIST_PREFIX_LENGTH ) != 0 )
		{
			return 0;
		}
	char * end;
	ino_t ind = strtol( filename.c_str( ) + INPUTLIST_PREFIX_LENGTH, & end, 10 );
	if( *end != '\0' )
		{
			std::ostringstream msg;
			msg << "Error in error message: Failed to extract suffix from inputlist filename: " << filename ;
			Kernel::thePostCheckErrorsList.push_back( new Exceptions::InternalError( msg ) );
			return false;
		}

  /* Used to first test ( ind < 0 ) below, but I'm currently on a platform where ino_t is unsigned,
   * so the test gives a compiler warning.  Hence, rather than checking ( ind < 0 ) we now check that
   * the ino_t is unsigned.
   */
  if( std::numeric_limits< ino_t >::is_signed ){
		Kernel::thePostCheckErrorsList.push_back( new Exceptions::InternalError( "Ast::FileID::is_inMemory: Incorrectly assumed that ino_t is unsigned." ) );
		return false;
  }
	if( Interaction::theInMemorySources.size( ) <= static_cast< size_t >( ind ) ||
			Interaction::theInMemorySources[ ind ] == 0 )
		{
			std::ostringstream msg;
			msg << "Error in error message: Inputlist index " << ind << " is out of range in: " << filename ;
			Kernel::thePostCheckErrorsList.push_back( new Exceptions::InternalError( msg ) );
			return false;
		}
	*fileDst = Ast::FileID::build_inMemory( ind, filename );
	return true;
}

void
Ast::FileID::initInfo( const std::string & name ) const
{
	Ast::FileInfo * res = Ast::theSourceFiles[ this ];
	if( res == 0 )
		{
			Ast::theSourceFiles[ this ] = new Ast::FileInfo( name );
		}
	else
		{
			std::cerr << "Internal error. Ast::FileID::initInfo called multiple times for the file: " << name << std::endl ;
			exit( Interaction::EXIT_INTERNAL_ERROR );
		}
}

const char *
Ast::FileID::name( ) const
{
	if( kind_ == INTERNAL )
		{
			return strData_;
		}
	Ast::FileInfo * res = Ast::theSourceFiles[ this ];
	if( res == 0 )
		{
			std::cerr << "Internal error. Ast::FileID::name called before initializaiton." << std::endl ;
			exit( Interaction::EXIT_INTERNAL_ERROR );
		}
	return res->name_.c_str( );
}

std::vector< Ast::Node * > &
Ast::FileID::nodes( ) const
{
	Ast::FileInfo * res = Ast::theSourceFiles[ this ];
	if( res == 0 )
		{
			std::cerr << "Internal error. Ast::FileID::nodes called before initializaiton." << std::endl ;
			exit( Interaction::EXIT_INTERNAL_ERROR );
		}
	return res->topLevelNodes_;
}

bool
Ast::FileID::hasPosition( ) const
{
	return kind_ != INTERNAL;
}

bool
Ast::FileID::operator < ( const Ast::FileID & other ) const
{
	if( kind_ == NORMAL_FILE && other.kind_ != NORMAL_FILE )
		{
			return true;
		}
	if( kind_ != NORMAL_FILE && other.kind_ == NORMAL_FILE )
		{
			return false;
		}
	if( kind_ == NORMAL_FILE )
		{
			if( st_dev_ < other.st_dev_ )
				{
					return true;
				}
			if( st_dev_ > other.st_dev_ )
				{
					return false;
				}
			return st_ino_ < other.st_ino_;
		}
	if( kind_ == IN_MEMORY && other.kind_ != IN_MEMORY )
		{
			return true;
		}
	if( kind_ != IN_MEMORY && other.kind_ == IN_MEMORY )
		{
			return false;
		}
	if( kind_ == IN_MEMORY )
		{
			return st_ino_ < other.st_ino_;
		}
	/* We reach here if both values are SPECIAL or INTERNAL. */
	return strData_ < other.strData_;
}

std::istream *
Ast::FileID::open( std::ifstream * ifsMem, std::istringstream * issMem ) const
{
	switch( kind_ )
		{
		case NORMAL_FILE:
			{
				ifsMem->open( strData_ );
				if( ! ifsMem->is_open( ) )
					{
						throw Exceptions::FileID_OpenError( this, "Failed to open NORMAL file." );
					}
				return ifsMem;
			}
		case IN_MEMORY:
			{
				issMem->str( Interaction::theInMemorySources[ st_ino_ ] );
				return issMem;
			}
		case SPECIAL:
		case INTERNAL:
			{
				/* This is an error, see below. */
				break;
			}
		}
	throw Exceptions::FileID_OpenError( this, "FileID::open is not applicable to SPECIAL or INTERNAL files" );
}

void
Ast::FileID::setMem( const char * data )
{
	if( kind_ != IN_MEMORY )
		{
			std::cerr << "Internal error: FileID::setMem is only applicable to IN_MEMORY objects." << std::endl ;
			exit( Interaction::EXIT_INTERNAL_ERROR );
		}
	if( Interaction::theInMemorySources.size( ) <= st_ino_ )
		{
			Interaction::theInMemorySources.resize( st_ino_ );
		}
	Interaction::theInMemorySources[ st_ino_ ] = data;
}

int
Ast::FileID::inMemoryIndex( ) const
{
	if( kind_ != IN_MEMORY )
		{
			std::cerr << "Internal error: FileID::inMemoryIndex is only applicable to IN_MEMORY objects." << std::endl ;
			exit( Interaction::EXIT_INTERNAL_ERROR );
		}
	return static_cast< int >( st_ino_ );
}

const Ast::FileID *
Ast::FileID::build_fresh_inMemory( )
{
	std::ostringstream fullName;
	ino_t index = Interaction::theInMemorySources.size( );
	fullName << INPUTLIST_PREFIX << index ;
	Interaction::theInMemorySources.push_back( 0 );
	return Ast::FileID::build_inMemory( index, fullName.str( ) );
}

size_t
Ast::FileID::inputlistPrefixLength( )
{
	return strlen( INPUTLIST_PREFIX );
}

Ast::SourceLocation::SourceLocation( )
	: file_( Ast::FileID::build_internal( "*bison-uninitialized*" ) ), firstLine( 1 ), firstColumn( 0 ), lastLine( 1 ), lastColumn( 0 )
{ }

Ast::SourceLocation::SourceLocation( const Ast::FileID * file )
	: file_( file ), firstLine( 1 ), firstColumn( 0 ), lastLine( 1 ), lastColumn( 0 )
{ }

Ast::SourceLocation::SourceLocation( const Ast::SourceLocation & firstLoc, const Ast::SourceLocation & lastLoc )
	: file_( firstLoc.file_ ), firstLine( firstLoc.firstLine ), firstColumn( firstLoc.firstColumn ), lastLine( lastLoc.lastLine ), lastColumn( lastLoc.lastColumn )
{ }

Ast::SourceLocation::SourceLocation( const Ast::SourceLocation & orig, bool dummy )
	: file_( orig.file_ ), firstLine( orig.firstLine ), firstColumn( orig.firstColumn ), lastLine( orig.lastLine ), lastColumn( orig.lastColumn )
{ }

Ast::SourceLocation &
Ast::SourceLocation::operator = ( const Ast::SourceLocation & orig )
{
	file_ = orig.file_;
	firstLine = orig.firstLine;
	firstColumn = orig.firstColumn;
	lastLine = orig.lastLine;
	lastColumn = orig.lastColumn;
	return *this;
}

void
Ast::SourceLocation::swap( SourceLocation * other )
{
  SourceLocation tmp( *this, bool( ) );
  *this = *other;
  *other = tmp;
}

bool
Ast::SourceLocation::isUnknown( ) const
{
	return file_ == SourceLocation::UNKNOWN_FILE;
}

bool
Ast::SourceLocation::contains( const Ast::SourceLocation & loc2 ) const
{
	return
		file_ == loc2.file_
		&&
		( firstLine < loc2.firstLine ||
			( firstLine == loc2.firstLine && firstColumn <= loc2.firstColumn ) )
		&&
		( lastLine > loc2.lastLine ||
			( lastLine == loc2.lastLine && lastColumn >= loc2.lastColumn ) )
		;
}

std::ostream &
Ast::operator << ( std::ostream & os, const Ast::SourceLocation & self )
{
	if( self.file_ == 0 )
		{
			os << "< unknown location >" ;
			return os;
		}

	os << self.file_->name( ) ;
	if( ! self.file_->hasPosition( ) )
		{
			return os;
		}
	os << ":" ;
	if( Interaction::characterColumnInBytes )
		{
			if( self.firstLine == self.lastLine )
				{
					os << self.firstLine << "(" << self.firstColumn << "-" << self.lastColumn << ")" ;
				}
			else
				{
					os << self.firstLine << "(" << self.firstColumn << ")-" << self.lastLine << "(" << self.lastColumn << ")" ;
				}
		}
	else
		{
			if( self.firstLine == self.lastLine )
				{
					os << self.firstLine << "(" ;
					{
						size_t col = self.file_->byteColumnToUTF8Column( self.firstLine, self.firstColumn );
						if( col != std::numeric_limits< size_t >::max( ) )
							{
								os << col ;
							}
						else
							{
								os << "?" ;
							}
					}
					os << "-" ;
					{
						size_t col = self.file_->byteColumnToUTF8Column( self.lastLine, self.lastColumn );
						if( col != std::numeric_limits< size_t >::max( ) )
							{
								os << col ;
							}
						else
							{
								os << "?" ;
							}
					}
					os << ")" ;
				}
			else
				{
					os << self.firstLine << "(" ;
					{
						size_t col = self.file_->byteColumnToUTF8Column( self.firstLine, self.firstColumn );
						if( col != std::numeric_limits< size_t >::max( ) )
							{
								os << col ;
							}
						else
							{
								os << "?" ;
							}
					}
					os << ")-"
						 << self.lastLine << "(" ;
					{
						size_t col = self.file_->byteColumnToUTF8Column( self.lastLine, self.lastColumn );
						if( col != std::numeric_limits< size_t >::max( ) )
							{
								os << col ;
							}
						else
							{
								os << "?" ;
							}
					}
					os << ")" ;
				}
		}

	return os;
}

void
Ast::SourceLocation::copy( std::ostream * os ) const
{
	std::ifstream iFile;
	std::istringstream iss;
	std::istream * is = 0;
	try
		{
			is = file_->open( & iFile, & iss );
		}
	catch( const Exceptions::FileID_OpenError & ball )
		{
			*os << "[UNABLE-TO-COPY-SOURCE]" ;
			return;
		}

	size_t line = 1;
	for( ; line < firstLine; ++line )
		{
			is->ignore( std::numeric_limits< std::streamsize >::max( ), '\n' );
		}
	is->ignore( firstColumn, '\0' );
	std::string tmpLine;
	size_t col1 = firstColumn;
	for( ; line < lastLine; ++line )
		{
			getline( *is, tmpLine );
			*os << tmpLine << std::endl ;
			col1 = 0;
		}
	getline( *is, tmpLine );
	*os << tmpLine.substr( 0, lastColumn - col1 ) ;
}

Ast::Expression *
Ast::SourceLocation::findExpression( ) const
{
	typedef typeof file_->nodes( ) ListType;
	ListType & exprs = file_->nodes( );
	for( ListType::iterator i = exprs.begin( ); i != exprs.end( ); ++i )
		{
			Ast::Expression * tmp = (*i)->findExpressionSameFile( *this );
			if( tmp != 0 )
				{
					return tmp;
				}
		}
	return 0;
}

size_t
Ast::FileID::byteColumnToUTF8Column( size_t line, size_t byteCol ) const
{
	static const Ast::FileID * fileInCache = 0;
	static size_t lineInCache = 0;
	static std::string cachedLine;

	if( byteCol == 0 )
		{
			return 0;
		}

	if( this != fileInCache ||
			line != lineInCache )
		{
			fileInCache = this;
			std::ifstream iFile;
			std::istringstream iss;
			std::istream * is = open( & iFile, & iss );
			for( lineInCache = 1; lineInCache < line; ++lineInCache )
				{
					is->ignore( std::numeric_limits< std::streamsize >::max( ), '\n' );
				}
			if( is->eof( ) )
				{
					std::ostringstream msg;
					msg << "Error in error message: Source location's line (" << line << ") is way beyond end of file: " << name( ) ;
					Kernel::thePostCheckErrorsList.push_back( new Exceptions::InternalError( msg ) );
					return 0;
				}
			getline( *is, cachedLine );
			/* is->eof( ) is acceptable here, since it "just" means that there was no newline
			 * terminating the last line in the file.
			 */
//			 if( is->eof( ) )
//				 {
//					std::ostringstream msg;
//					msg << "Error in error message: Source location's line (" << line << ") is one beyond end of file: " << filename ;
//					Kernel::thePostCheckErrorsList.push_back( new Exceptions::InternalError( msg );
//					 return 0;
//				 }
		}

	return byteColumnToUTF8Column( cachedLine, byteCol );
}

size_t
Ast::FileID::byteColumnToUTF8Column( const std::string & line, size_t byteCol )
{
	size_t count = 0;
	const char * end = line.c_str( ) + byteCol;
	for( const char * src = line.c_str( ); src != end && *src != '\0'; ++src )
		{
			if( Helpers::utf8leadByte( *src ) )
				{
					++count;
				}
		}
	return count;
}

size_t
Ast::FileID::UTF8ColumnTobyteColumn( size_t line, size_t utf8Col ) const
{
	std::ifstream iFile;
	std::istringstream iss;
	std::istream * is = open( & iFile, & iss );
	for( size_t i = 1; i < line; ++i )
		{
			is->ignore( std::numeric_limits< std::streamsize >::max( ), '\n' );
		}
	if( is->eof( ) )
		{
			throw Exceptions::OutOfRange( "Line number is past end of file." );
		}
	std::string lineData;
	getline( *is, lineData );
	return UTF8ColumnTobyteColumn( lineData, utf8Col );
}

size_t
Ast::FileID::UTF8ColumnTobyteColumn( const std::string & line, size_t utf8Col )
{
	const char * src = line.c_str( );
	for( ; utf8Col > 0 && *src != '\0'; ++src )
		{
			if( Helpers::utf8leadByte( *src ) )
				{
					--utf8Col;
				}
		}
	return src - line.c_str( );
}


Ast::SourceLocationFactory::SourceLocationFactory( )
{ }

Ast::SourceLocationFactory::~SourceLocationFactory( )
{
	while( ! mem_.empty( ) )
		{
			delete mem_.back( );
			mem_.pop_back( );
		}
}

const Ast::SourceLocation &
Ast::SourceLocationFactory::construct( const FileID * file )
{
	Ast::SourceLocation * loc = new Ast::SourceLocation( file );
	mem_.push_back( loc );
	return *loc;
}

const Ast::SourceLocation &
Ast::SourceLocationFactory::construct_internal( const char * locationName )
{
	Ast::SourceLocation * loc = new Ast::SourceLocation( Ast::FileID::build_internal( locationName ) );
	mem_.push_back( loc );
	return *loc;
}
