/* This file is part of Shapes.
 *
 * Shapes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Shapes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2009 Henrik Tidefelt
 */

#pragma once

#include <iostream>
#include <gsl/gsl_matrix.h>

namespace Shapes
{

	namespace Computation
	{

		typedef enum { QP_OK, QP_WARNING, QP_ERROR, QP_FAIL, QP_BAD } QPSolverExit;
		typedef enum { QP_OK_LM, QP_OK_GAP_ABS, QP_OK_GAP_REL, QP_OK_UPPER, QP_OK_LOWER } QPSolverTermination;
		typedef enum { QP_WARNING_NEGATIVE_LM, QP_WARNING_EARLY_EXIT } QPSolverWarning;
		typedef enum { QP_ERROR_UNDEFINED, QP_ERROR_ALLOCATE,
									 QP_ERROR_DIMENSIONS_X, QP_ERROR_DIMENSIONS_LM } QPSolverError;
		typedef enum { QP_FAIL_ITER } QPSolverFailure;
		typedef enum { QP_BAD_INCONSISTENT_EQ, QP_BAD_INCONSISTENT_INEQ, QP_BAD_INCONSISTENT_BOX, QP_BAD_INCONSISTENT_ACTIVE,
									 QP_BAD_POSITIVE_DEFINITE, QP_BAD_LINEAR_DEPENDENT,
									 QP_BAD_INITIAL_INFEASIBLE } QPSolverBadProblem;
		struct QPSolverStatus
		{
			QPSolverExit code;
			union
			{
				QPSolverTermination termination;
				QPSolverWarning warning;
				QPSolverError error;
				QPSolverFailure failure;
				QPSolverBadProblem bad;
			};
			int param; /* Parameter that can be used to give more detaild information. */
			size_t iterations; /* Number of iterations used. */
			const char * code_str( ) const;
			const char * sub_str( ) const;
		};

		void polytope_distance_generator_form( const size_t dim,
																					 /* Generators are stored with generator <i> occupying (start+dim*i) and the following (dim-1) positions. */
																					 const size_t n1, const double * g1, /* Generators for first polytope.*/
																					 const size_t n2, const double * g2, /* Generators for second polytope. */
																					 double * costLower, /* Pass 0 if lower bound shall not be computed. */
																					 double * costUpper, /* Returned bounds on the suared distance. */
																					 double stopLower, /* Stop if *costLower becomes lower than this value. Only used if positive and costLower != 0. */
																					 double stopUpper, /* Stop if *costUpper becomes lower than this value.  Only used if positive and costUpper != 0. */
																					 double gapTolAbs, /* Stop if ( *costUpper - *costLower ) < galTolAbs.  Only used if positive and costLower != 0 and costUpper != 0. */
																					 double gapTolRel, /* Stop if ( *costUpper - *costLower ) < galTolRel * (*costUpper).  Only used if positive and costLower != 0 and costUpper != 0. */
																					 double lmTol, /* Lagrange multiplier tolerance (for sign tests), use 0 for a default of 1e-14.
																													* Note that this tolerance may be interpreted as a gap tolerance which is linear in the distance,
																													* not distance squared.
																													*/
																					 double xTol, /* Tolerance to be compared with squared norm of primal step, use 0 for a default of 1e-8. */
																					 double sigmaTol, /* Truncate singular values below sigmaTol times max(sigma), use 0 for 1e-3. */
																					 double * y1, double * y2, /* Optimal point in each polytope.  Pass 0 unless needed! */
																					 QPSolverStatus * status );

		void polytope_distance_generator_form_write_data( std::ostream & os,
																											const size_t dim,
																											const size_t n1, const double * g1,
																											const size_t n2, const double * g2 );
		void polytope_distance_generator_form_write_binary_data( const char * oFilename,
																														 const size_t dim,
																														 const size_t n1, const double * g1,
																														 const size_t n2, const double * g2 );

	}

}
