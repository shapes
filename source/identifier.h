/* This file is part of Shapes.
 *
 * Shapes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Shapes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2014 Henrik Tidefelt
 */

#pragma once

#include "refcount.h"
#include "charptrless.h"
#include "Shapes_Ast_decls.h"

#include <list>
#include <map>
#include <iostream>

namespace Shapes
{
  namespace Ast
  {

    /* Memory management notice: The const char * strings used in NamespacePath, Identifier, and PlacedIdentifier
     * will not be deallocated when no longer in use.  This is by design, since the number of such strings should be remain
     * fixed once the AST has been built up.  Duplication of strings is avoided to some extent by the reference-counted
     * NamespacePath objects.
     *
     * If memory leaking turns out to be a problem, one should consider replacing const char * by std::string or
     * RefCountPtr< const char * >.
     */

    typedef std::list< const char * > NamespacePath;
    extern const RefCountPtr< const Ast::NamespacePath > THE_EMPTY_NAMESPACE_PATH;

    int compare( const Ast::NamespacePath & path1, const Ast::NamespacePath & path2 );
//  bool operator < ( const Ast::NamespacePath & path1, const Ast::NamespacePath & path2 );

    class NamespaceReference
    {
    public:
      enum Base { ABSOLUTE, LOCAL, RELATIVE };
    private:
      Base base_;
      RefCountPtr< const Ast::NamespacePath > path_;
    public:
      NamespaceReference( )
      : base_( ABSOLUTE ), path_( NullPtr< const Ast::NamespacePath >( ) )
      { }
      NamespaceReference( Base base, const RefCountPtr< const Ast::NamespacePath > & path )
      : base_( base ), path_( path )
      { }
      NamespaceReference( Base base, Ast::NamespacePath * path ) /* Transfers ownership. */
      : base_( base ), path_( path )
      { }
      Base base( ) const { return base_; }
      const Ast::NamespacePath & path( ) const { return *path_; }
      RefCountPtr< const Ast::NamespacePath > pathRef( ) const { return path_; }
      void show( std::ostream & os, bool showAbsoluteMark = true ) const;
    };

    /* A LookinPathStack holds the namespaces that have been explicitly listed to be looked in when looking up identifiers.
     * The stack is to be traversed from top to bottom when looking up identifiers.
     */
    class LookinPathStack
    {
      RefCountPtr< const Ast::NamespacePath > car_; /* Resolved at the point where the look-in is added. */
      RefCountPtr< const LookinPathStack > cdr_;
    public:
      LookinPathStack( const RefCountPtr< const Ast::NamespacePath > & car, const RefCountPtr< const LookinPathStack > & cdr )
        : car_( car ), cdr_( cdr )
      { }
      RefCountPtr< const Ast::NamespacePath > car( ) const { return car_; }
      RefCountPtr< const LookinPathStack > cdr( ) const { return cdr_; }
    };

    class SearchContext
    {
      RefCountPtr< const NamespacePath > lexicalPath_;
      RefCountPtr< const NamespacePath > encapsulationPath_;
      RefCountPtr< const LookinPathStack > lookinStack_;
      RefCountPtr< const char > privateName_;
    public:
      SearchContext( const RefCountPtr< const NamespacePath > & lexicalPath, const RefCountPtr< const NamespacePath > & encapsulationPath, const RefCountPtr< const LookinPathStack > & lookinStack, const RefCountPtr< const char > & privateName )
      : lexicalPath_( lexicalPath ), encapsulationPath_( encapsulationPath ), lookinStack_( lookinStack ), privateName_( privateName )
      { }
      SearchContext( const RefCountPtr< const NamespacePath > & lexicalPath, const RefCountPtr< const char > & privateName )
      : lexicalPath_( lexicalPath ), encapsulationPath_( Ast::THE_EMPTY_NAMESPACE_PATH ), lookinStack_( NullPtr< const LookinPathStack >( ) ), privateName_( privateName )
      { }
      const RefCountPtr< const NamespacePath > & lexicalPath( ) const { return lexicalPath_; }
      const RefCountPtr< const NamespacePath > & encapsulationPath( ) const { return encapsulationPath_; }
      const RefCountPtr< const LookinPathStack > & lookinStack( ) const { return lookinStack_; }
      const RefCountPtr< const char > & privateName( ) const { return privateName_; }
      bool insidePrivateNamespace( ) const { return privateName_ == NullPtr< const char >( ); }
      bool insideEncapsulationNamespace( ) const;

      static RefCountPtr< const char > makePrivateName( size_t i );
      static RefCountPtr< const char > makeEncapsulationName( size_t i );
      static bool isEncapsulationName( const char * name );
    };

    extern const RefCountPtr< const Ast::SearchContext > THE_EMPTY_SEARCH_CONTEXT;

    class PlacedIdentifier;

    class Identifier
    {
    public:
      /* The type of identifier is not stored in the identifier, as that would have a substantial impact on the
       * memory consumption considering the uniquitous use of identifiers in a program.  The identifier type only
       * matters when displaying the identifier back to the user, and then the type has to be provided by the caller.
       */
      enum Type{ VARIABLE, STATE, DYNAMIC_VARIABLE, DYNAMIC_STATE, TYPE };

    private:
      RefCountPtr< const SearchContext > searchContext_; /* Used also for absolute identifiers, in case they use a namespace alias that expands to a relative path. */
      NamespaceReference namespaceRef_;
      const char * simpleId_; /* Will not be deallocated, see memory management note above! */

    public:
      Identifier( const RefCountPtr< const SearchContext > & searchContext, Ast::NamespaceReference::Base base, NamespacePath * path, const char * simpleId ); /* Transfers ownership of path. */
      Identifier( const RefCountPtr< const SearchContext > & searchContext, Ast::NamespaceReference::Base base, const RefCountPtr< const Ast::NamespacePath > & path, const char * simpleId );
      Identifier( const RefCountPtr< const SearchContext > & searchContext, const NamespaceReference & namespaceRef, const char * simpleId );
      Identifier( const Identifier & orig );
      ~Identifier( );

      const SearchContext & searchContext( ) const { return *searchContext_; }
      NamespaceReference::Base base( ) const { return namespaceRef_.base( ); }
      const NamespacePath & path( ) const { return namespaceRef_.path( ); }
      const char * simpleId( ) const { return simpleId_; }

      /* Be careful when using the following destructive conversion functions!
       * *this will be unusable after calling these, and should be destroyed.
       */
      Ast::PlacedIdentifier * place( const Ast::SourceLocation & loc ); /* Reinterprets the identifier as a placed (LHS) identifier.  Always returns a new PlacedIdentifier, but will report an error if the conversion is unnatural. */
      const char * strip( const Ast::SourceLocation & loc ); /* Extracts the simple identifier. */
      RefCountPtr< const char > refstrip( const Ast::SourceLocation & loc ); /* Warning!  This will pass ownership to the reference counted result.  Must not be used if there exist other references to the simpleId_ memory! */

      void show( std::ostream & os, Ast::Identifier::Type type, bool showAbsoluteMark = true ) const;
      void debugShow( std::ostream & os ) const;

      /* Warning!  Comparison only takes the explicit part of the identifier into account.  The search path
       * is not used in the comparison, but absolute paths will precede relative paths.
       */
      bool operator < ( const Identifier & other ) const;

      static const char * identifierPrefix( Type type );
    };

    class IdentifierPtrLess
    {
     public:
      bool operator () ( const Ast::Identifier * id1, const Ast::Identifier * id2 ) const
      {
        return *id1 < *id2;
      }
    };

    /* A placed identifier belongs to the current environment, expressed using only a simple identifier.
     * That is, it is an absolute identifier, but looks like a relative identifier.  Due to the risk
     * of initial misinterpretation of a simple identifier in the scanner, there are methods that the parser
     * can use for converting to the other possible types of token values.
     * It is typically the left hand side of a binding statement.
     */
    class PlacedIdentifier
    {
      RefCountPtr< const NamespacePath > absolutePath_;
      const char * simpleId_; /* Will not be deallocated, see memory management note above! */
    public:
      PlacedIdentifier( const RefCountPtr< const NamespacePath > & absolutePath, const char * simpleId );
      PlacedIdentifier( NamespacePath * absolutePath, const char * simpleId ); /* Transfers ownership of absolutePath. */
      PlacedIdentifier( const NamespacePath & absolutePath, const char * simpleId );
      PlacedIdentifier( const PlacedIdentifier & orig );
      ~PlacedIdentifier( );

      const NamespacePath & absolutePath( ) const { return *absolutePath_; }
      const RefCountPtr< const NamespacePath > & absolutePathRef( ) const { return absolutePath_; }
      const char * simpleId( ) const { return simpleId_; }

      Ast::PlacedIdentifier * clone( ) const;
      Ast::Identifier * newAbsolute( ) const;

      /* Be careful when using the following destructive conversion functions!
       * *this will be unusable after calling these, and should be destroyed.
       */
      const char * strip( ); /* Extracts the simple identifier. */
      RefCountPtr< const char > refstrip( ); /* Warning!  This will pass ownership to the reference counted result.  Must not be used if there exist other references to the simpleId_ memory! */

      /* Since it should be clear from the context that this is a placed identifier, a leading "::" is _not_ shown to reduce clutter.
       */
      void show( std::ostream & os, Ast::Identifier::Type type ) const;

      bool operator < ( const PlacedIdentifier & other ) const;
    };

    class PlacedIdentifierPtrLess
    {
     public:
      bool operator () ( const Ast::PlacedIdentifier * id1, const Ast::PlacedIdentifier * id2 ) const
      {
        return *id1 < *id2;
      }
    };

  }
}
