/* This file is part of Shapes.
 *
 * Shapes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Shapes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2009, 2010 Henrik Tidefelt
 */

#include "Shapes_Helpers_decls.h"

#include <iostream>


using namespace Shapes;


namespace Shapes
{
	namespace Helpers
	{

		class RasterLoader
		{
		public:
			virtual ~RasterLoader( ) { }
			virtual RefCountPtr< const Lang::RasterImage > load( RefCountPtr< std::ifstream > & file, const std::string & full_filename, Concrete::Length resolution, bool override, const Ast::PlacedIdentifier & coreId, const Ast::SourceLocation & callLoc ) const = 0;
		};

		class RasterLoader_PNG : public RasterLoader
		{
		public:
			virtual ~RasterLoader_PNG( ) { }
			virtual RefCountPtr< const Lang::RasterImage > load( RefCountPtr< std::ifstream > & file, const std::string & full_filename, Concrete::Length resolution, bool override, const Ast::PlacedIdentifier & coreId, const Ast::SourceLocation & callLoc ) const;
		};

		class RasterLoader_JPEG : public RasterLoader
		{
		public:
			virtual ~RasterLoader_JPEG( ) { }
			virtual RefCountPtr< const Lang::RasterImage > load( RefCountPtr< std::ifstream > & file, const std::string & full_filename, Concrete::Length resolution, bool override, const Ast::PlacedIdentifier & coreId, const Ast::SourceLocation & callLoc ) const;
		};

	}
}
