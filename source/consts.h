/* This file is part of Shapes.
 *
 * Shapes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Shapes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2008, 2014, 2015 Henrik Tidefelt
 */

#pragma once

#include <cmath>

#include "elementarylength.h"
#include "identifier.h"

#include "refcount.h"
#include <cstddef> /* For size_t. */



namespace Shapes
{
	namespace Interaction
	{

		extern RefCountPtr< const char > SEVERAL_TYPES;
		extern RefCountPtr< const char > PUBLIC_SCOPE_NAME;
		extern RefCountPtr< const char > PROTECTED_SCOPE_NAME;

		extern const char NAMESPACE_SEPARATOR[];
		extern const char ENCAPSULATION_MARK[];
		extern const char DYNAMIC_VARIABLE_PREFIX[];
		extern const char STATE_PREFIX[];
		extern const char DYNAMIC_STATE_PREFIX[];
		extern const char BUILD_REQ_LIBJPEG[];
		extern const char BUILD_REQ_LIBPNG[];
		extern const char BUILD_REQ_FREETYPE[];
		extern const char BUILD_REQ_FONTCONFIG[];
		extern const char BUILD_REQ_OPENSSL[];
	}

	namespace Lang
	{

		extern const RefCountPtr< const Ast::NamespacePath > THE_NAMESPACE_Shapes;
		extern const RefCountPtr< const Ast::NamespacePath > THE_NAMESPACE_Shapes_Data;
		extern const RefCountPtr< const Ast::NamespacePath > THE_NAMESPACE_Shapes_Data_Type;
		extern const RefCountPtr< const Ast::NamespacePath > THE_NAMESPACE_Shapes_Debug;
		extern const RefCountPtr< const Ast::NamespacePath > THE_NAMESPACE_Shapes_Geometry;
		extern const RefCountPtr< const Ast::NamespacePath > THE_NAMESPACE_Shapes_Geometry3D;
		extern const RefCountPtr< const Ast::NamespacePath > THE_NAMESPACE_Shapes_Graphics;
		extern const RefCountPtr< const Ast::NamespacePath > THE_NAMESPACE_Shapes_Graphics_PDF;
		extern const RefCountPtr< const Ast::NamespacePath > THE_NAMESPACE_Shapes_Graphics_Tag;
		extern const RefCountPtr< const Ast::NamespacePath > THE_NAMESPACE_Shapes_Graphics3D;
		extern const RefCountPtr< const Ast::NamespacePath > THE_NAMESPACE_Shapes_Graphics3D_Tag;
		extern const RefCountPtr< const Ast::NamespacePath > THE_NAMESPACE_Shapes_IO;
		extern const RefCountPtr< const Ast::NamespacePath > THE_NAMESPACE_Shapes_Layout;
		extern const RefCountPtr< const Ast::NamespacePath > THE_NAMESPACE_Shapes_Numeric;
		extern const RefCountPtr< const Ast::NamespacePath > THE_NAMESPACE_Shapes_Numeric_Constant;
		extern const RefCountPtr< const Ast::NamespacePath > THE_NAMESPACE_Shapes_Numeric_Math;
		extern const RefCountPtr< const Ast::NamespacePath > THE_NAMESPACE_Shapes_Numeric_Random;
		extern const RefCountPtr< const Ast::NamespacePath > THE_NAMESPACE_Shapes_String;
		extern const RefCountPtr< const Ast::NamespacePath > THE_NAMESPACE_Shapes_Text;
		extern const RefCountPtr< const Ast::NamespacePath > THE_NAMESPACE_Shapes_Text_Font;
		extern const RefCountPtr< const Ast::NamespacePath > THE_NAMESPACE_Shapes_Traits;
		extern const RefCountPtr< const Ast::NamespacePath > THE_NAMESPACE_Shapes_Traits_BW;
		extern const RefCountPtr< const Ast::NamespacePath > THE_NAMESPACE_Shapes_Traits_Blend;
		extern const RefCountPtr< const Ast::NamespacePath > THE_NAMESPACE_Shapes_Traits_Cap;
		extern const RefCountPtr< const Ast::NamespacePath > THE_NAMESPACE_Shapes_Traits_Device;
		extern const RefCountPtr< const Ast::NamespacePath > THE_NAMESPACE_Shapes_Traits_Join;
		extern const RefCountPtr< const Ast::NamespacePath > THE_NAMESPACE_Shapes_Traits_Light;
		extern const RefCountPtr< const Ast::NamespacePath > THE_NAMESPACE_Shapes_Traits_RGB;

		extern const char TEX_SYNTAX_ID[];
		extern const Ast::Identifier SELF_ID;

		extern const char MESSAGE_DRAWABLE_DRAW_ID[];

		extern const char CONTINUATION_ID_ERROR[];

		extern const Ast::PlacedIdentifier HANDLER_NO_INTERSECTION;

		extern const Ast::PlacedIdentifier CANVAS_ID;
		extern const Ast::PlacedIdentifier CATALOG_ID;

		extern const Ast::PlacedIdentifier DYNAMIC_VARIABLE_ID_EYEZ;
		extern const Ast::PlacedIdentifier DYNAMIC_VARIABLE_ID_TEX_BLEED;
		extern const Ast::PlacedIdentifier DYNAMIC_VARIABLE_ID_DEFAULT_UNIT;
		extern const Ast::PlacedIdentifier DYNAMIC_VARIABLE_ID_BLEND_SPACE;
		extern const Ast::PlacedIdentifier DYNAMIC_VARIABLE_ID_STROKING;
		extern const Ast::PlacedIdentifier DYNAMIC_VARIABLE_ID_WIDTH;
		extern const Ast::PlacedIdentifier DYNAMIC_VARIABLE_ID_NONSTROKING;
		extern const Ast::PlacedIdentifier DYNAMIC_VARIABLE_ID_AUTOINTENSITY;

		extern const Ast::PlacedIdentifier DYNAMIC_VARIABLE_ID_TEXT_SIZE;
		extern const Ast::PlacedIdentifier DYNAMIC_VARIABLE_ID_TEXT_FONT;

	}

	namespace Kernel
	{

		extern const char SPLIT_VAR_PREFIX[];

		extern const char HTML_DIR[];
		extern const char RESOURCES_DIR[];

		extern const Ast::PlacedIdentifier SEQUENTIAL_EXPR_VAR_ID;
		extern const Ast::PlacedIdentifier MUTATOR_CURRY_VAR_ID;

	}

	namespace Computation
	{

		extern const size_t RREL_SIZE;
		extern const double RREL_TH_STEP;
		extern const double RREL_TABLE[];

		extern const double SINGULAR_TRANSFORM_LIMIT;

	}

	namespace Ast
	{

		typedef unsigned short int MemberMode;
		extern const MemberMode MEMBER_ACCESS_BITS;
		extern const MemberMode MEMBER_ACCESS_PRIVATE;
		extern const MemberMode MEMBER_ACCESS_PUBLIC_GET;
		extern const MemberMode MEMBER_ACCESS_PUBLIC_INSERT;
		extern const MemberMode MEMBER_ACCESS_PROTECTED_GET;
		extern const MemberMode MEMBER_ACCESS_PROTECTED_INSERT;
		extern const MemberMode MEMBER_CONST;
		extern const MemberMode MEMBER_METHOD;
		extern const MemberMode MEMBER_ABSTRACT;
		extern const MemberMode MEMBER_FINAL;
		extern const MemberMode MEMBER_TRANSFORMING;

		typedef unsigned short int ClassMode;
		extern const ClassMode CLASS_MODE_ABSTRACT;
		extern const ClassMode CLASS_MODE_FINAL;

		typedef unsigned short int FunctionMode;
		extern const FunctionMode FUNCTION_TRANSFORMING;

	}

	namespace Concrete
	{

		const Length HUGE_LENGTH( HUGE_VAL );
		const Length ZERO_LENGTH( 0 );
		const Length SOME_LENGTH( 1 );

		const Speed ZERO_SPEED( 0 );

		const Time ZERO_TIME( 0 );
		const Time UNIT_TIME( 1 );
		const Time HUGE_TIME( HUGE_VAL );

	}

	namespace BuiltInFonts
	{

		extern RefCountPtr< const char > TIMES_ROMAN;
		extern RefCountPtr< const char > TIMES_BOLD;
		extern RefCountPtr< const char > TIMES_ITALIC;
		extern RefCountPtr< const char > TIMES_BOLDITALIC;
		extern RefCountPtr< const char > HELVETICA;
		extern RefCountPtr< const char > HELVETICA_BOLD;
		extern RefCountPtr< const char > HELVETICA_OBLIQUE;
		extern RefCountPtr< const char > HELVETICA_BOLDOBLIQUE;
		extern RefCountPtr< const char > COURIER;
		extern RefCountPtr< const char > COURIER_BOLD;
		extern RefCountPtr< const char > COURIER_OBLIQUE;
		extern RefCountPtr< const char > COURIER_BOLDOBLIQUE;
		extern RefCountPtr< const char > SYMBOL;
		extern RefCountPtr< const char > ZAPFDINGBATS;
		extern RefCountPtr< const char > NUM_BUILTIN_FONTS;

	}
}
