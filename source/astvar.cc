/* This file is part of Shapes.
 *
 * Shapes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Shapes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2008, 2013, 2014, 2015 Henrik Tidefelt
 */

#include "astvar.h"
#include "shapesexceptions.h"
#include "globals.h"
#include "autoonoff.h"
#include "specialunits.h"
#include "astfun.h"
#include "continuations.h"
#include "containertypes.h"
#include "check.h"

#include <algorithm>

using namespace Shapes;
using namespace std;


Ast::SourceLocationMark::SourceLocationMark( const Ast::SourceLocation & loc )
	: Ast::Node( loc )
{ }

Ast::SourceLocationMark::~SourceLocationMark( )
{ }

void
Ast::SourceLocationMark::analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst )
{ }

namespace Shapes
{
	namespace Helpers
	{
		class IsSourceLocationMark
		{
		public:
			bool operator () ( const Ast::Node * n ) const
			{
				return dynamic_cast< const Ast::SourceLocationMark * >( n ) != 0;
			}
		};
	}
}


Ast::CodeBracket::CodeBracket( const Ast::SourceLocation & loc, std::list< Ast::Node * > * nodes, Ast::CodeBracket * extends )
	: Ast::Expression( loc ), nodes_( nodes ), extends_( extends ),
		argumentOrder_( ( extends == 0 ) ? ( new Ast::IdentifierTree( NULL, false ) ) : ( extends->argumentOrder_ ) ),
		dynamicMap_( ( extends == 0 ) ? ( 0 ) : ( extends->dynamicMap_ ) ),
		stateOrder_( ( extends == 0 ) ? ( new Ast::IdentifierTree( NULL, false ) ) : ( extends->stateOrder_ ) )
{
	/* First, we remove any source location marks -- we don't need them anymore. */
	nodes_->remove_if( Helpers::IsSourceLocationMark( ) );

	/* Alias definitions will be extracted and applied in this function. */
	PtrOwner_back_Access< std::list< const Ast::DefineAlias * > > aliasNodes;

	for( std::list< Ast::Node * >::const_iterator i = nodes_->begin( );
			 i != nodes_->end( );
			 )
		{
			typedef Ast::BindNode T;
			if( T * tmp = dynamic_cast< T * >( *i ) )
				{
					bindNodes_.push_back( tmp );

					const Ast::PlacedIdentifier * id = tmp->id( );
					if( dynamic_cast< const Ast::DefineVariable * >( tmp ) != NULL )
						{
							if( ! argumentOrder_->insert( *id, tmp->idLoc( ) ) ){
								Ast::theAnalysisErrorsList.push_back( new Exceptions::IntroducingExisting( tmp->idLoc( ), *id ) );
							}
						++i;
							continue;
						}
					if( dynamic_cast< const Ast::IntroduceState * >( tmp ) != NULL )
						{
							if( ! stateOrder_->insert( *id, tmp->idLoc( ) ) ){
								Ast::theAnalysisErrorsList.push_back( new Exceptions::IntroducingExisting( tmp->idLoc( ), *id ) );
							}
						++i;
							continue;
						}
					if( dynamic_cast< const Ast::DynamicVariableDecl * >( tmp ) != NULL )
						{
							if( dynamicMap_ == 0 )
								{
									setDynamicMap( new Ast::IdentifierTree( NULL, false ) ); /* Sending the new map to extends_ recursively is important for memory management. */
								}
							if( ! dynamicMap_->insert( *id, tmp->idLoc( ) ) ){
								Ast::theAnalysisErrorsList.push_back( new Exceptions::IntroducingExisting( tmp->idLoc( ), *id ) );
							}
						++i;
							continue;
						}
				}
  		if( const Ast::DefineAlias * defineAliasNode = dynamic_cast< const Ast::DefineAlias * >( *i ) )
  			{
  				aliasNodes.push_back( defineAliasNode );
  				std::list< Ast::Node * >::const_iterator j = i;
  				++i;
  				nodes_->erase( j );
  				continue;
  			}
			++i;
		}

  for( std::list< const Ast::DefineAlias * >::const_iterator i = aliasNodes.begin( );
  		 i != aliasNodes.end( );
  		 ++i )
  	{
  		bool resolved = false;
  		resolved = resolved || argumentOrder_->insertAlias( *((*i)->id( )), (*i)->expansion( ), (*i)->idLoc( ) );
  		resolved = resolved || stateOrder_->insertAlias( *((*i)->id( )), (*i)->expansion( ), (*i)->idLoc( ) );
  		if( dynamicMap_ != NULL ){
  			resolved = resolved || dynamicMap_->insertAlias( *((*i)->id( )), (*i)->expansion( ), (*i)->idLoc( ) );
  		}
  		if( ! resolved ){
  			Interaction::warn_or_push( new Exceptions::UndefinedNamespace( (*i)->expansionLoc( ), (*i)->expansion( ) ), & Ast::theAnalysisErrorsList );
  		}
  	}
}

void
Ast::CodeBracket::setDynamicMap( Ast::IdentifierTree * dynamicMap )
{
	dynamicMap_ = dynamicMap;
	if( extends_ != 0 )
		{
			extends_->setDynamicMap( dynamicMap );
		}
}

Ast::CodeBracket::~CodeBracket( )
{
	typedef list< Ast::Node * >::iterator I;
	for( I i = nodes_->begin( ); i != nodes_->end( ); ++i )
		{
			delete *i;
		}
	delete nodes_;

	if( extends_ == 0 )
		{
			/* If extends_ != 0, this object does not own the maps. */
			delete argumentOrder_;
			if( dynamicMap_ != 0 )
				{
					delete dynamicMap_;
				}
			delete stateOrder_;
		}
}

void
Ast::CodeBracket::analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * parentEnv, Ast::StateIDSet * freeStatesDst )
{
	if( extends_ == 0 )
		{
			analysisEnv_ = new Ast::AnalysisEnvironment( Ast::theAnalysisEnvironmentList, parentEnv, argumentOrder_, stateOrder_ );
		}
	else
		{
			analysisEnv_ = extends_->analysisEnv_;
			analysisEnv_->updateBindings( );
		}

	if( dynamicMap_ != 0 )
		{
			analysisEnv_->setupDynamicKeyVariables( dynamicMap_ );
		}


	Ast::StateIDSet allFreeStates;
	Ast::StateIDSet introducedStates;

	{
		Ast::StateIDSet freeStates;
		typedef list< Ast::Node * >::iterator I;
		for( I i = nodes_->begin( ); i != nodes_->end( ); ++i )
			{
				freeStates.clear( );
				(*i)->analyze( this, analysisEnv_, & freeStates );
				if( ! freeStates.empty( ) )
					{
						typedef typeof freeStates SetType;
						for( SetType::const_iterator j = freeStates.begin( ); j != freeStates.end( ); ++j )
							{
								allFreeStates.insert( *j );
							}
					}
				else
					{
						Ast::Expression * expr = dynamic_cast< Ast::Expression * >( *i );
						if( expr != 0 && ! expr->immediate_ )
							{
								I tmp = i;
								++tmp;
								if( tmp != nodes_->end( ) )
									{
										Ast::theAnalysisErrorsList.push_back( new Exceptions::ExpectedImmediate( (*i)->loc( ) ) );
									}
							}
					}
				Ast::IntroduceState * introduceStateExpr = dynamic_cast< Ast::IntroduceState * >( *i );
				if( introduceStateExpr != 0 )
					{
						introducedStates.insert( introduceStateExpr->getStateID( ) );
					}
			}
	}

	if( ! introducedStates.empty( ) )
		{
			Ast::StateIDSet diff;
			std::set_difference( allFreeStates.begin( ), allFreeStates.end( ),
													 introducedStates.begin( ), introducedStates.end( ),
													 std::inserter( diff, diff.begin( ) ) );
			allFreeStates.swap( diff );
		}
	if( ! allFreeStates.empty( ) )
		{
			typedef typeof allFreeStates SetType;
			for( SetType::const_iterator j = allFreeStates.begin( ); j != allFreeStates.end( ); ++j )
				{
					freeStatesDst->insert( *j );
				}
		}
}

void
Ast::CodeBracket::eval( Kernel::EvalState * evalState ) const
{
	if( nodes_->begin( ) == nodes_->end( ) )
		{
			Kernel::ContRef cont = evalState->cont_;
			cont->takeValue( Lang::THE_VOID,
											 evalState );
			return;
		}

	evalState->env_ = newEnvironment( evalState->env_ );

	if( dynamicMap_ != 0 )
		{
			evalState->env_->setupDynamicKeyVariables( dynamicMap_ );
		}

	typedef typeof bindNodes_ ListType;
	for( ListType::const_iterator i = bindNodes_.begin( ); i != bindNodes_.end( ); ++i ){
		(*i)->initializeEnvironment( evalState->env_, evalState->dyn_ );
	}

	RefCountPtr< const Kernel::CodeBracketContInfo > info( new Kernel::CodeBracketContInfo( this, *evalState ) );

	evalAt( info, nodes_->begin( ), evalState );
}

Kernel::Environment *
Ast::CodeBracket::newEnvironment( Kernel::Environment * parent, const char * debugName ) const
{
	CHECK(
				if( extends_ != 0 )
					{
						throw Exceptions::InternalError( "Ast::CodeBracket::newEnvironment( ): extends_ != 0" );
					}
				);
	std::vector< Kernel::VariableHandle > * envValues = new std::vector< Kernel::VariableHandle >;
	envValues->reserve( argumentOrder_->size( ) );
	while( envValues->size( ) < argumentOrder_->size( ) )
		{
			envValues->push_back( NullPtr< Kernel::Variable >( ) );
		}

	std::vector< Kernel::StateHandle > * envStates = new std::vector< Kernel::StateHandle >;
	envStates->reserve( stateOrder_->size( ) );
	while( envStates->size( ) < stateOrder_->size( ) )
		{
			envStates->push_back( NullPtr< Kernel::State >( ) );
		}

	return new Kernel::Environment( Kernel::theEnvironmentList, parent, argumentOrder_, RefCountPtr< std::vector< Kernel::VariableHandle > >( envValues ), stateOrder_, RefCountPtr< std::vector< Kernel::StateHandle > >( envStates ), debugName );
}

void
Ast::CodeBracket::eval( Kernel::EvalState * evalState, Kernel::Environment * extendsEnv ) const
{
	if( nodes_->begin( ) == nodes_->end( ) )
		{
			Kernel::ContRef cont = evalState->cont_;
			cont->takeValue( Lang::THE_VOID,
											 evalState );
			return;
		}

	evalState->env_ = extendsEnv;
	if( dynamicMap_ != 0 )
		{
			evalState->env_->setupDynamicKeyVariables( dynamicMap_ );
		}
	evalState->env_->extendVectors( );

	RefCountPtr< const Kernel::CodeBracketContInfo > info( new Kernel::CodeBracketContInfo( this, *evalState ) );

	evalAt( info, nodes_->begin( ), evalState );
}

void
Ast::CodeBracket::evalAt( const RefCountPtr< const Kernel::CodeBracketContInfo > & info, const std::list< Ast::Node * >::const_iterator & pos, Kernel::EvalState * evalState ) const
{
  {
    std::list< Ast::Node * >::const_iterator next = pos;
    ++next;
    if( next == nodes_->end( ) ){
      evalState->cont_ = info->cont_;
    }else{
      evalState->cont_ = Kernel::ContRef( new Kernel::CodeBracketContinuation( (*pos)->loc( ), info, next ) );
    }
  }
  evalState->env_ = info->env_;
  evalState->dyn_ = info->dyn_;
  if( Ast::Expression * e = dynamic_cast< Ast::Expression * >( *pos ) ){
    evalState->expr_ = e;
    return;
  }
  if( const Ast::BindNode * bn = dynamic_cast< const Ast::BindNode * >( *pos ) ){
    bn->evalHelper( evalState );
    return;
  }
  throw Exceptions::InternalError( (*pos)->loc( ), "CodeBracket::evalAt: Node was neither Expression or BindNode." );
}


Kernel::CodeBracketContInfo::CodeBracketContInfo( const Ast::CodeBracket * bracketExpr, const Kernel::EvalState & evalState )
	: bracketExpr_( bracketExpr ), env_( evalState.env_ ), dyn_( evalState.dyn_ ), cont_( evalState.cont_ )
{ }

Kernel::CodeBracketContInfo::~CodeBracketContInfo( )
{ }

void
Kernel::CodeBracketContInfo::gcMark( Kernel::GCMarkedSet & marked )
{
	env_->gcMark( marked );
	dyn_->gcMark( marked );
	cont_->gcMark( marked );
}

Kernel::CodeBracketContinuation::CodeBracketContinuation( const Ast::SourceLocation & traceLoc, const RefCountPtr< const Kernel::CodeBracketContInfo > & info, const std::list< Ast::Node * >::const_iterator & pos )
	: Kernel::Continuation( traceLoc ), info_( info ), pos_( pos )
{ }

Kernel::CodeBracketContinuation::~CodeBracketContinuation( )
{ }

void
Kernel::CodeBracketContinuation::takeValue( const RefCountPtr< const Lang::Value > & val, Kernel::EvalState * evalState, bool dummy ) const
{
	if( val.down_cast< const Lang::Void >( ) == NullPtr< const Lang::Void >( ) )
		{
			throw Exceptions::NonVoidStatement( traceLoc_, val );
		}
	info_->bracketExpr_->evalAt( info_,
															 pos_,
															 evalState );
}

Kernel::ContRef
Kernel::CodeBracketContinuation::up( ) const
{
	return info_->cont_;
}

RefCountPtr< const char >
Kernel::CodeBracketContinuation::description( ) const
{
	return strrefdup( "code bracket" );
}

void
Kernel::CodeBracketContinuation::gcMark( Kernel::GCMarkedSet & marked )
{
	const_cast< Kernel::CodeBracketContInfo * >( info_.getPtr( ) )->gcMark( marked );
}


Ast::LexiographicVariable::LexiographicVariable( const Ast::SourceLocation & loc, const Ast::Identifier * id, Kernel::Environment::LexicalKey ** idKey )
	: Ast::Expression( loc ), id_( id ), idKey_( idKey )
{
	immediate_ = true;
}

Ast::LexiographicVariable::~LexiographicVariable( )
{
	delete id_;
	if( *idKey_ != 0 )
		{
			delete *idKey_;
		}
	delete idKey_; /* This can be done only as long as this is not shared! */
}

void
Ast::LexiographicVariable::analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst )
{
	if( *idKey_ == 0 )
		{
			*idKey_ = new Kernel::Environment::LexicalKey( env->findLexicalVariableKey( loc_, *id_ ) );
		}
	scope_level_ = env->level( ) - (*idKey_)->up_;
}

void
Ast::LexiographicVariable::eval( Kernel::EvalState * evalState ) const
{
	evalState->env_->lookup( **idKey_, evalState );
}


Ast::PrivateAliasVariable::PrivateAliasVariable( const Ast::SourceLocation & loc, const Ast::PlacedIdentifier & id, const RefCountPtr< const char > & privateName )
	: Ast::Expression( loc ), id_( id ), privateName_( privateName ), aliasKey_( NULL )
{
	immediate_ = true;
}

Ast::PrivateAliasVariable::~PrivateAliasVariable( )
{
	/* Don't delete id_ -- it will be shared with the id_ of a DefineVariable. */
	if( aliasKey_ != NULL ){
		delete aliasKey_;
	}
}

void
Ast::PrivateAliasVariable::analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst )
{
	aliasKey_ = new Kernel::Environment::LexicalKey( env->findPrivateAliasVariableKey( loc_, id_, privateName_.getPtr( ) ) );
}

void
Ast::PrivateAliasVariable::eval( Kernel::EvalState * evalState ) const
{
	evalState->env_->lookup( *aliasKey_, evalState );
}


Ast::Force::Force( const Ast::SourceLocation & loc, Ast::Expression * expr )
	: Ast::Expression( loc ), expr_( expr )
{
	immediate_ = true;
}

Ast::Force::~Force( )
{
	delete expr_;
}

void
Ast::Force::analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst )
{
	expr_->analyze( this, env, freeStatesDst );
}

void
Ast::Force::eval( Kernel::EvalState * evalState ) const
{
	evalState->cont_ = Kernel::ContRef( new Kernel::ForcingContinuation( evalState->cont_, loc_ ) );
	evalState->expr_ = expr_;
}


Ast::EvalOutsideExpr::EvalOutsideExpr( const Ast::SourceLocation & loc, Ast::Expression * expr )
	: Ast::Expression( loc ), expr_( expr )
{
	immediate_ = true;
}

Ast::EvalOutsideExpr::~EvalOutsideExpr( )
{
	delete expr_;
}

void
Ast::EvalOutsideExpr::analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst )
{
	expr_->analyze( this, env->getParent( ), freeStatesDst );
}

void
Ast::EvalOutsideExpr::eval( Kernel::EvalState * evalState ) const
{
	evalState->expr_ = expr_;
	evalState->env_ = evalState->env_->getParent( );
}


Ast::MemberReferenceFunction::MemberReferenceFunction( const Ast::SourceLocation & loc, Ast::Expression * variable, const char * fieldID )
	: Lang::Function( new Kernel::EvaluatedFormals( Ast::FileID::build_internal( "<>.<>" ), true ) ), loc_( loc, bool( ) ), variable_( variable ), fieldID_( fieldID )
{ }

Ast::MemberReferenceFunction::~MemberReferenceFunction( )
{
	delete variable_;
	delete fieldID_;
}

void
Ast::MemberReferenceFunction::push_exprs( Ast::ArgListExprs * args ) const
{
	args->orderedExprs_->push_back( variable_ );
}

void
Ast::MemberReferenceFunction::analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst )
{
	/* The variable is analyzed as part of the arguments passed to this function, so nothing needs to be done here...
	 * unless we would be able to figure out the type of the argument, and then check if the field reference is valid.
	 */
}

void
Ast::MemberReferenceFunction::call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
{
	Kernel::ContRef cont = evalState->cont_;
	RefCountPtr< const Lang::Value > arg = args.getValue( 0 );
	cont->takeHandle( arg->getField( fieldID_, arg ),
										evalState );
}



Ast::MutatorReference::MutatorReference( const Ast::SourceLocation & mutatorLoc, Ast::StateReference * state, const char * mutatorID )
	: Ast::Expression( mutatorLoc ), mutatorLoc_( mutatorLoc, bool( ) ), state_( state ), mutatorID_( mutatorID )
{ }

Ast::MutatorReference::~MutatorReference( )
{
	/* At the time of implementing this bug-fix, state_ will allways be owned by the Ast::CallExpr that is also the owner of us.
	 * Hence, we do not consider ourselves owners.  Perhaps one should have a flag indicating whether ownership is transferred
	 * when calling the constructor, but at the moment this seems like a waste of resources.
	 */
	//	delete state_;
	delete mutatorID_;
}

void
Ast::MutatorReference::analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst )
{
	state_->analyze( this, env, freeStatesDst );
	/* If the type of the state was known here, we should verify that there is a mutator corresponding to the message <mutatorID>.
	 */
}

void
Ast::MutatorReference::eval( Kernel::EvalState * evalState ) const
{
	Kernel::ContRef cont = evalState->cont_;
	cont->takeValue( state_->getHandle( evalState->env_, evalState->dyn_ )->getMutator( mutatorID_ ),
									 evalState );
}



Ast::SpecialLength::SpecialLength( const Ast::SourceLocation & loc, double val, int sort )
	: Ast::Expression( loc ), val_( val ), sort_( sort )
{ }

Ast::SpecialLength::~SpecialLength( )
{ }

void
Ast::SpecialLength::analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst )
{
	/* Nothing to do! */
}

void
Ast::SpecialLength::eval( Kernel::EvalState * evalState ) const
{
	Concrete::Length d;
	double a0;
	double a1;

	evalState->dyn_->specialUnitService( & d, & a0, & a1 );

	if( sort_ == Computation::SPECIALU_NOINFLEX )
		{
			Kernel::ContRef cont = evalState->cont_;
			cont->takeValue( Kernel::ValueRef( new Lang::Length( val_ * d * Computation::specialUnitNoInflexion( a0, a1 ) ) ),
											 evalState );
			return;
		}
	if( ! sort_ & Computation::SPECIALU_DIST )
		{
			throw Exceptions::InternalError( strrefdup( "The special unit is neither based on inflexion or distance" ) );
		}

	double res = 1;

	if( sort_ & Computation::SPECIALU_CIRC )
		{
			res *= Computation::specialUnitCircleHandle( a0 );
		}

	if( sort_ & Computation::SPECIALU_CORR )
		{
			res *= Computation::specialUnitCorrection( a0, a1 );
		}

	if( sort_ & Computation::SPECIALU_NOINFLEX )
		{
			res = min( res, Computation::specialUnitNoInflexion( a0, a1 ) );
		}
	Kernel::ContRef cont = evalState->cont_;
	cont->takeValue( Kernel::ValueRef( new Lang::Length( val_ * d * res ) ),
									 evalState );
}


Ast::DynamicVariable::DynamicVariable( const Ast::SourceLocation & loc, const Ast::Identifier * id )
	: Ast::Expression( loc ), id_( id ), idKey_( new Kernel::Environment::LexicalKey * ( 0 ) )
{
	immediate_ = true;
}

Ast::DynamicVariable::~DynamicVariable( )
{
	delete id_;
	if( *idKey_ != 0 )
		{
			delete *idKey_;
		}
	delete idKey_;		 //	This can be done only as long as this is not shared!
}

void
Ast::DynamicVariable::analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst )
{
	if( *idKey_ == 0 )
		{
			*idKey_ = new Kernel::Environment::LexicalKey( env->findLexicalDynamicKey( loc_, *id_ ) );
		}
}

void
Ast::DynamicVariable::eval( Kernel::EvalState * evalState ) const
{
	const Kernel::DynamicVariableProperties & dynProps = evalState->env_->lookupDynamicVariable( **idKey_ );

	Kernel::VariableHandle res = dynProps.fetch( evalState->dyn_ );

	/* Now, we know that if the value was bound to a dynamic expression, a value was bound, and that value has
	 * a certain type.
	 */
	if( ! res->isThunk( ) )
		{
			try
				{
					typedef const Lang::DynamicExpression DynType;
					RefCountPtr< DynType > dynVal = res->tryVal< DynType >( );
					dynVal->evalHelper( evalState );
					return;
				}
			catch( const NonLocalExit::NotThisType & ball )
				{
					/* Never mind. */
				}
		}

	Kernel::ContRef cont = evalState->cont_;
	cont->takeHandle( res,
										evalState );
}


Kernel::DynamicBindingContinuation::DynamicBindingContinuation( const Ast::SourceLocation & traceLoc, const Kernel::PassedEnv & env, const Kernel::Environment::LexicalKey & key, const Ast::DynamicBindingExpression * bindingExpr, const Kernel::ContRef & cont )
	: Kernel::Continuation( traceLoc ), env_( env ), key_( key ), bindingExpr_( bindingExpr ), cont_( cont )
{ }

Kernel::DynamicBindingContinuation::~DynamicBindingContinuation( )
{ }

void
Kernel::DynamicBindingContinuation::takeHandle( Kernel::VariableHandle val, Kernel::EvalState * evalState, bool dummy ) const
{
	if( val->isThunk( ) )
		{
			val->force( val, evalState );
			return;
		}
	evalState->cont_ = cont_;
	env_->lookupDynamicVariable( key_ ).makeBinding( val, bindingExpr_, evalState );
}

Kernel::ContRef
Kernel::DynamicBindingContinuation::up( ) const
{
	return cont_;
}

RefCountPtr< const char >
Kernel::DynamicBindingContinuation::description( ) const
{
	return strrefdup( "dynamic binding" );
}

void
Kernel::DynamicBindingContinuation::gcMark( Kernel::GCMarkedSet & marked )
{
	env_->gcMark( marked );
	cont_->gcMark( marked );
}

Ast::DynamicBindingExpression::DynamicBindingExpression( const Ast::SourceLocation & idLoc, const Ast::Identifier * id, Ast::Expression * expr, Kernel::Environment::LexicalKey ** idKey )
	: Ast::Expression( idLoc, expr->loc( ) ), idLoc_( idLoc, bool( ) ), id_( id ), expr_( expr ), idKey_( new Kernel::Environment::LexicalKey * ( 0 ) )
{ }

Ast::DynamicBindingExpression::~DynamicBindingExpression( )
{
	delete id_;
	delete expr_;
	if( *idKey_ != 0 )
		{
			delete *idKey_;
			*idKey_ = 0;
		}
	// Don't delete idKey as it's shared!
}

void
Ast::DynamicBindingExpression::analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst )
{
	expr_->analyze( this, env, freeStatesDst );
	if( *idKey_ == 0 )
		{
			*idKey_ = new Kernel::Environment::LexicalKey( env->findLexicalDynamicKey( idLoc_, *id_ ) );
		}
}

void
Ast::DynamicBindingExpression::eval( Kernel::EvalState * evalState ) const
{
	const Kernel::DynamicVariableProperties & dynProps = evalState->env_->lookupDynamicVariable( **idKey_ );

	if( dynProps.forceValue( ) || expr_->immediate_ )
		{
			evalState->expr_ = expr_;
			evalState->cont_ = Kernel::ContRef( new Kernel::DynamicBindingContinuation( expr_->loc( ), evalState->env_, **idKey_, this, evalState->cont_ ) );
		}
	else
		{
			dynProps.makeBinding( Kernel::VariableHandle( new Kernel::Variable( new Kernel::Thunk( evalState->env_, evalState->dyn_, expr_ ) ) ),
														this,
														evalState );
		}
}


Ast::DynamicStateBindingExpression::DynamicStateBindingExpression( const Ast::SourceLocation & loc, const Ast::SourceLocation & dstLoc, const Ast::Identifier * dstId, Ast::StateReference * src )
	: Ast::Expression( loc ), dstLoc_( dstLoc, bool( ) ), dstId_( dstId ), dstIdKey_( new Kernel::Environment::LexicalKey * ( 0 ) ), src_( src )
{ }

Ast::DynamicStateBindingExpression::~DynamicStateBindingExpression( )
{
	delete src_;
	if( *dstIdKey_ != 0 )
		{
			delete *dstIdKey_;
		}
	delete dstIdKey_;		 //	This can be done only as long as this is not shared!
}

void
Ast::DynamicStateBindingExpression::analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst )
{
	if( *dstIdKey_ == 0 )
		{
			*dstIdKey_ = new Kernel::Environment::LexicalKey( env->findLexicalDynamicKey( dstLoc_, *dstId_ ) );
		}
}

void
Ast::DynamicStateBindingExpression::eval( Kernel::EvalState * evalState ) const
{
	const Kernel::DynamicStateProperties & dstDynProps = evalState->env_->lookupDynamicState( **dstIdKey_ );

	dstDynProps.makeBinding( src_->getHandle( evalState->env_, evalState->dyn_ ), this, evalState );
}


Kernel::WithDynamicContinuation::WithDynamicContinuation( const Ast::SourceLocation & traceLoc, Ast::Expression * expr, const Kernel::EvalState & evalState )
	: Kernel::Continuation( traceLoc ), expr_( expr ), env_( evalState.env_ ), dyn_( evalState.dyn_ ), cont_( evalState.cont_ )
{ }

Kernel::WithDynamicContinuation::~WithDynamicContinuation( )
{ }

void
Kernel::WithDynamicContinuation::takeValue( const RefCountPtr< const Lang::Value > & val, Kernel::EvalState * evalState, bool dummy ) const
{
	evalState->dyn_ = Kernel::PassedDyn( new Kernel::DynamicEnvironment( dyn_, *Helpers::down_cast< const Lang::DynamicBindings >( val, traceLoc_ ) ) );
	evalState->env_ = env_;
	evalState->expr_ = expr_;
	evalState->cont_ = cont_;
}

Kernel::ContRef
Kernel::WithDynamicContinuation::up( ) const
{
	return cont_;
}

RefCountPtr< const char >
Kernel::WithDynamicContinuation::description( ) const
{
	return strrefdup("with dynamic bindings" );
}

void
Kernel::WithDynamicContinuation::gcMark( Kernel::GCMarkedSet & marked )
{
	env_->gcMark( marked );
	dyn_->gcMark( marked );
	cont_->gcMark( marked );
}


Ast::WithDynamicExpr::WithDynamicExpr( const Ast::SourceLocation & loc, Ast::Expression * bindings, Ast::Expression * expr )
	: Ast::Expression( loc ), bindings_( bindings ), expr_( expr )
{ }

Ast::WithDynamicExpr::~WithDynamicExpr( )
{ }

void
Ast::WithDynamicExpr::analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst )
{
	bindings_->analyze( this, env, freeStatesDst );
	expr_->analyze( this, env, freeStatesDst );
}

void
Ast::WithDynamicExpr::eval( Kernel::EvalState * evalState ) const
{
	evalState->expr_ = bindings_;
	evalState->cont_ = Kernel::ContRef( new Kernel::WithDynamicContinuation( bindings_->loc( ), expr_, *evalState ) );
}


Ast::DynamicVariableDecl::DynamicVariableDecl( const Ast::SourceLocation & loc, const Ast::SourceLocation & idLoc, const Ast::PlacedIdentifier * id, Ast::Expression * filterExpr, Ast::Expression * defaultExpr )
	: Ast::BindNode( loc, idLoc, id ), idPos_( new size_t * ( 0 ) )
{
  /* This type of expression is an Ast::BindNode so that it is easy to recognize and extract the identifier for static analysis
   * and similar tasks.
   *
   * The expression is implemented as a function call, since there are two subexpressions that may need evaluation.
   */

  Ast::ArgListExprs * args = new Ast::ArgListExprs( false );
  Ast::DynamicVariableDeclFunction * res = new Ast::DynamicVariableDeclFunction( id, filterExpr, defaultExpr, idPos_ );
  res->push_exprs( args );
  Ast::CallExpr * callExpr =
    new Ast::CallExpr( loc,
                       RefCountPtr< const Lang::Function >( res ),
                       args );
  /* Wrap the call in Ast::Force as a poor substitute for doing the right thing, see comment in initializeEnvironment. */
  impl_ = new Ast::Force( loc, callExpr );
}

Ast::DynamicVariableDecl::~DynamicVariableDecl( )
{ }

void
Ast::DynamicVariableDecl::analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst )
{
  impl_->analyze( this, env, freeStatesDst );
  if( *idPos_ == 0 ){
    *idPos_ = new size_t( env->findLocalDynamicPosition( idLoc_, *id_ ) );
  }
}

void
Ast::DynamicVariableDecl::initializeEnvironment( Kernel::PassedEnv env, Kernel::PassedDyn dyn ) const
{
  /* Here, the right thing to do is probably to put the impl_ call in a DynamicVariableProperties thunk, but
   * that type of thunk is not invented yet.
   *
   * As a poor substitute for the right thing, the impl_ call is wrapped in Ast::Force, see contructor.
   */
}

void
Ast::DynamicVariableDecl::evalHelper( Kernel::EvalState * evalState ) const
{
	evalState->expr_ = impl_;
}


Ast::DynamicVariableDeclFunction::DynamicVariableDeclFunction( const Ast::PlacedIdentifier * id, Ast::Expression * filterExpr, Ast::Expression * defaultExpr, size_t ** idPos )
	: Lang::Function( new Kernel::EvaluatedFormals( Ast::FileID::build_internal( "< dynamic variable declaration >" ) ) ), id_( id ), filterExpr_( filterExpr ), defaultExpr_( defaultExpr ), idPos_( idPos )
{
	formals_->appendEvaluatedCoreFormal( "filter", Kernel::THE_SLOT_VARIABLE, true );
	formals_->appendEvaluatedCoreFormal( "default", Kernel::THE_SLOT_VARIABLE, false );
}

Ast::DynamicVariableDeclFunction::~DynamicVariableDeclFunction( )
{
	delete filterExpr_;
	delete defaultExpr_;
}

void
Ast::DynamicVariableDeclFunction::push_exprs( Ast::ArgListExprs * args ) const
{
	args->orderedExprs_->push_back( filterExpr_ );
	args->orderedExprs_->push_back( defaultExpr_ );
}

void
Ast::DynamicVariableDeclFunction::analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst )
{
	/* The analysis is carried out by the DynamicVariableDecl expression.
	 */
}

void
Ast::DynamicVariableDeclFunction::call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
{
	static const char * title = "< dynamic variable declaration >";
	typedef const Lang::Function FilterType;
	evalState->env_->defineDynamic( id_,
																	**idPos_,
																	Helpers::down_cast_SyntaxArgument< FilterType >( title, args, 0, callLoc ),
																	args.getHandle( 1 ) );

	Kernel::ContRef cont = evalState->cont_;
	cont->takeHandle( Kernel::THE_VOID_VARIABLE,
										evalState );
}


Ast::DynamicStateDecl::DynamicStateDecl( const Ast::SourceLocation & loc, const Ast::SourceLocation & idLoc, const Ast::PlacedIdentifier * id, Ast::StateReference * defaultState, size_t ** idPos )
	: Ast::BindNode( loc, idLoc, id ), idPos_( idPos ), defaultState_( defaultState )
{ }

Ast::DynamicStateDecl::~DynamicStateDecl( )
{ }


void
Ast::DynamicStateDecl::analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst )
{
	if( *idPos_ == 0 )
		{
			*idPos_ = new size_t( env->findLocalDynamicStatePosition( idLoc_, *id_ ) );
		}
}

void
Ast::DynamicStateDecl::initializeEnvironment( Kernel::PassedEnv env, Kernel::PassedDyn dyn ) const
{
	env->defineDynamicState( id_, **idPos_, env, dyn, defaultState_ );
}

void
Ast::DynamicStateDecl::evalHelper( Kernel::EvalState * evalState ) const
{
	Kernel::ContRef cont = evalState->cont_;
	cont->takeHandle( Kernel::THE_VOID_VARIABLE,
										evalState );
}


Ast::EvalSymbolFunction::EvalSymbolFunction( const Ast::SourceLocation & loc, Ast::Expression * expr, const RefCountPtr< const Ast::NamespacePath > & lexicalPath )
	: Lang::Function( new Kernel::EvaluatedFormals( Ast::FileID::build_internal( "< symbol evaluation >" ), true ) ), loc_( loc, bool( ) ), expr_( expr ), lexicalPath_( lexicalPath )
{ }

Ast::EvalSymbolFunction::~EvalSymbolFunction( )
{
	delete expr_;
}

void
Ast::EvalSymbolFunction::push_exprs( Ast::ArgListExprs * args ) const
{
	args->orderedExprs_->push_back( expr_ );
}

void
Ast::EvalSymbolFunction::analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst )
{
	analysisEnv_ = env;

	/* expr_ shall be analyzed from the calling expression.
	 * Here, it is only used to locate errors.
	 */
}

void
Ast::EvalSymbolFunction::call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const
{
	static const RefCountPtr< const Ast::NamespacePath > EMPTY_RELATIVE_PATH( new Ast::NamespacePath( ) );

	RefCountPtr< const Lang::Value > untypedVal = args.getValue( 0 );
	typedef const Lang::Symbol ArgType;
	ArgType * val = dynamic_cast< ArgType * >( untypedVal.getPtr( ) );
	if( val == 0 )
		{
			throw Exceptions::TypeMismatch( expr_->loc( ), untypedVal->getTypeName( ), ArgType::staticTypeName( ) );
		}
	if( val->isUnique( ) )
		{
			throw Exceptions::OutOfRange( expr_->loc( ), strrefdup( "Unique symbols can't denote variables." ) );
		}

	RefCountPtr< const Ast::SearchContext > searchContext( new Ast::SearchContext( lexicalPath_, RefCountPtr< const char >( NullPtr< const char >( ) ) ) );
	Kernel::Environment::LexicalKey key = analysisEnv_->findLexicalVariableKey( loc_, Ast::Identifier( searchContext, Ast::NamespaceReference::RELATIVE, EMPTY_RELATIVE_PATH, val->name( ).getPtr( ) ) );

	Kernel::PassedEnv env = evalState->env_;
	env->lookup( key, evalState );
}


Ast::DefineVariable::DefineVariable( const Ast::SourceLocation & idLoc, const Ast::PlacedIdentifier * id, Ast::Expression * expr, size_t ** idPos )
	: Ast::BindNode( idLoc, expr->loc( ), idLoc, id ), expr_( expr ), idPos_( idPos )
{ }

Ast::DefineVariable::~DefineVariable( )
{
	delete expr_;

	/* idPos_ is shared and will be a memory leak which must not be deleted.
	 * It would be easy to fix the leak using RefCountPtr< size_t >, but the leakage is constant space, so silly efficiency is prioritized.
	 */
}

void
Ast::DefineVariable::analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst )
{
	Ast::StateIDSet freeStates;
	expr_->analyze( this, env, & freeStates );
	if( ! freeStates.empty( ) )
		{
			Ast::Force * forcedExpr( new Ast::Force( loc_, expr_ ) );
			/* The forcedExpr should not be analyzed since the contained expr_ is already
			 * analyzed, and analyzing forcedExpr would just cause expr_ to be analyzed again.
			 * Instead, we just need to update parent/child links.
			 */
			expr_->changeParent( forcedExpr );
			forcedExpr->setParent( this, env );
			expr_ = forcedExpr;
			for( Ast::StateIDSet::const_iterator i = freeStates.begin( ); i != freeStates.end( ); ++i )
				{
					freeStatesDst->insert( *i );
				}
		}
	if( *idPos_ == 0 )
		{
			*idPos_ = new size_t( env->findLocalVariablePosition( idLoc_, *id_ ) );
		}
}

void
Ast::DefineVariable::initializeEnvironment( Kernel::PassedEnv env, Kernel::PassedDyn dyn ) const
{
  env->define( **idPos_,
               Kernel::VariableHandle( new Kernel::Variable( new Kernel::Thunk( env, dyn, expr_ ) ) ) );
}

void
Ast::DefineVariable::evalHelper( Kernel::EvalState * evalState ) const
{
	if( expr_->immediate_ ){
		Kernel::VariableHandle var = evalState->env_->getVarHandle( **idPos_ );
		/* Using sufficient but not necessary condition for recognizing that the thunk
		 * set up in initializeEnvironment is still not forced.  The worst thing that
		 * can happen is that another thunk for this expression, but in another environment
		 * or another dynamic environment, is forced.  If that happens, note that
		 * expr_->immediate_ also applies to the other thunk, so it should be forced sooner or
		 * later anywhay.
		 */
		if( var->isThunk( expr_ ) ){
			evalState->cont_ = Kernel::ContRef( new Kernel::IgnoreContinuation( evalState->cont_, expr_->loc( ) ) );
			var->force( var, evalState );
			return;
		}
	}

	Kernel::ContRef cont = evalState->cont_;
	cont->takeHandle( Kernel::THE_VOID_VARIABLE, evalState );
}


Ast::DefineAlias::DefineAlias( const Ast::SourceLocation & idLoc, const Ast::SourceLocation & expansionLoc, const Ast::PlacedIdentifier * id, const Ast::NamespaceReference & expansion )
	: Ast::Node( idLoc ), idLoc_( idLoc, bool( ) ), id_( id ), expansion_( expansion ), expansionLoc_( expansionLoc, bool( ) )
{ }

Ast::DefineAlias::~DefineAlias( )
{
	delete id_;
}

void
Ast::DefineAlias::analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst )
{
	throw Exceptions::InternalError( "Ast::DefineAlias::analyze_impl: Alias definitions should not be present at program analysis." );
}

Ast::LexicalVariableLocationExpr::LexicalVariableLocationExpr( const Ast::SourceLocation & idLoc, const Ast::Identifier * id, Kind kind )
	: Ast::Expression( idLoc ), id_( id ), kind_( kind ), value_( NullPtr< const Lang::Value >( ) )
{
	immediate_ = true;
}

Ast::LexicalVariableLocationExpr::~LexicalVariableLocationExpr( )
{ }

void
Ast::LexicalVariableLocationExpr::analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst )
{
	Kernel::Environment::LexicalKey key = Kernel::Environment::LexicalKey( env->findLexicalVariableKey( loc_, *id_ ) );
	if( key.isMissing( ) ){
		/* If the variable wasn't found, compilation will be terminated after the analysis sweep, and there's
		 * no point in trying to assign something meaningless to value_.
		 */
		return;
	}

	switch( kind_ )
		{
		case INDEX:
			value_ = Kernel::ValueRef( new Lang::Integer( key.pos_ ) );
			break;
		case DEPTH:
			value_ = Kernel::ValueRef( new Lang::Integer( key.up_ ) );
			break;
		case ABSID:
			{
				Ast::PlacedIdentifier idPlaced( env->reverseMapLexicalVariable( key ) );
				std::ostringstream oss;
				oss << Interaction::NAMESPACE_SEPARATOR ;
				idPlaced.show( oss, Ast::Identifier::VARIABLE );
				value_ = Kernel::ValueRef( new Lang::String( strrefdup( oss ) ) );
			}
			break;
		}
}

void
Ast::LexicalVariableLocationExpr::eval( Kernel::EvalState * evalState ) const
{
	Kernel::ContRef cont = evalState->cont_;
	cont->takeValue( value_, evalState );
}


Ast::LexicalVariableNameExpr::LexicalVariableNameExpr( const Ast::SourceLocation & loc )
	: Ast::Expression( loc ), value_( NullPtr< const Lang::Value >( ) )
{
	immediate_ = true;
}

Ast::LexicalVariableNameExpr::~LexicalVariableNameExpr( )
{ }

void
Ast::LexicalVariableNameExpr::analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst )
{
	Ast::Node * n = parent->parent( );
	while( n != 0 )
		{
			Ast::BindNode * bn = dynamic_cast< Ast::DefineVariable * >( n );
			if( bn != 0 )
				{
					std::ostringstream os;
					bn->id( )->show( os, Ast::Identifier::VARIABLE );
					value_ = Kernel::ValueRef( new Lang::String( strrefdup( os ) ) );
					return;
				}
			n = n->parent( );
		}
	Ast::theAnalysisErrorsList.push_back( new Exceptions::MiscellaneousStaticInconsistency( loc( ), strrefdup( "Cannot retrieve variable name, since the location is not inside a binding's right hand side." ) ) );
}

void
Ast::LexicalVariableNameExpr::eval( Kernel::EvalState * evalState ) const
{
	Kernel::ContRef cont = evalState->cont_;
	cont->takeValue( value_, evalState );
}


Kernel::AssertStructureContinuation::AssertStructureContinuation( const Kernel::ContRef & cont, const Ast::SourceLocation & traceLoc )
	: Kernel::Continuation( traceLoc ), cont_( cont )
{ }

Kernel::AssertStructureContinuation::~AssertStructureContinuation( )
{ }

void
Kernel::AssertStructureContinuation::takeValue( const RefCountPtr< const Lang::Value > & val, Kernel::EvalState * evalState, bool dummy ) const
{
	evalState->cont_ = cont_;
	cont_->takeValue( Helpers::down_cast_ContinuationArgument< const Lang::Structure >( val, this ), evalState );
}

Kernel::ContRef
Kernel::AssertStructureContinuation::up( ) const
{
	return cont_;
}

RefCountPtr< const char >
Kernel::AssertStructureContinuation::description( ) const
{
	return strrefdup( "Assert type is struct" );
}

void
Kernel::AssertStructureContinuation::gcMark( Kernel::GCMarkedSet & marked )
{
	cont_->gcMark( marked );
}


Ast::StructSplitReference::StructSplitReference( const Ast::SourceLocation & fieldLoc, const char * fieldId, Ast::Expression * defaultExpr )
	: Ast::Expression( fieldLoc ),
		structLoc_( Ast::THE_UNKNOWN_LOCATION, bool( ) ), // This is a dummy value!	The correct value is set later.
		fieldId_( fieldId ),
		defaultExpr_( defaultExpr )
{ }

Ast::StructSplitReference::StructSplitReference( const Ast::SourceLocation & fieldLoc, size_t fieldPos, Ast::Expression * defaultExpr )
	: Ast::Expression( fieldLoc ),
		structLoc_( Ast::THE_UNKNOWN_LOCATION, bool( ) ), // This is a dummy value!	The correct value is set later.
		fieldId_( 0 ), fieldPos_( fieldPos ),
		defaultExpr_( defaultExpr )
{ }

Ast::StructSplitReference::~StructSplitReference( )
{
	if( fieldId_ != 0 )
		{
			delete fieldId_;
		}
	if( defaultExpr_ != 0 )
		{
			delete defaultExpr_;
		}
}

void
Ast::StructSplitReference::setStruct( const Ast::SourceLocation & structLoc, size_t ** structPos )
{
	structLoc_ = structLoc;
	structPos_ = structPos;
}

void
Ast::StructSplitReference::analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst )
{
	if( defaultExpr_ != 0 )
		{
			defaultExpr_->analyze( this, env, freeStatesDst );
		}
}

void
Ast::StructSplitReference::eval( Kernel::EvalState * evalState ) const
{
	Kernel::VariableHandle structHandle = evalState->env_->getVarHandle( **structPos_ );
	typedef const Lang::Structure StructType;
	RefCountPtr< StructType > structVal = structHandle->getVal< StructType >( "Type-checked value in StructSplitReference::eval." );

	Kernel::ContRef cont = evalState->cont_;
	if( fieldId_ != 0 )
		{
			try
				{
					cont->takeHandle( structVal->getField( fieldId_, structVal ),
														evalState );
					return;
				}
			catch( const Exceptions::NonExistentMember & ball )
				{
					if( defaultExpr_ == 0 )
						{
							throw;
						}
					/* Never mind, we use the default instead.	See below. */
				}
		}
	else
		{
			try
				{
					cont->takeHandle( structVal->getPosition( fieldPos_, structVal ),
														evalState );
					return;
				}
			catch( const Exceptions::NonExistentPosition & ball )
				{
					if( defaultExpr_ == 0 )
						{
							throw;
						}
					/* Never mind, we use the default instead.	See below. */
				}
		}

	if( defaultExpr_ == 0 )
		{
			throw Exceptions::InternalError( "Just about to use null pointer defaultExpr_ in StructSplitReference::eval." );
		}
	evalState->expr_ = defaultExpr_;
}

Ast::StructSplitSink::StructSplitSink( )
	: Ast::Expression( Ast::THE_UNKNOWN_LOCATION ), structLoc_( Ast::THE_UNKNOWN_LOCATION, bool( ) )
{ }

Ast::StructSplitSink::~StructSplitSink( )
{ }

void
Ast::StructSplitSink::setStruct( const Ast::SourceLocation & structLoc, size_t ** structPos, size_t consumedArguments )
{
	structLoc_ = structLoc;
	structPos_ = structPos;
	consumedArguments_ = consumedArguments;
}

void
Ast::StructSplitSink::analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst )
{
	/* Nothing to do! */
}

void
Ast::StructSplitSink::eval( Kernel::EvalState * evalState ) const
{
	Kernel::VariableHandle structHandle = evalState->env_->getVarHandle( **structPos_ );
	typedef const Lang::Structure StructType;
	RefCountPtr< StructType > structVal = structHandle->getVal< StructType >( "Type-checked value in StructSplitReference::eval." );

	Kernel::ContRef cont = evalState->cont_;
	cont->takeValue( structVal->getSink( consumedArguments_ ),
									 evalState );
}


Ast::AssertNoSinkNeeded::AssertNoSinkNeeded( const Ast::SourceLocation & loc, size_t orderedCount, const Ast::SourceLocation & structLoc, size_t ** structPos )
	: Ast::Assertion( loc ), orderedCount_( orderedCount ), structLoc_( structLoc, bool( ) ), structPos_( structPos )
{ }

Ast::AssertNoSinkNeeded::~AssertNoSinkNeeded( )
{ }

void
Ast::AssertNoSinkNeeded::analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst )
{
	/* immediate_ is already true by the inheritage from Ast::Assertion. */
}

void
Ast::AssertNoSinkNeeded::eval( Kernel::EvalState * evalState ) const
{
	Kernel::VariableHandle structHandle = evalState->env_->getVarHandle( **structPos_ );
	typedef const Lang::Structure StructType;
	RefCountPtr< StructType > structVal = structHandle->getVal< StructType >( "Type-checked value in StructSplitReference::eval." );

	if( structVal->argList_->orderedExprs_->size( ) > orderedCount_ )
		{
			throw Exceptions::SinkRequired( loc_, orderedCount_, structVal->argList_->orderedExprs_->size( ) );
		}

	Kernel::ContRef cont = evalState->cont_;
	cont->takeHandle( Kernel::THE_VOID_VARIABLE, evalState );
}

size_t Ast::SplitDefineVariables::splitVarCount = 0;
PtrOwner_back_Access< std::list< const Ast::PlacedIdentifier * > > Ast::SplitDefineVariables::mem;

Ast::SplitDefineVariables::SplitDefineVariables( )
	: sinkDefine_( 0 ), sinkExpr_( 0 ), seenNamed_( false ), seenDefault_( false )
{
	std::ostringstream oss;
	oss << Kernel::SPLIT_VAR_PREFIX << splitVarCount ;
	splitVarId_ = new Ast::PlacedIdentifier( Lang::THE_NAMESPACE_Shapes, strdup( oss.str( ).c_str( ) ) );
	mem.push_back( splitVarId_ );
	++splitVarCount;
}

Ast::PlacedIdentifier *
Ast::SplitDefineVariables::newSplitVarId( ) const
{
	return splitVarId_->clone( );
}


Ast::StateReference::StateReference( const Ast::SourceLocation & loc )
	: Ast::Node( loc )
{ }

Ast::StateReference::~StateReference( )
{ }


Ast::LexiographicState::LexiographicState( const Ast::SourceLocation & loc, const Ast::Identifier * id, Kernel::Environment::LexicalKey ** idKey )
	: Ast::StateReference( loc ), id_( id ), idKey_( idKey )
{ }

Ast::LexiographicState::~LexiographicState( )
{
	delete id_;
	if( *idKey_ != 0 )
		{
			delete *idKey_;
		}
	delete idKey_;		 //	This can be done only as long as this is not shared!
}

void
Ast::LexiographicState::analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst )
{
	/* The following lines is the ugly solution to the problem that the inherited parent_ and analysisEnv_
	 * are not well defined for reused nodes.  And nodes of this type are typically reused.
	 * For this type of node, one _could_ refer to someParent_ and someAnalysisEnv_ instead, but probably,
	 * it is a mistake to even try.
	 */
	someParent_ = parent_;
	parent_ = 0;
	someAnalysisEnv_ = analysisEnv_;
	analysisEnv_ = 0;

	if( *idKey_ == 0 )
		{
			*idKey_ = new Kernel::Environment::LexicalKey( env->findLexicalStateKey( loc_, *id_, this ) );
		}
	if( ! (*idKey_)->isMissing( ) )
		{
			freeStatesDst->insert( env->getStateID( **idKey_ ) );
		}
}

Kernel::StateHandle
Ast::LexiographicState::getHandle( Kernel::PassedEnv env, Kernel::PassedDyn dyn ) const
{
	return env->getStateHandle( **idKey_ );
}


Ast::DynamicState::DynamicState( const Ast::SourceLocation & loc, const Ast::Identifier * id )
	: Ast::StateReference( loc ), id_( id ), idKey_( new Kernel::Environment::LexicalKey * ( 0 ) )
{ }

Ast::DynamicState::~DynamicState( )
{
	delete id_;
	if( *idKey_ != 0 )
		{
			delete *idKey_;
		}
	delete idKey_;		 //	This can be done only as long as this is not shared!
}

void
Ast::DynamicState::analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst )
{
	/* See comment in LexicalState::analyze_impl.
	 */
	someParent_ = parent_;
	parent_ = 0;
	someAnalysisEnv_ = analysisEnv_;
	analysisEnv_ = 0;

	/* It would make sense to check the reference and...
	 * This might be overly conservative... but we really shouldn't support dynamic states anyway.
	 */
	freeStatesDst->insert( Ast::AnalysisEnvironment::getTheAnyStateID( ) );
}

Kernel::StateHandle
Ast::DynamicState::getHandle( Kernel::PassedEnv env, Kernel::PassedDyn dyn ) const
{
	throw Exceptions::NotImplemented( "Referencing dynamic states" );
}


Ast::IntroduceState::IntroduceState( const Ast::SourceLocation & idLoc, const Ast::PlacedIdentifier * id, Ast::Expression * expr, size_t ** idPos )
	: Ast::BindNode( idLoc, expr->loc( ), idLoc, id ), expr_( expr ), idPos_( idPos )
{ }

Ast::IntroduceState::~IntroduceState( )
{
	delete expr_;

	/* idPos_ shared and will be a memory leak which must not be deleted.
	 * It would be easy to fix the leak using RefCountPtr< size_t >, but the leakage is constant space, so silly efficiency is prioritized.
	 */
}


void
Ast::IntroduceState::analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst )
{
  expr_->analyze( this, env, freeStatesDst );

  if( *idPos_ == 0 ){
    *idPos_ = new size_t( env->findLocalStatePosition( idLoc_, *id_ ) );
  }

  const Ast::Node * firstUse = env->getStateFirstUse( **idPos_ );
  if( firstUse != NULL ){
     Ast::theAnalysisErrorsList.push_back( new Exceptions::StateUninitializedUse( firstUse->loc( ), id_, this ) );
  }

  stateID_ = env->getStateID( **idPos_ );
  freeStatesDst->insert( stateID_ );
}

void
Ast::IntroduceState::initializeEnvironment( Kernel::PassedEnv env, Kernel::PassedDyn dyn ) const
{
  /* Although it would be possible to insert some kind of thunk in the environment here,
   * it is not obvious that such a feature would be desirable.  With such a feature, one
   * would be able to use a state before the place in the code where it is introduced:
   * {
   *   •dst << ...
   *   ...
   *   •dst: newGroup
   *   •dst;
   * }
   * This just seems awkward; this is a stateful program, and order generally matters -- this is
   * why use before declaration is considered illegal and treated as error already during
   * static analysis.
   */
}

void
Ast::IntroduceState::evalHelper( Kernel::EvalState * evalState ) const
{
	evalState->cont_ = Kernel::ContRef( new Kernel::IntroduceStateContinuation( evalState->env_,
																																							*idPos_,
																																							evalState->cont_,
																																							expr_->loc( ) ) );
	evalState->expr_ = expr_;
}


Ast::Insertion::Insertion( Ast::StateReference * stateRef, Ast::Expression * expr )
	: Ast::Expression( stateRef->loc( ), expr->loc( ) ), stateRef_( stateRef ), expr_( expr )
{ }

Ast::Insertion::~Insertion( )
{ }

void
Ast::Insertion::analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst )
{
	stateRef_->analyze( this, env, freeStatesDst ); /* This will always add something to the set of free states. */
	expr_->analyze( this, env, freeStatesDst );
}

void
Ast::Insertion::eval( Kernel::EvalState * evalState ) const
{
	evalState->cont_ = Kernel::ContRef( new Kernel::InsertionContinuation( stateRef_->getHandle( evalState->env_, evalState->dyn_ ),
																																				 evalState->cont_,
																																				 evalState->dyn_,
																																				 expr_->loc( ) ) );
	evalState->expr_ = expr_;
}

Ast::InsertionMutatorCall::InsertionMutatorCall( const Ast::SourceLocation & loc, Ast::Expression * expr )
	: Ast::Expression( loc ), expr_( expr )
{ }

Ast::InsertionMutatorCall::~InsertionMutatorCall( )
{ }

void
Ast::InsertionMutatorCall::analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst )
{
	throw Exceptions::InternalError( loc_, "An InsertionMutatorCall was analyzed (the contained expression should have been extracted and replaced this object)." );
}

void
Ast::InsertionMutatorCall::eval( Kernel::EvalState * evalState ) const
{
	throw Exceptions::InternalError( loc_, "An InsertionMutatorCall was evaluated (the contained expression should have been extracted and replaced this object)." );
}


Ast::Freeze::Freeze( const Ast::SourceLocation & idLoc, const Ast::PlacedIdentifier * id, size_t ** idPos )
	: Ast::Expression( idLoc ), id_( id ), idPos_( idPos )
{
	immediate_ = true;
}

Ast::Freeze::~Freeze( )
{
	/* idPos shared and will be a memory leak which must not be deleted.
	 * It would be easy to fix the leak using RefCountPtr< size_t >, but the leakage is constant space, so silly efficiency is prioritized.
	 */
}

void
Ast::Freeze::analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst )
{
	if( *idPos_ == 0 )
		{
			*idPos_ = new size_t( env->findLocalStatePosition( loc( ), *id_ ) );
		}

	freeStatesDst->insert( env->getStateID( **idPos_ ) );
}

void
Ast::Freeze::eval( Kernel::EvalState * evalState ) const
{
	evalState->env_->freeze( **idPos_, evalState, loc( ) );
}


Ast::Peek::Peek( const Ast::SourceLocation & idLoc, Ast::StateReference * stateRef )
	: Ast::Expression( idLoc ), stateRef_( stateRef )
{
	immediate_ = true;
}

Ast::Peek::~Peek( )
{
	/* idPos shared and will be a memory leak which must not be deleted.
	 * It would be easy to fix the leak using RefCountPtr< size_t >, but the leakage is constant space, so silly efficiency is prioritized.
	 */
}

void
Ast::Peek::analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst )
{
	stateRef_->analyze( this, env, freeStatesDst ); /* This will always add something to the set of free states. */
}

void
Ast::Peek::eval( Kernel::EvalState * evalState ) const
{
	stateRef_->getHandle( evalState->env_, evalState->dyn_ )->peek( evalState, loc( ) );
}


Ast::DynamicExpression::DynamicExpression( const Ast::SourceLocation & loc, Ast::Expression * expr )
	: Ast::Expression( loc ), expr_( expr )
{
	immediate_ = true;
}

Ast::DynamicExpression::~DynamicExpression( )
{ }

void
Ast::DynamicExpression::analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst )
{
	Ast::StateIDSet * freeStates = new Ast::StateIDSet;
	expr_->analyze( this, env, freeStates );

	if( ! freeStates->empty( ) )
		{
			Ast::theAnalysisErrorsList.push_back( new Exceptions::IllegalFreeStates( expr_->loc( ), freeStates, "dynamic expressions must be pure" ) );
		}
	else
		{
			delete freeStates;
		}
}

void
Ast::DynamicExpression::eval( Kernel::EvalState * evalState ) const
{
	Kernel::ContRef cont = evalState->cont_;
	cont->takeValue( Kernel::ValueRef( new Lang::DynamicExpression( evalState->env_, expr_ ) ),
									 evalState );
}


Ast::LexiographicType::LexiographicType( const Ast::SourceLocation & loc, const Ast::Identifier * id, Kernel::Environment::LexicalKey ** idKey )
	: Ast::Expression( loc ), id_( id ), idKey_( idKey )
{
	immediate_ = true;
}

Ast::LexiographicType::~LexiographicType( )
{
	delete id_;
	if( *idKey_ != 0 )
		{
			delete *idKey_;
		}
	delete idKey_;		 /* This can be done only as long as this is not shared! */
}

void
Ast::LexiographicType::analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst )
{
	if( *idKey_ == 0 )
		{
			*idKey_ = new Kernel::Environment::LexicalKey( env->findLexicalTypeKey( loc_, *id_ ) );
		}
}

void
Ast::LexiographicType::eval( Kernel::EvalState * evalState ) const
{
	evalState->env_->lookup( **idKey_, evalState );
}


