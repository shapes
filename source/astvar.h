/* This file is part of Shapes.
 *
 * Shapes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Shapes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2008, 2013, 2014, 2015 Henrik Tidefelt
 */

#pragma once

#include "Shapes_Kernel_decls.h"

#include "ast.h"

namespace Shapes
{
	namespace Ast
	{

		class SourceLocationMark : public Node
		{
		public:
			SourceLocationMark( const Ast::SourceLocation & loc );
			virtual ~SourceLocationMark( );

			virtual void analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst );
		};


		class CodeBracket : public Expression
		{
			std::list< Ast::Node * > * nodes_;
			Ast::CodeBracket * extends_;
			std::list< Ast::BindNode * > bindNodes_; /* Subset of nodes_. */
		public:
			Ast::IdentifierTree * argumentOrder_;
			Ast::IdentifierTree * dynamicMap_;
			Ast::IdentifierTree * stateOrder_;
		public:
			CodeBracket( const Ast::SourceLocation & loc, std::list< Ast::Node * > * nodes, Ast::CodeBracket * extends = 0 );
			virtual ~CodeBracket( );

			virtual void analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst );
			virtual void eval( Kernel::EvalState * evalState ) const;

			Kernel::Environment * newEnvironment( Kernel::Environment * parent, const char * debugName = "Bracket" ) const;
			void eval( Kernel::EvalState * evalState, Kernel::Environment * extendsEnv ) const;

			void evalAt( const RefCountPtr< const Kernel::CodeBracketContInfo > & info, const std::list< Ast::Node * >::const_iterator & i, Kernel::EvalState * evalState ) const;

			void setDynamicMap( Ast::IdentifierTree * dynamicMap );
		};

	}

	namespace Kernel
	{
		class CodeBracketContInfo
		{
		public:
			const Ast::CodeBracket * bracketExpr_;
			Kernel::PassedEnv env_;
			Kernel::PassedDyn dyn_;
			Kernel::ContRef cont_;

			CodeBracketContInfo( const Ast::CodeBracket * bracketExpr, const Kernel::EvalState & evalState );
			~CodeBracketContInfo( );

			void gcMark( Kernel::GCMarkedSet & marked );
		};

		class CodeBracketContinuation : public Kernel::Continuation
		{
			RefCountPtr< const Kernel::CodeBracketContInfo > info_;
			std::list< Ast::Node * >::const_iterator pos_;
		public:
			CodeBracketContinuation( const Ast::SourceLocation & _traceLoc, const RefCountPtr< const Kernel::CodeBracketContInfo > & _info, const std::list< Ast::Node * >::const_iterator & _pos );
			virtual ~CodeBracketContinuation( );
			virtual void takeValue( const RefCountPtr< const Lang::Value > & val, Kernel::EvalState * evalState, bool dummy ) const;
			virtual Kernel::ContRef up( ) const;
			virtual RefCountPtr< const char > description( ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
		};


		class DynamicBindingContinuation : public Kernel::Continuation
		{
			Kernel::PassedEnv env_;
			Kernel::Environment::LexicalKey key_;
			const Ast::DynamicBindingExpression * bindingExpr_;
			Kernel::ContRef cont_;
		public:
			DynamicBindingContinuation( const Ast::SourceLocation & traceLoc, const Kernel::PassedEnv & env, const Kernel::Environment::LexicalKey & key, const Ast::DynamicBindingExpression * bindingExpr, const Kernel::ContRef & cont );
			virtual ~DynamicBindingContinuation( );
			virtual void takeHandle( Kernel::VariableHandle val, Kernel::EvalState * evalState, bool dummy ) const;
			virtual Kernel::ContRef up( ) const;
			virtual RefCountPtr< const char > description( ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
		};

		class WithDynamicContinuation : public Kernel::Continuation
		{
			Ast::Expression * expr_;
			Kernel::PassedEnv env_;
			Kernel::PassedDyn dyn_;
			Kernel::ContRef cont_;
		public:
			WithDynamicContinuation( const Ast::SourceLocation & traceLoc, Ast::Expression * expr, const Kernel::EvalState & evalState );
			virtual ~WithDynamicContinuation( );
			virtual void takeValue( const RefCountPtr< const Lang::Value > & val, Kernel::EvalState * evalState, bool dummy ) const;
			virtual Kernel::ContRef up( ) const;
			virtual RefCountPtr< const char > description( ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
		};

		class AssertStructureContinuation : public Kernel::Continuation
		{
			Kernel::ContRef cont_;
		public:
			AssertStructureContinuation( const Kernel::ContRef & cont, const Ast::SourceLocation & traceLoc );
			~AssertStructureContinuation( );
			virtual void takeValue( const RefCountPtr< const Lang::Value > & val, Kernel::EvalState * evalState, bool dummy ) const;
			virtual Kernel::ContRef up( ) const;
			virtual RefCountPtr< const char > description( ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked );
		};

	} /* End of namespace Kernel. */

	namespace Ast
	{
		class LexiographicVariable : public Expression
		{
			const Ast::Identifier * id_;
			Kernel::Environment::LexicalKey ** idKey_;
			size_t scope_level_;
		public:
			LexiographicVariable( const Ast::SourceLocation & loc, const Ast::Identifier * id, Kernel::Environment::LexicalKey ** idKey );
			virtual ~LexiographicVariable( );
			virtual void analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst );
			virtual void eval( Kernel::EvalState * evalState ) const;

			/* The following function may be called after the expression has been analyzed, to find out what kind of binding that
			 * the variable refers to.  Kernel bindings can be fetched without evaluation before the program starts.  Prelude and user
			 * top-level bindings need only be evaluated once.  Other bindings are harder to analyze, and should be evaluated
			 * each time.
			 * Note that the "level" here refers to the level of the binding, and not the variable expression referring to the binding.
			 *
			 * 0 means kernel bindings, 1 means prelude bindings, 2 means user top-level, and 3 or higher means a non-global binding.
			 */
			size_t scope_level( ) const { return scope_level_; }
			const Ast::Identifier * id( ) const { return id_; }
		};

		class PrivateAliasVariable : public Expression
		{
			const Ast::PlacedIdentifier & id_;
			RefCountPtr< const char > privateName_;
			Kernel::Environment::LexicalKey * aliasKey_;
		public:
			PrivateAliasVariable( const Ast::SourceLocation & loc, const Ast::PlacedIdentifier & id, const RefCountPtr< const char > & privateName );
			virtual ~PrivateAliasVariable( );
			virtual void analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst );
			virtual void eval( Kernel::EvalState * evalState ) const;
		};

		/* Evaluate contained expresion and keep forcing thunks until a value is obtained.
		 *
		 * This serves two purposes:
		 * 1) Users may use it to prevent delayed evaluation for efficiency reasons.  (The opposite, preventing
		 *    forced evaluation, is not possible to express.)
		 * 2) The kernel may use it during the analysis of the program, for example, to prevent delayed
		 *    evaluation of expressions with free states.
		 *
		 * Compare the immediate_ property, which only prevents delaying the expression itself.
		 *
		 * To illustrate, consider the difference between the following brackets:
		 * {
		 *   x: 1 + 1  <-- rhs not immediate
		 *   y: x      <-- rhs immediate
		 *   y
		 * }
		 * vs
		 * {
		 *   x: 1 + 1  <-- rhs not immediate
		 *   y: !x     <-- rhs immediate and forced
		 *   y
		 * }
		 *
		 * In the first program, x is initialized to a thunk since its right hand side is not immediate.  When
		 * it comes to y, the right hand side is immediately evaluated, meaning that lookup of x is performed.
		 * The lookup results in the thunk for x, which is then bound to y.  When y is returned as the result of
		 * the bracket, the addition 1 + 1 is still a thunk.
     *
		 * In the second program, x is again initialized to a thunk, but when it comes to y, evaluation does not
		 * proceed until a value is obtained.  This will cause the thunk 1 + 1 to be forced, and the lookup of x
		 * finally results in the value 2.  When y is returned as the result of the bracket, the addition thunk
		 * is already gone, and the result is the value 2.
		 *
		 * Since the computation of free states can be (just a little bit) costly, the analysis is not performed
		 * for every single expression -- care must be taken during the analysis to ensure it is set correctly
		 * wherever needed.
		 */
		class Force : public Expression
		{
			Ast::Expression * expr_;
		public:
			Force( const Ast::SourceLocation & loc, Ast::Expression * expr );
			virtual ~Force( );
			virtual void analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst );
			virtual void eval( Kernel::EvalState * evalState ) const;
		};

		class EvalOutsideExpr : public Expression
		{
			Ast::Expression * expr_;
		public:
			EvalOutsideExpr( const Ast::SourceLocation & loc, Ast::Expression * expr );
			virtual ~EvalOutsideExpr( );
			virtual void analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst );
			virtual void eval( Kernel::EvalState * evalState ) const;
		};

		class DefineVariable : public BindNode
		{
			Ast::Expression * expr_;
			mutable size_t ** idPos_;
		public:
			DefineVariable( const Ast::SourceLocation & idLoc, const Ast::PlacedIdentifier * id, Ast::Expression * expr, size_t ** idPos );
			virtual ~DefineVariable( );
			virtual void analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst );
			virtual void initializeEnvironment( Kernel::PassedEnv env, Kernel::PassedDyn dyn ) const;
			virtual void evalHelper( Kernel::EvalState * evalState ) const;
		};

		class DefineAlias : public Node
		{
			const Ast::SourceLocation idLoc_; /* Note: Not a const reference. */
			const Ast::PlacedIdentifier * id_;
			Ast::NamespaceReference expansion_;
			const Ast::SourceLocation expansionLoc_; /* Note: Not a const reference. */
		public:
			DefineAlias( const Ast::SourceLocation & idLoc, const Ast::SourceLocation & expansionLoc, const Ast::PlacedIdentifier * id, const Ast::NamespaceReference & expansion );
			virtual ~DefineAlias( );
			virtual void analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst );
			const Ast::PlacedIdentifier * id( ) const { return id_; }
			const Ast::SourceLocation & idLoc( ) const { return idLoc_; }
			const Ast::NamespaceReference & expansion( ) const { return expansion_; }
			const Ast::SourceLocation & expansionLoc( ) const { return expansionLoc_; }
		};

		class LexicalVariableLocationExpr : public Expression
		{
		public:
			typedef enum { INDEX, DEPTH, ABSID } Kind;
		private:
			const Ast::Identifier * id_;
			Kind kind_;
			RefCountPtr< const Lang::Value > value_;
		public:
			LexicalVariableLocationExpr( const Ast::SourceLocation & idLoc, const Ast::Identifier * id, Kind kind );
			virtual ~LexicalVariableLocationExpr( );
			virtual void analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst );
			virtual void eval( Kernel::EvalState * evalState ) const;
		};

		class LexicalVariableNameExpr : public Expression
		{
			RefCountPtr< const Lang::Value > value_;
		public:
			LexicalVariableNameExpr( const Ast::SourceLocation & loc );
			virtual ~LexicalVariableNameExpr( );
			virtual void analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst );
			virtual void eval( Kernel::EvalState * evalState ) const;
		};

		class StructSplitReference : public Expression
		{
			Ast::SourceLocation structLoc_; /* Note: Not a const reference. */
			size_t ** structPos_;
			const char * fieldId_;
			size_t fieldPos_;
			Ast::Expression * defaultExpr_;
		public:
			StructSplitReference( const Ast::SourceLocation & fieldLoc, const char * fieldId, Ast::Expression * defaultExpr );
			StructSplitReference( const Ast::SourceLocation & fieldLoc, size_t fieldPos, Ast::Expression * defaultExpr );
			virtual ~StructSplitReference( );
			void setStruct( const Ast::SourceLocation & structLoc, size_t ** structPos );
			bool isOrdered( ) const { return fieldId_ == 0; }
			virtual void analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst );
			virtual void eval( Kernel::EvalState * evalState ) const;
		};

		class StructSplitSink : public Expression
		{
			Ast::SourceLocation structLoc_; /* Note: Not a const reference. */
			size_t ** structPos_;
			size_t consumedArguments_;
		public:
			StructSplitSink( );
			virtual ~StructSplitSink( );
			void setStruct( const Ast::SourceLocation & structLoc, size_t ** structPos, size_t consumedArguments );
			virtual void analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst );
			virtual void eval( Kernel::EvalState * evalState ) const;
		};

		class AssertNoSinkNeeded : public Assertion
		{
			size_t orderedCount_;
			Ast::SourceLocation structLoc_; /* Note: Not a const reference. */
			size_t ** structPos_;
		public:
			AssertNoSinkNeeded( const Ast::SourceLocation & loc, size_t orderedCount, const Ast::SourceLocation & structLoc, size_t ** structPos );
			virtual ~AssertNoSinkNeeded( );
			virtual void analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst );
			virtual void eval( Kernel::EvalState * evalState ) const;
		};

		class SplitDefineVariables
		{
			static size_t splitVarCount;
			static PtrOwner_back_Access< std::list< const Ast::PlacedIdentifier * > > mem;
			const Ast::PlacedIdentifier * splitVarId_;
		public:
			std::list< std::pair< Ast::DefineVariable *, Ast::StructSplitReference * > > exprs_;
			Ast::DefineVariable * sinkDefine_;
			Ast::StructSplitSink * sinkExpr_;
			bool seenNamed_;
			bool seenDefault_;
			SplitDefineVariables( );
			Ast::PlacedIdentifier * newSplitVarId( ) const;
		};

		class StateReference : public Node
		{
		protected:
			Node * someParent_; // The inherited parent_ does not make sense for reused nodes.  Use this instead.
			Ast::AnalysisEnvironment * someAnalysisEnv_; // The inherited analysisEnv_ does not make sense for reused nodes.  Use this instead.
		public:
			StateReference( const Ast::SourceLocation & loc );
			virtual ~StateReference( );
			virtual Kernel::StateHandle getHandle( Kernel::PassedEnv env, Kernel::PassedDyn dyn ) const = 0;
		};

		class LexiographicState : public StateReference
		{
			const Ast::Identifier * id_;
			mutable Kernel::Environment::LexicalKey ** idKey_;
		public:
			LexiographicState( const Ast::SourceLocation & loc, const Ast::Identifier * id, Kernel::Environment::LexicalKey ** idKey );
			virtual ~LexiographicState( );
			virtual void analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst );
			Kernel::StateHandle getHandle( Kernel::PassedEnv env, Kernel::PassedDyn dyn ) const;
		};

		class DynamicState : public Ast::StateReference
		{
			const Ast::Identifier * id_;
			mutable Kernel::Environment::LexicalKey ** idKey_;
		public:
			DynamicState( const Ast::SourceLocation & loc, const Ast::Identifier * id );
			virtual ~DynamicState( );
			virtual void analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst );
			Kernel::StateHandle getHandle( Kernel::PassedEnv env, Kernel::PassedDyn dyn ) const;
		};

		class IntroduceState : public BindNode
		{
			Ast::Expression * expr_;
			mutable size_t ** idPos_;
			Ast::StateID stateID_;
		public:
			IntroduceState( const Ast::SourceLocation & idLoc, const Ast::PlacedIdentifier * id, Ast::Expression * expr, size_t ** idPos );
			virtual ~IntroduceState( );
			virtual void analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst );
			virtual void initializeEnvironment( Kernel::PassedEnv env, Kernel::PassedDyn dyn ) const;
			virtual void evalHelper( Kernel::EvalState * evalState ) const;
			Ast::StateID getStateID( ) const { return stateID_; } /* Make sure analyze_impl has been called first! */
		};

		class Insertion : public Expression
		{
			Ast::StateReference * stateRef_;
			Ast::Expression * expr_;
		public:
			Insertion( Ast::StateReference * stateRef, Ast::Expression * expr );
			virtual ~Insertion( );
			virtual void analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst );
			virtual void eval( Kernel::EvalState * evalState ) const;
		};

		class InsertionMutatorCall : public Expression
		{
			Ast::Expression * expr_;
		public:
			InsertionMutatorCall( const Ast::SourceLocation & loc, Ast::Expression * expr );
			~InsertionMutatorCall( );
			virtual void analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst );
			virtual void eval( Kernel::EvalState * evalState ) const;
			Ast::Expression * getExpr( ) { return expr_; }
		};

		class Freeze : public Expression
		{
			const Ast::PlacedIdentifier * id_;
			mutable size_t ** idPos_;
		public:
			Freeze( const Ast::SourceLocation & idLoc, const Ast::PlacedIdentifier * id, size_t ** idPos );
			virtual ~Freeze( );
			virtual void analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst );
			virtual void eval( Kernel::EvalState * evalState ) const;
		};

		class Peek : public Expression
		{
			Ast::StateReference * stateRef_;
		public:
			Peek( const Ast::SourceLocation & idLoc, Ast::StateReference * stateRef );
			virtual ~Peek( );
			virtual void analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst );
			virtual void eval( Kernel::EvalState * evalState ) const;
		};

		class EvalSymbolFunction : public Lang::Function
		{
			Ast::SourceLocation loc_; /* Note: Not a const reference. */
			Ast::Expression * expr_;
			Ast::AnalysisEnvironment * analysisEnv_;
			RefCountPtr< const Ast::NamespacePath > lexicalPath_;
		public:
			EvalSymbolFunction( const Ast::SourceLocation & loc, Ast::Expression * expr, const RefCountPtr< const Ast::NamespacePath > & lexicalPath );
			virtual ~EvalSymbolFunction( );
			void push_exprs( Ast::ArgListExprs * args ) const;
			virtual void analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst );
			virtual void call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked ){ };
			virtual bool isTransforming( ) const { return false; }
		};

		class MemberReferenceFunction : public Lang::Function
		{
			Ast::SourceLocation loc_; /* Note: Not a const reference. */
			Ast::Expression * variable_;
			const char * fieldID_;
		public:
			MemberReferenceFunction( const Ast::SourceLocation & loc, Ast::Expression * variable, const char * fieldID );
			virtual ~MemberReferenceFunction( );
			void push_exprs( Ast::ArgListExprs * args ) const;
			virtual void analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst );
			virtual void call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked ){ };
			virtual bool isTransforming( ) const { return false; }
		};

		class MutatorReference : public Ast::Expression
		{
			Ast::SourceLocation mutatorLoc_; /* Note: Not a const reference. */
			Ast::StateReference * state_;
			const char * mutatorID_;
		public:
			MutatorReference( const Ast::SourceLocation & mutatorLoc, Ast::StateReference * state, const char * mutatorID );
			virtual ~MutatorReference( );
			virtual void analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst );
			virtual void eval( Kernel::EvalState * evalState ) const;
		};

		class SpecialLength : public Expression
		{
			double val_;
			int sort_;
		public:
			SpecialLength( const Ast::SourceLocation & loc, double val, int sort );
			virtual ~SpecialLength( );
			virtual void analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst );
			virtual void eval( Kernel::EvalState * evalState ) const;
		};

		class DynamicVariable : public Ast::Expression
		{
			const Ast::Identifier * id_;
			mutable Kernel::Environment::LexicalKey ** idKey_;
		public:
			DynamicVariable( const Ast::SourceLocation & loc, const Ast::Identifier * id );
			virtual ~DynamicVariable( );
			virtual void analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst );
			virtual void eval( Kernel::EvalState * evalState ) const;
		};

		class DynamicBindingExpression : public Ast::Expression
		{
			Ast::SourceLocation idLoc_; /* Note: Not a const reference. */
			const Ast::Identifier * id_;
			Ast::Expression * expr_;
			mutable Kernel::Environment::LexicalKey ** idKey_;
		public:
			DynamicBindingExpression( const Ast::SourceLocation & idLoc, const Ast::Identifier * id, Ast::Expression * expr, Kernel::Environment::LexicalKey ** idKey );
			virtual ~DynamicBindingExpression( );
			virtual void analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst );
			virtual void eval( Kernel::EvalState * evalState ) const;
			const Ast::SourceLocation & idLoc( ) const { return idLoc_; }
			const Ast::SourceLocation & exprLoc( ) const { return expr_->loc( ); }
			const Ast::Identifier * id( ) const { return id_; }
		};

		class DynamicStateBindingExpression : public Ast::Expression
		{
			Ast::SourceLocation dstLoc_; /* Note: Not a const reference. */
			const Ast::Identifier * dstId_;
			mutable Kernel::Environment::LexicalKey ** dstIdKey_;
			Ast::StateReference * src_;
		public:
			DynamicStateBindingExpression( const Ast::SourceLocation & loc, const Ast::SourceLocation & dstLoc, const Ast::Identifier * dstId, Ast::StateReference * src );
			virtual ~DynamicStateBindingExpression( );
			virtual void analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst );
			virtual void eval( Kernel::EvalState * evalState ) const;
		};

		class WithDynamicExpr : public Ast::Expression
		{
			Ast::Expression * bindings_;
			Ast::Expression * expr_;
		public:
			WithDynamicExpr( const Ast::SourceLocation & loc, Ast::Expression * bindings, Ast::Expression * expr );
			virtual ~WithDynamicExpr( );
			virtual void analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst );
			virtual void eval( Kernel::EvalState * evalState ) const;
		};

		class DynamicVariableDecl : public BindNode
		{
			mutable size_t ** idPos_;
			Ast::Expression * impl_;
		public:
			DynamicVariableDecl( const Ast::SourceLocation & loc, const Ast::SourceLocation & idLoc, const Ast::PlacedIdentifier * id, Ast::Expression * filterExpr, Ast::Expression * defaultExpr );
			virtual ~DynamicVariableDecl( );
			virtual void analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst );
			virtual void initializeEnvironment( Kernel::PassedEnv env, Kernel::PassedDyn dyn ) const;
			virtual void evalHelper( Kernel::EvalState * evalState ) const;
		};

		class DynamicVariableDeclFunction : public Lang::Function
		{
			const Ast::PlacedIdentifier * id_;
			Ast::Expression * filterExpr_;
			Ast::Expression * defaultExpr_;
			size_t ** idPos_;
		public:
			DynamicVariableDeclFunction( const Ast::PlacedIdentifier * id, Ast::Expression * filterExpr, Ast::Expression * defaultExpr, size_t ** idPos );
			virtual ~DynamicVariableDeclFunction( );
			void push_exprs( Ast::ArgListExprs * args ) const;
			virtual void analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst );
			virtual void call( Kernel::EvalState * evalState, Kernel::Arguments & args, const Ast::SourceLocation & callLoc ) const;
			virtual void gcMark( Kernel::GCMarkedSet & marked ){ };
			virtual bool isTransforming( ) const { return false; }
		};

		class DynamicStateDecl : public BindNode
		{
			mutable size_t ** idPos_;
			Ast::StateReference * defaultState_;
		public:
			DynamicStateDecl( const Ast::SourceLocation & loc, const Ast::SourceLocation & idLoc, const Ast::PlacedIdentifier * id, Ast::StateReference * defaultState, size_t ** idPos );
			virtual ~DynamicStateDecl( );
			virtual void analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst );
			virtual void initializeEnvironment( Kernel::PassedEnv env, Kernel::PassedDyn dyn ) const;
			virtual void evalHelper( Kernel::EvalState * evalState ) const;
		};

		class DynamicExpression : public Ast::Expression
		{
			Ast::Expression * expr_;
		public:
			DynamicExpression( const Ast::SourceLocation & loc, Ast::Expression * expr );
			virtual ~DynamicExpression( );
			virtual void analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst );
			virtual void eval( Kernel::EvalState * evalState ) const;
		};

		class LexiographicType : public Expression
		{
			const Ast::Identifier * id_;
			mutable Kernel::Environment::LexicalKey ** idKey_;
		public:
			LexiographicType( const Ast::SourceLocation & loc, const Ast::Identifier * id, Kernel::Environment::LexicalKey ** idKey );
			virtual ~LexiographicType( );
			virtual void analyze_impl( Ast::Node * parent, Ast::AnalysisEnvironment * env, Ast::StateIDSet * freeStatesDst );
			virtual void eval( Kernel::EvalState * evalState ) const;
		};


	}
}
