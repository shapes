/* This file is part of Shapes.
 *
 * Shapes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Shapes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2008, 2009 Henrik Tidefelt
 */

#pragma once

#include "Shapes_Kernel_decls.h"

#include "simplepdfi.h"
#include "simplepdfo.h"
#include "globals.h"
#include "consts.h"
#include "shapesexceptions.h"
#include "hottypes.h"
#include "multipage.h"
#include "continuations.h"
#include "charconverters.h"
#include "pagecontentstates.h"
#include "autoonoff.h"
#include "shapesscanner.h"
#include "texlabelmanager.h"
#include "debuglog.h"
#include "config.h"

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <unistd.h>
#include <stdlib.h>
#include <list>
#include <time.h>
#include <errno.h>
#include <limits>
#include <iconv.h>
#ifdef HAVE_FONTCONFIG
#include <fontconfig/fontconfig.h>
#endif

int shapesparse( );
extern int shapesdebug;
void printHelp( );
void printVersion( );

enum SplitMode{ SPLIT_NO, SPLIT_FLAT, SPLIT_DIR };

void argcAssertion( const char * optionSpecifier, int argc, int argcMin );
bool strprefixcmp( char * str, const char * prefix, const char ** prefixDst, char ** endp );
bool strtobool( const char * str, const char * containingString, const char * trueLabel = 0, const char * falseLabel = 0 );
std::string absoluteFilename( const char * filename );
std::string absoluteDirectory( const char * filename );
void ensureTmpDirectoryExists( const std::string & dirname, bool allowCreate );
RefCountPtr< std::ifstream > performIterativeStartup( const std::string & texJobName );
void abortProcedure( int exitCode );
void shipout( Shapes::Kernel::ValueRef resultUntyped, Shapes::Kernel::PassedDyn baseDyn, Shapes::Kernel::WarmCatalog::ShipoutList * documents, SplitMode splitMode );
void writeOutput( std::string & outputName, const Shapes::Kernel::WarmCatalog::ShipoutList & documents, SplitMode splitMode );
void setupGlobals( );
namespace Shapes
{
	namespace Interaction
	{
		class PreviewOptions
		{
		public:
			enum XpdfAction{ XPDF_DEFAULT, XPDF_RAISE, XPDF_RELOAD, XPDF_QUIT, XPDF_NOSERVER };
		public:
			bool launch_xpdf;
			XpdfAction xpdfAction;
			std::string xpdfServer;
			bool do_open;
			const char * do_open_application;
		public:
			PreviewOptions( );
			void preview( const std::string & filename, bool splitNoLaunch = false ) const;
		protected:
			void xpdfHelper( const std::string & filename ) const;
			void openHelper( const std::string & filename ) const;
		};

		class InteractionFormats
		{
			const char * prompt_;
			const char * show_;
			const char * file_;
			const char * bye_;
		public:
			InteractionFormats( );
			void setPrompt( const char * format );
			void setShow( const char * format );
			void setFile( const char * format );
			void setBye( const char * format );

			void formatPrompt( std::ostream & os, size_t inputNo ) const;
			void formatShow( std::ostream & os, size_t inputNo ) const;
			void formatFile( std::ostream & os, size_t inputNo ) const;
			void formatBye( std::ostream & os, size_t inputNo ) const;

		private:
			void writePrompt( std::ostream & os, const char * format, size_t inputNo ) const;
			void writeEscape( std::ostream & os, char c ) const;
		};

	}

	namespace Kernel
	{
		class InteractionContinuation;

		class WarmDebugger : public Kernel::State
		{
		public:
			Kernel::Environment * env_;
			Kernel::PassedDyn dyn_;
			Kernel::ContRef resume_;
			Ast::Expression * breakExpr_;
			WarmDebugger( Kernel::Environment * env, Kernel::PassedDyn dyn, Kernel::ContRef resume, Ast::Expression * breakExpr );
			virtual ~WarmDebugger( );
			virtual void tackOnImpl( Kernel::EvalState * evalState, const RefCountPtr< const Lang::Value > & piece, const Ast::SourceLocation & callLoc );
			virtual void peekImpl( Kernel::EvalState * evalState, const Ast::SourceLocation & callLoc );
			virtual void freezeImpl( Kernel::EvalState * evalState, const Ast::SourceLocation & callLoc );
			virtual void gcMark( Kernel::GCMarkedSet & marked );
			TYPEINFODECL;
		};

	}
}

extern Shapes::Interaction::PreviewOptions previewOptions;
extern Shapes::Interaction::InteractionFormats interactionFormats;

void addDefaultNeedPath( );
void addDefaultFontMetricsPath( );
void destroyGlobals( );
void escapeExtGlobChars( const std::string & str, std::ostream & dst );

void rmSplitFiles( const std::string & outputName, const char * sep );
void mkSplitDir( const std::string & outputName );
void noSplitOpen( std::ofstream * oFile, const std::string & outputName );
std::string splitOpen( std::ofstream * oFile, const std::string outputName, const char * sep, size_t physicalPageNo );
void interactiveEvaluation( std::istream & in, std::ostream & out, const std::string & outputName, SplitMode splitMode,
														bool evalTrace, bool evalBackTrace, bool cleanupMemory,
														RefCountPtr< std::ifstream > & labelDBFile, const std::string & labelDBName );
void nonInteractiveEvaluation( Shapes::Kernel::WarmCatalog::ShipoutList & documents, SplitMode splitMode,
															 bool evalTrace, bool evalBackTrace, bool cleanupMemory,
															 RefCountPtr< std::ifstream > & labelDBFile, const std::string & labelDBName );
void initCore( RefCountPtr< std::ifstream > & labelDBFile, const std::string & labelDBName );
