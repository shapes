/* This file is part of Shapes.
 *
 * Shapes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Shapes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2008 Henrik Tidefelt
 */

#pragma once

#include "FontMetrics_decls.h"

#include "charptrless.h"
#include "ptrowner.h"
#include "charconverters.h"
#include "config.h"

#include <vector>
#include <map>
#include <list>

#ifdef HAVE_FT2
#include <ft2build.h>
#include FT_FREETYPE_H
#endif

namespace FontMetrics
{
	class CharacterMetrics
	{
	public:
		size_t internalPosition_; // this is the position of this character in the containing charData_ vector of a WritingDirectionMetrics object.
		int characterCode_;
		double horizontalCharWidthX_;
		double horizontalCharWidthY_;
		double verticalCharWidthX_;
		double verticalCharWidthY_;
		double xmin_;
		double ymin_;
		double xmax_;
		double ymax_;
		double vX_;
		double vY_;

	private:
		// The following fields are mutable since they cannot be synchronized at an early stage, yet the object is not really
		// ready for access until synchronized.	Hence, it should be safe to store const pointers to objects of this type
		// in WritingDirectionMetrics, which are then synchronized once all characters are known by name.
		mutable std::map< size_t, size_t > ligatures_;
		mutable std::map< RefCountPtr< const char >, RefCountPtr< const char >, charRefPtrLess > * ligatureSetupMap_;

	public:
		CharacterMetrics( size_t internalPosition )
			: internalPosition_( internalPosition ),
				characterCode_( -1 ),
				horizontalCharWidthX_( 0 ),
				horizontalCharWidthY_( 0 ),
				verticalCharWidthX_( 0 ),
				verticalCharWidthY_( 0 ),
				xmin_( 0 ),
				ymin_( 0 ),
				xmax_( 0 ),
				ymax_( 0 ),
				vX_( 0 ),
				vY_( 0 ),
				ligatureSetupMap_( 0 )
		{ }
		~CharacterMetrics( );
		bool isEmpty( ) const;
		bool hasLigature( size_t otherInternalPosition, size_t * ligatureInternalPosition ) const;
		void addLigature( RefCountPtr< const char > otherName, RefCountPtr< const char > ligatureName );
		void setupLigatures( const std::map< RefCountPtr< const char >, size_t, charRefPtrLess > & nameMap ) const;

		void display( std::ostream & os ) const;
	};

	class WritingDirectionMetrics
	{
	public:
		WritingDirectionMetrics( );
		virtual ~WritingDirectionMetrics( );

		/* The result of this operation may, but does not have to, be the same pointer as is passed as <mem>. */
		virtual const CharacterMetrics * charByCode( Shapes::Kernel::UnicodeCodePoint code, CharacterMetrics * mem ) const = 0;

		/* This will always produce a pointer which is owned by the metrics object.
		 * The returned pointer is the special pointer only returned when a character is not found by charByCode.
		 */
		virtual const CharacterMetrics * default_char( ) const = 0;
	};

	class SingleByte_WritingDirectionMetrics : public WritingDirectionMetrics
	{
	public:
		double underlinePosition_;
		double underlineThickness_;
		double italicAngleRadians_;
		double charWidthX_;
		double charWidthY_;
		bool isFixedPitch_;

		PtrOwner_back_Access< std::vector< const CharacterMetrics * > > charData_;

		// The size_t is an index into charData_;
		std::map< RefCountPtr< const char >, size_t, charRefPtrLess > nameMap_;
		std::vector< size_t > codeMap_;

		SingleByte_WritingDirectionMetrics( );
		virtual ~SingleByte_WritingDirectionMetrics( );

		void setupLigatures( );
		const CharacterMetrics * charByName( const char * name ) const;
		const CharacterMetrics * charByCode_MacRoman( unsigned char code ) const;

		void display( std::ostream & os ) const;

		virtual const CharacterMetrics * charByCode( Shapes::Kernel::UnicodeCodePoint code, CharacterMetrics * mem ) const;
		virtual const CharacterMetrics * default_char( ) const;
	};

#ifdef HAVE_FT2
	class FreeType2_WritingDirectionMetrics : public WritingDirectionMetrics
	{
		FT_Face face_;
		FontMetrics::CharacterMetrics default_char_;
		bool vertical_;
		double font_unit_;
	public:
		FreeType2_WritingDirectionMetrics( FT_Face face, const FontMetric & parent, bool vertical );
		virtual ~FreeType2_WritingDirectionMetrics( );

		virtual const CharacterMetrics * charByCode( Shapes::Kernel::UnicodeCodePoint code, CharacterMetrics * mem ) const;
		virtual const CharacterMetrics * default_char( ) const;
	};
#endif

	class TrackKerning
	{
		double sizeLow_;
		double trackLow_;
		double sizeHigh_;
		double trackHigh_;
	public:
		TrackKerning( double sizeLow, double trackLow, double sizeHigh, double trackHigh );
		double operator () ( double sz ) const;
	};

	class FontMetric
	{
	public:
		RefCountPtr< const char > fontName_;
		RefCountPtr< const char > fullName_;
		RefCountPtr< const char > familyName_;
		RefCountPtr< const char > weight_;
		size_t weightNumber_;
		bool isCIDFont_;
		double fontBBoxXMin_;
		double fontBBoxYMin_;
		double fontBBoxXMax_;
		double fontBBoxYMax_;
		double ascender_;
		double descender_;
		double leading_;

		// Either of these may be null.
		RefCountPtr< WritingDirectionMetrics > horizontalMetrics_;
		RefCountPtr< WritingDirectionMetrics > verticalMetrics_;

		bool hasKerning_; /* This flag should only be true if there are no kern pairs, and only if it is expected that there _should_ have been at least some pairs defined. */
		typedef std::map< std::pair< Shapes::Kernel::UnicodeCodePoint, Shapes::Kernel::UnicodeCodePoint >, double > KernPairMap;
		KernPairMap horizontalKernPairsX_;
		KernPairMap horizontalKernPairsY_;
		KernPairMap verticalKernPairsX_;
		KernPairMap verticalKernPairsY_;

		FontMetric( bool isCIDFont )
			: fontName_( NullPtr< const char >( ) ),
				fullName_( NullPtr< const char >( ) ),
				familyName_( NullPtr< const char >( ) ),
				weight_( NullPtr< const char >( ) ),
				weightNumber_( 0 ),
				isCIDFont_( isCIDFont ),
				leading_( 0 ),
				horizontalMetrics_( NullPtr< WritingDirectionMetrics >( ) ),
				verticalMetrics_( NullPtr< WritingDirectionMetrics >( ) ),
				hasKerning_( false )
		{ }
		virtual ~FontMetric( );

		virtual double getHorizontalKernPairXByCode( Shapes::Kernel::UnicodeCodePoint code1, Shapes::Kernel::UnicodeCodePoint code2 ) const = 0;
	};

	class AFM : public FontMetric
	{
	public:
		RefCountPtr< const char > version_;
		RefCountPtr< const char > notice_;
		RefCountPtr< const char > encodingScheme_;
		RefCountPtr< const char > characterSet_;
		size_t charCount_;
		double capHeight_;
		double xHeight_;
		double stdHW_;
		double stdVW_;

		double vVectorX_;
		double vVectorY_;
		bool isFixedV_;

		std::map< int, RefCountPtr< TrackKerning > > trackKernings_;

		typedef std::pair< RefCountPtr< const char >, RefCountPtr< const char > > AssortedInfo;
		std::list< AssortedInfo > assortedGlobalInfo_;
		std::list< AssortedInfo > comments_;

		AFM( );
		virtual ~AFM( );

		virtual double getHorizontalKernPairXByCode( Shapes::Kernel::UnicodeCodePoint code1, Shapes::Kernel::UnicodeCodePoint code2 ) const;
	};

#ifdef HAVE_FT2
	class FreeType2_Metric : public FontMetric
	{
		FT_Face face_;
		double font_unit_;
	public:
		FreeType2_Metric( FT_Face face );
		virtual ~FreeType2_Metric( );

		virtual double getHorizontalKernPairXByCode( Shapes::Kernel::UnicodeCodePoint code1, Shapes::Kernel::UnicodeCodePoint code2 ) const;
	};
#endif

}
