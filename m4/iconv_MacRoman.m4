AC_DEFUN([AM_ICONV_MACROMAN],
[

	AC_MSG_CHECKING(for iconv name for Mac-Roman)

	if iconv -l | grep MacRoman > /dev/null
	then
	  MAC_ROMAN=[\"MacRoman\"]
	else
		if iconv -l | grep MACROMAN > /dev/null
		then
		  MAC_ROMAN=[\"MACROMAN\"]
		else
			if iconv -l | grep CSMACINTOSH > /dev/null
		  then
		    MAC_ROMAN=[\"CSMACINTOSH\"]
  		else
  			if iconv -l | grep Macintosh > /dev/null
	  	  then
		      MAC_ROMAN=[\"Macintosh\"]
  	  	else
			  	AC_MSG_ERROR([cannot find iconv name for Mac-Roman!])
		      MAC_ROMAN=[\"missing\"]
			  fi
			fi
		fi
	fi
	AC_MSG_RESULT($MAC_ROMAN)
  AC_DEFINE_UNQUOTED(MAC_ROMAN, $MAC_ROMAN, [Name to use with iconv to refer to Mac-Roman.])
])
