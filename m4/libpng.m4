dnl Check to find the zlib headers/libraries

AC_DEFUN([AM_PATH_LIBPNG],
[
  AC_ARG_WITH(libpng-exec-prefix,[  --with-libpng-exec-prefix=PFX Exec prefix where libpng-config is installed (optional)],
              libpng_exec_prefix="$withval", libpng_exec_prefix="")

  if test "x${LIBPNG_CONFIG+set}" != xset ; then
     if test "x$libpng_exec_prefix" != x ; then
        LIBPNG_CONFIG="$libpng_exec_prefix/bin/libpng-config"
     fi
  fi

  AC_PATH_PROG(LIBPNG_CONFIG, libpng-config, no)
  min_libpng_version=ifelse([$1], ,1.2.12,$1)
  AC_MSG_CHECKING(for libpng - version >= $min_libpng_version)
  no_libpng=""
  if test "$LIBPNG_CONFIG" = "no" ; then
    no_libpng=yes
  else
    LIBPNG_CFLAGS=`$LIBPNG_CONFIG --cflags`
    LIBPNG_LIBS=`$LIBPNG_CONFIG --libs`

    libpng_major_version=`$LIBPNG_CONFIG --version | \
           sed 's/^\([[0-9]]*\).*/\1/'`
    if test "x${libpng_major_version}" = "x" ; then
       libpng_major_version=0
    fi

    libpng_minor_version=`$LIBPNG_CONFIG --version | \
           sed 's/^\([[0-9]]*\)\.\{0,1\}\([[0-9]]*\).*/\2/'`
    if test "x${libpng_minor_version}" = "x" ; then
       libpng_minor_version=0
    fi

    libpng_micro_version=`$LIBPNG_CONFIG --version | \
           sed 's/^\([[0-9]]*\)\.\{0,1\}\([[0-9]]*\)\.\{0,1\}\([[0-9]]*\).*/\3/'`
    if test "x${libpng_micro_version}" = "x" ; then
       libpng_micro_version=0
    fi

    ac_save_CFLAGS="$CFLAGS"
    ac_save_LIBS="$LIBS"
    CFLAGS="$CFLAGS $LIBPNG_CFLAGS"
    LIBS="$LIBS $LIBPNG_LIBS"

    rm -f conf.libpngtest
    AC_TRY_RUN([
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char* my_strdup (const char *str);

char*
my_strdup (const char *str)
{
  char *new_str;

  if (str)
    {
      new_str = (char *)malloc ((strlen (str) + 1) * sizeof(char));
      strcpy (new_str, str);
    }
  else
    new_str = NULL;

  return new_str;
}

int main (void)
{
  int major = 0, minor = 0, micro = 0;
  int n;
  char *tmp_version;

  system ("touch conf.libpngtest");

  /* HP/UX 9 (%@#!) writes to sscanf strings */
  tmp_version = my_strdup("$min_libpng_version");

  n = sscanf(tmp_version, "%d.%d.%d", &major, &minor, &micro) ;

  if (n != 2 && n != 3) {
     printf("%s, bad version string\n", "$min_libpng_version");
     exit(1);
   }

   if (($libpng_major_version > major) ||
      (($libpng_major_version == major) && ($libpng_minor_version > minor)) ||
      (($libpng_major_version == major) && ($libpng_minor_version == minor) && ($libpng_micro_version >= micro)))
    {
      exit(0);
    }
  else
    {
      printf("\n*** 'libpng-config --version' returned %d.%d.%d, but the minimum version\n", $libpng_major_version, $libpng_minor_version, $libpng_micro_version);
      printf("*** of libpng required is %d.%d.%d. If libpng-config is correct, then it is\n", major, minor, micro);
      printf("*** best to upgrade to the required version.\n");
      printf("*** If libpng-config was wrong, set the environment variable LIBPNG_CONFIG\n");
      printf("*** to point to the correct copy of libpng-config, and remove the file\n");
      printf("*** config.cache before re-running configure\n");
      exit(1);
    }
}
],, no_libpng=yes,[echo $ac_n "cross compiling; assumed OK... $ac_c"])
       CFLAGS="$ac_save_CFLAGS"
       LIBS="$ac_save_LIBS"
  fi
  if test "x$no_libpng" = x ; then
     AC_DEFINE_UNQUOTED(HAVE_LIBPNG, true, [Define to true if libpng is installed])
		 HAVE_LIBPNG=true
     AC_MSG_RESULT(yes)
     ifelse([$2], , :, [$2])
  else
     AC_MSG_RESULT(no)
		 HAVE_LIBPNG=false
     if test "$LIBPNG_CONFIG" = "no" ; then
       echo "*** The libpng-config script installed with libpng could not be found"
       echo "*** If libpng was installed in PREFIX, make sure PREFIX/bin is in your path,"
       echo "*** or use the --with-libpng-exec-prefix=PFX option to configure."
       echo "*** You may also set the LIBPNG_CONFIG environment variable to the full path"
       echo "*** to libpng-config."
     else
       if test -f conf.libpngtest ; then
        :
       else
          echo "*** Could not run libpng test program, checking why..."
          CFLAGS="$CFLAGS $LIBPNG_CFLAGS"
          LIBS="$LIBS $LIBPNG_LIBS"
          AC_TRY_LINK([
#include <stdio.h>
],      [ return 0; ],
        [ echo "*** The test program compiled, but did not run. This usually means"
          echo "*** that the run-time linker is not finding libpng or finding the wrong"
          echo "*** version of libpng. If it is not finding libpng, you'll need to set your"
          echo "*** LD_LIBRARY_PATH environment variable, or edit /etc/ld.so.conf to point"
          echo "*** to the installed location  Also, make sure you have run ldconfig if that"
          echo "*** is required on your system"
	  echo "***"
          echo "*** If you have an old version installed, it is best to remove it, although"
          echo "*** you may also be able to get things to work by modifying LD_LIBRARY_PATH"],
        [ echo "*** The test program failed to compile or link. See the file config.log for the"
          echo "*** exact error that occured. This usually means libpng was incorrectly installed"
          echo "*** or that you have moved libpng since it was installed. In the latter case, you"
          echo "*** may want to edit the libpng-config script: $LIBPNG_CONFIG" ])
          CFLAGS="$ac_save_CFLAGS"
          LIBS="$ac_save_LIBS"
       fi
     fi
#     LIBPNG_CFLAGS=""
#     LIBPNG_LIBS=""
     ifelse([$3], , :, [$3])
  fi
  AC_SUBST(HAVE_LIBPNG)
  AC_SUBST(LIBPNG_CFLAGS)
  AC_SUBST(LIBPNG_LIBS)
  rm -f conf.libpngtest
])
