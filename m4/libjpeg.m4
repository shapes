dnl Check to find the libjpeg headers/libraries

AC_DEFUN([AM_PATH_LIBJPEG],
[
  AC_ARG_WITH(libjpeg-include,
    [  --with-libjpeg-include=DIR Where libjpeg headers are installed],
    LIBJPEG_CFLAGS="-I$withval", LIBJPEG_CFLAGS="" )

  AC_ARG_WITH(libjpeg-lib,
    [  --with-libjpeg-lib=DIR     Where the libjpeg library is installed],
    LIBJPEG_LIBS="-L$withval", LIBJPEG_LIBS="" )

  ac_save_CFLAGS="$CFLAGS"
  ac_save_LIBS="$LIBS"

  no_libjpeg=""

  if test "x${LIBJPEG_CFLAGS}X" != xX ; then
    CFLAGS = "$LIBJPEG_CFLAGS"
  fi
  if test "x${LIBJPEG_LIBS}X" != xX ; then
    LIBS = "$LIBJPEG_LIBS"
  fi

  AC_CHECK_HEADERS(jpeglib.h,
    ,
		no_libjpeg=yes
    [AC_MSG_WARN("libjpeg header files not found.")]
  )

  AC_CHECK_LIB(jpeg, jpeg_read_header,
    [LIBJPEG_LIBS="$LIBJPEG_LIBS -ljpeg"],
		no_libjpeg=yes
    [AC_MSG_WARN("libjpeg libraries not found.")]
  )

  if test "x$no_libjpeg" = x ; then
     AC_DEFINE_UNQUOTED(HAVE_LIBJPEG, true, [Define to true if libjpeg is installed])
		 HAVE_LIBJPEG=true
  else
		 HAVE_LIBJPEG=false
  fi

  CFLAGS="$ac_save_CFLAGS"
  LIBS="$ac_save_LIBS"
  AC_SUBST(HAVE_LIBJPEG)
  AC_SUBST(LIBJPEG_CFLAGS)
  AC_SUBST(LIBJPEG_LIBS)
])
