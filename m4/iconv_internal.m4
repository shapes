AC_DEFUN([AM_ICONV_INTERNAL],
[
	AC_MSG_CHECKING(for iconv name for Unicode in uint32_t)

  AC_TRY_RUN([
#include <stdint.h>

void helper_BE( uint32_t * a, unsigned char ** b, unsigned char x );

void
helper_BE( uint32_t * a, unsigned char ** b, unsigned char x )
{
  *a *= 0x100;
	*a += x;
	**b = x;
	++(*b);
}

int
main( )
{
  uint32_t a = 0;
  uint32_t b = 0;
  unsigned char * dst = ( unsigned char * )( & b );

	/* The following sequence will result in the same value in a and b
	 * if the machine is big endian.  (Four values are picked, rather than
	 * trying every possible value!  This is why the test is not "if and only if".)
	 * It is only after all four steps have been completed that the two
	 * would be the same, since b is written left to right, while a is
	 * shifted before each new value.
	 */
	helper_BE( & a, & dst, 0x01 );
	helper_BE( & a, & dst, 0x80 );
	helper_BE( & a, & dst, 0x04 );
	helper_BE( & a, & dst, 0x20 );

	return ( a == b ) ? 0 : 1;
}
    ],
    [
		  UCS_4_INTERNAL=[\"UCS-4BE\"]
    ],[
  AC_TRY_RUN([
#include <stdint.h>

void helper_LE( uint32_t * a, unsigned char ** b, unsigned char x );

void
helper_LE( uint32_t * a, unsigned char ** b, unsigned char x )
{
  *a *= 0x100;
	*a += x;
	--(*b);
	**b = x;
}

int
main( )
{
  uint32_t a = 0;
  uint32_t b = 0;
  unsigned char * dst = ( unsigned char * )( & b ) + sizeof( b );

	/* The following sequence will result in the same value in a and b
	 * if the machine is big endian.  (Four values are picked, rather than
	 * trying every possible value!  This is why the test is not "if and only if".)
	 * It is only after all four steps have been completed that the two
	 * would be the same, since b is written left to right, while a is
	 * shifted before each new value.
	 */
	helper_LE( & a, & dst, 0x01 );
	helper_LE( & a, & dst, 0x80 );
	helper_LE( & a, & dst, 0x04 );
	helper_LE( & a, & dst, 0x20 );

	return ( a == b ) ? 0 : 1;
}
    ],
    [
		  UCS_4_INTERNAL=[\"UCS-4LE\"]
    ],[
		  AC_MSG_WARN([Neither big nor little endian - hopefully, UCS-4-INTERNAL is supported anyway.])
		  UCS_4_INTERNAL=[\"UCS-4-INTERNAL\"]
		],)
		],)
	AC_MSG_RESULT($UCS_4_INTERNAL)
  AC_DEFINE_UNQUOTED(UCS_4_INTERNAL, $UCS_4_INTERNAL, [Name to use with iconv to refer to Unicode in uint32_t.])
])
