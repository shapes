AC_DEFUN([AC_PROG_FLEX],
[
  AM_PROG_LEX
  min_flex_version=ifelse([$1], ,2.5.30,$1)
  AC_MSG_CHECKING(for flex - version >= $min_flex_version)
  if test "$LEX" != flex; then
    LEX="$am_aux_dir/oldtool flex ${min_flex_version}"
    AC_SUBST([LEX_OUTPUT_ROOT], [lex.yy])
    AC_SUBST([LEXLIB], [''])
		AC_SUBST(HAVE_FLEX, 0)
    AC_MSG_RESULT(no)
  else
    flex_version=`$LEX --version | sed -e 's/^flex //' -e 's/version //'`

    AC_TRY_RUN([
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char* my_strdup (const char *str);

char* my_strdup (const char *str)
{
  char *new_str;

  if (str)
    {
      new_str = (char *)malloc ((strlen (str) + 1) * sizeof(char));
      strcpy (new_str, str);
    }
  else
    new_str = NULL;

  return new_str;
}

int main (void)
{
  int major = 0, minor = 0, micro = 0;
  int min_major = 0, min_minor = 0, min_micro = 0;
  int n;
  char *tmp_version;

  /* HP/UX 9 (%@#!) writes to sscanf strings */
  tmp_version = my_strdup("$min_flex_version");
  n = sscanf(tmp_version, "%d.%d.%d", &min_major, &min_minor, &min_micro);

  if (n != 2 && n != 3) {
     printf("%s, bad minimum version string\n", "$min_flex_version");
     exit(1);
   }

  tmp_version = my_strdup("$flex_version");
  n = sscanf(tmp_version, "%d.%d.%d", &major, &minor, &micro) ;

  if (n != 2 && n != 3) {
     printf("%d, %s, bad version string\n", n, "$flex_version");
     exit(1);
   }

  if ((major > min_major) ||
      ((major == min_major) && (minor > min_minor)) ||
      ((major == min_major) && (minor == min_minor) && (micro >= min_micro)))
    {
      exit(0);
    }
  else
    {
/*
	printf("WARNING: \`flex' on your system reports version %d.%d.%d, but version\n"
	       "         %d.%d.%d is required.  You should only need it if you\n"
               "         modified a .ll file.  You may need the \`Flex' package\n"
               "         in order for those modifications to take effect.  You can get\n"
               "         directions for how to obtain a recent \`Flex' from any GNU\n"
	       "         archive site.",
	       major, minor, micro, min_major, min_minor, min_micro);
*/
      exit(1);
    }
}
    ], [
      AC_SUBST(HAVE_FLEX, 1)
      AC_MSG_RESULT(yes)
    ], [
      LEX="$am_aux_dir/oldtool flex ${min_flex_version}"
      AC_SUBST([LEX_OUTPUT_ROOT], [lex.yy])
      AC_SUBST([LEXLIB], [''])
      AC_SUBST(HAVE_FLEX, 0)
      AC_MSG_RESULT(no)
    ],)
  fi
  cat <<EOF > lex_header.cc
#include "FlexLexer.h"
EOF
  FLEXLEXER_HEADER=`$CXX $CPPFLAGS -M -MP lex_header.cc | sed -n -e 's/\(.*FlexLexer.h\):.*/\\1/p' \
    | sed -e 's/\/\/\+/\//g'`
  AC_SUBST(FLEXLEXER_HEADER, [$FLEXLEXER_HEADER])
  rm lex_header.cc

# Note that it is basically required that $LEX equals "flex".  Now, if someone has installed a nice and new flex
# in a directory early on the path, but has forgotten to place the directory with the new FlexLexer.h equally early
# in the include path, there is trouble.  We now take some measures to detect this.
#
# The idea is that the flex program and FlexLexer.h should reside in sibling directories.  The most robust way to
# cover various cases of binary locations is to find the prefix used for the include, and then make sure that this
# prefix is also a prefix of the binary.
  FlexLexer_prefix=`echo ${FLEXLEXER_HEADER} | sed -n -e 's/\\(.*\\)\/[[^\\/]]\\{1,\\}\/FlexLexer\\.h/\\1/p'`
  prefix_sed_mask=`echo ${FlexLexer_prefix} | tr [[:print:]] .`
  flex_program_prefix=`which flex | sed -n -e "s/\\(${prefix_sed_mask}\\).*/\\1/p"`
  if test "X${FlexLexer_prefix}" != "X${flex_program_prefix}"; then
		 echo "Suspicious mismatch between location of flex program and FlexLexer.h:"
		 echo "  " `which flex` " vs " $FLEXLEXER_HEADER
		 echo "Treating as broken flex."
     LEX="$am_aux_dir/oldtool FlexLexer.h ${min_flex_version}"
     AC_MSG_RESULT(no)
     AC_SUBST(HAVE_FLEX, 0)
  fi

# In addition, any occurrence of <iostream.h> is a sign of a way too old FlexLexer.h.
  if test `grep -c 'iostream.h' ${FLEXLEXER_HEADER}` != 0; then
     echo "Found <iostream.h> in FlexLexer.h, which reminds of the much too old 2.5.4 version."
     echo "Please update FlexLexer.h to something more recent."
     echo "Treating as broken flex."
     LEX="$am_aux_dir/oldtool FlexLexer.h ${min_flex_version}"
     AC_MSG_RESULT(no)
     AC_SUBST(HAVE_FLEX, 0)
  fi
])
