AC_DEFUN([AM_DISPATCH_STYLE],
[
	AC_MSG_CHECKING(dispatch style)
	AC_ARG_WITH(dispatch,[  --with-dispatch=(VOID/NONE/VTBL/CASE)   Style of operator dispatch implementation (defaults to CASE).],
		dispatch_style="$withval", dispatch_style="CASE")

  case "x$dispatch_style" in
  xVOID)
    AC_DEFINE_UNQUOTED(DISPATCHSTYLE, [DISPATCHSTYLE_$dispatch_style], [Define to one of the macros listed in source/classtreemacros.h])
	  ;;
  xNONE)
    AC_DEFINE_UNQUOTED(DISPATCHSTYLE, [DISPATCHSTYLE_$dispatch_style], [Define to one of the macros listed in source/classtreemacros.h])
	  ;;
  xVTBL)
    AC_DEFINE_UNQUOTED(DISPATCHSTYLE, [DISPATCHSTYLE_$dispatch_style], [Define to one of the macros listed in source/classtreemacros.h])
	  ;;
  xCASE)
    AC_DEFINE_UNQUOTED(DISPATCHSTYLE, [DISPATCHSTYLE_$dispatch_style], [Define to one of the macros listed in source/classtreemacros.h])
	  ;;
  *)
    AC_MSG_ERROR( [Invalid dispatch style: $dispatch_style] )
    ;;
  esac

	AC_MSG_RESULT($dispatch_style)
])
