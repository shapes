/** This file is part of Shapes.
 **
 ** Shapes is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** any later version.
 **
 ** Shapes is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with Shapes.  If not, see <http://www.gnu.org/licenses/>.
 **
 ** Copyright 2013 Henrik Tidefelt
 **/

/** This extension is a replacement for the old extension conssupport.shext.
 **/

/** Lazy right fold.
 **/
foldr: \ op zero seq → [if [nil? seq] zero [op seq.car [foldr op zero seq.cdr]]]

/** Lazy right fold with index.
 **/
foldr_indexed: \ op zero seq →
{
  helper: \ i seq → [if [nil? seq] zero [op i seq.car [helper (i + '1) seq.cdr]]]
  [helper '0 seq]
}

/** Reverse sequence (eager).
 ** Note: The identifier "reverse" is occupied by a core function for reversing paths.
 **/
seq_reverse: \ seq → [seq.foldl (\ s e → [fcons e s]) nil]

/** Map (lazy and eager).
 **/
map: \ f seq → [foldr \ e s → [cons [f e] s] nil seq]
fmap: \ f seq → [seq.foldr \ e s → [cons [f e] s] nil]

/** Map over two sequences (lazy and eager).
 **/
map2: \ f s1 s2 → [if [nil? s1] or [nil? s2] nil [cons [f s1.car s2.car] [map2 f s1.cdr s2.cdr]]]
fmap2: \ f s1 s2 → [if [nil? s1] or [nil? s2] nil [fcons [f s1.car s2.car] [map2 f s1.cdr s2.cdr]]]

/** Filter (lazy and eager).
 **/
filter: \ pred seq → [foldr \ e s → [if [pred e] [cons e s] s] nil seq]
ffilter: \ pred seq → [seq.foldr \ e s → [if [pred e] [cons e s] s] nil]

/** Separate (lazy and eager).
 **/
separate: \ sep seq → [if [nil? seq] nil [cons seq.car [foldr \ e s → [cons sep [cons e s]] nil seq.cdr]]]
fseparate: \ sep seq → [if [nil? seq] nil seq.car ; [seq.cdr.foldr \ e s → ( sep ; e ; s ) nil]]

/** For each (eager).
 **/
foreach: \ f seq •dst → [seq.foldsl \ s e •dst → {[f e •dst] s} void •dst]

/** Insert (eager).
 **/
insert: \ seq •dst → [seq.foldsl \ s e •dst → {•dst << e  s} void •dst]

/** Drop n elements at the beginning.
 **/
drop: \ n seq →
{
  helper: \ i s →
    [if (i = '0)
      s
      [if [nil? s]
        [error 'out_of_range VARNAME `Can't drop more elements than length of list.´ n]
        [drop (i - '1) s.cdr]]]
  [if (n < '0)
    [error 'out_of_range VARNAME `Can't drop a negative number of elements.´ n]
    [helper n seq]
  ]
}

/** Take n elements at the beginning.
 **/
take: \ n seq →
{
  helper: \ i s →
    [if (i = '0)
      nil
      [if [nil? s]
        [error 'out_of_range VARNAME `Can't take more elements than length of list.´ n]
        [cons s.car [take (i - '1) s.cdr]]]]
  [if (n < '0)
    [error 'out_of_range VARNAME `Can't take a negative number of elements.´ n]
    [helper n seq]
  ]
}

/** Take element at index (eager).
 ** Second variant uses default value instead of throwing 'out_of_range.
 **/
seq_index: \ i seq →
{
  rest: [drop i seq]
  [if [nil? rest]
    [error 'out_of_range VARNAME `Index is just past end of sequence.´ i]
    rest.car
  ]
}
seq_index_default: \ i seq default →
  (escape_continuation return
    (escape_continuation error
      (escape_continue return
        [seq_index i seq]))
    >>
    \ ball →
      [if ball.kind = 'out_of_range
          default
          (escape_continue error ball)
      ]
  )

/** Sample every n'th element in a sequence.
 **/
downsample:
{
  helper: \ seq i n → [if [nil? seq] seq
                          [if i = n
                            [cons seq.car [helper seq.cdr '1 n]]
                            [helper seq.cdr i+'1 n]]]
  \ seq n →
    [if n < '1
      [error 'out_of_range VARNAME `Downsampling factor is less than unit.´ n]
      [helper seq n n]
    ]
}

/** Construct an array from the values in a (finite) sequence.
 **/
seq_vector: \ seq → array [] <>[unlist seq]

/** Group runs of equivalent elements into arrays.
 ** The equivalence test will be applied to adjacent elements, and whereever
 ** the test does not hold, the sequence is split.
 ** (There is no requirement that the test defines an equivalence relation.)
 **/
equivalent_runs:
{
  helper: \ test prev rest run revRes →
    [if [nil? rest]
      [seq_reverse [seq_vector [seq_reverse run]];revRes]
      [if [test prev rest.car]
        [helper test rest.car rest.cdr rest.car;run revRes]
        [helper test rest.car rest.cdr rest.car;nil [seq_vector [seq_reverse run]];revRes]
      ]
    ]

  \ test seq →
    [if [nil? seq]
      nil
      [helper test seq.car seq.cdr seq.car;nil nil]
    ]
}

/** Convert to string (eager).
 **/
seq_string: \ seq →
{
  •dst: ..Shapes..String..newString
  [insert seq •dst]
  freeze •dst
}
